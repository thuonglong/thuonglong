﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TramCanMinhNgoc
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateDatabase();
            WriteToFile();
            System.Windows.Forms.Application.Exit();
        }

        public void WriteToFile()
        {
            string IDChiNhanh = ConfigurationManager.AppSettings["IDChiNhanh"].ToString();
            string Path = ConfigurationManager.AppSettings["Path"].ToString();
            string Password = ConfigurationManager.AppSettings["Password"].ToString();
            string connectionString = ConfigurationManager.AppSettings["connectionString"].ToString();

            DateTime ngay = DateTime.Now;
            string year = ngay.Year.ToString();
            string mouth = ngay.Month.ToString();

            string maxIDMinhNgoc = "";
            string mdfFile = @"DataTPCan201912.mdb";
            try
            {
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();

                SqlCommand command = new SqlCommand("Select ID from [tblCan] where IDChiNhanh = '" + IDChiNhanh + "' order by ID desc", conn);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        maxIDMinhNgoc = String.Format("{0}", reader["id"]);
                    }
                }
                conn.Close();

                DataTable table = new DataTable();
                DataTable tableupdate = new DataTable();
                if (maxIDMinhNgoc == "")
                    maxIDMinhNgoc = "0";
                using (OleDbConnection connection = new OleDbConnection(string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Jet OLEDB:Database Password=" + Password + "", mdfFile)))
                {
                    using (OleDbCommand selectCommand = new OleDbCommand("SELECT * FROM DataPhieuCan where ID > " + maxIDMinhNgoc + " order by ID desc", connection))
                    {
                        connection.Open();
                        OleDbDataAdapter adapter = new OleDbDataAdapter();
                        adapter.SelectCommand = selectCommand;
                        adapter.Fill(table);

                        connection.Dispose();
                        connection.Close();
                    }
                }
                string saveStaff = "";
                if (table.Rows.Count > 0)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        string @ID = row["ID"].ToString();
                        string @SoPhieu = row["SoPhieu"].ToString();
                        string @BienSoXe = row["BienSo"].ToString();
                        string @LaiXe = row["LaiXe"].ToString();
                        string @NguoiBan = row["NguoiBan"].ToString();
                        string @NguoiMua = row["NguoiMua"].ToString();
                        string @LoaiHang = row["LoaiHang"].ToString();
                        string @KlCanL1 = row["TLcanL1"].ToString();
                        string @KlCanL2 = row["TLcanL2"].ToString();
                        string @TLHang = row["TLHang"].ToString();
                        string @TyKhoi = row["TyKhoi"].ToString();
                        string @DonGia = row["DonGia"].ToString();
                        string @TGian1 = row["TGian1"].ToString();
                        string @TGian2 = row["TGian2"].ToString();
                        string @Mota = row["Mota"].ToString();
                        string @XN = row["XN"].ToString();
                        string @TrucCan = row["TrucCan"].ToString();

                        saveStaff += " INSERT into tblCan VALUES (NEWID(),'" +
                            IDChiNhanh + "','" +
                            ID + "','" +
                            @SoPhieu + "','" +
                            @BienSoXe + "','" +
                            @LaiXe + "','" +
                            @NguoiBan + "','" +
                            @NguoiMua + "','" +
                            @LoaiHang + "','" +
                            @KlCanL1 + "','" +
                            @KlCanL2 + "','" +
                            @TLHang + "','" +
                            @TyKhoi + "','" +
                            @DonGia + "','" +
                            @TGian1 + "','" +
                            @TGian2 + "','" +
                            @Mota + "','" +
                            @XN + "','" +
                            @TrucCan + "')";
                    }
                }

                if (saveStaff != "")
                {
                    using (SqlConnection openCon = new SqlConnection("Data Source=(local);Initial Catalog=QLDN;User ID=sa;Password=anhkhoa123*"))
                    {
                        using (SqlCommand querySaveStaff = new SqlCommand(saveStaff))
                        {
                            querySaveStaff.Connection = openCon;
                            openCon.Open();
                            querySaveStaff.ExecuteNonQuery();
                            openCon.Close();
                            openCon.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (SqlConnection openCon = new SqlConnection("Data Source=(local);Initial Catalog=QLDN;User ID=sa;Password=anhkhoa123*"))
                {
                    using (SqlCommand querySaveStaff = new SqlCommand("INSERT into tblError VALUES ('" + ex.ToString() + "')"))
                    {
                        querySaveStaff.Connection = openCon;
                        openCon.Open();
                        querySaveStaff.ExecuteNonQuery();
                        openCon.Close();
                        openCon.Dispose();
                    }
                }
            }
        }

        public void UpdateDatabase()
        {
            string IDChiNhanh = ConfigurationManager.AppSettings["IDChiNhanh"].ToString();
            string Path = ConfigurationManager.AppSettings["Path"].ToString();
            string Password = ConfigurationManager.AppSettings["Password"].ToString();
            string connectionString = ConfigurationManager.AppSettings["connectionString"].ToString();

            DateTime ngay = DateTime.Now;
            string year = ngay.Year.ToString();
            string mouth = ngay.Month.ToString();

            string maxIDMinhNgoc = "";
            string mdfFile = @"DataTPCan201912.mdb";
            try
            {
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();

                SqlCommand command = new SqlCommand("Select ID from [tblCan] where IDChiNhanh = '" + IDChiNhanh + "' order by ID desc", conn);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        maxIDMinhNgoc = String.Format("{0}", reader["id"]);
                    }
                }
                conn.Close();

                DataTable tableupdate = new DataTable();
                if (maxIDMinhNgoc == "")
                    maxIDMinhNgoc = "0";
                using (OleDbConnection connection = new OleDbConnection(string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Jet OLEDB:Database Password=" + Password + "", mdfFile)))
                {
                    using (OleDbCommand selectCommand = new OleDbCommand("SELECT top 100 * FROM DataPhieuCan where ID < " + maxIDMinhNgoc + " order by ID desc", connection))
                    {
                        connection.Open();

                        OleDbDataAdapter adapter = new OleDbDataAdapter();
                        adapter.SelectCommand = selectCommand;
                        adapter.Fill(tableupdate);

                        connection.Dispose();
                        connection.Close();
                    }
                }
                string saveStaff = "";
                
                if (tableupdate.Rows.Count > 0)
                {
                    foreach (DataRow row in tableupdate.Rows)
                    {
                        string @ID = row["ID"].ToString();
                        string @SoPhieu = row["SoPhieu"].ToString();
                        string @BienSoXe = row["BienSo"].ToString();
                        string @LaiXe = row["LaiXe"].ToString();
                        string @NguoiBan = row["NguoiBan"].ToString();
                        string @NguoiMua = row["NguoiMua"].ToString();
                        string @LoaiHang = row["LoaiHang"].ToString();
                        string @KlCanL1 = row["TLcanL1"].ToString();
                        string @KlCanL2 = row["TLcanL2"].ToString();
                        string @TLHang = row["TLHang"].ToString();
                        string @TyKhoi = row["TyKhoi"].ToString();
                        string @DonGia = row["DonGia"].ToString();
                        string @TGian1 = row["TGian1"].ToString();
                        string @TGian2 = row["TGian2"].ToString();
                        string @Mota = row["Mota"].ToString();
                        string @XN = row["XN"].ToString();
                        string @TrucCan = row["TrucCan"].ToString();

                        saveStaff += " update tblCan set " +
                           " BienSoXe = '" + @BienSoXe + "'," +
                           " LaiXe    = '" + @LaiXe + "'," +
                           " NguoiBan = '" + @NguoiBan + "'," +
                           " NguoiMua = '" + @NguoiMua + "'," +
                           " LoaiHang = '" + @LoaiHang + "'," +
                           " KlCanL1  = '" + @KlCanL1 + "'," +
                           " KlCanL2  = '" + @KlCanL2 + "'," +
                           " TLHang   = '" + @TLHang + "'," +
                           " TyKhoi   = '" + @TyKhoi + "'," +
                           " DonGia   = '" + @DonGia + "'," +
                           " TGian1   = '" + @TGian1 + "'," +
                           " TGian2   = '" + @TGian2 + "'," +
                           " Mota     = '" + @Mota + "'," +
                           " XN       = '" + @XN + "'," +
                           " TrucCan  = '" + @TrucCan + "' where ID = " + @ID + "";
                    }
                }


                if (saveStaff != "")
                {
                    using (SqlConnection openCon = new SqlConnection("Data Source=(local);Initial Catalog=QLDN;User ID=sa;Password=anhkhoa123*"))
                    {
                        using (SqlCommand querySaveStaff = new SqlCommand(saveStaff))
                        {
                            querySaveStaff.Connection = openCon;
                            openCon.Open();
                            querySaveStaff.ExecuteNonQuery();
                            openCon.Close();
                            openCon.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (SqlConnection openCon = new SqlConnection("Data Source=(local);Initial Catalog=QLDN;User ID=sa;Password=anhkhoa123*"))
                {
                    using (SqlCommand querySaveStaff = new SqlCommand("INSERT into tblError VALUES ('" + ex.ToString() + "')"))
                    {
                        querySaveStaff.Connection = openCon;
                        openCon.Open();
                        querySaveStaff.ExecuteNonQuery();
                        openCon.Close();
                        openCon.Dispose();
                    }
                }
            }
        }

        OleDbConnection conn;
        private void btnConnect_Click(object sender, EventArgs e)
        {
            string IDChiNhanh = ConfigurationManager.AppSettings["IDChiNhanh"].ToString();
            string Path = ConfigurationManager.AppSettings["Path"].ToString();
            string Password = ConfigurationManager.AppSettings["Password"].ToString();
            string connectionString = ConfigurationManager.AppSettings["connectionString"].ToString();

            DateTime ngay = DateTime.Now;
            string year = ngay.Year.ToString();
            string mouth = ngay.Month.ToString();

            string maxIDMinhNgoc = "";
            string mdfFile = @"DataTPCan201912.mdb";
            var DBPath = Application.StartupPath + @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\MinhNgoc-Cam-Xoa\DataTPCan201912.mdb;;Jet OLEDB:Database Password=" + Password + "";
            conn = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + DBPath);
            conn.Open();
            System.Windows.Forms.MessageBox.Show("Connected successfully");


            //Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\MinhNgoc-Cam-Xoa\DataTPCan201912.mdb;Persist Security Info=True
        }
    }
}
