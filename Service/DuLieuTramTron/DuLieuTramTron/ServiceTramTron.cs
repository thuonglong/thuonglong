﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Reflection;
using System.Configuration;

namespace DuLieuTramTron
{
    public partial class ServiceTramTron : ServiceBase
    {
        Timer timer = new Timer();
        private System.Threading.Thread _thread;
        public ServiceTramTron()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _thread = new System.Threading.Thread(WriteToFile);
            _thread.Start();
            timer = new System.Timers.Timer();
            timer.Enabled = true;
            timer.Interval = 30000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(OnElapsedTime);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            WriteToFile();
        }
        public void WriteToFile()
        {
            DBDataContext db = new DBDataContext();
            string IDChiNhanh = ConfigurationManager.AppSettings["IDChiNhanh"].ToString();
            string Path = ConfigurationManager.AppSettings["Path"].ToString();
            string Password = ConfigurationManager.AppSettings["Password"].ToString();
            string connectionString = ConfigurationManager.AppSettings["connectionString"].ToString();

            string maxIDMinhNgoc = "";
            string mdfFile = @"SimViewHD.mdb";
            try
            {
                var maxid = (from p in db.tblDuLieuTramTrons
                             where p.IDChiNhanh == new Guid(IDChiNhanh)
                             orderby p.ID descending
                             select new { p.ID }).Skip(1).FirstOrDefault();
                if (maxid != null && maxid.ID != null)
                {
                    maxIDMinhNgoc = maxid.ID.ToString();
                }

                DataTable table = new DataTable();
                DataTable tableupdate = new DataTable();
                if (maxIDMinhNgoc == "")
                    maxIDMinhNgoc = "0";

                using (OleDbConnection connection = new OleDbConnection(string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + "", mdfFile)))
                {
                    using (OleDbCommand selectCommand = new OleDbCommand("SELECT top 200 * FROM OrderDataUpDate where ID > " + maxIDMinhNgoc + " order by ID desc", connection))
                    {
                        connection.Open();

                        OleDbDataAdapter adapter = new OleDbDataAdapter();
                        adapter.SelectCommand = selectCommand;
                        adapter.Fill(table);

                    }
                    using (OleDbCommand selectCommand = new OleDbCommand("SELECT top 50 * FROM OrderDataUpDate where ID <= " + maxIDMinhNgoc + " order by ID desc", connection))
                    {
                        OleDbDataAdapter adapter = new OleDbDataAdapter();
                        adapter.SelectCommand = selectCommand;
                        adapter.Fill(tableupdate);

                        connection.Dispose();
                        connection.Close();
                    }
                }

                string saveStaff = "";
                if (table.Rows.Count > 0)
                {
                    List<tblDuLieuTramTron> them = new List<tblDuLieuTramTron>();

                    foreach (DataRow row in table.Rows)
                    {
                        string iD = row["ID"].ToString();
                        string datetime = row["DateTime"].ToString();
                        string orderID = row["OrderID"].ToString();
                        string docketNumber = row["DocketNumber"].ToString();
                        string m3Order = row["m3Order"].ToString();
                        string numberofBatch = row["NumberofBatch"].ToString();
                        string m3pBatch = row["m3pBatch"].ToString();
                        string m3Total = row["m3Total"].ToString();
                        string m3Progress = row["m3Progress"].ToString();
                        string _operator = row["Operator"].ToString();
                        string mode = row["Mode"].ToString();
                        string customerName = row["CustomerName"].ToString();
                        string address = row["Address"].ToString();
                        string tell = row["Tell"].ToString();
                        string deliveryAddress = row["DeliveryAddress"].ToString();
                        string driver = row["Driver"].ToString();
                        string truck = row["Truck"].ToString();
                        string mixCode = row["MixCode"].ToString();
                        string mAgg1 = row["MAgg1"].ToString();
                        string mAgg2 = row["MAgg2"].ToString();
                        string mAgg3 = row["MAgg3"].ToString();
                        string mAgg4 = row["MAgg4"].ToString();
                        string mAgg5 = row["MAgg5"].ToString();
                        string mCem1 = row["MCem1"].ToString();
                        string mCem2 = row["MCem2"].ToString();
                        string mCem3 = row["MCem3"].ToString();
                        string mCem4 = row["MCem4"].ToString();
                        string mWater = row["MWater"].ToString();
                        string mAdd = row["MAdd"].ToString();
                        string mAdd2 = row["MAdd2"].ToString();
                        string mMixTime = row["MMixTime"].ToString();
                        string mHalfOpenTime = row["MHalfOpenTime"].ToString();
                        string mFullOpenTime = row["MFullOpenTime"].ToString();
                        string batchNo = row["BatchNo"].ToString();
                        string agg1 = row["Agg1"].ToString();
                        string agg2 = row["Agg2"].ToString();
                        string agg3 = row["Agg3"].ToString();
                        string agg4 = row["Agg4"].ToString();
                        string agg5 = row["Agg5"].ToString();
                        string cem1 = row["Cem1"].ToString();
                        string cem2 = row["Cem2"].ToString();
                        string cem3 = row["Cem3"].ToString();
                        string cem4 = row["Cem4"].ToString();
                        string water = row["Water"].ToString();
                        string add = row["Add"].ToString();
                        string add2 = row["Add2"].ToString();

                        var themoi = new tblDuLieuTramTron()
                        {
                            IDMN = Guid.NewGuid(),
                            IDChiNhanh = new Guid(IDChiNhanh),
                            ID = int.Parse(iD),
                            NgayThang = DateTime.Parse(datetime),
                            OrderID = orderID == "" ? (double?)null : double.Parse(orderID),
                            DocketNumber = docketNumber == "" ? (double?)null : double.Parse(docketNumber),
                            m3Order = m3Order == "" ? (double?)null : double.Parse(m3Order),
                            NumberofBatch = numberofBatch == "" ? (double?)null : double.Parse(numberofBatch),
                            m3pBatch = m3pBatch == "" ? (double?)null : double.Parse(m3pBatch),
                            m3Total = m3Total == "" ? (double?)null : double.Parse(m3Total),
                            m3Progress = m3Progress == "" ? (double?)null : double.Parse(m3Progress),
                            Operator = _operator,
                            Mode = mode,
                            CustomerName = customerName,
                            Address = address,
                            Tell = tell,
                            DeliveryAddress = deliveryAddress,
                            Driver = driver,
                            Truck = truck,
                            MixCode = mixCode,
                            MAgg1 = mAgg1 == "" ? (double?)null : double.Parse(mAgg1),
                            MAgg2 = mAgg2 == "" ? (double?)null : double.Parse(mAgg2),
                            MAgg3 = mAgg3 == "" ? (double?)null : double.Parse(mAgg3),
                            MAgg4 = mAgg4 == "" ? (double?)null : double.Parse(mAgg4),
                            MAgg5 = mAgg5 == "" ? (double?)null : double.Parse(mAgg5),
                            MCem1 = mCem1 == "" ? (double?)null : double.Parse(mCem1),
                            MCem2 = mCem2 == "" ? (double?)null : double.Parse(mCem2),
                            MCem3 = mCem3 == "" ? (double?)null : double.Parse(mCem3),
                            MCem4 = mCem4 == "" ? (double?)null : double.Parse(mCem4),
                            MWater = mWater == "" ? (double?)null : double.Parse(mWater),
                            MAdd = mAdd == "" ? (double?)null : double.Parse(mAdd),
                            MAdd2 = mAdd2 == "" ? (double?)null : double.Parse(mAdd2),
                            MMixTime = mMixTime == "" ? (double?)null : double.Parse(mMixTime),
                            MHalfOpenTime = mHalfOpenTime == "" ? (double?)null : double.Parse(mHalfOpenTime),
                            MFullOpenTime = mFullOpenTime == "" ? (double?)null : double.Parse(mFullOpenTime),
                            BatchNo = batchNo == "" ? (double?)null : double.Parse(batchNo),
                            Agg1 = agg1 == "" ? (double?)null : double.Parse(agg1),
                            Agg2 = agg2 == "" ? (double?)null : double.Parse(agg2),
                            Agg3 = agg3 == "" ? (double?)null : double.Parse(agg3),
                            Agg4 = agg4 == "" ? (double?)null : double.Parse(agg4),
                            Agg5 = agg5 == "" ? (double?)null : double.Parse(agg5),
                            Cem1 = cem1 == "" ? (double?)null : double.Parse(cem1),
                            Cem2 = cem2 == "" ? (double?)null : double.Parse(cem2),
                            Cem3 = cem3 == "" ? (double?)null : double.Parse(cem3),
                            Cem4 = cem4 == "" ? (double?)null : double.Parse(cem4),
                            Water = water == "" ? (double?)null : double.Parse(water),
                            Add1 = add == "" ? (double?)null : double.Parse(add),
                            Add2 = add2 == "" ? (double?)null : double.Parse(add2)
                        };
                        them.Add(themoi);
                    }
                    if (them.Count > 0)
                    {
                        db.tblDuLieuTramTrons.InsertAllOnSubmit(them);
                        db.SubmitChanges();
                    }
                }

                if (tableupdate.Rows.Count > 0)
                {
                    foreach (DataRow row in tableupdate.Rows)
                    {
                        string iD = row["ID"].ToString();
                        string dateTime = row["DateTime"].ToString();
                        string orderID = row["OrderID"].ToString();
                        string docketNumber = row["DocketNumber"].ToString();
                        string m3Order = row["m3Order"].ToString();
                        string numberofBatch = row["NumberofBatch"].ToString();
                        string m3pBatch = row["m3pBatch"].ToString();
                        string m3Total = row["m3Total"].ToString();
                        string m3Progress = row["m3Progress"].ToString();
                        string _operator = row["Operator"].ToString();
                        string mode = row["Mode"].ToString();
                        string customerName = row["CustomerName"].ToString();
                        string address = row["Address"].ToString();
                        string tell = row["Tell"].ToString();
                        string deliveryAddress = row["DeliveryAddress"].ToString();
                        string driver = row["Driver"].ToString();
                        string truck = row["Truck"].ToString();
                        string mixCode = row["MixCode"].ToString();
                        string mAgg1 = row["MAgg1"].ToString();
                        string mAgg2 = row["MAgg2"].ToString();
                        string mAgg3 = row["MAgg3"].ToString();
                        string mAgg4 = row["MAgg4"].ToString();
                        string mAgg5 = row["MAgg5"].ToString();
                        string mCem1 = row["MCem1"].ToString();
                        string mCem2 = row["MCem2"].ToString();
                        string mCem3 = row["MCem3"].ToString();
                        string mCem4 = row["MCem4"].ToString();
                        string mWater = row["MWater"].ToString();
                        string mAdd = row["MAdd"].ToString();
                        string mAdd2 = row["MAdd2"].ToString();
                        string mMixTime = row["MMixTime"].ToString();
                        string mHalfOpenTime = row["MHalfOpenTime"].ToString();
                        string mFullOpenTime = row["MFullOpenTime"].ToString();
                        string batchNo = row["BatchNo"].ToString();
                        string agg1 = row["Agg1"].ToString();
                        string agg2 = row["Agg2"].ToString();
                        string agg3 = row["Agg3"].ToString();
                        string agg4 = row["Agg4"].ToString();
                        string agg5 = row["Agg5"].ToString();
                        string cem1 = row["Cem1"].ToString();
                        string cem2 = row["Cem2"].ToString();
                        string cem3 = row["Cem3"].ToString();
                        string cem4 = row["Cem4"].ToString();
                        string water = row["Water"].ToString();
                        string add = row["Add"].ToString();
                        string add2 = row["Add2"].ToString();

                        var query = (from p in db.tblDuLieuTramTrons
                                     where p.ID == long.Parse(iD)
                                     && p.IDChiNhanh == new Guid(IDChiNhanh)
                                     select p).FirstOrDefault();
                        if (query != null && query.IDMN != null)
                        {
                            query.NgayThang = DateTime.Parse(dateTime);
                            query.OrderID = orderID == "" ? (double?)null : double.Parse(orderID);
                            query.DocketNumber = docketNumber == "" ? (double?)null : double.Parse(docketNumber);
                            query.m3Order = m3Order == "" ? (double?)null : double.Parse(m3Order);
                            query.NumberofBatch = numberofBatch == "" ? (double?)null : double.Parse(numberofBatch);
                            query.m3pBatch = m3pBatch == "" ? (double?)null : double.Parse(m3pBatch);
                            query.m3Total = m3Total == "" ? (double?)null : double.Parse(m3Total);
                            query.m3Progress = m3Progress == "" ? (double?)null : double.Parse(m3Progress);
                            query.Operator = _operator;
                            query.Mode = mode;
                            query.CustomerName = customerName;
                            query.Address = address;
                            query.Tell = tell;
                            query.DeliveryAddress = deliveryAddress;
                            query.Driver = driver;
                            query.Truck = truck;
                            query.MixCode = mixCode;
                            query.MAgg1 = mAgg1 == "" ? (double?)null : double.Parse(mAgg1);
                            query.MAgg2 = mAgg2 == "" ? (double?)null : double.Parse(mAgg2);
                            query.MAgg3 = mAgg3 == "" ? (double?)null : double.Parse(mAgg3);
                            query.MAgg4 = mAgg4 == "" ? (double?)null : double.Parse(mAgg4);
                            query.MAgg5 = mAgg5 == "" ? (double?)null : double.Parse(mAgg5);
                            query.MCem1 = mCem1 == "" ? (double?)null : double.Parse(mCem1);
                            query.MCem2 = mCem2 == "" ? (double?)null : double.Parse(mCem2);
                            query.MCem3 = mCem3 == "" ? (double?)null : double.Parse(mCem3);
                            query.MCem4 = mCem4 == "" ? (double?)null : double.Parse(mCem4);
                            query.MWater = mWater == "" ? (double?)null : double.Parse(mWater);
                            query.MAdd = mAdd == "" ? (double?)null : double.Parse(mAdd);
                            query.MAdd2 = mAdd2 == "" ? (double?)null : double.Parse(mAdd2);
                            query.MMixTime = mMixTime == "" ? (double?)null : double.Parse(mMixTime);
                            query.MHalfOpenTime = mHalfOpenTime == "" ? (double?)null : double.Parse(mHalfOpenTime);
                            query.MFullOpenTime = mFullOpenTime == "" ? (double?)null : double.Parse(mFullOpenTime);
                            query.BatchNo = batchNo == "" ? (double?)null : double.Parse(batchNo);
                            query.Agg1 = agg1 == "" ? (double?)null : double.Parse(agg1);
                            query.Agg2 = agg2 == "" ? (double?)null : double.Parse(agg2);
                            query.Agg3 = agg3 == "" ? (double?)null : double.Parse(agg3);
                            query.Agg4 = agg4 == "" ? (double?)null : double.Parse(agg4);
                            query.Agg5 = agg5 == "" ? (double?)null : double.Parse(agg5);
                            query.Cem1 = cem1 == "" ? (double?)null : double.Parse(cem1);
                            query.Cem2 = cem2 == "" ? (double?)null : double.Parse(cem2);
                            query.Cem3 = cem3 == "" ? (double?)null : double.Parse(cem3);
                            query.Cem4 = cem4 == "" ? (double?)null : double.Parse(cem4);
                            query.Water = water == "" ? (double?)null : double.Parse(water);
                            query.Add1 = add == "" ? (double?)null : double.Parse(add);
                            query.Add2 = add2 == "" ? (double?)null : double.Parse(add2);
                            db.SubmitChanges();
                        }
                    }
                    db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        protected override void OnStop()
        {

        }
    }
}
