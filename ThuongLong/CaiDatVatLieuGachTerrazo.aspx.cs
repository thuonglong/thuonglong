﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class CaiDatVatLieuGachTerrazo : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmDinhMucGach";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmCaiDatVatLieuGachTerrazo", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadLoaiVatLieu();
                        LoadLoaiVatLieuSearch();
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadLoaiVatLieu()
        {
            var qlLoaiSanPham = (from p in db.tblLoaiVatLieus
                                 where p.TrangThai == 2
                                 && p.IDNhomVatLieu == new Guid("7D25A3D1-BE90-4F1D-A0CB-8A518AC09B55")
                                 select new
                                 {
                                     p.ID,
                                     Ten = p.TenLoaiVatLieu
                                 }).OrderBy(p => p.Ten);

            dlLoaiSanPham.DataSource = qlLoaiSanPham;
            dlLoaiSanPham.DataBind();
            dlLoaiSanPham.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlMauXi = (from p in db.tblLoaiVatLieus
                           where p.TrangThai == 2
                           && p.IDNhomVatLieu == new Guid("6D66F6C2-E6E9-4116-9BAF-1C69CE0F845B")
                           select new
                           {
                               p.ID,
                               Ten = p.TenLoaiVatLieu
                           }).OrderBy(p => p.Ten);

            dlMauXi.DataSource = qlMauXi;
            dlMauXi.DataBind();
            dlMauXi.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlMauDo = (from p in db.tblLoaiVatLieus
                           where p.TrangThai == 2
                           && p.IDNhomVatLieu == new Guid("6D66F6C2-E6E9-4116-9BAF-1C69CE0F845B")
                           select new
                           {
                               p.ID,
                               Ten = p.TenLoaiVatLieu
                           }).OrderBy(p => p.Ten);

            dlMauDo.DataSource = qlMauDo;
            dlMauDo.DataBind();
            dlMauDo.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlBotDa = (from p in db.tblLoaiVatLieus
                           where p.TrangThai == 2
                           && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                           select new
                           {
                               p.ID,
                               Ten = p.TenLoaiVatLieu
                           }).OrderBy(p => p.Ten);

            dlBotDa.DataSource = qlBotDa;
            dlBotDa.DataBind();
            dlBotDa.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlDaDen2Ly = (from p in db.tblLoaiVatLieus
                              where p.TrangThai == 2
                              && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                              select new
                              {
                                  p.ID,
                                  Ten = p.TenLoaiVatLieu
                              }).OrderBy(p => p.Ten);

            dlDaDen2Ly.DataSource = qlDaDen2Ly;
            dlDaDen2Ly.DataBind();
            dlDaDen2Ly.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlDaTrang2Ly = (from p in db.tblLoaiVatLieus
                                where p.TrangThai == 2
                                && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                                select new
                                {
                                    p.ID,
                                    Ten = p.TenLoaiVatLieu
                                }).OrderBy(p => p.Ten);

            dlDaTrang2Ly.DataSource = qlDaTrang2Ly;
            dlDaTrang2Ly.DataBind();
            dlDaTrang2Ly.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlDaTrang4Ly = (from p in db.tblLoaiVatLieus
                                where p.TrangThai == 2
                                && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                                select new
                                {
                                    p.ID,
                                    Ten = p.TenLoaiVatLieu
                                }).OrderBy(p => p.Ten);

            dlDaTrang4Ly.DataSource = qlDaTrang4Ly;
            dlDaTrang4Ly.DataBind();
            dlDaTrang4Ly.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlXiMangPCB401 = (from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                                  && p.IDNhomVatLieu == new Guid("36FCDA11-FEF0-43C5-8AB8-C55F24281FF4")
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu
                                  }).OrderBy(p => p.Ten);

            dlXiMangPCB401.DataSource = qlXiMangPCB401;
            dlXiMangPCB401.DataBind();
            dlXiMangPCB401.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlXiMangPCB402 = (from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                                  && p.IDNhomVatLieu == new Guid("36FCDA11-FEF0-43C5-8AB8-C55F24281FF4")
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu
                                  }).OrderBy(p => p.Ten);

            dlXiMangPCB402.DataSource = qlXiMangPCB402;
            dlXiMangPCB402.DataBind();
            dlXiMangPCB402.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlMatDa = (from p in db.tblLoaiVatLieus
                           where p.TrangThai == 2
                           && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                           select new
                           {
                               p.ID,
                               Ten = p.TenLoaiVatLieu
                           }).OrderBy(p => p.Ten);

            dlMatDa.DataSource = qlMatDa;
            dlMatDa.DataBind();
            dlMatDa.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlCatSongDa = (from p in db.tblLoaiVatLieus
                               where p.TrangThai == 2
                               && p.IDNhomVatLieu == new Guid("8B27D124-A687-400E-89F6-E3114A08CF87")
                               select new
                               {
                                   p.ID,
                                   Ten = p.TenLoaiVatLieu
                               }).OrderBy(p => p.Ten);

            dlCatSongDa.DataSource = qlCatSongDa;
            dlCatSongDa.DataBind();
            dlCatSongDa.Items.Insert(0, new ListItem("--Chọn--", ""));

            //dlMauXi.Items.Insert(0, new ListItem("Màu xi", "E833B04B-83ED-41AF-8842-8B32557D611F"));
            //dlMauDo.Items.Insert(0, new ListItem("Màu đỏ", "8FF99370-A3BA-48CD-B887-143443CCE958"));
            //dlBotDa.Items.Insert(0, new ListItem("Bột đá", "DEC44856-4591-4803-9246-BCC8E5BDFD66"));
            //dlDaDen2Ly.Items.Insert(0, new ListItem("Đá đen 2 ly", "B0237ABD-67B2-43A0-A5D5-9AF3D6C427B3"));
            //dlDaTrang2Ly.Items.Insert(0, new ListItem("Đá trắng 2 ly", "074FD2D6-843E-4DE6-8F76-ECFD984E9CD8"));
            //dlDaTrang4Ly.Items.Insert(0, new ListItem("Đá trắng 4 ly", "08CCDE76-34CE-4033-A8B5-797703AC377A"));
            //dlXiMangPCB401.Items.Insert(0, new ListItem("Xi măng PCB 40", "840482F9-D5F6-4926-B6F1-77657A7A95D6"));
            //dlXiMangPCB402.Items.Insert(0, new ListItem("Xi măng PCB 40", "840482F9-D5F6-4926-B6F1-77657A7A95D6"));
            //dlMatDa.Items.Insert(0, new ListItem("Mạt đá", "7C716F3F-6262-4621-8452-9F8F43D02411"));
            //dlCatSongDa.Items.Insert(0, new ListItem("Cát sông đà", "51C056B4-DB1F-4024-817F-6E08F9EBF1F0"));
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlLoaiSanPham.SelectedValue == "")
            {
                s += " - Chọn loại sản phẩm<br />";
            }
            if (dlMauXi.SelectedValue == "")
            {
                s += " - Chọn màu xi<br />";
            }
            if (dlMauDo.SelectedValue == "")
            {
                s += " - Chọn màu đỏ<br />";
            }
            if (dlBotDa.SelectedValue == "")
            {
                s += " - Chọn bột đá<br />";
            }
            if (dlDaDen2Ly.SelectedValue == "")
            {
                s += " - Chọn đá đen 2 ly<br />";
            }
            if (dlDaTrang2Ly.SelectedValue == "")
            {
                s += " - Chọn đá trắng 2 ly<br />";
            }
            if (dlDaTrang4Ly.SelectedValue == "")
            {
                s += " - Chọn đá trắng 4 ly<br />";
            }
            if (dlXiMangPCB401.SelectedValue == "")
            {
                s += " - Chọn xi măng PCB40 1<br />";
            }
            if (dlXiMangPCB402.SelectedValue == "")
            {
                s += " - Chọn xi măng PCB40 2<br />";
            }
            if (dlMatDa.SelectedValue == "")
            {
                s += " - Chọn mạt đá<br />";
            }
            if (dlCatSongDa.SelectedValue == "")
            {
                s += " - Chọn cát sông đà<br />";
            }

            if (txtMauXi.Text == "")
            {
                s += " - Nhập định mức màu xi<br />";
            }

            if (txtMauDo.Text == "")
            {
                s += " - Nhập định mức màu đỏ<br />";
            }

            if (txtBotDa.Text == "")
            {
                s += " - Nhập định mức bột đá<br />";
            }

            if (txtDaDen2Ly.Text == "")
            {
                s += " - Nhập định mức đá đen 2 ly<br />";
            }

            if (txtDaTrang2Ly.Text == "")
            {
                s += " - Nhập định mức đá trắng 2 ly<br />";
            }

            if (txtDaTrang4Ly.Text == "")
            {
                s += " - Nhập định mức đá trắng 4 ly<br />";
            }

            if (txtXiMangPCB401.Text == "")
            {
                s += " - Nhập định mức xi măng PCB40 1<br />";
            }

            if (txtXiMangPCB402.Text == "")
            {
                s += " - Nhập định mức xi măng PCB40 2<br />";
            }

            if (txtMatDa.Text == "")
            {
                s += " - Nhập định mức mạt đá<br />";
            }

            if (txtCatSongDa.Text == "")
            {
                s += " - Nhập định mức cát sông đà<br />";
            }
            if (hdID.Value == "")
            {
                var query = (from p in db.tblCaiDatGachMenBongs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    s += "Đã thiết lập định mức cho loại gạch này";
                }
            }
            else
            {
                var query = (from p in db.tblCaiDatGachMenBongs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             && p.ID != new Guid(hdID.Value)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    s += "Đã thiết lập định mức cho loại gạch này";
                }
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            if (hdID.Value == "")
            {
                string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    if (CheckThem() == "")
                    {
                        var CaiDatVatLieuGachTerrazo = new tblCaiDatVatLieuGachTerrazo()
                        {
                            ID = Guid.NewGuid(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDLoaiVatLieu = GetDrop(dlLoaiSanPham),
                            ThoiTiet = int.Parse(dlThoiTiet.SelectedValue),
                            MauXi = GetDrop(dlMauXi),
                            MauDo = GetDrop(dlMauDo),
                            BotDa = GetDrop(dlBotDa),
                            DaDen2ly = GetDrop(dlDaDen2Ly),
                            DaTrang2Ly = GetDrop(dlDaTrang2Ly),
                            DaTrang4Ly = GetDrop(dlDaTrang4Ly),
                            XiMangPCB401 = GetDrop(dlXiMangPCB401),
                            XiMangPCB402 = GetDrop(dlXiMangPCB402),
                            MatDa = GetDrop(dlMatDa),
                            CatSongDa = GetDrop(dlCatSongDa),

                            DMMauXi = double.Parse(txtMauXi.Text.Trim()),
                            DMMauDo = double.Parse(txtMauDo.Text.Trim()),
                            DMBotDa = double.Parse(txtBotDa.Text.Trim()),
                            DMDaDen2ly = double.Parse(txtDaDen2Ly.Text.Trim()),
                            DMDaTrang2Ly = double.Parse(txtDaTrang2Ly.Text.Trim()),
                            DMDaTrang4Ly = double.Parse(txtDaTrang4Ly.Text.Trim()),
                            DMXiMangPCB401 = double.Parse(txtXiMangPCB401.Text.Trim()),
                            DMXiMangPCB402 = double.Parse(txtXiMangPCB402.Text.Trim()),
                            DMMatDa = double.Parse(txtMatDa.Text.Trim()),
                            DMCatSongDa = double.Parse(txtCatSongDa.Text.Trim()),

                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblCaiDatVatLieuGachTerrazos.InsertOnSubmit(CaiDatVatLieuGachTerrazo);
                        db.SubmitChanges();
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                var query = (from p in db.tblCaiDatVatLieuGachTerrazos
                             where p.ID == new Guid(hdID.Value)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckThem() == "")
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDLoaiVatLieu = GetDrop(dlLoaiSanPham);
                            query.ThoiTiet = int.Parse(dlThoiTiet.SelectedValue);
                            query.MauXi = GetDrop(dlMauXi);
                            query.MauDo = GetDrop(dlMauDo);
                            query.BotDa = GetDrop(dlBotDa);
                            query.DaDen2ly = GetDrop(dlDaDen2Ly);
                            query.DaTrang2Ly = GetDrop(dlDaTrang2Ly);
                            query.DaTrang4Ly = GetDrop(dlDaTrang4Ly);
                            query.XiMangPCB401 = GetDrop(dlXiMangPCB401);
                            query.XiMangPCB402 = GetDrop(dlXiMangPCB402);
                            query.MatDa = GetDrop(dlMatDa);
                            query.CatSongDa = GetDrop(dlCatSongDa);

                            query.DMMauXi = double.Parse(txtMauXi.Text.Trim());
                            query.DMMauDo = double.Parse(txtMauDo.Text.Trim());
                            query.DMBotDa = double.Parse(txtBotDa.Text.Trim());
                            query.DMDaDen2ly = double.Parse(txtDaDen2Ly.Text.Trim());
                            query.DMDaTrang2Ly = double.Parse(txtDaTrang2Ly.Text.Trim());
                            query.DMDaTrang4Ly = double.Parse(txtDaTrang4Ly.Text.Trim());
                            query.DMXiMangPCB401 = double.Parse(txtXiMangPCB401.Text.Trim());
                            query.DMXiMangPCB402 = double.Parse(txtXiMangPCB402.Text.Trim());
                            query.DMMatDa = double.Parse(txtMatDa.Text.Trim());
                            query.DMCatSongDa = double.Parse(txtCatSongDa.Text.Trim());
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";

                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);

                            Success("Sửa thành công");
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlMauXi.SelectedValue = "";
            dlMauDo.SelectedValue = "";
            dlBotDa.SelectedValue = "";
            dlDaDen2Ly.SelectedValue = "";
            dlDaTrang2Ly.SelectedValue = "";
            dlDaTrang4Ly.SelectedValue = "";
            dlXiMangPCB401.SelectedValue = "";
            dlXiMangPCB402.SelectedValue = "";
            dlMatDa.SelectedValue = "";
            dlCatSongDa.SelectedValue = "";

            txtMauXi.Text = "";
            txtMauDo.Text = "";
            txtBotDa.Text = "";
            txtDaDen2Ly.Text = "";
            txtDaTrang2Ly.Text = "";
            txtDaTrang4Ly.Text = "";
            txtXiMangPCB401.Text = "";
            txtXiMangPCB402.Text = "";
            txtMatDa.Text = "";
            txtCatSongDa.Text = "";
            hdID.Value = "";
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }

        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlChiNhanh.SelectedValue != "" && dlLoaiSanPham.SelectedValue != "")
            {
                var query = (from p in db.tblCaiDatVatLieuGachTerrazos
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    dlMauXi.SelectedValue = query.MauXi.ToString();
                    dlMauDo.SelectedValue = query.MauDo.ToString();
                    dlBotDa.SelectedValue = query.BotDa.ToString();
                    dlDaDen2Ly.SelectedValue = query.DaDen2ly.ToString();
                    dlDaTrang2Ly.SelectedValue = query.DaTrang2Ly.ToString();
                    dlDaTrang4Ly.SelectedValue = query.DaTrang4Ly.ToString();
                    dlXiMangPCB401.SelectedValue = query.XiMangPCB401.ToString();
                    dlXiMangPCB402.SelectedValue = query.XiMangPCB402.ToString();
                    dlMatDa.SelectedValue = query.MatDa.ToString();
                    dlCatSongDa.SelectedValue = query.CatSongDa.ToString();

                    txtMauXi.Text = query.DMMauXi.ToString();
                    txtMauDo.Text = query.DMMauDo.ToString();
                    txtBotDa.Text = query.DMBotDa.ToString();
                    txtDaDen2Ly.Text = query.DMDaDen2ly.ToString();
                    txtDaTrang2Ly.Text = query.DMDaTrang2Ly.ToString();
                    txtDaTrang4Ly.Text = query.DMDaTrang4Ly.ToString();
                    txtXiMangPCB401.Text = query.DMXiMangPCB401.ToString();
                    txtXiMangPCB402.Text = query.DMXiMangPCB402.ToString();
                    txtMatDa.Text = query.DMMatDa.ToString();
                    txtCatSongDa.Text = query.DMCatSongDa.ToString();
                }
                else
                    lblTaoMoiHopDong_Click(sender, e);
            }
            else
                lblTaoMoiHopDong_Click(sender, e);
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblCaiDatVatLieuGachTerrazos
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlThoiTiet.SelectedValue = query.ThoiTiet.ToString();
                        dlLoaiSanPham.SelectedValue = query.IDLoaiVatLieu.ToString();
                        dlMauXi.SelectedValue = query.MauXi.ToString();
                        dlMauDo.SelectedValue = query.MauDo.ToString();
                        dlBotDa.SelectedValue = query.BotDa.ToString();
                        dlDaDen2Ly.SelectedValue = query.DaDen2ly.ToString();
                        dlDaTrang2Ly.SelectedValue = query.DaTrang2Ly.ToString();
                        dlDaTrang4Ly.SelectedValue = query.DaTrang4Ly.ToString();
                        dlXiMangPCB401.SelectedValue = query.XiMangPCB401.ToString();
                        dlXiMangPCB402.SelectedValue = query.XiMangPCB402.ToString();
                        dlMatDa.SelectedValue = query.MatDa.ToString();
                        dlCatSongDa.SelectedValue = query.CatSongDa.ToString();

                        txtMauXi.Text = query.DMMauXi.ToString();
                        txtMauDo.Text = query.DMMauDo.ToString();
                        txtBotDa.Text = query.DMBotDa.ToString();
                        txtDaDen2Ly.Text = query.DMDaDen2ly.ToString();
                        txtDaTrang2Ly.Text = query.DMDaTrang2Ly.ToString();
                        txtDaTrang4Ly.Text = query.DMDaTrang4Ly.ToString();
                        txtXiMangPCB401.Text = query.DMXiMangPCB401.ToString();
                        txtXiMangPCB402.Text = query.DMXiMangPCB402.ToString();
                        txtMatDa.Text = query.DMMatDa.ToString();
                        txtCatSongDa.Text = query.DMCatSongDa.ToString();

                        hdID.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai == 1)
                        {
                            int countxoa = (from p in db.tblCaiDatVatLieuGachTerrazo_Logs
                                            where p.IDChung == query.ID
                                            select p).Count();
                            if (countxoa == 1)
                            {
                                db.tblCaiDatVatLieuGachTerrazos.DeleteOnSubmit(query);
                                db.SubmitChanges();
                                Success("Đã xóa");
                            }
                            else
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                        }
                        else if (query.TrangThai == 2)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                            Success("Xóa thành công");
                        }
                        else
                        {
                            Warning("Dữ liệu đang chờ duyệt xóa");
                        }
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_CaiDatVatLieuGachTerrazo_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin định mức đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = vatlieu.sp_CaiDatVatLieuGachTerrazo_Search(GetDrop(dlChiNhanhSearch), dlLoaiVatLieuSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlLoaiVatLieuSearch), 20, page);

            GV.DataSource = query;
            GV.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiVatLieuSearch();
        }
        void LoadLoaiVatLieuSearch()
        {
            dlLoaiVatLieuSearch.Items.Clear();
            var query = (from p in db.tblCaiDatVatLieuGachTerrazos
                         join q in db.tblLoaiVatLieus on p.IDLoaiVatLieu equals q.ID
                         where p.IDChiNhanh == GetDrop(dlChiNhanhSearch)
                         select new
                         {
                             ID = p.IDLoaiVatLieu,
                             Ten = q.TenLoaiVatLieu
                         }).Distinct().OrderBy(p => p.Ten);
            dlLoaiVatLieuSearch.DataSource = query;
            dlLoaiVatLieuSearch.DataBind();
            dlLoaiVatLieuSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlThoiTiet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlLoaiSanPham.SelectedValue != "" && dlChiNhanh.SelectedValue != "")
            {
                dlChiNhanhSearch_SelectedIndexChanged(sender, e);
            }
        }
    }
}