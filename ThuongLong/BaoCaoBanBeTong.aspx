﻿<%@ Page Title="Báo cáo doanh thu bán bê tông" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BaoCaoBanBeTong.aspx.cs" Inherits="ThuongLong.BaoCaoBanBeTong" %>

<%@ Register Src="UCBaoCaoBeTong/uc_BaoCaoDoanhThuBanBeTong.ascx" TagName="uc_BaoCaoDoanhThuBanBeTong" TagPrefix="uc1" %>
<%@ Register Src="UCBaoCaoBeTong/uc_BaoCaoDoanhThuBanBeTongNVKD.ascx" TagName="uc_BaoCaoDoanhThuBanBeTongNVKD" TagPrefix="uc2" %>
<%@ Register Src="UCBaoCaoBeTong/uc_BaoCaoDoanhThuBanBeTongMac.ascx" TagName="uc_BaoCaoDoanhThuBanBeTongMac" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmBaoCaoDoanhThuBanBeTong" runat="server" HeaderText="Khách hàng">
                            <ContentTemplate>
                                <uc1:uc_BaoCaoDoanhThuBanBeTong ID="uc_BaoCaoDoanhThuBanBeTong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Nhân viên kinh doanh">
                            <ContentTemplate>
                                <uc2:uc_BaoCaoDoanhThuBanBeTongNVKD ID="uc_BaoCaoDoanhThuBanBeTongNVKD1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Mác bê tông">
                            <ContentTemplate>
                                <uc3:uc_BaoCaoDoanhThuBanBeTongMac ID="uc_BaoCaoDoanhThuBanBeTongMac1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
