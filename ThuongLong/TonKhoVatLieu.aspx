﻿<%@ Page Title="Tồn kho vật liệu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TonKhoVatLieu.aspx.cs" Inherits="ThuongLong.TonKhoVatLieu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách tồn kho vật liệu</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-3">
                                <asp:Label ID="Label2" runat="server" Text="Tháng"></asp:Label><br />
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThang" OnSelectedIndexChanged="dlThang_SelectedIndexChanged" AutoPostBack="true"
                                    runat="server">
                                    <asp:ListItem Value="1">Tháng 1</asp:ListItem>
                                    <asp:ListItem Value="2">Tháng 2</asp:ListItem>
                                    <asp:ListItem Value="3">Tháng 3</asp:ListItem>
                                    <asp:ListItem Value="4">Tháng 4</asp:ListItem>
                                    <asp:ListItem Value="5">Tháng 5</asp:ListItem>
                                    <asp:ListItem Value="6">Tháng 6</asp:ListItem>
                                    <asp:ListItem Value="7">Tháng 7</asp:ListItem>
                                    <asp:ListItem Value="8">Tháng 8</asp:ListItem>
                                    <asp:ListItem Value="9">Tháng 9</asp:ListItem>
                                    <asp:ListItem Value="10">Tháng 10</asp:ListItem>
                                    <asp:ListItem Value="11">Tháng 11</asp:ListItem>
                                    <asp:ListItem Value="12">Tháng 12</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3">
                                <asp:Label ID="Label3" runat="server" Text="Năm"></asp:Label><br />
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNam" OnSelectedIndexChanged="dlThang_SelectedIndexChanged" AutoPostBack="true"
                                    runat="server">
                                    <asp:ListItem>2020</asp:ListItem>
                                    <asp:ListItem>2021</asp:ListItem>
                                    <asp:ListItem>2022</asp:ListItem>
                                    <asp:ListItem>2023</asp:ListItem>
                                    <asp:ListItem>2024</asp:ListItem>
                                    <asp:ListItem>2025</asp:ListItem>
                                    <asp:ListItem>2026</asp:ListItem>
                                    <asp:ListItem>2027</asp:ListItem>
                                    <asp:ListItem>2028</asp:ListItem>
                                    <asp:ListItem>2029</asp:ListItem>
                                    <asp:ListItem>2030</asp:ListItem>
                                    <asp:ListItem>2031</asp:ListItem>
                                    <asp:ListItem>2032</asp:ListItem>
                                    <asp:ListItem>2033</asp:ListItem>
                                    <asp:ListItem>2034</asp:ListItem>
                                    <asp:ListItem>2035</asp:ListItem>
                                    <asp:ListItem>2036</asp:ListItem>
                                    <asp:ListItem>2037</asp:ListItem>
                                    <asp:ListItem>2038</asp:ListItem>
                                    <asp:ListItem>2039</asp:ListItem>
                                    <asp:ListItem>2040</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-3" id="divchinhanh" runat="server">
                                <div class="form-group">
                                    <label>Chi nhánh</label>
                                    <asp:DropDownList ID="dlChiNhanh" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%" OnSelectedIndexChanged="dlThang_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3" id="div1" runat="server">
                                <div class="form-group">
                                    <label>Nhóm vật liệu</label>
                                    <asp:DropDownList ID="dlNhomVatLieu" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group col-lg-6">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-warning" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                                    <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                                    <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                    <asp:BoundField DataField="KLTon" HeaderText="Tồn đầu kỳ" />
                                    <asp:BoundField DataField="KLNhap" HeaderText="Phát sinh tăng" />
                                    <asp:BoundField DataField="KLXuat" HeaderText="Phát sinh giảm" />
                                    <asp:BoundField DataField="KLTonCuoi" HeaderText="Tồn cuối kỳ" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

            <script type="text/javascript"> 
                function clickButton(e, buttonid) {
                    var evt = e ? e : window.event;
                    var bt = document.getElementById(buttonid);

                    if (bt) {
                        if (evt.keyCode == 13) {
                            bt.click();
                            return false;
                        }
                    }
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
