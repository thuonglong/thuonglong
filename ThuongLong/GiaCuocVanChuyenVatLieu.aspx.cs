﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class GiaCuocVanChuyenVatLieu : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmGiaCuocVanChuyenVatLieu";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmGiaCuocVanChuyenVatLieu", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNhaCungCap();
                        LoadNoiNhan();
                        LoadNhomVatLieu();
                        LoadLoaiVatLieu();
                        LoadDonViTinh();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNhaCungCap()
        {
            var query = db.sp_LoadNhaCungCap();
            dlNhaCungCap.DataSource = query;
            dlNhaCungCap.DataBind();
            dlNhaCungCap.Items.Insert(0, new ListItem("--Chọn nhà cung cấp--", ""));
        }
        protected void LoadNoiNhan()
        {
            var query = db.sp_LoadNoiNhan();
            dlNoiNhan.DataSource = query;
            dlNoiNhan.DataBind();
            dlNoiNhan.Items.Insert(0, new ListItem("--Chọn nơi nhận--", ""));
        }
        protected void LoadNhomVatLieu()
        {
            var query = db.sp_LoadNhomVatLieu();
            dlNhomVatLieu.DataSource = query;
            dlNhomVatLieu.DataBind();
            dlNhomVatLieu.Items.Insert(0, new ListItem("--Chọn nhóm vật liệu--", ""));
        }
        protected void LoadLoaiVatLieu()
        {
            dlLoaiVatLieu.Items.Clear();
            var query = db.sp_LoadLoaiVatLieuByNhom(GetDrop(dlNhomVatLieu));
            dlLoaiVatLieu.DataSource = query;
            dlLoaiVatLieu.DataBind();
            dlLoaiVatLieu.Items.Insert(0, new ListItem("--Chọn loại vật liệu--", ""));
        }
        protected void LoadDonViTinh()
        {
            dlDonViTinh.Items.Clear();
            var query = (from p in db.tblDonViTinhs
                             //where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenDonViTinh
                         }).OrderBy(p => p.Ten);
            dlDonViTinh.DataSource = query;
            dlDonViTinh.DataBind();
            dlDonViTinh.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThemHopDong()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (dlNoiNhan.SelectedValue == "")
            {
                s += " - Chọn nơi nhận<br />";
            }
            if (dlNhomVatLieu.SelectedValue == "")
            {
                s += " - Chọn nhóm vật liệu<br />";
            }
            if (dlLoaiVatLieu.SelectedValue == "")
            {
                s += " - Chọn loại vật liệu<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            if (txtDonGiaCoThue.Text == "" || txtDonGiaKhongThue.Text == "" || (decimal.Parse(txtDonGiaCoThue.Text) == 0 && decimal.Parse(txtDonGiaKhongThue.Text) == 0))
            {
                s += " - Nhập giá mua vật liệu<br />";
            }
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                var query = vatlieu.sp_GiaCuocVanChuyenVatLieu_CheckThem(GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), GetDrop(dlNoiNhan), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text) > 0 ? true : false,
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaHopDong()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (dlNoiNhan.SelectedValue == "")
            {
                s += " - Chọn nơi nhận<br />";
            }
            if (dlNhomVatLieu.SelectedValue == "")
            {
                s += " - Chọn nhóm vật liệu<br />";
            }
            if (dlLoaiVatLieu.SelectedValue == "")
            {
                s += " - Chọn loại vật liệu<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            if (txtDonGiaCoThue.Text == "" || txtDonGiaKhongThue.Text == "" || (decimal.Parse(txtDonGiaCoThue.Text) == 0 && decimal.Parse(txtDonGiaKhongThue.Text) == 0))
            {
                s += " - Nhập giá mua vật liệu<br />";
            }
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                var query = vatlieu.sp_GiaCuocVanChuyenVatLieu_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), GetDrop(dlNoiNhan), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text) > 0 ? true : false,
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThemHopDong() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var GiaCuocVanChuyenVatLieu = new tblGiaCuocVanChuyenVatLieu()
                        {
                            ID = Guid.NewGuid(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhaCungCap = GetDrop(dlNhaCungCap),
                            IDNoiNhan = GetDrop(dlNoiNhan),
                            IDNhomVatLieu = GetDrop(dlNhomVatLieu),
                            IDLoaiVatLieu = GetDrop(dlLoaiVatLieu),
                            IDDonViTinh = GetDrop(dlDonViTinh),
                            DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text),
                            DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text),
                            TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                            DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblGiaCuocVanChuyenVatLieus.InsertOnSubmit(GiaCuocVanChuyenVatLieu);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSuaHopDong() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblGiaCuocVanChuyenVatLieus
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                            query.IDNoiNhan = GetDrop(dlNoiNhan);
                            query.IDNhomVatLieu = GetDrop(dlNhomVatLieu);
                            query.IDLoaiVatLieu = GetDrop(dlLoaiVatLieu);
                            query.IDDonViTinh = GetDrop(dlDonViTinh);
                            query.DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text);
                            query.DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text);
                            query.TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text));
                            query.DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text));
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin giá mua vật liệu đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlNhaCungCap.SelectedIndex = 0;
            dlNhomVatLieu.SelectedIndex = 0;
            LoadLoaiVatLieu();
            dlDonViTinh.SelectedValue = "";
            hdID.Value = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblGiaCuocVanChuyenVatLieus
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        dlNoiNhan.SelectedValue = query.IDNoiNhan.ToString();
                        dlNhomVatLieu.SelectedValue = query.IDNhomVatLieu.ToString();
                        LoadLoaiVatLieu();
                        dlLoaiVatLieu.SelectedValue = query.IDLoaiVatLieu.ToString();
                        dlDonViTinh.SelectedValue = query.IDDonViTinh.ToString();
                        txtDonGiaCoThue.Text = string.Format("{0:N0}", query.DonGiaCoThue);
                        txtDonGiaKhongThue.Text = string.Format("{0:N0}", query.DonGiaKhongThue);
                        txtTuNgay.Text = query.TuNgay.ToString("dd/MM/yyyy");
                        txtDenNgay.Text = query.DenNgay == null ? "" : query.DenNgay.Value.ToString("dd/MM/yyyy");
                        hdID.Value = id;
                        //btnSave.Text = "Cập nhật";
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //var checkxoa = from p in db.tblNhaCungCaps
                        //               where p.IDGiaCuocVanChuyenVatLieu == query.ID
                        //               select p;
                        //if (checkxoa.Count() > 0)
                        //{
                        //    Warning("Thông tin nhóm nhà cung cấp đã được sử dụng. Không được xóa.");
                        //}
                        //else
                        //{
                        if (query.TrangThai != 3)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                        }
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_GiaCuocVanChuyenVatLieu_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }

                //else if (e.CommandName == "Kích hoạt")
                //{
                //    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                //    if (thongbao != "")
                //    {
                //        if (url != "")
                //        {
                //            GstGetMess(thongbao, url);
                //        }
                //        else
                //        {
                //            Warning(thongbao);
                //        }
                //    }
                //    else
                //    {
                //        if (query.TrangThai == 1)
                //        {
                //            query.TrangThai = 1;
                //            query.TrangThaiText = "Đã kích hoạt";
                //            db.SubmitChanges();
                //            Success("Đã kích hoạt");
                //        }
                //        else
                //        {
                //            Success("Đã kích hoạt");
                //        }
                //        Search(1);
                //    }
                //}
                //else if (e.CommandName == "Khóa")
                //{
                //    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                //    if (thongbao != "")
                //    {
                //        if (url != "")
                //        {
                //            GstGetMess(thongbao, url);
                //        }
                //        else
                //        {
                //            Warning(thongbao);
                //        }
                //    }
                //    else
                //    {
                //        if (query.TrangThai == 1)
                //        {
                //            query.TrangThai = 1;
                //            query.TrangThaiText = "Đã khóa";
                //            db.SubmitChanges();
                //            Success("Đã khóa");
                //        }
                //        else
                //        {
                //            Success("Đã khóa");
                //        }
                //        Search(1);
                //    }
                //}
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void dlNhomVatLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiVatLieu();
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void LoadDl1()
        {
            List<sp_GiaCuocVanChuyenVatLieu_LoadDl1Result> query = vatlieu.sp_GiaCuocVanChuyenVatLieu_LoadDl1(GetDrop(dlChiNhanhSearch),
             dlXemTheo.SelectedValue, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            if (query != null)
            {
                dl1.Items.Clear();
                dl1.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL1,
                                      Ten = p.TenDL1
                                  }).Distinct().OrderBy(p => p.Ten);
                dl1.DataBind();
                dl1.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl2.Items.Clear();
                dl2.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL2,
                                      Ten = p.TenDL2
                                  }).Distinct().OrderBy(p => p.Ten);
                dl2.DataBind();
                dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl4.Items.Clear();
                dl4.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL4,
                                      Ten = p.TenDL4
                                  }).Distinct().OrderBy(p => p.Ten);
                dl4.DataBind();
                dl4.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl5.Items.Clear();
                dl5.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL5,
                                      Ten = p.TenDL5
                                  }).Distinct().OrderBy(p => p.Ten);
                dl5.DataBind();
                dl5.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        private void LoadDl2()
        {
            List<sp_GiaCuocVanChuyenVatLieu_LoadDl2Result> query = vatlieu.sp_GiaCuocVanChuyenVatLieu_LoadDl2(GetDrop(dlChiNhanhSearch), dl1.SelectedValue,
             dlXemTheo.SelectedValue, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            if (query != null)
            {
                dl2.Items.Clear();
                dl2.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL2,
                                      Ten = p.TenDL2
                                  }).Distinct().OrderBy(p => p.Ten);
                dl2.DataBind();
                dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl4.Items.Clear();
                dl4.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL4,
                                      Ten = p.TenDL4
                                  }).Distinct().OrderBy(p => p.Ten);
                dl4.DataBind();
                dl4.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl5.Items.Clear();
                dl5.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL5,
                                      Ten = p.TenDL5
                                  }).Distinct().OrderBy(p => p.Ten);
                dl5.DataBind();
                dl5.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        private void LoadDl3()
        {
            List<sp_GiaCuocVanChuyenVatLieu_LoadDl3Result> query = vatlieu.sp_GiaCuocVanChuyenVatLieu_LoadDl3(GetDrop(dlChiNhanhSearch), dl1.SelectedValue, dl2.SelectedValue,
             dlXemTheo.SelectedValue, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            if (query != null)
            {
                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl4.Items.Clear();
                dl4.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL4,
                                      Ten = p.TenDL4
                                  }).Distinct().OrderBy(p => p.Ten);
                dl4.DataBind();
                dl4.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl5.Items.Clear();
                dl5.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL5,
                                      Ten = p.TenDL5
                                  }).Distinct().OrderBy(p => p.Ten);
                dl5.DataBind();
                dl5.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        private void LoadDl4()
        {
            List<sp_GiaCuocVanChuyenVatLieu_LoadDl4Result> query = vatlieu.sp_GiaCuocVanChuyenVatLieu_LoadDl4(GetDrop(dlChiNhanhSearch), dl1.SelectedValue, dl2.SelectedValue, dl3.SelectedValue,
             dlXemTheo.SelectedValue, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            if (query != null)
            {
                dl4.Items.Clear();
                dl4.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL4,
                                      Ten = p.TenDL4
                                  }).Distinct().OrderBy(p => p.Ten);
                dl4.DataBind();
                dl4.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl5.Items.Clear();
                dl5.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL5,
                                      Ten = p.TenDL5
                                  }).Distinct().OrderBy(p => p.Ten);
                dl5.DataBind();
                dl5.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        private void LoadDl5()
        {
            List<sp_GiaCuocVanChuyenVatLieu_LoadDl5Result> query = vatlieu.sp_GiaCuocVanChuyenVatLieu_LoadDl5(GetDrop(dlChiNhanhSearch), dl1.SelectedValue, dl2.SelectedValue, dl3.SelectedValue, dl4.SelectedValue,
             dlXemTheo.SelectedValue, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            if (query != null)
            {
                dl5.Items.Clear();
                dl5.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL5,
                                      Ten = p.TenDL5
                                  }).Distinct().OrderBy(p => p.Ten);
                dl5.DataBind();
                dl5.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }

        protected void dl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dl2.Items.Clear();
            dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

            dl3.Items.Clear();
            dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

            dl4.Items.Clear();
            dl4.Items.Insert(0, new ListItem("--Chọn--", ""));

            dl5.Items.Clear();
            dl5.Items.Insert(0, new ListItem("--Chọn--", ""));

            if (dl1.SelectedValue != "")
                LoadDl2();
        }
        protected void dl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dl3.Items.Clear();
            dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

            dl4.Items.Clear();
            dl4.Items.Insert(0, new ListItem("--Chọn--", ""));

            dl5.Items.Clear();
            dl5.Items.Insert(0, new ListItem("--Chọn--", ""));

            if (dl2.SelectedValue != "")
                LoadDl3();
        }
        protected void dl3_SelectedIndexChanged(object sender, EventArgs e)
        {
            dl4.Items.Clear();
            dl4.Items.Insert(0, new ListItem("--Chọn--", ""));

            dl5.Items.Clear();
            dl5.Items.Insert(0, new ListItem("--Chọn--", ""));

            if (dl3.SelectedValue != "")
                LoadDl4();
        }
        protected void dl4_SelectedIndexChanged(object sender, EventArgs e)
        {
            dl5.Items.Clear();
            dl5.Items.Insert(0, new ListItem("--Chọn--", ""));

            if (dl4.SelectedValue != "")
                LoadDl5();
        }
        protected void dlXemTheo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlXemTheo.SelectedValue == "0")
            {
                divdl.Visible = false;
            }
            else if (dlXemTheo.SelectedValue == "1")
            {
                lbl1.Text = "Nhà cung cấp";
                lbl2.Text = "Nơi nhận";
                lbl3.Text = "Nhóm vật liệu";
                lbl4.Text = "Loại vật liệu";
                lbl5.Text = "Đơn vị tính";

                divdl.Visible = true;
                LoadDl1();
            }
            else if (dlXemTheo.SelectedValue == "2")
            {
                lbl1.Text = "Nơi nhận";
                lbl2.Text = "Nhà cung cấp";
                lbl3.Text = "Nhóm vật liệu";
                lbl4.Text = "Loại vật liệu";
                lbl5.Text = "Đơn vị tính";

                divdl.Visible = true;
                LoadDl1();
            }
            else if (dlXemTheo.SelectedValue == "3")
            {
                lbl1.Text = "Nhóm vật liệu";
                lbl2.Text = "Loại vật liệu";
                lbl3.Text = "Đơn vị tính";
                lbl4.Text = "Nhà cung cấp";
                lbl5.Text = "Nơi nhận";

                divdl.Visible = true;
                LoadDl1();
            }
            else if (dlXemTheo.SelectedValue == "4")
            {
                lbl1.Text = "Loại vật liệu";
                lbl2.Text = "Nhà cung cấp";
                lbl3.Text = "Nơi nhận";
                lbl4.Text = "Đơn vị tính";
                lbl5.Text = "Nhóm vật liệu";

                divdl.Visible = true;
                LoadDl1();
            }
            else if (dlXemTheo.SelectedValue == "5")
            {
                lbl1.Text = "Đơn vị tính";
                lbl2.Text = "Nhóm vật liệu";
                lbl3.Text = "Loại vật liệu";
                lbl4.Text = "Nhà cung cấp";
                lbl5.Text = "Nơi nhận";

                divdl.Visible = true;
                LoadDl1();
            }
        }
        private void Search(int page)
        {
            string index = "";
            if (dlXemTheo.SelectedValue == "0" || dl1.SelectedValue == "")
                index = "0";
            else if (dl5.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "5";
            }
            else if (dl4.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "4";
            }
            else if (dl3.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "3";
            }
            else if (dl2.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "2";
            }
            else if (dl1.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "1";
            }
            var query = vatlieu.sp_GiaCuocVanChuyenVatLieu_Search(GetDrop(dlChiNhanhSearch), dl1.SelectedValue, dl2.SelectedValue, dl3.SelectedValue, dl4.SelectedValue, dl5.SelectedValue,
            index, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text)), 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
        }
    }
}