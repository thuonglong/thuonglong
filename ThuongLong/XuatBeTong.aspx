﻿<%@ Page Title="Bán bê tông" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="XuatBeTong.aspx.cs" Inherits="ThuongLong.XuatBeTong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<style>
        .select2-container {
            z-index: 100000;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin giá</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Giờ xuất</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtGioXuat" runat="server" class="form-control" data-inputmask="'alias': 'hh:mm'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày tháng</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Khách hàng</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCap" OnSelectedIndexChanged="dlNhaCungCap_SelectedIndexChanged" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Công trình</label>
                        <asp:DropDownList CssClass="form-control select2" data-toggle="tooltip" data-original-title="Công trình"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCongTrinh" OnSelectedIndexChanged="dlCongTrinh_SelectedIndexChanged" AutoPostBack="true"
                            runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Hạng mục</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Hạng mục"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlHangMuc" runat="server" OnSelectedIndexChanged="dlHangMuc_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Nhân viên kinh doanh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhân viên kinh doanh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhanVienKinhDoanh" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Mác bê tông</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Mác bê tông"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMacBeTong" runat="server" OnSelectedIndexChanged="dlMacBeTong_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Hình thức bơm</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Hình thức bơm" OnSelectedIndexChanged="dlHinhThucBom_SelectedIndexChanged" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlHinhThucBom" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3" id="divChonPhieuCan" runat="server">
                        <div class="input-group margin">
                            <div class="input-group-btn">
                                <asp:Button ID="btnCan" runat="server" Text="Chọn" class="btn btn-info" OnClick="btnCan_Click" />
                            </div>
                            <asp:TextBox ID="txtSoPhieuCan" runat="server" class="form-control" placeholder="Phiếu cân" Width="98%" ReadOnly="true"></asp:TextBox>
                            <div class="input-group-btn">
                                <asp:Button ID="btnResetCan" runat="server" Text="Reset" class="btn btn-info" OnClick="btnResetCan_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">TL tổng</label>
                        <asp:TextBox ID="txtTLTong" runat="server" class="form-control" placeholder="TL tổng" Width="98%" Text="0" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLTong" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">TL bì</label>
                        <asp:TextBox ID="txtTLBi" runat="server" class="form-control" placeholder="TL bì" Width="98%" Text="0" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLBi" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">TL hàng</label>
                        <asp:TextBox ID="txtTLHang" runat="server" class="form-control" placeholder="TL hàng" Width="98%" Text="0" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tỷ lệ quy đổi</label>
                        <asp:TextBox ID="txtTyLeQuyDoi" runat="server" class="form-control" placeholder="Tỷ lệ quy đổi" Width="98%" Text="0" onchange="KLQuaCan()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTyLeQuyDoi" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Khối lượng cân</label>
                        <asp:TextBox ID="txtKLQuaCan" runat="server" class="form-control" placeholder="Khối lượng qua cân" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLQuaCan" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Khối lượng thực xuất</label>
                        <asp:TextBox ID="txtKLThucXuat" runat="server" class="form-control" placeholder="Khối lượng thực xuất" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLThucXuat" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Khối lượng hóa đơn</label>
                        <asp:TextBox ID="txtKLHoaDon" runat="server" class="form-control" placeholder="Khối lượng hóa đơn" Width="98%" onchange="HoaDon()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLHoaDon" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Khối lượng khách hàng</label>
                        <asp:TextBox ID="txtKLKhachHang" runat="server" class="form-control" placeholder="Khối lượng khách hàng" Width="98%" onchange="ThanhToan()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLKhachHang" ValidChars=".," />
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xe vận chuyển bê tông</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Xe vận chuyển bê tông"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXeTron" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xe bơm</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Xe bơm"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXeBom" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá hóa đơn</label>
                        <asp:TextBox ID="txtDonGiaHoaDon" runat="server" class="form-control" placeholder="Đơn giá hóa đơn" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaHoaDon" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá thanh toán</label>
                        <asp:TextBox ID="txtDonGiaThanhToan" runat="server" class="form-control" placeholder="Đơn giá thanh toán" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaThanhToan" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tiền hóa đơn</label>
                        <asp:TextBox ID="txtTienHoaDon" runat="server" class="form-control" placeholder="Tiền hóa đơn" Width="98%" disabled></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTienHoaDon" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tiền thanh toán</label>
                        <asp:TextBox ID="txtTienThanhToan" runat="server" class="form-control" placeholder="Tiền thanh toán" Width="98%" disabled></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTienThanhToan" ValidChars=".," />
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá hóa đơn bơm</label>
                        <asp:TextBox ID="txtDonGiaHoaDonBom" runat="server" class="form-control" placeholder="Đơn giá hóa đơn bơm" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaHoaDonBom" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá thanh toán bơm</label>
                        <asp:TextBox ID="txtDonGiaThanhToanBom" runat="server" class="form-control" placeholder="Đơn giá thanh toán bơm" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaThanhToanBom" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tiền hóa đơn bơm</label>
                        <asp:TextBox ID="txtTienHoaDonBom" runat="server" class="form-control" placeholder="Tiền hóa đơn bơm" Width="98%" disabled></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTienHoaDonBom" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tiền thanh toán bơm</label>
                        <asp:TextBox ID="txtTienThanhToanBom" runat="server" class="form-control" placeholder="Tiền thanh toán bơm" Width="98%" disabled></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTienThanhToanBom" ValidChars=".," />
                    </div>

                    <%--                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xi măng</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Xi măng" OnSelectedIndexChanged="dlMacBeTong_SelectedIndexChanged" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMang" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Cát</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Cát" OnSelectedIndexChanged="dlMacBeTong_SelectedIndexChanged" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCat" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Cát" OnSelectedIndexChanged="dlMacBeTong_SelectedIndexChanged" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDa" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Phụ gia</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Phụ gia" OnSelectedIndexChanged="dlMacBeTong_SelectedIndexChanged" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlPhuGia" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tro bay</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Tro bay" OnSelectedIndexChanged="dlMacBeTong_SelectedIndexChanged" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlTroBay" runat="server">
                        </asp:DropDownList>
                    </div>--%>

                    <div class="form-group col-lg-3" id="div1" runat="server" visible="false">
                        <label for="exampleInputEmail1">TL Xi măng</label>
                        <asp:TextBox ID="txtKLXiMang" runat="server" class="form-control" placeholder="TL xi măng" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLXiMang" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3" id="div2" runat="server" visible="false">
                        <label for="exampleInputEmail1">TL Đá</label>
                        <asp:TextBox ID="txtKLDa" runat="server" class="form-control" placeholder="TL đá" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLDa" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3" id="div3" runat="server" visible="false">
                        <label for="exampleInputEmail1">TL Cát</label>
                        <asp:TextBox ID="txtKLCat" runat="server" class="form-control" placeholder="TL cát" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLCat" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3" id="div4" runat="server" visible="false">
                        <label for="exampleInputEmail1">TL Phụ gia</label>
                        <asp:TextBox ID="txtKLPhuGia" runat="server" class="form-control" placeholder="TL phụ gia" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLPhuGia" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3" id="div5" runat="server" visible="false">
                        <label for="exampleInputEmail1">TL Tro bay</label>
                        <asp:TextBox ID="txtKLTroBay" runat="server" class="form-control" placeholder="TL tro bay" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLTroBay" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Người trực</label>
                        <asp:TextBox ID="txtNguoiTruc" runat="server" class="form-control" placeholder="Người trực" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Kỹ thuật</label>
                        <asp:TextBox ID="txtKyThuat" runat="server" class="form-control" placeholder="Kỹ thuật" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Người giới thiệu</label>
                        <asp:TextBox ID="txtNguoiGioiThieu" runat="server" class="form-control" placeholder="Người giới thiệu" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Phụ phí</label>
                        <asp:TextBox ID="txtPhuPhi" runat="server" class="form-control" placeholder="Phụ phí" Width="98%" onchange="PhuPhi()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtPhuPhi" ValidChars="-.," />
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <h5>Từ ngày</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Đến ngày</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Chi nhánh</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Khách hàng</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlKhachHangSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Mác bê tông</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Mác bê tông" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlMacBeTongSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnResetFilter" runat="server" class="btn btn-app bg-warning" OnClick="btnResetFilter_Click" Visible="false"><i class="fa fa-filter"></i>Lọc</asp:LinkButton>
                            <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                            <asp:LinkButton ID="btnOpenChot" runat="server" class="btn btn-app bg-green" OnClick="btnOpenChot_Click"><i class="fa fa-close"></i>Chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnOpenMoChot" runat="server" class="btn btn-app bg-warning" OnClick="btnOpenMoChot_Click"><i class="fa fa-opencart"></i>Mở chốt</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" Width="3200px" OnRowCreated="GV_RowCreated" ShowFooter="true"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Thu tiền">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblThuTien"
                                                Text='<%#Eval("DaThuTien")%>' runat="server" CssClass="padding" CommandName="ThuTien"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GioXuat" HeaderText="Giờ xuất" />
                                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />

                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" HeaderStyle-BackColor="Gold" />
                                    <asp:BoundField DataField="TenCongTrinh" HeaderText="Công trình" HeaderStyle-BackColor="Gold" />
                                    <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" HeaderStyle-BackColor="Gold" />
                                    <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" HeaderStyle-BackColor="Gold" />
                                    <asp:BoundField DataField="TenMacBeTong" HeaderText="Mác bê tông" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Gold" />

                                    <asp:BoundField DataField="TenXeTron" HeaderText="Xe vận chuyển bê tông" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />
                                    <asp:BoundField DataField="TenXeBom" HeaderText="Xe bơm" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />

                                    <asp:BoundField DataField="KLThucXuat" HeaderText="KLthực xuất" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />
                                    <asp:BoundField DataField="KLHoaDon" HeaderText="Kl hóa đơn" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />
                                    <asp:BoundField DataField="KLKhachHang" HeaderText="Kl khách hàng" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />
                                    <asp:BoundField DataField="KLQuaCan" HeaderText="Kl qua cân" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />

                                    <asp:BoundField DataField="DonGiaHoaDon" HeaderText="Giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                    <asp:BoundField DataField="DonGiaThanhToan" HeaderText="Giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                    <asp:BoundField DataField="TienHoaDon" HeaderText="Tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                    <asp:BoundField DataField="TienThanhToan" HeaderText="Tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />

                                    <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" HeaderStyle-BackColor="Aquamarine" />
                                    <asp:BoundField DataField="DonGiaHoaDonBom" HeaderText="Giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                    <asp:BoundField DataField="DonGiaThanhToanBom" HeaderText="Giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                    <asp:BoundField DataField="TienHoaDonBom" HeaderText="Tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                    <asp:BoundField DataField="TienThanhToanBom" HeaderText="Tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                    
                                    <asp:BoundField DataField="PhuPhi" HeaderText="Phụ phí" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />
                                    <asp:BoundField DataField="TongHoaDon" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />
                                    <asp:BoundField DataField="TongThanhToan" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />

                                    <%--                                    <asp:BoundField DataField="TenXiMang" HeaderText="Xi măng" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenCat" HeaderText="Cát" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenLoaiDa" HeaderText="Loại đá" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenPhuGia" HeaderText="Phụ gia" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenTroBay" HeaderText="Tro bay" HeaderStyle-BackColor="Goldenrod" />--%>

                                    <%--                                    <asp:BoundField DataField="KLXiMang" HeaderText="Xi măng" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLCat" HeaderText="Cát" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLDa" HeaderText="Loại đá" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLPhuGia" HeaderText="Phụ gia" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLTroBay" HeaderText="Tro bay" HeaderStyle-BackColor="DarkSlateGray" />--%>

                                    <asp:BoundField DataField="NguoiTruc" HeaderText="Người trực" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KyThuat" HeaderText="Kỹ thuật" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="NguoiGioiThieu" HeaderText="Người giới thiệu" HeaderStyle-BackColor="DarkSlateGray" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>


            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function PhuPhi() {
                    var txtPhuPhi = parseFloat(getvalue(document.getElementById("<%=txtPhuPhi.ClientID %>").value));
                    if (txtPhuPhi != '0') {
                        document.getElementById("<%=txtPhuPhi.ClientID %>").value = (txtPhuPhi).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtPhuPhi.ClientID %>").value = '0';
                    }
                }
                function HoaDon() {
                    var txtKLHoaDon = parseFloat(getvalue(document.getElementById("<%=txtKLHoaDon.ClientID %>").value));
                    var txtDonGiaHoaDon = parseFloat(getvalue(document.getElementById("<%=txtDonGiaHoaDon.ClientID %>").value));

                    var txtDonGiaHoaDonBom = parseFloat(getvalue(document.getElementById("<%=txtDonGiaHoaDonBom.ClientID %>").value));

                    var hdLoaiBom = document.getElementById("<%=hdLoaiBom.ClientID %>").value;

                    if (txtKLHoaDon != 0) {
                        document.getElementById("<%=txtTienHoaDon.ClientID %>").value = (txtDonGiaHoaDon * txtKLHoaDon).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtTienHoaDon.ClientID %>").value = '0';
                    }
                    if (txtKLHoaDon != 0 && hdLoaiBom.toUpperCase() == '0'.toUpperCase()) {

                        document.getElementById("<%=txtTienHoaDonBom.ClientID %>").value = (txtDonGiaHoaDonBom).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else if (txtKLHoaDon != 0 && hdLoaiBom.toUpperCase() == '1'.toUpperCase()) {
                        document.getElementById("<%=txtTienHoaDonBom.ClientID %>").value = (txtDonGiaHoaDonBom * txtKLHoaDon).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtTienHoaDonBom.ClientID %>").value = '0';
                    }
                }
                function ThanhToan() {
                    var txtKLKhachHang = parseFloat(getvalue(document.getElementById("<%=txtKLKhachHang.ClientID %>").value));
                    var txtDonGiaThanhToan = parseFloat(getvalue(document.getElementById("<%=txtDonGiaThanhToan.ClientID %>").value));

                    var txtDonGiaThanhToanBom = parseFloat(getvalue(document.getElementById("<%=txtDonGiaThanhToanBom.ClientID %>").value));

                    var hdLoaiBom = document.getElementById("<%=hdLoaiBom.ClientID %>").value;

                    if (txtKLKhachHang != 0) {
                        document.getElementById("<%=txtTienThanhToan.ClientID %>").value = (txtDonGiaThanhToan * txtKLKhachHang).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtTienThanhToan.ClientID %>").value = '0';
                    }
                    if (txtKLKhachHang != 0 && hdLoaiBom.toUpperCase() == '0'.toUpperCase()) {

                        document.getElementById("<%=txtTienThanhToanBom.ClientID %>").value = (txtDonGiaThanhToanBom).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else if (txtKLKhachHang != 0 && hdLoaiBom.toUpperCase() == '1'.toUpperCase()) {
                        document.getElementById("<%=txtTienThanhToanBom.ClientID %>").value = (txtDonGiaThanhToanBom * txtKLKhachHang).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtTienThanhToanBom.ClientID %>").value = '0';
                    }
                }
            </script>

            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdIDChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdCan" runat="server" Value="" />
            <asp:HiddenField ID="hdChot" runat="server" Value="" />
            <asp:HiddenField ID="hdLoaiBom" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound" OnRowCreated="gvLichSu_RowCreated"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="GioXuat" HeaderText="Giờ xuất" />
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />

                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" HeaderStyle-BackColor="Gold" />
                                <asp:BoundField DataField="TenCongTrinh" HeaderText="Công trình" HeaderStyle-BackColor="Gold" />
                                <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" HeaderStyle-BackColor="Gold" />
                                <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" HeaderStyle-BackColor="Gold" />
                                <asp:BoundField DataField="TenMacBeTong" HeaderText="Mác bê tông" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Gold" />

                                <asp:BoundField DataField="TenXeTron" HeaderText="Xe vận chuyển bê tông" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />
                                <asp:BoundField DataField="TenXeBom" HeaderText="Xe bơm" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />

                                <asp:BoundField DataField="KLThucXuat" HeaderText="KLthực xuất" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />
                                <asp:BoundField DataField="KLKhachHang" HeaderText="Kl khách hàng" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />
                                <asp:BoundField DataField="KLQuaCan" HeaderText="Kl qua cân" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />

                                <asp:BoundField DataField="DonGiaHoaDon" HeaderText="Giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                <asp:BoundField DataField="DonGiaThanhToan" HeaderText="Giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                <asp:BoundField DataField="TienHoaDon" HeaderText="Tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                <asp:BoundField DataField="TienThanhToan" HeaderText="Tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />

                                <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" HeaderStyle-BackColor="Aquamarine" />
                                <asp:BoundField DataField="DonGiaHoaDonBom" HeaderText="Giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                <asp:BoundField DataField="DonGiaThanhToanBom" HeaderText="Giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                <asp:BoundField DataField="TienHoaDonBom" HeaderText="Tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                <asp:BoundField DataField="TienThanhToanBom" HeaderText="Tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />

                                <asp:BoundField DataField="TongHoaDon" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />
                                <asp:BoundField DataField="TongThanhToan" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />

                                <%--                                    <asp:BoundField DataField="TenXiMang" HeaderText="Xi măng" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenCat" HeaderText="Cát" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenLoaiDa" HeaderText="Loại đá" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenPhuGia" HeaderText="Phụ gia" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenTroBay" HeaderText="Tro bay" HeaderStyle-BackColor="Goldenrod" />--%>

                                <%--                                    <asp:BoundField DataField="KLXiMang" HeaderText="Xi măng" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLCat" HeaderText="Cát" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLDa" HeaderText="Loại đá" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLPhuGia" HeaderText="Phụ gia" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLTroBay" HeaderText="Tro bay" HeaderStyle-BackColor="DarkSlateGray" />--%>

                                <asp:BoundField DataField="NguoiTruc" HeaderText="Người trực" HeaderStyle-BackColor="DarkSlateGray" />
                                <asp:BoundField DataField="KyThuat" HeaderText="Kỹ thuật" HeaderStyle-BackColor="DarkSlateGray" />
                                <asp:BoundField DataField="NguoiGioiThieu" HeaderText="Người giới thiệu" HeaderStyle-BackColor="DarkSlateGray" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpCan" runat="server" CancelControlID="btnDongCan"
                Drag="True" TargetControlID="hdCan" BackgroundCssClass="modalBackground" PopupControlID="pnCan"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnCan" runat="server" Style="width: 80%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Danh sách phiếu cân
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvCan" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound" OnRowCommand="gvCan_RowCommand"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="TGian2" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="NguoiBan" HeaderText="Người bán" />
                                <asp:BoundField DataField="NguoiMua" HeaderText="Người mua" />
                                <asp:BoundField DataField="LoaiHang" HeaderText="Loại hàng" />
                                <asp:BoundField DataField="BienSoXe" HeaderText="Biển số xe" />
                                <asp:BoundField DataField="LaiXe" HeaderText="Lái xe" />
                                <asp:BoundField DataField="KlCanL1" HeaderText="Trọng lượng tổng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KlCanL2" HeaderText="Trọng lượng bì" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLHang" HeaderText="Trọng lượng hàng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:TemplateField HeaderText="Chọn" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtChon" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("IDMN")%>' ToolTip="Chọn" CommandName="Chon">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongCan" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpChot" runat="server" CancelControlID="btnDongChot"
                Drag="True" TargetControlID="hdChot" BackgroundCssClass="modalBackground" PopupControlID="pnChot"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnChot" runat="server" Style="width: 98%; height: 100%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        <asp:Label ID="lblChot" runat="server" Text="Chốt bán bê tông"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh chốt</label>
                            <asp:DropDownList CssClass="form-control" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh chốt"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhChot" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày chốt</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayChot" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Button ID="lbtSearchChot" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="lbtSearchChot_Click" />
                            <asp:Button ID="btnChot" runat="server" Text="Chốt" CssClass="btn btn-primary" OnClick="btnChot_Click" />
                            <asp:Button ID="btnMoChot" runat="server" Text="Mở chốt" CssClass="btn btn-primary" OnClick="btnMoChot_Click" />
                            <asp:Button ID="btnDongChot" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                        </div>
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="GVChot" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound" ShowFooter="true" OnRowCreated="GVChot_RowCreated"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px" Width="2300px">
                            <Columns>
                                <asp:BoundField DataField="TrangThaiText" HeaderText="Trạng thái" />
                                <asp:BoundField DataField="GioXuat" HeaderText="Giờ xuất" />
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />

                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" HeaderStyle-BackColor="Gold" />
                                <asp:BoundField DataField="TenCongTrinh" HeaderText="Công trình" HeaderStyle-BackColor="Gold" />
                                <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" HeaderStyle-BackColor="Gold" />
                                <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" HeaderStyle-BackColor="Gold" />
                                <asp:BoundField DataField="TenMacBeTong" HeaderText="Mác bê tông" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Gold" />

                                <asp:BoundField DataField="TenXeTron" HeaderText="Xe vận chuyển bê tông" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />
                                <asp:BoundField DataField="TenXeBom" HeaderText="Xe bơm" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />

                                <asp:BoundField DataField="KLThucXuat" HeaderText="KLthực xuất" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />
                                <asp:BoundField DataField="KLKhachHang" HeaderText="Kl khách hàng" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />
                                <asp:BoundField DataField="KLQuaCan" HeaderText="Kl qua cân" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Violet" />

                                <asp:BoundField DataField="DonGiaHoaDon" HeaderText="Giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                <asp:BoundField DataField="DonGiaThanhToan" HeaderText="Giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                <asp:BoundField DataField="TienHoaDon" HeaderText="Tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />
                                <asp:BoundField DataField="TienThanhToan" HeaderText="Tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="LawnGreen" />

                                <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" HeaderStyle-BackColor="Aquamarine" />
                                <asp:BoundField DataField="DonGiaHoaDonBom" HeaderText="Giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                <asp:BoundField DataField="DonGiaThanhToanBom" HeaderText="Giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                <asp:BoundField DataField="TienHoaDonBom" HeaderText="Tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />
                                <asp:BoundField DataField="TienThanhToanBom" HeaderText="Tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Aquamarine" />

                                <asp:BoundField DataField="TongHoaDon" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />
                                <asp:BoundField DataField="TongThanhToan" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Cyan" />

                                <%--                                    <asp:BoundField DataField="TenXiMang" HeaderText="Xi măng" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenCat" HeaderText="Cát" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenLoaiDa" HeaderText="Loại đá" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenPhuGia" HeaderText="Phụ gia" HeaderStyle-BackColor="Goldenrod" />
                                    <asp:BoundField DataField="TenTroBay" HeaderText="Tro bay" HeaderStyle-BackColor="Goldenrod" />--%>

                                <%--                                    <asp:BoundField DataField="KLXiMang" HeaderText="Xi măng" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLCat" HeaderText="Cát" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLDa" HeaderText="Loại đá" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLPhuGia" HeaderText="Phụ gia" HeaderStyle-BackColor="DarkSlateGray" />
                                    <asp:BoundField DataField="KLTroBay" HeaderText="Tro bay" HeaderStyle-BackColor="DarkSlateGray" />--%>

                                <asp:BoundField DataField="NguoiTruc" HeaderText="Người trực" HeaderStyle-BackColor="DarkSlateGray" />
                                <asp:BoundField DataField="KyThuat" HeaderText="Kỹ thuật" HeaderStyle-BackColor="DarkSlateGray" />
                                <asp:BoundField DataField="NguoiGioiThieu" HeaderText="Người giới thiệu" HeaderStyle-BackColor="DarkSlateGray" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtLuuHopDong" />--%>
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
