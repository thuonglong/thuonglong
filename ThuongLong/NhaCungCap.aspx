﻿<%@ Page Title="Nhà cung cấp/khách hàng" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NhaCungCap.aspx.cs" Inherits="ThuongLong.NhaCungCap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin nhà cung cấp/khách hàng</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Nhóm nhà cung cấp/khách hàng</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chọn người dùng"
                                    Font-Size="13px" DataTextField="Name" DataValueField="ID" ID="dlNhomNhaCungCap"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Tên nhà cung cấp/khách hàng</label>
                                <asp:TextBox ID="txtName" runat="server" class="form-control" placeholder="Tên nhà cung cấp/khách hàng"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Địa chỉ</label>
                                <asp:TextBox ID="txtDiaChi" runat="server" class="form-control" placeholder="Địa chỉ"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Email</label>
                                <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Số điện thoại</label>
                                <asp:TextBox ID="txtSoDienThoai" runat="server" class="form-control" placeholder="Số điện thoại"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Mã số thuế</label>
                                <asp:TextBox ID="txtMaSoThue" runat="server" class="form-control" placeholder="Mã số thuế"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Giám đốc</label>
                                <asp:TextBox ID="txtGiamDoc" runat="server" class="form-control" placeholder="Giám đốc"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Kế toán trưởng</label>
                                <asp:TextBox ID="txtKeToanTruong" runat="server" class="form-control" placeholder="Kế toán trưởng"></asp:TextBox>
                            </div>
                            <%--<div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Trạng thái</label>
                                <label class="tgl" style="font-size: 13px">
                                    <asp:CheckBox ID="ckStatus" Checked="false" runat="server" />
                                    <span data-on="Bật" data-off="Tắt"></span>
                                </label>
                            </div>--%>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnSave" runat="server" Text="Lưu" class="btn btn-primary" OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Hủy" class="btn btn-warning" OnClick="btnCancel_Click" />
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách nhà cung cấp/khách hàng</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-6">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control" placeholder="Tên nhà cung cấp/khách hàng"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" class="btn btn-primary" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnSua" runat="server" Text="Sửa" class="btn btn-primary" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnXoa" runat="server" Text="Xóa" class="btn btn-danger" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Khóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnKhoa" runat="server" Text='<%#Eval("TrangThai")%>' class="btn btn-info"
                                                CommandArgument='<%#Eval("ID")%>' ToolTip="Khóa" CommandName='<%#Eval("TrangThai")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <%--<asp:BoundField DataField="TrangThaiText" HeaderText="Trạng thái" />--%>
                                    <asp:BoundField DataField="NhomNhaCungCap" HeaderText="Nhóm nhà cung cấp/khách hàng" />
                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Tên nhà cung cấp/khách hàng" />
                                    <asp:BoundField DataField="DiaChi" HeaderText="Địa chỉ" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="SoDienThoai" HeaderText="Số điện thoại" />
                                    <asp:BoundField DataField="MaSoThue" HeaderText="Mã số thuế" />
                                    <asp:BoundField DataField="GiamDoc" HeaderText="Giám đốc" />
                                    <asp:BoundField DataField="KeToanTruong" HeaderText="Kế toán trưởng" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <script type="text/javascript"> 
                function clickButton(e, buttonid) {
                    var evt = e ? e : window.event;
                    var bt = document.getElementById(buttonid);

                    if (bt) {
                        if (evt.keyCode == 13) {
                            bt.click();
                            return false;
                        }
                    }
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
