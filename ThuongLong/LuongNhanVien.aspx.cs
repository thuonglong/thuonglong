﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class LuongNhanVien : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        NhanSuDataContext nhansu = new NhanSuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmLuongNhanVien";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmLuongNhanVien", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadBoPhan();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadBoPhan()
        {
            dlBoPhan.Items.Clear();
            dlChucVu.Items.Clear();
            dlNhanVien.Items.Clear();
            if (dlChiNhanh.SelectedValue != "")
            {
                var query = from p in db.tblBoPhanChucVus
                            join q in db.tblBoPhans on p.IDBoPhan equals q.ID
                            where p.TrangThai == 2
                            orderby q.TenBoPhan
                            select new
                            {
                                q.ID,
                                Ten = q.TenBoPhan
                            };
                dlBoPhan.DataSource = query;
                dlBoPhan.DataBind();
                dlBoPhan.Items.Insert(0, new ListItem("--Chọn--", ""));
                dlChucVu.Items.Insert(0, new ListItem("--Chọn--", ""));
                dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
            else
            {
                dlBoPhan.Items.Insert(0, new ListItem("--Chọn--", ""));
                dlChucVu.Items.Insert(0, new ListItem("--Chọn--", ""));
                dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        protected void LoadChucVu()
        {
            dlChucVu.Items.Clear();
            dlNhanVien.Items.Clear();
            if (dlBoPhan.SelectedValue != "")
            {
                var query = from p in db.tblBoPhanChucVus
                            join q in db.tblChucVus on p.IDChucVu equals q.ID
                            where p.TrangThai == 2
                            && p.IDBoPhan == GetDrop(dlBoPhan)
                            orderby q.TenChucVu
                            select new
                            {
                                q.ID,
                                Ten = q.TenChucVu
                            };
                dlChucVu.DataSource = query;
                dlChucVu.DataBind();
                dlChucVu.Items.Insert(0, new ListItem("--Chọn--", ""));
                dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
            else
            {
                dlChucVu.Items.Insert(0, new ListItem("--Chọn--", ""));
                dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        protected void LoadNhanVien()
        {
            dlNhanVien.Items.Clear();
            if (dlChucVu.SelectedValue != "")
            {
                var query = from p in db.tblBoPhanChucVus
                            join q in db.tblNhanSus on p.IDNhanVien equals q.ID
                            where p.TrangThai == 2
                            && p.IDBoPhan == GetDrop(dlBoPhan)
                            && p.IDChucVu == GetDrop(dlChucVu)
                            orderby q.TenNhanVien
                            select new
                            {
                                q.ID,
                                Ten = q.TenNhanVien
                            };
                dlNhanVien.DataSource = query;
                dlNhanVien.DataBind();
                dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
            else
            {
                dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }

        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBoPhan();
        }

        protected void dlBoPhan_SelectedIndexChanged1(object sender, EventArgs e)
        {
            LoadChucVu();
        }

        protected void dlChucVu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhanVien();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem(ref string idbophanchucvu)
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlBoPhan.SelectedValue == "")
            {
                s += " - Chọn bộ phận<br />";
            }
            if (dlChucVu.SelectedValue == "")
            {
                s += " - Chọn chức vụ<br />";
            }
            if (dlNhanVien.SelectedValue == "")
            {
                s += " - Chọn nhân viên<br />";
            }
            if (txtLuongCoBan.Text == "")
            {
                s += " - Nhập lương cơ bản<br />";
            }
            if (txtLuongPhuCap.Text == "")
            {
                s += " - Nhập lương phụ cấp<br />";
            }
            if (txtLuongTrachNhiem.Text == "")
            {
                s += " - Nhập lương trách nhiệm<br />";
            }
            if (txtBaoHiem.Text == "")
            {
                s += " - Nhập tiền bảo hiểm<br />";
            }
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            if (s == "")
            {
                var query = nhansu.sp_LuongNhanVien_CheckThem(GetDrop(dlChiNhanh), GetDrop(dlNhanVien), GetDrop(dlBoPhan), GetDrop(dlChucVu),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref idbophanchucvu, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua(ref string idbophanchucvu)
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlBoPhan.SelectedValue == "")
            {
                s += " - Chọn bộ phận<br />";
            }
            if (dlChucVu.SelectedValue == "")
            {
                s += " - Chọn chức vụ<br />";
            }
            if (dlNhanVien.SelectedValue == "")
            {
                s += " - Chọn nhân viên<br />";
            }
            if (txtLuongCoBan.Text == "")
            {
                s += " - Nhập lương cơ bản<br />";
            }
            if (txtLuongPhuCap.Text == "")
            {
                s += " - Nhập lương phụ cấp<br />";
            }
            if (txtLuongTrachNhiem.Text == "")
            {
                s += " - Nhập lương trách nhiệm<br />";
            }
            if (txtBaoHiem.Text == "")
            {
                s += " - Nhập tiền bảo hiểm<br />";
            }
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            if (txtBaoHiem.Text != "" && txtLuongCoBan.Text != "" && 
                decimal.Parse(txtBaoHiem.Text.Trim()) > decimal.Parse(txtLuongCoBan.Text.Trim()))
            {
                s += " - Lương bảo hiểm phải nhỏ hơn lương cơ bản<br />";
            }
            if (s == "")
            {
                var query = nhansu.sp_LuongNhanVien_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhanVien), GetDrop(dlBoPhan), GetDrop(dlChucVu),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref idbophanchucvu, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string idbophanchucvu = "";
            if (hdID.Value == "")
            {
                if (CheckThem(ref idbophanchucvu) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var LuongNhanVien = new tblLuongNhanVien()
                        {
                            ID = Guid.NewGuid(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhanVien = GetDrop(dlNhanVien),
                            IDBoPhan = GetDrop(dlBoPhan),
                            IDChucVu = GetDrop(dlChucVu),
                            IDBoPhanChucVu = new Guid(idbophanchucvu),
                            LuongCoBan = txtLuongCoBan.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongCoBan.Text.Trim()),
                            LuongPhuCap = txtLuongPhuCap.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongPhuCap.Text.Trim()),
                            LuongTrachNhiem = txtLuongTrachNhiem.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongTrachNhiem.Text.Trim()),
                            TongLuong = (txtLuongCoBan.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongCoBan.Text.Trim()))
                            + (txtLuongPhuCap.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongPhuCap.Text.Trim()))
                            + (txtLuongTrachNhiem.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongTrachNhiem.Text.Trim()))
                            - (txtBaoHiem.Text.Trim() == "" ? 0 : decimal.Parse(txtBaoHiem.Text.Trim()))
                            - (txtCongDoan.Text.Trim() == "" ? 0 : decimal.Parse(txtCongDoan.Text.Trim()))
                            - (txtKhauTruKhac.Text.Trim() == "" ? 0 : decimal.Parse(txtKhauTruKhac.Text.Trim()))
                            ,
                            TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                            DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now,
                            BaoHiem = txtBaoHiem.Text.Trim() == "" ? 0 : decimal.Parse(txtBaoHiem.Text.Trim()),
                            CongDoanPhi = txtCongDoan.Text.Trim() == "" ? 0 : decimal.Parse(txtCongDoan.Text.Trim()),
                            KhauTruKhac = txtKhauTruKhac.Text.Trim() == "" ? 0 : decimal.Parse(txtKhauTruKhac.Text.Trim())

                        };
                        db.tblLuongNhanViens.InsertOnSubmit(LuongNhanVien);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua(ref idbophanchucvu) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblLuongNhanViens
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhanVien = GetDrop(dlNhanVien);
                            query.IDBoPhan = GetDrop(dlBoPhan);
                            query.IDChucVu = GetDrop(dlChucVu);
                            query.IDBoPhanChucVu = new Guid(idbophanchucvu);
                            query.LuongCoBan = txtLuongCoBan.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongCoBan.Text.Trim());
                            query.LuongPhuCap = txtLuongPhuCap.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongPhuCap.Text.Trim());
                            query.LuongTrachNhiem = txtLuongTrachNhiem.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongTrachNhiem.Text.Trim());
                            query.TongLuong = (txtLuongCoBan.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongCoBan.Text.Trim()))
                            + (txtLuongPhuCap.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongPhuCap.Text.Trim()))
                            + (txtLuongTrachNhiem.Text.Trim() == "" ? 0 : decimal.Parse(txtLuongTrachNhiem.Text.Trim())
                            - (txtBaoHiem.Text.Trim() == "" ? 0 : decimal.Parse(txtBaoHiem.Text.Trim()))
                            - (txtCongDoan.Text.Trim() == "" ? 0 : decimal.Parse(txtCongDoan.Text.Trim()))
                            - (txtKhauTruKhac.Text.Trim() == "" ? 0 : decimal.Parse(txtKhauTruKhac.Text.Trim()))
                            );
                            query.TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text));
                            query.DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text));
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            query.BaoHiem = txtBaoHiem.Text.Trim() == "" ? 0 : decimal.Parse(txtBaoHiem.Text.Trim());
                            query.CongDoanPhi = txtCongDoan.Text.Trim() == "" ? 0 : decimal.Parse(txtCongDoan.Text.Trim());
                            query.KhauTruKhac = txtKhauTruKhac.Text.Trim() == "" ? 0 : decimal.Parse(txtKhauTruKhac.Text.Trim());
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin giá mua vật liệu đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedValue = "";
            LoadBoPhan();
            txtLuongCoBan.Text = "";
            txtLuongPhuCap.Text = "";
            txtLuongCoBan.Text = "";
            txtLuongTrachNhiem.Text = "";
            txtBaoHiem.Text = "";
            txtKhauTruKhac.Text = "";
            txtCongDoan.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
            hdID.Value = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblLuongNhanViens
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        nhansu.sp_LuongNhanVien_CheckSuaGV(query.ID, query.IDNhanVien, query.IDBoPhan, query.IDChucVu, query.IDBoPhanChucVu, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            LoadBoPhan();
                            dlBoPhan.SelectedValue = query.IDBoPhan.ToString();
                            LoadChucVu();
                            dlChucVu.SelectedValue = query.IDChucVu.ToString();
                            LoadNhanVien();
                            dlNhanVien.SelectedValue = query.IDNhanVien.ToString();
                            txtLuongCoBan.Text = string.Format("{0:N0}", query.LuongCoBan);
                            txtLuongPhuCap.Text = string.Format("{0:N0}", query.LuongPhuCap);
                            txtLuongTrachNhiem.Text = string.Format("{0:N0}", query.LuongTrachNhiem);
                            txtTongLuong.Text = string.Format("{0:N0}", query.TongLuong);
                            txtBaoHiem.Text = string.Format("{0:N0}", query.BaoHiem);
                            txtCongDoan.Text = string.Format("{0:N0}", query.CongDoanPhi);
                            txtKhauTruKhac.Text = string.Format("{0:N0}", query.KhauTruKhac);
                            txtTuNgay.Text = query.TuNgay.ToString("dd/MM/yyyy");
                            txtDenNgay.Text = query.DenNgay == null ? "" : query.DenNgay.Value.ToString("dd/MM/yyyy");
                            hdID.Value = id;
                            //btnSave.Text = "Cập nhật";
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        nhansu.sp_LuongNhanVien_CheckXoa(query.ID, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai != 3)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                            }
                            Success("Xóa thành công");
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = nhansu.sp_LuongNhanVien_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void dlBoPhan_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadChucVu();
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }

        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            if (divdl.Visible == true)
            {
                divdl.Visible = false;
                dlTenNhanVienSearch.Items.Clear();
                dlBoPhanSearch.Items.Clear();
                dlChucVuSearch.Items.Clear();
            }
            else
            {
                divdl.Visible = true;
                ResetFilter();
            }
        }
        protected void ResetFilter()
        {
            dlTenNhanVienSearch.Items.Clear();
            dlBoPhanSearch.Items.Clear();
            dlChucVuSearch.Items.Clear();
            List<sp_LuongNhanVien_ResetFilterResult> query = nhansu.sp_LuongNhanVien_ResetFilter(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();

            dlTenNhanVienSearch.DataSource = (from p in query
                                              select new
                                              {
                                                  ID = p.IDNhanVien,
                                                  Ten = p.TenNhanVien
                                              }).Distinct().OrderBy(p => p.Ten);
            dlTenNhanVienSearch.DataBind();

            dlBoPhanSearch.DataSource = (from p in query
                                         select new
                                         {
                                             ID = p.IDBoPhan,
                                             Ten = p.TenBoPhan
                                         }).Distinct().OrderBy(p => p.Ten);
            dlBoPhanSearch.DataBind();
            dlChucVuSearch.DataSource = (from p in query
                                         select new
                                         {
                                             ID = p.IDChucVu,
                                             Ten = p.TenChucVu
                                         }).Distinct().OrderBy(p => p.Ten);
            dlChucVuSearch.DataBind();
        }
        private void Search(int page)
        {
            if (txtTuNgaySearch.Text != "" && txtDenNgaySearch.Text != "" && DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)) <= DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)))
            {
                string query = "SELECT a.ID, e.TenChiNhanh, b.TenNhanVien, c.TenBoPhan, d.TenChucVu, a.LuongCoBan, a.LuongPhuCap, a.LuongTrachNhiem, a.TongLuong, a.TuNgay, a.DenNgay, a.TrangThaiText, a.NgayTao, a.NguoiTao, " +
                    " a.CongDoanPhi, a.BaoHiem,a.KhauTruKhac " +
                "FROM tblLuongNhanVien AS a JOIN tblNhanSu AS b ON b.ID = a.IDNhanVien JOIN tblBoPhan AS c ON c.ID = a.IDBoPhan JOIN tblChucVu AS d ON d.ID = a.IDChucVu JOIN tblChiNhanh AS e ON e.ID = a.IDChiNhanh " +
                "WHERE  a.IDChiNhanh = '" + dlChiNhanhSearch.SelectedValue + "' and ('" + GetNgayThang(txtTuNgaySearch.Text) + "' BETWEEN a.TuNgay AND ISNULL(a.DenNgay, '6/6/2079') OR '" + GetNgayThang(txtDenNgaySearch.Text) + "' BETWEEN a.TuNgay AND ISNULL(a.DenNgay, '6/6/2079') OR a.TuNgay BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "' OR ISNULL(a.DenNgay, '6/6/2079') BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "') ";
                string sqlgiua = "";

                sqlgiua += GetValueSelectedListBox(" and a.IDNhanVien ", dlTenNhanVienSearch);
                sqlgiua += GetValueSelectedListBox(" and a.IDBoPhan ", dlBoPhanSearch);
                sqlgiua += GetValueSelectedListBox(" and a.IDChucVu ", dlChucVuSearch);

                string sqlcuoi = " ORDER BY a.TrangThaiText OFFSET 20 * (" + Convert.ToString(page) + " - 1) ROWS FETCH NEXT 20 ROWS ONLY";

                query = query + sqlgiua + sqlcuoi;

                SqlConnection con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);

                GV.DataSource = ds;
                GV.DataBind();
            }
            else
            {
                GV.DataSource = null;
                GV.DataBind();
            }
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (divdl.Visible == true)
            {
                ResetFilter();
            }
            else
            {
                dlTenNhanVienSearch.Items.Clear();
                dlBoPhanSearch.Items.Clear();
                dlChucVuSearch.Items.Clear();
            }
        }
    }
}