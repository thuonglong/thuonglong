﻿<%@ Page Title="Phân quyền vật liệu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CaiDatNhanSu.aspx.cs" Inherits="ThuongLong.CaiDatNhanSu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Chi nhánh</label>
                                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chọn chi nhánh"
                                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh"
                                                runat="server" ValidationGroup="Them" AutoPostBack="true" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Người dùng</label>
                                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chọn người dùng"
                                                Font-Size="13px" DataTextField="UserName" DataValueField="UserName" ID="dlNguoiDung"
                                                runat="server" ValidationGroup="Them" AutoPostBack="true" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label></label>
                                            <br />
                                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                                ValidationGroup="Them">Lưu cài đặt</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" style="background-color: #f5f5f5">
                                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" AutoPostBack="true" CssClass="NewsTab" CssTheme="None"
                                        ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged">
                                        <ajaxToolkit:TabPanel ID="frmNhanSu" runat="server" HeaderText="Nhân viên" Visible="false">
                                            <ContentTemplate>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="frmBoPhanChucVu" runat="server" HeaderText="Bộ phận" Visible="false">
                                            <ContentTemplate>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="frmLuongNhanVien" runat="server" HeaderText="Lương nhân viên" Visible="false">
                                            <ContentTemplate>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                    </ajaxToolkit:TabContainer>
                                </div>
                                <div class="row">
                                    <br />
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col-lg-3" id="divChung" runat="server">
                                        <div id="Them" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label2" runat="server" Text="Thêm"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckThem" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div id="Sua" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label9" runat="server" Text="Sửa"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckSua" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div id="Xoa" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label11" runat="server" Text="Xóa"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckXoa" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div id="Xem" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label12" runat="server" Text="Xem"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckXem" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div id="Xuat" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label14" runat="server" Text="Xuất"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckXuat" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>

                                        <div id="ThuCap" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label3" runat="server" Text="Duyệt"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckThuCap" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>

                                        <div id="DuyetXoa" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label8" runat="server" Text="Duyệt xóa"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckDuyetXoa" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-3" id="divChot" runat="server">
                                        <div id="Chot" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label15" runat="server" Text="Chốt"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckChot" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div id="MoChot" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label293" runat="server" Text="Mở chốt"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckMoChot" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>

                                        <div id="DuyetChot" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label24" runat="server" Text="Duyệt chốt"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckDuyetChot" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>


                                        <div id="DuyetMoChot" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label28" runat="server" Text="Duyệt mở chốt"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckDuyetMoChot" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-3" id="divThanhLy" runat="server">
                                        <div id="ThanhLy" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label296" runat="server" Text="Thanh lý"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckThanhLy" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div id="MoThanhLy" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label17" runat="server" Text="Mở thanh lý"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckMoThanhLy" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div id="DuyetThanhLy" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label37" runat="server" Text="Duyệt thanh lý"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckDuyetThanhLy" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>

                                        <div id="DuyetMoThanhLy" runat="server" style="padding-bottom: 30px; padding-top: 8px;">
                                            <div class="col-lg-6" style="text-align: left;">
                                                <b>
                                                    <asp:Label ID="Label40" runat="server" Text="Duyệt mở thanh lý"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-6" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckDuyetMoThanhLy" Checked="false" runat="server" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-lg-3" id="CaiDat" runat="server" visible="false">
                                        <div class="panel-heading" style="padding-bottom: 30px; border: 2px solid #4CAF50;">
                                            <div class="col-lg-9" style="text-align: center;">
                                                <b>
                                                    <asp:Label ID="lbCaiDat" runat="server" Text="Quyền thao tác"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-3" style="text-align: right;">
                                                <label class="tgl" style="font-size: 13px">
                                                    <asp:CheckBox ID="ckThaoTac" Checked="false" runat="server" onclick="ckThaoTacChecked()" />
                                                    <span data-on="Bật" data-off="Tắt"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="panel-body" style="padding-bottom: 5px; border: 2px solid #4CAF50; border-bottom: hidden; border-top: hidden;">
                                        </div>
                                        <div class="panel-footer" style="padding-bottom: 30px; border: 2px solid #4CAF50;">
                                        </div>
                                    </div>

                                </div>

                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <asp:HiddenField ID="hdCaiDat" runat="server" />
            <asp:HiddenField ID="hdDuyetThuCap" runat="server" />
            <asp:HiddenField ID="hdDuyetCaoCap" runat="server" />
            <asp:HiddenField ID="hdDuyetLai" runat="server" />
            <asp:HiddenField ID="hdNgay" runat="server" />
            <asp:HiddenField ID="hdActive" runat="server" />
            <asp:HiddenField ID="hdThem" runat="server" />
            <asp:HiddenField ID="hdSua" runat="server" />
            <asp:HiddenField ID="hdXoa" runat="server" />
            <asp:HiddenField ID="hdXem" runat="server" />
            <asp:HiddenField ID="hdXuat" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
