﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace ThuongLong
{


    public partial class DuyetCongVan
    {

        /// <summary>
        /// UpdatePanel1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanel1;

        /// <summary>
        /// TabContainer1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabContainer TabContainer1;

        /// <summary>
        /// frmCongVanDen control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel frmCongVanDen;

        /// <summary>
        /// uc_DuyetCongVanDen1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ThuongLong.UCVatLieu.uc_DuyetCongVanDen uc_DuyetCongVanDen1;

        /// <summary>
        /// frmCongVanDi control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel frmCongVanDi;

        /// <summary>
        /// uc_DuyetCongVanDi1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ThuongLong.UCVatLieu.uc_DuyetCongVanDi uc_DuyetCongVanDi1;

        /// <summary>
        /// frmQuanLyQuyetDinh control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel frmQuanLyQuyetDinh;

        /// <summary>
        /// uc_DuyetQuanLyQuyetDinh1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ThuongLong.UCVatLieu.uc_DuyetQuanLyQuyetDinh uc_DuyetQuanLyQuyetDinh1;
    }
}
