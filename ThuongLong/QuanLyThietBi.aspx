﻿<%@ Page Title="Quản lý tài sản" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QuanLyThietBi.aspx.cs" Inherits="ThuongLong.QuanLyThietBi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin tài sản</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Tên tài sản</label>
                            <asp:TextBox ID="txtTenThietBi" runat="server" class="form-control" placeholder="Tên tài sản" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Nhóm tài sản</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm tài sản" AutoPostBack="true"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhomThietBi" runat="server" OnSelectedIndexChanged="dlNhomThietBi_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Loại tài sản</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại tài sản"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiThietBi" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đơn vị tính</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDonViTinh" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Giá có thuế</label>
                            <asp:TextBox ID="txtDonGiaCoThue" runat="server" class="form-control" placeholder="Giá có thuế" onchange="CoThue()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDonGiaCoThue" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Giá không thuế</label>
                            <asp:TextBox ID="txtDonGiaKhongThue" runat="server" class="form-control" placeholder="Giá không thuế" onchange="KhongThue()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDonGiaKhongThue" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Thời gian khấu hao(tháng)</label>
                            <asp:TextBox ID="txtThoiGiaKhauHao" runat="server" class="form-control" placeholder="Thời gian khấu hao" onchange="KhauHao()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtThoiGiaKhauHao" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Giá trị khấu hao(tháng)</label>
                            <asp:TextBox ID="txtGiaTriKhauHao" runat="server" class="form-control" placeholder="Giá trị khấu hao" Width="98%" disabled></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtGiaTriKhauHao" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chiều cao bình nhiên liệu</label>
                            <asp:TextBox ID="txtChieuCaoBinhNhienLieu" runat="server" class="form-control" placeholder="Chiều cao bình nhiên liệu" onchange="KhauHao()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtChieuCaoBinhNhienLieu" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Nhiên liệu đổ đầy bình</label>
                            <asp:TextBox ID="txtNhienLieuDoDayBinh" runat="server" class="form-control" placeholder="Nhiên liệu đổ đầy bình" Width="98%" onchange="KhauHao()"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtNhienLieuDoDayBinh" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Số lit/cm</label>
                            <asp:TextBox ID="txtLitCM" runat="server" class="form-control" placeholder="Số lit/cm" Width="98%" disabled></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtLitCM" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Định mức nhiên liệu</label>
                            <asp:TextBox ID="txtDinhMucNhienLieu" runat="server" class="form-control" placeholder="Định mức nhiên liệu" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDinhMucNhienLieu" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách tài sản</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server" Text="Xem theo"></asp:Label><br />
                            <asp:DropDownList ID="dlXemTheo" runat="server" Width="98%" CssClass="form-control select2"
                                AutoPostBack="true" OnSelectedIndexChanged="dlXemTheo_SelectedIndexChanged" Font-Size="12px">
                                <asp:ListItem Value="0"> --Xem theo-- </asp:ListItem>
                                <asp:ListItem Value="1">Nhóm tài sản</asp:ListItem>
                                <asp:ListItem Value="2">Loại tài sản</asp:ListItem>
                                <asp:ListItem Value="3">Đơn vị tính</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="box-body" id="divdl" visible="false" runat="server">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl1" runat="server"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98 % " data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dl1"
                                OnSelectedIndexChanged="dl1_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl2" runat="server"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98 % " data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dl2"
                                OnSelectedIndexChanged="dl2_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl3" runat="server"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98 % " data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dl3"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-6">
                            <asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" class="btn btn-primary" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenThietBi" HeaderText="Tên tài sản" />
                                    <asp:BoundField DataField="TenNhomThietBi" HeaderText="Nhóm tài sản" />
                                    <asp:BoundField DataField="TenLoaiThietBi" HeaderText="Loại tài sản" />
                                    <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                    <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá có thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá không thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ThoiGianKhauHao" HeaderText="Thời gian khấu hao(tháng)" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="GiaTriKhauHao" HeaderText="Giá trị khấu hao(tháng)" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="NhienLieuDoDayBinh" HeaderText="Nhiên liệu đổ đầy bình" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ChieuCaoBinhNhienLieu" HeaderText="Chiều cao bình nhiên liệu" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="LitCm" HeaderText="Lít/cm" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DinhMucNhienLieu" HeaderText="Định mức" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function CoThue() {
                    var txtDonGiaCoThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value));
                    var txtDonGiaKhongThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value));
                    var txtThoiGiaKhauHao = parseFloat(getvalue(document.getElementById("<%=txtThoiGiaKhauHao.ClientID %>").value));
                    if (txtDonGiaCoThue != '0') {
                        document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value = (txtDonGiaCoThue).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value = (0).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value = '0';
                    }
                    if (txtThoiGiaKhauHao != '0')
                        document.getElementById("<%=txtGiaTriKhauHao.ClientID %>").value = ((txtDonGiaCoThue + txtDonGiaKhongThue) / txtThoiGiaKhauHao).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                }
                function KhongThue() {
                    var txtDonGiaCoThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value));
                    var txtDonGiaKhongThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value));
                    var txtThoiGiaKhauHao = parseFloat(getvalue(document.getElementById("<%=txtThoiGiaKhauHao.ClientID %>").value));
                    if (txtDonGiaKhongThue != '0') {
                        document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value = (txtDonGiaKhongThue).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value = (0).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value = '0';
                        if (txtThoiGiaKhauHao != '0')
                            document.getElementById("<%=txtGiaTriKhauHao.ClientID %>").value = ((txtDonGiaCoThue + txtDonGiaKhongThue) / txtThoiGiaKhauHao).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                }
                function KhauHao() {
                    var txtDonGiaCoThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value));
                    var txtDonGiaKhongThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value));
                    var txtThoiGiaKhauHao = parseFloat(getvalue(document.getElementById("<%=txtThoiGiaKhauHao.ClientID %>").value));
                    if (txtThoiGiaKhauHao != '0')
                        document.getElementById("<%=txtGiaTriKhauHao.ClientID %>").value = ((txtDonGiaCoThue + txtDonGiaKhongThue) / txtThoiGiaKhauHao).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenThietBi" HeaderText="Tên tài sản" />
                                <asp:BoundField DataField="TenNhomThietBi" HeaderText="Nhóm tài sản" />
                                <asp:BoundField DataField="TenLoaiThietBi" HeaderText="Loại tài sản" />
                                <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá có thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá không thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThoiGianKhauHao" HeaderText="Thời gian khấu hao(tháng)" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="GiaTriKhauHao" HeaderText="Giá trị khấu hao(tháng)" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="NhienLieuDoDayBinh" HeaderText="Nhiên liệu đổ đầy bình" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ChieuCaoBinhNhienLieu" HeaderText="Chiều cao bình nhiên liệu" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="LitCm" HeaderText="Lít/cm" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DinhMucNhienLieu" HeaderText="Định mức" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
