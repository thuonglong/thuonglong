﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class DinhMucXeBom : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        ThietBiDataContext thietbi = new ThietBiDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmDinhMucXeBom";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmDinhMucXeBom", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadThietBi();
                        LoadThietBiSearch();
                        hdPage.Value = "1";
                    }
                }
            }
        }
        protected void LoadThietBi()
        {
            var query = from p in db.tblXeVanChuyens
                        where p.IDNhomThietBi == new Guid(hdNhomThietBi.Value)
                        orderby p.TenThietBi
                        select new
                        {
                            p.ID,
                            Ten = p.TenThietBi
                        };
            dlXe.DataSource = query;
            dlXe.DataBind();
            dlXe.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadThietBiSearch()
        {
            dlThietBiSearch.Items.Clear();
            var query = (from p in db.tblDinhMucNhienLieus
                         join q in db.tblXeVanChuyens on p.IDXe equals q.ID
                         where p.IDNhomThietBi == new Guid(hdNhomThietBi.Value)
                         select new
                         {
                             ID = p.IDXe,
                             Ten = q.TenThietBi
                         }).Distinct().OrderBy(p => p.Ten);
            dlThietBiSearch.DataSource = query;
            dlThietBiSearch.DataBind();
            dlThietBiSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlXe.SelectedValue == "")
            {
                s += " - Chọn biển số xe<br />";
            }

            if (txtDiChuyen.Text.Trim() == "")
            {
                s += "- Nhập định mức di chuyển<br/>";
            }
            if (txtBomBeTong.Text.Trim() == "")
            {
                s += "- Nhập định mức bơm bê tông<br/>";
            }
            if (txtVeSinhRaChan.Text.Trim() == "")
            {
                s += "- Nhập định mức vệ sinh, ra chân<br/>";
            }

            if (s == "")
            {
                if (hdID.Value == "")
                {
                    int query = (from p in db.tblDinhMucNhienLieus
                                 where p.IDXe == GetDrop(dlXe)
                                 select p).Count();
                    if (query > 0)
                        s += "Đã thiết lập định mức nhiên liệu cho xe " + dlXe.SelectedItem.Text.ToString() + "";

                }
                else
                {
                    int query = (from p in db.tblDinhMucNhienLieus
                                 where p.IDXe == GetDrop(dlXe)
                                 && p.ID != new Guid(hdID.Value)
                                 select p).Count();
                    if (query > 0)
                        s += "Đã thiết lập định mức nhiên liệu cho xe " + dlXe.SelectedItem.Text.ToString() + "";
                }
                ////var query = nhansu.sp_DinhMucXeBom_CheckThem(GetDrop(dlChiNhanh), GetDrop(dlNhanVien), GetDrop(dlXe), GetDrop(dlChucVu), decimal.Parse(txtDonGiaCoThue.Text) > 0 ? true : false,
                ////    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var DinhMucXeBom = new tblDinhMucNhienLieu()
                        {
                            ID = Guid.NewGuid(),
                            IDNhomThietBi = new Guid(hdNhomThietBi.Value),

                            IDXe = GetDrop(dlXe),

                            DiChuyen = double.Parse(txtDiChuyen.Text),
                            BomBeTong = double.Parse(txtBomBeTong.Text),
                            VeSinhRaChan = double.Parse(txtVeSinhRaChan.Text),

                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblDinhMucNhienLieus.InsertOnSubmit(DinhMucXeBom);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblDinhMucNhienLieus
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDXe = GetDrop(dlXe);

                            query.DiChuyen = double.Parse(txtDiChuyen.Text);
                            query.BomBeTong = double.Parse(txtBomBeTong.Text);
                            query.VeSinhRaChan = double.Parse(txtVeSinhRaChan.Text);

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin giá mua vật liệu đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            LoadThietBiSearch();
            dlXe.SelectedValue = "";

            txtDiChuyen.Text = "";
            txtBomBeTong.Text = "";
            txtVeSinhRaChan.Text = "";

            hdID.Value = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblDinhMucNhienLieus
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlXe.SelectedValue = query.IDXe.ToString();

                        txtDiChuyen.Text = query.DiChuyen.ToString();
                        txtBomBeTong.Text = query.BomBeTong.ToString();
                        txtVeSinhRaChan.Text = query.VeSinhRaChan.ToString();

                        hdID.Value = id;
                        //btnSave.Text = "Cập nhật";
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //var checkxoa = from p in db.tblNhaCungCaps
                        //               where p.IDDinhMucXeBom == query.ID
                        //               select p;
                        //if (checkxoa.Count() > 0)
                        //{
                        //    Warning("Thông tin nhóm nhà cung cấp đã được sử dụng. Không được xóa.");
                        //}
                        //else
                        //{
                        if (query.TrangThai != 3)
                        {
                            query.TrangThai = 3;
                            query.NguoiXoa = Session["IDND"].ToString();
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                        }
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = thietbi.sp_DinhMucNhienLieu_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = thietbi.sp_DinhMucNhienLieu_Search(new Guid(hdNhomThietBi.Value), dlThietBiSearch.SelectedValue, 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
        }
    }
}