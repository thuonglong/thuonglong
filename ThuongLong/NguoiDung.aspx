﻿<%@ Page Title="Quản lý tài khoản" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NguoiDung.aspx.cs" Inherits="ThuongLong.NguoiDung" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin tài khoản</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Tên đăng nhập</label>
                                <asp:TextBox ID="txtUserName" runat="server" class="form-control" placeholder="Tên đăng nhập"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Mật khẩu</label>
                                <asp:TextBox ID="txtPassword" runat="server" class="form-control" placeholder="Mật khẩu" TextMode="Password"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Ngày sinh</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtNgaySinh" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Email</label>
                                <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Điện thoại</label>
                                <asp:TextBox ID="txtPhone" runat="server" class="form-control" placeholder="Điện thoại"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnSave" runat="server" Text="Lưu" class="btn btn-primary" OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Hủy" class="btn btn-warning" OnClick="btnCancel_Click" />
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách tài khoản</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-6">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control" placeholder="Tên đăng nhập"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" class="btn btn-primary" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <%--<div class="container">
                                <div class="jumbotron text-center" style="overflow: auto; width: 100%;">
                                    <asp:GridView ID="GV" runat="server" AutoGenerateColumns="true"
                                        OnPreRender="GV_PreRender"
                                        CssClass="table table-striped">
                                    </asp:GridView>

                                </div>
                            </div>--%>
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" OnPreRender="GV_PreRender"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnSua" runat="server" Text="Sửa" class="btn btn-primary" CommandArgument='<%#Eval("UserName")%>' ToolTip="Sửa" CommandName="Sua" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnXoa" runat="server" Text="Xóa" class="btn btn-danger" CommandArgument='<%#Eval("UserName")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Khóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnKhoa" runat="server" Text='<%#Eval("TrangThai")%>' class="btn btn-info"
                                                CommandArgument='<%#Eval("UserName")%>' ToolTip="Khóa" CommandName='<%#Eval("TrangThai")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TrangThaiText" HeaderText="Trạng thái" />
                                    <asp:BoundField DataField="UserName" HeaderText="Tên đăng nhập" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="PhoneNumber" HeaderText="Số điện thoại" />
                                    <asp:BoundField DataField="BirthDay" HeaderText="Ngày sinh" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />

            <script type="text/javascript">
                $('#<%= txtNgaySinh.ClientID %>').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });

                $(document).ready(function () {
                    $('#<%= GV.ClientID %>').dataTable({
                        "aLengthMenu": [[10, 50, 75, -1], [10, 50, 75, "All"]],
                        "iDisplayLength": 10,
                        "order": [[2, "asc"]],
                        stateSave: true,
                        stateSaveCallback: function (settings, data) {
                            localStorage.setItem
                                ('DataTables_' + settings.sInstance, JSON.stringify(data));
                        },
                        stateLoadCallback: function (settings) {
                            return JSON.parse
                                (localStorage.getItem('DataTables_' + settings.sInstance));
                        }
                    });
                });
            </script>


        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
