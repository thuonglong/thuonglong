﻿<%@ Page Title="Nhật trình xe" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NhatTrinhXe.aspx.cs" Inherits="ThuongLong.NhatTrinhXe" %>

<%@ Register Src="UCDinhMucXe/uc_NhatTrinhXeBen.ascx" TagName="uc_NhatTrinhXeBen" TagPrefix="uc1" %>
<%@ Register Src="UCDinhMucXe/uc_NhatTrinhXeDauKeo.ascx" TagName="uc_NhatTrinhXeDauKeo" TagPrefix="uc2" %>
<%@ Register Src="UCDinhMucXe/uc_NhatTrinhXeXucLat.ascx" TagName="uc_NhatTrinhXeXucLat" TagPrefix="uc3" %>
<%@ Register Src="UCDinhMucXe/uc_NhatTrinhXeXucDao.ascx" TagName="uc_NhatTrinhXeXucDao" TagPrefix="uc4" %>
<%@ Register Src="UCDinhMucXe/uc_NhatTrinhXeBom.ascx" TagName="uc_NhatTrinhXeBom" TagPrefix="uc5" %>
<%@ Register Src="UCDinhMucXe/uc_NhatTrinhXeBeTong.ascx" TagName="uc_NhatTrinhXeBeTong" TagPrefix="uc6" %>
<%@ Register Src="UCDinhMucXe/uc_NhatTrinhXeCau.ascx" TagName="uc_NhatTrinhXeCau" TagPrefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chọn loại xe</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chọn loại xe"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiXe" runat="server" OnSelectedIndexChanged="dlLoaiXe_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="">Chọn loại xe</asp:ListItem>
                                <asp:ListItem Value="1">Xe ben</asp:ListItem>
                                <asp:ListItem Value="2">Xe đầu kéo</asp:ListItem>
                                <asp:ListItem Value="3">Xe xúc lật</asp:ListItem>
                                <asp:ListItem Value="4">Xe xúc đào</asp:ListItem>
                                <asp:ListItem Value="5">Xe bơm bê tông</asp:ListItem>
                                <asp:ListItem Value="6">Xe vận chuyển bê tông</asp:ListItem>
                                <asp:ListItem Value="7">Xe cẩu</asp:ListItem>
                                <asp:ListItem Value="">Xe nâng</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <uc1:uc_NhatTrinhXeBen ID="NhatTrinhXeBen" runat="server" />
            <uc2:uc_NhatTrinhXeDauKeo ID="NhatTrinhXeDauKeo" runat="server" />
            <uc3:uc_NhatTrinhXeXucLat ID="NhatTrinhXeXucLat" runat="server" />
            <uc4:uc_NhatTrinhXeXucDao ID="NhatTrinhXeXucDao" runat="server" />
            <uc5:uc_NhatTrinhXeBom ID="NhatTrinhXeBom" runat="server" />
            <uc6:uc_NhatTrinhXeBeTong ID="NhatTrinhXeBeTong" runat="server" />
            <uc7:uc_NhatTrinhXeCau ID="NhatTrinhXeCau" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
