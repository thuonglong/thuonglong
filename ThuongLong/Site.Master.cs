﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class SiteMaster : MasterPage
    {
        DBDataContext db = new DBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IDND"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                LoadMenu();
                lblTenDangNhap.Text = Session["IDND"].ToString();
            }
        }
        void LoadMenu()
        {
            string s = "";
            var query = (from p in db.tblMenus
                         where p.IDCha == null
                         && p.TrangThai == true
                         select new
                         {
                             p.ID,
                             p.Ten,
                             p.ViTri
                         }).OrderBy(p => p.ViTri);
            foreach (var item in query)
            {
                s += "<li class=\"treeview\">\n";
                s += "<a href = \"#\" >";
                s += "<i class=\"fa fa-dashboard\"></i><span>" + item.Ten + "</span>";
                s += "<span class=\"pull-right-container\">";
                s += "<i class=\"fa fa-angle-left pull-right\"></i>";
                s += "</span>";
                s += "</a>";

                var querycon = (from p in db.tblMenus
                                where p.IDCha == item.ID
                                && p.TrangThai == true
                                select new
                                {
                                    p.ID,
                                    p.Ten,
                                    p.ViTri,
                                    p.Link
                                }).OrderBy(p => p.ViTri);
                if (querycon != null)
                {
                    s += "<ul class=\"treeview-menu\">";
                    foreach (var a in querycon)
                    {
                        s += "<li><a href = \"" + a.Link + "\" ><i class=\"fa fa-circle-o\"></i>" + a.Ten + "</a></li>";
                    }
                    s += "</ul>";
                }
                s += "</li>";
            }
            //s += "<li class=\"treeview\">															\n";
            //s += "  <a href=\"#\">                                                                  \n";
            //s += "	<i class=\"fa fa-share\"></i> <span>Multilevel</span>                          \n";
            //s += "	<span class=\"pull-right-container\">                                          \n";
            //s += "	  <i class=\"fa fa-angle-left pull-right\"></i>                                \n";
            //s += "	</span>                                                                        \n";
            //s += "  </a>                                                                            \n";
            //s += "  <ul class=\"treeview-menu\">                                                    \n";
            //s += "	<li><a href=\"#\"><i class=\"fa fa-circle-o\"></i> Level One</a></li>          \n";
            //s += "	<li class=\"treeview\">                                                        \n";
            //s += "	  <a href=\"#\"><i class=\"fa fa-circle-o\"></i> Level One                     \n";
            //s += "		<span class=\"pull-right-container\">                                      \n";
            //s += "		  <i class=\"fa fa-angle-left pull-right\"></i>                            \n";
            //s += "		</span>                                                                    \n";
            //s += "	  </a>                                                                         \n";
            //s += "	  <ul class=\"treeview-menu\">                                                 \n";
            //s += "		<li><a href=\"#\"><i class=\"fa fa-circle-o\"></i> Level Two</a></li>      \n";
            //s += "		<li class=\"treeview\">                                                    \n";
            //s += "		  <a href=\"#\"><i class=\"fa fa-circle-o\"></i> Level Two                 \n";
            //s += "			<span class=\"pull-right-container\">                                  \n";
            //s += "			  <i class=\"fa fa-angle-left pull-right\"></i>                        \n";
            //s += "			</span>                                                                \n";
            //s += "		  </a>                                                                     \n";
            //s += "		  <ul class=\"treeview-menu\">                                             \n";
            //s += "			<li><a href=\"#\"><i class=\"fa fa-circle-o\"></i> Level Three</a></li>\n";
            //s += "			<li><a href=\"#\"><i class=\"fa fa-circle-o\"></i> Level Three</a></li>\n";
            //s += "		  </ul>                                                                    \n";
            //s += "		</li>                                                                      \n";
            //s += "	  </ul>                                                                        \n";
            //s += "	</li>                                                                          \n";
            //s += "	<li><a href=\"#\"><i class=\"fa fa-circle-o\"></i> Level One</a></li>          \n";
            //s += "  </ul>                                                                           \n";
            //s += "</li>                                                                             \n";
            Literal1.Text = s;
        }
    }
}