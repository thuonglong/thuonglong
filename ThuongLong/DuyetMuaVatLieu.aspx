﻿<%@ Page Title="Duyệt mua vật liệu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetMuaVatLieu.aspx.cs" Inherits="ThuongLong.DuyetMuaVatLieu" %>

<%@ Register Src="UCVatLieu/uc_DuyetHopDongMuaVatLieu.ascx" TagName="uc_DuyetHopDongMuaVatLieu" TagPrefix="uc1" %>
<%@ Register Src="UCVatLieu/uc_DuyetNhapKhoVatLieu.ascx" TagName="uc_DuyetNhapKhoVatLieu" TagPrefix="uc2" %>
<%@ Register Src="UCVatLieu/uc_DuyetHopDongBanVatLieu.ascx" TagName="uc_DuyetHopDongBanVatLieu" TagPrefix="uc3" %>
<%@ Register Src="UCVatLieu/uc_DuyetBanVatLieu.ascx" TagName="uc_DuyetBanVatLieu" TagPrefix="uc4" %>
<%@ Register Src="UCVatLieu/uc_DuyetChotNhapKhoVatLieu.ascx" TagName="uc_DuyetChotNhapKhoVatLieu" TagPrefix="uc5" %>
<%@ Register Src="UCVatLieu/uc_DuyetChotBanVatLieu.ascx" TagName="uc_DuyetChotBanVatLieu" TagPrefix="uc6" %>
<%@ Register Src="UCVatLieu/uc_DuyetSoDuVatLieu.ascx" TagName="uc_DuyetSoDuVatLieu" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmGiaMuaVatLieu" runat="server" HeaderText="HĐ mua vật liệu">
                            <ContentTemplate>
                                <uc1:uc_DuyetHopDongMuaVatLieu ID="uc_DuyetHopDongMuaVatLieu1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmNhapKhoVatLieu" runat="server" HeaderText="Nhập kho vật liệu">
                            <ContentTemplate>
                                <uc2:uc_DuyetNhapKhoVatLieu ID="uc_DuyetNhapKhoVatLieu1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmGiaBanVatLieu" runat="server" HeaderText="HĐ bán vật liệu">
                            <ContentTemplate>
                                <uc3:uc_DuyetHopDongBanVatLieu ID="uc_DuyetHopDongBanVatLieu1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmBanVatLieu" runat="server" HeaderText="Bán vật liệu">
                            <ContentTemplate>
                                <uc4:uc_DuyetBanVatLieu ID="uc_DuyetBanVatLieu1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmChotNhapKhoVatLieu" runat="server" HeaderText="Chốt nhập kho">
                            <ContentTemplate>
                                <uc5:uc_DuyetChotNhapKhoVatLieu ID="uc_DuyetChotNhapKhoVatLieu1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmChotBanVatLieu" runat="server" HeaderText="Chốt bán vật liệu">
                            <ContentTemplate>
                                <uc6:uc_DuyetChotBanVatLieu ID="uc_DuyetChotBanVatLieu1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmSoDuVatLieu" runat="server" HeaderText="Số dư vật liệu">
                            <ContentTemplate>
                                <uc7:uc_DuyetSoDuVatLieu ID="uc_DuyetSoDuVatLieu1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
