﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class KeHoach : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmKeHoach";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmKeHoach", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadLoaiKeHoach();
                        LoadLoaiKeHoachSearch();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadLoaiKeHoach()
        {
            var query = (from p in db.tblLoaiKeHoaches
                         where p.TrangThai == 2 orderby p.STT
                         select new
                         {
                             p.ID,
                             Ten = p.TenLoaiKeHoach
                         });
            dlLoaiKeHoach.DataSource = query;
            dlLoaiKeHoach.DataBind();
            dlLoaiKeHoach.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadLoaiKeHoachSearch()
        {
            dlLoaiKeHoachSearch.Items.Clear();
            var query = (from p in db.tblKeHoaches
                         join q in db.tblLoaiKeHoaches on p.IDLoaiKeHoach equals q.ID
                         where p.IDChiNhanh == GetDrop(dlChiNhanhSearch)
                         select new
                         {
                             ID = p.IDLoaiKeHoach,
                             Ten = q.TenLoaiKeHoach
                         }).Distinct().OrderBy(p => p.Ten);
            dlLoaiKeHoachSearch.DataSource = query;
            dlLoaiKeHoachSearch.DataBind();
            dlLoaiKeHoachSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }
            else if (dlLoaiKeHoach.SelectedValue == "")
            {
                s += " - Chọn loại kế hoạch";
            }
            else if (txtTenKeHoach.Text.Trim() == "")
            {
                s += " - Nhập tên kế hoạch";
            }
            else if (txtNoiDung.Text.Trim() == "")
            {
                s += " - Nhập nội dung";
            }
            else if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày tháng";
            }
            //else if (FileUpload1.HasFile == false)
            //{
            //    s += " - Chọn file scan kế hoạch";
            //}
            //if (s == "")
            //{
            //    var query = vatlieu.sp_KeHoach_CheckThem(GetDrop(dlChiNhanh), GetDrop(dlLoaiKeHoach), GetDrop(dlNoiNhan), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text) > 0 ? true : false,
            //        DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            //}
            if (s != "")
                GstGetMess(s, "");
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }
            else if (dlLoaiKeHoach.SelectedValue == "")
            {
                s += " - Chọn loại kế hoạc";
            }
            else if (txtTenKeHoach.Text.Trim() == "")
            {
                s += " - Nhập tên kế hoạch";
            }
            else if (txtNoiDung.Text.Trim() == "")
            {
                s += " - Nhập nội dung";
            }
            else if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày tháng";
            }
            //else if (FileUpload1.HasFile == false)
            //{
            //    s += " - Chọn file scan kế hoạch";
            //}
            //else if (FileUpload1.HasFile == false)
            //{
            //    s += " - Chọn file scan kế hoạch";
            //}
            //if (s == "")
            //{
            //    var query = vatlieu.sp_KeHoach_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlLoaiKeHoach), GetDrop(dlNoiNhan), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text) > 0 ? true : false,
            //        DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            //}

            if (s != "")
                GstGetMess(s, "");
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string filename = "";
            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        Guid ID = Guid.NewGuid();
                        if (FileUpload1.HasFile)
                            UploadFile(1, ID.ToString(), ref filename);

                        var KeHoach = new tblKeHoach()
                        {
                            ID = ID,
                            NgayThang = txtNgayThang.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDLoaiKeHoach = GetDrop(dlLoaiKeHoach),
                            TenKeHoach = txtTenKeHoach.Text.Trim(),
                            NoiDung = txtNoiDung.Text.Trim(),
                            DonViThucHien = txtDonViThucHien.Text.Trim(),
                            FileUpload = filename,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            Thang = dlThangKeHoach.SelectedValue,
                            Nam = txtNamKeHoach.Text,
                            SoTien = txtSoTien.Text,
                            SoLuong = txtSoLuong.Text,

                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblKeHoaches.InsertOnSubmit(KeHoach);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        GstGetMess("Lưu thành công.", "");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblKeHoaches
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDLoaiKeHoach = GetDrop(dlLoaiKeHoach);
                            query.TenKeHoach = txtTenKeHoach.Text.Trim();
                            query.NoiDung = txtNoiDung.Text.Trim();
                            query.DonViThucHien = txtDonViThucHien.Text.Trim();
                            query.NgayThang = txtNgayThang.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            if (FileUpload1.HasFile)
                            {
                                UploadFile(1, hdID.Value, ref filename);
                                query.FileUpload = filename;
                            }
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            query.Thang = dlThangKeHoach.SelectedValue;
                            query.Nam = txtNamKeHoach.Text;
                            query.SoTien = txtSoTien.Text;
                            query.SoLuong = txtSoLuong.Text;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            GstGetMess("Sửa thành công", "");
                        }
                        else
                        {
                            Warning("Thông tin kế hoạch đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        void UploadFile(int loai, string ID, ref string filename)
        {
            if (FileUpload1.HasFile)
            {
                if (loai == 1)
                {
                    string extision = FileUpload1.FileName.Trim();
                    string time = DateTime.Now.ToString("yyyyMMddHHmmss");
                    filename = ID.ToUpper() + "-" + time + extision.Substring(extision.LastIndexOf('.'));
                    FileUpload1.SaveAs(Server.MapPath("~/ImageKeHoach/") + filename);
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlLoaiKeHoach.SelectedValue = "";
            txtTenKeHoach.Text = "";
            txtNoiDung.Text = "";
            txtDonViThucHien.Text = "";
            txtNgayThang.Text = "";
            hdID.Value = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblKeHoaches
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh.Value, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlLoaiKeHoach.SelectedValue = query.IDLoaiKeHoach.ToString();
                        txtTenKeHoach.Text = query.TenKeHoach;
                        txtNoiDung.Text = query.NoiDung;
                        txtDonViThucHien.Text = query.DonViThucHien;
                        dlThangKeHoach.SelectedValue = query.Thang;
                        txtNamKeHoach.Text = query.Nam;
                        txtSoTien.Text = query.SoTien;
                        txtSoLuong.Text = query.SoLuong;

                        txtNgayThang.Text = query.NgayThang.Value == null ? "" : query.NgayThang.Value.ToString("dd/MM/yyyy");
                        hdID.Value = id;
                        //btnSave.Text = "Cập nhật";
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh.Value, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai != 3)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                        }
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = db.sp_KeHoach_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
                else if (e.CommandName == "HinhAnh")
                {
                    imgHinhAnh.ImageUrl = "~/ImageKeHoach/" + query.FileUpload;
                    mpHinhAnh.Show();
                }
            }
            else
            {
                Warning("Thông tin kế hoạch đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = db.sp_KeHoach_Search(GetDrop(dlChiNhanhSearch), dlLoaiKeHoachSearch.SelectedValue,
            DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text)), 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiKeHoachSearch();
        }
    }
}