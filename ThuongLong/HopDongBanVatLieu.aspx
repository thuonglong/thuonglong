﻿<%@ Page Title="Hợp đồng bán vật liệu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HopDongBanVatLieu.aspx.cs" Inherits="ThuongLong.HopDongBanVatLieu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin hợp đồng</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày ký hợp đồng</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Khách hàng</label>
                        <div class="input-group input-group-sm">
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCap"
                                runat="server">
                            </asp:DropDownList>
                            <span class="input-group-btn">
                                <asp:LinkButton ID="btnTaoMoiCongTrinh" runat="server" class="btn btn-info btn-flat" OnClick="btnTaoMoiCongTrinh_Click"><i class="fa fa-plus"></i></asp:LinkButton>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số HĐ</label>
                        <asp:TextBox ID="txtSoHD" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                    </div>
                    <%--<div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Công trình</label>
                        <asp:TextBox ID="txtCongTrinh" runat="server" class="form-control" placeholder="Công trình" Width="98%"></asp:TextBox>
                    </div>--%>
                </div>
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin giá bán</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Nhóm vật liệu</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm vật liệu" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhomVatLieu" runat="server" OnSelectedIndexChanged="dlNhomVatLieu_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Loại vật liệu</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại vật liệu"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiVatLieu" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn vị tính</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDonViTinh" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá hóa đơn</label>
                        <asp:TextBox ID="txtDonGiaCoThue" runat="server" class="form-control" placeholder="Đơn giá hóa đơn" onchange="CoThue()" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaCoThue" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá thanh toán</label>
                        <asp:TextBox ID="txtDonGiaKhongThue" runat="server" class="form-control" placeholder="Đơn giá thanh toán" onchange="KhongThue()" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaKhongThue" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Từ ngày</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtTuNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đến ngày</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtDenNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtSave" runat="server" class="btn btn-app bg-green" OnClick="lbtSave_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lbtTaoGiaMoi" runat="server" class="btn btn-app bg-warning" OnClick="lbtTaoGiaMoi_Click"><i class="fa fa-plus"></i>Thêm vật liệu</asp:LinkButton>
                    </div>
                </div>

                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12" style="overflow: auto; width: 100%;">
                                <asp:GridView ID="gvChiTiet" runat="server" AutoGenerateColumns="false" OnRowCommand="gvChiTiet_RowCommand"
                                    EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trạng thái">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblLichSu"
                                                    Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                    CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                                        <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                                        <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                        <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn Đơn giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn Đơn giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" />
                                        <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" />
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                        HorizontalAlign="Right" />
                                    <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                    <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu hợp đồng</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo hợp đồng mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách hợp đồng</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server" Text="Khách hàng"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlKhachHangSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Nhân đôi hợp đồng" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtDuplicate" runat="server" class="btn btn-small bg-success" CommandArgument='<%#Eval("ID")%>' ToolTip="Nhân đôi hợp đồng" CommandName="duplicate">
                                                <i class="fa fa-diamond"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TrangThaiText" HeaderText="Trạng thái" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" />
                                    <%--<asp:BoundField DataField="CongTrinh" HeaderText="Công trình" />--%>
                                    <asp:BoundField DataField="SoHD" HeaderText="Số HĐ" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function CoThue() {
                    var txtDonGiaCoThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value));
                    var txtDonGiaKhongThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value));
                    if (txtDonGiaCoThue != '0') {
                        document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value = (txtDonGiaCoThue).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value = (txtDonGiaKhongThue).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value = '0';
                    }
                }
                function KhongThue() {
                    var txtDonGiaCoThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value));
                    var txtDonGiaKhongThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value));
                    if (txtDonGiaKhongThue != '0') {
                        document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value = (txtDonGiaKhongThue).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value = (txtDonGiaCoThue).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value = '0';
                    }
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdDuplicate" runat="server" Value="" />
            <asp:HiddenField ID="hdChung" runat="server" Value="" />
            <asp:HiddenField ID="hdChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdForm" runat="server" Value="" />
            <asp:HiddenField ID="hdNgayTao" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSuHopDong" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpHopDong" runat="server" CancelControlID="btnDongLichSuHopDong"
                Drag="True" TargetControlID="hdLichSuHopDong" BackgroundCssClass="modalBackground" PopupControlID="pnLichSuHopDong"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSuHopDong" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                                <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn Đơn giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn Đơn giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" />
                                <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSuHopDong" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="NhomVatLieu" HeaderText="Nhóm vật liệu" />
                                <asp:BoundField DataField="LoaiVatLieu" HeaderText="Loại vật liệu" />
                                <asp:BoundField DataField="DonViTinh" HeaderText="Đơn vị tính" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn Đơn giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn Đơn giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <asp:HiddenField ID="hdCongTrinh" runat="server" Value="" />
            <ajaxToolkit:ModalPopupExtender ID="mpCongTrinh" runat="server" CancelControlID="btnDongCongTrinh"
                Drag="True" TargetControlID="hdCongTrinh" BackgroundCssClass="modalBackground" PopupControlID="pnCongTrinh"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnCongTrinh" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Thêm khách hàng
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">

                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Nhóm khách hàng</label>
                            <asp:DropDownList CssClass="form-control" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiNhaCungCap" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Tên khách hàng</label>
                            <asp:TextBox ID="txtTenNhaCungCap" runat="server" class="form-control" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Số điện thoại</label>
                            <asp:TextBox ID="txtSoDienThoai" runat="server" class="form-control" placeholder="Số điện thoại" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtSoDienThoai" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Mã số thuế</label>
                            <asp:TextBox ID="txtMaSoThue" runat="server" class="form-control" placeholder="Mã số thuế" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtMaSoThue" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Địa chỉ</label>
                            <asp:TextBox ID="txtDiaChi" runat="server" class="form-control" placeholder="Địa chỉ" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnLuuCongTrinh" runat="server" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuuCongTrinh_Click" />
                        <asp:Button ID="btnDongCongTrinh" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
