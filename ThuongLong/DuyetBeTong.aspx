﻿<%@ Page Title="Duyệt bê tông" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetBeTong.aspx.cs" Inherits="ThuongLong.DuyetBeTong" %>

<%@ Register Src="UCBeTong/uc_DuyetDinhMucBeTong.ascx" TagName="uc_DuyetDinhMucBeTong" TagPrefix="uc1" %>
<%@ Register Src="UCBeTong/uc_DuyetHopDongBanBeTong.ascx" TagName="uc_DuyetHopDongBanBeTong" TagPrefix="uc2" %>
<%@ Register Src="UCBeTong/uc_DuyetLichXuatBeTong.ascx" TagName="uc_DuyetLichXuatBeTong" TagPrefix="uc3" %>
<%@ Register Src="UCBeTong/uc_DuyetHopDongThueBom.ascx" TagName="uc_DuyetHopDongThueBom" TagPrefix="uc4" %>
<%@ Register Src="UCBeTong/uc_DuyetXuatBeTong.ascx" TagName="uc_DuyetXuatBeTong" TagPrefix="uc5" %>
<%@ Register Src="UCBeTong/uc_DuyetChotXuatBeTong.ascx" TagName="uc_DuyetChotXuatBeTong" TagPrefix="uc6" %>
<%@ Register Src="UCBeTong/uc_DuyetThueBom.ascx" TagName="uc_DuyetThueBom" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmDinhMucBeTong" runat="server" HeaderText="Định mức bê tông">
                            <ContentTemplate>
                                <uc1:uc_DuyetDinhMucBeTong ID="uc_DuyetDinhMucBeTong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmGiaBanBeTong" runat="server" HeaderText="Hợp đồng bán bê tông">
                            <ContentTemplate>
                                <uc2:uc_DuyetHopDongBanBeTong ID="uc_DuyetHopDongBanBeTong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxToolkit:TabPanel ID="frmHopDongThueBom" runat="server" HeaderText="Giá thuê bơm">
                            <ContentTemplate>
                                <uc4:uc_DuyetHopDongThueBom ID="uc_DuyetHopDongThueBom1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>--%>
                        <ajaxToolkit:TabPanel ID="frmLichXuatBeTong" runat="server" HeaderText="Lịch bán bê tông">
                            <ContentTemplate>
                                <uc3:uc_DuyetLichXuatBeTong ID="uc_DuyetLichXuatBeTong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmXuatBeTong" runat="server" HeaderText="Bán bê tông">
                            <ContentTemplate>
                                <uc5:uc_DuyetXuatBeTong ID="uc_DuyetXuatBeTong" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmChotXuatBeTong" runat="server" HeaderText="Chốt bán bê tông">
                            <ContentTemplate>
                                <uc6:uc_DuyetChotXuatBeTong ID="uc_DuyetChotXuatBeTong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmThueBom" runat="server" HeaderText="Thuê bơm">
                            <ContentTemplate>
                                <uc7:uc_DuyetThueBom ID="uc_DuyetThueBom1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
