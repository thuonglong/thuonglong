﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class NhanSu : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        NhanSuDataContext nhansu = new NhanSuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmNhanSu";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmNhanSu", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //LoadChiNhanh();
                        //LoadChiNhanhSearch();
                        LoadTinh();
                        LoadTrinhDo();
                        LoadChuyenMon();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        //protected void LoadChiNhanh()
        //{
        //    var query = db.sp_LoadChiNhanhDaiLy();
        //    dlChiNhanh.DataSource = query;
        //    dlChiNhanh.DataBind();
        //    dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        //}
        //protected void LoadChiNhanhSearch()
        //{
        //    var query = db.sp_LoadChiNhanhDaiLy();
        //    dlChiNhanhSearch.DataSource = query;
        //    dlChiNhanhSearch.DataBind();
        //}
        protected void LoadTinh()
        {
            dlTinh.Items.Clear();
            dlTinh.DataSource = from p in db.tblTinhs
                                orderby p.TenTinh
                                select new
                                {
                                    p.ID,
                                    Ten = p.TenTinh
                                };
            dlTinh.DataBind();
            dlTinh.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadHuyen()
        {
            dlHuyen.Items.Clear();
            dlHuyen.DataSource = from p in db.tblHuyens
                                 where p.IDTinh == int.Parse(dlTinh.SelectedValue)
                                 orderby p.TenHuyen
                                 select new
                                 {
                                     p.ID,
                                     Ten = p.TenHuyen
                                 };
            dlHuyen.DataBind();
            dlHuyen.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadXa()
        {
            dlXa.DataSource = from p in db.tblXas
                              where p.IDHuyen == int.Parse(dlHuyen.SelectedValue)
                              orderby p.TenXa
                              select new
                              {
                                  p.ID,
                                  Ten = p.TenXa
                              };
            dlXa.DataBind();
            dlXa.Items.Insert(0, new ListItem("--Chọn--", ""));
        }

        protected void LoadTrinhDo()
        {
            dlTrinhDo.DataSource = from p in db.tblTrinhDos
                                   orderby p.TenTrinhDo
                                   select new
                                   {
                                       p.ID,
                                       Ten = p.TenTrinhDo
                                   };
            dlTrinhDo.DataBind();
            dlTrinhDo.Items.Insert(0, new ListItem("--Chọn--", ""));
        }


        protected void LoadChuyenMon()
        {
            dlChuyenMon.DataSource = from p in db.tblChuyenMons
                                     orderby p.TenChuyenMon
                                     select new
                                     {
                                         p.ID,
                                         Ten = p.TenChuyenMon
                                     };
            dlChuyenMon.DataBind();
            dlChuyenMon.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlTinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadHuyen();
        }
        protected void dlHuyen_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadXa();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";

            if (txtTenNhanVien.Text.Trim() == "")
            {
                s += " - Nhập tên nhân viên<br />";
            }
            if (txtNgaySinh.Text.Trim() == "")
            {
                s += " - Nhập ngày sinh<br />";
            }
            if (dlTrinhDo.SelectedValue == "")
            {
                s += " - Chọn trình độ<br />";
            }
            if (dlChuyenMon.SelectedValue == "")
            {
                s += " - Chọn chuyên môn<br />";
            }
            if (dlTinh.SelectedValue == "")
            {
                s += " - Chọn tỉnh<br />";
            }
            if (dlHuyen.SelectedValue == "")
            {
                s += " - Chọn huyện<br />";
            }
            if (dlXa.SelectedValue == "")
            {
                s += " - Chọn xã<br />";
            }
            if (txtCMT.Text.Trim() == "")
            {
                s += " - Nhập CMND<br />";
            }
            //if (txtDienThoai.Text.Trim() == "")
            //{
            //    s += " - Nhập số điện thoại<br />";
            //}
            if (txtTuNgay.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (s == "")
            {
                nhansu.sp_NhanSu_CheckThem(GetDrop(dlTrinhDo), GetDrop(dlChuyenMon),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (txtTenNhanVien.Text == "")
            {
                s += " - Nhập tên nhân viên<br />";
            }
            if (txtNgaySinh.Text == "")
            {
                s += " - Nhập ngày sinh<br />";
            }
            if (dlTrinhDo.SelectedValue == "")
            {
                s += " - Chọn trình độ<br />";
            }
            if (dlChuyenMon.SelectedValue == "")
            {
                s += " - Chọn chuyên môn<br />";
            }
            if (dlTinh.SelectedValue == "")
            {
                s += " - Chọn tỉnh<br />";
            }
            if (dlHuyen.SelectedValue == "")
            {
                s += " - Chọn huyện<br />";
            }
            if (dlXa.SelectedValue == "")
            {
                s += " - Chọn xã<br />";
            }
            if (txtCMT.Text == "")
            {
                s += " - Nhập CMND<br />";
            }
            //if (txtDienThoai.Text == "")
            //{
            //    s += " - Nhập số điện thoại<br />";
            //}
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (s == "")
            {
                nhansu.sp_NhanSu_CheckSua(new Guid(hdID.Value), GetDrop(dlTrinhDo), GetDrop(dlChuyenMon),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var NhanSu = new tblNhanSu()
                        {
                            ID = Guid.NewGuid(),
                            TenNhanVien = txtTenNhanVien.Text.Trim(),
                            MaSoThueCaNhan = txtMaSoThueCaNhan.Text.Trim(),
                            NgaySinh = DateTime.Parse(GetNgayThang(txtNgaySinh.Text)),
                            GioiTinh = dlGioiTinh.SelectedValue,
                            TinhTrangHonNhan = dlTinhTrangHonNhan.SelectedValue,
                            Mota = dlTrangThaiLamViec.SelectedValue,
                            //TrangThaiLamViec = dlTrangThaiLamViec.SelectedValue,
                            IDTrinhDo = GetDrop(dlTrinhDo),
                            IDChuyenMon = GetDrop(dlChuyenMon),
                            IDTinh = int.Parse(dlTinh.SelectedValue),
                            IDHuyen = int.Parse(dlHuyen.SelectedValue),
                            IDXa = int.Parse(dlXa.SelectedValue),
                            CMND = txtCMT.Text.Trim(),
                            //Email = txtEmail.Text.Trim(),
                            DienThoai = txtDienThoai.Text.Trim(),
                            //IDTrinhDo                               =    ,
                            //IDChuyenMon                             =    ,
                            //IDChucVu                                =    ,
                            IDChucVu = GetDrop(dlTrinhDo),
                            NgayKyHopDong = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                            NgayKetThucHopDong = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                            //HinhThucHopDong                         =    ,
                            //GhiChu                                  =    ,
                            //IDChiNhanh                              =    ,
                            //HinhAnhHopDong                          =    ,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //TrangThaiSua                            =    ,
                            //NguoiXoa                                =    ,
                            //NgayXoa                                 =    ,
                            //TrangThaiThanhLy                        =    ,
                            //NguoiThanhLy                            =    ,
                            //NguoiMoThanhLy                          =    ,
                            //NguoiDuyetThanhLy                       =    ,
                            //NguoiDuyetMoThanhLy                     =    ,
                            //NgayLamThanhLy                          =    ,
                            //NgayLamMoThanhLy                        =    ,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblNhanSus.InsertOnSubmit(NhanSu);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblNhanSus
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.TenNhanVien = txtTenNhanVien.Text.Trim();
                            query.MaSoThueCaNhan = txtMaSoThueCaNhan.Text.Trim();
                            query.NgaySinh = DateTime.Parse(GetNgayThang(txtNgaySinh.Text));
                            query.GioiTinh = dlGioiTinh.SelectedValue;
                            query.TinhTrangHonNhan = dlTinhTrangHonNhan.SelectedValue;
                            //query.TrangThaiLamViec = dlTrangThaiLamViec.SelectedValue;
                            query.Mota = dlTrangThaiLamViec.SelectedValue;
                            query.IDTrinhDo = GetDrop(dlTrinhDo);
                            query.IDChuyenMon = GetDrop(dlChuyenMon);
                            query.IDTinh = int.Parse(dlTinh.SelectedValue);
                            query.IDHuyen = int.Parse(dlHuyen.SelectedValue);
                            query.IDXa = int.Parse(dlXa.SelectedValue);
                            query.CMND = txtCMT.Text.Trim();
                            query.DienThoai = txtDienThoai.Text.Trim();
                            query.NgayKyHopDong = DateTime.Parse(GetNgayThang(txtTuNgay.Text));
                            query.NgayKetThucHopDong = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text));
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";

                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin nhân viên đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            txtTenNhanVien.Text = "";
            txtMaSoThueCaNhan.Text = "";
            txtNgaySinh.Text = "";
            dlGioiTinh.SelectedIndex = 0;
            dlTinhTrangHonNhan.SelectedIndex = 0;
            dlTrangThaiLamViec.SelectedIndex = 0;
            dlTrinhDo.SelectedValue = "";
            dlChuyenMon.SelectedValue = "";
            dlTinh.SelectedValue = "";

            dlHuyen.Items.Clear();
            dlXa.Items.Clear();
            dlHuyen.Items.Insert(0, new ListItem("--Chọn--", ""));
            dlXa.Items.Insert(0, new ListItem("--Chọn--", ""));

            txtCMT.Text = "";
            txtDienThoai.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
            hdID.Value = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblNhanSus
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        nhansu.sp_NhanSu_CheckSuaGV(query.ID, query.IDTrinhDo, query.IDChuyenMon, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            txtTenNhanVien.Text = query.TenNhanVien;
                            txtMaSoThueCaNhan.Text = query.MaSoThueCaNhan;
                            txtNgaySinh.Text = query.NgaySinh.ToString("dd/MM/yyyy");
                            dlGioiTinh.SelectedValue = query.GioiTinh;
                            dlTinhTrangHonNhan.SelectedValue = query.TinhTrangHonNhan;
                            dlTrangThaiLamViec.SelectedValue = query.Mota;
                            //dlTrangThaiLamViec.SelectedValue = query.TrangThaiLamViec;
                            dlTrinhDo.SelectedValue = query.IDTrinhDo.ToString();
                            dlChuyenMon.SelectedValue = query.IDChuyenMon.ToString();
                            dlTinh.SelectedValue = query.IDTinh.ToString();
                            LoadHuyen();
                            dlHuyen.SelectedValue = query.IDHuyen.ToString();
                            LoadXa();
                            dlXa.SelectedValue = query.IDXa.ToString();
                            txtCMT.Text = query.CMND;
                            txtDienThoai.Text = query.DienThoai;
                            txtTuNgay.Text = query.NgayKyHopDong.ToString("dd/MM/yyyy");
                            txtDenNgay.Text = query.NgayKetThucHopDong == null ? "" : query.NgayKetThucHopDong.Value.ToString("dd/MM/yyyy");
                            hdID.Value = id;
                            //btnSave.Text = "Cập nhật";
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        nhansu.sp_NhanSu_CheckXoa(query.ID, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai != 3)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                            }
                            Success("Xóa thành công");
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = nhansu.sp_NhanSu_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin nhân viên đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            if (divdl.Visible == true)
            {
                divdl.Visible = false;
                dlTenNhanVienSearch.Items.Clear();
                dlTrinhDoSearch.Items.Clear();
                dlChuyenMonSearch.Items.Clear();
            }
            else
            {
                divdl.Visible = true;
                ResetFilter();
            }
        }
        protected void ResetFilter()
        {
            dlTenNhanVienSearch.Items.Clear();
            dlTrinhDoSearch.Items.Clear();
            dlChuyenMonSearch.Items.Clear();
            List<sp_NhanSu_ResetFilterResult> query = nhansu.sp_NhanSu_ResetFilter(DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();

            dlTenNhanVienSearch.DataSource = (from p in query
                                              select new
                                              {
                                                  ID = p.ID,
                                                  Ten = p.TenNhanVien
                                              }).Distinct().OrderBy(p => p.Ten);
            dlTenNhanVienSearch.DataBind();

            dlTrinhDoSearch.DataSource = (from p in query
                                          select new
                                          {
                                              ID = p.IDTrinhDo,
                                              Ten = p.TenTrinhDo
                                          }).Distinct().OrderBy(p => p.Ten);
            dlTrinhDoSearch.DataBind();
            dlChuyenMonSearch.DataSource = (from p in query
                                            select new
                                            {
                                                ID = p.IDChuyenMon,
                                                Ten = p.TenChuyenMon
                                            }).Distinct().OrderBy(p => p.Ten);
            dlChuyenMonSearch.DataBind();
        }
        private void Search(int page)
        {
            string query = "SELECT a.ID, a.TenNhanVien, a.MaSoThueCaNhan, a.NgaySinh, a.GioiTinh, a.TinhTrangHonNhan, DiaChi = d.TenXa + ' ' + C.TenHuyen + ' ' + b.TenTinh, a.CMND, a.Email, a.DienThoai, e.TenTrinhDo, " +
                "f.TenChuyenMon, a.TrangThaiText, a.Mota as TrangThaiLamViec FROM tblNhanSu AS a JOIN tblTinh AS b ON a.IDTinh = b.ID JOIN tblHuyen AS c ON a.IDHuyen = c.ID JOIN tblXa AS d ON a.IDXa = d.ID " +
                "JOIN tblTrinhDo AS e ON a.idTrinhDo = e.ID JOIN tblChuyenMon AS f ON a.IDChuyenMon = f.ID  " +
                "WHERE  ('" + GetNgayThang(txtTuNgaySearch.Text) + "' BETWEEN a.NgayKyHopDong AND ISNULL(a.NgayKetThucHopDong, '6/6/2079') OR '" + GetNgayThang(txtDenNgaySearch.Text) + "' BETWEEN a.NgayKyHopDong AND ISNULL(a.NgayKetThucHopDong, '6/6/2079') OR a.NgayKyHopDong BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "' OR ISNULL(a.NgayKetThucHopDong, '6/6/2079') BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "') ";
            string sqlgiua = "";

            sqlgiua += GetValueSelectedListBox(" and a.ID ", dlTenNhanVienSearch);
            sqlgiua += GetValueSelectedListBox(" and a.IDTrinhDo ", dlTrinhDoSearch);
            sqlgiua += GetValueSelectedListBox(" and a.IDChuyenMon ", dlChuyenMonSearch);

            string sqlcuoi = " ORDER BY a.TrangThaiText, a.NgayKyHopDong desc OFFSET 20 * (" + Convert.ToString(page) + " - 1) ROWS FETCH NEXT 20 ROWS ONLY";

            query = query + sqlgiua + sqlcuoi;

            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);

            GV.DataSource = ds;
            GV.DataBind();
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }

        protected void txtTuNgaySearch_TextChanged(object sender, EventArgs e)
        {
            if (divdl.Visible == true)
            {
                ResetFilter();
            }
            else
            {
                dlTenNhanVienSearch.Items.Clear();
                dlTrinhDoSearch.Items.Clear();
                dlChuyenMonSearch.Items.Clear();
            }
        }
    }
}