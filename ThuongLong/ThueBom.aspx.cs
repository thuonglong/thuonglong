﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class ThueBom : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        BeTongDataContext betong = new BeTongDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmThueBom";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmThueBom", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        LoadChiNhanh();
                        LoadNhaCungCap();
                        LoadHinhThucBom();
                        LoadXe();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        void LoadXe()
        {
            dlXeBom.Items.Clear();
            dlXeBom.DataSource = from p in db.tblXeVanChuyens
                                 where p.TrangThai == 2
                                 && p.IsTrangThai == 1
                                 && p.IDNhomThietBi == new Guid("0F47524B-BB6F-4ED6-898E-77528924024E")
                                 orderby p.STT
                                 select new
                                 {
                                     p.ID,
                                     Ten = p.TenThietBi,
                                 };
            dlXeBom.DataBind();
            dlXeBom.Items.Insert(0, new ListItem("--Chọn --", ""));
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
            dlChiNhanhSearch.Items.Insert(0, new ListItem("Tất cả chi nhánh", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlHinhThucBom.SelectedValue == "")
            {
                s += " - Chọn hình thức bơm<br />";
            }
            if (dlXeBom.SelectedValue == "")
            {
                s += " - Chọn xe bơm<br />";
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlHinhThucBom.SelectedValue == "")
            {
                s += " - Chọn hình thức bơm<br />";
            }
            if (dlXeBom.SelectedValue == "")
            {
                s += " - Chọn xe bơm<br />";
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var thuebom = new tblThueBom();

                        thuebom.ID = Guid.NewGuid();
                        thuebom.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                        thuebom.IDChiNhanh = GetDrop(dlChiNhanh);
                        thuebom.IDNhaCungCap = GetDrop(dlNhaCungCap);
                        //thuebom.IDCongTrinh = GetDrop(dlCongTrinh);
                        thuebom.HinhThucBom = GetDrop(dlHinhThucBom);
                        thuebom.IDXeBom = dlXeBom.SelectedValue == "" ? (Guid?)null : GetDrop(dlXeBom);
                        thuebom.TenXeBom = dlXeBom.SelectedValue == "" ? "" : dlXeBom.SelectedItem.Text.ToString();
                        thuebom.TienHoaDon = decimal.Parse(txtTienHoaDon.Text);
                        thuebom.TienThanhToan = decimal.Parse(txtTienThanhToan.Text);
                        thuebom.PhuPhi = txtPhuPhi.Text == "" ? 0 : decimal.Parse(txtPhuPhi.Text);
                        thuebom.TrangThai = 1;
                        thuebom.TrangThaiText = "Chờ duyệt";
                        thuebom.NguoiTao = Session["IDND"].ToString();
                        thuebom.NgayTao = DateTime.Now;

                        db.tblThueBoms.InsertOnSubmit(thuebom);
                        db.SubmitChanges();

                        lblTaoMoiHopDong_Click(sender, e);
                        //Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblThueBoms
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                            //query.IDCongTrinh = GetDrop(dlCongTrinh);
                            query.HinhThucBom = GetDrop(dlHinhThucBom);
                            query.IDXeBom = dlXeBom.SelectedValue == "" ? (Guid?)null : GetDrop(dlXeBom);
                            query.TenXeBom = dlXeBom.SelectedValue == "" ? "" : dlXeBom.SelectedItem.Text.ToString();
                            query.TienHoaDon = decimal.Parse(txtTienHoaDon.Text);
                            query.TienThanhToan = decimal.Parse(txtTienThanhToan.Text);
                            query.PhuPhi = txtPhuPhi.Text == "" ? 0 : decimal.Parse(txtPhuPhi.Text);
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            //Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin phiếu xuất bê tông đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlXeBom.SelectedValue = "";
            dlChiNhanh.SelectedValue = "";
            dlNhaCungCap.SelectedValue = "";
            dlHinhThucBom.SelectedValue = "";
            hdID.Value = "";
            hdLoaiBom.Value = "";

            txtDonGiaHoaDon.Text = "";
            txtDonGiaThanhToan.Text = "";
            txtTienHoaDon.Text = "";
            txtTienThanhToan.Text = "";
            txtPhuPhi.Text = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblThueBoms
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        dlHinhThucBom.SelectedValue = query.HinhThucBom.ToString();
                        txtDonGiaHoaDon.Text = string.Format("{0:N0}", query.DonGiaHoaDon);
                        txtDonGiaThanhToan.Text = string.Format("{0:N0}", query.DonGiaThanhToan);
                        txtTienHoaDon.Text = string.Format("{0:N0}", query.TienHoaDon);
                        txtTienThanhToan.Text = string.Format("{0:N0}", query.TienThanhToan);
                        txtPhuPhi.Text = string.Format("{0:N0}", query.PhuPhi);
                        dlXeBom.SelectedValue = query.IDXeBom.ToString();
                        hdID.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai == 1)
                        {
                            int countxoa = (from p in db.tblThueBom_Logs
                                            where p.IDChung == query.ID
                                            select p).Count();
                            if (countxoa == 1)
                            {
                                db.tblThueBoms.DeleteOnSubmit(query);
                                db.SubmitChanges();
                                Success("Đã xóa");
                            }
                            else
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                        }
                        else if (query.TrangThai == 2)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                            Success("Xóa thành công");
                        }
                        else
                        {
                            Warning("Dữ liệu đang chờ duyệt xóa");
                        }
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = betong.sp_ThueBom_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
                else if (e.CommandName == "ThuTien")
                {
                    if (query.DaThuTien == "Đã thu tiền")
                    {
                        query.DaThuTien = "Chưa thu tiền";
                    }
                    else
                    {
                        query.DaThuTien = "Đã thu tiền";
                    }
                    db.SubmitChanges();
                    Search(hdPage.Value == "" ? 1 : int.Parse(hdPage.Value));
                }
            }
            else
            {
                Warning("Thông tin phiếu xuất bê tông đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[10].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Phụ phí",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tổng tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void gvLichSu_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[10].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Phụ phí",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tổng tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {

            if (txtTuNgaySearch.Text == "")
            {
                Warning("Nhập từ ngày tìm kiếm");
            }
            else if (txtDenNgaySearch.Text == "")
            {
                Warning("Nhập đến ngày tìm kiếm");
            }
            else if (DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)) > DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)))
            {
                Warning("Ngày bắt đầu tìm kiếm không được lớn hơn ngày kết thúc");
            }
            else
            {
                var query = betong.sp_ThueBom_Search(dlChiNhanhSearch.SelectedValue, dlKhachHangSearch.SelectedValue, dlBienXeBomSearch.SelectedValue,
                    DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 20, page);

                GV.DataSource = query;
                GV.DataBind();

                if (GV.Rows.Count > 0)
                {
                    sp_ThueBom_FooterResult footer = betong.sp_ThueBom_Footer(dlChiNhanhSearch.SelectedValue, dlKhachHangSearch.SelectedValue, dlBienXeBomSearch.SelectedValue,
                        DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();

                    GV.FooterRow.Cells[0].ColumnSpan = 8;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", decimal.Parse(footer.SoLuong.ToString()));
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;
                    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", decimal.Parse(footer.TienHoaDon.ToString()));
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", decimal.Parse(footer.TienThanhToan.ToString()));
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", decimal.Parse(footer.PhuPhi.ToString()));
                    GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", decimal.Parse(footer.TongHoaDon.ToString()));
                    GV.FooterRow.Cells[5].Text = string.Format("{0:N0}", decimal.Parse(footer.TongThanhToan.ToString()));

                    GV.FooterRow.Cells[6].Visible = false;
                    GV.FooterRow.Cells[7].Visible = false;
                    GV.FooterRow.Cells[8].Visible = false;
                    GV.FooterRow.Cells[9].Visible = false;
                    GV.FooterRow.Cells[10].Visible = false;
                    GV.FooterRow.Cells[11].Visible = false;
                    GV.FooterRow.Cells[12].Visible = false;
                }
            }

        }
        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        void LoadNhaCungCap()
        {
            var query = (from p in db.tblNhaCungCaps
                         select new
                         {
                             p.ID,
                             Ten = p.TenNhaCungCap
                         }).OrderBy(p => p.Ten);
            dlNhaCungCap.DataSource = query;
            dlNhaCungCap.DataBind();
            dlNhaCungCap.Items.Insert(0, new ListItem("Chọn", ""));
        }
        void LoadHinhThucBom()
        {
            var query = (from p in db.tblHinhThucBoms
                         select new
                         {
                             p.ID,
                             Ten = p.TenHinhThucBom
                         }).OrderBy(p => p.Ten);
            dlHinhThucBom.DataSource = query;
            dlHinhThucBom.DataBind();
            dlHinhThucBom.Items.Insert(0, new ListItem("Chọn", ""));
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_ThueBom_PrintResult> query = betong.sp_ThueBom_Print(dlChiNhanhSearch.SelectedValue, dlKhachHangSearch.SelectedValue, dlBienXeBomSearch.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();


            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "BÁO CÁO THUÊ BƠM";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 7);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO THUÊ BƠM TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                }
                else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO THUÊ BƠM NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 10);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 10);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Ngày tháng",
                    "Chi nhánh",
                    "Thông tin khách hàng",
                    "Thông tin khách hàng",
                    "Thông tin khách hàng",
                    "Giá thuê bơm",
                    "Giá thuê bơm",
                    "Phụ phí",
                    "Thành tiền",
                    "Thành tiền"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Ngày tháng",
                    "Chi nhánh",
                    "Khách hàng",
                    "Hình thức bơm",
                    "Xe bơm",
                    "Hóa đơn",
                    "Thanh toán",
                    "Phụ phí",
                    "Hóa đơn",
                    "Thanh toán"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 3, 5);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 6, 7);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 8, 8);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 9, 10);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.NgayThang);
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(item.TenNhaCungCap);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(item.TenHinhThucBom);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(item.TenXeBom);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(double.Parse(item.TienHoaDon.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(double.Parse(item.TienThanhToan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(double.Parse(item.PhuPhi.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(double.Parse(item.TongHoaDon.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(double.Parse(item.TongThanhToan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= header2.Length; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.CellFormula = "SUM(G" + RowDau.ToString() + ":G" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(7);
                cell.CellFormula = "SUM(H" + RowDau.ToString() + ":H" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(8);
                cell.CellFormula = "SUM(I" + RowDau.ToString() + ":I" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(9);
                cell.CellFormula = "SUM(J" + RowDau.ToString() + ":J" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(10);
                cell.CellFormula = "SUM(K" + RowDau.ToString() + ":K" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                #endregion

                #region[Set Footer]

                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 10);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;
                
                #endregion

                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO THUÊ BƠM -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                         ".xls";
                    }
                    else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO THUÊ BƠM -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                         "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }
    }
}