﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class BanVatLieu : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        clsXuLy xl = new clsXuLy();
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmBanVatLieu";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmBanVatLieu", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        txtNgayThang.Text = dateTimenow.ToString("dd/MM/yyyy");
                        LoadChiNhanh();
                        dlChiNhanh_SelectedIndexChanged(sender, e);
                        LoadXe();
                        dlChiNhanhSearch_SelectedIndexChanged(sender, e);
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            dlChiNhanhChot.DataSource = query;
            dlChiNhanhChot.DataBind();
            dlChiNhanhChot.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        void LoadXe()
        {
            dlBienSoXe.Items.Clear();
            dlBienSoXe.DataSource = from p in db.tblXeVanChuyens
                                    where p.TrangThai == 2
                                    && p.IsTrangThai == 1
                                    && (
                                    p.IDNhomThietBi == new Guid("AB7BDA38-9DA8-4796-AD2C-F73081CC61C7")
                                    || p.IDNhomThietBi == new Guid("6810B3FB-B192-449D-B9F9-3F2231A8145C")
                                    //|| p.IDNhomThietBi == new Guid("AB7BDA38-9DA8-4796-AD2C-F73081CC61C7")
                                    )
                                    orderby p.STT
                                    select new
                                    {
                                        p.ID,
                                        Ten = p.TenThietBi,
                                    };
            dlBienSoXe.DataBind();
            dlBienSoXe.Items.Insert(0, new ListItem("--Chọn --", ""));
        }
        protected void LoadNhaCungCap()
        {
            dlNhaCungCap.Items.Clear();
            dlIDHDong.Items.Clear();
            dlNhomVatLieu.Items.Clear();
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";

            if (dlChiNhanh.SelectedValue != "" && valDate(txtNgayThang.Text) == true)
            {
                var query = vatlieu.sp_BanVatLieu_LoadNhaCungCap(GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)));
                dlNhaCungCap.DataSource = query;
                dlNhaCungCap.DataBind();
                if (dlNhaCungCap.Items.Count == 1)
                {
                    LoadSoHD();
                }
                else
                {
                    dlNhaCungCap.Items.Insert(0, new ListItem("--Chọn--", ""));
                    dlNhomVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
                    dlLoaiVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
                    dlDonViTinh.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadSoHD()
        {
            dlIDHDong.Items.Clear();
            dlNhomVatLieu.Items.Clear();
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";

            if (dlNhaCungCap.SelectedValue != "" && valDate(txtNgayThang.Text) == true)
            {
                var query = vatlieu.sp_BanVatLieu_LoadSoHD(GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), DateTime.Parse(GetNgayThang(txtNgayThang.Text)));

                dlIDHDong.DataSource = query;
                dlIDHDong.DataBind();
                if (dlIDHDong.Items.Count == 1)
                {
                    LoadNhomVatLieu();
                }
                else
                {
                    dlIDHDong.Items.Insert(0, new ListItem("--Chọn--", ""));
                    dlNhomVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
                    dlLoaiVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
                    dlDonViTinh.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadNhomVatLieu()
        {
            dlNhomVatLieu.Items.Clear();
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            if (dlIDHDong.SelectedValue != "" && valDate(txtNgayThang.Text) == true)
            {
                var query = vatlieu.sp_BanVatLieu_LoadNhomVatLieu(GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlIDHDong));
                dlNhomVatLieu.DataSource = query;
                dlNhomVatLieu.DataBind();

                if (dlNhomVatLieu.Items.Count == 1)
                {
                    LoadLoaiVatLieu();
                }
                else
                {
                    dlNhomVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadLoaiVatLieu()
        {
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            if (dlNhomVatLieu.SelectedValue != "" && valDate(txtNgayThang.Text) == true)
            {
                var query = vatlieu.sp_BanVatLieu_LoadLoaiVatLieu(GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlIDHDong), GetDrop(dlNhomVatLieu));
                dlLoaiVatLieu.DataSource = query;
                dlLoaiVatLieu.DataBind();

                if (dlLoaiVatLieu.Items.Count == 1)
                {
                    LoadDonViTinh();
                }
                else
                {
                    dlLoaiVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadDonViTinh()
        {
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            if (dlLoaiVatLieu.SelectedValue != "" && valDate(txtNgayThang.Text) == true)
            {
                var query = vatlieu.sp_BanVatLieu_LoadDonViTinh(GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlIDHDong),
                    GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu));
                dlDonViTinh.DataSource = query;
                dlDonViTinh.DataBind();

                if (dlLoaiVatLieu.Items.Count == 1)
                {
                    LoadGiaMuaVatLieu();
                }
                else
                {
                    dlDonViTinh.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadGiaMuaVatLieu()
        {
            if (dlDonViTinh.SelectedValue != "" && valDate(txtNgayThang.Text) == true)
            {
                var query = vatlieu.sp_BanVatLieu_LoadGiaMuaVatLieu(GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlIDHDong),
                    GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh)).FirstOrDefault();
                if (query != null && query.DonGiaCoThue != null)
                {
                    txtDonGiaCoThue.Text = string.Format("{0:N0}", query.DonGiaCoThue);
                    txtDonGiaKhongThue.Text = string.Format("{0:N0}", query.DonGiaKhongThue);
                    if (txtKLBan.Text.Trim() != "" && decimal.Parse(txtKLBan.Text) > 0)
                    {
                        txtThanhTienCoThue.Text = string.Format("{0:N0}", query.DonGiaCoThue * decimal.Parse(txtKLBan.Text));
                        txtThanhTienKhongThue.Text = string.Format("{0:N0}", query.DonGiaKhongThue * decimal.Parse(txtKLBan.Text));
                    }
                    else
                    {
                        txtThanhTienCoThue.Text = "0";
                        txtThanhTienKhongThue.Text = "0";
                    }
                }
                else
                {
                    txtDonGiaCoThue.Text = "0";
                    txtDonGiaKhongThue.Text = "0";
                    txtThanhTienCoThue.Text = "0";
                    txtThanhTienKhongThue.Text = "0";
                }
            }
            else
            {
                txtDonGiaCoThue.Text = "0";
                txtDonGiaKhongThue.Text = "0";
                txtThanhTienCoThue.Text = "0";
                txtThanhTienKhongThue.Text = "0";
            }
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }

        protected void dlNhaCungCap_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSoHD();
        }

        protected void dlNhomVatLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiVatLieu();
        }
        protected void dlLoaiVatLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDonViTinh();
        }

        protected void dlDonViTinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGiaMuaVatLieu();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem(ref string IDHopDong)
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlNhomVatLieu.SelectedValue == "")
            {
                s += " - Chọn nhóm vật liệu<br />";
            }
            if (dlLoaiVatLieu.SelectedValue == "")
            {
                s += " - Chọn loại vật liệu<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || txtDonGiaKhongThue.Text == "" || (decimal.Parse(txtDonGiaCoThue.Text) == 0 && decimal.Parse(txtDonGiaKhongThue.Text) == 0))
            //{
            //    s += " - Nhập giá bán vật liệu<br />";
            //}
            if (txtKLBan.Text == "")
            {
                s += " - Nhập khối lượng bán<br />";
            }
            if (txtKLXuatKho.Text == "")
            {
                s += " - Nhập khối lượng xuất kho<br />";
            }
            //if (txtSoPhieuCan.Text.Trim() == "")
            //{
            //    s += " - Chọn phiếu cân";
            //}
            //if (txtTyLeQuyDoi.Text.Trim() == "" || double.Parse(txtTyLeQuyDoi.Text) == 0)
            //{
            //    s += " - Nhập tỷ lệ quy đổi";
            //}
            if (s == "")
            {
                vatlieu.sp_BanVatLieu_CheckThem(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), GetDrop(dlIDHDong),
                    GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text), decimal.Parse(txtDonGiaKhongThue.Text), ref IDHopDong, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua(ref string IDHopDong)
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlNhomVatLieu.SelectedValue == "")
            {
                s += " - Chọn nhóm vật liệu<br />";
            }
            if (dlLoaiVatLieu.SelectedValue == "")
            {
                s += " - Chọn loại vật liệu<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || txtDonGiaKhongThue.Text == "" || (decimal.Parse(txtDonGiaCoThue.Text) == 0 && decimal.Parse(txtDonGiaKhongThue.Text) == 0))
            //{
            //    s += " - Nhập giá bán vật liệu<br />";
            //}
            if (txtKLBan.Text == "")
            {
                s += " - Nhập khối lượng bán<br />";
            }
            if (txtKLXuatKho.Text == "")
            {
                s += " - Nhập khối lượng xuất kho<br />";
            }
            if (s == "")
            {
                vatlieu.sp_BanVatLieu_CheckSua(new Guid(hdID.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), GetDrop(dlIDHDong),
                    GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text), decimal.Parse(txtDonGiaKhongThue.Text), ref IDHopDong, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string idHopDong = "";
            string filename = "";
            if (hdID.Value == "")
            {
                if (CheckThem(ref idHopDong) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        Guid ID = Guid.NewGuid();

                        //if (FileUpload1.HasFile)
                        //    UploadFile(1, ID.ToString(), ref filename);

                        var BanVatLieu = new tblBanVatLieu()
                        {
                            ID = ID,
                            NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhaCungCap = GetDrop(dlNhaCungCap),
                            IDHDong = GetDrop(dlIDHDong),
                            IDNhomVatLieu = GetDrop(dlNhomVatLieu),
                            IDLoaiVatLieu = GetDrop(dlLoaiVatLieu),
                            IDDonViTinh = GetDrop(dlDonViTinh),
                            TLTong = double.Parse(txtTLTong.Text),
                            TLBi = double.Parse(txtTLBi.Text),
                            TLHang = double.Parse(txtTLHang.Text),
                            TyLeQuyDoi = txtTyLeQuyDoi.Text == "" ? 0 : double.Parse(txtTyLeQuyDoi.Text),
                            KMVanChuyen = txtKMVanChuyen.Text == "" ? 0 : double.Parse(txtKMVanChuyen.Text),
                            KLQuanCan = double.Parse(txtKLQuaCan.Text),
                            KLBan = double.Parse(txtKLBan.Text),
                            KLXuatKho = double.Parse(txtKLXuatKho.Text),
                            IDHopDong = new Guid(idHopDong),
                            IDBienSoXe = dlBienSoXe.SelectedValue == "" ? (Guid?)null : GetDrop(dlBienSoXe),
                            TenBienSoXe = dlBienSoXe.SelectedValue == "" ? "" : dlBienSoXe.SelectedItem.Text.ToUpper(),
                            TenLaiXe = txtTenLaiXe.Text.ToUpper(),
                            MoTa = txtMoTa.Text.Trim(),
                            SoPhieuCan = txtSoPhieuCan.Text == "" ? 0 : int.Parse(txtSoPhieuCan.Text),
                            DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text),
                            DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text),
                            ThanhTienCoThue = decimal.Parse(txtThanhTienCoThue.Text),
                            ThanhTienKhongThue = decimal.Parse(txtThanhTienKhongThue.Text),
                            //HinhAnh = filename,
                            Loai = 1,
                            TenLoai = "Bán vật liệu",
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblBanVatLieus.InsertOnSubmit(BanVatLieu);
                        db.SubmitChanges();

                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");

                    }
                }
            }
            else
            {
                if (CheckThem(ref idHopDong) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblBanVatLieus
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                            query.IDHDong = GetDrop(dlIDHDong);
                            query.IDNhomVatLieu = GetDrop(dlNhomVatLieu);
                            query.IDLoaiVatLieu = GetDrop(dlLoaiVatLieu);
                            query.IDDonViTinh = GetDrop(dlDonViTinh);
                            query.TLTong = double.Parse(txtTLTong.Text);
                            query.TLBi = double.Parse(txtTLBi.Text);
                            query.TLHang = double.Parse(txtTLHang.Text);
                            query.TyLeQuyDoi = txtTyLeQuyDoi.Text == "" ? 0 : double.Parse(txtTyLeQuyDoi.Text);
                            query.KMVanChuyen = txtKMVanChuyen.Text == "" ? 0 : double.Parse(txtKMVanChuyen.Text);
                            query.SoPhieuCan = txtSoPhieuCan.Text == "" ? 0 : int.Parse(txtSoPhieuCan.Text);
                            query.KLQuanCan = double.Parse(txtKLQuaCan.Text);
                            query.KLBan = double.Parse(txtKLBan.Text);
                            query.KLXuatKho = double.Parse(txtKLXuatKho.Text);
                            query.IDHopDong = new Guid(idHopDong);
                            query.IDBienSoXe = dlBienSoXe.SelectedValue == "" ? (Guid?)null : GetDrop(dlBienSoXe);
                            query.TenBienSoXe = dlBienSoXe.SelectedValue == "" ? "" : dlBienSoXe.SelectedItem.Text.ToUpper();
                            query.TenLaiXe = txtTenLaiXe.Text.ToUpper();
                            query.MoTa = txtMoTa.Text.Trim();
                            query.SoPhieuCan = txtSoPhieuCan.Text == "" ? 0 : int.Parse(txtSoPhieuCan.Text);
                            query.DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text);
                            query.DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text);

                            query.ThanhTienCoThue = decimal.Parse(txtThanhTienCoThue.Text);
                            query.ThanhTienKhongThue = decimal.Parse(txtThanhTienKhongThue.Text);

                            //if (FileUpload1.HasFile)
                            //{
                            //    UploadFile(1, hdID.Value, ref filename);
                            //    query.HinhAnh = filename;
                            //}

                            query.Loai = 1;
                            query.TenLoai = "Bán vật liệu";
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();

                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin giá bán vật liệu đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        void UploadFile(int loai, string ID, ref string filename)
        {
            //if (FileUpload1.HasFile)
            //{
            //    if (loai == 1)
            //    {
            //        string extision = FileUpload1.FileName.Trim();
            //        string time = DateTime.Now.ToString("yyyyMMddHHmmss");
            //        filename = ID.ToUpper() + "-" + time + extision.Substring(extision.LastIndexOf('.'));
            //        FileUpload1.SaveAs(Server.MapPath("~/ImageNhapKho/") + filename);
            //    }
            //}
        }
        protected void btnCan_Click(object sender, EventArgs e)
        {
            if (dlChiNhanh.SelectedValue == "")
            {
                GstGetMess("Bạn phải chọn chi nhánh.", "");
            }
            else
            {
                mpCan.Show();
                DateTime dtime = DateTime.Parse(GetNgayThang(txtNgayThang.Text));

                var query = db.sp_Can_LoadPhieuCanTheoNgay(GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)));
                gvCan.DataSource = query;
                gvCan.DataBind();
            }
        }
        protected void btnResetCan_Click(object sender, EventArgs e)
        {
            txtTLTong.Text = "0";
            txtTLBi.Text = "0";
            txtTLHang.Text = "0";
            txtKLQuaCan.Text = "0";
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlChiNhanh_SelectedIndexChanged(sender, e);
            dlBienSoXe.SelectedValue = "";
            txtTenLaiXe.Text = "";
            txtMoTa.Text = "";
            txtKLBan.Text = "0";
            txtKLXuatKho.Text = "0";
            txtTLTong.Text = "0";
            txtTLBi.Text = "0";
            txtTLHang.Text = "0";
            txtKLQuaCan.Text = "0";
            txtTyLeQuyDoi.Text = "0";
            txtKMVanChuyen.Text = "0";

            txtSoPhieuCan.Text = "";
            hdID.Value = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblBanVatLieus
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_BanVatLieu_CheckSuaGV(query.NgayThang, query.IDChiNhanh, query.IDNhaCungCap, query.ID, query.IDHopDong, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            LoadNhaCungCap();
                            dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                            LoadSoHD();
                            dlIDHDong.SelectedValue = query.IDHDong.ToString();
                            LoadNhomVatLieu();
                            dlNhomVatLieu.SelectedValue = query.IDNhomVatLieu.ToString();
                            LoadLoaiVatLieu();
                            dlLoaiVatLieu.SelectedValue = query.IDLoaiVatLieu.ToString();
                            LoadDonViTinh();
                            dlDonViTinh.SelectedValue = query.IDDonViTinh.ToString();
                            LoadGiaMuaVatLieu();
                            txtSoPhieuCan.Text = query.SoPhieuCan.ToString();
                            txtKLBan.Text = query.KLBan.ToString();
                            txtKLXuatKho.Text = query.KLXuatKho.ToString();
                            dlBienSoXe.SelectedValue = query.IDBienSoXe.ToString();
                            txtTenLaiXe.Text = query.TenLaiXe;
                            txtMoTa.Text = query.MoTa;

                            txtDonGiaCoThue.Text = string.Format("{0:N0}", query.DonGiaCoThue);
                            txtDonGiaKhongThue.Text = string.Format("{0:N0}", query.DonGiaKhongThue);
                            txtThanhTienCoThue.Text = string.Format("{0:N0}", query.ThanhTienCoThue);
                            txtThanhTienKhongThue.Text = string.Format("{0:N0}", query.ThanhTienKhongThue);

                            txtSoPhieuCan.Text = query.SoPhieuCan.ToString();
                            txtTLTong.Text = string.Format("{0:N0}", query.TLTong);
                            txtTLBi.Text = string.Format("{0:N0}", query.TLBi);
                            txtTLHang.Text = string.Format("{0:N0}", query.TLHang);
                            txtTyLeQuyDoi.Text = query.TyLeQuyDoi.ToString();
                            txtKMVanChuyen.Text = query.KMVanChuyen.ToString();
                            txtKLQuaCan.Text = string.Format("{0:N2}", query.KLQuanCan);

                            hdID.Value = id;
                            //btnSave.Text = "Cập nhật";
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThaiChot != null)
                        {
                            Warning("Thời gian này đã chốt bán vật liệu");
                        }
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblBanVatLieu_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblBanVatLieus.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_BanVatLieu_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin phiếu bán vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "khách hàng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Nhóm vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);
                headerCell = new TableCell
                {
                    Text = "Loại vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Đơn vị tính",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Biển số xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Lái xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khối lượng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Beige,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Đơn giá",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thành tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin cân",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.DarkCyan,
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void GVChot_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "khách hàng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Nhóm vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);
                headerCell = new TableCell
                {
                    Text = "Loại vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Đơn vị tính",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Biển số xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Lái xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khối lượng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Beige,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Đơn giá",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thành tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin cân",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.DarkCyan,
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }

        //protected void ResetFilter()
        //{
        //    dlNhaCungCapSearch.Items.Clear();
        //    dlNhomVatLieuSearch.Items.Clear();
        //    dlLoaiVatLieuSearch.Items.Clear();
        //    dlDonViTinhSearch.Items.Clear();
        //    List<sp_BanVatLieu_ResetFilterResult> query = vatlieu.sp_BanVatLieu_ResetFilter(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text))).ToList();

        //    dlNhaCungCapSearch.DataSource = (from p in query
        //                                     select new
        //                                     {
        //                                         ID = p.IDNhaCungCap,
        //                                         Ten = p.TenNhaCungCap
        //                                     }).Distinct().OrderBy(p => p.Ten);
        //    dlNhaCungCapSearch.DataBind();
        //    dlNhomVatLieuSearch.DataSource = (from p in query
        //                                      select new
        //                                      {
        //                                          ID = p.IDNhomVatLieu,
        //                                          Ten = p.TenNhomVatLieu
        //                                      }).Distinct().OrderBy(p => p.Ten);
        //    dlNhomVatLieuSearch.DataBind();
        //    dlLoaiVatLieuSearch.DataSource = (from p in query
        //                                      select new
        //                                      {
        //                                          ID = p.IDLoaiVatLieu,
        //                                          Ten = p.TenLoaiVatLieu
        //                                      }).Distinct().OrderBy(p => p.Ten);
        //    dlLoaiVatLieuSearch.DataBind();
        //    dlDonViTinhSearch.DataSource = (from p in query
        //                                    select new
        //                                    {
        //                                        ID = p.IDDonViTinh,
        //                                        Ten = p.TenDonViTinh
        //                                    }).Distinct().OrderBy(p => p.Ten);
        //    dlDonViTinhSearch.DataBind();
        //}

        protected void ResetFilter()
        {
            dlNhaCungCapSearch.Items.Clear();
            //dlNhomVatLieuSearch.Items.Clear();
            //dlLoaiVatLieuSearch.Items.Clear();
            //dlDonViTinhSearch.Items.Clear();
            List<sp_BanVatLieu_ResetFilterLan1Result> query = vatlieu.sp_BanVatLieu_ResetFilterLan1(GetDrop(dlChiNhanhSearch)).ToList();

            dlNhaCungCapSearch.DataSource = query;
            dlNhaCungCapSearch.DataBind();
            dlNhaCungCapSearch.Items.Insert(0, new ListItem("Tất cả", ""));
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            if (txtTuNgaySearch.Text == "")
            {
                Warning("Nhập từ ngày tìm kiếm");
            }
            else if (txtDenNgaySearch.Text == "")
            {
                Warning("Nhập đến ngày tìm kiếm");
            }
            else if (DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)) > DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)))
            {
                Warning("Ngày bắt đầu tìm kiếm không được lớn hơn ngày kết thúc");
            }
            else
            {
                var query = vatlieu.sp_BanVatLieu_Search(GetDrop(dlChiNhanhSearch), dlNhaCungCapSearch.SelectedValue,
                    DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 20, page);

                GV.DataSource = query;
                GV.DataBind();

                if (GV.Rows.Count > 0)
                {
                    sp_BanVatLieu_FooterResult footer = vatlieu.sp_BanVatLieu_Footer(GetDrop(dlChiNhanhSearch), dlNhaCungCapSearch.SelectedValue,
                        DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();

                    GV.FooterRow.Cells[0].ColumnSpan = 11;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", decimal.Parse(footer.SoLuong.ToString()));
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", decimal.Parse(footer.KLBan.ToString()));
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", decimal.Parse(footer.KLXuatKho.ToString()));
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", decimal.Parse(footer.KLQuanCan.ToString()));
                    GV.FooterRow.Cells[6].Text = string.Format("{0:N0}", decimal.Parse(footer.ThanhTienCoThue.ToString()));
                    GV.FooterRow.Cells[7].Text = string.Format("{0:N0}", decimal.Parse(footer.ThanhTienKhongThue.ToString()));
                    GV.FooterRow.Cells[8].Text = string.Format("{0:N0}", decimal.Parse(footer.TLTong.ToString()));
                    GV.FooterRow.Cells[9].Text = string.Format("{0:N0}", decimal.Parse(footer.TLBi.ToString()));
                    GV.FooterRow.Cells[10].Text = string.Format("{0:N0}", decimal.Parse(footer.TLHang.ToString()));

                    GV.FooterRow.Cells[11].Visible = false;
                    GV.FooterRow.Cells[12].Visible = false;
                    GV.FooterRow.Cells[13].Visible = false;
                    GV.FooterRow.Cells[14].Visible = false;
                    GV.FooterRow.Cells[15].Visible = false;
                    GV.FooterRow.Cells[16].Visible = false;
                    GV.FooterRow.Cells[17].Visible = false;
                    GV.FooterRow.Cells[18].Visible = false;
                    GV.FooterRow.Cells[19].Visible = false;
                    GV.FooterRow.Cells[20].Visible = false;
                }
            }
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_BanVatLieu_PrintResult> query = vatlieu.sp_BanVatLieu_Print(GetDrop(dlChiNhanhSearch), dlNhaCungCapSearch.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();


            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "BÁO CÁO BÁN VẬT LIỆU";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 7);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO BÁN VẬT LIỆU TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                }
                else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO BÁN VẬT LIỆU NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 21);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 21);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Số phiếu",
                    "Chi nhánh",
                    "Ngày tháng",
                    "khách hàng",
                    "Nhóm vật liệu",
                    "Loại vật liệu",
                    "Đơn vị tính",
                    "Biển số xe",
                    "Lái xe",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Đơn giá",
                    "Đơn giá",
                    "Thành tiền",
                    "Thành tiền",
                    "Thông tin cân",
                    "Thông tin cân",
                    "Thông tin cân",
                    "Thông tin cân",
                    "Ghi chú"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Số phiếu",
                    "Chi nhánh",
                    "Ngày tháng",
                    "khách hàng",
                    "Nhóm vật liệu",
                    "Loại vật liệu",
                    "Đơn vị tính",
                    "Biển số xe",
                    "Lái xe",
                    "KL bán",
                    "KL xuất kho",
                    "KL qua cân",
                    "Hóa đơn",
                    "Thanh toán",
                    "Hóa đơn",
                    "Thanh toán",
                    "TL tổng",
                    "TL bì",
                    "TL hàng",
                    "Tỷ lệ quy đổi",
                    "Ghi chú"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 3, 3);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 4, 4);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 5, 5);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 6, 6);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 7, 7);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 8, 8);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 9, 9);
                sheet.AddMergedRegion(cra);

                cra = new CellRangeAddress(4, 4, 10, 12);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 13, 14);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 15, 16);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 17, 20);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 21, 21);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.SoPhieu.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.TenChiNhanh.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(item.NgayThang.ToString());
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(item.TenNhaCungCap.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(item.TenNhomVatLieu.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(item.TenLoaiVatLieu.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(item.TenDonViTinh.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(item.TenBienSoXe.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(item.TenLaiXe.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(double.Parse(item.KLBan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(11);
                    cell.SetCellValue(double.Parse(item.KLXuatKho.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(12);
                    cell.SetCellValue(double.Parse(item.KLQuanCan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(13);
                    cell.SetCellValue(double.Parse(item.DonGiaCoThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(14);
                    cell.SetCellValue(double.Parse(item.DonGiaKhongThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(15);
                    cell.SetCellValue(double.Parse(item.ThanhTienCoThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(16);
                    cell.SetCellValue(double.Parse(item.ThanhTienKhongThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(17);
                    cell.SetCellValue(double.Parse(item.TLTong.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(18);
                    cell.SetCellValue(double.Parse(item.TLBi.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(19);
                    cell.SetCellValue(double.Parse(item.TLHang.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(20);
                    cell.SetCellValue(double.Parse(item.TyLeQuyDoi.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(21);
                    cell.SetCellValue(item.MoTa.ToString());
                    cell.CellStyle = styleBody;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= 21; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 8);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(9);
                cell.CellFormula = "COUNT(A" + RowDau.ToString() + ":A" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(10);
                cell.CellFormula = "SUM(K" + RowDau.ToString() + ":K" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(11);
                cell.CellFormula = "SUM(L" + RowDau.ToString() + ":L" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(12);
                cell.CellFormula = "SUM(M" + RowDau.ToString() + ":M" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(15);
                cell.CellFormula = "SUM(P" + RowDau.ToString() + ":P" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(16);
                cell.CellFormula = "SUM(Q" + RowDau.ToString() + ":Q" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(17);
                cell.CellFormula = "SUM(R" + RowDau.ToString() + ":R" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(18);
                cell.CellFormula = "SUM(S" + RowDau.ToString() + ":S" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(19);
                cell.CellFormula = "SUM(T" + RowDau.ToString() + ":T" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                #endregion

                #region[Set Footer]

                //rowIndex++;
                //rowIndex++;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo ca:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo khối:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê xe:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Không dùng bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Mua bê tông:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;
                //tên
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 1);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(0);
                cell.SetCellValue("Người xuất hàng");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(2);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 4, 5);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(4);
                cell.SetCellValue("Trưởng bộ phận");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 8);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(6);
                cell.SetCellValue("Lái xe");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 9, 10);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(9);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 11, 13);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(11);
                cell.SetCellValue("Giám sát");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                #endregion

                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO BÁN VẬT LIỆU -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                         ".xls";
                    }
                    else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO BÁN VẬT LIỆU -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                         "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }
        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            //if (divdl.Visible == true)
            //    divdl.Visible = false;
            //else
            //    divdl.Visible = true;
            //dlNhaCungCapSearch.Items.Clear();
            //dlNhomVatLieuSearch.Items.Clear();
            //dlLoaiVatLieuSearch.Items.Clear();
            //dlDonViTinhSearch.Items.Clear();
        }
        protected void gvCan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblCans
                         where p.IDMN == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Chon")
                {
                    txtTLTong.Text = query.KlCanL1 > query.KlCanL2 ? string.Format("{0:N0}", query.KlCanL1) : string.Format("{0:N0}", query.KlCanL2);
                    txtTLBi.Text = query.KlCanL1 < query.KlCanL2 ? string.Format("{0:N0}", query.KlCanL1) : string.Format("{0:N0}", query.KlCanL2);
                    txtTLHang.Text = string.Format("{0:N0}", query.TLHang);

                    //txtTenBienSoXe.Text = query.BienSoXe.ToUpper();
                    GridViewRow gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    int rowind = gvRow.RowIndex;
                    Label lblLaiXeCan = (Label)gvRow.FindControl("lblLaiXeCan");
                    txtTenLaiXe.Text = lblLaiXeCan.Text;
                    txtSoPhieuCan.Text = query.SoPhieu.ToString();
                }
            }
        }
        protected void btnChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 6, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else
                {
                    vatlieu.sp_BanVatLieu_CheckChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        vatlieu.sp_BanVatLieu_Chot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), Session["IDND"].ToString(), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }
        protected void btnMoChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 7, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh mở chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else
                {
                    vatlieu.sp_BanVatLieu_CheckMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        vatlieu.sp_BanVatLieu_MoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), Session["IDND"].ToString(), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }
        protected void btnOpenChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Chốt bán vật liệu";
            mpChot.Show();
            btnChot.Visible = true;
            btnMoChot.Visible = false;
        }
        protected void btnOpenMoChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Mở chốt bán vật liệu";
            mpChot.Show();
            btnChot.Visible = false;
            btnMoChot.Visible = true;
        }
        protected void lbtSearchChot_Click(object sender, EventArgs e)
        {
            if (btnChot.Visible == true)
            {
                List<sp_BanVatLieu_ListChotResult> query = vatlieu.sp_BanVatLieu_ListChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
                GVChot.DataSource = query;
                GVChot.DataBind();

                if (GVChot.Rows.Count > 0)
                {
                    if (query != null)
                    {
                        var valuefooter = (from p in query
                                           group p by 1 into g
                                           select new
                                           {
                                               SumTLTong = g.Sum(x => x.TLTong),
                                               SumTLBi = g.Sum(x => x.TLBi),
                                               SumTLHang = g.Sum(x => x.TLHang),
                                               SumSumDone = g.Sum(x => x.TLBi),
                                               SumKLBan = g.Sum(x => x.KLBan),
                                               SumKLXuatKho = g.Sum(x => x.KLXuatKho),
                                               SumKLQuanCan = g.Sum(x => x.KLQuanCan),
                                               SumThanhTienCoThue = g.Sum(x => x.ThanhTienCoThue),
                                               SumThanhTienKhongThue = g.Sum(x => x.ThanhTienKhongThue)
                                           }).FirstOrDefault();
                        GVChot.FooterRow.Cells[0].ColumnSpan = 5;
                        GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Count) + " phiếu bán";
                        GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                        GVChot.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.SumKLBan);
                        GVChot.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.SumKLXuatKho);
                        GVChot.FooterRow.Cells[7].Text = string.Format("{0:N0}", valuefooter.SumKLQuanCan);
                        GVChot.FooterRow.Cells[10].Text = string.Format("{0:N0}", valuefooter.SumThanhTienCoThue);
                        GVChot.FooterRow.Cells[11].Text = string.Format("{0:N0}", valuefooter.SumThanhTienKhongThue);
                        GVChot.FooterRow.Cells[12].Text = string.Format("{0:N0}", valuefooter.SumTLTong);
                        GVChot.FooterRow.Cells[13].Text = string.Format("{0:N0}", valuefooter.SumTLBi);
                        GVChot.FooterRow.Cells[14].Text = string.Format("{0:N0}", valuefooter.SumTLHang);

                        GVChot.FooterRow.Cells[15].Visible = false;
                        GVChot.FooterRow.Cells[16].Visible = false;
                        GVChot.FooterRow.Cells[17].Visible = false;
                        GVChot.FooterRow.Cells[18].Visible = false;
                        GVChot.FooterRow.Cells[19].Visible = false;
                    }
                }
            }
            else if (btnChot.Visible == false)
            {
                List<sp_BanVatLieu_ListMoChotResult> query = vatlieu.sp_BanVatLieu_ListMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
                GVChot.DataSource = query;
                GVChot.DataBind();

                if (GVChot.Rows.Count > 0)
                {
                    if (query != null)
                    {
                        var valuefooter = (from p in query
                                           group p by 1 into g
                                           select new
                                           {
                                               SumTLTong = g.Sum(x => x.TLTong),
                                               SumTLBi = g.Sum(x => x.TLBi),
                                               SumTLHang = g.Sum(x => x.TLHang),
                                               SumSumDone = g.Sum(x => x.TLBi),
                                               SumKLBan = g.Sum(x => x.KLBan),
                                               SumKLXuatKho = g.Sum(x => x.KLXuatKho),
                                               SumKLQuanCan = g.Sum(x => x.KLQuanCan),
                                               SumThanhTienCoThue = g.Sum(x => x.ThanhTienCoThue),
                                               SumThanhTienKhongThue = g.Sum(x => x.ThanhTienKhongThue)
                                           }).FirstOrDefault();
                        GVChot.FooterRow.Cells[0].ColumnSpan = 5;
                        GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Count) + " phiếu bán";
                        GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                        GVChot.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.SumKLBan);
                        GVChot.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.SumKLXuatKho);
                        GVChot.FooterRow.Cells[7].Text = string.Format("{0:N0}", valuefooter.SumKLQuanCan);
                        GVChot.FooterRow.Cells[10].Text = string.Format("{0:N0}", valuefooter.SumThanhTienCoThue);
                        GVChot.FooterRow.Cells[11].Text = string.Format("{0:N0}", valuefooter.SumThanhTienKhongThue);
                        GVChot.FooterRow.Cells[12].Text = string.Format("{0:N0}", valuefooter.SumTLTong);
                        GVChot.FooterRow.Cells[13].Text = string.Format("{0:N0}", valuefooter.SumTLBi);
                        GVChot.FooterRow.Cells[14].Text = string.Format("{0:N0}", valuefooter.SumTLHang);


                        GVChot.FooterRow.Cells[15].Visible = false;
                        GVChot.FooterRow.Cells[16].Visible = false;
                        GVChot.FooterRow.Cells[17].Visible = false;
                        GVChot.FooterRow.Cells[18].Visible = false;
                        GVChot.FooterRow.Cells[19].Visible = false;
                    }
                }
            }
            mpChot.Show();
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFilter();
        }
        protected void dlIDHDong_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhomVatLieu();
        }
    }
}