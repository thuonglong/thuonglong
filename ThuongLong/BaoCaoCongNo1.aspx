﻿<%@ Page Title="Báo cáo công nợ" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BaoCaoCongNo1.aspx.cs" Inherits="ThuongLong.BaoCaoCongNo1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách công nợ</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Nhà cung cấp</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCapSearch"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3" id="div1" runat="server" visible="false">
                            <label for="exampleInputEmail1">Hình thức</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDuNo" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                                <asp:ListItem Value="0">Tất cả</asp:ListItem>
                                <asp:ListItem Value="1">Hết dư nợ</asp:ListItem>
                                <asp:ListItem Value="2">Còn dư nợ</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-6">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-blue" OnClick="btnPrint_Click" Visible="false"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <div class="box box-success">
                                <%--<div class="box-header with-border">
                                    <h3 class="box-title">Tổng</h3>
                                </div>--%>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group  col-lg-2">
                                            <label>Công nợ thu</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtCongNoThu" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group  col-lg-2">
                                            <label>Công nợ trả</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtCongNoTra" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group  col-lg-2">
                                            <label>Số tiền đã thu</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtSoTienThu" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group  col-lg-2">
                                            <label>Số tiền đã trả</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtSoTienTra" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group  col-lg-4">
                                            <label>Còn lại</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtConLai" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>

                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" ShowFooter="true"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <%--<asp:TemplateField HeaderText="Xem" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtHinhAnh" runat="server" class="btn btn-small bg-green" CommandArgument='<%#Eval("ID")%>' ToolTip="Xem" CommandName="HinhAnh"><i class="fa fa-image"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-primary" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-red" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Nhà cung cấp" />
                                    <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />
                                    <asp:BoundField DataField="Thu" HeaderText="Công nợ thu" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Tra" HeaderText="Công nợ trả" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ThanhToanThu" HeaderText="Số tiền đã thu" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ThanhToanTra" HeaderText="Số tiền đã trả" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <%--<asp:BoundField DataField='<%# Eval("SumTruocThu") + Eval("SumTruocTra") %>' HeaderText="Công nợ phải trả 1" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField='<%# Eval("SumTruocThu") + Eval("SumTruocTra") %>' HeaderText="Công nợ phải trả 2" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />--%>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdHinhAnh" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="NgayNhan" HeaderText="Ngày nhận" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="NhaCungCap" HeaderText="Nhà cung cấp" />
                                <asp:BoundField DataField="TenHoaDon" HeaderText="Tên công nợ" />
                                <asp:BoundField DataField="HoaDonPhaiThu" HeaderText="Công nợ phải thu" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="HoaDonPhaiTra" HeaderText="Công nợ phải trả" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpHinhAnh" runat="server" CancelControlID="btnDongHinhAnh"
                Drag="True" TargetControlID="hdHinhAnh" BackgroundCssClass="modalBackground" PopupControlID="pnHinhAnh"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnHinhAnh" runat="server" Style="width: 98%; height: 100%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Hình ảnh
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:Image ID="imgHinhAnh" runat="server" ImageUrl="" />
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongHinhAnh" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>

            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
