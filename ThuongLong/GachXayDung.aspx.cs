﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class GachXayDung : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmSanXuatGach";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmGachXayDung", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        txtNgayThang.Text = dateTimenow.ToString("dd/MM/yyyy");
                        LoadChiNhanh();
                        LoadLoaiGach();
                        LoadLoaiVatLieu();
                        //dlChiNhanh_SelectedIndexChanged(sender, e);
                        //LoadChiNhanhSearch();
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            dlChiNhanhChot.DataSource = query;
            dlChiNhanhChot.DataBind();
            dlChiNhanhChot.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }

        protected void LoadLoaiVatLieu()
        {
            var qlCatSongDa1 = (from p in db.tblLoaiVatLieus
                                where p.TrangThai == 2
                                && p.IDNhomVatLieu == new Guid("8B27D124-A687-400E-89F6-E3114A08CF87")
                                select new
                                {
                                    p.ID,
                                    Ten = p.TenLoaiVatLieu
                                }).OrderBy(p => p.Ten);

            dlCat.DataSource = qlCatSongDa1;
            dlCat.DataBind();
            dlCat.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlBotMau = (from p in db.tblLoaiVatLieus
                            where p.TrangThai == 2
                            && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                            select new
                            {
                                p.ID,
                                Ten = p.TenLoaiVatLieu
                            }).OrderBy(p => p.Ten);

            dlDaMat.DataSource = qlBotMau;
            dlDaMat.DataBind();
            dlDaMat.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlKeoBong = (from p in db.tblLoaiVatLieus
                             where p.TrangThai == 2
                             && p.IDNhomVatLieu == new Guid("AC14CAFA-F77A-4F9B-8E06-6D19D7202AEC")
                             select new
                             {
                                 p.ID,
                                 Ten = p.TenLoaiVatLieu
                             }).OrderBy(p => p.Ten);

            dlVLKhac.DataSource = qlKeoBong;
            dlVLKhac.DataBind();
            dlVLKhac.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlXiMangPCB401 = (from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                                  && p.IDNhomVatLieu == new Guid("36FCDA11-FEF0-43C5-8AB8-C55F24281FF4")
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu
                                  }).OrderBy(p => p.Ten);

            dlXiMang.DataSource = qlXiMangPCB401;
            dlXiMang.DataBind();
            dlXiMang.Items.Insert(0, new ListItem("--Chọn--", ""));

            //dlXiMangPCB40.Items.Insert(0, new ListItem("Xi măng PCB 40", "840482F9-D5F6-4926-B6F1-77657A7A95D6"));
            //dlCatSongDa.Items.Insert(0, new ListItem("Cát sông đà", "51C056B4-DB1F-4024-817F-6E08F9EBF1F0"));
            //dlDaMat.Items.Insert(0, new ListItem("Mạt đá", "7C716F3F-6262-4621-8452-9F8F43D02411"));
            //dlVatLieuKhac.Items.Insert(0, new ListItem("Vật liệu khác", "EC0AA0AB-697F-4449-8E84-CF79E20C8E0C"));
        }
        protected void LoadLoaiGach()
        {
            dlLoaiGach.Items.Clear();

            var query = (from p in db.tblLoaiVatLieus
                         where p.TrangThai == 2
                         && p.IDNhomVatLieu == new Guid("97CF73BB-E552-4A07-9F8B-5DD5C68F7287")
                         select new
                         {
                             p.ID,
                             Ten = p.TenLoaiVatLieu
                         }).OrderBy(p => p.Ten);

            dlLoaiGach.DataSource = query;
            dlLoaiGach.DataBind();
            dlLoaiGach.Items.Insert(0, new ListItem("--Chọn--", ""));
        }

        //protected void LoadChiNhanhSearch()
        //{
        //    var query = db.sp_LoadChiNhanhDaiLy();
        //    dlChiNhanhSearch.DataSource = query;
        //    dlChiNhanhSearch.DataBind();
        //}
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            double haohut = txtSoLuongHaoHut.Text.Trim() == "" ? 0 : double.Parse(txtSoLuongHaoHut.Text.Trim());
            if (dlChiNhanh.SelectedValue != "" && dlLoaiGach.SelectedValue != "")
            {
                var query = (from p in db.tblCaiDatGachXayDungs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiGach)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    dlXiMang.SelectedValue = query.XiMangPCB40.ToString();
                    dlCat.SelectedValue = query.CatSongDa.ToString();
                    dlDaMat.SelectedValue = query.DaMat.ToString();
                    dlVLKhac.SelectedValue = query.VatLieuKhac.ToString();

                    txtDMXiMang.Text = query.DMXiMangPCB40.ToString();
                    txtDMCat.Text = query.DMCatSongDa.ToString();
                    txtDMDaMat.Text = query.DMDaMat.ToString();
                    txtDMVLKhac.Text = query.DMVatLieuKhac.ToString();

                    if (txtSoLuong.Text.Trim() != "" && double.Parse(txtSoLuong.Text.Trim()) > 0)
                    {
                        txtKLXiMang.Text = string.Format("{0:N0}", double.Parse(txtDMXiMang.Text) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtKLCat.Text = string.Format("{0:N0}", double.Parse(txtDMCat.Text) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtKLDaMat.Text = string.Format("{0:N0}", double.Parse(txtDMDaMat.Text) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtKLVLKhac.Text = string.Format("{0:N0}", double.Parse(txtDMVLKhac.Text) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                    }
                    else
                    {
                        txtKLXiMang.Text = "";
                        txtKLCat.Text = "";
                        txtKLDaMat.Text = "";
                        txtKLVLKhac.Text = "";
                    }
                }
                else
                {
                    dlXiMang.SelectedValue = "";
                    dlCat.SelectedValue = "";
                    dlDaMat.SelectedValue = "";
                    dlVLKhac.SelectedValue = "";

                    txtKLXiMang.Text = "";
                    txtKLCat.Text = "";
                    txtKLDaMat.Text = "";
                    txtKLVLKhac.Text = "";
                    Warning("Bạn chưa thiết lập định mức cho loại gạch vừa chọn");
                }
            }
            else
            {
                dlXiMang.SelectedValue = "";
                dlCat.SelectedValue = "";
                dlDaMat.SelectedValue = "";
                dlVLKhac.SelectedValue = "";

                txtSoLuong.Text = "";
                txtKLXiMang.Text = "";
                txtKLCat.Text = "";
                txtKLDaMat.Text = "";
                txtKLVLKhac.Text = "";

            }
        }
        protected void txtSoLuong_TextChanged(object sender, EventArgs e)
        {
            double haohut = txtSoLuongHaoHut.Text.Trim() == "" ? 0 : double.Parse(txtSoLuongHaoHut.Text.Trim());
            if (txtDMXiMang.Text != "" && dlChiNhanh.SelectedValue != "" && dlLoaiGach.SelectedValue != "" && txtSoLuong.Text.Trim() != "" && double.Parse(txtSoLuong.Text.Trim()) > 0)
            {
                txtKLXiMang.Text = string.Format("{0:N0}", double.Parse(txtDMXiMang.Text) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtKLCat.Text = string.Format("{0:N0}", double.Parse(txtDMCat.Text) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtKLDaMat.Text = string.Format("{0:N0}", double.Parse(txtDMDaMat.Text) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtKLVLKhac.Text = string.Format("{0:N0}", double.Parse(txtDMVLKhac.Text) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
            }
            else
            {
                txtKLXiMang.Text = "";
                txtKLCat.Text = "";
                txtKLDaMat.Text = "";
                txtKLVLKhac.Text = "";
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }
            if (dlLoaiGach.SelectedValue == "")
            {
                s += " - Chọn loại gạch sản xuất";
            }

            if (txtSoLuong.Text == "")
            {
                s += " - Nhập số lượng đạt<br />";
            }
            if (txtSoLuongHaoHut.Text == "")
            {
                s += " - Nhập số lượng hao hụt<br />";
            }
            //if (txtKLXiMang.Text == "")
            //{
            //    s += " - Nhập xi măng<br />";
            //}
            //if (txtKLCat.Text == "")
            //{
            //    s += " - Nhập cát<br />";
            //}
            //if (txtKLDaMat.Text == "")
            //{
            //    s += " - Nhập đá mạt<br />";
            //}
            //if (txtKLVLKhac.Text == "")
            //{
            //    s += " - Nhập vật liệu khác<br />";
            //}

            if (s == "")
            {
                vatlieu.sp_GachXayDung_CheckThem(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlLoaiGach), int.Parse(dlThoiTiet.SelectedValue), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }
            if (dlLoaiGach.SelectedValue == "")
            {
                s += " - Chọn loại gạch sản xuất<br />";
            }
            if (txtSoLuong.Text == "")
            {
                s += " - Nhập số lượng đạt<br />";
            }
            if (txtSoLuongHaoHut.Text == "")
            {
                s += " - Nhập số lượng hao hụt<br />";
            }
            //if (txtKLXiMang.Text == "")
            //{
            //    s += " - Nhập xi măng<br />";
            //}
            //if (txtKLCat.Text == "")
            //{
            //    s += " - Nhập cát<br />";
            //}
            //if (txtKLDaMat.Text == "")
            //{
            //    s += " - Nhập đá mạt<br />";
            //}
            //if (txtKLVLKhac.Text == "")
            //{
            //    s += " - Nhập vật liệu khác<br />";
            //}

            if (s == "")
            {
                vatlieu.sp_GachXayDung_CheckSua(new Guid(hdID.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        Guid ID = Guid.NewGuid();
                        //if (FileUpload1.HasFile)
                        //    UploadFile(1, ID.ToString(), ref filename);

                        var GachXayDung = new tblGachXayDung()
                        {
                            ID = ID,
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),

                            XiMang = GetDrop(dlXiMang),
                            Cat = GetDrop(dlCat),
                            DaMat = GetDrop(dlDaMat),
                            VLKhac = GetDrop(dlVLKhac),

                            TenXiMang = dlXiMang.SelectedItem.Text.ToString(),
                            TenCat = dlCat.SelectedItem.Text.ToString(),
                            TenDaMat = dlDaMat.SelectedItem.Text.ToString(),
                            TenVLKhac = dlVLKhac.SelectedItem.Text.ToString(),

                            DMXiMang = double.Parse(txtDMXiMang.Text.Trim()),
                            DMCat = double.Parse(txtDMCat.Text.Trim()),
                            DMDaMat = double.Parse(txtDMDaMat.Text.Trim()),
                            DMVLKhac = double.Parse(txtDMVLKhac.Text.Trim()),

                            KLXiMang = txtKLXiMang.Text == "" ? 0 : double.Parse(txtKLXiMang.Text.Trim()),
                            KLCat = txtKLCat.Text == "" ? 0 : double.Parse(txtKLCat.Text.Trim()),
                            KLDaMat = txtKLDaMat.Text == "" ? 0 : double.Parse(txtKLDaMat.Text.Trim()),
                            KLVLKhac = txtKLVLKhac.Text == "" ? 0 : double.Parse(txtKLVLKhac.Text.Trim()),

                            ThoiTiet = int.Parse(dlThoiTiet.SelectedValue),
                            SoLuong = txtSoLuong.Text == "" ? 0 : int.Parse(txtSoLuong.Text.Trim()),
                            SoLuongHaoHut = txtSoLuongHaoHut.Text == "" ? 0 : int.Parse(txtSoLuongHaoHut.Text.Trim()),
                            LoaiGach = GetDrop(dlLoaiGach),
                            TenLoaiGach = dlLoaiGach.SelectedItem.Text.ToString(),
                            GhiChu = txtGhiChu.Text.Trim(),
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblGachXayDungs.InsertOnSubmit(GachXayDung);
                        db.SubmitChanges();

                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        GstGetMess("Lưu thành công.", "");

                        //GstGetMess("Lưu thành công.", "");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblGachXayDungs
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));

                            query.XiMang = GetDrop(dlXiMang);
                            query.Cat = GetDrop(dlCat);
                            query.DaMat = GetDrop(dlDaMat);
                            query.VLKhac = GetDrop(dlVLKhac);

                            query.TenXiMang = dlXiMang.SelectedItem.Text.ToString();
                            query.TenCat = dlCat.SelectedItem.Text.ToString();
                            query.TenDaMat = dlDaMat.SelectedItem.Text.ToString();
                            query.TenVLKhac = dlVLKhac.SelectedItem.Text.ToString();

                            query.DMXiMang = double.Parse(txtDMXiMang.Text.Trim());
                            query.DMCat = double.Parse(txtDMCat.Text.Trim());
                            query.DMDaMat = double.Parse(txtDMDaMat.Text.Trim());
                            query.DMVLKhac = double.Parse(txtDMVLKhac.Text.Trim());

                            query.KLXiMang = txtKLXiMang.Text == "" ? 0 : double.Parse(txtKLXiMang.Text.Trim());
                            query.KLCat = txtKLCat.Text == "" ? 0 : double.Parse(txtKLCat.Text.Trim());
                            query.KLDaMat = txtKLDaMat.Text == "" ? 0 : double.Parse(txtKLDaMat.Text.Trim());
                            query.KLVLKhac = txtKLVLKhac.Text == "" ? 0 : double.Parse(txtKLVLKhac.Text.Trim());

                            query.ThoiTiet = int.Parse(dlThoiTiet.SelectedValue);
                            query.SoLuong = txtSoLuong.Text == "" ? 0 : int.Parse(txtSoLuong.Text.Trim());
                            query.SoLuongHaoHut = txtSoLuongHaoHut.Text == "" ? 0 : int.Parse(txtSoLuongHaoHut.Text.Trim());

                            query.LoaiGach = GetDrop(dlLoaiGach);
                            query.TenLoaiGach = dlLoaiGach.SelectedItem.Text.ToString();
                            query.GhiChu = txtGhiChu.Text.Trim();

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";

                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();

                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            GstGetMess("Sửa thành công", "");
                        }
                        else
                        {
                            GstGetMess("Thông tin gạch xây dựng đã bị xóa.", "");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        void UploadFile(int loai, string ID, ref string filename)
        {
            //if (FileUpload1.HasFile)
            //{
            //    if (loai == 1)
            //    {
            //        string extision = FileUpload1.FileName.Trim();
            //        string time = DateTime.Now.ToString("yyyyMMddHHmmss");
            //        filename = ID.ToUpper() + "-" + time + extision.Substring(extision.LastIndexOf('.'));
            //        FileUpload1.SaveAs(Server.MapPath("~/ImageNhapKho/") + filename);
            //    }
            //}
        }
        protected void btnCan_Click(object sender, EventArgs e)
        {

        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlChiNhanh_SelectedIndexChanged(sender, e);

            txtSoLuong.Text = "";
            txtSoLuongHaoHut.Text = "";
            txtKLXiMang.Text = "";
            txtKLCat.Text = "";
            txtKLDaMat.Text = "";
            txtKLVLKhac.Text = "";

            hdID.Value = "";
            dlChiNhanh.Enabled = true;
            dlThoiTiet.Enabled = true;
            dlLoaiGach.Enabled = true;
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblGachXayDungs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (query.TrangThaiChot != null)
                    GstGetMess("Thời gian này đã chốt nhập gạch xây dựng, không thể sửa xóa.", "");
                else if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        vatlieu.sp_GachXayDung_CheckSuaGV(query.NgayThang, query.IDChiNhanh, ref thongbao);
                        if (thongbao != "")
                            GstGetMess(thongbao, "");
                        else
                        {
                            txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            dlLoaiGach.SelectedValue = query.LoaiGach.ToString();
                            dlChiNhanh_SelectedIndexChanged(sender, e);
                            txtKLXiMang.Text = query.KLXiMang.ToString();
                            txtKLCat.Text = query.KLCat.ToString();
                            txtKLDaMat.Text = query.KLDaMat.ToString();
                            txtKLVLKhac.Text = query.KLVLKhac.ToString();
                            txtSoLuong.Text = query.SoLuong.ToString();
                            txtSoLuongHaoHut.Text = query.SoLuongHaoHut.ToString();
                            dlThoiTiet.SelectedValue = query.ThoiTiet.ToString();
                            dlLoaiGach.SelectedValue = query.LoaiGach.ToString();
                            txtGhiChu.Text = query.GhiChu;
                            hdID.Value = id;
                            dlChiNhanh.Enabled = false;
                            dlThoiTiet.Enabled = false;
                            dlLoaiGach.Enabled = false;
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        if (query.TrangThaiChot != null)
                        {
                            Warning("Thời gian này đã chốt nhập kho vật liệu");
                        }
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblGachXayDung_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblGachXayDungs.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_GachXayDung_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                GstGetMess("Thông tin gạch xây dựng đã bị xóa.", "");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[12].Visible = false;

                //e.Row.Cells[5].Visible = false;
                //e.Row.Cells[6].Visible = false;
                //e.Row.Cells[7].Visible = false;
                //e.Row.Cells[8].Visible = false;
                //e.Row.Cells[9].Visible = false;
                //e.Row.Cells[10].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thời tiết",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thành phẩm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.DarkCyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Nguyên liệu sản xuất(kg)",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ghi chú",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void GVChot_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[10].Visible = false;

                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thành phẩm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.DarkCyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Nguyên liệu sản xuất(kg)",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ghi chú",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {

            List<sp_GachXayDung_SearchResult> query = vatlieu.sp_GachXayDung_Search(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), page).ToList();

            GV.DataSource = query;
            GV.DataBind();

            if (GV.Rows.Count > 0)
            {
                if (query != null)
                {
                    var valuefooter = vatlieu.sp_GachXayDung_GetFooter(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();
                    GV.FooterRow.Cells[0].ColumnSpan = 7;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", valuefooter.SoDong);
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", valuefooter.SoLuong);
                    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", valuefooter.SoLuongHaoHut);
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", valuefooter.KLXiMang);
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", valuefooter.KLCat);
                    GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", valuefooter.KLDaMat);
                    GV.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.KLVLKhac);

                    GV.FooterRow.Cells[6].Visible = false;
                    GV.FooterRow.Cells[7].Visible = false;
                    GV.FooterRow.Cells[8].Visible = false;
                    GV.FooterRow.Cells[9].Visible = false;
                    GV.FooterRow.Cells[10].Visible = false;
                    GV.FooterRow.Cells[11].Visible = false;
                }
            }
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //List<sp_GachXayDung_PrintResult> query = vatlieu.sp_GachXayDung_Print(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();

            //if (query.Count > 0)
            //{
            //    string dc = "", tencongty = "", sdt = "";
            //    var tencty = (from x in db.tblThamSos
            //                  select x).FirstOrDefault();
            //    if (tencty != null && tencty.ID != null)
            //    {
            //        dc = tencty.DiaChi;
            //        sdt = tencty.SoDienThoai;
            //        tencongty = tencty.TenCongTy;
            //    }


            //    var workbook = new HSSFWorkbook();
            //    IDataFormat dataFormatCustom = workbook.CreateDataFormat();
            //    string sheetname = "BÁO CÁO SẢN XUẤT GẠCH XÂY DỰNG";

            //    #region[CSS]

            //    var sheet = workbook.CreateSheet(sheetname);
            //    sheet = xl.SetPropertySheet(sheet);

            //    IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
            //    IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
            //    IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
            //    IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

            //    ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
            //    ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
            //    ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
            //    ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
            //    ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
            //    ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
            //    ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
            //    ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

            //    ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
            //    ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

            //    ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
            //    ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

            //    ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
            //    ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
            //    ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
            //    #endregion

            //    #region[Tạo header trên]

            //    var rowIndex = 0;
            //    var row = sheet.CreateRow(rowIndex);
            //    ICell r1c1 = row.CreateCell(0);
            //    r1c1.SetCellValue(tencongty);
            //    r1c1.CellStyle = styleboldcenternoborder;
            //    r1c1.Row.Height = 400;
            //    CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
            //    sheet.AddMergedRegion(cra);
            //    rowIndex++;
            //    row = sheet.CreateRow(rowIndex);
            //    r1c1 = row.CreateCell(0);
            //    r1c1.SetCellValue(dc);
            //    r1c1.CellStyle = styleboldcenternoborder;
            //    r1c1.Row.Height = 400;
            //    cra = new CellRangeAddress(1, 1, 0, 7);
            //    sheet.AddMergedRegion(cra);
            //    //Tiêu đề báo cáo

            //    rowIndex++;
            //    row = sheet.CreateRow(rowIndex);
            //    r1c1 = row.CreateCell(0);
            //    r1c1.SetCellValue("CHI NHÁNH " + dlChiNhanhSearch.SelectedItem.Text.ToString().ToUpper());

            //    r1c1.CellStyle = styleHeader1;
            //    r1c1.Row.Height = 600;
            //    cra = new CellRangeAddress(2, 2, 0, 9);
            //    sheet.AddMergedRegion(cra);

            //    rowIndex++;
            //    row = sheet.CreateRow(rowIndex);
            //    r1c1 = row.CreateCell(0);

            //    if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
            //    {
            //        r1c1.SetCellValue("BÁO CÁO NHẬP GẠCH XÂY DỰNG TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
            //                          "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
            //    }
            //    else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
            //    {
            //        r1c1.SetCellValue("BÁO CÁO NHẬP GẠCH XÂY DỰNG NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
            //    }

            //    r1c1.CellStyle = styleHeader2;
            //    r1c1.Row.Height = 400;
            //    cra = new CellRangeAddress(3, 3, 0, 9);
            //    sheet.AddMergedRegion(cra);

            //    rowIndex++;
            //    row = sheet.CreateRow(rowIndex);

            //    r1c1 = row.CreateCell(0);
            //    string kq = "";

            //    r1c1.SetCellValue(kq);



            //    r1c1.CellStyle = styleHeader2;
            //    r1c1.Row.Height = 500;
            //    cra = new CellRangeAddress(3, 3, 0, 9);
            //    sheet.AddMergedRegion(cra);
            //    sheet.CreateFreezePane(0, 6);
            //    #endregion

            //    #region[Tạo header dưới]
            //    rowIndex++;
            //    cra = new CellRangeAddress(4, 4, 0, 9);
            //    sheet.AddMergedRegion(cra);

            //    row = sheet.CreateRow(rowIndex);
            //    //for (int i = 1; i <= 60; i++)
            //    //{
            //    //    cra = new CellRangeAddress(4, 5, i, i);
            //    //    sheet.AddMergedRegion(cra);
            //    //}

            //    sheet.AddMergedRegion(cra);
            //    string[] header1 =
            //      {
            //        "STT",
            //        "Chi nhánh",
            //        "Ngày tháng",
            //        "Nguyên liệu sản xuất(kg)",
            //        "Nguyên liệu sản xuất(kg)",
            //        "Nguyên liệu sản xuất(kg)",
            //        "Nguyên liệu sản xuất(kg)",
            //        "Thành phẩm(viên)",
            //        "Thành phẩm(viên)"
            //      };

            //    for (int h = 0; h < header1.Length; h++)
            //    {
            //        r1c1 = row.CreateCell(h);
            //        r1c1.SetCellValue(header1[h].ToString());
            //        r1c1.CellStyle = styleHeaderChiTietCenter;
            //        r1c1.Row.Height = 500;
            //    }

            //    rowIndex++;
            //    row = sheet.CreateRow(rowIndex);

            //    string[] header2 =
            //        {
            //        "Chi nhánh",
            //        "Ngày tháng",
            //        "Xi măng",
            //        "Cát",
            //        "Đá mạt",
            //        "Vật liệu khác",
            //        "Loại gạch",
            //        "Số lượng"
            //    };
            //    var cell = row.CreateCell(0);

            //    cell.SetCellValue("STT");
            //    cell.CellStyle = styleHeaderChiTietCenter;
            //    cell.Row.Height = 600;

            //    cra = new CellRangeAddress(5, 6, 0, 0);
            //    sheet.AddMergedRegion(cra);
            //    cra = new CellRangeAddress(5, 6, 1, 1);
            //    sheet.AddMergedRegion(cra);
            //    cra = new CellRangeAddress(5, 6, 2, 2);
            //    sheet.AddMergedRegion(cra);
            //    cra = new CellRangeAddress(5, 5, 3, 7);
            //    sheet.AddMergedRegion(cra);

            //    cra = new CellRangeAddress(5, 5, 8, 11);
            //    sheet.AddMergedRegion(cra);
            //    cra = new CellRangeAddress(5, 5, 12, 9);
            //    sheet.AddMergedRegion(cra);

            //    for (int hk = 0; hk < header2.Length; hk++)
            //    {
            //        r1c1 = row.CreateCell(hk + 1);
            //        r1c1.SetCellValue(header2[hk].ToString());
            //        r1c1.CellStyle = styleHeaderChiTietCenter;
            //        r1c1.Row.Height = 800;
            //    }
            //    #endregion

            //    #region[ghi dữ liệu]
            //    int STT2 = 0;

            //    string RowDau = (rowIndex + 2).ToString();
            //    foreach (var item in query)
            //    {
            //        STT2++;
            //        rowIndex++;
            //        row = sheet.CreateRow(rowIndex);

            //        cell = row.CreateCell(0);
            //        cell.SetCellValue(STT2);
            //        cell.CellStyle = styleBodyIntCenter;

            //        cell = row.CreateCell(1);
            //        cell.SetCellValue(item.NgayThang);
            //        cell.CellStyle = styleBodyDatetime;

            //        cell = row.CreateCell(2);
            //        cell.SetCellValue(item.TenChiNhanh);
            //        cell.CellStyle = styleBody;

            //        cell = row.CreateCell(3);
            //        cell.SetCellValue(double.Parse(item.SoMeTron.ToString()));
            //        cell.CellStyle = styleBodyIntRight;

            //        cell = row.CreateCell(4);
            //        cell.SetCellValue(double.Parse(item.KLXiMang.ToString()));
            //        cell.CellStyle = styleBodyIntRight;

            //        cell = row.CreateCell(5);
            //        cell.SetCellValue(double.Parse(item.KLCat.ToString()));
            //        cell.CellStyle = styleBodyIntRight;

            //        cell = row.CreateCell(6);
            //        cell.SetCellValue(double.Parse(item.KLDaMat.ToString()));
            //        cell.CellStyle = styleBodyIntRight;

            //        cell = row.CreateCell(7);
            //        cell.SetCellValue(double.Parse(item.KLVLKhac.ToString()));
            //        cell.CellStyle = styleBodyIntRight;

            //        cell = row.CreateCell(8);
            //        cell.SetCellValue(item.TenLoaiVatLieu);
            //        cell.CellStyle = styleBody;

            //        cell = row.CreateCell(9);
            //        cell.SetCellValue(double.Parse(item.SoLuong.ToString()));
            //        cell.CellStyle = styleBodyIntRight;

            //    }
            //    #endregion

            //    #region Tổng cộng
            //    rowIndex++;
            //    string RowCuoi = rowIndex.ToString();
            //    row = sheet.CreateRow(rowIndex);
            //    for (int i = 1; i <= 9; i++)
            //    {
            //        cell = row.CreateCell(i);
            //        cell.CellStyle = styleBodyIntRight;
            //        cell.Row.Height = 400;
            //    }
            //    cell = row.CreateCell(0);
            //    cell.SetCellValue("Tổng cộng:");
            //    cell.Row.Height = 400;
            //    cell.CellStyle = styleboldrightborder;

            //    cra = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
            //    sheet.AddMergedRegion(cra);

            //    cell = row.CreateCell(3);
            //    cell.CellFormula = "SUM(D" + RowDau.ToString() + ":D" + RowCuoi.ToString() + ")";
            //    cell.CellStyle = styleBodyIntFooterRight;

            //    cell = row.CreateCell(4);
            //    cell.CellFormula = "SUM(E" + RowDau.ToString() + ":E" + RowCuoi.ToString() + ")";
            //    cell.CellStyle = styleBodyIntRight;

            //    cell = row.CreateCell(5);
            //    cell.CellFormula = "SUM(F" + RowDau.ToString() + ":F" + RowCuoi.ToString() + ")";
            //    cell.CellStyle = styleBodyIntRight;

            //    cell = row.CreateCell(6);
            //    cell.CellFormula = "SUM(G" + RowDau.ToString() + ":G" + RowCuoi.ToString() + ")";
            //    cell.CellStyle = styleBodyIntRight;

            //    cell = row.CreateCell(7);
            //    cell.CellFormula = "SUM(H" + RowDau.ToString() + ":H" + RowCuoi.ToString() + ")";
            //    cell.CellStyle = styleBodyIntRight;

            //    cell = row.CreateCell(8);
            //    cell.CellFormula = "SUM(I" + RowDau.ToString() + ":I" + RowCuoi.ToString() + ")";
            //    cell.CellStyle = styleBodyIntRight;

            //    cell = row.CreateCell(9);
            //    cell.CellFormula = "SUM(J" + RowDau.ToString() + ":J" + RowCuoi.ToString() + ")";
            //    cell.CellStyle = styleBodyIntRight;

            //    #endregion

            //    #region[Set Footer]

            //    //rowIndex++;
            //    //rowIndex++;

            //    //rowIndex++;
            //    //row = sheet.CreateRow(rowIndex);

            //    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
            //    //sheet.AddMergedRegion(cra);
            //    //cell = row.CreateCell(3);
            //    //cell.SetCellValue("Bơm theo ca:");
            //    //cell.CellStyle = styleFooterText;

            //    //cell = row.CreateCell(6);
            //    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
            //    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
            //    //sheet.AddMergedRegion(cra);
            //    //cell = row.CreateCell(8);
            //    //cell.SetCellValue("Thuê bơm:");
            //    //cell.CellStyle = styleFooterText;

            //    //cell = row.CreateCell(12);
            //    //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
            //    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //    //rowIndex++;
            //    //row = sheet.CreateRow(rowIndex);

            //    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
            //    //sheet.AddMergedRegion(cra);
            //    //cell = row.CreateCell(3);
            //    //cell.SetCellValue("Bơm theo khối:");
            //    //cell.CellStyle = styleFooterText;

            //    //cell = row.CreateCell(6);
            //    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
            //    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
            //    //sheet.AddMergedRegion(cra);
            //    //cell = row.CreateCell(8);
            //    //cell.SetCellValue("Thuê xe:");
            //    //cell.CellStyle = styleFooterText;

            //    //cell = row.CreateCell(12);
            //    //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
            //    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //    //rowIndex++;
            //    //row = sheet.CreateRow(rowIndex);

            //    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
            //    //sheet.AddMergedRegion(cra);
            //    //cell = row.CreateCell(3);
            //    //cell.SetCellValue("Không dùng bơm:");
            //    //cell.CellStyle = styleFooterText;

            //    //cell = row.CreateCell(6);
            //    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
            //    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
            //    //sheet.AddMergedRegion(cra);
            //    //cell = row.CreateCell(8);
            //    //cell.SetCellValue("Mua bê tông:");
            //    //cell.CellStyle = styleFooterText;

            //    //cell = row.CreateCell(12);
            //    //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
            //    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //    //rowIndex++;
            //    //rowIndex++;
            //    //row = sheet.CreateRow(rowIndex);

            //    //đại diện
            //    rowIndex++; rowIndex++;
            //    row = sheet.CreateRow(rowIndex);

            //    cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
            //    sheet.AddMergedRegion(cra);

            //    cell = row.CreateCell(0);
            //    cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
            //    cell.CellStyle = styleBodyFooterCenterNoborder;

            //    cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
            //    sheet.AddMergedRegion(cra);

            //    cell = row.CreateCell(6);
            //    cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
            //    cell.CellStyle = styleBodyFooterCenterNoborder;
            //    //tên
            //    rowIndex++;
            //    row = sheet.CreateRow(rowIndex);
            //    cra = new CellRangeAddress(rowIndex, rowIndex, 0, 1);
            //    sheet.AddMergedRegion(cra);
            //    cell = row.CreateCell(0);
            //    cell.SetCellValue("Người nhập kho");
            //    cell.CellStyle = styleBodyFooterCenterNoborder;

            //    cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
            //    sheet.AddMergedRegion(cra);
            //    cell = row.CreateCell(2);
            //    cell.SetCellValue("Kế toán");
            //    cell.CellStyle = styleBodyFooterCenterNoborder;

            //    cra = new CellRangeAddress(rowIndex, rowIndex, 4, 5);
            //    sheet.AddMergedRegion(cra);
            //    cell = row.CreateCell(4);
            //    cell.SetCellValue("Trưởng bộ phận");
            //    cell.CellStyle = styleBodyFooterCenterNoborder;

            //    cra = new CellRangeAddress(rowIndex, rowIndex, 6, 8);
            //    sheet.AddMergedRegion(cra);
            //    cell = row.CreateCell(6);
            //    cell.SetCellValue("Giám đốc");
            //    cell.CellStyle = styleBodyFooterCenterNoborder;

            //    cra = new CellRangeAddress(rowIndex, rowIndex, 9, 10);
            //    sheet.AddMergedRegion(cra);
            //    cell = row.CreateCell(9);
            //    cell.SetCellValue("Kế toán");
            //    cell.CellStyle = styleBodyFooterCenterNoborder;

            //    cra = new CellRangeAddress(rowIndex, rowIndex, 11, 13);
            //    sheet.AddMergedRegion(cra);
            //    cell = row.CreateCell(11);
            //    cell.SetCellValue("Giám sát");
            //    cell.CellStyle = styleBodyFooterCenterNoborder;

            //    #endregion

            //    using (var exportData = new MemoryStream())
            //    {
            //        workbook.Write(exportData);

            //        #region [Set title dưới]

            //        string saveAsFileName = "";
            //        string Bienso = "";

            //        if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
            //        {
            //            saveAsFileName = "SẢN XUẤT GẠCH XÂY DỰNG -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
            //                             ".xls";
            //        }
            //        else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
            //        {
            //            saveAsFileName = "SẢN XUẤT GẠCH XÂY DỰNG -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
            //                             "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
            //        }

            //        Response.ContentType = "application/vnd.ms-excel";
            //        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

            //        #endregion

            //        Response.Clear();
            //        Response.BinaryWrite(exportData.GetBuffer());
            //        Response.End();
            //    }
            //}
            //else
            //    GstGetMess("Không có dữ liệu nào để in", "");
        }


        protected void btnChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else if (GVChot.Rows.Count == 0)
                {
                    GstGetMess("Thời gian này bạn không nhập gạch xây dựng", "");
                    mpChot.Show();
                }
                else
                {
                    var query = vatlieu.sp_GachXayDung_CheckChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        vatlieu.sp_GachXayDung_Chot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }

        protected void btnMoChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh mở chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else if (GVChot.Rows.Count == 0)
                {
                    GstGetMess("Thời gian này bạn không có gì để mở chốt.", "");
                    mpChot.Show();
                }
                else
                {
                    var query = vatlieu.sp_GachXayDung_CheckMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        vatlieu.sp_GachXayDung_MoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }

        protected void btnOpenChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Chốt nhập gạch";
            mpChot.Show();
            btnChot.Visible = true;
            btnMoChot.Visible = false;
            GVChot.DataSource = null;
            GVChot.DataBind();
        }

        protected void btnOpenMoChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Mở chốt nhập gạch";
            mpChot.Show();
            btnChot.Visible = false;
            btnMoChot.Visible = true;
            GVChot.DataSource = null;
            GVChot.DataBind();
        }

        protected void lbtSearchChot_Click(object sender, EventArgs e)
        {
            //if (btnChot.Visible == true)
            //{
            //    List<sp_GachXayDung_ListChotResult> query = vatlieu.sp_GachXayDung_ListChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
            //    GVChot.DataSource = query;
            //    GVChot.DataBind();

            //    if (query != null)
            //    {
            //        GVChot.FooterRow.Cells[0].ColumnSpan = 4;
            //        GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Count);
            //        GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

            //        var valuefooter = (from p in query
            //                           group p by 1 into g
            //                           select new
            //                           {
            //                               SumKLXiMang = g.Sum(x => x.KLXiMang),
            //                               SumKLCat = g.Sum(x => x.KLCat),
            //                               SumKLDaMat = g.Sum(x => x.KLDaMat),
            //                               SumKLVLKhac = g.Sum(x => x.KLVLKhac),
            //                               SumSoLuong = g.Sum(x => x.SoLuong),
            //                               SumSoMeTron = g.Sum(x => x.SoMeTron)
            //                           }).FirstOrDefault();

            //        GVChot.FooterRow.Cells[1].Text = string.Format("{0:N0}", valuefooter.SumSoLuong);
            //        GVChot.FooterRow.Cells[2].Text = string.Format("{0:N0}", valuefooter.SumSoMeTron);
            //        GVChot.FooterRow.Cells[3].Text = string.Format("{0:N0}", valuefooter.SumKLXiMang);
            //        GVChot.FooterRow.Cells[4].Text = string.Format("{0:N0}", valuefooter.SumKLCat);
            //        GVChot.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.SumKLDaMat);
            //        GVChot.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.SumKLVLKhac);

            //        GVChot.FooterRow.Cells[7].Visible = false;
            //        GVChot.FooterRow.Cells[8].Visible = false;
            //        GVChot.FooterRow.Cells[9].Visible = false;
            //        //GVChot.FooterRow.Cells[10].Visible = false;
            //    }
            //}
            //else if (btnChot.Visible == false)
            //{
            //    List<sp_GachXayDung_ListMoChotResult> query = vatlieu.sp_GachXayDung_ListMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
            //    GVChot.DataSource = query;
            //    GVChot.DataBind();

            //    if (query != null)
            //    {
            //        GVChot.FooterRow.Cells[0].ColumnSpan = 4;
            //        GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Count);
            //        GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

            //        var valuefooter = (from p in query
            //                           group p by 1 into g
            //                           select new
            //                           {
            //                               SumKLXiMang = g.Sum(x => x.KLXiMang),
            //                               SumKLCat = g.Sum(x => x.KLCat),
            //                               SumKLDaMat = g.Sum(x => x.KLDaMat),
            //                               SumKLVLKhac = g.Sum(x => x.KLVLKhac),
            //                               SumSoLuong = g.Sum(x => x.SoLuong),
            //                               SumSoMeTron = g.Sum(x => x.SoMeTron)
            //                           }).FirstOrDefault();

            //        GVChot.FooterRow.Cells[1].Text = string.Format("{0:N0}", valuefooter.SumSoLuong);
            //        GVChot.FooterRow.Cells[2].Text = string.Format("{0:N0}", valuefooter.SumSoMeTron);
            //        GVChot.FooterRow.Cells[3].Text = string.Format("{0:N0}", valuefooter.SumKLXiMang);
            //        GVChot.FooterRow.Cells[4].Text = string.Format("{0:N0}", valuefooter.SumKLCat);
            //        GVChot.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.SumKLDaMat);
            //        GVChot.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.SumKLVLKhac);

            //        GVChot.FooterRow.Cells[7].Visible = false;
            //        GVChot.FooterRow.Cells[8].Visible = false;
            //        GVChot.FooterRow.Cells[9].Visible = false;
            //        //GVChot.FooterRow.Cells[10].Visible = false;
            //    }
            //}
            //mpChot.Show();
        }

    }
}