﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class QuanLyGiaoDich : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        NganHangDataContext nganhang = new NganHangDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmQuanLyGiaoDich";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmQuanLyGiaoDich", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadHangMucThuChi();
                        LoadDoiTuongNhan();
                        LoadChiNhanhSearch();
                        LoadNganHang();
                        dlNganHang_SelectedIndexChanged(sender, e);
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        LoadNganHangSearch();
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();

            dlChiNhanhUpLoad.DataSource = query;
            dlChiNhanhUpLoad.DataBind();
        }
        protected void LoadHangMucThuChi()
        {
            var query = (from p in db.tblHangMucThuChis
                         where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenHangMucThuChi
                         }).OrderBy(p => p.Ten);
            dlHangMucChi.DataSource = query;
            dlHangMucChi.DataBind();
            dlHangMucChi.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadDoiTuongNhan()
        {
            var query = nganhang.sp_QuanLyGiaoDich_LoadDoiTuongNhan();
            dlDoiTuongNhan.DataSource = query;
            dlDoiTuongNhan.DataBind();
            dlDoiTuongNhan.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNganHang()
        {
            dlNganHang.Items.Clear();
            dlSoTaiKhoan.Items.Clear();
            dlLoaiTien.Items.Clear();
            var query = nganhang.sp_QuanLyGiaoDich_LoadNganHang(GetDrop(dlChiNhanh));
            dlNganHang.DataSource = query;
            dlNganHang.DataBind();
            if (dlNganHang.Items.Count == 1)
            {
                LoadSoTaiKhoan();
            }
            else
            {
                dlNganHang.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        protected void LoadSoTaiKhoan()
        {
            dlSoTaiKhoan.Items.Clear();
            dlLoaiTien.Items.Clear();
            var query = nganhang.sp_QuanLyGiaoDich_LoadSoTaiKhoan(GetDrop(dlChiNhanh), GetDrop(dlNganHang));
            dlSoTaiKhoan.DataSource = query;
            dlSoTaiKhoan.DataBind();
            if (dlSoTaiKhoan.Items.Count == 1)
            {
                LoadLoaiTien();
            }
            else
            {
                dlSoTaiKhoan.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        protected void LoadLoaiTien()
        {
            dlLoaiTien.Items.Clear();
            var query = nganhang.sp_QuanLyGiaoDich_LoadLoaiTien(dlSoTaiKhoan.SelectedValue);
            dlLoaiTien.DataSource = query;
            dlLoaiTien.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNganHang.SelectedValue == "")
            {
                s += " - Chọn đối tượng nhận<br />";
            }
            if (dlSoTaiKhoan.SelectedValue == "")
            {
                s += " - Chọn số tài khoản<br />";
            }
            if (txtNgayHoachToan.Text == "")
            {
                s += " - Nhập hoạch toán<br />";
            }
            if (txtNgayChungTu.Text == "")
            {
                s += " - Nhập chứng từ<br />";
            }
            if (dlThuChi.SelectedValue == "")
            {
                s += " - Chọn loại thu - chi<br />";
            }

            //if (txtTaiKhoanDoiUng.Text == "")
            //{
            //    s += " - Nhận người nhận<br />";
            //}
            if (txtSoTien.Text.Trim() == "" || decimal.Parse(txtSoTien.Text) == 0)
            {
                s += " - Nhập số tiền<br />";
            }
            //if (txtDienGiai.Text.Trim() == "")
            //{
            //    s += " - Nhập nội dung<br />";
            //}
            if (s == "")
            {
                var query = nganhang.sp_QuanLyGiaoDich_CheckThem(GetDrop(dlChiNhanh), dlSoTaiKhoan.SelectedValue, ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNganHang.SelectedValue == "")
            {
                s += " - Chọn đối tượng nhận<br />";
            }
            if (dlSoTaiKhoan.SelectedValue == "")
            {
                s += " - Chọn số tài khoản<br />";
            }
            if (txtNgayHoachToan.Text == "")
            {
                s += " - Nhập hoạch toán<br />";
            }
            if (txtNgayChungTu.Text == "")
            {
                s += " - Nhập chứng từ<br />";
            }
            if (dlThuChi.SelectedValue == "")
            {
                s += " - Chọn loại thu - chi<br />";
            }

            //if (txtTaiKhoanDoiUng.Text == "")
            //{
            //    s += " - Nhận người nhận<br />";
            //}
            if (txtSoTien.Text.Trim() == "" || decimal.Parse(txtSoTien.Text) == 0)
            {
                s += " - Nhập số tiền<br />";
            }
            //if (txtDienGiai.Text.Trim() == "")
            //{
            //    s += " - Nhập nội dung<br />";
            //}
            if (s == "")
            {
                nganhang.sp_QuanLyGiaoDich_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), dlSoTaiKhoan.SelectedValue, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var QuanLyGiaoDich = new tblQuanLyGiaoDich()
                        {
                            ID = Guid.NewGuid(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            HangMucChi = GetDrop(dlHangMucChi),
                            IDDoiTuongNhan = GetDrop(dlDoiTuongNhan),
                            IDNganHang = GetDrop(dlNganHang),
                            SoTaiKhoan = dlSoTaiKhoan.SelectedValue,
                            LoaiTien = dlLoaiTien.SelectedValue,
                            NgayHoachToan = DateTime.Parse(GetNgayThang(txtNgayHoachToan.Text)),
                            NgayChungTu = DateTime.Parse(GetNgayThang(txtNgayChungTu.Text)),
                            TaiKhoanDoiUng = txtTaiKhoanDoiUng.Text.Trim(),
                            HangMucThuChi = dlThuChi.SelectedValue,
                            SoTienThu = dlThuChi.SelectedValue == "Thu" ? decimal.Parse(txtSoTien.Text) : 0,
                            SoTienChi = dlThuChi.SelectedValue == "Chi" ? decimal.Parse(txtSoTien.Text) : 0,
                            DienGiai = txtDienGiai.Text.Trim(),
                            GhiChu = txtGhiChu.Text.Trim(),
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblQuanLyGiaoDiches.InsertOnSubmit(QuanLyGiaoDich);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");

                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblQuanLyGiaoDiches
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.HangMucChi = GetDrop(dlHangMucChi);
                            query.IDDoiTuongNhan = GetDrop(dlDoiTuongNhan);
                            query.IDNganHang = GetDrop(dlNganHang);
                            query.SoTaiKhoan = dlSoTaiKhoan.SelectedValue;
                            query.LoaiTien = dlLoaiTien.SelectedValue;
                            query.NgayHoachToan = DateTime.Parse(GetNgayThang(txtNgayHoachToan.Text));
                            query.NgayChungTu = DateTime.Parse(GetNgayThang(txtNgayChungTu.Text));
                            query.TaiKhoanDoiUng = txtTaiKhoanDoiUng.Text.Trim();
                            query.HangMucThuChi = dlThuChi.SelectedValue;
                            query.SoTienThu = dlThuChi.SelectedValue == "Thu" ? decimal.Parse(txtSoTien.Text) : 0;
                            query.SoTienChi = dlThuChi.SelectedValue == "Chi" ? decimal.Parse(txtSoTien.Text) : 0;
                            query.DienGiai = txtDienGiai.Text.Trim();
                            query.GhiChu = txtGhiChu.Text.Trim();
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin giá mua vật liệu đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh_SelectedIndexChanged(sender, e);
            dlNganHang_SelectedIndexChanged(sender, e);
            txtNgayHoachToan.Text = "";
            txtNgayChungTu.Text = "";
            txtTaiKhoanDoiUng.Text = "";
            dlThuChi.SelectedValue = "";
            txtSoTien.Text = "";
            txtDienGiai.Text = "";
            txtGhiChu.Text = "";
            hdID.Value = "";
        }
        protected void btnUpLoad_Click(object sender, EventArgs e)
        {
            if (dlNganHangUpLoad.SelectedValue == "")
            {
                GstGetMess("Chọn ngân hàng", "");
            }
            else if (dlSoTaiKhoanUpLoad.SelectedValue == "")
            {
                GstGetMess("Chọn số tài khoản", "");
            }
            else if (FileUpload1.HasFile)
            {
                try
                {

                    string extision = FileUpload1.FileName.Trim();

                    string time = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string filename = time + extision.Substring(extision.LastIndexOf('.'));
                    string path = Server.MapPath("~/MyFolder/" + filename);
                    FileUpload1.SaveAs(path);
                    string ext = Path.GetExtension(FileUpload1.FileName).ToLower();
                    string ConStr = "";
                    ////Extantion of the file upload control saving into ext because   
                    ////there are two types of extation .xls and .xlsx of Excel   
                    //string ext = Path.GetExtension(FileUpload1.FileName).ToLower();
                    ////getting the path of the file   
                    //string path = Server.MapPath("~/MyFolder/" + FileUpload1.FileName);
                    ////saving the file inside the MyFolder of the server  
                    //FileUpload1.SaveAs(path);

                    //checking that extantion is .xls or .xlsx  
                    if (ext.Trim() == ".xls")
                    {
                        //connection string for that file which extantion is .xls  
                        ConStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    else if (ext.Trim() == ".xlsx")
                    {
                        //connection string for that file which extantion is .xlsx  
                        ConStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //making query  
                    string query = "SELECT * FROM [Sheet1$]";
                    //Providing connection  
                    OleDbConnection conn = new OleDbConnection(ConStr);
                    //checking that connection state is closed or not if closed the   
                    //open the connection  
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }
                    //create command object  
                    OleDbCommand cmd = new OleDbCommand(query, conn);
                    // create a data adapter and get the data into dataadapter  
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    DataTable ds = new DataTable();
                    //fill the Excel data to data set  
                    da.Fill(ds);

                    List<tblQuanLyGiaoDich> giaodich = new List<tblQuanLyGiaoDich>();

                    for (int i = 0; i < ds.Rows.Count; i++)
                    {
                        if (ds.Rows[i]["Ngày hạch toán"].ToString() != "" && ds.Rows[i]["Ngày chứng từ"].ToString() != "" && ds.Rows[i]["TK đối ứng"].ToString() != "")
                        {
                            var them = new tblQuanLyGiaoDich();
                            them.ID = Guid.NewGuid();
                            them.IDChiNhanh = GetDrop(dlChiNhanhUpLoad);
                            them.IDNganHang = GetDrop(dlNganHangUpLoad);
                            them.SoTaiKhoan = dlSoTaiKhoanUpLoad.SelectedValue;
                            them.LoaiTien = dlLoaiTienUpLoad.SelectedValue;
                            them.NgayHoachToan = DateTime.Parse(ds.Rows[i]["Ngày hạch toán"].ToString());
                            them.NgayChungTu = DateTime.Parse(ds.Rows[i]["Ngày chứng từ"].ToString());
                            them.TaiKhoanDoiUng = ds.Rows[i]["TK đối ứng"].ToString();
                            them.HangMucThuChi = decimal.Parse(ds.Rows[i]["Thu"].ToString()) > 0 ? "Thu" : "Chi";
                            them.SoTienThu = decimal.Parse(ds.Rows[i]["Thu"].ToString());
                            them.SoTienChi = decimal.Parse(ds.Rows[i]["Chi"].ToString());
                            them.DienGiai = ds.Rows[i]["Diễn giải"].ToString();
                            them.GhiChu = "";
                            them.TrangThai = 1;
                            them.TrangThaiText = "Chờ duyệt";
                            them.NgayTao = DateTime.Now;
                            them.NguoiTao = Session["IDND"].ToString();
                            giaodich.Add(them);
                        }
                    }
                    db.tblQuanLyGiaoDiches.InsertAllOnSubmit(giaodich);
                    db.SubmitChanges();
                    //set data source of the grid view  
                    //gvUpLoad.DataSource = ds.Tables[0];
                    //binding the gridview  
                    //gvUpLoad.DataBind();
                    //close the connection  

                    conn.Close();
                    //mpUpLoad.Show();
                    GstGetMess("UpLoad thành công", "");
                }
                catch (Exception ae)
                {
                    GstGetMess("UpLoad không thành công - " + ae.ToString(), "");
                    throw;
                }
            }
            else
            {
                GstGetMess("Bạn phải chọn file upload", "");
                mpUpLoad.Show();
            }
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";

            string id = e.CommandArgument.ToString();

            var query = (from p in db.tblQuanLyGiaoDiches
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //soquy.sp_QuanLyGiaoDich_CheckSuaGV(query.ID, query.IDChiNhanh, query.NgayHoachToan, query.IDNganHang, ref thongbao);
                        //if (thongbao != "")
                        //    Warning(thongbao);
                        //else
                        //{
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlHangMucChi.SelectedValue = query.HangMucChi.ToString();
                        dlHangMucChi_SelectedIndexChanged(sender, e);
                        dlDoiTuongNhan.SelectedValue = query.IDDoiTuongNhan.ToString();
                        LoadNganHang();
                        dlNganHang.SelectedValue = query.IDNganHang.ToString();
                        dlNganHang_SelectedIndexChanged(sender, e);
                        dlSoTaiKhoan.SelectedValue = query.SoTaiKhoan;
                        dlSoTaiKhoan_SelectedIndexChanged(sender, e);

                        txtNgayHoachToan.Text = query.NgayHoachToan.Value.ToString("dd/MM/yyyy");
                        txtNgayChungTu.Text = query.NgayChungTu.Value.ToString("dd/MM/yyyy");
                        txtTaiKhoanDoiUng.Text = query.TaiKhoanDoiUng;

                        dlThuChi.SelectedValue = query.HangMucThuChi.ToString();
                        txtSoTien.Text = string.Format("{0:N0}", query.SoTienChi > 0 ? query.SoTienChi : query.SoTienThu);
                        txtDienGiai.Text = query.DienGiai;
                        txtGhiChu.Text = query.GhiChu;
                        hdID.Value = id;
                        //}
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //soquy.sp_QuanLyGiaoDich_CheckXoa(query.IDChiNhanh, query.NgayHoachToan, ref thongbao);
                        //if (thongbao != "")
                        //    Warning(thongbao);
                        //else
                        //{
                        if (query.TrangThai != 3)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                        }
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = soquy.sp_QuanLyGiaoDich_LichSu(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
                //else if (e.CommandName == "HinhAnh")
                //{
                //    imgHinhAnh.ImageUrl = "~/ImageQuanLyGiaoDich/" + query.HinhAnh;
                //    mpHinhAnh.Show();
                //}
                //else if (e.CommandName == "UpLoad")
                //{
                //    hdHinhAnh.Value = id;
                //    mpUpLoad.Show();
                //}
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void ResetFilter()
        {
            //dlSoTaiKhoanSearch.Items.Clear();
            //dlNganHangSearch.Items.Clear();
            //dlTaiKhoanDoiUngSearch.Items.Clear();
            //List<sp_QuanLyGiaoDich_ResetFilterResult> query = soquy.sp_QuanLyGiaoDich_ResetFilter(GetDrop(dlChiNhanhSearch), int.Parse(dlThuChiSearch.SelectedValue), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text))).ToList();

            //dlSoTaiKhoanSearch.DataSource = (from p in query
            //                                 select new
            //                                 {
            //                                     ID = p.HangMucChi,
            //                                     Ten = p.TenHangMucThuChi
            //                                 }).Distinct().OrderBy(p => p.Ten);
            //dlSoTaiKhoanSearch.DataBind();
            //dlNganHangSearch.DataSource = (from p in query
            //                                   select new
            //                                   {
            //                                       ID = p.IDNganHang,
            //                                       Ten = p.TenNganHang
            //                                   }).Distinct().OrderBy(p => p.Ten);
            //dlNganHangSearch.DataBind();
            //dlTaiKhoanDoiUngSearch.DataSource = (from p in query
            //                                select new
            //                                {
            //                                    ID = p.TaiKhoanDoiUng,
            //                                    Ten = p.TaiKhoanDoiUng
            //                                }).Distinct().OrderBy(p => p.Ten);
            //dlTaiKhoanDoiUngSearch.DataBind();
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private string GetValueSelectedTaiKhoanDoiUng(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "N'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",N'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = N'" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            List<sp_QuanLyGiaoDich_SearchResult> query = nganhang.sp_QuanLyGiaoDich_Search(dlChiNhanhSearch.SelectedValue, dlNganHangSearch.SelectedValue, dlSoTaiKhoanSearch.SelectedValue,
            DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text)), 10, page).ToList();

            GV.DataSource = query;
            GV.DataBind();
            if (GV.Rows.Count > 0)
            {
                sp_QuanLyGiaoDich_SearchFooterResult footer = nganhang.sp_QuanLyGiaoDich_SearchFooter(dlChiNhanhSearch.SelectedValue, dlNganHangSearch.SelectedValue, dlSoTaiKhoanSearch.SelectedValue,
                DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();

                GV.FooterRow.Cells[0].ColumnSpan = 12;
                GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", footer.SoLuong);
                GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", footer.SoTienThu);
                GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", footer.SoTienChi);
                GV.FooterRow.Cells[3].Text = "Còn lại:";
                GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", footer.ConLai);

                GV.FooterRow.Cells[5].Visible = false;
                GV.FooterRow.Cells[6].Visible = false;
                GV.FooterRow.Cells[7].Visible = false;
                GV.FooterRow.Cells[8].Visible = false;
                GV.FooterRow.Cells[9].Visible = false;
                GV.FooterRow.Cells[10].Visible = false;
                GV.FooterRow.Cells[11].Visible = false;
                GV.FooterRow.Cells[12].Visible = false;
                GV.FooterRow.Cells[13].Visible = false;
                GV.FooterRow.Cells[14].Visible = false;
                GV.FooterRow.Cells[15].Visible = false;
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 5, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                List<sp_QuanLyGiaoDich_PrintResult> query = nganhang.sp_QuanLyGiaoDich_Print(dlChiNhanhSearch.SelectedValue, dlNganHangSearch.SelectedValue, dlSoTaiKhoanSearch.SelectedValue,
                DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();
                if (query.Count > 0)
                {
                    string dc = "", tencongty = "", sdt = "";
                    var tencty = (from x in db.tblThamSos
                                  select x).FirstOrDefault();
                    if (tencty != null && tencty.ID != null)
                    {
                        dc = tencty.DiaChi;
                        sdt = tencty.SoDienThoai;
                        tencongty = tencty.TenCongTy;
                    }


                    var workbook = new HSSFWorkbook();
                    IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                    string sheetname = "BÁO CÁO GIAO DỊCH NGÂN HÀNG";

                    #region[CSS]

                    var sheet = workbook.CreateSheet(sheetname);
                    sheet = xl.SetPropertySheet(sheet);

                    IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                    IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                    IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                    IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                    ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                    ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                    ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                    ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                    ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                    ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                    ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                    ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                    ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                    ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                    ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                    ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                    ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                    ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                    ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                    #endregion

                    #region[Tạo header trên]

                    var rowIndex = 0;
                    var row = sheet.CreateRow(rowIndex);
                    ICell r1c1 = row.CreateCell(0);
                    r1c1.SetCellValue(tencongty);
                    r1c1.CellStyle = styleboldcenternoborder;
                    r1c1.Row.Height = 400;
                    CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 2);
                    sheet.AddMergedRegion(cra);
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);
                    r1c1 = row.CreateCell(0);
                    r1c1.SetCellValue(dc);
                    r1c1.CellStyle = styleboldcenternoborder;
                    r1c1.Row.Height = 400;
                    cra = new CellRangeAddress(1, 1, 0, 2);
                    sheet.AddMergedRegion(cra);
                    //Tiêu đề báo cáo

                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);
                    r1c1 = row.CreateCell(0);

                    if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                    {
                        r1c1.SetCellValue("BÁO CÁO GIAO DỊCH NGÂN HÀNG TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                          "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                    }
                    else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                    {
                        r1c1.SetCellValue("BÁO CÁO GIAO DỊCH NGÂN HÀNG NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                    }

                    r1c1.CellStyle = styleHeader1;
                    r1c1.Row.Height = 800;
                    cra = new CellRangeAddress(2, 2, 0, 15);
                    sheet.AddMergedRegion(cra);

                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    r1c1 = row.CreateCell(0);
                    string kq = "";

                    r1c1.SetCellValue(kq);



                    r1c1.CellStyle = styleHeader2;
                    r1c1.Row.Height = 500;
                    cra = new CellRangeAddress(3, 3, 0, 15);
                    sheet.AddMergedRegion(cra);
                    sheet.CreateFreezePane(0, 15);
                    #endregion

                    #region[Tạo header dưới]
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    string[] header2 =
                        {
                        "Ngày tháng",
                        "Chi nhánh",
                        "Nhà cung cấp",
                        "Hạng mục chi",
                        "Ngân hàng",
                        "Số tài khoản",
                        "Loại tiền",
                        "Ngày hoạch toán",
                        "Ngày chứng từ",
                        "Tài khoản đối ứng",
                        "Thu - Chi",
                        "Số tiền thu",
                        "Số tiền chi",
                        "Diễn giải",
                        "Ghi chú"
                    };
                    var cell = row.CreateCell(0);

                    cell.SetCellValue("STT");
                    cell.CellStyle = styleHeaderChiTietCenter;
                    cell.Row.Height = 600;

                    for (int hk = 0; hk < header2.Length; hk++)
                    {
                        r1c1 = row.CreateCell(hk + 1);
                        r1c1.SetCellValue(header2[hk].ToString());
                        r1c1.CellStyle = styleHeaderChiTietCenter;
                        r1c1.Row.Height = 800;
                    }
                    #endregion

                    #region[ghi dữ liệu]
                    int STT2 = 0;

                    string RowDau = (rowIndex + 2).ToString();
                    foreach (var item in query)
                    {
                        STT2++;
                        rowIndex++;
                        row = sheet.CreateRow(rowIndex);

                        cell = row.CreateCell(0);
                        cell.SetCellValue(STT2);
                        cell.CellStyle = styleBodyIntCenter;

                        cell = row.CreateCell(1);
                        cell.SetCellValue(item.NgayHoachToan.Value);
                        cell.CellStyle = styleBodyDatetime;

                        cell = row.CreateCell(2);
                        cell.SetCellValue(item.TenChiNhanh);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(3);
                        cell.SetCellValue(item.TenNhaCungCap);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(4);
                        cell.SetCellValue(item.TenHangMucThuChi);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(5);
                        cell.SetCellValue(item.TenNganHang);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(6);
                        cell.SetCellValue(item.SoTaiKhoan);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(7);
                        cell.SetCellValue(item.LoaiTien);
                        cell.CellStyle = styleBody;
                        
                        cell = row.CreateCell(8);
                        cell.SetCellValue(item.NgayHoachToan.Value);
                        cell.CellStyle = styleBodyDatetime;

                        cell = row.CreateCell(9);
                        cell.SetCellValue(item.NgayChungTu.Value);
                        cell.CellStyle = styleBodyDatetime;

                        cell = row.CreateCell(10);
                        cell.SetCellValue(item.TaiKhoanDoiUng);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(11);
                        cell.SetCellValue(item.HangMucThuChi);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(12);
                        cell.SetCellValue(double.Parse(item.SoTienThu.ToString()));
                        cell.CellStyle = styleBodyIntRight;

                        cell = row.CreateCell(13);
                        cell.SetCellValue(double.Parse(item.SoTienChi.ToString()));
                        cell.CellStyle = styleBodyIntRight;

                        cell = row.CreateCell(14);
                        cell.SetCellValue(item.DienGiai);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(15);
                        cell.SetCellValue(item.GhiChu);
                        cell.CellStyle = styleBody;

                    }
                    #endregion

                    #region Tổng cộng
                    rowIndex++;
                    string RowCuoi = rowIndex.ToString();
                    row = sheet.CreateRow(rowIndex);
                    for (int i = 1; i <= 15; i++)
                    {
                        cell = row.CreateCell(i);
                        cell.CellStyle = styleBodyIntRight;
                        cell.Row.Height = 400;
                    }
                    cell = row.CreateCell(0);
                    cell.SetCellValue("Tổng cộng:");
                    cell.Row.Height = 400;
                    cell.CellStyle = styleboldrightborder;

                    cra = new CellRangeAddress(rowIndex, rowIndex, 0, 1);
                    sheet.AddMergedRegion(cra);

                    cell = row.CreateCell(12);
                    cell.CellFormula = "SUM(M" + RowDau.ToString() + ":M" + RowCuoi.ToString() + ")";
                    cell.CellStyle = styleBodyIntFooterRight;

                    cell = row.CreateCell(13);
                    cell.CellFormula = "SUM(N" + RowDau.ToString() + ":N" + RowCuoi.ToString() + ")";
                    cell.CellStyle = styleBodyIntRight;


                    #endregion

                    #region[Set Footer]

                    //rowIndex++;
                    //rowIndex++;

                    //rowIndex++;
                    //row = sheet.CreateRow(rowIndex);

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(3);
                    //cell.SetCellValue("Bơm theo ca:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(6);
                    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(8);
                    //cell.SetCellValue("Thuê bơm:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(12);
                    //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //rowIndex++;
                    //row = sheet.CreateRow(rowIndex);

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(3);
                    //cell.SetCellValue("Bơm theo khối:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(6);
                    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(8);
                    //cell.SetCellValue("Thuê xe:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(12);
                    //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //rowIndex++;
                    //row = sheet.CreateRow(rowIndex);

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(3);
                    //cell.SetCellValue("Không dùng bơm:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(6);
                    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(8);
                    //cell.SetCellValue("Mua bê tông:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(12);
                    //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //rowIndex++;
                    //rowIndex++;
                    //row = sheet.CreateRow(rowIndex);

                    //đại diện
                    rowIndex++; rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                    sheet.AddMergedRegion(cra);

                    //tên
                    rowIndex++;

                    cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
                    sheet.AddMergedRegion(cra);
                    cell = row.CreateCell(2);
                    cell.SetCellValue("Kế toán");
                    cell.CellStyle = styleBodyFooterCenterNoborder;

                    cra = new CellRangeAddress(rowIndex, rowIndex, 5, 6);
                    sheet.AddMergedRegion(cra);
                    cell = row.CreateCell(5);
                    cell.SetCellValue("Giám đốc");
                    cell.CellStyle = styleBodyFooterCenterNoborder;

                    #endregion

                    using (var exportData = new MemoryStream())
                    {
                        workbook.Write(exportData);

                        #region [Set title dưới]

                        string saveAsFileName = "";
                        string Bienso = "";

                        if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                        {
                            saveAsFileName = "BÁO CÁO GIAO DỊCH NGÂN HÀNG -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                             ".xls";
                        }
                        else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                        {
                            saveAsFileName = "BÁO CÁO GIAO DỊCH NGÂN HÀNG -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                             "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                        }

                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                        #endregion

                        Response.Clear();
                        Response.BinaryWrite(exportData.GetBuffer());
                        Response.End();
                    }
                }
                else
                    GstGetMess("Không có dữ liệu nào để in", "");
            }
        }

        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            //if (divdl.Visible == true)
            //{
            //    divdl.Visible = false;
            //    dlSoTaiKhoanSearch.Items.Clear();
            //    dlNganHangSearch.Items.Clear();
            //    dlTaiKhoanDoiUngSearch.Items.Clear();
            //}
            //else
            //{
            //    divdl.Visible = true;
            //    ResetFilter();
            //}
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNganHangSearch();
        }
        protected void LoadNganHangSearch()
        {
            dlNganHangSearch.Items.Clear();
            List<sp_QuanLyGiaoDich_LoadNganHangSearchResult> query = nganhang.sp_QuanLyGiaoDich_LoadNganHangSearch(GetDrop(dlChiNhanhSearch)).ToList();
            dlNganHangSearch.DataSource = query;
            dlNganHangSearch.DataBind();
            dlNganHangSearch.Items.Insert(0, new ListItem("Tất cả", ""));

        }
        protected void LoadSoTaiKhoanSearch()
        {
            dlNganHangSearch.Items.Clear();
            List<sp_QuanLyGiaoDich_LoadSoTaiKhoanSearchResult> query = nganhang.sp_QuanLyGiaoDich_LoadSoTaiKhoanSearch(GetDrop(dlChiNhanhSearch), GetDrop(dlNganHangSearch)).ToList();
            dlNganHangSearch.DataSource = query;
            dlNganHangSearch.DataBind();
            dlNganHangSearch.Items.Insert(0, new ListItem("Tất cả", ""));
        }
        protected void dlNganHang_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSoTaiKhoan();
        }

        protected void dlSoTaiKhoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiTien();
        }

        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNganHang();
        }
        protected void dlHangMucChi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlHangMucChi.SelectedValue != "")
            {
                var query = (from p in db.tblHangMucThuChis
                             where p.ID == GetDrop(dlHangMucChi)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    ckLoai.Checked = query.Loai.Value;
                }
                else
                {
                    ckLoai.Checked = false;
                }
            }
            else
            {
                ckLoai.Checked = false;
            }
        }
        protected void lbtImportFile_Click(object sender, EventArgs e)
        {
            dlChiNhanhUpLoad_SelectedIndexChanged(sender, e);
            mpUpLoad.Show();
        }

        protected void dlChiNhanhUpLoad_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNganHangUpLoad();
        }

        protected void dlNganHangUpLoad_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSoTaiKhoanUpLoad();
        }

        protected void dlSoTaiKhoanUpLoad_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiTienUpLoad();
        }
        protected void LoadNganHangUpLoad()
        {
            dlNganHangUpLoad.Items.Clear();
            dlSoTaiKhoanUpLoad.Items.Clear();
            dlLoaiTienUpLoad.Items.Clear();
            var query = nganhang.sp_QuanLyGiaoDich_LoadNganHang(GetDrop(dlChiNhanh));
            dlNganHangUpLoad.DataSource = query;
            dlNganHangUpLoad.DataBind();
            if (dlNganHangUpLoad.Items.Count == 1)
            {
                LoadSoTaiKhoanUpLoad();
            }
            else
            {
                dlNganHangUpLoad.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        protected void LoadSoTaiKhoanUpLoad()
        {
            dlSoTaiKhoanUpLoad.Items.Clear();
            dlLoaiTienUpLoad.Items.Clear();
            var query = nganhang.sp_QuanLyGiaoDich_LoadSoTaiKhoan(GetDrop(dlChiNhanh), GetDrop(dlNganHang));
            dlSoTaiKhoanUpLoad.DataSource = query;
            dlSoTaiKhoanUpLoad.DataBind();
            if (dlSoTaiKhoanUpLoad.Items.Count == 1)
            {
                LoadLoaiTienUpLoad();
            }
            else
            {
                dlSoTaiKhoanUpLoad.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        protected void LoadLoaiTienUpLoad()
        {
            dlLoaiTienUpLoad.Items.Clear();
            var query = nganhang.sp_QuanLyGiaoDich_LoadLoaiTien(dlSoTaiKhoan.SelectedValue);
            dlLoaiTienUpLoad.DataSource = query;
            dlLoaiTienUpLoad.DataBind();
        }
    }
}