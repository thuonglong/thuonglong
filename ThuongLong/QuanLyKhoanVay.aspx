﻿<%@ Page Title="Quản lý khoản vay" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QuanLyKhoanVay.aspx.cs" Inherits="ThuongLong.QuanLyKhoanVay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin khoản vay</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày vay(*)</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayVay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%" OnTextChanged="txtNgayVay_TextChanged" AutoPostBack="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh(*)</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngân hàng(*)</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Ngân hàng"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNganHang"
                            runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Lần thứ(*)</label>
                        <asp:TextBox ID="txtLanThu" runat="server" class="form-control" placeholder="Lần thứ" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtLanThu" ValidChars="" />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số tiền giải ngân(*)(VNĐ)</label>
                        <asp:TextBox ID="txtSoTienGiaiNganChinh" runat="server" class="form-control" placeholder="Số tiền giải ngân" onchange="SoTienGiaiNganChinh()" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtSoTienGiaiNganChinh" ValidChars=".," />
                    </div>
                    <%--<div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số tiền giải ngân bằng chữ(*)</label>
                        <asp:TextBox ID="txtSoTienGianNganBangChu" runat="server" class="form-control" placeholder="Số tiền giải ngân bằng chữ(*)" Width="98%" disabled></asp:TextBox>
                    </div>--%>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Mục đích sử dụng(*)</label>
                        <asp:TextBox ID="txtMucDichSuDung" runat="server" class="form-control" placeholder="Mục đích sử dụng" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Kỳ hạn(*)(Tháng)</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Kỳ hạn"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlKyHan" OnSelectedIndexChanged="txtNgayVay_TextChanged" AutoPostBack="true"
                            runat="server">
                            <asp:ListItem Value="">Chọn</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                            <asp:ListItem Value="13">13</asp:ListItem>
                            <asp:ListItem Value="14">14</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="16">16</asp:ListItem>
                            <asp:ListItem Value="17">17</asp:ListItem>
                            <asp:ListItem Value="18">18</asp:ListItem>
                            <asp:ListItem Value="19">19</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="21">21</asp:ListItem>
                            <asp:ListItem Value="22">22</asp:ListItem>
                            <asp:ListItem Value="23">23</asp:ListItem>
                            <asp:ListItem Value="24">24</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="26">26</asp:ListItem>
                            <asp:ListItem Value="27">27</asp:ListItem>
                            <asp:ListItem Value="28">28</asp:ListItem>
                            <asp:ListItem Value="29">29</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="31">31</asp:ListItem>
                            <asp:ListItem Value="32">32</asp:ListItem>
                            <asp:ListItem Value="33">33</asp:ListItem>
                            <asp:ListItem Value="34">34</asp:ListItem>
                            <asp:ListItem Value="35">35</asp:ListItem>
                            <asp:ListItem Value="36">36</asp:ListItem>
                            <asp:ListItem Value="37">37</asp:ListItem>
                            <asp:ListItem Value="38">38</asp:ListItem>
                            <asp:ListItem Value="39">39</asp:ListItem>
                            <asp:ListItem Value="40">40</asp:ListItem>
                            <asp:ListItem Value="41">41</asp:ListItem>
                            <asp:ListItem Value="42">42</asp:ListItem>
                            <asp:ListItem Value="43">43</asp:ListItem>
                            <asp:ListItem Value="44">44</asp:ListItem>
                            <asp:ListItem Value="45">45</asp:ListItem>
                            <asp:ListItem Value="46">46</asp:ListItem>
                            <asp:ListItem Value="47">47</asp:ListItem>
                            <asp:ListItem Value="48">48</asp:ListItem>
                            <asp:ListItem Value="49">49</asp:ListItem>
                            <asp:ListItem Value="50">50</asp:ListItem>
                            <asp:ListItem Value="51">51</asp:ListItem>
                            <asp:ListItem Value="52">52</asp:ListItem>
                            <asp:ListItem Value="53">53</asp:ListItem>
                            <asp:ListItem Value="54">54</asp:ListItem>
                            <asp:ListItem Value="55">55</asp:ListItem>
                            <asp:ListItem Value="56">56</asp:ListItem>
                            <asp:ListItem Value="57">57</asp:ListItem>
                            <asp:ListItem Value="58">58</asp:ListItem>
                            <asp:ListItem Value="59">59</asp:ListItem>
                            <asp:ListItem Value="60">60</asp:ListItem>
                            <asp:ListItem Value="61">61</asp:ListItem>
                            <asp:ListItem Value="62">62</asp:ListItem>
                            <asp:ListItem Value="63">63</asp:ListItem>
                            <asp:ListItem Value="64">64</asp:ListItem>
                            <asp:ListItem Value="65">65</asp:ListItem>
                            <asp:ListItem Value="66">66</asp:ListItem>
                            <asp:ListItem Value="67">67</asp:ListItem>
                            <asp:ListItem Value="68">68</asp:ListItem>
                            <asp:ListItem Value="69">69</asp:ListItem>
                            <asp:ListItem Value="70">70</asp:ListItem>
                            <asp:ListItem Value="71">71</asp:ListItem>
                            <asp:ListItem Value="72">72</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày đáo hạn(*)</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayDaoHan" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%" disabled></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Hạn trả cuối cùng(*)</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtHanTraCuoiCung" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Lãi suất(*)(%/năm)</label>
                        <asp:TextBox ID="txtLaiSuat" runat="server" class="form-control" placeholder="Lãi suất" onchange="KhongThue()" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtLaiSuat" ValidChars="." />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Lãi suất quá hạn(*)(%/năm)</label>
                        <asp:TextBox ID="txtLaiSuatQuaHan" runat="server" class="form-control" placeholder="Lãi suất quá hạn" onchange="KhongThue()" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtLaiSuatQuaHan" ValidChars="." />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày trả lãi đầu tiên(*)</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayTraLaiDauTien" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Duy trì trả lãi tự động(*)</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Duy trì trả lãi tự động"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDuyTriTraLaiTuDong"
                            runat="server">
                            <asp:ListItem Value="0">Có</asp:ListItem>
                            <asp:ListItem Value="1">Không</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Phương thức giải ngân(*)</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Phương thức giải ngân"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlPhuongThucGiaiNgan"
                            runat="server">
                            <asp:ListItem Value="0">Chuyển khoản</asp:ListItem>
                            <asp:ListItem Value="1">Tiền mặt</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số tài khoản thanh toán(*)</label>
                        <asp:TextBox ID="txtTaiKhoanVayChinh" runat="server" class="form-control" Width="98%"></asp:TextBox>
                    </div>
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin giải ngân</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Bên thụ hưởng</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title=""
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlBenThuHuong"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tài khoản thụ hưởng</label>
                        <asp:TextBox ID="txtSoTaiKhoan" runat="server" class="form-control" placeholder="Tài khoản thụ hưởng" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số hóa đơn</label>
                        <asp:TextBox ID="txtSoHoaDon" runat="server" class="form-control" placeholder="Số hóa đơn" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày giải ngân</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayGiaiNgan" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Mục đích</label>
                        <asp:TextBox ID="txtMucDich" runat="server" class="form-control" placeholder="Mục đích" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3" id="divsotienhoadon" runat="server" visible="false">
                        <label for="exampleInputEmail1">Số tiền trên hóa đơn</label>
                        <asp:TextBox ID="txtSoTienTrenHoaDon" runat="server" class="form-control" placeholder="Số tiền trên hóa đơn" onchange="SoTienTrenHoaDon()" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtSoTienTrenHoaDon" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số tiền giải ngân</label>
                        <asp:TextBox ID="txtSoTienGiaiNgan" runat="server" class="form-control" placeholder="Số tiền giải ngân" onchange="SoTienGiaiNgan()" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtSoTienGiaiNgan" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Hạng mục thu - chi</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <asp:CheckBox ID="ckLoai" runat="server" Enabled="false" />
                            </span>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Hạng mục chi" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlHangMucChi"  runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtSave" runat="server" class="btn btn-app bg-green" OnClick="lbtSave_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lbtTaoGiaMoi" runat="server" class="btn btn-app bg-warning" OnClick="lbtTaoGiaMoi_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>

                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12" style="overflow: auto; width: 100%;">
                                <asp:GridView ID="gvChiTiet" runat="server" AutoGenerateColumns="false" OnRowCommand="gvChiTiet_RowCommand"
                                    EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SoTaiKhoan" HeaderText="Số tài khoản" />
                                        <asp:BoundField DataField="TenBenHuongThu" HeaderText="Bên hưởng thụ" />
                                        <asp:BoundField DataField="SoHoaDon" HeaderText="Số hóa đơn" />
                                        <asp:BoundField DataField="NgayGiaiNgan" HeaderText="Ngày giải ngân" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="MucDich" HeaderText="Mục đích" />
                                        <%--<asp:BoundField DataField="SoTienTrenHoaDon" HeaderText="Số tiền trên hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />--%>
                                        <asp:BoundField DataField="SoTienGiaiNgan" HeaderText="Số tiền giải ngân" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="TenHangMucChi" HeaderText="Hạng mục chi" />
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                        HorizontalAlign="Right" />
                                    <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                    <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu khoản vay</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo khoản vay mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách khoản vay</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server" Text="Ngân hàng"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Ngân hàng" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlKhachHangSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="TrangThaiText" HeaderText="Trạng thái" />--%>
                                    <asp:BoundField DataField="NgayVay" HeaderText="Ngày vay" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenNganHang" HeaderText="Ngân hàng" />
                                    <asp:BoundField DataField="LanThu" HeaderText="Lần thứ" />
                                    <asp:BoundField DataField="SoTienGiaiNgan" HeaderText="Số tiền giải ngân" DataFormatString="{0:N0}" />
                                    <%--<asp:BoundField DataField="SoTienGiaiNganBangChu" HeaderText="" />--%>
                                    <asp:BoundField DataField="MucDichSuDung" HeaderText="Mục đích sử dụng" />
                                    <%--<asp:BoundField DataField="KyHan" HeaderText="" />--%>
                                    <asp:BoundField DataField="TenKyHan" HeaderText="Tên kỳ hạn" />
                                    <asp:BoundField DataField="NgayDaoHan" HeaderText="Ngày đáo hạn" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="HanTraCuoiCung" HeaderText="Hạn trả cuối cùng" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="LaiSuat" HeaderText="Lãi suất" />
                                    <asp:BoundField DataField="LaiSuatQuaHan" HeaderText="Lãi suất quá hạn" />
                                    <asp:BoundField DataField="NgayTraLaiDauTien" HeaderText="Ngày trả lãi đầu tiên" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="DuyTriTraLaiTuDong" HeaderText="Duy trì trả lại tự động" />

                                    <asp:TemplateField HeaderText="Duy trì trả lại tự động">
                                        <ItemTemplate>
                                            <asp:Label ID="lblduytri" runat="server" Text='<%# (Convert.ToBoolean(Eval("DuyTriTraLaiTuDong")) == true) ? "Có" : "Không"   %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <%--<asp:BoundField DataField="PhuongThucGiaiNgan" HeaderText="Phương thức giải ngân" />--%>
                                    <asp:BoundField DataField="TenPhuongThucGiaiNgan" HeaderText="Phương thức giải ngân" />
                                    <asp:BoundField DataField="SoTaiKhoanTra" HeaderText="Số tài khoản thanh toán" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>

            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdChung" runat="server" Value="" />
            <asp:HiddenField ID="hdChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdForm" runat="server" Value="" />
            <asp:HiddenField ID="hdNgayTao" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSuHopDong" runat="server" Value="" />
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function SoTienGiaiNganChinh() {
                    var txtSoTienGiaiNganChinh = parseFloat(getvalue(document.getElementById("<%=txtSoTienGiaiNganChinh.ClientID %>").value));
                    if (txtSoTienGiaiNganChinh != '0') {
                        document.getElementById("<%=txtSoTienGiaiNganChinh.ClientID %>").value = (txtSoTienGiaiNganChinh).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtSoTienGiaiNganChinh.ClientID %>").value = '0';
                    }
                }

                function SoTienTrenHoaDon() {
                    var txtSoTienTrenHoaDon = parseFloat(getvalue(document.getElementById("<%=txtSoTienTrenHoaDon.ClientID %>").value));
                    if (txtSoTienTrenHoaDon != '0') {
                        document.getElementById("<%=txtSoTienTrenHoaDon.ClientID %>").value = (txtSoTienTrenHoaDon).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtSoTienGiaiNgan.ClientID %>").value = (txtSoTienTrenHoaDon).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtSoTienTrenHoaDon.ClientID %>").value = '0';
                        document.getElementById("<%=txtSoTienTrenHoaDon.ClientID %>").value = '0';
                    }
                }
                function SoTienGiaiNgan() {
                    var txtSoTienGiaiNgan = parseFloat(getvalue(document.getElementById("<%=txtSoTienGiaiNgan.ClientID %>").value));
                    if (txtSoTienGiaiNgan != '0') {
                        document.getElementById("<%=txtSoTienGiaiNgan.ClientID %>").value = (txtSoTienGiaiNgan).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtSoTienTrenHoaDon.ClientID %>").value = (txtSoTienGiaiNgan).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtSoTienGiaiNgan.ClientID %>").value = '0';
                        document.getElementById("<%=txtSoTienTrenHoaDon.ClientID %>").value = '0';
                    }
                }
            </script>
            <ajaxToolkit:ModalPopupExtender ID="mpHopDong" runat="server" CancelControlID="btnDongLichSuHopDong"
                Drag="True" TargetControlID="hdLichSuHopDong" BackgroundCssClass="modalBackground" PopupControlID="pnLichSuHopDong"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSuHopDong" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                                <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" />
                                <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSuHopDong" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="NhomVatLieu" HeaderText="Nhóm vật liệu" />
                                <asp:BoundField DataField="LoaiVatLieu" HeaderText="Loại vật liệu" />
                                <asp:BoundField DataField="DonViTinh" HeaderText="Đơn vị tính" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <asp:HiddenField ID="hdCongTrinh" runat="server" Value="" />
            <ajaxToolkit:ModalPopupExtender ID="mpCongTrinh" runat="server" CancelControlID="btnDongCongTrinh"
                Drag="True" TargetControlID="hdCongTrinh" BackgroundCssClass="modalBackground" PopupControlID="pnCongTrinh"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnCongTrinh" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Thêm khách hàng
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">

                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Ngân hàng</label>
                            <asp:DropDownList CssClass="form-control" Width="98%" data-toggle="tooltip" data-original-title="Ngân hàng"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiNhaCungCap" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Tên Ngân hàng</label>
                            <asp:TextBox ID="txtTenNhaCungCap" runat="server" class="form-control" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Số điện thoại</label>
                            <asp:TextBox ID="txtSoDienThoai" runat="server" class="form-control" placeholder="Số điện thoại" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtSoDienThoai" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Mã số thuế</label>
                            <asp:TextBox ID="txtMaSoThue" runat="server" class="form-control" placeholder="Mã số thuế" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtMaSoThue" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Địa chỉ</label>
                            <asp:TextBox ID="txtDiaChi" runat="server" class="form-control" placeholder="Địa chỉ" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnLuuCongTrinh" runat="server" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuuCongTrinh_Click" />
                        <asp:Button ID="btnDongCongTrinh" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
