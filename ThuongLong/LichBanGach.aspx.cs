﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class LichBanGach : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmLichBanGach";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmLichBanGach", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();

                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;

                        var firstDayOfMonth = dateTimenow.AddDays(-1);
                        var lastDayOfMonth = dateTimenow.AddDays(1);

                        //var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        //var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        LoadKhachHangSearch();
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem(ref string IDHopDong, ref string IDHopDongBom, ref string IDChiTietKD)
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            if (txtKLThucXuat.Text == "" || double.Parse(txtKLThucXuat.Text) == 0)
            {
                s += " - Nhập số lượng tạm tính<br />";
            }
            if (txtKLKhachHang.Text == "" || double.Parse(txtKLKhachHang.Text) == 0)
            {
                s += " - Nhập số lượng khách hàng mua<br />";
            }
            if (s == "")
            {
                vatlieu.sp_LichBanGach_CheckThem(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlCongTrinh), txtHangMuc.Text.Trim().ToUpper(), GetDrop(dlNhanVien), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh),
                      ref IDHopDong, ref IDHopDongBom, ref IDChiTietKD, ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua(ref string IDHopDong, ref string IDHopDongBom, ref string IDChiTietKD)
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            if (txtKLThucXuat.Text == "" || double.Parse(txtKLThucXuat.Text) == 0)
            {
                s += " - Nhập số lượng tạm tính<br />";
            }
            if (txtKLKhachHang.Text == "" || double.Parse(txtKLKhachHang.Text) == 0)
            {
                s += " - Nhập số lượng khách hàng mua<br />";
            }
            if (s == "")
            {
                vatlieu.sp_LichBanGach_CheckSua(new Guid(hdID.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlCongTrinh), txtHangMuc.Text.Trim().ToUpper(), GetDrop(dlNhanVien), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh),
                      ref IDHopDong, ref IDHopDongBom, ref IDChiTietKD, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string idhopdong = "";
            string idhopdongbom = "";
            string idchitietkinhdoanh = "";
            if (hdID.Value == "")
            {
                if (CheckThem(ref idhopdong, ref idhopdongbom, ref idchitietkinhdoanh) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var LichBanGach = new tblLichBanGach()
                        {
                            ID = Guid.NewGuid(),
                            GioXuat = TimeSpan.Parse(txtGioXuat.Text),
                            NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhaCungCap = GetDrop(dlNhaCungCap),
                            IDCongTrinh = GetDrop(dlCongTrinh),
                            HangMuc = txtHangMuc.Text.Trim().ToUpper(),
                            IDChiTietKinhDoanh = new Guid(idchitietkinhdoanh),
                            IDNVKD = GetDrop(dlNhanVien),
                            IDNhomVatLieu = GetDrop(dlNhomVatLieu),
                            IDLoaiVatLieu = GetDrop(dlLoaiVatLieu),
                            IDDonViTinh = GetDrop(dlDonViTinh),
                            IDHopDong = new Guid(idhopdong),
                            KLThucXuat = double.Parse(txtKLThucXuat.Text),
                            KLKhachHang = double.Parse(txtKLKhachHang.Text),
                            CuLyVanChuyen = txtCuLyVanChuyen.Text.Trim() == "" ? 0 : double.Parse(txtCuLyVanChuyen.Text),

                            NguoiThuTien = txtNguoiThuTien.Text.Trim(),
                            KLDaBan = 0,
                            KLDaXuat = 0,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            TrangThaiHoanThanh = "Chưa hoàn thành",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblLichBanGaches.InsertOnSubmit(LichBanGach);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua(ref idhopdong, ref idhopdongbom, ref idchitietkinhdoanh) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblLichBanGaches
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.GioXuat = TimeSpan.Parse(txtGioXuat.Text);
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                            query.IDCongTrinh = GetDrop(dlCongTrinh);
                            query.HangMuc = txtHangMuc.Text.Trim().ToUpper();
                            query.IDChiTietKinhDoanh = new Guid(idchitietkinhdoanh);
                            query.IDNVKD = GetDrop(dlNhanVien);
                            query.IDNhomVatLieu = GetDrop(dlNhomVatLieu);
                            query.IDLoaiVatLieu = GetDrop(dlLoaiVatLieu);
                            query.IDDonViTinh = GetDrop(dlDonViTinh);
                            query.IDHopDong = new Guid(idhopdong);
                            query.KLThucXuat = double.Parse(txtKLThucXuat.Text);
                            query.KLKhachHang = txtCuLyVanChuyen.Text.Trim() == "" ? 0 : double.Parse(txtKLKhachHang.Text);
                            query.CuLyVanChuyen = double.Parse(txtCuLyVanChuyen.Text);
                            query.NguoiThuTien = txtNguoiThuTien.Text.Trim();

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin lịch bán gạch đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh_SelectedIndexChanged(sender, e);
            hdID.Value = "";
            txtKLThucXuat.Text = "";
            txtKLKhachHang.Text = "";
            txtCuLyVanChuyen.Text = "";
            txtHangMuc.Text = "";
            txtKyThuat.Text = "";
            txtNguoiThuTien.Text = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblLichBanGaches
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_LichBanGach_CheckSuaGV(query.ID, query.IDCongTrinh, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            txtGioXuat.Text = query.GioXuat.ToString();
                            txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            LoadNhaCungCap();
                            dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                            if (dlCongTrinh.Items.Count == 0)
                                LoadCongTrinh();
                            dlCongTrinh.SelectedValue = query.IDCongTrinh.ToString();
                            if (dlNhanVien.Items.Count == 0)
                                LoadNVKD();
                            dlNhanVien.SelectedValue = query.IDNVKD.ToString();

                            if (dlNhomVatLieu.Items.Count == 0)
                                LoadNhomVatLieu();
                            dlNhomVatLieu.SelectedValue = query.IDNhomVatLieu.ToString();

                            if (dlLoaiVatLieu.Items.Count == 0)
                                LoadLoaiVatLieu();
                            dlLoaiVatLieu.SelectedValue = query.IDLoaiVatLieu.ToString();

                            if (dlDonViTinh.Items.Count == 0)
                                LoadDonVitinh();
                            dlDonViTinh.SelectedValue = query.IDDonViTinh.ToString();

                            txtHangMuc.Text = query.HangMuc.ToString();
                            txtKLThucXuat.Text = query.KLThucXuat.ToString();
                            txtKLKhachHang.Text = query.KLKhachHang.ToString();
                            txtCuLyVanChuyen.Text = query.CuLyVanChuyen.ToString();

                            txtNguoiThuTien.Text = query.NguoiThuTien.ToString();
                            hdID.Value = id;
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_LichBanGach_CheckXoa(query.ID, query.IDChiNhanh, query.IDNhaCungCap, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblLichBanGach_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblLichBanGaches.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "hoanthanh")
                {
                    if (query.TrangThaiHoanThanh == "Đã hoàn thành")
                    {
                        query.TrangThaiHoanThanh = "Chưa hoàn thành";
                    }
                    else
                    {
                        query.TrangThaiHoanThanh = "Đã hoàn thành";
                    }
                    db.SubmitChanges();
                    Search(int.Parse(hdPage.Value));
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_LichBanGach_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin lịch bán gạch đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                            if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                            {
                                gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                                e.Row.Cells[i].BackColor = Color.LightCyan;
                            }
                    }
                }
            }
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadKhachHangSearch();
        }
        void LoadKhachHangSearch()
        {
            dlNhaCungCapSearch.Items.Clear();
            var query = vatlieu.sp_LichBanGach_LoadNhaCungCapSearch(dlChiNhanhSearch.SelectedValue, DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)));
            dlNhaCungCapSearch.DataSource = query;
            dlNhaCungCapSearch.DataBind();
            dlNhaCungCapSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            var query = vatlieu.sp_LichBanGach_Search(dlChiNhanhSearch.SelectedValue, dlNhaCungCapSearch.SelectedValue, DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 15, page);
            GV.DataSource = query;
            GV.DataBind();
        }

        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        void LoadNhaCungCap()
        {
            if (txtNgayThang.Text != "" && dlChiNhanh.SelectedValue != "")
            {
                dlDonViTinh.Items.Clear();
                dlNhomVatLieu.Items.Clear();
                dlLoaiVatLieu.Items.Clear();
                dlNhanVien.Items.Clear();
                dlNhaCungCap.Items.Clear();
                dlCongTrinh.Items.Clear();
                var query = vatlieu.sp_LichBanGach_LoadNhaCungCap(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh));
                dlNhaCungCap.DataSource = query;
                dlNhaCungCap.DataBind();
                if (dlNhaCungCap.Items.Count == 1)
                {
                    LoadCongTrinh();
                }
                else
                {
                    dlNhaCungCap.Items.Insert(0, new ListItem("Chọn", ""));
                    dlCongTrinh.Items.Clear();
                    dlNhanVien.Items.Clear();
                    dlLoaiVatLieu.Items.Clear();
                }
            }
            else
            {
                dlDonViTinh.Items.Clear();
                dlNhomVatLieu.Items.Clear();
                dlLoaiVatLieu.Items.Clear();
                dlNhanVien.Items.Clear();
                dlNhaCungCap.Items.Clear();
                dlCongTrinh.Items.Clear();
            }
        }
        void LoadCongTrinh()
        {
            if (txtNgayThang.Text != "" && dlNhaCungCap.SelectedValue != "")
            {
                dlDonViTinh.Items.Clear();
                dlNhomVatLieu.Items.Clear();
                dlCongTrinh.Items.Clear();
                dlLoaiVatLieu.Items.Clear();
                dlNhanVien.Items.Clear();
                var query = vatlieu.sp_LichBanGach_LoadCongTrinh(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap));
                dlCongTrinh.DataSource = query;
                dlCongTrinh.DataBind();
                if (dlCongTrinh.Items.Count == 1)
                {
                    LoadNhomVatLieu();
                    LoadNVKD();
                }
                else
                {
                    dlCongTrinh.Items.Insert(0, new ListItem("Chọn", ""));
                    dlLoaiVatLieu.Items.Clear();
                    dlNhanVien.Items.Clear();
                }
            }
            else
            {
                dlDonViTinh.Items.Clear();
                dlNhomVatLieu.Items.Clear();
                dlCongTrinh.Items.Clear();
                dlLoaiVatLieu.Items.Clear();
                dlNhanVien.Items.Clear();
            }
        }
        void LoadNhomVatLieu()
        {
            if (txtNgayThang.Text != "" && dlCongTrinh.SelectedValue != "")
            {
                dlDonViTinh.Items.Clear();
                dlLoaiVatLieu.Items.Clear();
                dlNhomVatLieu.Items.Clear();
                var query = vatlieu.sp_LichBanGach_LoadNhomVatLieu(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh));
                dlNhomVatLieu.DataSource = query;
                dlNhomVatLieu.DataBind();
                if (dlNhomVatLieu.Items.Count == 1)
                {
                    LoadLoaiVatLieu();
                }
                else
                {
                    dlNhomVatLieu.Items.Insert(0, new ListItem("Chọn", ""));
                }
            }
            else
            {
                dlDonViTinh.Items.Clear();
                dlLoaiVatLieu.Items.Clear();
                dlNhomVatLieu.Items.Clear();
            }
        }
        void LoadLoaiVatLieu()
        {
            if (txtNgayThang.Text != "" && dlNhomVatLieu.SelectedValue != "")
            {
                dlDonViTinh.Items.Clear();
                dlLoaiVatLieu.Items.Clear();
                var query = vatlieu.sp_LichBanGach_LoadLoaiVatLieu(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), GetDrop(dlNhomVatLieu));
                dlLoaiVatLieu.DataSource = query;
                dlLoaiVatLieu.DataBind();
                if (dlLoaiVatLieu.Items.Count == 1)
                {
                    LoadDonVitinh();
                }
                else
                {
                    dlLoaiVatLieu.Items.Insert(0, new ListItem("Chọn", ""));
                }
            }
            else
            {
                dlLoaiVatLieu.Items.Clear();
            }
        }
        void LoadDonVitinh()
        {
            if (txtNgayThang.Text != "" && dlLoaiVatLieu.SelectedValue != "")
            {
                dlDonViTinh.Items.Clear();
                var query = vatlieu.sp_LichBanGach_LoadDonViTinh(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), GetDrop(dlLoaiVatLieu));
                dlDonViTinh.DataSource = query;
                dlDonViTinh.DataBind();
                if (dlDonViTinh.Items.Count == 1)
                {
                    //LoadLoaiDa();
                }
                else
                {
                    dlDonViTinh.Items.Insert(0, new ListItem("Chọn", ""));
                }
            }
            else
            {
                dlDonViTinh.Items.Clear();
            }
        }
        void LoadNVKD()
        {
            if (txtNgayThang.Text != "" && dlCongTrinh.SelectedValue != "")
            {
                dlNhanVien.Items.Clear();
                var query = vatlieu.sp_LichBanGach_LoadNVKD(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh));
                dlNhanVien.DataSource = query;
                dlNhanVien.DataBind();
                if (dlNhanVien.Items.Count == 1)
                {
                    //LoadLoaiDa();
                }
                else
                {
                    dlNhanVien.Items.Insert(0, new ListItem("Chọn", ""));
                }
            }
            else
            {
                dlNhanVien.Items.Clear();
            }
        }
        protected void LoadNVKDDM()
        {
            dlNhanVien.Items.Clear();
            var query = (from p in db.tblNhanSus
                         where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenNhanVien
                         }).OrderBy(p => p.Ten);
            dlNhanVien.DataSource = query;
            dlNhanVien.DataBind();
            dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }
        protected void dlNhaCungCap_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCongTrinh();
        }
        protected void dlCongTrinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhomVatLieu();
            LoadNVKD();
        }

        protected void dlNhomVatLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiVatLieu();
        }

        protected void dlLoaiVatLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDonVitinh();
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_LichBanGach_PrintResult> query = vatlieu.sp_LichBanGach_Print(dlChiNhanhSearch.SelectedValue, dlNhaCungCapSearch.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();


            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "LỊCH BÁN GẠCH";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 7);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("LỊCH BÁN GẠCH TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                }
                else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("LỊCH BÁN GẠCH NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 14);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 14);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Giờ xuất",
                    "Ngày tháng",
                    "Chi nhánh",
                    "Khách hàng",
                    "Công trình",
                    "Hạng mục",
                    "Nhân viên",
                    "Loại vật liệu",
                    "Đơn vị tính",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Cự ly vận chuyển",
                    "Người thu tiền",
                    "Hoàn thành"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Giờ xuất",
                    "Ngày tháng",
                    "Chi nhánh",
                    "Khách hàng",
                    "Công trình",
                    "Hạng mục",
                    "Nhân viên",
                    "Loại vật liệu",
                    "Đơn vị tính",
                    "KL tạm tính",
                    "KL khách hàng",
                    "Cự ly vận chuyển",
                    "Người thu tiền",
                    "Hoàn thành"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 3, 3);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 4, 4);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 5, 5);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 6, 6);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 7, 7);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 8, 8);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 9, 9);
                sheet.AddMergedRegion(cra);

                cra = new CellRangeAddress(4, 4, 10, 11);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 12, 12);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 13, 13);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 14, 14);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.GioXuat.ToString());
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.NgayThang);
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(item.TenNhaCungCap);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(item.TenCongTrinh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(item.HangMuc);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(item.TenNhanVien);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(item.TenLoaiVatLieu);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(item.TenDonViTinh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(double.Parse(item.KLThucXuat.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(11);
                    cell.SetCellValue(double.Parse(item.KLKhachHang.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(12);
                    cell.SetCellValue(double.Parse(item.CuLyVanChuyen.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(13);
                    cell.SetCellValue(item.NguoiThuTien);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(14);
                    cell.SetCellValue(item.TrangThaiHoanThanh);
                    cell.CellStyle = styleBody;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= 14; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 8);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(9);
                cell.CellFormula = "COUNT(A" + RowDau.ToString() + ":A" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(10);
                cell.CellFormula = "SUM(K" + RowDau.ToString() + ":K" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(11);
                cell.CellFormula = "SUM(L" + RowDau.ToString() + ":L" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                #endregion

                #region[Set Footer]

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;
                //tên
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 1);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(0);
                cell.SetCellValue("Người xuất hàng");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(2);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 4, 5);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(4);
                cell.SetCellValue("Trưởng bộ phận");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 8);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(6);
                cell.SetCellValue("Lái xe");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 9, 10);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(9);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 11, 13);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(11);
                cell.SetCellValue("Giám sát");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                #endregion

                sheet.SetColumnWidth(4, 7120);
                sheet.SetColumnWidth(5, 3000);
                sheet.SetColumnWidth(6, 7000);
                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "LỊCH BÁN GẠCH -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                         ".xls";
                    }
                    else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "LỊCH BÁN GẠCH -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                         "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }

    }
}