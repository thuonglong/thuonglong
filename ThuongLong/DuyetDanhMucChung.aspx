﻿<%@ Page Title="Duyệt danh mục" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetDanhMucChung.aspx.cs" Inherits="ThuongLong.DuyetDanhMucChung" %>

<%@ Register Src="DuyetDanhMuc/uc_DuyetNhomThietBi.ascx" TagName="uc_DuyetNhomThietBi" TagPrefix="uc1" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetLoaiThietBi.ascx" TagName="uc_DuyetLoaiThietBi" TagPrefix="uc2" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetNhomVatLieu.ascx" TagName="uc_DuyetNhomVatLieu" TagPrefix="uc3" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetLoaiVatLieu.ascx" TagName="uc_DuyetLoaiVatLieu" TagPrefix="uc4" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetDonViTinh.ascx" TagName="uc_DuyetDonViTinh" TagPrefix="uc5" %>

<%@ Register Src="DuyetDanhMuc/uc_DuyetTrinhDo.ascx" TagName="uc_DuyetTrinhDo" TagPrefix="uc6" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetChuyenMon.ascx" TagName="uc_DuyetChuyenMon" TagPrefix="uc7" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetBoPhan.ascx" TagName="uc_DuyetBoPhan" TagPrefix="uc8" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetChucVu.ascx" TagName="uc_DuyetChucVu" TagPrefix="uc9" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetHangMucThuChi.ascx" TagName="uc_DuyetHangMucThuChi" TagPrefix="uc10" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetNhomNhaCungCap.ascx" TagName="uc_DuyetNhomNhaCungCap" TagPrefix="uc11" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetNhaCungCap.ascx" TagName="uc_DuyetNhaCungCap" TagPrefix="uc12" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetLoaiKeHoach.ascx" TagName="uc_DuyetLoaiKeHoach" TagPrefix="uc13" %>

<%@ Register Src="DuyetDanhMuc/uc_DuyetDoSut.ascx" TagName="uc_DuyetDoSut" TagPrefix="uc14" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetYCDB.ascx" TagName="uc_DuyetYCDB" TagPrefix="uc15" %>
<%@ Register Src="DuyetDanhMuc/uc_DuyetHinhThucBom.ascx" TagName="uc_DuyetHinhThucBom" TagPrefix="uc16" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmNhomNhaCungCap" runat="server" HeaderText="Nhóm nhà cung cấp">
                            <ContentTemplate>
                                <uc11:uc_DuyetNhomNhaCungCap ID="uc_DuyetNhomNhaCungCap1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmNhaCungCap" runat="server" HeaderText="Nhà cung cấp">
                            <ContentTemplate>
                                <uc12:uc_DuyetNhaCungCap ID="uc_DuyetNhaCungCap1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Nhóm thiết bị">
                            <ContentTemplate>
                                <uc1:uc_DuyetNhomThietBi ID="uc_DuyetNhomThietBi2" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmNhomThietBi" runat="server" HeaderText="Nhóm thiết bị">
                            <ContentTemplate>
                                <uc1:uc_DuyetNhomThietBi ID="uc_DuyetNhomThietBi" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmLoaiThietBi" runat="server" HeaderText="Loại thiết bị">
                            <ContentTemplate>
                                <uc2:uc_DuyetLoaiThietBi ID="uc_DuyetLoaiThietBi" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmNhomVatLieu" runat="server" HeaderText="Nhóm vật liệu">
                            <ContentTemplate>
                                <uc3:uc_DuyetNhomVatLieu ID="uc_DuyetNhomVatLieu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmLoaiVatLieu" runat="server" HeaderText="Loại vật liệu">
                            <ContentTemplate>
                                <uc4:uc_DuyetLoaiVatLieu ID="uc_DuyetLoaiVatLieu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmDonViTinh" runat="server" HeaderText="Đơn vị tính">
                            <ContentTemplate>
                                <uc5:uc_DuyetDonViTinh ID="uc_DuyetDonViTinh" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmTrinhDo" runat="server" HeaderText="Trình độ">
                            <ContentTemplate>
                                <uc6:uc_DuyetTrinhDo ID="uc_DuyetTrinhDo" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmChuyenMon" runat="server" HeaderText="Chuyên môn">
                            <ContentTemplate>
                                <uc7:uc_DuyetChuyenMon ID="uc_DuyetChuyenMon" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmBoPhan" runat="server" HeaderText="Bộ phận">
                            <ContentTemplate>
                                <uc8:uc_DuyetBoPhan ID="uc_DuyetBoPhan" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmChucVu" runat="server" HeaderText="Chức vụ">
                            <ContentTemplate>
                                <uc9:uc_DuyetChucVu ID="uc_DuyetChucVu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmHangMucThuChi" runat="server" HeaderText="Hạng mục thu chi">
                            <ContentTemplate>
                                <uc10:uc_DuyetHangMucThuChi ID="uc_DuyetHangMucThuChi1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmLoaiKeHoach" runat="server" HeaderText="Loại kế hoạch">
                            <ContentTemplate>
                                <uc13:uc_DuyetLoaiKeHoach ID="uc_DuyetLoaiKeHoach1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmDoSut" runat="server" HeaderText="Độ sụt">
                            <ContentTemplate>
                                <uc14:uc_DuyetDoSut ID="uc_DuyetDoSut1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmYCDB" runat="server" HeaderText="Yêu cầu đặc biệt">
                            <ContentTemplate>
                                <uc15:uc_DuyetYCDB ID="uc_DuyetYCDB1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmHinhThucBom" runat="server" HeaderText="Hình thức bơm">
                            <ContentTemplate>
                                <uc16:uc_DuyetHinhThucBom ID="uc_DuyetHinhThucBom1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
