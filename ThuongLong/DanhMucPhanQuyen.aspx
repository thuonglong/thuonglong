﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DanhMucPhanQuyen.aspx.cs" Inherits="ThuongLong.DanhMucPhanQuyen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Tên nghiệp vụ</label>
                                <asp:TextBox ID="txtTenNghiepVu" runat="server" class="form-control" placeholder="Tên nghiệp vụ"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Tag</label>
                                <asp:TextBox ID="txtTag" runat="server" class="form-control" placeholder="tag"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">STT</label>
                                <asp:TextBox ID="txtSTT" runat="server" class="form-control" placeholder="Thứ tự sắp xếp"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Kích hoạt</label>
                                <label class="tgl" style="font-size: 13px">
                                    <asp:CheckBox ID="ckTrangThai" Checked="false" runat="server" />
                                    <span data-on="Bật" data-off="Tắt"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnSave" runat="server" Text="Lưu" class="btn btn-primary" OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Hủy" class="btn btn-warning" OnClick="btnCancel_Click" />
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-6">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control" placeholder="Tên đăng nhập"></asp:TextBox>
                            </div>
                            <div class="form-group col-lg-6">
                                <asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" class="btn btn-primary" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnSua" runat="server" Text="Sửa" class="btn btn-primary" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnXoa" runat="server" Text="Xóa" class="btn btn-danger" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Khóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnKhoa" runat="server" Text='<%#Eval("TrangThai")%>' class="btn btn-info"
                                                CommandArgument='<%#Eval("ID")%>' ToolTip="Khóa" CommandName='<%#Eval("TrangThai")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TenNghiepVu" HeaderText="Tên nghiệp vụ" />
                                    <asp:BoundField DataField="Tag" HeaderText="Tag" />
                                    <asp:BoundField DataField="STT" HeaderText="Số thứ tự" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
