﻿<%@ Page Title="Thuê bơm" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ThueBom.aspx.cs" Inherits="ThuongLong.ThueBom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<style>
        .select2-container {
            z-index: 100000;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin giá</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày tháng</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Khách hàng</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCap" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div11" runat="server" visible="false">
                        <label for="exampleInputEmail1">Công trình</label>
                        <asp:DropDownList CssClass="form-control select2" data-toggle="tooltip" data-original-title="Công trình"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCongTrinh" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3" id="div111" runat="server" visible="false">
                        <label for="exampleInputEmail1">Nhân viên kinh doanh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhân viên kinh doanh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhanVienKinhDoanh" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3" id="div12" runat="server">
                        <label for="exampleInputEmail1">Hình thức bơm</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Hình thức bơm"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlHinhThucBom" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xe bơm</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Xe bơm"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXeBom" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3" id="div14" runat="server" visible="false">
                        <label for="exampleInputEmail1">Đơn giá hóa đơn</label>
                        <asp:TextBox ID="txtDonGiaHoaDon" runat="server" class="form-control" placeholder="Đơn giá hóa đơn" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaHoaDon" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3" id="div15" runat="server" visible="false">
                        <label for="exampleInputEmail1">Đơn giá thanh toán</label>
                        <asp:TextBox ID="txtDonGiaThanhToan" runat="server" class="form-control" placeholder="Đơn giá thanh toán" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaThanhToan" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tiền hóa đơn</label>
                        <asp:TextBox ID="txtTienHoaDon" runat="server" class="form-control" placeholder="Tiền hóa đơn" Width="98%" onchange="HoaDon()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTienHoaDon" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tiền thanh toán</label>
                        <asp:TextBox ID="txtTienThanhToan" runat="server" class="form-control" placeholder="Tiền thanh toán" Width="98%" onchange="HoaDon()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTienThanhToan" ValidChars=".," />
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Phụ phí</label>
                        <asp:TextBox ID="txtPhuPhi" runat="server" class="form-control" placeholder="Phụ phí" Width="98%" onchange="PhuPhi()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtPhuPhi" ValidChars="-.," />
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <h5>Từ ngày</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Đến ngày</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Chi nhánh</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Khách hàng</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlKhachHangSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Xe bơm</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Xe bơm" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlBienXeBomSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" OnRowCreated="GV_RowCreated" ShowFooter="true"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" HeaderStyle-HorizontalAlign="Center" />

                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="TenXeBom" HeaderText="Xe bơm" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />

                                    <asp:BoundField DataField="TienHoaDon" HeaderText="Tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="TienThanhToan" HeaderText="Tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />

                                    <asp:BoundField DataField="PhuPhi" HeaderText="Phụ phí" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="TongHoaDon" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="TongThanhToan" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />

                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Center" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>

            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function PhuPhi() {
                    var txtPhuPhi = parseFloat(getvalue(document.getElementById("<%=txtPhuPhi.ClientID %>").value));
                    if (txtPhuPhi != '0') {
                        document.getElementById("<%=txtPhuPhi.ClientID %>").value = (txtPhuPhi).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtPhuPhi.ClientID %>").value = '0';
                    }
                }
                function HoaDon() {
                    var txtTienHoaDon = parseFloat(getvalue(document.getElementById("<%=txtTienHoaDon.ClientID %>").value));
                    var txtTienThanhToan = parseFloat(getvalue(document.getElementById("<%=txtTienThanhToan.ClientID %>").value));
                    var hdLoaiBom = document.getElementById("<%=hdLoaiBom.ClientID %>").value;
                    document.getElementById("<%=txtTienHoaDon.ClientID %>").value = (txtTienHoaDon).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    document.getElementById("<%=txtTienThanhToan.ClientID %>").value = (txtTienThanhToan).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                }
            </script>

            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdIDChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdCan" runat="server" Value="" />
            <asp:HiddenField ID="hdChot" runat="server" Value="" />
            <asp:HiddenField ID="hdLoaiBom" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound" OnRowCreated="gvLichSu_RowCreated"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="TrangThaiText" HeaderText="Trạng thái" />
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" HeaderStyle-HorizontalAlign="Center" />

                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TenXeBom" HeaderText="Xe bơm" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />

                                <asp:BoundField DataField="TienHoaDon" HeaderText="Tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TienThanhToan" HeaderText="Tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />

                                <asp:BoundField DataField="PhuPhi" HeaderText="Phụ phí" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TongHoaDon" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TongThanhToan" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />

                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtLuuHopDong" />--%>
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
