﻿<%@ Page Title="Lương nhân viên" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LuongNhanVien.aspx.cs" Inherits="ThuongLong.LuongNhanVien" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin lương nhân viên</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Bộ phận</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Bộ phận"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlBoPhan" runat="server" OnSelectedIndexChanged="dlBoPhan_SelectedIndexChanged1" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chức vụ</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chức vụ"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChucVu" runat="server" OnSelectedIndexChanged="dlChucVu_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Nhân viên</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhân viên"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhanVien"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Lương cơ bản</label>
                            <asp:TextBox ID="txtLuongCoBan" runat="server" class="form-control" placeholder="Lương cơ bản" onchange="TinhLuong()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtLuongCoBan" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Lương phụ cấp</label>
                            <asp:TextBox ID="txtLuongPhuCap" runat="server" class="form-control" placeholder="Lương phụ cấp" onchange="TinhLuong()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtLuongPhuCap" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Lương trách nhiệm</label>
                            <asp:TextBox ID="txtLuongTrachNhiem" runat="server" class="form-control" placeholder="Lương trách nhiệm" onchange="TinhLuong()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtLuongTrachNhiem" ValidChars=".," />
                        </div>
                         <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1"><i class="fa fa-minus text-danger"></i> Bảo hiểm</label>
                            <asp:TextBox ID="txtBaoHiem" runat="server" class="form-control" placeholder="Bảo hiểm" onchange="TinhLuong()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtBaoHiem" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1"><i class="fa fa-minus text-danger"></i> Công đoàn/Đoàn phí</label>
                            <asp:TextBox ID="txtCongDoan" runat="server" class="form-control" placeholder="Công đoàn/ Đoàn phí" Width="98%" disabled></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtCongDoan" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1"><i class="fa fa-minus text-danger"></i> Tiền ăn/ Khác</label>
                            <asp:TextBox ID="txtKhauTruKhac" runat="server" class="form-control" placeholder="Tiền ăn/ Khác" onchange="TinhLuong()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtKhauTruKhac" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Tổng lương</label>
                            <asp:TextBox ID="txtTongLuong" runat="server" class="form-control" placeholder="Tổng lương" Width="98%" disabled></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtTongLuong" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnResetFilter" runat="server" class="btn btn-app bg-warning" OnClick="btnResetFilter_Click"><i class="fa fa-filter"></i>Lọc</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="box-body" id="divdl" visible="false" runat="server">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl1" runat="server">Tên nhân viên</asp:Label><br />
                            <asp:ListBox ID="dlTenNhanVienSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl4" runat="server">Bộ phận</asp:Label><br />
                            <asp:ListBox ID="dlBoPhanSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label2" runat="server">Chức vụ</asp:Label><br />
                            <asp:ListBox ID="dlChucVuSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenBoPhan" HeaderText="Bộ phận" />
                                    <asp:BoundField DataField="TenChucVu" HeaderText="Chức vụ" />
                                    <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" />
                                    <asp:BoundField DataField="LuongCoBan" HeaderText="Lương cơ bản" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="LuongPhuCap" HeaderText="Lương phụ cấp" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="LuongTrachNhiem" HeaderText="Lương trách nhiệm" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="BaoHiem" HeaderText="Bảo hiểm" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="CongDoanPhi" HeaderText="Công đoàn phí" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="KhauTruKhac" HeaderText="Khấu trừ khác" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TongLuong" HeaderText="Tổng lương" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>

            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function TinhLuong() {
                    var txtLuongCoBan = parseFloat(getvalue(document.getElementById("<%=txtLuongCoBan.ClientID %>").value));
                    var txtLuongPhuCap = parseFloat(getvalue(document.getElementById("<%=txtLuongPhuCap.ClientID %>").value));
                    var txtLuongTrachNhiem = parseFloat(getvalue(document.getElementById("<%=txtLuongTrachNhiem.ClientID %>").value));
                    var txtBaoHiem = parseFloat(getvalue(document.getElementById("<%=txtBaoHiem.ClientID %>").value));
                    var txtKhauTruKhac = parseFloat(getvalue(document.getElementById("<%=txtKhauTruKhac.ClientID %>").value));
                    var tongLuong = (txtLuongCoBan + txtLuongPhuCap + txtLuongTrachNhiem) - txtBaoHiem - txtKhauTruKhac;
                    document.getElementById("<%=txtCongDoan.ClientID %>").value = (tongLuong * 0.01).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    document.getElementById("<%=txtTongLuong.ClientID %>").value = (tongLuong * 0.99).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenBoPhan" HeaderText="Bộ phận" />
                                <asp:BoundField DataField="TenChucVu" HeaderText="Chức vụ" />
                                <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" />
                                <asp:BoundField DataField="LuongCoBan" HeaderText="Lương cơ bản" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="LuongPhuCap" HeaderText="Lương phụ cấp" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="LuongTrachNhiem" HeaderText="Lương trách nhiệm" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="BaoHiem" HeaderText="Bảo hiểm" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="CongDoanPhi" HeaderText="Công đoàn phí" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KhauTruKhac" HeaderText="Khấu trừ khác" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TongLuong" HeaderText="Tổng lương" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
