﻿using NPOI.HSSF.Record;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class HopDongThueBom : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        BeTongDataContext vatlieu = new BeTongDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmHopDongThueBom";
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmHopDongThueBom", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNhaCungCap();
                        LoadHinhThucBom();
                        hdPage.Value = "1";
                        Session["HopDongThueBom"] = null;
                        hdForm.Value = Guid.NewGuid().ToString();
                        GetTable();
                        LoadKhachHangSearch();
                    }
                }
            }
        }
        void GetTable()
        {
            dt.Columns.Add("ID", typeof(Guid));
            dt.Columns.Add("IDHD", typeof(Guid));
            dt.Columns.Add("IDHinhThucBom", typeof(Guid));
            dt.Columns.Add("TenHinhThucBom", typeof(string));
            dt.Columns.Add("DonGiaCoThue", typeof(decimal));
            dt.Columns.Add("DonGiaKhongThue", typeof(decimal));
            dt.Columns.Add("KhoiLuongDuKien", typeof(double));
            dt.Columns.Add("TuNgay", typeof(string));
            dt.Columns.Add("DenNgay", typeof(string));
            dt.Columns.Add("TrangThai", typeof(Int32));
            dt.Columns.Add("TrangThaiText", typeof(string));
            dt.Columns.Add("NguoiDuyet", typeof(string));
            dt.Columns.Add("NguoiXoa", typeof(string));
            dt.Columns.Add("NgayTao", typeof(DateTime));
            dt.Columns.Add("NguoiTao", typeof(string));
            dt.Columns.Add("MoTa", typeof(string));
            dt.Columns.Add("Loai", typeof(Int32));
            dt.Columns.Add("hdForm", typeof(string));
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNhaCungCap()
        {
            dlNhaCungCap.Items.Clear();
            dlCongTrinh.Items.Clear();
            if (dlChiNhanh.SelectedValue != "")
            {
                var query = (from p in db.tblHopDongBanBeTongs
                             join q in db.tblNhaCungCaps on p.IDNhaCungCap equals q.ID
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             select new
                             {
                                 ID = p.IDNhaCungCap,
                                 Ten = q.TenNhaCungCap
                             }).Distinct().OrderBy(p => p.Ten);

                dlNhaCungCap.DataSource = query;
                dlNhaCungCap.DataBind();
                if (dlNhaCungCap.Items.Count == 1)
                {
                    LoadCongTrinh();
                }
                else
                {
                    dlNhaCungCap.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }
        protected void dlNhaCungCap_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCongTrinh();
        }
        protected void LoadCongTrinh()
        {
            dlCongTrinh.Items.Clear();
            if (dlNhaCungCap.SelectedValue != "")
            {
                var query = (from p in db.tblHopDongBanBeTongs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDNhaCungCap == GetDrop(dlNhaCungCap)
                             select new
                             {
                                 p.ID,
                                 Ten = p.CongTrinh
                             }).Distinct().OrderBy(p => p.Ten);

                dlCongTrinh.DataSource = query;
                dlCongTrinh.DataBind();
                if (dlCongTrinh.Items.Count != 1)
                {
                    dlCongTrinh.Items.Insert(0, new ListItem("--Chọn--", ""));
                }

            }
        }
        protected void LoadHinhThucBom()
        {
            dlHinhThucBom.Items.Clear();
            var query = (from p in db.tblHinhThucBoms
                         where p.ID != new Guid("2862C6F2-0AE1-499B-93B7-6E2B0AB46B83")
                         select new
                         {
                             p.ID,
                             Ten = p.TenHinhThucBom
                         }).OrderBy(p => p.Ten);
            dlHinhThucBom.DataSource = query;
            dlHinhThucBom.DataBind();
            dlHinhThucBom.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (dlCongTrinh.SelectedValue == "")
            {
                s += " - Chọn công trình<br />";
            }

            if (s == "")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vatlieu.sp_HopDongThueBom_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDHinhThucBom"].ToString()),
                        DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);

                    if (s != "")
                    {
                        break;
                    }
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (dlCongTrinh.SelectedValue == "")
            {
                s += " - Chọn công trình<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongThueBom_CheckSuaHopDong(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), ref s);
            }
            if (s == "")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["TrangThai"].ToString() == "3")
                    {
                        vatlieu.sp_HopDongThueBom_CheckXoaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), ref s);
                    }
                    else if (dt.Rows[i]["TrangThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() == "0")
                    {
                        vatlieu.sp_HopDongThueBom_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDHinhThucBom"].ToString()),
                            DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);
                    }
                    else if (dt.Rows[i]["TrangThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() != "1")
                    {
                        vatlieu.sp_HopDongThueBom_CheckSuaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDHinhThucBom"].ToString()),
                            decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString()), decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString()),
                            DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);
                    }
                    if (s != "")
                    {
                        break;
                    }
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckThemChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (dlCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlHinhThucBom.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongThueBom_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlHinhThucBom),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (dlCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlHinhThucBom.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongThueBom_CheckSuaChiTiet(new Guid(hdChiTiet.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlHinhThucBom),
                    decimal.Parse(txtDonGiaCoThue.Text), decimal.Parse(txtDonGiaKhongThue.Text), DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (Session["HopDongThueBom"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["HopDongThueBom"] as DataTable;
            }

            DateTime ngaykyhd = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
            DateTime ngayss = ngaykyhd;
            DateTime ngaymin = ngaykyhd;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["TuNgay"].ToString() != "" && DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())) < ngayss)
                {
                    ngaymin = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                }
            }

            if (dt.Rows.Count == 0 && gvChiTiet.Rows.Count > 0)
            {
                Warning("Thông tin giá hợp đồng đã thay đổi.");
            }
            else if (dt.Rows.Count == 0)
            {
                Warning("Bạn chưa thiết lập thông tin giá bê tông");
            }
            else if (dt.Rows.Count > 0 && dt.Rows[0]["hdForm"].ToString() != hdForm.Value)
            {
                Warning("Bạn không được mở 2 tab trên cùng 1 trình duyệt.");
            }
            else if (ngaymin < ngaykyhd)
            {
                Warning("Ngày bắt đầu tính giá bán gạch không được nhỏ hơn ngày ký hợp đồng.");
            }
            else
            {
                if (hdID.Value == "")
                {
                    if (CheckThem() == "")
                    {
                        string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            Guid ID = Guid.NewGuid();
                            var HopDongThueBom = new tblHopDongThueBom()
                            {
                                ID = new Guid(hdChung.Value),
                                NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                                IDChiNhanh = GetDrop(dlChiNhanh),
                                IDNhaCungCap = GetDrop(dlNhaCungCap),
                                IDCongTrinh = GetDrop(dlCongTrinh),
                                //IDNhomVatLieu = GetDrop(dlNhomVatLieu),
                                //IDLoaiVatLieu = GetDrop(dlLoaiVatLieu),
                                //IDHinhThucBom = GetDrop(dlHinhThucBom),
                                //DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text),
                                //DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text),
                                //TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                                //DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                                TrangThai = 1,
                                TrangThaiText = "Chờ duyệt",
                                //STT = 0,
                                NguoiTao = Session["IDND"].ToString(),
                                NgayTao = DateTime.Now
                            };
                            db.tblHopDongThueBoms.InsertOnSubmit(HopDongThueBom);

                            List<tblHopDongThueBom_ChiTiet> xoa = new List<tblHopDongThueBom_ChiTiet>();
                            List<tblHopDongThueBom_ChiTiet> them = new List<tblHopDongThueBom_ChiTiet>();

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["Loai"].ToString() != "10")
                                {
                                    var xoachitiet = (from p in db.tblHopDongThueBom_ChiTiets
                                                      where p.ID == new Guid(dt.Rows[i]["ID"].ToString())
                                                      select p).FirstOrDefault();
                                    if (xoachitiet != null && xoachitiet.ID != null)
                                    {
                                        xoa.Add(xoachitiet);
                                    }
                                    var themchitiet = new tblHopDongThueBom_ChiTiet();
                                    themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                    themchitiet.IDHD = new Guid(dt.Rows[i]["IDHD"].ToString());
                                    themchitiet.HinhThucBom = new Guid(dt.Rows[i]["IDHinhThucBom"].ToString());
                                    themchitiet.DonGiaCoThue = decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString());
                                    themchitiet.DonGiaKhongThue = decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString());
                                    themchitiet.KhoiLuongDuKien = double.Parse(dt.Rows[i]["KhoiLuongDuKien"].ToString());
                                    themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                                    themchitiet.DenNgay = dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString()));
                                    themchitiet.TrangThai = int.Parse(dt.Rows[i]["TrangThai"].ToString());
                                    themchitiet.TrangThaiText = dt.Rows[i]["TrangThaiText"].ToString();
                                    themchitiet.NguoiDuyet = dt.Rows[i]["NguoiDuyet"].ToString();
                                    themchitiet.NguoiXoa = dt.Rows[i]["NguoiXoa"].ToString();
                                    themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                    themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                    themchitiet.MoTa = dt.Rows[i]["MoTa"].ToString();
                                    them.Add(themchitiet);
                                }
                            }
                            db.tblHopDongThueBom_ChiTiets.DeleteAllOnSubmit(xoa);
                            db.tblHopDongThueBom_ChiTiets.InsertAllOnSubmit(them);
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Lưu thành công.");
                        }
                    }
                }
                else
                {
                    if (CheckSua() == "")
                    {

                        string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            var query = (from p in db.tblHopDongThueBoms
                                         where p.ID == new Guid(hdID.Value)
                                         select p).FirstOrDefault();
                            if (query != null && query.ID != null)
                            {
                                DateTime dtcheck = DateTime.Parse(hdNgayTao.Value);
                                if (hdID.Value != ""
                                    && (
                                    dtcheck.Year != query.NgayTao.Value.Year
                                    || dtcheck.Month != query.NgayTao.Value.Month
                                    || dtcheck.Day != query.NgayTao.Value.Day
                                    || dtcheck.Hour != query.NgayTao.Value.Hour
                                    || dtcheck.Minute != query.NgayTao.Value.Minute
                                    || dtcheck.Second != query.NgayTao.Value.Second
                                    )
                                    )
                                {
                                    Warning("Thông tin hợp đồng đã bị sửa, xin vui lòng kiểm tra lại.");
                                }
                                else
                                {

                                    query.IDChiNhanh = GetDrop(dlChiNhanh);
                                    query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                                    query.IDCongTrinh = GetDrop(dlCongTrinh);
                                    //query.IDNhomVatLieu = GetDrop(dlNhomVatLieu);
                                    //query.IDLoaiVatLieu = GetDrop(dlLoaiVatLieu);
                                    //query.IDHinhThucBom = GetDrop(dlHinhThucBom);
                                    //query.DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text);
                                    //query.DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text);
                                    //query.TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text));
                                    //query.DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text));
                                    query.TrangThai = 1;
                                    query.TrangThaiText = "Chờ duyệt";
                                    //query.STT = query.STT + 1;
                                    query.NguoiTao = Session["IDND"].ToString();
                                    query.NgayTao = DateTime.Now;


                                    List<tblHopDongThueBom_ChiTiet> xoa = new List<tblHopDongThueBom_ChiTiet>();
                                    List<tblHopDongThueBom_ChiTiet> them = new List<tblHopDongThueBom_ChiTiet>();

                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        if (dt.Rows[i]["Loai"].ToString() != "10")
                                        {
                                            var xoachitiet = (from p in db.tblHopDongThueBom_ChiTiets
                                                              where p.ID == new Guid(dt.Rows[i]["ID"].ToString())
                                                              select p).FirstOrDefault();
                                            if (xoachitiet != null && xoachitiet.ID != null)
                                            {
                                                xoa.Add(xoachitiet);
                                            }
                                            var themchitiet = new tblHopDongThueBom_ChiTiet();
                                            themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                            themchitiet.IDHD = new Guid(dt.Rows[i]["IDHD"].ToString());
                                            themchitiet.HinhThucBom = new Guid(dt.Rows[i]["IDHinhThucBom"].ToString());
                                            themchitiet.DonGiaCoThue = decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString());
                                            themchitiet.DonGiaKhongThue = decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString());
                                            themchitiet.KhoiLuongDuKien = double.Parse(dt.Rows[i]["KhoiLuongDuKien"].ToString());
                                            themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                                            themchitiet.DenNgay = dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString()));
                                            themchitiet.TrangThai = int.Parse(dt.Rows[i]["TrangThai"].ToString());
                                            themchitiet.TrangThaiText = dt.Rows[i]["TrangThaiText"].ToString();
                                            themchitiet.NguoiDuyet = dt.Rows[i]["NguoiDuyet"].ToString();
                                            themchitiet.NguoiXoa = dt.Rows[i]["NguoiXoa"].ToString();
                                            themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                            themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                            themchitiet.MoTa = dt.Rows[i]["MoTa"].ToString();
                                            them.Add(themchitiet);
                                        }
                                    }
                                    db.tblHopDongThueBom_ChiTiets.DeleteAllOnSubmit(xoa);
                                    db.tblHopDongThueBom_ChiTiets.InsertAllOnSubmit(them);

                                    db.SubmitChanges();

                                    lblTaoMoiHopDong_Click(sender, e);
                                    Search(1);
                                    Success("Sửa thành công");
                                }
                            }
                            else
                            {
                                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                                lblTaoMoiHopDong_Click(sender, e);
                            }
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlChiNhanh_SelectedIndexChanged(sender, e);
            dlHinhThucBom.SelectedValue = "";
            hdID.Value = "";
            hdForm.Value = "";
            hdChiTiet.Value = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtKhoiLuongDuKien.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
            Session["HopDongThueBom"] = null;
            gvChiTiet.DataSource = null;
            gvChiTiet.DataBind();
            LoadKhachHangSearch();
        }
        protected void lbtSave_Click(object sender, EventArgs e)
        {
            string url = "";
            if (Session["HopDongThueBom"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["HopDongThueBom"] as DataTable;
            }

            if (hdChiTiet.Value == "")
            {
                if (CheckThemChiTiet() == "" && CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (Check(dt, txtTuNgay.Text.Trim(), txtDenNgay.Text.Trim()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            if (hdID.Value == "" && hdChung.Value == "")
                                hdChung.Value = Guid.NewGuid().ToString();

                            dt.Rows.Add(Guid.NewGuid(),
                            new Guid(hdChung.Value),
                            GetDrop(dlHinhThucBom),
                            dlHinhThucBom.SelectedItem.Text.ToString(),
                            decimal.Parse(txtDonGiaCoThue.Text),
                            decimal.Parse(txtDonGiaKhongThue.Text),
                            double.Parse(txtKhoiLuongDuKien.Text),
                            txtTuNgay.Text.Trim(),
                            txtDenNgay.Text.Trim(),
                            1,
                            "Chờ duyệt",
                            "",
                            "",
                            DateTime.Now, Session["IDND"].ToString(),
                            "",
                            0,
                            hdForm.Value
                            );

                            lbtTaoGiaMoi_Click(sender, e);
                            gvChiTiet.DataSource = dt;
                            gvChiTiet.DataBind();
                            Session["HopDongThueBom"] = dt;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
            else
            {
                if (CheckSuaChiTiet() == "" && CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckSua(dt, txtTuNgay.Text.Trim(), txtDenNgay.Text.Trim(), hdChiTiet.Value.ToUpper()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(hdChiTiet.Value)).FirstOrDefault();
                            if (dr != null)
                            {
                                dr["IDHD"] = new Guid(hdChung.Value);
                                dr["IDHinhThucBom"] = GetDrop(dlHinhThucBom);
                                dr["TenHinhThucBom"] = dlHinhThucBom.SelectedItem.Text.ToString();
                                dr["DonGiaCoThue"] = decimal.Parse(txtDonGiaCoThue.Text);
                                dr["DonGiaKhongThue"] = decimal.Parse(txtDonGiaKhongThue.Text);
                                dr["KhoiLuongDuKien"] = double.Parse(txtKhoiLuongDuKien.Text);
                                dr["TuNgay"] = txtTuNgay.Text;
                                dr["DenNgay"] = txtDenNgay.Text;
                                dr["TrangThai"] = 1;
                                dr["TrangThaiText"] = "Chờ duyệt";
                                dr["NguoiDuyet"] = "";
                                dr["NguoiXoa"] = "";
                                dr["NgayTao"] = DateTime.Now;
                                dr["NguoiTao"] = Session["IDND"].ToString();
                                dr["MoTa"] = "";
                                dr["Loai"] = 1;
                                dr["hdForm"] = hdForm.Value;
                            }

                            lbtTaoGiaMoi_Click(sender, e);
                            gvChiTiet.DataSource = dt;
                            gvChiTiet.DataBind();
                            Session["HopDongThueBom"] = dt;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
        }

        protected void lbtTaoGiaMoi_Click(object sender, EventArgs e)
        {
            dlHinhThucBom.SelectedValue = "";
            hdChiTiet.Value = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtKhoiLuongDuKien.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
        }

        protected void gvChiTiet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();

            if (Session["HopDongThueBom"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["HopDongThueBom"] as DataTable;
            }

            DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(id)).FirstOrDefault();
            if (dr != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlHinhThucBom.SelectedValue = dr["IDHinhThucBom"].ToString();
                        txtDonGiaCoThue.Text = string.Format("{0:N0}", dr["DonGiaCoThue"]);
                        txtDonGiaKhongThue.Text = string.Format("{0:N0}", dr["DonGiaKhongThue"]);
                        txtKhoiLuongDuKien.Text = string.Format("{0:N0}", dr["KhoiLuongDuKien"]);
                        txtTuNgay.Text = dr["TuNgay"].ToString();
                        txtDenNgay.Text = dr["DenNgay"].ToString();
                        hdChiTiet.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_HopDongThueBom_CheckXoaChiTiet(new Guid(id), ref thongbao);
                        if (thongbao != "")
                        {
                            Warning(thongbao);
                        }
                        else
                        {
                            var query = (from p in db.tblHopDongThueBom_ChiTiets
                                         where p.ID == new Guid(id)
                                         select p).FirstOrDefault();

                            if (query != null && query.ID != null)
                            {
                                dr["Loai"] = "3";
                                dr["TrangThai"] = "3";
                                dr["TrangThaiText"] = "Chờ duyệt xóa";

                                Success("Xóa thành công.");
                                gvChiTiet.DataSource = dt;
                                gvChiTiet.DataBind();
                                Session["GiBanGach"] = dt;
                                lbtTaoGiaMoi_Click(sender, e);
                            }
                            else
                            {
                                dt.Rows.Remove(dr);
                                dt.AcceptChanges();

                                Success("Xóa thành công.");
                                gvChiTiet.DataSource = dt;
                                gvChiTiet.DataBind();
                                Session["GiBanGach"] = dt;
                                lbtTaoGiaMoi_Click(sender, e);
                            }
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_HopDongThueBom_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblHopDongThueBoms
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        LoadNhaCungCap();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        if (dlNhaCungCap.Items.Count ==1)
                        {
                            LoadCongTrinh();
                        }
                        dlCongTrinh.SelectedValue = query.IDCongTrinh.ToString();
                        hdID.Value = id;
                        hdChung.Value = id;
                        hdNgayTao.Value = query.NgayTao.ToString();
                        hdForm.Value = Guid.NewGuid().ToString();
                        LoadChiTiet();
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_HopDongThueBom_CheckXoa(query.ID, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblHopDongThueBom_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblHopDongThueBoms.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = vatlieu.sp_HopDongThueBom_LichSu(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        private string Check(DataTable tableinsert, string tungay, string denngay)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                else _err = "";
            }
            if (_err != "")
            {
                Warning(_err);
            }

            return _err;

        }
        private string CheckSua(DataTable tableinsert, string tungay, string denngay, string ids)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                else _err = "";
            }

            if (_err != "")
            {
                Warning(_err);
            }

            return _err;
        }

        //private string Check(DataTable tableinsert, string tungay, string denngay)
        //{
        //    string _er = "";
        //    DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
        //    DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

        //    var dr_tontai =
        //        (tableinsert.AsEnumerable()
        //            .Where(p => p.Field<Guid>("IDLoaiVatLieu") == GetDrop(dlLoaiVatLieu)
        //                        && p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom)
        //                        &&
        //                        (
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay && DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_tungay)
        //                        ||
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_denngay && (p.Field<string>("DenNgay") == "" || DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_denngay))
        //                        ||
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay && (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_denngay))
        //                        ||
        //                        (p.Field<string>("DenNgay") != "" && (DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_tungay && (DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) <= dt_denngay)))
        //                        || (p.Field<string>("DenNgay") == "" && denngay == "")
        //                        )
        //            )).Count();
        //    if (dr_tontai > 0)
        //    {
        //        _er = "Trong cùng 1 khoảng thời gian không được tồn tại 2 thông tin giá giống nhau";
        //        Warning(_er);
        //    }
        //    return _er;
        //}

        //private string CheckSua(DataTable tableinsert, string tungay, string denngay, string id)
        //{
        //    string _er = "";
        //    DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
        //    DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

        //    var dr_tontai =
        //        (tableinsert.AsEnumerable()
        //            .Where(p => p.Field<Guid>("IDLoaiVatLieu") == GetDrop(dlLoaiVatLieu)
        //                        && p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom)
        //                        &&
        //                        (
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay && (p.Field<string>("DenNgay") != "" && DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_tungay))
        //                        ||
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_denngay && (p.Field<string>("DenNgay") == "" || DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_denngay))
        //                        //||
        //                        //(DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay && (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_denngay))
        //                        //||
        //                        //(p.Field<string>("DenNgay") != "" && (DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_tungay && (DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) <= dt_denngay)))
        //                        //|| (p.Field<string>("DenNgay") == "" && denngay == "")
        //                        )
        //                        && p.Field<Guid>("ID") != new Guid(id)
        //            )).Count();
        //    if (dr_tontai > 0)
        //    {
        //        _er = "Trong cùng 1 khoảng thời gian không được tồn tại 2 thông tin giá giống nhau";
        //        Warning(_er);
        //    }
        //    return _er;
        //}
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            var query = vatlieu.sp_HopDongThueBom_Search(GetDrop(dlChiNhanhSearch), dlKhachHangSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlKhachHangSearch), 20, page);

            GV.DataSource = query;
            GV.DataBind();
        }
        private void LoadChiTiet()
        {
            List<sp_HopDongThueBom_ChiTietSearchResult> query = vatlieu.sp_HopDongThueBom_ChiTietSearch(new Guid(hdID.Value), hdForm.Value).ToList();
            GetTable();
            foreach (var item in query)
            {
                dt.Rows.Add(
                                item.ID,
                                item.IDHD,
                                item.IDHinhThucBom,
                                item.TenHinhThucBom,
                                item.DonGiaCoThue,
                                item.DonGiaKhongThue,
                                item.KhoiLuongDuKien,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                item.TrangThai,
                                item.TrangThaiText,
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                item.Loai,
                                item.hdForm
                            );
            }
            gvChiTiet.DataSource = query;
            gvChiTiet.DataBind();
            Session["HopDongThueBom"] = dt;
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadKhachHangSearch();
        }
        void LoadKhachHangSearch()
        {
            dlKhachHangSearch.Items.Clear();
            var query = vatlieu.sp_HopDongThueBom_LoadNhaCungCapSearch(GetDrop(dlChiNhanhSearch));
            dlKhachHangSearch.DataSource = query;
            dlKhachHangSearch.DataBind();
            dlKhachHangSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }

    }
}