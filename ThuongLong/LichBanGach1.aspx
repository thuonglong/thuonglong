﻿<%@ Page Title="Lịch sản xuất bê tông" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LichBanGach1.aspx.cs" Inherits="ThuongLong.LichBanGach1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<style>
        .select2-container {
            z-index: 100000;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin giá</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <h5>Giờ xuất</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtGioXuat" runat="server" class="form-control" data-inputmask="'alias': 'hh:mm'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Ngày tháng</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%" OnTextChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Chi nhánh</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Khách hàng</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng" AutoPostBack="true"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCap" OnSelectedIndexChanged="dlNhaCungCap_SelectedIndexChanged" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Công trình</h5>
                            <asp:DropDownList CssClass="form-control select2" data-toggle="tooltip" data-original-title="Công trình"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCongTrinh" OnSelectedIndexChanged="dlCongTrinh_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Hạng mục</h5>
                            <asp:TextBox ID="txtHangMuc" runat="server" class="form-control" placeholder="Hạng mục" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Nhân viên kinh doanh</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhân viên kinh doanh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhanVien" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Mác bê tông</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Mác bê tông"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMacBeTong" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Hình thức bơm</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Hình thức bơm"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlHinhThucBom" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Khối lượng tạm tính</h5>
                            <asp:TextBox ID="txtKLThucXuat" runat="server" class="form-control" placeholder="Khối lượng tạm tính" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtKLThucXuat" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Khối lượng khách hàng</h5>
                            <asp:TextBox ID="txtKLKhachHang" runat="server" class="form-control" placeholder="Khối lượng khách hàng" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtKLKhachHang" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Cự ly di chuyển</h5>
                            <asp:TextBox ID="txtCuLyVanChuyen" runat="server" class="form-control" placeholder="Cự ly di chuyển" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtCuLyVanChuyen" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>NV kỹ thuật</h5>
                            <asp:TextBox ID="txtKyThuat" runat="server" class="form-control" placeholder="NV kỹ thuật" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Người thu tiền</h5>
                            <asp:TextBox ID="txtNguoiThuTien" runat="server" class="form-control" placeholder="Người thu tiền" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <h5>Từ ngày</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Đến ngày</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Chi nhánh</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnResetFilter" runat="server" class="btn btn-app bg-warning" OnClick="btnResetFilter_Click"><i class="fa fa-filter"></i>Lọc</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="box-body" id="divdl" visible="false" runat="server">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <h5>Khách hàng</h5>
                            <asp:ListBox ID="dlNhaCungCapSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Công trình</h5>
                            <asp:ListBox ID="dlCongTrinhSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Mác bê tông</h5>
                            <asp:ListBox ID="dlMacBeTongSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Hình thức bơm</h5>
                            <asp:ListBox ID="dlHinhThucBomSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Hoàn thành">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblhoanthanh"
                                                Text='<%#Eval("TrangThaiHoanThanh")%>' runat="server" CssClass="padding" CommandName="hoanthanh"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GioXuat" HeaderText="Giờ xuất" />
                                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" />
                                    <asp:BoundField DataField="TenCongTrinh" HeaderText="Công trình" />
                                    <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" />
                                    <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" />
                                    <asp:BoundField DataField="TenMacBeTong" HeaderText="Mác bê tông" />
                                    <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" />
                                    <asp:BoundField DataField="KLThucXuat" HeaderText="KL tạm tính" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="KLKhachHang" HeaderText="Kl khách hàng" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="CuLyVanChuyen" HeaderText="Cự ly vận chuyển" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="KyThuat" HeaderText="NV kỹ thuật" />
                                    <asp:BoundField DataField="NguoiThuTien" HeaderText="Người thu tiền" />
                                    <asp:BoundField DataField="KLDaXuat" HeaderText="KL đã trộn" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="KLDaBan" HeaderText="Kl đã bán" ItemStyle-HorizontalAlign="Right" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdIDChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="GioXuat" HeaderText="Giờ xuất" />
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" />
                                <asp:BoundField DataField="TenCongTrinh" HeaderText="Công trình" />
                                <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" />
                                <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" />
                                <asp:BoundField DataField="TenMacBeTong" HeaderText="Mác bê tông" />
                                <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" />
                                <asp:BoundField DataField="KLThucXuat" HeaderText="KL tạm tính" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KLKhachHang" HeaderText="Kl khách hàng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="CuLyVanChuyen" HeaderText="Cự ly vận chuyển" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KyThuat" HeaderText="NV kỹ thuật" />
                                <asp:BoundField DataField="NguoiThuTien" HeaderText="Người thu tiền" />
                                <asp:BoundField DataField="KLDaXuat" HeaderText="KL đã trộn" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KLDaBan" HeaderText="Kl đã bán" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
