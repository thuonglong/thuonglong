﻿<%@ Page Title="Quản lý giao dịch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QuanLyGiaoDich.aspx.cs" Inherits="ThuongLong.QuanLyGiaoDich" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">

                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Thu - chi</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại thu chi"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThuChi"
                                runat="server">
                                <asp:ListItem Value="">Chọn</asp:ListItem>
                                <asp:ListItem>Thu</asp:ListItem>
                                <asp:ListItem>Chi</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Hạng mục thu - chi</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <asp:CheckBox ID="ckLoai" runat="server" Enabled="false" />
                                </span>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Hạng mục chi" Font-Size="13px"
                                    DataTextField="Ten" DataValueField="ID" ID="dlHangMucChi" OnSelectedIndexChanged="dlHangMucChi_SelectedIndexChanged" AutoPostBack="true" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đối tượng nhận</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đối lượng nhận"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDoiTuongNhan"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngân hàng</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Ngân hàng"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNganHang" OnSelectedIndexChanged="dlNganHang_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Số tài khoản</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Số tài khoản"
                                Font-Size="13px" DataTextField="SoTaiKhoan" DataValueField="SoTaiKhoan" ID="dlSoTaiKhoan"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3" id="divloaitien" runat="server" visible="false">
                            <label for="exampleInputEmail1">Loại tiền</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại thu chi"
                                Font-Size="13px" DataTextField="LoaiTien" DataValueField="LoaiTien" ID="dlLoaiTien"
                                runat="server">
                                <asp:ListItem Value="1">VND</asp:ListItem>
                                <asp:ListItem Value="2">Dollar</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày hoạch toán</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayHoachToan" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày chứng từ</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayChungTu" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Tài khoản đối ứng</label>
                            <asp:TextBox ID="txtTaiKhoanDoiUng" runat="server" class="form-control" placeholder="Người nhận" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Số tiền</label>
                            <asp:TextBox ID="txtSoTien" runat="server" class="form-control" placeholder="Số tiền chi" onchange="SoTien()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtSoTien" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Diễn giải</label>
                            <asp:TextBox ID="txtDienGiai" runat="server" class="form-control" placeholder="Nội dung" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ghi chú</label>
                            <asp:TextBox ID="txtGhiChu" runat="server" class="form-control" placeholder="Nội dung" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu </asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                        <asp:LinkButton ID="lbtImportFile" runat="server" class="btn btn-app bg-green" OnClick="lbtImportFile_Click"><i class="fa fa-info-circle"></i>Nhập từ excel</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Từ ngày</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"
                                        OnTextChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Đến ngày</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"
                                        OnTextChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-lg-3">
                                <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh"></asp:Label><br />
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                    DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3">
                                <asp:Label ID="lbl2" runat="server">Ngân hàng</asp:Label><br />
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                    DataTextField="Ten" DataValueField="ID" ID="dlNganHangSearch" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-3" id="divstks" runat="server" visible="false">
                                <asp:Label ID="lbl1" runat="server">Số tài khoản</asp:Label><br />
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                    DataTextField="SoTaiKhoan" DataValueField="SoTaiKhoan" ID="dlSoTaiKhoanSearch" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                                <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-blue" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                                <asp:LinkButton ID="btnResetFilter" runat="server" class="btn btn-app bg-warning" OnClick="btnResetFilter_Click" Visible="false"><i class="fa fa-filter"></i>Lọc</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-body" id="divdl" visible="false" runat="server">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server">Loại</asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại thu chi"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                                <asp:ListItem Value="0">Tất cả</asp:ListItem>
                                <asp:ListItem Value="1">Phiếu thu</asp:ListItem>
                                <asp:ListItem Value="2">Phiếu chi</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" ShowFooter="true"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-danger" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="NgayHoachToan" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Nhà cung cấp" />
                                    <asp:BoundField DataField="TenHangMucThuChi" HeaderText="Hạng mục chi" />
                                    <asp:BoundField DataField="TenNganHang" HeaderText="Ngân hàng" />
                                    <asp:BoundField DataField="SoTaiKhoan" HeaderText="Số tài khoản" />
                                    <asp:BoundField DataField="LoaiTien" HeaderText="Loại tiền" />
                                    <asp:BoundField DataField="NgayHoachToan" HeaderText="Ngày hoạch toán" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="NgayChungtu" HeaderText="Ngày chứng từ" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TaiKhoanDoiUng" HeaderText="Tài khoản đối ứng" />
                                    <asp:BoundField DataField="HangMucThuChi" HeaderText="Loại thu - chi" />
                                    <asp:BoundField DataField="SoTienThu" HeaderText="Số tiền thu" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="SoTienChi" HeaderText="Số tiền chi" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DienGiai" HeaderText="Diễn giải" />
                                    <asp:BoundField DataField="GhiChu" HeaderText="Ghi chú" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function SoTien() {
                    var txtSoTien = parseFloat(getvalue(document.getElementById("<%=txtSoTien.ClientID %>").value));
                    document.getElementById("<%=txtSoTien.ClientID %>").value = (txtSoTien).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdHinhAnh" runat="server" Value="" />
            <asp:HiddenField ID="hdUpLoad" runat="server" Value="" />
            <asp:HiddenField ID="hdChot" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Danh sách phiếu thu - chi
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="NgayHoachToan" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenLoai" HeaderText="Loại phiếu" />
                                <asp:BoundField DataField="TenHangMucThuChi" HeaderText="Hạng mục chi" />
                                <asp:BoundField DataField="TenNganHang" HeaderText="Ngân hàng" />
                                <asp:BoundField DataField="TaiKhoanDoiUng" HeaderText="Người nhận" />
                                <asp:BoundField DataField="SoTienThu" HeaderText="Số tiền thu" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="SoTienChi" HeaderText="Số tiền chi" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpUpLoad" runat="server" CancelControlID="btnDongUpLoad"
                Drag="True" TargetControlID="hdUpLoad" BackgroundCssClass="modalBackground" PopupControlID="pnUpLoad"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnUpLoad" runat="server" Style="width: 50%; height: 50%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Upload hình ảnh
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control " Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhUpLoad" OnSelectedIndexChanged="dlChiNhanhUpLoad_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngân hàng</label>
                            <asp:DropDownList CssClass="form-control " Width="98%" data-toggle="tooltip" data-original-title="Ngân hàng"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNganHangUpLoad" OnSelectedIndexChanged="dlNganHangUpLoad_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Số tài khoản</label>
                            <asp:DropDownList CssClass="form-control " Width="98%" data-toggle="tooltip" data-original-title="Số tài khoản"
                                Font-Size="13px" DataTextField="SoTaiKhoan" DataValueField="SoTaiKhoan" ID="dlSoTaiKhoanUpLoad" OnSelectedIndexChanged="dlSoTaiKhoanUpLoad_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Loại tiền</label>
                            <asp:DropDownList CssClass="form-control " Width="98%" data-toggle="tooltip" data-original-title="Loại tiền"
                                Font-Size="13px" DataTextField="LoaiTien" DataValueField="LoaiTien" ID="dlLoaiTienUpLoad"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </div>

                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvUpLoad" runat="server"></asp:GridView>
                    </div>
                    <div class="panel-footer" style="text-align: center">
                        <asp:Button ID="btnUpLoad" runat="server" Text="Upload" CssClass="btn btn-success" OnClick="btnUpLoad_Click" />
                        <asp:Button ID="btnDongUpLoad" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpHinhAnh" runat="server" CancelControlID="btnDongHinhAnh"
                Drag="True" TargetControlID="hdHinhAnh" BackgroundCssClass="modalBackground" PopupControlID="pnHinhAnh"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnHinhAnh" runat="server" Style="width: auto; height: auto; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Hình ảnh
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:Image ID="imgHinhAnh" runat="server" ImageUrl="" />
                    </div>
                    <div class="panel-footer" style="text-align: center">
                        <asp:Button ID="btnDongHinhAnh" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpLoad" />
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
