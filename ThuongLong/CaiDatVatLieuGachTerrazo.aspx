﻿<%@ Page Title="Định mức gạch Terrazo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CaiDatVatLieuGachTerrazo.aspx.cs" Inherits="ThuongLong.CaiDatVatLieuGachTerrazo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="divloaisanpham" runat="server">
                        <label for="exampleInputEmail1">Loại sản phẩm*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại sản phẩm"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiSanPham" runat="server" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div10" runat="server">
                        <label for="exampleInputEmail1">Thời tiết*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Thời tiết"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThoiTiet" runat="server" OnSelectedIndexChanged="dlThoiTiet_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Nắng</asp:ListItem>
                            <asp:ListItem Value="1">Mưa</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>


            </div>
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Lớp mặt</h3>
                </div>
                <div class="box-body">
                    <div class="form-group col-lg-3" id="div" runat="server">
                        <label for="exampleInputEmail1">Màu xi*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMauXi"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div1" runat="server">
                        <label for="exampleInputEmail1">Màu đỏ*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm vật liệu"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMauDo" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div2" runat="server">
                        <label for="exampleInputEmail1">Bột đá*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm vật liệu"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlBotDa" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div3" runat="server">
                        <label for="exampleInputEmail1">Đá đen 2 ly*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại vật liệu"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDaDen2Ly" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div4" runat="server">
                        <label for="exampleInputEmail1">Đá trắng 2 ly*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại vật liệu"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDaTrang2Ly" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div5" runat="server">
                        <label for="exampleInputEmail1">Đá trắng 4 ly*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDaTrang4Ly" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div6" runat="server">
                        <label for="exampleInputEmail1">Xi măng PCB40*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMangPCB401" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Màu xi*</label>
                        <asp:TextBox ID="txtMauXi" runat="server" class="form-control" placeholder="Màu xi" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtMauXi" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Màu đỏ*</label>
                        <asp:TextBox ID="txtMauDo" runat="server" class="form-control" placeholder="Màu đỏ" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtMauDo" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Bột đá*</label>
                        <asp:TextBox ID="txtBotDa" runat="server" class="form-control" placeholder="Bột đá" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtBotDa" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá đen 2 ly*</label>
                        <asp:TextBox ID="txtDaDen2Ly" runat="server" class="form-control" placeholder="Đá đen 2 ly" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDaDen2Ly" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá trắng 2 ly*</label>
                        <asp:TextBox ID="txtDaTrang2Ly" runat="server" class="form-control" placeholder="Đá trắng 2 ly" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDaTrang2Ly" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá trắng 4 ly*</label>
                        <asp:TextBox ID="txtDaTrang4Ly" runat="server" class="form-control" placeholder="Đá trắng 4 ly" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDaTrang4Ly" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xi măng PCB40*</label>
                        <asp:TextBox ID="txtXiMangPCB401" runat="server" class="form-control" placeholder="Xi măng PCB40 1" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtXiMangPCB401" ValidChars=".," />
                    </div>
                </div>
            </div>


            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Lớp thân</h3>
                </div>
                <div class="box-body">
                    <div class="form-group col-lg-3" id="div7" runat="server">
                        <label for="exampleInputEmail1">Xi măng PCB40(Lớp thân)</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMangPCB402" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div8" runat="server">
                        <label for="exampleInputEmail1">Mạt đá</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMatDa" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div9" runat="server">
                        <label for="exampleInputEmail1">Cát sông đà</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCatSongDa" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xi măng PCB40(Lớp thân)</label>
                        <asp:TextBox ID="txtXiMangPCB402" runat="server" class="form-control" placeholder="Xi măng PCB40 2" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtXiMangPCB402" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Mạt đá</label>
                        <asp:TextBox ID="txtMatDa" runat="server" class="form-control" placeholder="Mạt đá" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtMatDa" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Cát sông đà</label>
                        <asp:TextBox ID="txtCatSongDa" runat="server" class="form-control" placeholder="Cát sông đà" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtCatSongDa" ValidChars=".," />
                    </div>
                </div>
            </div>


            <div class="box-footer">
                <div class="col-md-12">
                    <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                    <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách giá</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server" Text="Loại sản phẩm"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại sản phẩm" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlLoaiVatLieuSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                    <asp:BoundField DataField="LoaiVatLieu" HeaderText="Loại gạch" />
                                    <asp:BoundField DataField="MauXi" HeaderText="Màu xi" />
                                    <asp:BoundField DataField="MauDo" HeaderText="Màu đỏ" />
                                    <asp:BoundField DataField="BotDa" HeaderText="Bột đá" />
                                    <asp:BoundField DataField="DaDen2ly" HeaderText="Đá đen 2 ly" />
                                    <asp:BoundField DataField="DaTrang2Ly" HeaderText="Đá trắng 2 ly" />
                                    <asp:BoundField DataField="DaTrang4Ly" HeaderText="Đá trắng 4 ly" />
                                    <asp:BoundField DataField="XiMangPCB401" HeaderText="Xi măng PCB40 1" />
                                    <asp:BoundField DataField="XiMangPCB402" HeaderText="Xi măng PCB40 2" />
                                    <asp:BoundField DataField="MatDa" HeaderText="Mạt đá" />
                                    <asp:BoundField DataField="CatSongDa" HeaderText="Cát sông đà" />
                                    <asp:BoundField DataField="DMMauXi" HeaderText="ĐM Màu xi" />
                                    <asp:BoundField DataField="DMMauDo" HeaderText="ĐM Màu đỏ" />
                                    <asp:BoundField DataField="DMBotDa" HeaderText="ĐM Bột đá" />
                                    <asp:BoundField DataField="DMDaDen2ly" HeaderText="ĐM Đá đen 2 ly" />
                                    <asp:BoundField DataField="DMDaTrang2Ly" HeaderText="ĐM Đá trắng 2 ly" />
                                    <asp:BoundField DataField="DMDaTrang4Ly" HeaderText="ĐM Đá trắng 4 ly" />
                                    <asp:BoundField DataField="DMXiMangPCB401" HeaderText="ĐM Xi măng PCB40 1" />
                                    <asp:BoundField DataField="DMXiMangPCB402" HeaderText="ĐM Xi măng PCB40 2" />
                                    <asp:BoundField DataField="DMMatDa" HeaderText="ĐM Mạt đá" />
                                    <asp:BoundField DataField="DMCatSongDa" HeaderText="ĐM Cát sông đà" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>

            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdChung" runat="server" Value="" />
            <asp:HiddenField ID="hdChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                <asp:BoundField DataField="LoaiVatLieu" HeaderText="Loại gạch" />
                                <asp:BoundField DataField="MauXi" HeaderText="Màu xi" />
                                <asp:BoundField DataField="MauDo" HeaderText="Màu đỏ" />
                                <asp:BoundField DataField="BotDa" HeaderText="Bột đá" />
                                <asp:BoundField DataField="DaDen2ly" HeaderText="Đá đen 2 ly" />
                                <asp:BoundField DataField="DaTrang2Ly" HeaderText="Đá trắng 2 ly" />
                                <asp:BoundField DataField="DaTrang4Ly" HeaderText="Đá trắng 4 ly" />
                                <asp:BoundField DataField="XiMangPCB401" HeaderText="Xi măng PCB40 1" />
                                <asp:BoundField DataField="XiMangPCB402" HeaderText="Xi măng PCB40 2" />
                                <asp:BoundField DataField="MatDa" HeaderText="Mạt đá" />
                                <asp:BoundField DataField="CatSongDa" HeaderText="Cát sông đà" />
                                <asp:BoundField DataField="DMMauXi" HeaderText="ĐM Màu xi" />
                                <asp:BoundField DataField="DMMauDo" HeaderText="ĐM Màu đỏ" />
                                <asp:BoundField DataField="DMBotDa" HeaderText="ĐM Bột đá" />
                                <asp:BoundField DataField="DMDaDen2ly" HeaderText="ĐM Đá đen 2 ly" />
                                <asp:BoundField DataField="DMDaTrang2Ly" HeaderText="ĐM Đá trắng 2 ly" />
                                <asp:BoundField DataField="DMDaTrang4Ly" HeaderText="ĐM Đá trắng 4 ly" />
                                <asp:BoundField DataField="DMXiMangPCB401" HeaderText="ĐM Xi măng PCB40 1" />
                                <asp:BoundField DataField="DMXiMangPCB402" HeaderText="ĐM Xi măng PCB40 2" />
                                <asp:BoundField DataField="DMMatDa" HeaderText="ĐM Mạt đá" />
                                <asp:BoundField DataField="DMCatSongDa" HeaderText="ĐM Cát sông đà" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
