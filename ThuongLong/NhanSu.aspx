﻿<%@ Page Title="Nhân sự" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NhanSu.aspx.cs" Inherits="ThuongLong.NhanSu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin nhân sự</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">

                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Tên nhân viên</label>
                            <asp:TextBox ID="txtTenNhanVien" runat="server" class="form-control" placeholder="Họ tên" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Mã số thuế cá nhân</label>
                            <asp:TextBox ID="txtMaSoThueCaNhan" runat="server" class="form-control" placeholder="Mã số thuế cá nhân" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày sinh</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgaySinh" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Giới tính</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Giới tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlGioiTinh"
                                runat="server">
                                <asp:ListItem Value="Nam">Nam</asp:ListItem>
                                <asp:ListItem Value="Nữ">Nữ</asp:ListItem>
                                <asp:ListItem Value="Khác">Khác</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Tình trạng hôn nhân</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Tình trạng hôn nhân"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlTinhTrangHonNhan"
                                runat="server">
                                <asp:ListItem Value="Độc thân">Độc thân</asp:ListItem>
                                <asp:ListItem Value="Đã kết hôn">Đã kết hôn</asp:ListItem>
                                <asp:ListItem Value="Chưa kết hôn">Chưa kết hôn</asp:ListItem>
                                <asp:ListItem Value="Đã ly hôn">Đã ly hôn</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Trình độ</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Xã"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlTrinhDo" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chuyên môn</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Xã"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChuyenMon" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Tỉnh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Tỉnh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlTinh" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlTinh_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Huyện</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Huyện"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlHuyen" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlHuyen_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Xã</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Xã"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXa" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">CMT</label>
                            <asp:TextBox ID="txtCMT" runat="server" class="form-control" placeholder="CMT" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Số điện thoại</label>
                            <asp:TextBox ID="txtDienThoai" runat="server" class="form-control" placeholder="SĐT" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày ký hợp đồng</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày kết thúc hợp đồng</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Trạng thái làm việc</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Trạng thái làm việc"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlTrangThaiLamViec"
                                runat="server">
                                <asp:ListItem Value="Đang làm việc">Đang làm việc</asp:ListItem>
                                <asp:ListItem Value="Đã nghỉ việc">Đã nghỉ việc</asp:ListItem>
                                <asp:ListItem Value="Thử việc">Thử việc</asp:ListItem>
                                <asp:ListItem Value="Nghỉ tạm thời">Nghỉ tạm thời</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%" ></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnResetFilter" runat="server" class="btn btn-app bg-warning" OnClick="btnResetFilter_Click"><i class="fa fa-filter"></i>Lọc</asp:LinkButton>
                        </div>

                    </div>
                </div>
                <div class="box-body" id="divdl" visible="false" runat="server">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl1" runat="server">Tên nhân viên</asp:Label><br />
                            <asp:ListBox ID="dlTenNhanVienSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl4" runat="server">Trình độ</asp:Label><br />
                            <asp:ListBox ID="dlTrinhDoSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server">Chuyên môn</asp:Label><br />
                            <asp:ListBox ID="dlChuyenMonSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" />
                                    <asp:BoundField DataField="MaSoThueCaNhan" HeaderText="Mã số thuế cá nhân" />
                                    <asp:BoundField DataField="NgaySinh" HeaderText="Ngày sinh" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="GioiTinh" HeaderText="Giới tính" />
                                    <asp:BoundField DataField="TinhTrangHonNhan" HeaderText="Tình trạng hôn nhân" />
                                    <asp:BoundField DataField="DiaChi" HeaderText="Địa chỉ" />
                                    <asp:BoundField DataField="CMND" HeaderText="CMND" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="DienThoai" HeaderText="Điện thoại" />
                                    <asp:BoundField DataField="TenTrinhDo" HeaderText="Trình độ" />
                                    <asp:BoundField DataField="TenChuyenMon" HeaderText="Chuyên môn" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>

            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" />
                                <asp:BoundField DataField="MaSoThueCaNhan" HeaderText="Mã số thuế cá nhân" />
                                <asp:BoundField DataField="NgaySinh" HeaderText="Ngày sinh" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="GioiTinh" HeaderText="Giới tính" />
                                <asp:BoundField DataField="TinhTrangHonNhan" HeaderText="Tình trạng hôn nhân" />
                                <asp:BoundField DataField="DiaChi" HeaderText="Địa chỉ" />
                                <asp:BoundField DataField="CMND" HeaderText="CMND" />
                                <asp:BoundField DataField="Email" HeaderText="Email" />
                                <asp:BoundField DataField="DienThoai" HeaderText="Điện thoại" />
                                <asp:BoundField DataField="TenTrinhDo" HeaderText="Trình độ" />
                                <asp:BoundField DataField="TenChuyenMon" HeaderText="Chuyên môn" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="btnGiaTri" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btncapnhat" />
            <asp:PostBackTrigger ControlID="btnKhoiLuong" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
