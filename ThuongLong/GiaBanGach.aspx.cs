﻿using NPOI.HSSF.Record;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class GiaBanGach : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmGiaBanGach";
        DataTable dt = new DataTable();
        DataTable dtNVKD = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmGiaBanGach", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNhaCungCap();
                        LoadNhomVatLieu();
                        LoadLoaiVatLieu();
                        LoadDonViTinh();
                        LoadNVKD();
                        hdPage.Value = "1";
                        Session["GiaBanGach"] = null;
                        Session["GiaBanGach_NVKD"] = null;
                        hdForm.Value = Guid.NewGuid().ToString();
                        GetTable();
                        LoadKhachHangSearch();
                    }
                }
            }
        }
        void GetTable()
        {
            dt.Columns.Add("ID", typeof(Guid));
            dt.Columns.Add("IDHD", typeof(Guid));
            dt.Columns.Add("IDNhomVatLieu", typeof(Guid));
            dt.Columns.Add("TenNhomVatLieu", typeof(string));
            dt.Columns.Add("IDLoaiVatLieu", typeof(Guid));
            dt.Columns.Add("TenLoaiVatLieu", typeof(string));
            dt.Columns.Add("IDDonViTinh", typeof(Guid));
            dt.Columns.Add("TenDonViTinh", typeof(string));
            dt.Columns.Add("DonGiaCoThue", typeof(decimal));
            dt.Columns.Add("DonGiaKhongThue", typeof(decimal));
            dt.Columns.Add("TuNgay", typeof(string));
            dt.Columns.Add("DenNgay", typeof(string));
            dt.Columns.Add("TrangThai", typeof(Int32));
            dt.Columns.Add("TrangThaiText", typeof(string));
            dt.Columns.Add("NguoiDuyet", typeof(string));
            dt.Columns.Add("NguoiXoa", typeof(string));
            dt.Columns.Add("NgayTao", typeof(DateTime));
            dt.Columns.Add("NguoiTao", typeof(string));
            dt.Columns.Add("MoTa", typeof(string));
            dt.Columns.Add("Loai", typeof(Int32));
            dt.Columns.Add("hdForm", typeof(string));
        }
        void GetNVKD(DataTable table)
        {
            table.Columns.Add("ID", typeof(Guid));
            table.Columns.Add("IDHD", typeof(Guid));
            table.Columns.Add("IDNhanVien", typeof(Guid));
            table.Columns.Add("TenNhanVien", typeof(string));
            table.Columns.Add("TuNgay", typeof(string));
            table.Columns.Add("DenNgay", typeof(string));
            table.Columns.Add("TrangThai", typeof(Int32));
            table.Columns.Add("TrangThaiText", typeof(string));
            table.Columns.Add("NguoiDuyet", typeof(string));
            table.Columns.Add("NguoiXoa", typeof(string));
            table.Columns.Add("NgayTao", typeof(DateTime));
            table.Columns.Add("NguoiTao", typeof(string));
            table.Columns.Add("MoTa", typeof(string));
            table.Columns.Add("Loai", typeof(Int32));
            table.Columns.Add("hdForm", typeof(string));
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNhaCungCap()
        {
            var query = db.sp_LoadNhaCungCap();
            dlNhaCungCap.DataSource = query;
            dlNhaCungCap.DataBind();
            dlNhaCungCap.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadNhomVatLieu()
        {
            var query = (from p in db.tblNhomVatLieus
                         where p.ID == new Guid("97CF73BB-E552-4A07-9F8B-5DD5C68F7287")
                         || p.ID == new Guid("C2B47A53-3F0A-4E9F-8172-3C3809034BCA")
                         || p.ID == new Guid("7D25A3D1-BE90-4F1D-A0CB-8A518AC09B55")
                         select new
                         {
                             p.ID,
                             Ten = p.TenNhomVatLieu
                         }).OrderBy(p => p.Ten);

            dlNhomVatLieu.DataSource = query;
            dlNhomVatLieu.DataBind();
            dlNhomVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadLoaiVatLieu()
        {
            dlLoaiVatLieu.Items.Clear();
            var query = db.sp_LoadLoaiVatLieuByNhom(GetDrop(dlNhomVatLieu));
            dlLoaiVatLieu.DataSource = query;
            dlLoaiVatLieu.DataBind();
            dlLoaiVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadDonViTinh()
        {
            dlDonViTinh.Items.Clear();
            var query = (from p in db.tblDonViTinhs
                             //where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenDonViTinh
                         }).OrderBy(p => p.Ten);
            dlDonViTinh.DataSource = query;
            dlDonViTinh.DataBind();
            dlDonViTinh.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadNVKD()
        {
            dlNhanVien.Items.Clear();
            var query = (from p in db.tblNhanSus
                         where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenNhanVien
                         }).OrderBy(p => p.Ten);
            dlNhanVien.DataSource = query;
            dlNhanVien.DataBind();
            dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }

            if (s == "")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vatlieu.sp_GiaBanGach_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDNhomVatLieu"].ToString()),
                        new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()),
                        DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);

                    if (s != "")
                    {
                        break;
                    }
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (s == "" && hdID.Value != "")
            {
                vatlieu.sp_GiaBanGach_CheckSuaHopDong(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), ref s);
            }
            if (s == "")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["TrangThai"].ToString() == "3")
                    {
                        vatlieu.sp_GiaBanGach_CheckXoaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), ref s);
                    }
                    else if (dt.Rows[i]["TrangThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() == "0")
                    {
                        vatlieu.sp_GiaBanGach_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDNhomVatLieu"].ToString()),
                            new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()),
                            DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);
                    }
                    else if (dt.Rows[i]["TrangThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() != "1")
                    {
                        vatlieu.sp_GiaBanGach_CheckSuaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDNhomVatLieu"].ToString()),
                            new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()),
                            decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString()), decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString()),
                            DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);
                    }
                    if (s != "")
                    {
                        break;
                    }
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckThemChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlNhomVatLieu.SelectedValue == "")
            {
                s += " - Chọn nhóm vật liệu<br />";
            }
            if (dlLoaiVatLieu.SelectedValue == "")
            {
                s += " - Chọn loại vật liệu<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_GiaBanGach_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlNhomVatLieu.SelectedValue == "")
            {
                s += " - Chọn nhóm vật liệu<br />";
            }
            if (dlLoaiVatLieu.SelectedValue == "")
            {
                s += " - Chọn loại vật liệu<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_GiaBanGach_CheckSuaChiTiet(new Guid(hdChiTiet.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh),
                    decimal.Parse(txtDonGiaCoThue.Text), decimal.Parse(txtDonGiaKhongThue.Text), DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (Session["GiaBanGach"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["GiaBanGach"] as DataTable;
            }

            if (Session["GiaBanGach_NVKD"] == null)
            {
                GetNVKD(dtNVKD);
            }
            else
            {
                dtNVKD = Session["GiaBanGach_NVKD"] as DataTable;
            }
            DateTime ngaykyhd = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
            DateTime ngayss = ngaykyhd;
            DateTime ngaymin = ngaykyhd;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["TuNgay"].ToString() != "" && DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())) < ngayss)
                {
                    ngaymin = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                }
            }

            if (dt.Rows.Count == 0 && gvChiTiet.Rows.Count > 0)
            {
                Warning("Thông tin giá hợp đồng đã thay đổi.");
            }
            else if (dt.Rows.Count == 0)
            {
                Warning("Bạn chưa thiết lập thông tin giá bê tông");
            }
            else if (dt.Rows.Count > 0 && dt.Rows[0]["hdForm"].ToString() != hdForm.Value)
            {
                Warning("Bạn không được mở 2 tab trên cùng 1 trình duyệt.");
            }
            else if (ngaymin < ngaykyhd)
            {
                Warning("Ngày bắt đầu tính giá bán gạch không được nhỏ hơn ngày ký hợp đồng.");
            }
            else
            {
                if (hdID.Value == "")
                {
                    if (CheckThem() == "")
                    {
                        string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            Guid ID = Guid.NewGuid();
                            var GiaBanGach = new tblGiaBanGach()
                            {
                                ID = new Guid(hdChung.Value),
                                NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                                IDChiNhanh = GetDrop(dlChiNhanh),
                                IDNhaCungCap = GetDrop(dlNhaCungCap),
                                CongTrinh = txtCongTrinh.Text.Trim().ToUpper(),
                                //IDNhomVatLieu = GetDrop(dlNhomVatLieu),
                                //IDLoaiVatLieu = GetDrop(dlLoaiVatLieu),
                                //IDDonViTinh = GetDrop(dlDonViTinh),
                                //DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text),
                                //DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text),
                                //TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                                //DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                                TrangThai = 1,
                                TrangThaiText = "Chờ duyệt",
                                //STT = 0,
                                NguoiTao = Session["IDND"].ToString(),
                                NgayTao = DateTime.Now
                            };
                            db.tblGiaBanGaches.InsertOnSubmit(GiaBanGach);

                            List<tblGiaBanGach_ChiTiet> them = new List<tblGiaBanGach_ChiTiet>();

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["Loai"].ToString() != "10")
                                {
                                    var themchitiet = new tblGiaBanGach_ChiTiet();
                                    themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                    themchitiet.IDHD = new Guid(dt.Rows[i]["IDHD"].ToString());
                                    themchitiet.IDNhomVatLieu = new Guid(dt.Rows[i]["IDNhomVatLieu"].ToString());
                                    themchitiet.IDLoaiVatLieu = new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString());
                                    themchitiet.IDDonViTinh = new Guid(dt.Rows[i]["IDDonViTinh"].ToString());
                                    themchitiet.DonGiaCoThue = decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString());
                                    themchitiet.DonGiaKhongThue = decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString());
                                    themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                                    themchitiet.DenNgay = dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString()));
                                    themchitiet.TrangThai = int.Parse(dt.Rows[i]["TrangThai"].ToString());
                                    themchitiet.TrangThaiText = dt.Rows[i]["TrangThaiText"].ToString();
                                    themchitiet.NguoiDuyet = dt.Rows[i]["NguoiDuyet"].ToString();
                                    themchitiet.NguoiXoa = dt.Rows[i]["NguoiXoa"].ToString();
                                    themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                    themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                    themchitiet.MoTa = dt.Rows[i]["MoTa"].ToString();
                                    them.Add(themchitiet);
                                }
                            }
                            db.tblGiaBanGach_ChiTiets.InsertAllOnSubmit(them);


                            List<tblGiaBanGach_NVKD> themnvkd = new List<tblGiaBanGach_NVKD>();

                            for (int i = 0; i < dtNVKD.Rows.Count; i++)
                            {
                                if (dtNVKD.Rows[i]["Loai"].ToString() != "10")
                                {
                                    var themchitiet = new tblGiaBanGach_NVKD();
                                    themchitiet.ID = new Guid(dtNVKD.Rows[i]["ID"].ToString());
                                    themchitiet.IDHD = new Guid(dtNVKD.Rows[i]["IDHD"].ToString());
                                    themchitiet.IDNhanVien = new Guid(dtNVKD.Rows[i]["IDNhanVien"].ToString());
                                    themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dtNVKD.Rows[i]["TuNgay"].ToString()));
                                    themchitiet.DenNgay = dtNVKD.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dtNVKD.Rows[i]["DenNgay"].ToString()));
                                    themchitiet.TrangThai = int.Parse(dtNVKD.Rows[i]["TrangThai"].ToString());
                                    themchitiet.TrangThaiText = dtNVKD.Rows[i]["TrangThaiText"].ToString();
                                    themchitiet.NguoiDuyet = dtNVKD.Rows[i]["NguoiDuyet"].ToString();
                                    themchitiet.NguoiXoa = dtNVKD.Rows[i]["NguoiXoa"].ToString();
                                    themchitiet.NgayTao = DateTime.Parse(dtNVKD.Rows[i]["NgayTao"].ToString());
                                    themchitiet.NguoiTao = dtNVKD.Rows[i]["NguoiTao"].ToString();
                                    themchitiet.MoTa = dtNVKD.Rows[i]["MoTa"].ToString();
                                    themnvkd.Add(themchitiet);
                                }
                            }
                            db.tblGiaBanGach_NVKDs.InsertAllOnSubmit(themnvkd);



                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Lưu thành công.");
                        }
                    }
                }
                else
                {
                    if (CheckSua() == "")
                    {

                        string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            var query = (from p in db.tblGiaBanGaches
                                         where p.ID == new Guid(hdID.Value)
                                         select p).FirstOrDefault();
                            if (query != null && query.ID != null)
                            {
                                DateTime dtcheck = DateTime.Parse(hdNgayTao.Value);
                                if (hdID.Value != ""
                                    && (
                                    dtcheck.Year != query.NgayTao.Value.Year
                                    || dtcheck.Month != query.NgayTao.Value.Month
                                    || dtcheck.Day != query.NgayTao.Value.Day
                                    || dtcheck.Hour != query.NgayTao.Value.Hour
                                    || dtcheck.Minute != query.NgayTao.Value.Minute
                                    || dtcheck.Second != query.NgayTao.Value.Second
                                    )
                                    )
                                {
                                    Warning("Thông tin hợp đồng đã bị sửa, xin vui lòng kiểm tra lại.");
                                }
                                else
                                {

                                    query.IDChiNhanh = GetDrop(dlChiNhanh);
                                    query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                                    query.CongTrinh = txtCongTrinh.Text.Trim().ToUpper();
                                    //query.IDNhomVatLieu = GetDrop(dlNhomVatLieu);
                                    //query.IDLoaiVatLieu = GetDrop(dlLoaiVatLieu);
                                    //query.IDDonViTinh = GetDrop(dlDonViTinh);
                                    //query.DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text);
                                    //query.DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text);
                                    //query.TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text));
                                    //query.DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text));
                                    query.TrangThai = 1;
                                    query.TrangThaiText = "Chờ duyệt";
                                    //query.STT = query.STT + 1;
                                    query.NguoiTao = Session["IDND"].ToString();
                                    query.NgayTao = DateTime.Now;


                                    List<tblGiaBanGach_ChiTiet> xoa = new List<tblGiaBanGach_ChiTiet>();
                                    List<tblGiaBanGach_ChiTiet> them = new List<tblGiaBanGach_ChiTiet>();

                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        if (dt.Rows[i]["Loai"].ToString() != "10")
                                        {
                                            var xoachitiet = (from p in db.tblGiaBanGach_ChiTiets
                                                              where p.ID == new Guid(dt.Rows[i]["ID"].ToString())
                                                              select p).FirstOrDefault();
                                            if (xoachitiet != null && xoachitiet.ID != null)
                                            {
                                                xoa.Add(xoachitiet);
                                            }
                                            var themchitiet = new tblGiaBanGach_ChiTiet();
                                            themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                            themchitiet.IDHD = new Guid(dt.Rows[i]["IDHD"].ToString());
                                            themchitiet.IDNhomVatLieu = new Guid(dt.Rows[i]["IDNhomVatLieu"].ToString());
                                            themchitiet.IDLoaiVatLieu = new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString());
                                            themchitiet.IDDonViTinh = new Guid(dt.Rows[i]["IDDonViTinh"].ToString());
                                            themchitiet.DonGiaCoThue = decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString());
                                            themchitiet.DonGiaKhongThue = decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString());
                                            themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                                            themchitiet.DenNgay = dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString()));
                                            themchitiet.TrangThai = int.Parse(dt.Rows[i]["TrangThai"].ToString());
                                            themchitiet.TrangThaiText = dt.Rows[i]["TrangThaiText"].ToString();
                                            themchitiet.NguoiDuyet = dt.Rows[i]["NguoiDuyet"].ToString();
                                            themchitiet.NguoiXoa = dt.Rows[i]["NguoiXoa"].ToString();
                                            themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                            themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                            themchitiet.MoTa = dt.Rows[i]["MoTa"].ToString();
                                            them.Add(themchitiet);
                                        }
                                    }
                                    db.tblGiaBanGach_ChiTiets.DeleteAllOnSubmit(xoa);
                                    db.tblGiaBanGach_ChiTiets.InsertAllOnSubmit(them);

                                    List<tblGiaBanGach_NVKD> xoanvkd = new List<tblGiaBanGach_NVKD>();
                                    List<tblGiaBanGach_NVKD> themnvkd = new List<tblGiaBanGach_NVKD>();

                                    for (int i = 0; i < dtNVKD.Rows.Count; i++)
                                    {
                                        if (dtNVKD.Rows[i]["Loai"].ToString() != "10")
                                        {
                                            var xoachitiet = (from p in db.tblGiaBanGach_NVKDs
                                                              where p.ID == new Guid(dtNVKD.Rows[i]["ID"].ToString())
                                                              select p).FirstOrDefault();
                                            if (xoachitiet != null && xoachitiet.ID != null)
                                            {
                                                xoanvkd.Add(xoachitiet);
                                            }
                                            var themchitiet = new tblGiaBanGach_NVKD();
                                            themchitiet.ID = new Guid(dtNVKD.Rows[i]["ID"].ToString());
                                            themchitiet.IDHD = new Guid(dtNVKD.Rows[i]["IDHD"].ToString());
                                            themchitiet.IDNhanVien = new Guid(dtNVKD.Rows[i]["IDNhanVien"].ToString());
                                            themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dtNVKD.Rows[i]["TuNgay"].ToString()));
                                            themchitiet.DenNgay = dtNVKD.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dtNVKD.Rows[i]["DenNgay"].ToString()));
                                            themchitiet.TrangThai = int.Parse(dtNVKD.Rows[i]["TrangThai"].ToString());
                                            themchitiet.TrangThaiText = dtNVKD.Rows[i]["TrangThaiText"].ToString();
                                            themchitiet.NguoiDuyet = dtNVKD.Rows[i]["NguoiDuyet"].ToString();
                                            themchitiet.NguoiXoa = dtNVKD.Rows[i]["NguoiXoa"].ToString();
                                            themchitiet.NgayTao = DateTime.Parse(dtNVKD.Rows[i]["NgayTao"].ToString());
                                            themchitiet.NguoiTao = dtNVKD.Rows[i]["NguoiTao"].ToString();
                                            themchitiet.MoTa = dtNVKD.Rows[i]["MoTa"].ToString();
                                            themnvkd.Add(themchitiet);
                                        }
                                    }
                                    db.tblGiaBanGach_NVKDs.DeleteAllOnSubmit(xoanvkd);
                                    db.tblGiaBanGach_NVKDs.InsertAllOnSubmit(themnvkd);

                                    db.SubmitChanges();

                                    lblTaoMoiHopDong_Click(sender, e);
                                    Search(1);
                                    Success("Sửa thành công");
                                }
                            }
                            else
                            {
                                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                                lblTaoMoiHopDong_Click(sender, e);
                            }
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlNhaCungCap.SelectedValue = "";
            txtCongTrinh.Text = "";
            dlNhomVatLieu.SelectedValue = "";
            LoadLoaiVatLieu();
            dlDonViTinh.SelectedValue = "";
            hdID.Value = "";
            hdForm.Value = "";
            hdChiTiet.Value = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
            dlNhanVien.SelectedValue = "";
            hdKinhDoanh.Value = "";
            txtKDTuNgay.Text = "";
            txtKDDenNgay.Text = "";

            Session["GiaBanGach"] = null;
            gvChiTiet.DataSource = null;
            gvChiTiet.DataBind();

            Session["GiaBanGach_NVKD"] = null;
            gvNVKD.DataSource = null;
            gvNVKD.DataBind();

            LoadKhachHangSearch();
        }
        protected void lbtSave_Click(object sender, EventArgs e)
        {
            string url = "";
            if (Session["GiaBanGach"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["GiaBanGach"] as DataTable;
            }

            if (hdChiTiet.Value == "")
            {
                if (CheckThemChiTiet() == "" && CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (Check(dt, txtTuNgay.Text.Trim(), txtDenNgay.Text.Trim()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            if (hdID.Value == "" && hdChung.Value == "")
                                hdChung.Value = Guid.NewGuid().ToString();

                            dt.Rows.Add(Guid.NewGuid(),
                            new Guid(hdChung.Value),
                            GetDrop(dlNhomVatLieu),
                            dlNhomVatLieu.SelectedItem.Text.ToString(),
                            GetDrop(dlLoaiVatLieu),
                            dlLoaiVatLieu.SelectedItem.Text.ToString(),
                            GetDrop(dlDonViTinh),
                            dlDonViTinh.SelectedItem.Text.ToString(),
                            decimal.Parse(txtDonGiaCoThue.Text),
                            decimal.Parse(txtDonGiaKhongThue.Text),
                            txtTuNgay.Text.Trim(),
                            txtDenNgay.Text.Trim(),
                            1,
                            "Chờ duyệt",
                            "",
                            "",
                            DateTime.Now, Session["IDND"].ToString(),
                            "",
                            0,
                            hdForm.Value
                            );

                            lbtTaoGiaMoi_Click(sender, e);
                            gvChiTiet.DataSource = dt;
                            gvChiTiet.DataBind();
                            Session["GiaBanGach"] = dt;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
            else
            {
                if (CheckSuaChiTiet() == "" && CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckSua(dt, txtTuNgay.Text.Trim(), txtDenNgay.Text.Trim(), hdChiTiet.Value.ToUpper()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(hdChiTiet.Value)).FirstOrDefault();
                            if (dr != null)
                            {
                                dr["IDHD"] = new Guid(hdChung.Value);
                                dr["IDNhomVatLieu"] = GetDrop(dlNhomVatLieu);
                                dr["TenNhomVatLieu"] = dlNhomVatLieu.SelectedItem.Text.ToString();
                                dr["IDLoaiVatLieu"] = GetDrop(dlLoaiVatLieu);
                                dr["TenLoaiVatLieu"] = dlLoaiVatLieu.SelectedItem.Text.ToString();
                                dr["IDDonViTinh"] = GetDrop(dlDonViTinh);
                                dr["TenDonViTinh"] = dlDonViTinh.SelectedItem.Text.ToString();
                                dr["DonGiaCoThue"] = decimal.Parse(txtDonGiaCoThue.Text);
                                dr["DonGiaKhongThue"] = decimal.Parse(txtDonGiaKhongThue.Text);
                                dr["TuNgay"] = txtTuNgay.Text;
                                dr["DenNgay"] = txtDenNgay.Text;
                                dr["TrangThai"] = 1;
                                dr["TrangThaiText"] = "Chờ duyệt";
                                dr["NguoiDuyet"] = "";
                                dr["NguoiXoa"] = "";
                                dr["NgayTao"] = DateTime.Now;
                                dr["NguoiTao"] = Session["IDND"].ToString();
                                dr["MoTa"] = "";
                                dr["Loai"] = 1;
                                dr["hdForm"] = hdForm.Value;
                            }

                            lbtTaoGiaMoi_Click(sender, e);
                            gvChiTiet.DataSource = dt;
                            gvChiTiet.DataBind();
                            Session["GiaBanGach"] = dt;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
        }

        protected void lbtTaoGiaMoi_Click(object sender, EventArgs e)
        {
            dlNhomVatLieu.SelectedValue = "";
            LoadLoaiVatLieu();
            dlDonViTinh.SelectedValue = "";
            hdChiTiet.Value = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
        }

        protected void gvChiTiet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();

            if (Session["GiaBanGach"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["GiaBanGach"] as DataTable;
            }

            DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(id)).FirstOrDefault();
            if (dr != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlNhomVatLieu.SelectedValue = dr["IDNhomVatLieu"].ToString();
                        LoadLoaiVatLieu();
                        dlLoaiVatLieu.SelectedValue = dr["IDLoaiVatLieu"].ToString();
                        dlDonViTinh.SelectedValue = dr["IDDonViTinh"].ToString();
                        txtDonGiaCoThue.Text = string.Format("{0:N2}", dr["DonGiaCoThue"]);
                        txtDonGiaKhongThue.Text = string.Format("{0:N2}", dr["DonGiaKhongThue"]);
                        txtTuNgay.Text = dr["TuNgay"].ToString();
                        txtDenNgay.Text = dr["DenNgay"].ToString();
                        hdChiTiet.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_GiaBanGach_CheckXoaChiTiet(new Guid(id), ref thongbao);
                        if (thongbao != "")
                        {
                            Warning(thongbao);
                        }
                        else
                        {
                            var query = (from p in db.tblGiaBanGach_ChiTiets
                                         where p.ID == new Guid(id)
                                         select p).FirstOrDefault();

                            if (query != null && query.ID != null)
                            {
                                dr["Loai"] = "3";
                                dr["TrangThai"] = "3";
                                dr["TrangThaiText"] = "Chờ duyệt xóa";

                                Success("Xóa thành công.");
                                gvChiTiet.DataSource = dt;
                                gvChiTiet.DataBind();
                                Session["GiBanGach"] = dt;
                                lbtTaoGiaMoi_Click(sender, e);
                            }
                            else
                            {
                                dt.Rows.Remove(dr);
                                dt.AcceptChanges();

                                Success("Xóa thành công.");
                                gvChiTiet.DataSource = dt;
                                gvChiTiet.DataBind();
                                Session["GiBanGach"] = dt;
                                lbtTaoGiaMoi_Click(sender, e);
                            }
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_GiaBanGach_LichSuChiTiet(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblGiaBanGaches
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        txtCongTrinh.Text = query.CongTrinh;
                        hdID.Value = id;
                        hdChung.Value = id;
                        hdNgayTao.Value = query.NgayTao.ToString();
                        hdForm.Value = Guid.NewGuid().ToString();
                        LoadChiTiet();
                        LoadChiTietKD();
                    }
                }
                else if (e.CommandName == "duplicate")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        txtCongTrinh.Text = query.CongTrinh;
                        hdDuplicate.Value = id;
                        hdChung.Value = Guid.NewGuid().ToString();
                        hdNgayTao.Value = query.NgayTao.ToString();
                        hdForm.Value = Guid.NewGuid().ToString();
                        LoadChiTietDuplicate();
                        LoadChiTietKDDuplicate();
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_GiaBanGach_CheckXoa(query.ID, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblGiaBanGach_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblGiaBanGaches.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = vatlieu.sp_GiaBanGach_LichSu(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        string CheckThemKDChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlNhanVien.SelectedValue == "")
            {
                s += " - Chọn nhân viên kinh doanh<br />";
            }
            //if (txtKDDonGiaCoThue.Text == "" || decimal.Parse(txtKDDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtKDDonGiaKhongThue.Text == "" || decimal.Parse(txtKDDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtKDTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            if (s == "")
            {
                vatlieu.sp_GiaBanGach_CheckThemKDChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlNhanVien),
                    DateTime.Parse(GetNgayThang(txtKDTuNgay.Text)), txtKDDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtKDDenNgay.Text)), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaKDChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (txtKDTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            //if (txtKDDonGiaCoThue.Text == "" || decimal.Parse(txtKDDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtKDDonGiaKhongThue.Text == "" || decimal.Parse(txtKDDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtKDTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_GiaBanGach_CheckSuaKDChiTiet(new Guid(hdKinhDoanh.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlNhanVien),
                    DateTime.Parse(GetNgayThang(txtKDTuNgay.Text)), txtKDDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtKDDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        private string CheckKD(DataTable tableinsert, string tungay, string denngay)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                else _err = "";
            }
            if (_err != "")
            {
                Warning(_err);
            }

            return _err;

        }
        private string CheckSuaKD(DataTable tableinsert, string tungay, string denngay, string ids)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDNhanVien") == GetDrop(dlNhanVien) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDNhanVien") == GetDrop(dlNhanVien) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                else _err = "";
            }

            if (_err != "")
            {
                Warning(_err);
            }

            return _err;
        }
        private string Check(DataTable tableinsert, string tungay, string denngay)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDLoaiVatLieu") == GetDrop(dlLoaiVatLieu) &&
                                p.Field<Guid>("IDDonViTinh") == GetDrop(dlDonViTinh) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDLoaiVatLieu") == GetDrop(dlLoaiVatLieu) &&
                                p.Field<Guid>("IDDonViTinh") == GetDrop(dlDonViTinh) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                else _err = "";
            }
            if (_err != "")
            {
                Warning(_err);
            }

            return _err;

        }
        private string CheckSua(DataTable tableinsert, string tungay, string denngay, string ids)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDLoaiVatLieu") == GetDrop(dlLoaiVatLieu) &&
                                p.Field<Guid>("IDDonViTinh") == GetDrop(dlDonViTinh) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDLoaiVatLieu") == GetDrop(dlLoaiVatLieu) &&
                                p.Field<Guid>("IDDonViTinh") == GetDrop(dlDonViTinh) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 loại vật liệu";
                else _err = "";
            }

            if (_err != "")
            {
                Warning(_err);
            }

            return _err;
        }

        protected void lbtSaveNV_Click(object sender, EventArgs e)
        {
            string url = "";
            if (Session["GiaBanGach_NVKD"] == null)
            {
                GetNVKD(dtNVKD);
            }
            else
            {
                dtNVKD = Session["GiaBanGach_NVKD"] as DataTable;
            }

            if (hdKinhDoanh.Value == "")
            {
                if (CheckThemKDChiTiet() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckKD(dtNVKD, txtKDTuNgay.Text.Trim(), txtKDDenNgay.Text.Trim()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            if (hdID.Value == "" && hdChung.Value == "")
                                hdChung.Value = Guid.NewGuid().ToString();

                            dtNVKD.Rows.Add(Guid.NewGuid(),
                            new Guid(hdChung.Value),
                            GetDrop(dlNhanVien),
                            dlNhanVien.SelectedItem.Text.ToString(),
                            txtKDTuNgay.Text.Trim(),
                            txtKDDenNgay.Text.Trim(),
                            1,
                            "Chờ duyệt",
                            "",
                            "",
                            DateTime.Now, Session["IDND"].ToString(),
                            "",
                            0,
                            hdForm.Value
                            );

                            lbtTaoMoiNV_Click(sender, e);
                            gvNVKD.DataSource = dtNVKD;
                            gvNVKD.DataBind();
                            Session["GiaBanGach_NVKD"] = dtNVKD;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
            else
            {
                if (CheckSuaKDChiTiet() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckSuaKD(dtNVKD, txtKDTuNgay.Text.Trim(), txtKDDenNgay.Text.Trim(), hdKinhDoanh.Value.ToUpper()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            DataRow dr = dtNVKD.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(hdKinhDoanh.Value)).FirstOrDefault();
                            if (dr != null)
                            {
                                dr["IDHD"] = new Guid(hdChung.Value);
                                dr["IDNhanVien"] = GetDrop(dlNhanVien);
                                dr["TenNhanVien"] = dlNhanVien.SelectedItem.Text.ToString();
                                dr["TuNgay"] = txtKDTuNgay.Text;
                                dr["DenNgay"] = txtKDDenNgay.Text;
                                dr["TrangThai"] = 1;
                                dr["TrangThaiText"] = "Chờ duyệt";
                                dr["NguoiDuyet"] = "";
                                dr["NguoiXoa"] = "";
                                dr["NgayTao"] = DateTime.Now;
                                dr["NguoiTao"] = Session["IDND"].ToString();
                                dr["MoTa"] = "";
                                dr["Loai"] = 1;
                                dr["hdForm"] = hdForm.Value;
                            }

                            lbtTaoMoiNV_Click(sender, e);
                            gvNVKD.DataSource = dtNVKD;
                            gvNVKD.DataBind();
                            Session["GiaBanGach_NVKD"] = dtNVKD;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
        }

        protected void lbtTaoMoiNV_Click(object sender, EventArgs e)
        {
            dlNhanVien.SelectedValue = "";
            hdKinhDoanh.Value = "";
            txtKDTuNgay.Text = "";
            txtKDDenNgay.Text = "";
        }

        protected void gvNVKD_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();

            if (Session["GiaBanGach_NVKD"] == null)
            {
                GetNVKD(dtNVKD);
            }
            else
            {
                dtNVKD = Session["GiaBanGach_NVKD"] as DataTable;
            }

            DataRow dr = dtNVKD.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(id)).FirstOrDefault();
            if (dr != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlNhanVien.SelectedValue = dr["IDNhanVien"].ToString();
                        txtKDTuNgay.Text = dr["TuNgay"].ToString();
                        txtKDDenNgay.Text = dr["DenNgay"].ToString();
                        hdKinhDoanh.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_GiaBanGach_CheckXoaKD(new Guid(id), ref thongbao);
                        if (thongbao != "")
                        {
                            Warning(thongbao);
                        }
                        else
                        {
                            var query = (from p in db.tblGiaBanGach_NVKDs
                                         where p.ID == new Guid(id)
                                         select p).FirstOrDefault();

                            if (query != null && query.ID != null)
                            {
                                dr["Loai"] = "3";
                                dr["TrangThai"] = "3";
                                dr["TrangThaiText"] = "Chờ duyệt xóa";

                                Success("Xóa thành công.");
                                gvNVKD.DataSource = dtNVKD;
                                gvNVKD.DataBind();
                                Session["GiBanGach"] = dtNVKD;
                                lbtTaoMoiNV_Click(sender, e);
                            }
                            else
                            {
                                dtNVKD.Rows.Remove(dr);
                                dtNVKD.AcceptChanges();

                                Success("Xóa thành công.");
                                gvNVKD.DataSource = dtNVKD;
                                gvNVKD.DataBind();
                                Session["GiBanGach"] = dtNVKD;
                                lbtTaoMoiNV_Click(sender, e);
                            }
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_GiaBanGach_LichSuKinhDoanh(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin nhân viên kinh doanh đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void dlNhomVatLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiVatLieu();
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            var query = vatlieu.sp_GiaBanGach_Search(GetDrop(dlChiNhanhSearch), dlKhachHangSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlKhachHangSearch), 20, page);

            GV.DataSource = query;
            GV.DataBind();
        }
        private void LoadChiTiet()
        {
            List<sp_GiaBanGach_ChiTietSearchResult> query = vatlieu.sp_GiaBanGach_ChiTietSearch(new Guid(hdID.Value), hdForm.Value).ToList();
            GetTable();
            foreach (var item in query)
            {
                dt.Rows.Add(
                                item.ID,
                                item.IDHD,
                                item.IDNhomVatLieu,
                                item.TenNhomVatLieu,
                                item.IDLoaiVatLieu,
                                item.TenLoaiVatLieu,
                                item.IDDonViTinh,
                                item.TenDonViTinh,
                                item.DonGiaCoThue,
                                item.DonGiaKhongThue,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                item.TrangThai,
                                item.TrangThaiText,
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                item.Loai,
                                item.hdForm
                            );
            }
            gvChiTiet.DataSource = query;
            gvChiTiet.DataBind();
            Session["GiaBanGach"] = dt;
        }
        private void LoadChiTietKD()
        {
            List<sp_GiaBanGach_ChiTietKDSearchResult> query = vatlieu.sp_GiaBanGach_ChiTietKDSearch(new Guid(hdID.Value), hdForm.Value).ToList();
            GetNVKD(dtNVKD);
            foreach (var item in query)
            {
                dtNVKD.Rows.Add(
                                item.ID,
                                item.IDHD,
                                item.IDNhanVien,
                                item.TenNhanVien,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                item.TrangThai,
                                item.TrangThaiText,
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                item.Loai,
                                item.hdForm
                            );
            }
            gvNVKD.DataSource = query;
            gvNVKD.DataBind();
            Session["GiaBanGach_NVKD"] = dtNVKD;
        }
        private void LoadChiTietDuplicate()
        {
            List<sp_GiaBanGach_ChiTietSearchResult> query = vatlieu.sp_GiaBanGach_ChiTietSearch(new Guid(hdDuplicate.Value), hdForm.Value).ToList();
            GetTable();
            foreach (var item in query)
            {
                dt.Rows.Add(
                                Guid.NewGuid(),
                                new Guid(hdDuplicate.Value),
                                item.IDNhomVatLieu,
                                item.TenNhomVatLieu,
                                item.IDLoaiVatLieu,
                                item.TenLoaiVatLieu,
                                item.IDDonViTinh,
                                item.TenDonViTinh,
                                item.DonGiaCoThue,
                                item.DonGiaKhongThue,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                1,
                                "Chờ duyệt",
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                0,
                                item.hdForm
                            );
            }
            gvChiTiet.DataSource = dt;
            gvChiTiet.DataBind();
            Session["GiaBanGach"] = dt;
        }
        private void LoadChiTietKDDuplicate()
        {
            List<sp_GiaBanGach_ChiTietKDSearchResult> query = vatlieu.sp_GiaBanGach_ChiTietKDSearch(new Guid(hdDuplicate.Value), hdForm.Value).ToList();
            GetNVKD(dtNVKD);
            foreach (var item in query)
            {
                dtNVKD.Rows.Add(
                                Guid.NewGuid(),
                                new Guid(hdDuplicate.Value),
                                item.IDNhanVien,
                                item.TenNhanVien,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                1,
                                "Chờ duyệt",
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                0,
                                item.hdForm
                            );
            }
            gvNVKD.DataSource = dtNVKD;
            gvNVKD.DataBind();
            Session["GiaBanGach_NVKD"] = dtNVKD;
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadKhachHangSearch();
        }
        void LoadKhachHangSearch()
        {
            dlKhachHangSearch.Items.Clear();
            var query = vatlieu.sp_GiaBanGach_LoadNhaCungCapSearch(GetDrop(dlChiNhanhSearch));
            dlKhachHangSearch.DataSource = query;
            dlKhachHangSearch.DataBind();
            dlKhachHangSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnTaoMoiCongTrinh_Click(object sender, EventArgs e)
        {
            mpCongTrinh.Show();
            txtTenNhaCungCap.Text = "";
            txtSoDienThoai.Text = "";
            txtMaSoThue.Text = "";
            txtDiaChi.Text = "";
        }
        protected void btnLuuCongTrinh_Click(object sender, EventArgs e)
        {
            var checktrung = from p in db.tblNhaCungCaps
                             where p.TenNhaCungCap.ToUpper() == txtTenNhaCungCap.Text.Trim().ToUpper()
                             && p.IDNhomNhaCungCap == GetDrop(dlLoaiNhaCungCap)
                             select p;
            if (txtTenNhaCungCap.Text.Trim() == "")
            {
                GstGetMess("Nhập tên nhà cung cấp.", "");
                mpCongTrinh.Show();
            }
            else if (checktrung.Count() > 0)
            {
                GstGetMess("Tên nhà cung cấp theo loại nhà cung cấp đã tồn tại.", "");
                mpCongTrinh.Show();
            }
            else
            {
                Guid id = Guid.NewGuid();
                var insertcongtrinh = new tblNhaCungCap()
                {
                    ID = Guid.NewGuid(),
                    IDNhomNhaCungCap = GetDrop(dlLoaiNhaCungCap),
                    TenNhaCungCap = txtTenNhaCungCap.Text.Trim(),
                    SoDienThoai = txtSoDienThoai.Text.Trim(),
                    MaSoThue = txtMaSoThue.Text.Trim(),
                    DiaChi = txtDiaChi.Text.Trim(),
                    NgayTao = DateTime.Now,
                    NguoiTao = Session["IDND"].ToString()
                };
                db.tblNhaCungCaps.InsertOnSubmit(insertcongtrinh);
                db.SubmitChanges();
                LoadNhaCungCap();
                //dlNhaCungCap.SelectedValue = id.ToString().ToUpper();
            }
        }
    }
}