﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class DoiMatKhau : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["IDND"] = null;
            if (!IsPostBack)
            {
                LoadDanhMucCauHoi();
            }
        }
        void LoadDanhMucCauHoi()
        {
            List<tblCauHoiBiMat> query = (from p in db.tblCauHoiBiMats
                                          orderby p.ID
                                          select p).ToList();
            dlCauHoi1.DataSource = query;
            dlCauHoi1.DataBind();
            dlCauHoi2.DataSource = query;
            dlCauHoi2.DataBind();
        }
        string Mahoa(string password)
        {
            MD5 mh = MD5.Create();
            //Chuyển kiểu chuổi thành kiểu byte
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(password);
            //mã hóa chuỗi đã chuyển
            byte[] hash = mh.ComputeHash(inputBytes);
            //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            var query = (from p in db.tblUserAccounts
                         where p.UserName.ToUpper() == txtUsername.Text.ToUpper()
                         select p).FirstOrDefault();
            if (query != null && query.UserName != null)
            {
                if (dlCauHoi1.SelectedValue == "")
                {
                    Warning("Chọn câu hỏi 1");
                }
                else if (txtTraLoi1.Text.Trim() == "")
                {
                    Warning("Nhập câu trả lời 1");
                }
                else if (dlCauHoi2.SelectedValue == "")
                {
                    Warning("Chọn câu hỏi 2");
                }
                else if (txtTraLoi2.Text.Trim() == "")
                {
                    Warning("Nhập câu trả lời 2");
                }
                else if (txtPassword.Text.Trim() == "")
                {
                    Warning("Nhập mật khẩu");
                }
                else
                {
                    int dem = 0;
                    if (int.Parse(dlCauHoi1.SelectedValue) == query.CauHoi1 && txtTraLoi1.Text.Trim().Replace(" ", "").ToLower() == query.TraLoi1.Replace(" ", "").ToLower())
                    {
                        dem++;
                    }
                    if (int.Parse(dlCauHoi1.SelectedValue) == query.CauHoi2 && txtTraLoi1.Text.Trim().Replace(" ", "").ToLower() == query.TraLoi2.Replace(" ", "").ToLower())
                    {
                        dem++;
                    }
                    if (int.Parse(dlCauHoi1.SelectedValue) == query.CauHoi3 && txtTraLoi1.Text.Trim().Replace(" ", "").ToLower() == query.TraLoi3.Replace(" ", "").ToLower())
                    {
                        dem++;
                    }

                    if (int.Parse(dlCauHoi2.SelectedValue) == query.CauHoi1 && txtTraLoi2.Text.Trim().Replace(" ", "").ToLower() == query.TraLoi1.Replace(" ", "").ToLower())
                    {
                        dem++;
                    }
                    if (int.Parse(dlCauHoi2.SelectedValue) == query.CauHoi2 && txtTraLoi2.Text.Trim().Replace(" ", "").ToLower() == query.TraLoi2.Replace(" ", "").ToLower())
                    {
                        dem++;
                    }
                    if (int.Parse(dlCauHoi2.SelectedValue) == query.CauHoi3 && txtTraLoi2.Text.Trim().Replace(" ", "").ToLower() == query.TraLoi3.Replace(" ", "").ToLower())
                    {
                        dem++;
                    }
                    if (dem < 2)
                    {
                        Warning("Câu trả lời không đúng");
                    }
                    else
                    {
                        query.Password = Mahoa(txtPassword.Text.Trim());
                        db.SubmitChanges();
                        GstGetMess("Đổi thành công","Login.aspx");
                    }    
                }
            }
            else
            {
                Warning("Thông tin tài khoản không đúng.");
            }

        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void LuuThanhCong(string mess, string gstLink)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');window.location.href='" + gstLink + "'", true);
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
    }
}