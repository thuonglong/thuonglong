﻿<%@ Page Title="Kế hoạch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="KeHoach.aspx.cs" Inherits="ThuongLong.KeHoach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin kế hoạch</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày làm kế hoạch</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Loại kế hoạch</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại kế hoạch"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiKeHoach"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Tên kế hoạch</label>
                            <asp:TextBox ID="txtTenKeHoach" runat="server" class="form-control" placeholder="Tên kế hoạch" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="exampleInputEmail1">Nội dung</label>
                            <asp:TextBox ID="txtNoiDung" runat="server" class="form-control" placeholder="Nội dung" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đơn vị thực hiện</label>
                            <asp:TextBox ID="txtDonViThucHien" runat="server" class="form-control" placeholder="Đơn vị thực hiện" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Hình ảnh</label>
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Muc tiêu tài chính</label>
                            <asp:TextBox ID="txtSoTien" runat="server" class="form-control" placeholder="Nhập số tiền" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtSoTien" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Mục tiêu sản xuất</label>
                            <asp:TextBox ID="txtSoLuong" runat="server" class="form-control" placeholder="Nhập số lượng" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Tháng thực hiện</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Tháng thực hiện"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThangKeHoach"
                                    runat="server">
                                    <asp:ListItem Value="1">Tháng 1</asp:ListItem>
                                    <asp:ListItem Value="2">Tháng 2</asp:ListItem>
                                    <asp:ListItem Value="3">Tháng 3</asp:ListItem>
                                    <asp:ListItem Value="4">Tháng 4</asp:ListItem>
                                    <asp:ListItem Value="5">Tháng 5</asp:ListItem>
                                    <asp:ListItem Value="6">Tháng 6</asp:ListItem>
                                    <asp:ListItem Value="7">Tháng 7</asp:ListItem>
                                    <asp:ListItem Value="8">Tháng 8</asp:ListItem>
                                    <asp:ListItem Value="9">Tháng 9</asp:ListItem>
                                    <asp:ListItem Value="10">Tháng 10</asp:ListItem>
                                    <asp:ListItem Value="11">Tháng 11</asp:ListItem>
                                    <asp:ListItem Value="12">Tháng 12</asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:TextBox ID="txtThangKeHoach" runat="server" class="form-control"  Width="98%"></asp:TextBox>--%>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Năm thực hiện</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNamKeHoach" runat="server" class="form-control" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách Kế hoạch</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server" Text="Loại kế hoạch"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại kế hoạch"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiKeHoachSearch"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-6">
                            <asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" class="btn btn-primary" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
<%--                                    <asp:TemplateField HeaderText="Xem" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtHinhAnh" runat="server" class="btn btn-small bg-green" CommandArgument='<%#Eval("ID")%>' ToolTip="Xem" CommandName="HinhAnh"><i class="fa fa-image"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-primary" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-red" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="NgayThang" HeaderText="NgayThang" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenLoaiKeHoach" HeaderText="Loại kế hoạch" />
                                    <asp:BoundField DataField="TenKeHoach" HeaderText="Tên kế hoạch" />
                                    <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />
                                    <asp:BoundField DataField="DonViThucHien" HeaderText="Đơn vị thực hiện" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdHinhAnh" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="NgayThang" HeaderText="NgayThang" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenLoaiKeHoach" HeaderText="Loại kế hoạch" />
                                <asp:BoundField DataField="TenKeHoach" HeaderText="Tên kế hoạch" />
                                <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />
                                <asp:BoundField DataField="DonViThucHien" HeaderText="Đơn vị thực hiện" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpHinhAnh" runat="server" CancelControlID="btnDongHinhAnh"
                Drag="True" TargetControlID="hdHinhAnh" BackgroundCssClass="modalBackground" PopupControlID="pnHinhAnh"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnHinhAnh" runat="server" Style="width: 98%; height: 100%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Hình ảnh
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:Image ID="imgHinhAnh" runat="server" ImageUrl="" />
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongHinhAnh" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtLuuHopDong" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
