﻿<%@ Page Title="Nhật trình xe bê tông" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NhatTrinhXeBeTong.aspx.cs" Inherits="ThuongLong.NhatTrinhXeBeTong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày tháng</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Biển số xe</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXe" runat="server" OnSelectedIndexChanged="dlXe_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Nội dung</label>
                            <asp:TextBox ID="txtNoiDung" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-danger">
                                <h3 class="box-title">Định mức nhiên liệu</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển có tải</label>
                                    <asp:TextBox ID="txtDMDiChuyenCoTaiBeTong" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMDiChuyenCoTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển không tải</label>
                                    <asp:TextBox ID="txtDMDiChuyenKhongTaiBeTong" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMDiChuyenKhongTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Lấy bê tông</label>
                                    <asp:TextBox ID="txtDMLayBeTongTaiTram" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMLayBeTongTaiTram" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, xả bê tông</label>
                                    <asp:TextBox ID="txtDMXaBeTongVeSinh" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMXaBeTongVeSinh" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Nổ máy quay thùng</label>
                                    <asp:TextBox ID="txtDMNoMayQuayThung" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMNoMayQuayThung" ValidChars=".," />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <h3 class="box-title">Nhật trình xe</h3>
                            </div>
                            <div class="box-body">

                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển có tải</label>
                                    <asp:TextBox ID="txtDiChuyenCoTaiBeTong" runat="server" class="form-control" onchange="DiChuyenCoTaiBeTong()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDiChuyenCoTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển không tải</label>
                                    <asp:TextBox ID="txtDiChuyenKhongTaiBeTong" runat="server" class="form-control" onchange="DiChuyenKhongTaiBeTong()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDiChuyenKhongTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Lấy bê tông</label>
                                    <asp:TextBox ID="txtLayBeTongTaiTram" runat="server" class="form-control" onchange="LayBeTongTaiTram()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtLayBeTongTaiTram" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, xả bê tông</label>
                                    <asp:TextBox ID="txtXaBeTongVeSinh" runat="server" class="form-control" onchange="XaBeTongVeSinh()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtXaBeTongVeSinh" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Nổ máy quay thùng</label>
                                    <asp:TextBox ID="txtNoMayQuayThung" runat="server" class="form-control" onchange="NoMayQuayThung()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtNoMayQuayThung" ValidChars=".," />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-info">
                                <h3 class="box-title">Nhiên liệu tiêu hao thực tế</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển có tải</label>
                                    <asp:TextBox ID="txtDauDiChuyenCoTaiBeTong" runat="server" class="form-control" disabled></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauDiChuyenCoTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển không tải</label>
                                    <asp:TextBox ID="txtDauDiChuyenKhongTaiBeTong" runat="server" class="form-control" disabled></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauDiChuyenKhongTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Lấy bê tông</label>
                                    <asp:TextBox ID="txtDauLayBeTongTaiTram" runat="server" class="form-control" disabled></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauLayBeTongTaiTram" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, xả bê tông</label>
                                    <asp:TextBox ID="txtDauXaBeTongVeSinh" runat="server" class="form-control" disabled></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauXaBeTongVeSinh" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Nổ máy quay thùng</label>
                                    <asp:TextBox ID="txtDauNoMayQuayThung" runat="server" class="form-control" disabled></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauNoMayQuayThung" ValidChars=".," />
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="box box-success">
                                <h3 class="box-title">Dầu cấp</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Dư đầu kỳ</label>
                                    <asp:TextBox ID="txtDuDauKy" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDuDauKy" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Dầu cấp</label>
                                    <asp:TextBox ID="txtDoDau" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDoDau" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Dư cuối kỳ</label>
                                    <asp:TextBox ID="txtDuCuoiKy" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDuCuoiKy" ValidChars=".," />
                                </div>

                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Khối lượng</label>
                                    <asp:TextBox ID="txtKhoiLuong" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtKhoiLuong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Số chuyến</label>
                                    <asp:TextBox ID="txtChuyenChay" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtChuyenChay" ValidChars=".," />
                                </div>
                                <%--<div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Bơm theo ca</label>
                                    <asp:TextBox ID="txtBomCa" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtBomCa" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Bơm theo khối</label>
                                    <asp:TextBox ID="txtBomKhoi" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtBomKhoi" ValidChars=".," />
                                </div>--%>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách xe</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"
                                    OnTextChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"
                                    OnTextChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" AutoPostBack="true"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" runat="server" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Biển số xe</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThietBiSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" OnRowDataBound="GridViewRowDataBound" OnRowCreated="GV_RowCreated" ShowFooter="true"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Xóa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Tên chi nhánh" />
                                    <asp:BoundField DataField="TenThietBi" HeaderText="Biển số xe" />
                                    <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />

                                    <asp:BoundField DataField="DMDiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMDiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMLayBeTongTaiTram" HeaderText="Lấy bê tông tại trạm" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMXaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMNoMayQuayThung" HeaderText="Nổ máy, quay thùng" ItemStyle-HorizontalAlign="Right" />

                                    <asp:BoundField DataField="DiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="LayBeTongTaiTram" HeaderText="Lấy bê tông tại trạm" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="XaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="NoMayQuayThung" HeaderText="Nổ máy, quay thùng" ItemStyle-HorizontalAlign="Right" />

                                    <asp:BoundField DataField="DauDiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DauDiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DauLayBeTongTaiTram" HeaderText="Lấy bê tông tại trạm" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DauXaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DauNoMayQuayThung" HeaderText="Nổ máy, quay thùng" ItemStyle-HorizontalAlign="Right" />

                                    <asp:BoundField DataField="DuDauKy" HeaderText="Dư đầu kỳ" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DoDau" HeaderText="Cấp dầu" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DuCuoiKy" HeaderText="Dư cuối kỳ" ItemStyle-HorizontalAlign="Right" />

                                    <asp:BoundField DataField="KhoiLuong" HeaderText="Khối lượng" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ChuyenChay" HeaderText="Chuyến" ItemStyle-HorizontalAlign="Right" />

                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>

            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function DiChuyenCoTaiBeTong() {
                    var txtDiChuyenCoTaiBeTong = parseFloat(getvalue(document.getElementById("<%=txtDiChuyenCoTaiBeTong.ClientID %>").value));
                    var txtDMDiChuyenCoTaiBeTong = parseFloat(getvalue(document.getElementById("<%=txtDMDiChuyenCoTaiBeTong.ClientID %>").value));
                    if (txtDiChuyenCoTaiBeTong != '0' && txtDMDiChuyenCoTaiBeTong != '0') {
                        document.getElementById("<%=txtDauDiChuyenCoTaiBeTong.ClientID %>").value = (txtDiChuyenCoTaiBeTong * txtDMDiChuyenCoTaiBeTong);
                    }
                    else {
                        document.getElementById("<%=txtDauDiChuyenCoTaiBeTong.ClientID %>").value = '0';
                    }
                }
                function DiChuyenKhongTaiBeTong() {
                    var txtDiChuyenKhongTaiBeTong = parseFloat(getvalue(document.getElementById("<%=txtDiChuyenKhongTaiBeTong.ClientID %>").value));
                    var txtDMDiChuyenKhongTaiBeTong = parseFloat(getvalue(document.getElementById("<%=txtDMDiChuyenKhongTaiBeTong.ClientID %>").value));
                    if (txtDiChuyenKhongTaiBeTong != '0' && txtDMDiChuyenKhongTaiBeTong != '0') {
                        document.getElementById("<%=txtDauDiChuyenKhongTaiBeTong.ClientID %>").value = (txtDiChuyenKhongTaiBeTong * txtDMDiChuyenKhongTaiBeTong);
                    }
                    else {
                        document.getElementById("<%=txtDauDiChuyenKhongTaiBeTong.ClientID %>").value = '0';
                    }
                }
                function LayBeTongTaiTram() {
                    var txtLayBeTongTaiTram = parseFloat(getvalue(document.getElementById("<%=txtLayBeTongTaiTram.ClientID %>").value));
                    var txtDMLayBeTongTaiTram = parseFloat(getvalue(document.getElementById("<%=txtDMLayBeTongTaiTram.ClientID %>").value));
                    if (txtLayBeTongTaiTram != '0' && txtDMLayBeTongTaiTram != '0') {
                        document.getElementById("<%=txtDauLayBeTongTaiTram.ClientID %>").value = (txtLayBeTongTaiTram * txtDMLayBeTongTaiTram);
                    }
                    else {
                        document.getElementById("<%=txtDauLayBeTongTaiTram.ClientID %>").value = '0';
                    }
                }
                function XaBeTongVeSinh() {
                    var txtXaBeTongVeSinh = parseFloat(getvalue(document.getElementById("<%=txtXaBeTongVeSinh.ClientID %>").value));
                    var txtDMXaBeTongVeSinh = parseFloat(getvalue(document.getElementById("<%=txtDMXaBeTongVeSinh.ClientID %>").value));
                    if (txtXaBeTongVeSinh != '0' && txtDMXaBeTongVeSinh != '0') {
                        document.getElementById("<%=txtDauXaBeTongVeSinh.ClientID %>").value = (txtXaBeTongVeSinh * txtDMXaBeTongVeSinh);
                    }
                    else {
                        document.getElementById("<%=txtDauXaBeTongVeSinh.ClientID %>").value = '0';
                    }
                }
                function NoMayQuayThung() {
                    var txtNoMayQuayThung = parseFloat(getvalue(document.getElementById("<%=txtNoMayQuayThung.ClientID %>").value));
                    var txtDMNoMayQuayThung = parseFloat(getvalue(document.getElementById("<%=txtDMNoMayQuayThung.ClientID %>").value));
                    if (txtNoMayQuayThung != '0' && txtDMNoMayQuayThung != '0') {
                        document.getElementById("<%=txtDauNoMayQuayThung.ClientID %>").value = (txtNoMayQuayThung * txtDMNoMayQuayThung);
                    }
                    else {
                        document.getElementById("<%=txtDauNoMayQuayThung.ClientID %>").value = '0';
                    }
                }

            </script>

            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdNhomThietBi" runat="server" Value="3EB17A86-BCA4-4852-B1F4-01B1CABE106D" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />

                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Tên chi nhánh" />
                                <asp:BoundField DataField="TenThietBi" HeaderText="Biển số xe" />
                                <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />

                                <asp:BoundField DataField="DMDiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMDiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMLayBeTongTaiTram" HeaderText="Lấy bê tông tại trạm" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMXaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMNoMayQuayThung" HeaderText="Nổ máy, quay thùng" ItemStyle-HorizontalAlign="Right" />

                                <asp:BoundField DataField="DiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="LayBeTongTaiTram" HeaderText="Lấy bê tông tại trạm" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="XaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="NoMayQuayThung" HeaderText="Nổ máy, quay thùng" ItemStyle-HorizontalAlign="Right" />

                                <asp:BoundField DataField="DauDiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DauDiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DauLayBeTongTaiTram" HeaderText="Lấy bê tông tại trạm" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DauXaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DauNoMayQuayThung" HeaderText="Nổ máy, quay thùng" ItemStyle-HorizontalAlign="Right" />

                                <asp:BoundField DataField="DuDauKy" HeaderText="Dư đầu kỳ" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DoDau" HeaderText="Cấp dầu" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DuCuoiKy" HeaderText="Dư cuối kỳ" ItemStyle-HorizontalAlign="Right" />

                                <asp:BoundField DataField="KhoiLuong" HeaderText="Khối lượng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ChuyenChay" HeaderText="Chuyến" ItemStyle-HorizontalAlign="Right" />

                                <asp:BoundField DataField="NguoiDuyet" HeaderText="Người duyệt" />
                                <asp:BoundField DataField="NguoiXoa" HeaderText="Người xóa" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
