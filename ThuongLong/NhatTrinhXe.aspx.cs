﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class NhatTrinhXe : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        ThietBiDataContext thietbi = new ThietBiDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmNhatTrinhXe";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmNhatTrinhXe", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        NhatTrinhXeBen.Visible = false;
                        NhatTrinhXeDauKeo.Visible = false;
                        NhatTrinhXeXucLat.Visible = false;
                        NhatTrinhXeXucDao.Visible = false;
                        NhatTrinhXeBom.Visible = false;
                        NhatTrinhXeBeTong.Visible = false;
                        NhatTrinhXeCau.Visible = false;
                    }
                }
            }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }

        protected void dlLoaiXe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlLoaiXe.SelectedValue == "")
            {
                NhatTrinhXeBen.Visible = false;
                NhatTrinhXeDauKeo.Visible = false;
                NhatTrinhXeXucLat.Visible = false;
                NhatTrinhXeXucDao.Visible = false;
                NhatTrinhXeBom.Visible = false;
                NhatTrinhXeBeTong.Visible = false;
                NhatTrinhXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "1")
            {
                NhatTrinhXeBen.Visible = true;
                NhatTrinhXeDauKeo.Visible = false;
                NhatTrinhXeXucLat.Visible = false;
                NhatTrinhXeXucDao.Visible = false;
                NhatTrinhXeBom.Visible = false;
                NhatTrinhXeBeTong.Visible = false;
                NhatTrinhXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "2")
            {
                NhatTrinhXeBen.Visible = false;
                NhatTrinhXeDauKeo.Visible = true;
                NhatTrinhXeXucLat.Visible = false;
                NhatTrinhXeXucDao.Visible = false;
                NhatTrinhXeBom.Visible = false;
                NhatTrinhXeBeTong.Visible = false;
                NhatTrinhXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "3")
            {
                NhatTrinhXeBen.Visible = false;
                NhatTrinhXeDauKeo.Visible = false;
                NhatTrinhXeXucLat.Visible = true;
                NhatTrinhXeXucDao.Visible = false;
                NhatTrinhXeBom.Visible = false;
                NhatTrinhXeBeTong.Visible = false;
                NhatTrinhXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "4")
            {
                NhatTrinhXeBen.Visible = false;
                NhatTrinhXeDauKeo.Visible = false;
                NhatTrinhXeXucLat.Visible = false;
                NhatTrinhXeXucDao.Visible = true;
                NhatTrinhXeBom.Visible = false;
                NhatTrinhXeBeTong.Visible = false;
                NhatTrinhXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "5")
            {
                NhatTrinhXeBen.Visible = false;
                NhatTrinhXeDauKeo.Visible = false;
                NhatTrinhXeXucLat.Visible = false;
                NhatTrinhXeXucDao.Visible = false;
                NhatTrinhXeBom.Visible = true;
                NhatTrinhXeBeTong.Visible = false;
                NhatTrinhXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "6")
            {
                NhatTrinhXeBen.Visible = false;
                NhatTrinhXeDauKeo.Visible = false;
                NhatTrinhXeXucLat.Visible = false;
                NhatTrinhXeXucDao.Visible = false;
                NhatTrinhXeBom.Visible = false;
                NhatTrinhXeBeTong.Visible = true;
                NhatTrinhXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "7")
            {
                NhatTrinhXeBen.Visible = false;
                NhatTrinhXeDauKeo.Visible = false;
                NhatTrinhXeXucLat.Visible = false;
                NhatTrinhXeXucDao.Visible = false;
                NhatTrinhXeBom.Visible = false;
                NhatTrinhXeBeTong.Visible = false;
                NhatTrinhXeCau.Visible = true;
            }
        }
    }
}