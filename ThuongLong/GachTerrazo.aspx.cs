﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class GachTerrazo : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmSanXuatGach";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmGachTerrazo", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        txtNgayThang.Text = dateTimenow.ToString("dd/MM/yyyy");
                        LoadChiNhanh();
                        LoadLoaiGach();
                        LoadLoaiVatLieu();
                        dlChiNhanh_SelectedIndexChanged(sender, e);
                        //LoadChiNhanhSearch();
                    }
                }
            }
        }
        protected void LoadLoaiGach()
        {
            dlLoaiGach.Items.Clear();

            var query = (from p in db.tblLoaiVatLieus
                         where p.TrangThai == 2
                         && p.IDNhomVatLieu == new Guid("7D25A3D1-BE90-4F1D-A0CB-8A518AC09B55")
                         select new
                         {
                             p.ID,
                             Ten = p.TenLoaiVatLieu
                         }).OrderBy(p => p.Ten);

            dlLoaiGach.DataSource = query;
            dlLoaiGach.DataBind();
            dlLoaiGach.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadLoaiVatLieu()
        {
            dlMauXi.Items.Clear();
            dlMauDo.Items.Clear();
            dlBotDa.Items.Clear();
            dlDaDen2Ly.Items.Clear();
            dlDaTrang2Ly.Items.Clear();
            dlDaTrang4Ly.Items.Clear();
            dlXiMangPCB401.Items.Clear();
            dlXiMangPCB402.Items.Clear();
            dlMatDa.Items.Clear();
            dlCatSongDa.Items.Clear();

            var query = (from p in db.tblLoaiVatLieus
                         where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenLoaiVatLieu
                         }).OrderBy(p => p.Ten);

            dlMauXi.DataSource = query;
            dlMauXi.DataBind();
            dlMauXi.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlMauDo.DataSource = query;
            dlMauDo.DataBind();
            dlMauDo.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlBotDa.DataSource = query;
            dlBotDa.DataBind();
            dlBotDa.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlDaDen2Ly.DataSource = query;
            dlDaDen2Ly.DataBind();
            dlDaDen2Ly.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlDaTrang2Ly.DataSource = query;
            dlDaTrang2Ly.DataBind();
            dlDaTrang2Ly.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlDaTrang4Ly.DataSource = query;
            dlDaTrang4Ly.DataBind();
            dlDaTrang4Ly.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlXiMangPCB401.DataSource = query;
            dlXiMangPCB401.DataBind();
            dlXiMangPCB401.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlXiMangPCB402.DataSource = query;
            dlXiMangPCB402.DataBind();
            dlXiMangPCB402.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlMatDa.DataSource = query;
            dlMatDa.DataBind();
            dlMatDa.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlCatSongDa.DataSource = query;
            dlCatSongDa.DataBind();
            dlCatSongDa.Items.Insert(0, new ListItem("--Chọn--", ""));

        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            dlChiNhanhChot.DataSource = query;
            dlChiNhanhChot.DataBind();
            dlChiNhanhChot.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        //protected void LoadChiNhanhSearch()
        //{
        //    var query = db.sp_LoadChiNhanhDaiLy();
        //    dlChiNhanhSearch.DataSource = query;
        //    dlChiNhanhSearch.DataBind();
        //}
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            double haohut = txtSoLuongHaoHut.Text.Trim() == "" ? 0 : double.Parse(txtSoLuongHaoHut.Text.Trim());
            if (dlChiNhanh.SelectedValue != "" && dlLoaiGach.SelectedValue != "")
            {
                var query = (from p in db.tblCaiDatVatLieuGachTerrazos
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiGach)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    dlMauXi.SelectedValue = query.MauXi.ToString();
                    dlMauDo.SelectedValue = query.MauDo.ToString();
                    dlBotDa.SelectedValue = query.BotDa.ToString();
                    dlDaDen2Ly.SelectedValue = query.DaDen2ly.ToString();
                    dlDaTrang2Ly.SelectedValue = query.DaTrang2Ly.ToString();
                    dlDaTrang4Ly.SelectedValue = query.DaTrang4Ly.ToString();
                    dlXiMangPCB401.SelectedValue = query.XiMangPCB401.ToString();
                    dlXiMangPCB402.SelectedValue = query.XiMangPCB402.ToString();
                    dlMatDa.SelectedValue = query.MatDa.ToString();
                    dlCatSongDa.SelectedValue = query.CatSongDa.ToString();

                    txtMauXi.Text = query.DMMauXi.ToString();
                    txtMauDo.Text = query.DMMauDo.ToString();
                    txtBotDa.Text = query.DMBotDa.ToString();
                    txtDaDen2Ly.Text = query.DMDaDen2ly.ToString();
                    txtDaTrang2Ly.Text = query.DMDaTrang2Ly.ToString();
                    txtDaTrang4Ly.Text = query.DMDaTrang4Ly.ToString();
                    txtXiMangPCB401.Text = query.DMXiMangPCB401.ToString();
                    txtXiMangPCB402.Text = query.DMXiMangPCB402.ToString();
                    txtMatDa.Text = query.DMMatDa.ToString();
                    txtCatSongDa.Text = query.DMCatSongDa.ToString();

                    if (txtSoLuong.Text.Trim() != "" && double.Parse(txtSoLuong.Text.Trim()) > 0)
                    {
                        txtTLMauXi.Text = "";
                        txtTLMauDo.Text = "";
                        txtTLBotDa.Text = "";
                        txtTLDaDen2Ly.Text = "";
                        txtTLDaTrang2Ly.Text = "";
                        txtTLDaTrang4Ly.Text = "";
                        txtTLXiMangPCB401.Text = "";
                        txtTLXiMangPCB402.Text = "";
                        txtTLMatDa.Text = "";
                        txtTLCatSongDa.Text = "";

                        txtTLMauXi.Text = string.Format("{0:N0}", double.Parse(txtMauXi.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLMauDo.Text = string.Format("{0:N0}", double.Parse(txtMauDo.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLBotDa.Text = string.Format("{0:N0}", double.Parse(txtBotDa.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLDaDen2Ly.Text = string.Format("{0:N0}", double.Parse(txtDaDen2Ly.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLDaTrang2Ly.Text = string.Format("{0:N0}", double.Parse(txtDaTrang2Ly.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLDaTrang4Ly.Text = string.Format("{0:N0}", double.Parse(txtDaTrang4Ly.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLXiMangPCB401.Text = string.Format("{0:N0}", double.Parse(txtXiMangPCB401.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLXiMangPCB402.Text = string.Format("{0:N0}", double.Parse(txtXiMangPCB402.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLMatDa.Text = string.Format("{0:N0}", double.Parse(txtMatDa.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                        txtTLCatSongDa.Text = string.Format("{0:N0}", double.Parse(txtCatSongDa.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                    }
                    else
                    {
                        txtTLMauXi.Text = "0";
                        txtTLMauDo.Text = "0";
                        txtTLBotDa.Text = "0";
                        txtTLDaDen2Ly.Text = "0";
                        txtTLDaTrang2Ly.Text = "0";
                        txtTLDaTrang4Ly.Text = "0";
                        txtTLXiMangPCB401.Text = "0";
                        txtTLXiMangPCB402.Text = "0";
                        txtTLMatDa.Text = "0";
                        txtTLCatSongDa.Text = "0";
                    }
                }
                else
                {
                    txtMauXi.Text = "0";
                    txtMauDo.Text = "0";
                    txtBotDa.Text = "0";
                    txtDaDen2Ly.Text = "0";
                    txtDaTrang2Ly.Text = "0";
                    txtDaTrang4Ly.Text = "0";
                    txtXiMangPCB401.Text = "0";
                    txtXiMangPCB402.Text = "0";
                    txtMatDa.Text = "0";
                    txtCatSongDa.Text = "0";

                    txtTLMauXi.Text = "0";
                    txtTLMauDo.Text = "0";
                    txtTLBotDa.Text = "0";
                    txtTLDaDen2Ly.Text = "0";
                    txtTLDaTrang2Ly.Text = "0";
                    txtTLDaTrang4Ly.Text = "0";
                    txtTLXiMangPCB401.Text = "0";
                    txtTLXiMangPCB402.Text = "0";
                    txtTLMatDa.Text = "0";
                    txtTLCatSongDa.Text = "0";

                    Warning("Bạn chưa thiết lập định mức cho loại gạch vừa chọn");
                }
            }
            else
            {
                txtMauXi.Text = "0";
                txtMauDo.Text = "0";
                txtBotDa.Text = "0";
                txtDaDen2Ly.Text = "0";
                txtDaTrang2Ly.Text = "0";
                txtDaTrang4Ly.Text = "0";
                txtXiMangPCB401.Text = "0";
                txtXiMangPCB402.Text = "0";
                txtMatDa.Text = "0";
                txtCatSongDa.Text = "0";

                txtTLMauXi.Text = "0";
                txtTLMauDo.Text = "0";
                txtTLBotDa.Text = "0";
                txtTLDaDen2Ly.Text = "0";
                txtTLDaTrang2Ly.Text = "0";
                txtTLDaTrang4Ly.Text = "0";
                txtTLXiMangPCB401.Text = "0";
                txtTLXiMangPCB402.Text = "0";
                txtTLMatDa.Text = "0";
                txtTLCatSongDa.Text = "0";
            }
        }
        protected void txtSoLuong_TextChanged(object sender, EventArgs e)
        {
            double haohut = txtSoLuongHaoHut.Text.Trim() == "" ? 0 : double.Parse(txtSoLuongHaoHut.Text.Trim());
            if (txtMauXi.Text.Trim() != "" && dlChiNhanh.SelectedValue != "" && dlLoaiGach.SelectedValue != "" && txtSoLuong.Text.Trim() != "" && double.Parse(txtSoLuong.Text.Trim()) > 0)
            {
                txtTLMauXi.Text = "";
                txtTLMauDo.Text = "";
                txtTLBotDa.Text = "";
                txtTLDaDen2Ly.Text = "";
                txtTLDaTrang2Ly.Text = "";
                txtTLDaTrang4Ly.Text = "";
                txtTLXiMangPCB401.Text = "";
                txtTLXiMangPCB402.Text = "";
                txtTLMatDa.Text = "";
                txtTLCatSongDa.Text = "";

                txtTLMauXi.Text = string.Format("{0:N0}", double.Parse(txtMauXi.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLMauDo.Text = string.Format("{0:N0}", double.Parse(txtMauDo.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLBotDa.Text = string.Format("{0:N0}", double.Parse(txtBotDa.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLDaDen2Ly.Text = string.Format("{0:N0}", double.Parse(txtDaDen2Ly.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLDaTrang2Ly.Text = string.Format("{0:N0}", double.Parse(txtDaTrang2Ly.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLDaTrang4Ly.Text = string.Format("{0:N0}", double.Parse(txtDaTrang4Ly.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLXiMangPCB401.Text = string.Format("{0:N0}", double.Parse(txtXiMangPCB401.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLXiMangPCB402.Text = string.Format("{0:N0}", double.Parse(txtXiMangPCB402.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLMatDa.Text = string.Format("{0:N0}", double.Parse(txtMatDa.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
                txtTLCatSongDa.Text = string.Format("{0:N0}", double.Parse(txtCatSongDa.Text.Trim()) * (double.Parse(txtSoLuong.Text.Trim()) + haohut));
            }
            else
            {
                txtTLMauXi.Text = "0";
                txtTLMauDo.Text = "0";
                txtTLBotDa.Text = "0";
                txtTLDaDen2Ly.Text = "0";
                txtTLDaTrang2Ly.Text = "0";
                txtTLDaTrang4Ly.Text = "0";
                txtTLXiMangPCB401.Text = "0";
                txtTLXiMangPCB402.Text = "0";
                txtTLMatDa.Text = "0";
                txtTLCatSongDa.Text = "0";
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }

            //if (txtTLMauXi.Text == "")
            //{
            //    s += " - Nhập màu xi<br />";
            //}

            //if (txtTLMauDo.Text == "")
            //{
            //    s += " - Nhập màu đỏ<br />";
            //}

            //if (txtBotDa.Text == "")
            //{
            //    s += " - Nhập bột đá<br />";
            //}

            //if (txtTLDaDen2Ly.Text == "")
            //{
            //    s += " - Nhập đá đen 2 ly<br />";
            //}

            //if (txtTLDaTrang2Ly.Text == "")
            //{
            //    s += " - Nhập đá trắng 2 ly<br />";
            //}

            //if (txtTLDaTrang4Ly.Text == "")
            //{
            //    s += " - Nhập đá trắng 4 ly<br />";
            //}

            //if (txtTLXiMangPCB401.Text == "")
            //{
            //    s += " - Nhập xi măng PCB40 1<br />";
            //}

            //if (txtTLXiMangPCB402.Text == "")
            //{
            //    s += " - Nhập xi măng PCB40 2<br />";
            //}

            //if (txtTLMatDa.Text == "")
            //{
            //    s += " - Nhập mạt đá<br />";
            //}

            //if (txtTLCatSongDa.Text == "")
            //{
            //    s += " - Nhập cát sông đà<br />";
            //}
            if (txtSoLuong.Text == "")
            {
                s += " - Nhập số lượng đạt<br />";
            }
            if (txtSoLuongHaoHut.Text == "")
            {
                s += " - Nhập số lượng hao hụt<br />";
            }
            if (s == "")
            {
                vatlieu.sp_GachTerrazo_CheckThem(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlLoaiGach), int.Parse(dlThoiTiet.SelectedValue), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }
            //if (txtTLMauXi.Text == "")
            //{
            //    s += " - Nhập màu xi<br />";
            //}

            //if (txtTLMauDo.Text == "")
            //{
            //    s += " - Nhập màu đỏ<br />";
            //}

            //if (txtBotDa.Text == "")
            //{
            //    s += " - Nhập bột đá<br />";
            //}

            //if (txtTLDaDen2Ly.Text == "")
            //{
            //    s += " - Nhập đá đen 2 ly<br />";
            //}

            //if (txtTLDaTrang2Ly.Text == "")
            //{
            //    s += " - Nhập đá trắng 2 ly<br />";
            //}

            //if (txtTLDaTrang4Ly.Text == "")
            //{
            //    s += " - Nhập đá trắng 4 ly<br />";
            //}

            //if (txtTLXiMangPCB401.Text == "")
            //{
            //    s += " - Nhập xi măng PCB40 1<br />";
            //}

            //if (txtTLXiMangPCB402.Text == "")
            //{
            //    s += " - Nhập xi măng PCB40 2<br />";
            //}

            //if (txtTLMatDa.Text == "")
            //{
            //    s += " - Nhập mạt đá<br />";
            //}

            //if (txtTLCatSongDa.Text == "")
            //{
            //    s += " - Nhập cát sông đà<br />";
            //}
            if (txtSoLuong.Text == "")
            {
                s += " - Nhập số lượng đạt<br />";
            }
            if (txtSoLuongHaoHut.Text == "")
            {
                s += " - Nhập số lượng hao hụt<br />";
            }
            if (s == "")
            {
                vatlieu.sp_GachTerrazo_CheckSua(new Guid(hdID.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        Guid ID = Guid.NewGuid();
                        //if (FileUpload1.HasFile)
                        //    UploadFile(1, ID.ToString(), ref filename);

                        var GachTerrazo = new tblGachTerrazo()
                        {
                            ID = ID,
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                            LoaiGach = GetDrop(dlLoaiGach),
                            TenLoaiGach = dlLoaiGach.SelectedItem.Text.ToString(),
                            GhiChu = txtGhiChu.Text.Trim(),
                            MauXi = GetDrop(dlMauXi),
                            MauDo = GetDrop(dlMauDo),
                            BotDa = GetDrop(dlBotDa),
                            DaDen2ly = GetDrop(dlDaDen2Ly),
                            DaTrang2Ly = GetDrop(dlDaTrang2Ly),
                            DaTrang4Ly = GetDrop(dlDaTrang4Ly),
                            XiMangPCB401 = GetDrop(dlXiMangPCB401),
                            XiMangPCB402 = GetDrop(dlXiMangPCB402),
                            MatDa = GetDrop(dlMatDa),
                            CatSongDa = GetDrop(dlCatSongDa),

                            TenMauXi = dlMauXi.SelectedItem.Text.ToString(),
                            TenMauDo = dlMauDo.SelectedItem.Text.ToString(),
                            TenBotDa = dlBotDa.SelectedItem.Text.ToString(),
                            TenDaDen2Ly = dlDaDen2Ly.SelectedItem.Text.ToString(),
                            TenDaTrang2Ly = dlDaTrang2Ly.SelectedItem.Text.ToString(),
                            TenDaTrang4Ly = dlDaTrang4Ly.SelectedItem.Text.ToString(),
                            TenXiMangPCB401 = dlXiMangPCB401.SelectedItem.Text.ToString(),
                            TenXiMangPCB402 = dlXiMangPCB402.SelectedItem.Text.ToString(),
                            TenMatDa = dlMatDa.SelectedItem.Text.ToString(),
                            TenCatSongDa = dlCatSongDa.SelectedItem.Text.ToString(),

                            ThoiTiet = int.Parse(dlThoiTiet.SelectedValue),

                            SoLuong = txtSoLuong.Text == "" ? 0 : int.Parse(txtSoLuong.Text.Trim()),
                            SoLuongHaoHut = txtSoLuongHaoHut.Text == "" ? 0 : int.Parse(txtSoLuongHaoHut.Text.Trim()),

                            DMMauXi = txtMauXi.Text == "" ? 0 : double.Parse(txtMauXi.Text.Trim()),
                            DMMauDo = txtMauDo.Text == "" ? 0 : double.Parse(txtMauDo.Text.Trim()),
                            DMBotDa = txtBotDa.Text == "" ? 0 : double.Parse(txtBotDa.Text.Trim()),
                            DMDaDen2ly = txtDaDen2Ly.Text == "" ? 0 : double.Parse(txtDaDen2Ly.Text.Trim()),
                            DMDaTrang2Ly = txtDaTrang2Ly.Text == "" ? 0 : double.Parse(txtDaTrang2Ly.Text.Trim()),
                            DMDaTrang4Ly = txtDaTrang4Ly.Text == "" ? 0 : double.Parse(txtDaTrang4Ly.Text.Trim()),
                            DMXiMangPCB401 = txtXiMangPCB401.Text == "" ? 0 : double.Parse(txtXiMangPCB401.Text.Trim()),
                            DMXiMangPCB402 = txtXiMangPCB402.Text == "" ? 0 : double.Parse(txtXiMangPCB402.Text.Trim()),
                            DMMatDa = txtMatDa.Text == "" ? 0 : double.Parse(txtMatDa.Text.Trim()),
                            DMCatSongDa = txtCatSongDa.Text == "" ? 0 : double.Parse(txtCatSongDa.Text.Trim()),

                            TLMauXi = txtTLMauXi.Text == "" ? 0 : double.Parse(txtTLMauXi.Text.Trim()),
                            TLMauDo = txtTLMauDo.Text == "" ? 0 : double.Parse(txtTLMauDo.Text.Trim()),
                            TLBotDa = txtTLBotDa.Text == "" ? 0 : double.Parse(txtTLBotDa.Text.Trim()),
                            TLDaDen2ly = txtTLDaDen2Ly.Text == "" ? 0 : double.Parse(txtTLDaDen2Ly.Text.Trim()),
                            TLDaTrang2Ly = txtTLDaTrang2Ly.Text == "" ? 0 : double.Parse(txtTLDaTrang2Ly.Text.Trim()),
                            TLDaTrang4Ly = txtTLDaTrang4Ly.Text == "" ? 0 : double.Parse(txtTLDaTrang4Ly.Text.Trim()),
                            TLXiMangPCB401 = txtTLXiMangPCB401.Text == "" ? 0 : double.Parse(txtTLXiMangPCB401.Text.Trim()),
                            TLXiMangPCB402 = txtTLXiMangPCB402.Text == "" ? 0 : double.Parse(txtTLXiMangPCB402.Text.Trim()),
                            TLMatDa = txtTLMatDa.Text == "" ? 0 : double.Parse(txtTLMatDa.Text.Trim()),
                            TLCatSongDa = txtTLCatSongDa.Text == "" ? 0 : double.Parse(txtTLCatSongDa.Text.Trim()),

                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblGachTerrazos.InsertOnSubmit(GachTerrazo);
                        db.SubmitChanges();

                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        GstGetMess("Lưu thành công.", "");

                        //GstGetMess("Lưu thành công.", "");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblGachTerrazos
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.LoaiGach = GetDrop(dlLoaiGach);
                            query.TenLoaiGach = dlLoaiGach.SelectedItem.Text.ToString();
                            query.GhiChu = txtGhiChu.Text.Trim();

                            query.ThoiTiet = int.Parse(dlThoiTiet.SelectedValue);

                            query.SoLuong = txtSoLuong.Text == "" ? 0 : int.Parse(txtSoLuong.Text.Trim());
                            query.SoLuongHaoHut = txtSoLuongHaoHut.Text == "" ? 0 : int.Parse(txtSoLuongHaoHut.Text.Trim());

                            query.DMMauXi = txtMauXi.Text == "" ? 0 : double.Parse(txtMauXi.Text.Trim());
                            query.DMMauDo = txtMauDo.Text == "" ? 0 : double.Parse(txtMauDo.Text.Trim());
                            query.DMBotDa = txtBotDa.Text == "" ? 0 : double.Parse(txtBotDa.Text.Trim());
                            query.DMDaDen2ly = txtDaDen2Ly.Text == "" ? 0 : double.Parse(txtDaDen2Ly.Text.Trim());
                            query.DMDaTrang2Ly = txtDaTrang2Ly.Text == "" ? 0 : double.Parse(txtDaTrang2Ly.Text.Trim());
                            query.DMDaTrang4Ly = txtDaTrang4Ly.Text == "" ? 0 : double.Parse(txtDaTrang4Ly.Text.Trim());
                            query.DMXiMangPCB401 = txtXiMangPCB401.Text == "" ? 0 : double.Parse(txtXiMangPCB401.Text.Trim());
                            query.DMXiMangPCB402 = txtXiMangPCB402.Text == "" ? 0 : double.Parse(txtXiMangPCB402.Text.Trim());
                            query.DMMatDa = txtMatDa.Text == "" ? 0 : double.Parse(txtMatDa.Text.Trim());
                            query.DMCatSongDa = txtCatSongDa.Text == "" ? 0 : double.Parse(txtCatSongDa.Text.Trim());

                            query.TLMauXi = txtTLMauXi.Text == "" ? 0 : double.Parse(txtTLMauXi.Text.Trim());
                            query.TLMauDo = txtTLMauDo.Text == "" ? 0 : double.Parse(txtTLMauDo.Text.Trim());
                            query.TLBotDa = txtTLBotDa.Text == "" ? 0 : double.Parse(txtTLBotDa.Text.Trim());
                            query.TLDaDen2ly = txtTLDaDen2Ly.Text == "" ? 0 : double.Parse(txtTLDaDen2Ly.Text.Trim());
                            query.TLDaTrang2Ly = txtTLDaTrang2Ly.Text == "" ? 0 : double.Parse(txtTLDaTrang2Ly.Text.Trim());
                            query.TLDaTrang4Ly = txtTLDaTrang4Ly.Text == "" ? 0 : double.Parse(txtTLDaTrang4Ly.Text.Trim());
                            query.TLXiMangPCB401 = txtTLXiMangPCB401.Text == "" ? 0 : double.Parse(txtTLXiMangPCB401.Text.Trim());
                            query.TLXiMangPCB402 = txtTLXiMangPCB402.Text == "" ? 0 : double.Parse(txtTLXiMangPCB402.Text.Trim());
                            query.TLMatDa = txtTLMatDa.Text == "" ? 0 : double.Parse(txtTLMatDa.Text.Trim());
                            query.TLCatSongDa = txtTLCatSongDa.Text == "" ? 0 : double.Parse(txtTLCatSongDa.Text.Trim());

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";

                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();

                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            GstGetMess("Sửa thành công", "");
                        }
                        else
                        {
                            GstGetMess("Thông tin gạch Terrazo đã bị xóa.", "");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        void UploadFile(int loai, string ID, ref string filename)
        {
            //if (FileUpload1.HasFile)
            //{
            //    if (loai == 1)
            //    {
            //        string extision = FileUpload1.FileName.Trim();
            //        string time = DateTime.Now.ToString("yyyyMMddHHmmss");
            //        filename = ID.ToUpper() + "-" + time + extision.Substring(extision.LastIndexOf('.'));
            //        FileUpload1.SaveAs(Server.MapPath("~/ImageNhapKho/") + filename);
            //    }
            //}
        }
        protected void btnCan_Click(object sender, EventArgs e)
        {

        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlChiNhanh_SelectedIndexChanged(sender, e);

            txtSoLuong.Text = "";
            txtSoLuongHaoHut.Text = "";
            txtTLMauXi.Text = "";
            txtTLMauDo.Text = "";
            txtTLBotDa.Text = "";
            txtTLDaDen2Ly.Text = "";
            txtTLDaTrang2Ly.Text = "";
            txtTLDaTrang4Ly.Text = "";
            txtTLXiMangPCB401.Text = "";
            txtTLXiMangPCB402.Text = "";
            txtTLMatDa.Text = "";
            txtTLCatSongDa.Text = "";

            hdID.Value = "";
            dlChiNhanh.Enabled = true;
            dlThoiTiet.Enabled = true;
            dlLoaiGach.Enabled = true;
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblGachTerrazos
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (query.TrangThaiChot != null)
                    GstGetMess("Thời gian này đã chốt nhập gạch Terrazo, không thể sửa xóa.", "");
                else if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        vatlieu.sp_GachTerrazo_CheckSuaGV(query.NgayThang, query.IDChiNhanh, ref thongbao);
                        if (thongbao != "")
                            GstGetMess(thongbao, "");
                        else
                        {
                            txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            dlLoaiGach.SelectedValue = query.LoaiGach.ToString();

                            dlChiNhanh_SelectedIndexChanged(sender, e);

                            dlThoiTiet.SelectedValue = query.ThoiTiet.ToString();
                            txtTLMauXi.Text = query.TLMauXi.ToString();
                            txtTLMauDo.Text = query.TLMauDo.ToString();
                            txtTLBotDa.Text = query.TLBotDa.ToString();
                            txtTLDaDen2Ly.Text = query.TLDaDen2ly.ToString();
                            txtTLDaTrang2Ly.Text = query.TLDaTrang2Ly.ToString();
                            txtTLDaTrang4Ly.Text = query.TLDaTrang4Ly.ToString();
                            txtTLXiMangPCB401.Text = query.TLXiMangPCB401.ToString();
                            txtTLXiMangPCB402.Text = query.TLXiMangPCB402.ToString();
                            txtTLMatDa.Text = query.TLMatDa.ToString();
                            txtTLCatSongDa.Text = query.TLCatSongDa.ToString();
                            txtSoLuong.Text = query.SoLuong.ToString();
                            txtSoLuongHaoHut.Text = query.SoLuongHaoHut.ToString();
                            dlChiNhanh.Enabled = false;
                            dlThoiTiet.Enabled = false;
                            dlLoaiGach.Enabled = false;
                            hdID.Value = id;

                            //btnSave.Text = "Cập nhật";
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            GstGetMess(thongbao, "");
                        }
                    }
                    else
                    {
                        if (query.TrangThaiChot != null)
                        {
                            Warning("Thời gian này đã chốt nhập kho vật liệu");
                        }
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblGachTerrazo_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblGachTerrazos.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_GachTerrazo_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                GstGetMess("Thông tin gạch Terrazo đã bị xóa.", "");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[18].Visible = false;
                //e.Row.Cells[6].Visible = false;
                //e.Row.Cells[7].Visible = false;
                //e.Row.Cells[8].Visible = false;
                //e.Row.Cells[9].Visible = false;
                //e.Row.Cells[10].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thời tiết",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Số viên gạch",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Orange,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Lớp mặt(kg)",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 7,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Lớp thân",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.DarkCyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ghi chú",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void GVChot_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[17].Visible = false;

                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Số viên gạch",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Orange,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Lớp mặt(kg)",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 8,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Lớp thân",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.DarkCyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ghi chú",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {

            List<sp_GachTerrazo_SearchResult> query = vatlieu.sp_GachTerrazo_Search(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), page).ToList();

            GV.DataSource = query;
            GV.DataBind();

            if (GV.Rows.Count > 0)
            {
                if (query != null)
                {
                    var valuefooter = vatlieu.sp_GachTerrazo_GetFooter(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();

                    GV.FooterRow.Cells[0].ColumnSpan = 7;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", valuefooter.SoDong);
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", valuefooter.SoLuong);
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", valuefooter.SoLuongHaoHut);
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", valuefooter.TLMauXi);
                    GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", valuefooter.TLMauDo);
                    GV.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.TLBotDa);
                    GV.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.TLDaDen2ly);
                    GV.FooterRow.Cells[7].Text = string.Format("{0:N0}", valuefooter.TLDaTrang2Ly);
                    GV.FooterRow.Cells[8].Text = string.Format("{0:N0}", valuefooter.TLDaTrang4Ly);
                    GV.FooterRow.Cells[9].Text = string.Format("{0:N0}", valuefooter.TLXiMangPCB401);
                    GV.FooterRow.Cells[10].Text = string.Format("{0:N0}", valuefooter.TLXiMangPCB402);
                    GV.FooterRow.Cells[11].Text = string.Format("{0:N0}", valuefooter.TLMatDa);
                    GV.FooterRow.Cells[12].Text = string.Format("{0:N0}", valuefooter.TLCatSongDa);

                    GV.FooterRow.Cells[14].Visible = false;
                    GV.FooterRow.Cells[15].Visible = false;
                    GV.FooterRow.Cells[16].Visible = false;
                    GV.FooterRow.Cells[17].Visible = false;
                    GV.FooterRow.Cells[18].Visible = false;
                    GV.FooterRow.Cells[19].Visible = false;
                }
            }

        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string query = "SELECT a.NgayThang, g.TenChiNhanh, a.ThoiTiet, a.TLMauXi,a.TLMauDo,a.TLBotDa,a.TLDaDen2ly,a.TLDaTrang2Ly,a.TLDaTrang4Ly,a.TLXiMangPCB401," +
                "a.SoMeTron2,a.TLXiMangPCB402,a.TLMatDa,a.TLCatSongDa,a.SoLuong,a.SoVien400x400" +
                " FROM tblGachTerrazo AS a JOIN tblChiNhanh AS g ON a.IDChiNhanh = g.ID  " +
                "WHERE a.IDChiNhanh = '" + dlChiNhanhSearch.SelectedValue + "' AND a.NgayThang BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "' ";

            string sqlcuoi = " order by a.NgayThang";

            query = query + sqlcuoi;

            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "BÁO CÁO SẢN XUẤT GẠCH TERRAZO";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 7);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue("CHI NHÁNH " + dlChiNhanhSearch.SelectedItem.Text.ToString().ToUpper());

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 600;
                cra = new CellRangeAddress(2, 2, 0, 16);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO nhập gạch TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                }
                else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO nhập gạch NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(3, 3, 0, 16);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 16);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Chi nhánh",
                    "Ngày tháng",
                    "Lớp mặt(kg)",
                    "Lớp mặt(kg)",
                    "Lớp mặt(kg)",
                    "Lớp mặt(kg)",
                    "Lớp mặt(kg)",
                    "Lớp mặt(kg)",
                    "Lớp mặt(kg)",
                    "Lớp mặt(kg)",
                    "Lớp thân(kg)",
                    "Lớp thân(kg)",
                    "Lớp thân(kg)",
                    "Lớp thân(kg)",
                    "Số viên gạch",
                    "Số viên gạch"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Chi nhánh",
                    "Ngày tháng",
                    "Số mẻ trộn",
                    "Màu xi",
                    "Màu đỏ",
                    "Bột đá",
                    "Đá đen 2 ly",
                    "Đá trắng 2 ly",
                    "Đá trắng 4 ly",
                    "Xi măng PCB40",
                    "Số mẻ trộn",
                    "Xe măng PCB40",
                    "Mạt đá",
                    "Cát sông đà",
                    "Loại 300x300",
                    "Loại 400x400"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(5, 6, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(5, 6, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(5, 6, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(5, 5, 3, 10);
                sheet.AddMergedRegion(cra);

                cra = new CellRangeAddress(5, 5, 11, 14);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(5, 5, 15, 16);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(DateTime.Parse(dt.Rows[i]["NgayThang"].ToString()));
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(dt.Rows[i]["TenChiNhanh"].ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["ThoiTiet"].ToString()));
                    cell.CellStyle = styleBodyIntRight;


                    cell = row.CreateCell(4);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLMauXi"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLMauDo"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLBotDa"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLDaDen2ly"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLDaTrang2Ly"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLDaTrang4Ly"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLXiMangPCB401"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(11);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["SoMeTron2"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(12);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLXiMangPCB402"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(13);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLMatDa"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(14);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["TLCatSongDa"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(15);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["SoLuong"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(16);
                    cell.SetCellValue(double.Parse(dt.Rows[i]["SoVien400x400"].ToString()));
                    cell.CellStyle = styleBodyIntRight;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= 16; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(3);
                cell.CellFormula = "SUM(D" + RowDau.ToString() + ":D" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(4);
                cell.CellFormula = "SUM(E" + RowDau.ToString() + ":E" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(5);
                cell.CellFormula = "SUM(F" + RowDau.ToString() + ":F" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(6);
                cell.CellFormula = "SUM(G" + RowDau.ToString() + ":G" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(7);
                cell.CellFormula = "SUM(H" + RowDau.ToString() + ":H" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(8);
                cell.CellFormula = "SUM(I" + RowDau.ToString() + ":I" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(9);
                cell.CellFormula = "SUM(J" + RowDau.ToString() + ":J" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(10);
                cell.CellFormula = "SUM(K" + RowDau.ToString() + ":K" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(11);
                cell.CellFormula = "SUM(L" + RowDau.ToString() + ":L" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(12);
                cell.CellFormula = "SUM(M" + RowDau.ToString() + ":M" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(13);
                cell.CellFormula = "SUM(N" + RowDau.ToString() + ":N" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(14);
                cell.CellFormula = "SUM(O" + RowDau.ToString() + ":O" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(15);
                cell.CellFormula = "SUM(P" + RowDau.ToString() + ":P" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                cell = row.CreateCell(16);
                cell.CellFormula = "SUM(Q" + RowDau.ToString() + ":Q" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntRight;

                #endregion

                #region[Set Footer]

                //rowIndex++;
                //rowIndex++;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo ca:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo khối:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê xe:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Không dùng bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Mua bê tông:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;
                //tên
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 1);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(0);
                cell.SetCellValue("Người xuất hàng");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(2);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 4, 5);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(4);
                cell.SetCellValue("Trưởng bộ phận");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 8);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(6);
                cell.SetCellValue("Lái xe");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 9, 10);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(9);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 11, 13);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(11);
                cell.SetCellValue("Giám sát");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                #endregion

                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "SẢN XUẤT GẠCH TERRAZO -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                         ".xls";
                    }
                    else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "SẢN XUẤT GẠCH TERRAZO -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                         "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }


        protected void btnChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else if (GVChot.Rows.Count == 0)
                {
                    GstGetMess("Thời gian này bạn không nhập gạch Terrazo", "");
                    mpChot.Show();
                }
                else
                {
                    var query = vatlieu.sp_GachTerrazo_CheckChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        vatlieu.sp_GachTerrazo_Chot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }

        protected void btnMoChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh mở chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else if (GVChot.Rows.Count == 0)
                {
                    GstGetMess("Thời gian này bạn không có gì để mở chốt.", "");
                    mpChot.Show();
                }
                else
                {
                    var query = vatlieu.sp_GachTerrazo_CheckMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        vatlieu.sp_GachTerrazo_MoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }

        protected void btnOpenChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Chốt nhập gạch";
            mpChot.Show();
            btnChot.Visible = true;
            btnMoChot.Visible = false;
            GVChot.DataSource = null;
            GVChot.DataBind();
        }

        protected void btnOpenMoChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Mở chốt nhập gạch";
            mpChot.Show();
            btnChot.Visible = false;
            btnMoChot.Visible = true;
            GVChot.DataSource = null;
            GVChot.DataBind();
        }

        protected void lbtSearchChot_Click(object sender, EventArgs e)
        {
            //if (btnChot.Visible == true)
            //{
            //    List<sp_GachTerrazo_ListChotResult> query = vatlieu.sp_GachTerrazo_ListChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
            //    GVChot.DataSource = query;
            //    GVChot.DataBind();

            //    if (GVChot.Rows.Count > 0)
            //    {
            //        if (query != null)
            //        {
            //            var valuefooter = (from p in query
            //                               group p by 1 into g
            //                               select new
            //                               {
            //                                   SumThoiTiet = g.Sum(x => x.ThoiTiet),
            //                                   SumSoMeTron2 = g.Sum(x => x.SoMeTron2),
            //                                   SumTLMauXi = g.Sum(x => x.TLMauXi),
            //                                   SumTLMauDo = g.Sum(x => x.TLMauDo),
            //                                   SumTLBotDa = g.Sum(x => x.TLBotDa),
            //                                   SumTLDaDen2ly = g.Sum(x => x.TLDaDen2ly),
            //                                   SumTLDaTrang2Ly = g.Sum(x => x.TLDaTrang2Ly),
            //                                   SumTLDaTrang4Ly = g.Sum(x => x.TLDaTrang4Ly),
            //                                   SumTLXiMangPCB401 = g.Sum(x => x.TLXiMangPCB401),
            //                                   SumTLXiMangPCB402 = g.Sum(x => x.TLXiMangPCB402),
            //                                   SumTLMatDa = g.Sum(x => x.TLMatDa),
            //                                   SumTLCatSongDa = g.Sum(x => x.TLCatSongDa),
            //                                   SumSoLuong = g.Sum(x => x.SoLuong)

            //                               }).FirstOrDefault();

            //            GVChot.FooterRow.Cells[0].ColumnSpan = 4;
            //            GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Count);
            //            GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

            //            GVChot.FooterRow.Cells[1].Text = string.Format("{0:N0}", valuefooter.SumSoLuong);
            //            GVChot.FooterRow.Cells[2].Text = string.Format("{0:N0}", valuefooter.SumThoiTiet);
            //            GVChot.FooterRow.Cells[3].Text = string.Format("{0:N0}", valuefooter.SumSoMeTron2);
            //            GVChot.FooterRow.Cells[4].Text = string.Format("{0:N0}", valuefooter.SumTLMauXi);
            //            GVChot.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.SumTLMauDo);
            //            GVChot.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.SumTLBotDa);
            //            GVChot.FooterRow.Cells[7].Text = string.Format("{0:N0}", valuefooter.SumTLDaDen2ly);
            //            GVChot.FooterRow.Cells[8].Text = string.Format("{0:N0}", valuefooter.SumTLDaTrang2Ly);
            //            GVChot.FooterRow.Cells[9].Text = string.Format("{0:N0}", valuefooter.SumTLDaTrang4Ly);
            //            GVChot.FooterRow.Cells[10].Text = string.Format("{0:N0}", valuefooter.SumTLXiMangPCB401);
            //            GVChot.FooterRow.Cells[11].Text = string.Format("{0:N0}", valuefooter.SumTLXiMangPCB402);
            //            GVChot.FooterRow.Cells[12].Text = string.Format("{0:N0}", valuefooter.SumTLMatDa);
            //            GVChot.FooterRow.Cells[13].Text = string.Format("{0:N0}", valuefooter.SumTLCatSongDa);

            //            GVChot.FooterRow.Cells[15].Visible = false;
            //            GVChot.FooterRow.Cells[16].Visible = false;
            //            //GVChot.FooterRow.Cells[17].Visible = false;
            //            //GVChot.FooterRow.Cells[18].Visible = false;
            //            //GVChot.FooterRow.Cells[19].Visible = false;
            //        }
            //    }
            //}
            //else if (btnChot.Visible == false)
            //{
            //    List<sp_GachTerrazo_ListMoChotResult> query = vatlieu.sp_GachTerrazo_ListMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
            //    GVChot.DataSource = query;
            //    GVChot.DataBind();

            //    if (GVChot.Rows.Count > 0)
            //    {
            //        if (query != null)
            //        {
            //            var valuefooter = (from p in query
            //                               group p by 1 into g
            //                               select new
            //                               {
            //                                   SumThoiTiet = g.Sum(x => x.ThoiTiet),
            //                                   SumSoMeTron2 = g.Sum(x => x.SoMeTron2),
            //                                   SumTLMauXi = g.Sum(x => x.TLMauXi),
            //                                   SumTLMauDo = g.Sum(x => x.TLMauDo),
            //                                   SumTLBotDa = g.Sum(x => x.TLBotDa),
            //                                   SumTLDaDen2ly = g.Sum(x => x.TLDaDen2ly),
            //                                   SumTLDaTrang2Ly = g.Sum(x => x.TLDaTrang2Ly),
            //                                   SumTLDaTrang4Ly = g.Sum(x => x.TLDaTrang4Ly),
            //                                   SumTLXiMangPCB401 = g.Sum(x => x.TLXiMangPCB401),
            //                                   SumTLXiMangPCB402 = g.Sum(x => x.TLXiMangPCB402),
            //                                   SumTLMatDa = g.Sum(x => x.TLMatDa),
            //                                   SumTLCatSongDa = g.Sum(x => x.TLCatSongDa),
            //                                   SumSoLuong = g.Sum(x => x.SoLuong)
            //                               }).FirstOrDefault();

            //            GVChot.FooterRow.Cells[0].ColumnSpan = 4;
            //            GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Count);
            //            GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

            //            GVChot.FooterRow.Cells[1].Text = string.Format("{0:N0}", valuefooter.SumSoLuong);
            //            GVChot.FooterRow.Cells[2].Text = string.Format("{0:N0}", valuefooter.SumThoiTiet);
            //            GVChot.FooterRow.Cells[3].Text = string.Format("{0:N0}", valuefooter.SumSoMeTron2);
            //            GVChot.FooterRow.Cells[4].Text = string.Format("{0:N0}", valuefooter.SumTLMauXi);
            //            GVChot.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.SumTLMauDo);
            //            GVChot.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.SumTLBotDa);
            //            GVChot.FooterRow.Cells[7].Text = string.Format("{0:N0}", valuefooter.SumTLDaDen2ly);
            //            GVChot.FooterRow.Cells[8].Text = string.Format("{0:N0}", valuefooter.SumTLDaTrang2Ly);
            //            GVChot.FooterRow.Cells[9].Text = string.Format("{0:N0}", valuefooter.SumTLDaTrang4Ly);
            //            GVChot.FooterRow.Cells[10].Text = string.Format("{0:N0}", valuefooter.SumTLXiMangPCB401);
            //            GVChot.FooterRow.Cells[11].Text = string.Format("{0:N0}", valuefooter.SumTLXiMangPCB402);
            //            GVChot.FooterRow.Cells[12].Text = string.Format("{0:N0}", valuefooter.SumTLMatDa);
            //            GVChot.FooterRow.Cells[13].Text = string.Format("{0:N0}", valuefooter.SumTLCatSongDa);

            //            GVChot.FooterRow.Cells[15].Visible = false;
            //            GVChot.FooterRow.Cells[16].Visible = false;
            //            //GVChot.FooterRow.Cells[17].Visible = false;
            //            //GVChot.FooterRow.Cells[18].Visible = false;
            //            //GVChot.FooterRow.Cells[19].Visible = false;
            //        }
            //    }
            //}
            //mpChot.Show();
        }

    }
}