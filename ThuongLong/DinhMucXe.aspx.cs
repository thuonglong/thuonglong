﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class DinhMucXe : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        ThietBiDataContext thietbi = new ThietBiDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmDinhMucXe";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmDinhMucXe", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        DinhMucXeBen.Visible = false;
                        DinhMucXeDauKeo.Visible = false;
                        DinhMucXeXucLat.Visible = false;
                        DinhMucXeXucDao.Visible = false;
                        DinhMucXeBom.Visible = false;
                        DinhMucXeBeTong.Visible = false;
                        DinhMucXeCau.Visible = false;
                    }
                }
            }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }

        protected void dlLoaiXe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlLoaiXe.SelectedValue == "")
            {
                DinhMucXeBen.Visible = false;
                DinhMucXeDauKeo.Visible = false;
                DinhMucXeXucLat.Visible = false;
                DinhMucXeXucDao.Visible = false;
                DinhMucXeBom.Visible = false;
                DinhMucXeBeTong.Visible = false;
                DinhMucXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "1")
            {
                DinhMucXeBen.Visible = true;
                DinhMucXeDauKeo.Visible = false;
                DinhMucXeXucLat.Visible = false;
                DinhMucXeXucDao.Visible = false;
                DinhMucXeBom.Visible = false;
                DinhMucXeBeTong.Visible = false;
                DinhMucXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "2")
            {
                DinhMucXeBen.Visible = false;
                DinhMucXeDauKeo.Visible = true;
                DinhMucXeXucLat.Visible = false;
                DinhMucXeXucDao.Visible = false;
                DinhMucXeBom.Visible = false;
                DinhMucXeBeTong.Visible = false;
                DinhMucXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "3")
            {
                DinhMucXeBen.Visible = false;
                DinhMucXeDauKeo.Visible = false;
                DinhMucXeXucLat.Visible = true;
                DinhMucXeXucDao.Visible = false;
                DinhMucXeBom.Visible = false;
                DinhMucXeBeTong.Visible = false;
                DinhMucXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "4")
            {
                DinhMucXeBen.Visible = false;
                DinhMucXeDauKeo.Visible = false;
                DinhMucXeXucLat.Visible = false;
                DinhMucXeXucDao.Visible = true;
                DinhMucXeBom.Visible = false;
                DinhMucXeBeTong.Visible = false;
                DinhMucXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "5")
            {
                DinhMucXeBen.Visible = false;
                DinhMucXeDauKeo.Visible = false;
                DinhMucXeXucLat.Visible = false;
                DinhMucXeXucDao.Visible = false;
                DinhMucXeBom.Visible = true;
                DinhMucXeBeTong.Visible = false;
                DinhMucXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "6")
            {
                DinhMucXeBen.Visible = false;
                DinhMucXeDauKeo.Visible = false;
                DinhMucXeXucLat.Visible = false;
                DinhMucXeXucDao.Visible = false;
                DinhMucXeBom.Visible = false;
                DinhMucXeBeTong.Visible = true;
                DinhMucXeCau.Visible = false;
            }
            else if (dlLoaiXe.SelectedValue == "7")
            {
                DinhMucXeBen.Visible = false;
                DinhMucXeDauKeo.Visible = false;
                DinhMucXeXucLat.Visible = false;
                DinhMucXeXucDao.Visible = false;
                DinhMucXeBom.Visible = false;
                DinhMucXeBeTong.Visible = false;
                DinhMucXeCau.Visible = true;
            }
        }
    }
}