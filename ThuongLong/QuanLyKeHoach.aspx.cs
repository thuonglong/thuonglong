﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class QuanLyKeHoach : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmKeHoach";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmKeHoach", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtThang.Text = dateTimenow.Month.ToString();
                        txtNam.Text = dateTimenow.Year.ToString();
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }

            if (s != "")
                GstGetMess(s, "");
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }

            //else if (FileUpload1.HasFile == false)
            //{
            //    s += " - Chọn file scan kế hoạch";
            //}
            //if (s == "")
            //{
            //    var query = vatlieu.sp_KeHoach_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlLoaiKeHoach), GetDrop(dlNoiNhan), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text) > 0 ? true : false,
            //        DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            //}

            if (s != "")
                GstGetMess(s, "");
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string filename = "";
            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        Guid ID = Guid.NewGuid();
                        List<tblKeHoachThang> them = new List<tblKeHoachThang>();
                        var betong = new tblKeHoachThang()
                        {
                            ID = Guid.NewGuid(),
                            IDChung = ID,
                            NgayThang = new DateTime(int.Parse(txtNam.Text.Trim()), int.Parse(txtThang.Text.Trim()), 1),
                            Thang = int.Parse(txtThang.Text.Trim()),
                            Nam = int.Parse(txtNam.Text.Trim()),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhomVatLieu = new Guid("29FC56FA-D0DD-469C-A551-CDC8123C1189"),
                            SoLuong = double.Parse(txtBeTong.Text.Trim()),
                            Loai = 1,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",

                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        them.Add(betong);
                        var terrazo = new tblKeHoachThang()
                        {
                            ID = Guid.NewGuid(),

                            IDChung = ID,
                            NgayThang = new DateTime(int.Parse(txtNam.Text.Trim()), int.Parse(txtThang.Text.Trim()), 1),
                            Thang = int.Parse(txtThang.Text.Trim()),
                            Nam = int.Parse(txtNam.Text.Trim()),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhomVatLieu = new Guid("7D25A3D1-BE90-4F1D-A0CB-8A518AC09B55"),
                            SoLuong = double.Parse(txtTerrazo.Text.Trim()),
                            Loai = 2,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",

                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        them.Add(terrazo);
                        var menbong = new tblKeHoachThang()
                        {
                            ID = Guid.NewGuid(),
                            IDChung = ID,
                            NgayThang = new DateTime(int.Parse(txtNam.Text.Trim()), int.Parse(txtThang.Text.Trim()), 1),
                            Thang = int.Parse(txtThang.Text.Trim()),
                            Nam = int.Parse(txtNam.Text.Trim()),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhomVatLieu = new Guid("C2B47A53-3F0A-4E9F-8172-3C3809034BCA"),
                            SoLuong = double.Parse(txtMenBong.Text.Trim()),
                            Loai = 3,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",

                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        them.Add(menbong);

                        var xaydung = new tblKeHoachThang()
                        {
                            ID = Guid.NewGuid(),
                            IDChung = ID,
                            NgayThang = new DateTime(int.Parse(txtNam.Text.Trim()), int.Parse(txtThang.Text.Trim()), 1),
                            Thang = int.Parse(txtThang.Text.Trim()),
                            Nam = int.Parse(txtNam.Text.Trim()),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhomVatLieu = new Guid("97CF73BB-E552-4A07-9F8B-5DD5C68F7287"),
                            SoLuong = double.Parse(txtXayDung.Text.Trim()),
                            Loai = 4,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",

                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        them.Add(xaydung);

                        db.tblKeHoachThangs.InsertAllOnSubmit(them);
                        db.SubmitChanges();

                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        GstGetMess("Lưu thành công.", "");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var betong = (from p in db.tblKeHoachThangs
                                      where p.IDChung == new Guid(hdID.Value)
                                      && p.Loai == 1
                                      select p).FirstOrDefault();
                        if (betong != null && betong.ID != null)
                        {
                            betong.IDChiNhanh = GetDrop(dlChiNhanh);
                            betong.NgayThang = new DateTime(int.Parse(txtNam.Text.Trim()), int.Parse(txtThang.Text.Trim()), 1);
                            betong.Thang = int.Parse(txtThang.Text.Trim());
                            betong.Nam = int.Parse(txtNam.Text.Trim());
                            betong.SoLuong = double.Parse(txtBeTong.Text.Trim());
                            betong.TrangThai = 1;
                            betong.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            betong.NguoiTao = Session["IDND"].ToString();
                            betong.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                        }
                        else
                        {
                            Warning("Thông tin kế hoạch đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }


                        var terrazo = (from p in db.tblKeHoachThangs
                                       where p.IDChung == new Guid(hdID.Value)
                                       && p.Loai == 2
                                       select p).FirstOrDefault();
                        if (betong != null && betong.ID != null)
                        {
                            betong.IDChiNhanh = GetDrop(dlChiNhanh);
                            betong.NgayThang = new DateTime(int.Parse(txtNam.Text.Trim()), int.Parse(txtThang.Text.Trim()), 1);
                            betong.Thang = int.Parse(txtThang.Text.Trim());
                            betong.Nam = int.Parse(txtNam.Text.Trim());
                            betong.SoLuong = double.Parse(txtTerrazo.Text.Trim());
                            betong.TrangThai = 1;
                            betong.TrangThaiText = "Chờ duyệt";

                            betong.NguoiTao = Session["IDND"].ToString();
                            betong.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                        }
                        else
                        {
                            Warning("Thông tin kế hoạch đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }

                        var menbong = (from p in db.tblKeHoachThangs
                                       where p.IDChung == new Guid(hdID.Value)
                                       && p.Loai == 3
                                       select p).FirstOrDefault();
                        if (menbong != null && menbong.ID != null)
                        {
                            menbong.IDChiNhanh = GetDrop(dlChiNhanh);
                            menbong.NgayThang = new DateTime(int.Parse(txtNam.Text.Trim()), int.Parse(txtThang.Text.Trim()), 1);
                            menbong.Thang = int.Parse(txtThang.Text.Trim());
                            menbong.Nam = int.Parse(txtNam.Text.Trim());
                            menbong.SoLuong = double.Parse(txtMenBong.Text.Trim());
                            menbong.TrangThai = 1;
                            menbong.TrangThaiText = "Chờ duyệt";
                            menbong.NguoiTao = Session["IDND"].ToString();
                            menbong.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                        }
                        else
                        {
                            Warning("Thông tin kế hoạch đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }

                        var xaydung = (from p in db.tblKeHoachThangs
                                       where p.IDChung == new Guid(hdID.Value)
                                       && p.Loai == 3
                                       select p).FirstOrDefault();
                        if (xaydung != null && xaydung.ID != null)
                        {
                            xaydung.IDChiNhanh = GetDrop(dlChiNhanh);
                            xaydung.NgayThang = new DateTime(int.Parse(txtNam.Text.Trim()), int.Parse(txtThang.Text.Trim()), 1);
                            xaydung.Thang = int.Parse(txtThang.Text.Trim());
                            xaydung.Nam = int.Parse(txtNam.Text.Trim());
                            xaydung.SoLuong = double.Parse(txtMenBong.Text.Trim());
                            xaydung.TrangThai = 1;
                            xaydung.TrangThaiText = "Chờ duyệt";
                            xaydung.NguoiTao = Session["IDND"].ToString();
                            xaydung.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                        }
                        else
                        {
                            Warning("Thông tin kế hoạch đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }

                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            txtTerrazo.Text = "";
            txtMenBong.Text = "";
            txtXayDung.Text = "";
            txtBeTong.Text = "";
            hdID.Value = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblKeHoachThangs
                         where p.IDChung == new Guid(id)
                         && p.Loai == 1
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        txtThang.Text = query.Thang.ToString();
                        txtNam.Text = query.Nam.ToString();
                        txtBeTong.Text = query.SoLuong.ToString();
                        var terrazo = (from p in db.tblKeHoachThangs
                                       where p.IDChung == new Guid(id)
                                       && p.Loai == 1
                                       select p).FirstOrDefault();

                        txtTerrazo.Text = terrazo.SoLuong.ToString();
                        var menbong = (from p in db.tblKeHoachThangs
                                       where p.IDChung == new Guid(id)
                                       && p.Loai == 1
                                       select p).FirstOrDefault();

                        txtMenBong.Text = menbong.SoLuong.ToString();
                        var xaydung = (from p in db.tblKeHoachThangs
                                       where p.IDChung == new Guid(id)
                                       && p.Loai == 1
                                       select p).FirstOrDefault();

                        txtXayDung.Text = xaydung.SoLuong.ToString();

                        hdID.Value = id;
                        //btnSave.Text = "Cập nhật";
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var xoa = from p in db.tblKeHoachThangs
                                  where p.IDChung == new Guid(id)
                                  select p;
                        foreach (var item in xoa)
                        {
                            item.TrangThai = 3;
                            item.TrangThaiText = "Chờ duyệt xóa";
                        }
                        db.SubmitChanges();
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = db.sp_KeHoach_LichSu(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin kế hoạch đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = db.sp_KeHoachThang_Search(GetDrop(dlChiNhanhSearch), 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
        }
    }
}