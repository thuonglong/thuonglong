﻿<%@ Page Title="Bán gạch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BanGach.aspx.cs" Inherits="ThuongLong.BanGach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin khách hàng</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày bán*</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Khách hàng*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCap" OnSelectedIndexChanged="dlNhaCungCap_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Công trình*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Công trình"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCongTrinh" OnSelectedIndexChanged="dlCongTrinh_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Hạng mục</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Hạng mục"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlHangMuc" OnSelectedIndexChanged="dlHangMuc_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Nhân viên kinh doanh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhân viên kinh doanh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhanVien" runat="server" disabled>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Biển số xe*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlBienSoXe" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Lái xe*</label>
                        <asp:TextBox ID="txtTenLaiXe" runat="server" class="form-control" placeholder="Lái xe" Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="box-header with-border">
                    <h3 class="box-title">Chi tiết phiếu</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Nhóm gạch*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm gạch" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhomVatLieu" runat="server" OnSelectedIndexChanged="dlNhomVatLieu_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Loại gạch*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại gạch"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiVatLieu" runat="server" OnSelectedIndexChanged="dlLoaiVatLieu_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn vị tính*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDonViTinh" runat="server" OnSelectedIndexChanged="dlDonViTinh_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá hóa đơn*</label>
                        <asp:TextBox ID="txtDonGiaCoThue" runat="server" class="form-control" placeholder="Đơn giá hóa đơn" Width="98%" disabled></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá thanh toán*</label>
                        <asp:TextBox ID="txtDonGiaKhongThue" runat="server" class="form-control" placeholder="Đơn giá thanh toán" Width="98%" disabled></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số lượng thực xuất*</label>
                        <asp:TextBox ID="txtSoLuongThucXuat" runat="server" class="form-control" placeholder="Số lượng thực xuất" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtSoLuongThucXuat" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số lượng KH xác nhận*</label>
                        <asp:TextBox ID="txtSoLuongNhan" runat="server" class="form-control" placeholder="Số lượng khách hàng xác nhận" onchange="klKhachHang()" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDonGiaKhongThue" ValidChars=".," />
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Thành tiền hóa đơn</label>
                        <asp:TextBox ID="txtThanhTienCoThue" runat="server" class="form-control" placeholder="Thành tiền hóa đơn" Width="98%" disabled></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Thành tiền thanh toán</label>
                        <asp:TextBox ID="txtThanhTienKhongThue" runat="server" class="form-control" placeholder="Thành tiền thanh toán" Width="98%" disabled></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Phụ phí</label>
                        <asp:TextBox ID="txtPhuPhi" runat="server" class="form-control" placeholder="Phụ phí" Width="98%" onchange="phuphi()"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ghi chú</label>
                        <asp:TextBox ID="txtMoTa" runat="server" class="form-control" placeholder="Ghi chú" Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtSave" runat="server" class="btn btn-app bg-green" OnClick="lbtSave_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lbtTaoGiaMoi" runat="server" class="btn btn-app bg-warning" OnClick="lbtTaoGiaMoi_Click"><i class="fa fa-plus"></i>Tạo giá mới</asp:LinkButton>
                    </div>
                </div>

                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12" style="overflow: auto; width: 100%;">
                                <asp:GridView ID="gvChiTiet" runat="server" AutoGenerateColumns="false" OnRowCommand="gvChiTiet_RowCommand"
                                    EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trạng thái">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblLichSu"
                                                    Text='<%#Eval("TThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                    CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm gạch" />
                                        <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại gạch" />
                                        <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                        <asp:BoundField DataField="SoLuongThucXuat" HeaderText="Số lượng thực xuất" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="SoLuongNhan" HeaderText="Số lượng khách hàng nhận" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="TTCoThue" HeaderText="Thành tiền hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="TTKhongThue" HeaderText="Thành tiền thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="PhuPhi" HeaderText="Phụ phí" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                        HorizontalAlign="Right" />
                                    <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                    <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu phiếu bán</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo phiếu bán mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách bán gạch</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Khách hàng</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Khách hàng" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlKhachHangSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                            <asp:LinkButton ID="btnOpenChot" runat="server" class="btn btn-app bg-green" OnClick="btnOpenChot_Click"><i class="fa fa-close"></i>Chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnOpenMoChot" runat="server" class="btn btn-app bg-warning" OnClick="btnOpenMoChot_Click"><i class="fa fa-opencart"></i>Mở chốt</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" ShowFooter="true"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="SoPhieu" HeaderText="Số phiếu" />
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" />
                                    <asp:BoundField DataField="CongTrinh" HeaderText="Công trình" />
                                    <asp:BoundField DataField="TenNhanVien" HeaderText="Nhân viên kinh doanh" />
                                    <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" />
                                    <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm gạch" />
                                    <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại gạch" />
                                    <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                    <asp:BoundField DataField="SoLuongThucXuat" HeaderText="Số lượng thực xuất" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="SoLuongNhan" HeaderText="Số lượng khách hàng nhận" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TTCoThue" HeaderText="Thành tiền hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TTKhongThue" HeaderText="Thành tiền thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="PhuPhi" HeaderText="Phụ phí" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Tổng tiền hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Tổng tiền thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="MoTa" HeaderText="Ghi chú" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function klKhachHang() {
                    var txtDonGiaCoThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value));
                    var txtDonGiaKhongThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value));
                    var txtSoLuongNhan = parseFloat(getvalue(document.getElementById("<%=txtSoLuongNhan.ClientID %>").value));
                    if (txtSoLuongNhan != '0') {
                        document.getElementById("<%=txtThanhTienCoThue.ClientID %>").value = (txtDonGiaCoThue * txtSoLuongNhan).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtThanhTienKhongThue.ClientID %>").value = (txtDonGiaKhongThue * txtSoLuongNhan).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtThanhTienCoThue.ClientID %>").value = '0';
                        document.getElementById("<%=txtThanhTienKhongThue.ClientID %>").value = '0';
                    }
                }
                function phuphi() {
                    var txtPhuPhi = parseFloat(getvalue(document.getElementById("<%=txtPhuPhi.ClientID %>").value));
                    document.getElementById("<%=txtPhuPhi.ClientID %>").value = (txtPhuPhi).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdChung" runat="server" Value="" />
            <asp:HiddenField ID="hdChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdForm" runat="server" Value="" />
            <asp:HiddenField ID="hdNgayTao" runat="server" Value="" />
            <asp:HiddenField ID="hdChot" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="SoPhieu" HeaderText="Số phiếu" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" />
                                <asp:BoundField DataField="CongTrinh" HeaderText="Công trình" />
                                <asp:BoundField DataField="TenNhanVien" HeaderText="Nhân viên kinh doanh" />
                                <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" />
                                <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm gạch" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại gạch" />
                                <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                <asp:BoundField DataField="SoLuongThucXuat" HeaderText="Số lượng thực xuất" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="SoLuongNhan" HeaderText="Số lượng khách hàng nhận" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TTCoThue" HeaderText="Thành tiền hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TTKhongThue" HeaderText="Thành tiền thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="PhuPhi" HeaderText="Phụ phí" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Tổng tiền hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Tổng tiền thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpChot" runat="server" CancelControlID="btnDongChot"
                Drag="True" TargetControlID="hdChot" BackgroundCssClass="modalBackground" PopupControlID="pnChot"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnChot" runat="server" Style="width: 98%; height: 100%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        <asp:Label ID="lblChot" runat="server" Text="Chốt bán gạch"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh chốt</label>
                            <asp:DropDownList CssClass="form-control" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh chốt"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhChot" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày chốt</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayChot" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Button ID="lbtSearchChot" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="lbtSearchChot_Click" />
                            <asp:Button ID="btnChot" runat="server" Text="Chốt" CssClass="btn btn-primary" OnClick="btnChot_Click" />
                            <asp:Button ID="btnMoChot" runat="server" Text="Mở chốt" CssClass="btn btn-primary" OnClick="btnMoChot_Click" />
                            <asp:Button ID="btnDongChot" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                        </div>
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="GVChot" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound" ShowFooter="true"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px" Width="2300px">
                            <Columns>
                                <asp:BoundField DataField="TrangThaiText" HeaderText="Trạng thái" />
                                <asp:BoundField DataField="SoPhieu" HeaderText="Số phiếu" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="Khách hàng" />
                                <asp:BoundField DataField="CongTrinh" HeaderText="Công trình" />
                                <asp:BoundField DataField="TenNhanVien" HeaderText="Nhân viên kinh doanh" />
                                <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" />
                                <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm gạch" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại gạch" />
                                <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                                <asp:BoundField DataField="SoLuongThucXuat" HeaderText="Số lượng thực xuất" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="SoLuongNhan" HeaderText="Số lượng khách hàng nhận" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TTCoThue" HeaderText="Thành tiền hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TTKhongThue" HeaderText="Thành tiền thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="PhuPhi" HeaderText="Phụ phí" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Tổng tiền hóa đơn" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Tổng tiền thanh toán" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
