﻿<%@ Page Title="Định mức gạch men bóng" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CaiDatGachMenBong.aspx.cs" Inherits="ThuongLong.CaiDatGachMenBong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh*</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3" id="divloaisanpham" runat="server">
                            <label for="exampleInputEmail1">Loại sản phẩm*</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại sản phẩm"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiSanPham" runat="server" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3" id="div2" runat="server">
                            <label for="exampleInputEmail1">Thời tiết*</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Thời tiết"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThoiTiet" runat="server" OnSelectedIndexChanged="dlThoiTiet_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Nắng</asp:ListItem>
                                <asp:ListItem Value="1">Mưa</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
            </div>


            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Lớp mặt</h3>
                </div>
                <div class="box-body">
                    <div class="form-group col-lg-3" id="div9" runat="server">
                        <label for="exampleInputEmail1">Cát sông đà*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCatSongDa1" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div" runat="server">
                        <label for="exampleInputEmail1">Bột màu*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlBotMau"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div1" runat="server">
                        <label for="exampleInputEmail1">Keo bóng*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm vật liệu"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlKeoBong" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div6" runat="server">
                        <label for="exampleInputEmail1">Xi măng PCB40*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMangPCB401" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Cát sông đà*</label>
                        <asp:TextBox ID="txtCatSongDa1" runat="server" class="form-control" placeholder="Cát sông đà" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtCatSongDa1" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Bột màu*</label>
                        <asp:TextBox ID="txtBotMau" runat="server" class="form-control" placeholder="Màu xi" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtBotMau" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Keo bóng*</label>
                        <asp:TextBox ID="txtKeoBong" runat="server" class="form-control" placeholder="Màu đỏ" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKeoBong" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xi măng PCB40*</label>
                        <asp:TextBox ID="txtXiMangPCB401" runat="server" class="form-control" placeholder="Xi măng PCB40 1" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtXiMangPCB401" ValidChars=".," />
                    </div>
                </div>
            </div>




            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Lớp thân</h3>
                </div>
                <div class="box-body">
                    <div class="form-group col-lg-3" id="div10" runat="server">
                        <label for="exampleInputEmail1">Cát sông đà*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCatSongDa2" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div7" runat="server">
                        <label for="exampleInputEmail1">Xi măng PCB40*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMangPCB402" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div8" runat="server">
                        <label for="exampleInputEmail1">Đá mạt*</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDaMat" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Cát sông đà*</label>
                        <asp:TextBox ID="txtCatSongDa2" runat="server" class="form-control" placeholder="Cát sông đà" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtCatSongDa2" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xi măng PCB40*</label>
                        <asp:TextBox ID="txtXiMangPCB402" runat="server" class="form-control" placeholder="Xi măng PCB40 2" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtXiMangPCB402" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá mạt*</label>
                        <asp:TextBox ID="txtDaMat" runat="server" class="form-control" placeholder="Mạt đá" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtDaMat" ValidChars=".," />
                    </div>
                </div>
            </div>


            <div class="box-footer">
                <div class="col-md-12">
                    <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                    <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách giá</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server" Text="Loại sản phẩm"></asp:Label><br />
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại sản phẩm" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlLoaiVatLieuSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                    <asp:BoundField DataField="LoaiVatLieu" HeaderText="Loại gạch" />
                                    <asp:BoundField DataField="CatSongDa1" HeaderText="Cát sông đà lớp mặt" />
                                    <asp:BoundField DataField="BotMau" HeaderText="Bột màu" />
                                    <asp:BoundField DataField="KeoBong" HeaderText="Keo bóng" />
                                    <asp:BoundField DataField="XiMangPCB401" HeaderText="Xi măng PCB40 lớp mặt" />
                                    <asp:BoundField DataField="CatSongDa2" HeaderText="Cát sông đà lớp thân" />
                                    <asp:BoundField DataField="XiMangPCB402" HeaderText="Xi măng PCB40 lớp thân" />
                                    <asp:BoundField DataField="DaMat" HeaderText="Đá mạt" />
                                    <asp:BoundField DataField="DMCatSongDa1" HeaderText="ĐM Cát sông đà lớp mặt" />
                                    <asp:BoundField DataField="DMBotMau" HeaderText="ĐM Bột màu" />
                                    <asp:BoundField DataField="DMKeoBong" HeaderText="ĐM Keo bóng" />
                                    <asp:BoundField DataField="DMXiMangPCB401" HeaderText="ĐM Xi măng PCB40 lớp mặt" />
                                    <asp:BoundField DataField="DMCatSongDa2" HeaderText="ĐM Cát sông đà lớp thân" />
                                    <asp:BoundField DataField="DMXiMangPCB402" HeaderText="ĐM Xi măng PCB40 lớp thân" />
                                    <asp:BoundField DataField="DMDaMat" HeaderText="ĐM Đá mạt" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>

            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdChung" runat="server" Value="" />
            <asp:HiddenField ID="hdChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                <asp:BoundField DataField="LoaiVatLieu" HeaderText="Loại gạch" />
                                <asp:BoundField DataField="CatSongDa1" HeaderText="Cát sông đà lớp mặt" />
                                <asp:BoundField DataField="BotMau" HeaderText="Bột màu" />
                                <asp:BoundField DataField="KeoBong" HeaderText="Keo bóng" />
                                <asp:BoundField DataField="XiMangPCB401" HeaderText="Xi măng PCB40 lớp mặt" />
                                <asp:BoundField DataField="CatSongDa2" HeaderText="Cát sông đà lớp thân" />
                                <asp:BoundField DataField="XiMangPCB402" HeaderText="Xi măng PCB40 lớp thân" />
                                <asp:BoundField DataField="DaMat" HeaderText="Đá mạt" />
                                <asp:BoundField DataField="DMCatSongDa1" HeaderText="ĐM Cát sông đà lớp mặt" />
                                <asp:BoundField DataField="DMBotMau" HeaderText="ĐM Bột màu" />
                                <asp:BoundField DataField="DMKeoBong" HeaderText="ĐM Keo bóng" />
                                <asp:BoundField DataField="DMXiMangPCB401" HeaderText="ĐM Xi măng PCB40 lớp mặt" />
                                <asp:BoundField DataField="DMCatSongDa2" HeaderText="ĐM Cát sông đà lớp thân" />
                                <asp:BoundField DataField="DMXiMangPCB402" HeaderText="ĐM Xi măng PCB40 lớp thân" />
                                <asp:BoundField DataField="DMDaMat" HeaderText="ĐM Đá mạt" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
