﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_DuyetDoSut.ascx.cs" Inherits="ThuongLong.DuyetDanhMuc.uc_DuyetDoSut" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="panel-body">
            <div class="form-group">
                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-warning" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
            </div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                EmptyDataText="Không có dữ liệu nào để duyệt" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                <Columns>
                    <asp:TemplateField HeaderText="Duyệt" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Duyệt"
                                CommandName="Sua" OnClientClick="return confirm('Bạn có muốn duyệt không?')"><i class="fa fa-check"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trạng thái">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblLichSu"
                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TenDoSut" HeaderText="Tên độ sụt" />
                    <asp:BoundField DataField="STT" HeaderText="Sắp xếp" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                    HorizontalAlign="Right" />
                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
            </asp:GridView>
        </div>
        <div style="margin: 5px;" class="btn-group">
            <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
            <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
            <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
        </div>

        <ajaxToolkit:ModalPopupExtender ID="mpDuyet" runat="server" CancelControlID="btnDong"
            Drag="True" TargetControlID="hdDuyet" BackgroundCssClass="modalBackground" PopupControlID="pnDuyet"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnDuyet" runat="server" Style="display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    DUYỆT
                </div>
                <div class="panel-body">
                    <asp:GridView ID="gvDuyet" runat="server" AutoGenerateColumns="false" Width="1120px" OnRowDataBound="GridView2_RowDataBound"
                        EmptyDataText="Không có dữ liệu nào" class="table table-striped table-bordered table-hover">
                        <Columns>
                            <asp:BoundField DataField="STT" HeaderText="Trạng thái" />
                            <asp:BoundField DataField="TenDoSut" HeaderText="Tên độ sụt" />
                            <asp:BoundField DataField="GhiChu" HeaderText="Ghi chú" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnDuyet" runat="server" CssClass="btn btn-primary" OnClick="btnDuyetTB_Click"
                        OnClientClick="return confirm('Bạn chắc chắn muốn duyệt ?')" Text="Duyệt" />
                    <asp:Button ID="btnKhongDuyet" runat="server" CssClass="btn btn-warning" OnClick="btnKDuyetTB_Click"
                        Text="Không duyệt" />
                    <asp:Button ID="btnDong" runat="server" CssClass="btn btn-danger"
                        Text="Đóng" />
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hdID" runat="server" />
        <asp:HiddenField ID="hdPage" runat="server" />
        <asp:HiddenField ID="hdDuyet" runat="server" />

        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

        <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
            Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center">
                    Lịch sử
                </div>
                <div class="panel-body" style="max-height: 500px; overflow: auto;">
                    <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                        EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                        <Columns>
                            <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                            <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                            <asp:BoundField DataField="TenDoSut" HeaderText="Tên độ sụt" />
                            <asp:BoundField DataField="STT" HeaderText="Sắp xếp" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer" style="align-content: center; text-align: center">
                    <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                </div>
            </div>
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <%--<asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="btnGiaTri" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btncapnhat" />
            <asp:PostBackTrigger ControlID="btnKhoiLuong" />--%>
    </Triggers>
</asp:UpdatePanel>


