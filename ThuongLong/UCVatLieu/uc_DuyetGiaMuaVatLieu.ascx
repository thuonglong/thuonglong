﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_DuyetGiaMuaVatLieu.ascx.cs" Inherits="ThuongLong.UCVatLieu.uc_DuyetGiaMuaVatLieu" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-3" id="divchinhanh" runat="server">
                    <div class="form-group">
                        <label>Chi nhánh</label>
                        <asp:DropDownList ID="dlChiNhanh" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3" runat="server" visible="false">
                    <div class="form-group">
                        <label>Trạng thái</label>
                        <asp:RadioButtonList ID="rdTrangThai" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Value="1">Chưa duyệt</asp:ListItem>
                            <asp:ListItem Value="2">Đã duyệt</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label></label>
                        <br />
                        <asp:Button ID="btnSearch" runat="server" Text="Xem" class="btn btn-primary" OnClick="btnSearch_Click" />
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                <Columns>
                    <asp:TemplateField HeaderText="Duyệt" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Duyệt"
                                CommandName="Sua" OnClientClick="return confirm('Bạn có muốn duyệt không?')"><i class="fa fa-check"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Không duyệt" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtHuy" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Duyệt"
                                CommandName="Huy"><i class="fa fa-times" style="color:#ff0000;"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trạng thái">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblLichSu"
                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                    <asp:BoundField DataField="NhaCungCap" HeaderText="Nhà cung cấp" />
                    <asp:BoundField DataField="NhomVatLieu" HeaderText="Nhóm vật liệu" />
                    <asp:BoundField DataField="LoaiVatLieu" HeaderText="Loại vật liệu" />
                    <asp:BoundField DataField="DonViTinh" HeaderText="Đơn vị tính" />
                    <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá có thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá không thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                    HorizontalAlign="Right" />
                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
            </asp:GridView>
        </div>
        <div style="margin: 5px;" class="btn-group">
            <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
            <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
            <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
        </div>
        
        <ajaxToolkit:ModalPopupExtender ID="mpDuyet" runat="server" CancelControlID="btnDong"
            Drag="True" TargetControlID="hdDuyet" BackgroundCssClass="modalBackground" PopupControlID="pnDuyet"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnDuyet" runat="server" Style="display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    DUYỆT
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12">
                        <label for="exampleInputEmail1">Lý do không duyệt</label>
                        <asp:TextBox ID="txtMota" runat="server" class="form-control" placeholder="Lý do không duyệt" Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnKhongDuyet" runat="server" CssClass="btn btn-warning" OnClick="btnKDuyetTB_Click"
                        Text="Không duyệt" />
                    <asp:Button ID="btnDong" runat="server" CssClass="btn btn-danger"
                        Text="Đóng" />
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hdID" runat="server" />
        <asp:HiddenField ID="hdPage" runat="server" />
        <asp:HiddenField ID="hdDuyet" runat="server" />

        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

        <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
            Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center">
                    Lịch sử
                </div>
                <div class="panel-body" style="max-height: 500px; overflow: auto;">
                    <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                        EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                        <Columns>
                            <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                            <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                            <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                            <asp:BoundField DataField="NhaCungCap" HeaderText="Nhà cung cấp" />
                            <asp:BoundField DataField="NhomVatLieu" HeaderText="Nhóm vật liệu" />
                            <asp:BoundField DataField="LoaiVatLieu" HeaderText="Loại vật liệu" />
                            <asp:BoundField DataField="DonViTinh" HeaderText="Đơn vị tính" />
                            <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá có thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá không thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer" style="align-content: center; text-align: center">
                    <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                </div>
            </div>
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <%--<asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="btnGiaTri" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btncapnhat" />
            <asp:PostBackTrigger ControlID="btnKhoiLuong" />--%>
    </Triggers>
</asp:UpdatePanel>


