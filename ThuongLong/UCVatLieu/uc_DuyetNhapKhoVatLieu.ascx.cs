﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCVatLieu
{
    public partial class uc_DuyetNhapKhoVatLieu : System.Web.UI.UserControl
    {
        private string frmName = "frmNhapKhoVatLieu";
        private static int PageNumber = 20;
        private static int CurrentPage = 1;
        private DBDataContext db = new DBDataContext();
        private VatLieuDataContext dm = new VatLieuDataContext();
        private clsPhanQuyen phanquyen = new clsPhanQuyen();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    LoadChiNhanh();
                    //  IDDN = new Guid(Session["IDDN"].ToString());
                }
            }
        }

        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            //dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        public DateTime GetLastDayOfMonth(int iMonth, int iYear)
        {
            DateTime dtResult = new DateTime(iYear, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-1);
            return dtResult;
        }

        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        protected void btnDong_Click(object sender, EventArgs e)
        {
            mpDuyet.Hide();

        }
        private void Search(int page)
        {
            //  rdTrangThai.SelectedValue =="1" : chưa duyệt
            //  rdTrangThai.SelectedValue =="2" : đã duyệt
            int isDuyet = rdTrangThai.SelectedValue == "1" ? 1 : 2;
            System.Data.Linq.ISingleResult<sp_NhapKhoVatLieu_ListDuyetResult> dt = dm.sp_NhapKhoVatLieu_ListDuyet(GetDrop(dlChiNhanh), 1, PageNumber, page);
            GV.DataSource = dt;
            GV.DataBind();

        }
        protected void btnXem_OnClick(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void grdDanhSachHD_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;
            GV.DataSource = null;
            GV.DataSource = dm.sp_NhapKhoVatLieu_LichSu(new Guid(ID));
            GV.DataBind();

            btnKhongDuyet.Visible = rdTrangThai.SelectedValue == "1" ? true : false;
            mpDuyet.Show();
        }

        private Guid GetDrop(DropDownList dl)
        {
            if (dl.SelectedValue.Trim().Equals(""))
            {
                return Guid.Empty;
            }
            else
            {
                return new Guid(dl.SelectedValue);
            }
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        protected void grdDanhSachHD_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#EEFFAA'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            hdPage.Value = "1";
            Search(CurrentPage);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }

        protected void btnDuyetTB_Click(object sender, EventArgs e)
        {
            tblNhapKhoVatLieu updatehd = (from p in db.tblNhapKhoVatLieus
                                          where p.ID == new Guid(hdID.Value)
                                          select p).FirstOrDefault();
            if (updatehd.TrangThai == 1)
            {
                #region [Duyệt]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    if (updatehd.SoPhieu == null)
                    {
                        var sophieu = (from p in db.tblNhapKhoVatLieus
                                       where p.IDChiNhanh == updatehd.IDChiNhanh
                                       orderby p.SoPhieu descending
                                       select p).FirstOrDefault();
                        if (sophieu != null && sophieu.ID != null)
                            updatehd.SoPhieu = sophieu.SoPhieu + 1;
                        else
                            updatehd.SoPhieu = 1;
                    }
                    updatehd.TrangThai = 2;
                    updatehd.TrangThaiText = "Đã duyệt";
                    db.SubmitChanges();
                    Success("Đã duyệt giá mua vật liệu");

                    //luu nhat trinh
                    var query = (from p in db.tblDinhMucNhienLieus
                                 where p.IDXe == updatehd.IDBienSoXe
                                 && p.TrangThai == 2
                                 select p).FirstOrDefault();

                    if (query != null && query.ID != null)
                    {
                        var queryNhaCungCap = (from p in db.tblNhaCungCaps
                                               where p.ID == updatehd.IDNhaCungCap
                                               select p).FirstOrDefault();
                        string noiDung = "Nhật trình sinh từ phiếu bán vật liệu";
                        if (queryNhaCungCap != null && queryNhaCungCap.ID != null)
                        {
                            noiDung = queryNhaCungCap.TenNhaCungCap;
                        }


                        //chi luu nhat trinh khi da khai bao dinh muc
                        var NhatTrinhXe = new tblNhatTrinhXe();
                        if (query.IDNhomThietBi.ToString() == "AB7BDA38-9DA8-4796-AD2C-F73081CC61C7")
                        {
                            string txtDMDiChuyenCoTaiBen = query.DiChuyenCoTaiBen.ToString();
                            string txtDMDiChuyenKhongTaiBen = query.DiChuyenKhongTaiBen.ToString();
                            string txtDMCauHaHang = query.CauHaHang.ToString();
                            //NhatTrinhXeDauKeo
                            NhatTrinhXe = new tblNhatTrinhXe()
                            {
                                ID = Guid.NewGuid(),
                                NgayThang = updatehd.NgayThang,
                                IDNhomThietBi = new Guid("AB7BDA38-9DA8-4796-AD2C-F73081CC61C7"),
                                IDChiNhanh = updatehd.IDChiNhanh,
                                IDXe = new Guid(updatehd.IDBienSoXe.ToString()),
                                NoiDung = noiDung,
                                GioChay = TimeSpan.Parse("00:00"),
                                IDPhieuBan = updatehd.ID,

                                DMDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen),
                                DMDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen),
                                DMCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang),

                                DMDiChuyenCoTaiBeTong = 0,
                                DMDiChuyenKhongTaiBeTong = 0,
                                DMLayBeTongTaiTram = 0,
                                DMXaBeTongVeSinh = 0,
                                DMNoMayQuayThung = 0,

                                DMDiChuyen = 0,
                                DMBomBeTong = 0,
                                DMVeSinhRaChan = 0,

                                DiChuyenCoTaiBen = updatehd.KMVanChuyen,
                                DiChuyenKhongTaiBen = updatehd.KMVanChuyen,
                                CauHaHang = 1,

                                DiChuyenCoTaiBeTong = 0,
                                DiChuyenKhongTaiBeTong = 0,
                                LayBeTongTaiTram = 0,
                                XaBeTongVeSinh = 0,
                                NoMayQuayThung = 0,

                                DiChuyen = 0,
                                BomBeTong = 0,
                                VeSinhRaChan = 0,

                                DauDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen) * updatehd.KMVanChuyen,
                                DauDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen) * updatehd.KMVanChuyen,
                                DauCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang) * 1,

                                DauDiChuyenCoTaiBeTong = 0,
                                DauDiChuyenKhongTaiBeTong = 0,
                                DauLayBeTongTaiTram = 0,
                                DauXaBeTongVeSinh = 0,
                                DauNoMayQuayThung = 0,

                                DauDiChuyen = 0,
                                DauBomBeTong = 0,
                                DauVeSinhRaChan = 0,

                                DuDauKy = 0,
                                DoDau = 0,
                                DuCuoiKy = 0,
                                KhoiLuong = 0,
                                ChuyenChay = 0,
                                BomCa = 0,
                                BomKhoi = 0,
                                Loai = "NhatTrinhXeDauKeo",
                                Mota = "Auto",

                                TrangThai = 2,
                                TrangThaiText = "Đã duyệt",
                                NguoiDuyet = Session["IDND"].ToString(),
                                NguoiTao = Session["IDND"].ToString(),
                                NgayTao = DateTime.Now
                            };
                            db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                            db.SubmitChanges();
                        }
                        else if (query.IDNhomThietBi.ToString() == "6810B3FB-B192-449D-B9F9-3F2231A8145C")
                        {
                            string txtDMDiChuyenCoTaiBen = query.DiChuyenCoTaiBen.ToString();
                            string txtDMDiChuyenKhongTaiBen = query.DiChuyenKhongTaiBen.ToString();
                            string txtDMCauHaHang = query.CauHaHang.ToString();
                            //NhatTrinhXeBen
                            NhatTrinhXe = new tblNhatTrinhXe()
                            {
                                ID = Guid.NewGuid(),
                                NgayThang = updatehd.NgayThang,
                                IDNhomThietBi = query.IDNhomThietBi,
                                IDChiNhanh = updatehd.IDChiNhanh,
                                IDXe = new Guid(updatehd.IDBienSoXe.ToString()),
                                NoiDung = noiDung,
                                GioChay = TimeSpan.Parse("00:00"),
                                IDPhieuBan = updatehd.ID,

                                DMDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen),
                                DMDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen),
                                DMCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang),


                                DMDiChuyenCoTaiBeTong = 0,
                                DMDiChuyenKhongTaiBeTong = 0,
                                DMLayBeTongTaiTram = 0,
                                DMXaBeTongVeSinh = 0,
                                DMNoMayQuayThung = 0,

                                DMDiChuyen = 0,
                                DMBomBeTong = 0,
                                DMVeSinhRaChan = 0,

                                DiChuyenCoTaiBen = updatehd.KMVanChuyen,
                                DiChuyenKhongTaiBen = updatehd.KMVanChuyen,
                                CauHaHang = updatehd.KMVanChuyen * 2,

                                DiChuyenCoTaiBeTong = 0,
                                DiChuyenKhongTaiBeTong = 0,
                                LayBeTongTaiTram = 0,
                                XaBeTongVeSinh = 0,
                                NoMayQuayThung = 0,

                                DiChuyen = 0,
                                BomBeTong = 0,
                                VeSinhRaChan = 0,

                                DauDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen) * updatehd.KMVanChuyen,
                                DauDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen) * updatehd.KMVanChuyen,
                                DauCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang) * 1,

                                DauDiChuyenCoTaiBeTong = 0,
                                DauDiChuyenKhongTaiBeTong = 0,
                                DauLayBeTongTaiTram = 0,
                                DauXaBeTongVeSinh = 0,
                                DauNoMayQuayThung = 0,

                                DauDiChuyen = 0,
                                DauBomBeTong = 0,
                                DauVeSinhRaChan = 0,

                                DuDauKy = 0,
                                DoDau = 0,
                                DuCuoiKy = 0,
                                KhoiLuong = 0,
                                ChuyenChay = 0,
                                BomCa = 0,
                                BomKhoi = 0,
                                Loai = "NhatTrinhXeBen",
                                Mota = "Auto",

                                TrangThai = 2,
                                TrangThaiText = "Đã duyệt",
                                NguoiDuyet = Session["IDND"].ToString(),
                                NguoiTao = Session["IDND"].ToString(),
                                NgayTao = DateTime.Now
                            };
                            db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                            db.SubmitChanges();
                        }


                    }

                }
                Search(1);
                #endregion
            }
            // Duyệt xóa
            else if (updatehd.TrangThai == 3)
            {
                #region [Duyệt xóa]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    db.tblNhapKhoVatLieus.DeleteOnSubmit(updatehd);
                    db.SubmitChanges();
                    Success("Đã duyệt xóa giá mua vật liệu.");
                    Search(1);
                }
                #endregion [Duyệt xóa]
            }
            else
            {

            }
        }

        protected void btnKDuyetTB_Click(object sender, EventArgs e)
        {
            #region [Không duyệt]
            var query = (from p in db.tblNhapKhoVatLieus
                         where p.ID == new Guid(hdID.Value)
                         select p).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                query.TrangThaiText = query.TrangThai == 1 ? "Không duyệt" : "Không duyệt xóa";
                query.MoTa = txtMota.Text;
                db.SubmitChanges();
                Success("Không duyệt thành công");
                Search(int.Parse(hdPage.Value));
            }
            #endregion [Không duyệt]
        }
        protected void rdDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Xem";
        }
        protected void rdKhongDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Duyệt";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;

            if (e.CommandName.Equals("Sua"))
            {
                //System.Data.Linq.ISingleResult<sp_NhapKhoVatLieu_LichSuResult> item = dm.sp_NhapKhoVatLieu_LichSu(new Guid(ID));
                //gvDuyet.DataSource = item;
                //gvDuyet.DataBind();
                //btnDuyet.Visible = rdTrangThai.SelectedItem.Value == "1" ? true : false;
                //btnKhongDuyet.Visible = rdTrangThai.SelectedItem.Value == "1" ? true : false;
                //mpDuyet.Show();
                var updatehd = (from p in db.tblNhapKhoVatLieus
                             where p.ID == new Guid(ID)
                             select p).FirstOrDefault();
                if (updatehd != null && updatehd.ID != null)
                {
                    if (updatehd.TrangThai == 1)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            if (updatehd.SoPhieu == null)
                            {
                                var getsophieu = (from p in db.tblNhapKhoVatLieus
                                                  where p.SoPhieu != null
                                                  orderby p.SoPhieu descending
                                                  select p).FirstOrDefault();
                                if (getsophieu != null && getsophieu.ID != null)
                                {
                                    updatehd.SoPhieu = getsophieu.SoPhieu + 1;
                                }
                                else
                                {
                                    updatehd.SoPhieu = 1;
                                }
                            }
                            updatehd.NguoiDuyet = Session["IDND"].ToString();
                            updatehd.TrangThai = 2;
                            updatehd.TrangThaiText = "Đã duyệt";
                            db.SubmitChanges();
                            Success("Đã duyệt");

                            //luu nhat trinh
                            var query = (from p in db.tblDinhMucNhienLieus
                                         where p.IDXe == updatehd.IDBienSoXe
                                         && p.TrangThai == 2
                                         select p).FirstOrDefault();

                            if (query != null && query.ID != null)
                            {
                                var queryNhaCungCap = (from p in db.tblNhaCungCaps
                                                       where p.ID == updatehd.IDNhaCungCap
                                                       select p).FirstOrDefault();
                                string noiDung = "Nhật trình sinh từ phiếu bán vật liệu";
                                if (queryNhaCungCap != null && queryNhaCungCap.ID != null)
                                {
                                    noiDung = queryNhaCungCap.TenNhaCungCap;
                                }


                                //chi luu nhat trinh khi da khai bao dinh muc
                                var NhatTrinhXe = new tblNhatTrinhXe();
                                if (query.IDNhomThietBi.ToString().ToUpper() == "AB7BDA38-9DA8-4796-AD2C-F73081CC61C7")
                                {
                                    string txtDMDiChuyenCoTaiBen = query.DiChuyenCoTaiBen.ToString();
                                    string txtDMDiChuyenKhongTaiBen = query.DiChuyenKhongTaiBen.ToString();
                                    string txtDMCauHaHang = query.CauHaHang.ToString();
                                    //NhatTrinhXeDauKeo
                                    NhatTrinhXe = new tblNhatTrinhXe()
                                    {
                                        ID = Guid.NewGuid(),
                                        NgayThang = updatehd.NgayThang,
                                        IDNhomThietBi = new Guid("AB7BDA38-9DA8-4796-AD2C-F73081CC61C7"),
                                        IDChiNhanh = updatehd.IDChiNhanh,
                                        IDXe = new Guid(updatehd.IDBienSoXe.ToString()),
                                        NoiDung = noiDung,
                                        GioChay = TimeSpan.Parse("00:00"),
                                        IDPhieuBan = updatehd.ID,

                                        DMDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen),
                                        DMDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen),
                                        DMCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang),

                                        DMDiChuyenCoTaiBeTong = 0,
                                        DMDiChuyenKhongTaiBeTong = 0,
                                        DMLayBeTongTaiTram = 0,
                                        DMXaBeTongVeSinh = 0,
                                        DMNoMayQuayThung = 0,

                                        DMDiChuyen = 0,
                                        DMBomBeTong = 0,
                                        DMVeSinhRaChan = 0,

                                        DiChuyenCoTaiBen = updatehd.KMVanChuyen,
                                        DiChuyenKhongTaiBen = updatehd.KMVanChuyen,
                                        CauHaHang = 1,

                                        DiChuyenCoTaiBeTong = 0,
                                        DiChuyenKhongTaiBeTong = 0,
                                        LayBeTongTaiTram = 0,
                                        XaBeTongVeSinh = 0,
                                        NoMayQuayThung = 0,

                                        DiChuyen = 0,
                                        BomBeTong = 0,
                                        VeSinhRaChan = 0,

                                        DauDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen) * updatehd.KMVanChuyen,
                                        DauDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen) * updatehd.KMVanChuyen,
                                        DauCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang) * 1,

                                        DauDiChuyenCoTaiBeTong = 0,
                                        DauDiChuyenKhongTaiBeTong = 0,
                                        DauLayBeTongTaiTram = 0,
                                        DauXaBeTongVeSinh = 0,
                                        DauNoMayQuayThung = 0,

                                        DauDiChuyen = 0,
                                        DauBomBeTong = 0,
                                        DauVeSinhRaChan = 0,

                                        DuDauKy = 0,
                                        DoDau = 0,
                                        DuCuoiKy = 0,
                                        KhoiLuong = 0,
                                        ChuyenChay = 0,
                                        BomCa = 0,
                                        BomKhoi = 0,
                                        Loai = "NhatTrinhXeDauKeo",
                                        Mota = "Auto",

                                        TrangThai = 2,
                                        TrangThaiText = "Đã duyệt",
                                        NguoiDuyet = Session["IDND"].ToString(),
                                        NguoiTao = Session["IDND"].ToString(),
                                        NgayTao = DateTime.Now
                                    };
                                    db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                                    db.SubmitChanges();
                                }
                                else if (query.IDNhomThietBi.ToString().ToUpper() == "6810B3FB-B192-449D-B9F9-3F2231A8145C")
                                {
                                    string txtDMDiChuyenCoTaiBen = query.DiChuyenCoTaiBen.ToString();
                                    string txtDMDiChuyenKhongTaiBen = query.DiChuyenKhongTaiBen.ToString();
                                    string txtDMCauHaHang = query.CauHaHang.ToString();
                                    //NhatTrinhXeBen
                                    NhatTrinhXe = new tblNhatTrinhXe()
                                    {
                                        ID = Guid.NewGuid(),
                                        NgayThang = updatehd.NgayThang,
                                        IDNhomThietBi = query.IDNhomThietBi,
                                        IDChiNhanh = updatehd.IDChiNhanh,
                                        IDXe = new Guid(updatehd.IDBienSoXe.ToString()),
                                        NoiDung = noiDung,
                                        GioChay = TimeSpan.Parse("00:00"),
                                        IDPhieuBan = updatehd.ID,

                                        DMDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen),
                                        DMDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen),
                                        DMCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang),


                                        DMDiChuyenCoTaiBeTong = 0,
                                        DMDiChuyenKhongTaiBeTong = 0,
                                        DMLayBeTongTaiTram = 0,
                                        DMXaBeTongVeSinh = 0,
                                        DMNoMayQuayThung = 0,

                                        DMDiChuyen = 0,
                                        DMBomBeTong = 0,
                                        DMVeSinhRaChan = 0,

                                        DiChuyenCoTaiBen = updatehd.KMVanChuyen,
                                        DiChuyenKhongTaiBen = updatehd.KMVanChuyen,
                                        CauHaHang = updatehd.KMVanChuyen * 2,

                                        DiChuyenCoTaiBeTong = 0,
                                        DiChuyenKhongTaiBeTong = 0,
                                        LayBeTongTaiTram = 0,
                                        XaBeTongVeSinh = 0,
                                        NoMayQuayThung = 0,

                                        DiChuyen = 0,
                                        BomBeTong = 0,
                                        VeSinhRaChan = 0,

                                        DauDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen) * updatehd.KMVanChuyen,
                                        DauDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen) * updatehd.KMVanChuyen,
                                        DauCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang) * 1,

                                        DauDiChuyenCoTaiBeTong = 0,
                                        DauDiChuyenKhongTaiBeTong = 0,
                                        DauLayBeTongTaiTram = 0,
                                        DauXaBeTongVeSinh = 0,
                                        DauNoMayQuayThung = 0,

                                        DauDiChuyen = 0,
                                        DauBomBeTong = 0,
                                        DauVeSinhRaChan = 0,

                                        DuDauKy = 0,
                                        DoDau = 0,
                                        DuCuoiKy = 0,
                                        KhoiLuong = 0,
                                        ChuyenChay = 0,
                                        BomCa = 0,
                                        BomKhoi = 0,
                                        Loai = "NhatTrinhXeBen",
                                        Mota = "Auto",

                                        TrangThai = 2,
                                        TrangThaiText = "Đã duyệt",
                                        NguoiDuyet = Session["IDND"].ToString(),
                                        NguoiTao = Session["IDND"].ToString(),
                                        NgayTao = DateTime.Now
                                    };
                                    db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                                    db.SubmitChanges();
                                }


                            }

                            
                            Search(int.Parse(hdPage.Value));
                        }
                    }
                    else if (updatehd.TrangThai == 3)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Inverted(thongbao);
                            }
                        }
                        else
                        {
                            db.tblNhapKhoVatLieus.DeleteOnSubmit(updatehd);
                            db.SubmitChanges();
                            Success("Đã duyệt xóa");
                            Search(int.Parse(hdPage.Value));
                        }
                    }
                }
                else
                {
                    Search(int.Parse(hdPage.Value));
                }
            }
            else if (e.CommandName == "Huy")
            {
                hdID.Value = ID;
                mpDuyet.Show();
                txtMota.Text = "";
            }
            else if (e.CommandName == "Xem")
            {
                var view = dm.sp_NhapKhoVatLieu_LichSu(new Guid(ID));
                gvLichSu.DataSource = view;
                gvLichSu.DataBind();
                mpLichSu.Show();
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
    }
}