﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class NguoiDung : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    IQueryable<tblUserAccount> setup = from x in db.tblUserAccounts
                                                       where x.UserName.ToUpper() == Session["IDND"].ToString()
                                                       && x.LoaiTaiKhoan == 2
                                                       //&& x.IDChiNhanh == new Guid(Session["IDDN"].ToString())
                                                       select x;
                    if (setup.Count() == 0)
                    {
                        GstGetMess("Bạn không có quyền truy cập trang này.", "default.aspx");
                    }
                    else
                    {
                        hdPage.Value = "1";

                    }
                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(1);
        }
        string Mahoa(string password)
        {
            MD5 mh = MD5.Create();
            //Chuyển kiểu chuổi thành kiểu byte
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(password);
            //mã hóa chuỗi đã chuyển
            byte[] hash = mh.ComputeHash(inputBytes);
            //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hdID.Value == "")
            {
                var checktontai = (from p in db.tblUserAccounts
                                   where p.UserName.ToLower() == txtUserName.Text.Trim().ToLower()
                                   select p).Count();
                if (checktontai > 0)
                {
                    Warning("Thông tin tài khoản đã tồn tại.");
                }
                else
                {
                    var useraccount = new tblUserAccount()
                    {
                        UserName = txtUserName.Text.Trim().ToLower(),
                        Password = Mahoa(txtPassword.Text.Trim()),
                        BirthDay = DateTime.Parse(GetNgayThang(txtNgaySinh.Text)),
                        Email = txtEmail.Text.Trim().ToLower(),
                        PhoneNumber = txtPhone.Text.Trim(),
                        TrangThai = true,
                        TrangThaiText = "Đã kích hoạt",
                        LoaiTaiKhoan = 1,
                        NguoiTao = Session["IDND"].ToString(),
                        NgayTao = DateTime.Now,
                        SessionLogin = ""
                        //NguoiTao =  ,
                        //CauHoi1 =  ,
                        //TraLoi1 =  ,
                        //CauHoi2 =  ,
                        //TraLoi2 =  ,
                        //CauHoi3 =  ,
                        //TraLoi3 =
                    };
                    db.tblUserAccounts.InsertOnSubmit(useraccount);
                    db.SubmitChanges();
                    btnCancel_Click(sender, e);
                    Search(1);
                    Success("Lưu thành công.");
                }
            }
            else
            {
                var checktontai = (from p in db.tblUserAccounts
                                   where p.UserName.ToLower() == txtUserName.Text.Trim().ToLower()
                                   && p.UserName != hdID.Value
                                   select p).Count();
                if (checktontai > 0)
                {
                    Warning("Thông tin tài khoản đã tồn tại.");
                }
                else
                {
                    var query = (from p in db.tblUserAccounts
                                 where p.UserName == hdID.Value
                                 select p).FirstOrDefault();
                    if (query != null && query.UserName != null)
                    {
                        query.BirthDay = DateTime.Parse(GetNgayThang(txtNgaySinh.Text));
                        query.Email = txtEmail.Text.Trim().ToLower();
                        query.PhoneNumber = txtPhone.Text.Trim();
                        //query.TrangThai = false;
                        //query.TrangThaiText = "Chờ duyệt";
                        //query.TraLoi1
                        db.SubmitChanges();
                        btnCancel_Click(sender, e);
                        Search(1);
                        Success("Sửa thành công");
                    }
                    else
                    {
                        Warning("Thông tin tài khoản không tồn tại.");
                        btnCancel_Click(sender, e);
                    }
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtUserName.Text = "";
            txtUserName.ReadOnly = false;
            txtEmail.Text = "";
            txtNgaySinh.Text = "";
            txtPassword.Text = "";
            txtPhone.Text = "";
            txtSearch.Text = "";
            btnSave.Text = "Lưu";
            hdID.Value = "";
        }
        void Search(int page)
        {
            GV.DataSource = db.sp_UserAccount_Search(txtSearch.Text.Trim(), 10, page);
            GV.DataBind();
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblUserAccounts
                         where p.UserName == id
                         select p).FirstOrDefault();

            if (query != null && query.UserName != null)
            {
                if (e.CommandName == "Sua")
                {
                    txtUserName.Text = query.UserName;
                    txtEmail.Text = query.Email;
                    txtNgaySinh.Text = query.BirthDay.Value.ToString("dd/MM/yyyy");
                    txtPhone.Text = query.PhoneNumber;
                    hdID.Value = query.UserName;
                    txtUserName.ReadOnly = true;
                    btnSave.Text = "Cập nhật";
                }
                else if (e.CommandName == "Xoa")
                {
                    var phanquyen = from p in db.tblPhanQuyens
                                    where p.UserName == query.UserName
                                    select p;
                    db.tblPhanQuyens.DeleteAllOnSubmit(phanquyen);

                    var phanquyencaidat = from p in db.tblPhanQuyenCaiDats
                                          where p.UserName == query.UserName
                                          select p;
                    db.tblPhanQuyenCaiDats.DeleteAllOnSubmit(phanquyencaidat);

                    db.tblUserAccounts.DeleteOnSubmit(query);
                    db.SubmitChanges();
                    Success("Xóa thành công");
                    btnCancel_Click(sender, e);
                    Search(1);
                }
                else if (e.CommandName == "Kích hoạt")
                {
                    if (query.TrangThai == false)
                    {
                        query.TrangThai = true;
                        query.TrangThaiText = "Đã kích hoạt";
                        db.SubmitChanges();
                        Success("Đã kích hoạt");
                    }
                    else
                    {
                        Success("Đã kích hoạt");
                    }
                    Search(1);
                }
                else if (e.CommandName == "Khóa")
                {
                    if (query.TrangThai == true)
                    {
                        query.TrangThai = false;
                        query.TrangThaiText = "Đã khóa";
                        db.SubmitChanges();
                        Success("Đã khóa");
                    }
                    else
                    {
                        Success("Đã khóa");
                    }
                    Search(1);
                }
            }
            else
            {
                Warning("Thông tin người dùng không tồn tại");
                btnCancel_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            Search(1);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            int CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            int CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }

        protected void GV_PreRender(object sender, EventArgs e)
        {
            GridView gv = (GridView)sender;

            if ((gv.ShowHeader == true && gv.Rows.Count > 0)
                || (gv.ShowHeaderWhenEmpty == true))
            {
                //Force GridView to use <thead> instead of <tbody> - 11/03/2013 - MCR.
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            if (gv.ShowFooter == true && gv.Rows.Count > 0)
            {
                //Force GridView to use <tfoot> instead of <tbody> - 11/03/2013 - MCR.
                gv.FooterRow.TableSection = TableRowSection.TableFooter;
            }
        }
    }
}