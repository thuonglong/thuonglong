﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class QuanLyNhienLieu : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        ThietBiDataContext thietbi = new ThietBiDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmQuanLyNhienLieu";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmQuanLyNhienLieu", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadThietBi();
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        txtNgayThang.Text = dateTimenow.ToString("dd/MM/yyyy");
                        LoadThietBiSearch();
                        hdPage.Value = "1";
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            //dlChiNhanhChot.DataSource = query;
            //dlChiNhanhChot.DataBind();
            //dlChiNhanhChot.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadThietBi()
        {
            var query = (from p in db.tblDinhMucNhienLieus
                         join q in db.tblXeVanChuyens on p.IDXe equals q.ID
                         where
                         //p.IDNhomThietBi == new Guid(hdNhomThietBi.Value)
                         //&& 
                         q.TrangThai == 2
                         select new
                         {
                             ID = p.IDXe,
                             Ten = q.TenThietBi
                         }).Distinct().OrderBy(p => p.Ten);
            dlXe.DataSource = query;
            dlXe.DataBind();
            dlXe.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlXe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlXe.SelectedValue != "")
            {
                LoadDinhMuc();
                var query = (from p in db.tblNguoiVanHanhs
                             join q in db.tblNhanSus on p.IDNhanVien equals q.ID
                             where p.IDThietBi == new Guid(dlXe.SelectedValue)
                             select q
                ).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    txtLaiXe.Text = query.TenNhanVien;

                }
            }
        }
        void LoadDinhMuc()
        {
            var query = (from p in db.tblDinhMucNhienLieus
                         where p.IDXe == GetDrop(dlXe)
                         && p.TrangThai == 2
                         select p).FirstOrDefault();
            if (query != null && query.ID != null)
            {

                txtDuDauKy.Text = "";
                txtDoDau.Text = "";
                txtDuCuoiKy.Text = "";
                txtChuyenChay.Text = "";
                LoadLoaiVatLieu(query.IDLoaiNhienLieu.ToString());
            }
            else
            {

                txtDuDauKy.Text = "";
                txtDoDau.Text = "";
                txtDuCuoiKy.Text = "";
                txtChuyenChay.Text = "";

                dlXe.SelectedValue = "";
                Warning("Bạn chưa thiết lập định mức cho xe này");
            }
        }

        protected void LoadThietBiSearch()
        {
            dlThietBiSearch.Items.Clear();
            var query = (from p in db.tblDinhMucNhienLieus
                         join q in db.tblXeVanChuyens on p.IDXe equals q.ID
                         where p.IDNhomThietBi == new Guid(hdNhomThietBi.Value)
                         select new
                         {
                             ID = p.IDXe,
                             Ten = q.TenThietBi
                         }).Distinct().OrderBy(p => p.Ten);
            dlThietBiSearch.DataSource = query;
            dlThietBiSearch.DataBind();
            dlThietBiSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlXe.SelectedValue == "")
            {
                s += " - Chọn biển số xe<br />";
            }

            if (txtNgayThang.Text == "" && valDate(txtNgayThang.Text) == true)
            {
                s += "- Nhập ngày tháng<br/>";
            }
            if (txtNoiDung.Text == "")
            {
                s += "- Nhập nội dung<br/>";
            }

            if (s == "")
            {
                if (hdID.Value == "")
                {
                    int query = (from p in db.tblDinhMucNhienLieus
                                 where p.IDXe == GetDrop(dlXe)
                                 select p).Count();
                    if (query == 0)
                        s += "Bạn phải thiết lập định mức cho xe trước khi nhập nhiên liệu xe.";
                }
                else
                {
                    var querycheck = (from p in db.tblNhatTrinhXes
                                      where p.ID == new Guid(hdID.Value)
                                      select p).FirstOrDefault();
                    if (querycheck != null && querycheck.IDXe != null && querycheck.IDXe != GetDrop(dlXe))
                    {
                        int query = (from p in db.tblNhatTrinhXes
                                     where p.IDXe == GetDrop(dlXe)
                                     && p.ID != new Guid(hdID.Value)
                                     select p).Count();
                        if (query > 0)
                            s += "Đã thiết lập định mức nhiên liệu cho xe " + dlXe.SelectedItem.Text.ToString() + "";
                    }
                }
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var NhatTrinhXe = new tblNhatTrinhXe()
                        {
                            ID = Guid.NewGuid(),
                            NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                            IDNhomThietBi = new Guid(hdNhomThietBi.Value),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDXe = GetDrop(dlXe),
                            NoiDung = txtNoiDung.Text.Trim(),

                            DMDiChuyenCoTaiBen = 0,
                            DMDiChuyenKhongTaiBen = 0,
                            DMCauHaHang = 0,

                            DMDiChuyenCoTaiBeTong = 0,
                            DMDiChuyenKhongTaiBeTong = 0,
                            DMLayBeTongTaiTram = 0,
                            DMXaBeTongVeSinh = 0,
                            DMNoMayQuayThung = 0,

                            DMDiChuyen = 0,
                            DMBomBeTong = 0,
                            DMVeSinhRaChan = 0,

                            DiChuyenCoTaiBen = 0,
                            DiChuyenKhongTaiBen = 0,
                            CauHaHang = 0,

                            DiChuyenCoTaiBeTong = 0,
                            DiChuyenKhongTaiBeTong = 0,
                            LayBeTongTaiTram = 0,
                            XaBeTongVeSinh = 0,
                            NoMayQuayThung = 0,

                            DiChuyen = 0,
                            BomBeTong = 0,
                            VeSinhRaChan = 0,

                            DauDiChuyenCoTaiBen = 0,
                            DauDiChuyenKhongTaiBen = 0,
                            DauCauHaHang = 0,

                            DauDiChuyenCoTaiBeTong = 0,
                            DauDiChuyenKhongTaiBeTong = 0,
                            DauLayBeTongTaiTram = 0,
                            DauXaBeTongVeSinh = 0,
                            DauNoMayQuayThung = 0,

                            DauDiChuyen = 0,
                            DauBomBeTong = 0,
                            DauVeSinhRaChan = 0,

                            DuDauKy = txtDuDauKy.Text == "" ? 0 : double.Parse(txtDuDauKy.Text),
                            DoDau = txtDoDau.Text == "" ? 0 : double.Parse(txtDoDau.Text),
                            DuCuoiKy = txtDuCuoiKy.Text == "" ? 0 : double.Parse(txtDuCuoiKy.Text),
                            KhoiLuong = 0,
                            ChuyenChay = txtChuyenChay.Text == "" ? 0 : double.Parse(txtChuyenChay.Text),
                            BomCa = 0,
                            BomKhoi = 0,
                            Loai = "CapDau",

                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblNhatTrinhXes
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.IDNhomThietBi = new Guid(hdNhomThietBi.Value);
                            query.IDXe = GetDrop(dlXe);
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.NoiDung = txtNoiDung.Text.Trim();

                            query.DMDiChuyenCoTaiBen = 0;
                            query.DMDiChuyenKhongTaiBen = 0;
                            query.DMCauHaHang = 0;

                            query.DiChuyenCoTaiBen = 0;
                            query.DiChuyenKhongTaiBen = 0;
                            query.CauHaHang = 0;

                            query.DauDiChuyenCoTaiBen = 0;
                            query.DauDiChuyenKhongTaiBen = 0;
                            query.DauCauHaHang = 0;

                            query.DuDauKy = txtDuDauKy.Text == "" ? 0 : double.Parse(txtDuDauKy.Text);
                            query.DoDau = txtDoDau.Text == "" ? 0 : double.Parse(txtDoDau.Text);
                            query.DuCuoiKy = txtDuCuoiKy.Text == "" ? 0 : double.Parse(txtDuCuoiKy.Text);
                            query.ChuyenChay = txtChuyenChay.Text == "" ? 0 : double.Parse(txtChuyenChay.Text);

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin nhiên liệu đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlXe.SelectedValue = "";
            dlXe_SelectedIndexChanged(sender, e);
            txtNoiDung.Text = "";


            txtDuDauKy.Text = "";
            txtDoDau.Text = "";
            txtDuCuoiKy.Text = "";
            txtChuyenChay.Text = "";

            LoadThietBiSearch();

            hdID.Value = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblNhatTrinhXes
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlXe.SelectedValue = query.IDXe.ToString();
                        txtNoiDung.Text = query.NoiDung.ToString();

                        txtDuDauKy.Text = query.DuDauKy.ToString();
                        txtDoDau.Text = query.DoDau.ToString();
                        txtDuCuoiKy.Text = query.DuCuoiKy.ToString();
                        txtChuyenChay.Text = query.ChuyenChay.ToString();

                        hdID.Value = id;
                        //btnSave.Text = "Cập nhật";

                        var queryNV = (from p in db.tblNguoiVanHanhs
                                       join q in db.tblNhanSus on p.IDNhanVien equals q.ID
                                       where p.IDThietBi == new Guid(dlXe.SelectedValue)
                                       select q
                        ).FirstOrDefault();
                        if (queryNV != null && queryNV.ID != null)
                        {
                            txtLaiXe.Text = queryNV.TenNhanVien;

                        }
                        LoadLoaiVatLieuEdit(query.IDXe.ToString());
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai != 3)
                        {
                            query.TrangThai = 3;
                            query.NguoiXoa = Session["IDND"].ToString();
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                        }
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = thietbi.sp_NhatTrinhXe_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin nhiên liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = thietbi.sp_QuanLyNhienLieu_Search(GetDrop(dlChiNhanhSearch), dlThietBiSearch.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
            if (GV.Rows.Count > 0)
            {
                /*
                var getfooter = thietbi.sp_QuanLyNhienLieu_GetFooter(GetDrop(dlChiNhanhSearch), dlThietBiSearch.SelectedValue,
                    DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();
                if (getfooter != null && getfooter.DuCuoiKy != null)
                {
                    GV.FooterRow.Cells[0].ColumnSpan = 7;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", getfooter.SoLuong);
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;
                    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", getfooter.DuDauKy);
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", getfooter.DoDau);
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", getfooter.DuCuoiKy);
                    GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", getfooter.ChuyenChay);

                    //GV.FooterRow.Cells[11].Visible = false;
                    //GV.FooterRow.Cells[12].Visible = false;
                    //GV.FooterRow.Cells[13].Visible = false;
                    //GV.FooterRow.Cells[14].Visible = false;
                    //GV.FooterRow.Cells[15].Visible = false;
                    //GV.FooterRow.Cells[16].Visible = false;
                }
                */
            }
        }

        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Biến số xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);
                headerCell = new TableCell
                {
                    Text = "Lái xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);
                headerCell = new TableCell
                {
                    Text = "Loại nhiên liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Nội dung",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin cấp dầu",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.DarkCyan,
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadThietBiSearch();
        }
        void LoadLoaiVatLieu(string idLoaiNhienLieu)
        {
            var query = (from p in db.tblLoaiVatLieus
                         where p.ID == new Guid(idLoaiNhienLieu)
                         select p
                         ).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                txtLoaiNhienLieu.Text = query.TenLoaiVatLieu;

            }

        }

        void LoadLoaiVatLieuEdit(string idxe)
        {
            var query = (from p in db.tblDinhMucNhienLieus
                         join q in db.tblLoaiVatLieus on p.IDLoaiNhienLieu equals q.ID
                         where p.IDXe == new Guid(idxe)
                         && p.TrangThai == 2
                         select q).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                txtLoaiNhienLieu.Text = query.TenLoaiVatLieu;

            }

        }
    }
}