﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ThuongLong._Default" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <!-- plugins:css -->
    <link rel="stylesheet" href="theme/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="theme/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="theme/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="theme/css/custom_style.css">

    <script src="Scripts/jquery-3.4.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/Chart.min.js"></script>

    <script type="text/javascript" src="theme/vendors/daterangerpicker/moment.min.js"></script>
    <script type="text/javascript" src="theme/vendors/daterangerpicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="theme/vendors/daterangerpicker/daterangepicker.css" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="container-fluid">
               <div id="overlay">
                  <div class="loader"></div>
              </div>
                <div class="row pt-3 bg-white mb-2" runat="server" visible="true">
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 form-group">
                        <asp:DropDownList CssClass="form-control select2" Width="100%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Height="32px"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh"
                            onchange="selectChiNhanh(this)"
                                runat="server">
                            </asp:DropDownList>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 form-group">
                                <input type="button" id="btnSearch" class="btn btn-primary" value="Tìm kiếm" onclick="search()"></input>
                            </div>
                </div>
                <div class="row" id="divLoadForm">
                    <asp:Literal ID="Literal1" runat="server"  visible="false"></asp:Literal>
                </div>

                <div class="row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <a href="/baocaothuchi" class="w-100 text-black">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-cube text-danger icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right">Tổng thu trong ngày</p>
                                            <div class="fluid-container">
                                                <h3 class="font-weight-medium text-right mb-0" id="lbTongThu" runat="server">1230.002032.000</h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <a href="/baocaothuchi" class="w-100 text-black">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-receipt text-warning icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right">Tổng chi trong ngày</p>
                                            <div class="fluid-container">
                                                <h3 class="font-weight-medium text-right mb-0" id="lbTongChi" runat="server">3455</h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <a href="/baocaocongnothu" class="w-100 text-black">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-archive-arrow-down text-success icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right">Công nợ phải thu trong ngày</p>
                                            <div class="fluid-container">
                                                <h3 class="font-weight-medium text-right mb-0" id="lbCongNoThu" runat="server">5693</h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <a href="/baocaocongnotra" class="w-100 text-black">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-archive-arrow-up text-info icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right">Công nợ phải trả trong ngày</p>
                                            <div class="fluid-container">
                                                <h3 class="font-weight-medium text-right mb-0" id="lbCongNoChi" runat="server">246</h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <%--<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
                        <div class="card card-revenue">
                            <div class="card-body">
                                <div class="clearfix">
                                    <canvas id="chartProgress_2"></canvas>
                                </div>
								<div class="number" style="
									position: absolute;
									top: 50%;
									left: 0;
									width: 100%;
									-webkit-transform: translateY(-50%);
									-ms-transform: translateY(-50%);
									transform: translateY(-50%);
									font-size: 1.56rem;
									text-align: center;
								">120000.00</div>
                            </div>
                        </div>
                    </div>--%>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
                        <a href="/baocaobanbetong" class="w-100 text-black">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <canvas id="chartKLBeTongBan"></canvas>
                                    </div>
                                    <h6 class="mt-3 text-center">Khối lượng bê tông đã bán</h6>
                                    <p class="text-muted mt-1 mb-0 text-center">
                                        Khối lượng bê tông đã bán trong ngày
                                    </p>
                                </div>

                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
                        <a href="/baocaotronbetong" class="w-100 text-black">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <canvas id="chartKLBeTongDaTron"></canvas>
                                    </div>
                                    <h6 class="mt-3 text-center">Khối lượng bê tông đã trộn</h6>
                                    <p class="text-muted mt-1 mb-0 text-center">
                                        Khối lượng bê tông đã trộn trong ngày
                                    </p>
                                </div>

                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
                        <a href="/lichxuatbetong" class="w-100 text-black">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <canvas id="chartKLBeTongDuKien"></canvas>
                                    </div>
                                    <h6 class="mt-3 text-center">Khối lượng bê tông dự kiến bán</h6>
                                    <p class="text-muted mt-1 mb-0 text-center">
                                        Khối lượng bê tông dự kiến bán trong ngày
                                    </p>
                                </div>

                            </div>
                        </a>
                    </div>
                    
                </div> 
                
                <div class="row" id="chartSanXuat">
                </div>

                <div class="row">
                    <div class="col-lg-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tình hình kinh doanh</h4>
                                <canvas id="barChart" style="height: 200px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Doanh thu trong tháng</h4>
                                <canvas id="doughnutChart" style="height: 350px"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="row pt-3 bg-white mb-2" runat="server" visible="true">
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 form-group">
                        <div id="reportrange" style="padding: 5px 10px; border: 1px solid #ccc">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down" style="float: right;"></i>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 form-group">
                        <asp:DropDownList CssClass="form-control select2" Width="100%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Height="32px"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="DropDownList1"
                            onchange="selectChiNhanh(this)"
                                runat="server">
                            </asp:DropDownList>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 form-group">
                                <input type="button" id="btnSearchMonth" class="btn btn-primary" value="Tìm kiếm" onclick="search()"></input>
                            </div>
                </div>

<%--                <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 stretch-card grid-margin">
                    <div class="card card-revenue-table">
                      <div class="card-body">
                        <h4 class="card-title">Sản xuất bê tông</h4>
                        <div class="revenue-item d-flex">
                          <div class="revenue-desc">
                            <h6>Khối lượng bê tông đã bán</h6>
                            <p class="font-weight-light"> Khối lượng bê tông đã bán trong ngày </p>
                          </div>
                          <div class="revenue-amount">
                            <p class="text-primary" id="lbKLBeTongBan" runat="server"> +168.900 </p>
                          </div>
                        </div>
                        <div class="revenue-item d-flex">
                          <div class="revenue-desc">
                            <h6>Khối lượng bê tông đã trộn</h6>
                            <p class="font-weight-light"> Khối lượng bê tông đã trộn trong ngày </p>
                          </div>
                          <div class="revenue-amount">
                            <p class="text-primary" id="lbKLBeTongDaTron" runat="server"> +6890.00 </p>
                          </div>
                        </div>                        
                        <div class="revenue-item d-flex">
                          <div class="revenue-desc">
                            <h6>Khối lượng bê tông dự kiến bán</h6>
                            <p class="font-weight-light"> Khối lượng bê tông dự kiến bán trong ngày </p>
                          </div>
                          <div class="revenue-amount">
                            <p class="text-primary" id="lbKLBeTongDuKien" runat="server"> +6890.00 </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    
                </div>
            --%>
            </div>
            <!-- /.container-fluid -->
           <%-- <script type="text/javascript">
                $(function () {
                    LoadChart();

                    var dlChiNhanh = document.getElementById("<%=dlChiNhanh.ClientID %>");

                    dlChiNhanh.bind("change", function () {
                        LoadChart();
                    });
                });
                function LoadChart() {
                    var dlChiNhanh = document.getElementById("<%=dlChiNhanh.ClientID %>");

                    $.ajax({
                        type: "POST",
                        url: "Default.aspx/GetChart",
                        data: "{country: '" + dlChiNhanh.Value + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            $("#dvChart").html("");
                            $("#dvLegend").html("");
                            var data = eval(r.d);
                            var el = document.createElement('canvas');
                            $("#dvChart")[0].appendChild(el);

                            //Fix for IE 8
                            if ($.browser.msie && $.browser.version == "8.0") {
                                G_vmlCanvasManager.initElement(el);
                            }
                            var ctx = el.getContext('2d');
                            var userStrengthsChart;
                            switch (chartType) {
                                case 1:
                                    userStrengthsChart = new Chart(ctx).Pie(data);
                                    break;
                                case 2:
                                    userStrengthsChart = new Chart(ctx).Doughnut(data);
                                    break;
                            }
                            for (var i = 0; i < data.length; i++) {
                                var div = $("<div />");
                                div.css("margin-bottom", "10px");
                                div.html("<span style = 'display:inline-block;height:10px;width:10px;background-color:" + data[i].color + "'></span> " + data[i].text);
                                $("#dvLegend").append(div);
                            }
                        },
                        failure: function (response) {
                            alert('There was an error.');
                        }
                    });
                }
            </script>--%>

            <script>

            </script>
        

            <script src="Scripts/dashboard/dashboard-init.js"></script>
            <script src="Scripts/dashboard/chart-daily.js"></script>
            <script src="Scripts/dashboard/chart-monthly.js"></script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
