﻿<%@ Page Title="Duyệt nhật trình xe" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetNhatTrinhXe.aspx.cs" Inherits="ThuongLong.DuyetNhatTrinhXe" %>

<%@ Register Src="UCNhatTrinhXe/uc_DuyetDinhMucXeBen.ascx" TagName="uc_DuyetDinhMucXeBen" TagPrefix="uc1" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetDinhMucXeBeTong.ascx" TagName="uc_DuyetDinhMucXeBeTong" TagPrefix="uc2" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetDinhMucXeBom.ascx" TagName="uc_DuyetDinhMucXeBom" TagPrefix="uc3" %>

<%@ Register Src="UCNhatTrinhXe/uc_DuyetDinhMucXeDauKeo.ascx" TagName="uc_DuyetDinhMucXeDauKeo" TagPrefix="uc7" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetDinhMucXeCau.ascx" TagName="uc_DuyetDinhMucXeCau" TagPrefix="uc8" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetDinhMucXeXucLat.ascx" TagName="uc_DuyetDinhMucXeXucLat" TagPrefix="uc9" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetDinhMucXeXucDao.ascx" TagName="uc_DuyetDinhMucXeXucDao" TagPrefix="uc10" %>

<%@ Register Src="UCNhatTrinhXe/uc_DuyetNhatTrinhXeBen.ascx" TagName="uc_DuyetNhatTrinhXeBen" TagPrefix="uc4" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetNhatTrinhXeBeTong.ascx" TagName="uc_DuyetNhatTrinhXeBeTong" TagPrefix="uc5" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetNhatTrinhXeBom.ascx" TagName="uc_DuyetNhatTrinhXeBom" TagPrefix="uc6" %>

<%@ Register Src="UCNhatTrinhXe/uc_DuyetNhatTrinhXeDauKeo.ascx" TagName="uc_DuyetNhatTrinhXeDauKeo" TagPrefix="uc11" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetNhatTrinhXeCau.ascx" TagName="uc_DuyetNhatTrinhXeCau" TagPrefix="uc12" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetNhatTrinhXeXucLat.ascx" TagName="uc_DuyetNhatTrinhXeXucLat" TagPrefix="uc13" %>
<%@ Register Src="UCNhatTrinhXe/uc_DuyetNhatTrinhXeXucDao.ascx" TagName="uc_DuyetNhatTrinhXeXucDao" TagPrefix="uc14" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">

                        <ajaxToolkit:TabPanel ID="frmDinhMucXeBen" runat="server" HeaderText="Định mức xe ben">
                            <ContentTemplate>
                                <uc1:uc_DuyetDinhMucXeBen ID="frmDinhMucXeBen1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Định mức xe đầu kéo">
                            <ContentTemplate>
                                <uc7:uc_DuyetDinhMucXeDauKeo ID="uc_DuyetDinhMucXeDauKeo1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Định mức xe xúc lật">
                            <ContentTemplate>
                                <uc9:uc_DuyetDinhMucXeXucLat ID="uc_DuyetDinhMucXeXucLat1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="Định mức xe xúc đào">
                            <ContentTemplate>
                                <uc10:uc_DuyetDinhMucXeXucDao ID="uc_DuyetDinhMucXeXucDao1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="uc_DuyetDinhMucXeBeTong" runat="server" HeaderText="Định mức xe bê tông">
                            <ContentTemplate>
                                <uc2:uc_DuyetDinhMucXeBeTong ID="frmDinhMucXeBeTong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="uc_DuyetDinhMucXeBom" runat="server" HeaderText="Định mức xe bơm">
                            <ContentTemplate>
                                <uc3:uc_DuyetDinhMucXeBom ID="uc_DuyetDinhMucXeBom1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanel4" runat="server" HeaderText="Định mức xe cẩu">
                            <ContentTemplate>
                                <uc8:uc_DuyetDinhMucXeCau ID="uc_DuyetDinhMucXeCau1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>


                        <ajaxToolkit:TabPanel ID="frmNhatTrinhXeBen" runat="server" HeaderText="Nhật trình xe ben">
                            <ContentTemplate>
                                <uc4:uc_DuyetNhatTrinhXeBen ID="frmNhatTrinhXeBen1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanel5" runat="server" HeaderText="Nhật trình xe đầu kéo">
                            <ContentTemplate>
                                <uc11:uc_DuyetNhatTrinhXeDauKeo ID="Uc_DuyetNhatTrinhXeDauKeo1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanel6" runat="server" HeaderText="Nhật trình xe xúc lật">
                            <ContentTemplate>
                                <uc13:uc_DuyetNhatTrinhXeXucLat ID="uc_DuyetNhatTrinhXeXucLat1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanel7" runat="server" HeaderText="Nhật trình xe xúc đào">
                            <ContentTemplate>
                                <uc14:uc_DuyetNhatTrinhXeXucDao ID="uc_DuyetNhatTrinhXeXucDao1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="uc_DuyetNhatTrinhXeBeTong" runat="server" HeaderText="Nhật trình xe bê tông">
                            <ContentTemplate>
                                <uc5:uc_DuyetNhatTrinhXeBeTong ID="frmNhatTrinhXeBeTong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="uc_DuyetNhatTrinhXeBom" runat="server" HeaderText="Nhật trình xe bơm">
                            <ContentTemplate>
                                <uc6:uc_DuyetNhatTrinhXeBom ID="uc_DuyetNhatTrinhXeBom1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanel8" runat="server" HeaderText="Nhật trình xe cẩu">
                            <ContentTemplate>
                                <uc12:uc_DuyetNhatTrinhXeCau ID="uc_DuyetNhatTrinhXeCau1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
