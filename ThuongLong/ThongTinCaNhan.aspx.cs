﻿using Microsoft.Ajax.Utilities;
using NPOI.HSSF.Record;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class ThongTinCaNhan : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    LoadDanhMucCauHoi();
                    LoadForm();
                }
            }
        }
        void LoadForm()
        {
            var query = (from p in db.tblUserAccounts
                         where p.UserName.ToUpper() == Session["IDND"].ToString().ToUpper()
                         select p).FirstOrDefault();
            if (query != null && query.UserName != null)
            {
                dlCauHoi1.SelectedValue = query.CauHoi1.ToString();
                dlCauHoi2.SelectedValue = query.CauHoi2.ToString();
                dlCauHoi3.SelectedValue = query.CauHoi3.ToString();
                txtCauTraLoi1.Text = query.TraLoi1;
                txtCauTraLoi2.Text = query.TraLoi2;
                txtCauTraLoi3.Text = query.TraLoi3;
                txtEmail.Text = query.Email;
            }
        }
        void LoadDanhMucCauHoi()
        {
            List<tblCauHoiBiMat> query = (from p in db.tblCauHoiBiMats
                                          orderby p.ID
                                          select p).ToList();
            dlCauHoi1.DataSource = query;
            dlCauHoi1.DataBind();
            dlCauHoi2.DataSource = query;
            dlCauHoi2.DataBind();
            dlCauHoi3.DataSource = query;
            dlCauHoi3.DataBind();
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }

        protected void lbtLuu_Click(object sender, EventArgs e)
        {
            if (dlCauHoi1.SelectedValue == "" || dlCauHoi2.SelectedValue =="" || dlCauHoi3.SelectedValue == "")
            {
                Warning("Bạn phải chọn câu hỏi bí mật");
            }
            else if (dlCauHoi1.SelectedValue == dlCauHoi2.SelectedValue || dlCauHoi1.SelectedValue == dlCauHoi3.SelectedValue || dlCauHoi2.SelectedValue == dlCauHoi3.SelectedValue)
            {
                Warning("Bạn phải chọn 3 câu hỏi khác nhau.");
            }
            else if (txtCauTraLoi1.Text.Trim() == "" || txtCauTraLoi2.Text.Trim() == "" || txtCauTraLoi3.Text.Trim() == "")
            {
                Warning("Bạn phải nhập câu trả lời cho câu hỏi.");
            }
            else if (txtEmail.Text.Trim() == "" )
            {
                Warning("Bạn phải nhập email.");
            }
            else
            {
                var query = (from p in db.tblUserAccounts
                             where p.UserName.ToUpper() == Session["IDND"].ToString().ToUpper()
                             select p).FirstOrDefault();
                if (query != null && query.UserName != null)
                {
                    query.CauHoi1 = dlCauHoi1.SelectedValue == "" ? (int?)null : int.Parse(dlCauHoi1.SelectedValue);
                    query.CauHoi2 = dlCauHoi2.SelectedValue == "" ? (int?)null : int.Parse(dlCauHoi2.SelectedValue);
                    query.CauHoi3 = dlCauHoi3.SelectedValue == "" ? (int?)null : int.Parse(dlCauHoi3.SelectedValue);

                    query.TraLoi1 = txtCauTraLoi1.Text.Trim();
                    query.TraLoi2 = txtCauTraLoi2.Text.Trim();
                    query.TraLoi3 = txtCauTraLoi3.Text.Trim();
                    query.Email = txtEmail.Text.Trim();

                    db.SubmitChanges();
                    Success("Lưu thành công");
                }
            }
        }

        protected void lbtDoiMatKhau_Click(object sender, EventArgs e)
        {
            var query = (from p in db.tblUserAccounts
                         where p.UserName.ToUpper() == Session["IDND"].ToString().ToUpper()
                         select p).FirstOrDefault();
            if (txtMatKhauHienTai.Text.Trim() == "")
            {
                Warning("Nhập mật khẩu hiện tại");
            }
            else if (txtMatKhauMoi.Text.Trim() == "")
            {
                Warning("Nhập mật khẩu mới");
            }
            else if (txtNhapLaiMatKhauMoi.Text.Trim() == "")
            {
                Warning("Nhập lại mật khẩu mới");
            }
            else if (Mahoa(txtMatKhauHienTai.Text.Trim()) != query.Password)
            {
                Warning("Mật khẩu hiện tại không đúng");
            }
            else if (Mahoa(txtMatKhauMoi.Text.Trim()) != Mahoa(txtNhapLaiMatKhauMoi.Text.Trim()))
            {
                Warning("Mật khẩu mới không khớp");
            }
            else
            {
                query.Password = Mahoa(txtMatKhauMoi.Text.Trim());

                db.SubmitChanges();
                Success("Lưu thành công");
            }
        }
        string Mahoa(string password)
        {
            MD5 mh = MD5.Create();
            //Chuyển kiểu chuổi thành kiểu byte
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(password);
            //mã hóa chuỗi đã chuyển
            byte[] hash = mh.ComputeHash(inputBytes);
            //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}