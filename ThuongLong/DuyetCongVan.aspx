﻿<%@ Page Title="Duyệt công văn" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetCongVan.aspx.cs" Inherits="ThuongLong.DuyetCongVan" %>

<%@ Register Src="UCCongVan/uc_DuyetCongVanDen.ascx" TagName="uc_DuyetCongVanDen" TagPrefix="uc1" %>
<%@ Register Src="UCCongVan/uc_DuyetCongVanDi.ascx" TagName="uc_DuyetCongVanDi" TagPrefix="uc2" %>
<%@ Register Src="UCCongVan/uc_DuyetQuanLyQuyetDinh.ascx" TagName="uc_DuyetQuanLyQuyetDinh" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmCongVanDen" runat="server" HeaderText="Công văn đến">
                            <ContentTemplate>
                                <uc1:uc_DuyetCongVanDen ID="uc_DuyetCongVanDen1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmCongVanDi" runat="server" HeaderText="Công văn đi">
                            <ContentTemplate>
                                <uc2:uc_DuyetCongVanDi ID="uc_DuyetCongVanDi1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmQuanLyQuyetDinh" runat="server" HeaderText="Quyết định">
                            <ContentTemplate>
                                <uc3:uc_DuyetQuanLyQuyetDinh ID="uc_DuyetQuanLyQuyetDinh1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
