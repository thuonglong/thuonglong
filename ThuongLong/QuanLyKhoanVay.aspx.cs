﻿using NPOI.HSSF.Record;
using NPOI.OpenXml4Net.OPC.Internal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class QuanLyKhoanVay : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        NganHangDataContext soquy = new NganHangDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmQuanLyKhoanVay";
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmQuanLyKhoanVay", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh(); 
                        LoadHangMucThuChi();
                        LoadNhaCungCap();
                        LoadChiNhanhSearch();
                        LoadNganHang();

                        hdPage.Value = "1";
                        Session["QuanLyKhoanVay"] = null;
                        hdForm.Value = Guid.NewGuid().ToString();
                        GetTable();
                        LoadKhachHangSearch();
                    }
                }
            }
        }
        void GetTable()
        {
            dt.Columns.Add("ID", typeof(Guid));
            dt.Columns.Add("IDKhoanVay", typeof(Guid));
            dt.Columns.Add("SoTaiKhoan", typeof(string));
            dt.Columns.Add("HangMucChi", typeof(string));
            dt.Columns.Add("TenHangMucChi", typeof(string));
            dt.Columns.Add("BenThuHuong", typeof(string));
            dt.Columns.Add("TenBenHuongThu", typeof(string));
            dt.Columns.Add("SoHoaDon", typeof(string));
            dt.Columns.Add("NgayGiaiNgan", typeof(string));
            dt.Columns.Add("MucDich", typeof(string));
            dt.Columns.Add("SoTienTrenHoaDon", typeof(decimal));
            dt.Columns.Add("SoTienGiaiNgan", typeof(decimal));
            dt.Columns.Add("NgayTao", typeof(DateTime));
            dt.Columns.Add("NguoiTao", typeof(string));
            dt.Columns.Add("TrangThai", typeof(Int32));
            dt.Columns.Add("Loai", typeof(Int32));
            dt.Columns.Add("hdForm", typeof(string));
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadHangMucThuChi()
        {
            var query = (from p in db.tblHangMucThuChis
                         select new
                         {
                             p.ID,
                             Ten = p.TenHangMucThuChi
                         }).OrderBy(p => p.Ten);
            dlHangMucChi.DataSource = query;
            dlHangMucChi.DataBind();
            dlHangMucChi.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadNhaCungCap()
        {
            var query = from p in db.tblNhaCungCaps
                        orderby p.TenNhaCungCap
                        select new
                        {
                            p.ID,
                            Ten = p.TenNhaCungCap
                        };
            dlBenThuHuong.DataSource = query;
            dlBenThuHuong.DataBind();
            dlBenThuHuong.Items.Insert(0, new ListItem("--Chọn bên hưởng thụ--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNganHang()
        {
            var query = (from p in db.tblNganHangs
                         select new
                         {
                             p.ID,
                             Ten = p.TenNganHang
                         }).OrderBy(p => p.Ten);
            dlNganHang.DataSource = query;
            dlNganHang.DataBind();
            dlNganHang.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (txtNgayVay.Text.Trim() == "")
            {
                s += " - Nhập ngày vay<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNganHang.SelectedValue == "")
            {
                s += " - Chọn ngân hàng<br />";
            }
            if (txtLanThu.Text.Trim() == "")
            {
                s += " - Nhập lần thứ<br />";
            }
            if (txtSoTienGiaiNganChinh.Text.Trim() == "")
            {
                s += " - Nhập số tiền giải ngân<br />";
            }
            if (txtSoTienGiaiNganChinh.Text.Trim() != "" && decimal.Parse(txtSoTienGiaiNganChinh.Text) <= 0)
            {
                s += " - Số tiền giải ngân phải lớn hơn 0<br />";
            }
            if (txtMucDichSuDung.Text.Trim() == "")
            {
                s += " - Nhập mục đích sử dụng<br />";
            }
            if (dlKyHan.SelectedValue == "")
            {
                s += " - Chọn kỳ hạn<br />";
            }
            if (txtNgayDaoHan.Text.Trim() == "")
            {
                s += " - Nhập ngày đáo hạn<br />";
            }
            if (txtHanTraCuoiCung.Text.Trim() == "")
            {
                s += " - Nhập hạn trả cuối cùng<br />";
            }
            if (txtLaiSuat.Text.Trim() == "")
            {
                s += " - Nhập lãi suất<br />";
            }
            if (txtLaiSuatQuaHan.Text.Trim() == "")
            {
                s += " - Nhập lãi suất quá hạn<br />";
            }
            if (txtNgayTraLaiDauTien.Text.Trim() == "")
            {
                s += " - Nhập ngày trả lãi đầu tiên<br />";
            }
            if (txtLanThu.Text.Trim() == "")
            {
                s += " - Nhập lần thứ<br />";
            }
            if (dlPhuongThucGiaiNgan.SelectedValue == "0")
            {
                if (txtTaiKhoanVayChinh.Text.Trim() == "")
                {
                    s += " - Nhập tài khoản vay chính<br />";
                }
            }


            //if (s == "")
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        vatlieu.sp_QuanLyKhoanVay_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayVay.Text)), new Guid(dt.Rows[i]["IDNganHang"].ToString()),
            //            new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()),
            //            DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);

            //        if (s != "")
            //        {
            //            break;
            //        }
            //    }
            //}
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (txtNgayVay.Text.Trim() == "")
            {
                s += " - Nhập ngày vay<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNganHang.SelectedValue == "")
            {
                s += " - Chọn ngân hàng<br />";
            }
            if (txtLanThu.Text.Trim() == "")
            {
                s += " - Nhập lần thứ<br />";
            }
            if (txtSoTienGiaiNganChinh.Text.Trim() == "")
            {
                s += " - Nhập số tiền giải ngân<br />";
            }
            if (txtSoTienGiaiNganChinh.Text.Trim() != "" && decimal.Parse(txtSoTienGiaiNganChinh.Text) <= 0)
            {
                s += " - Số tiền giải ngân phải lớn hơn 0<br />";
            }
            if (txtMucDichSuDung.Text.Trim() == "")
            {
                s += " - Nhập mục đích sử dụng<br />";
            }
            if (dlKyHan.SelectedValue == "")
            {
                s += " - Chọn kỳ hạn<br />";
            }
            if (txtNgayDaoHan.Text.Trim() == "")
            {
                s += " - Nhập ngày đáo hạn<br />";
            }
            if (txtHanTraCuoiCung.Text.Trim() == "")
            {
                s += " - Nhập hạn trả cuối cùng<br />";
            }
            if (txtLaiSuat.Text.Trim() == "")
            {
                s += " - Nhập lãi suất<br />";
            }
            if (txtLaiSuatQuaHan.Text.Trim() == "")
            {
                s += " - Nhập lãi suất quá hạn<br />";
            }
            if (txtNgayTraLaiDauTien.Text.Trim() == "")
            {
                s += " - Nhập ngày trả lãi đầu tiên<br />";
            }
            if (txtLanThu.Text.Trim() == "")
            {
                s += " - Nhập lần thứ<br />";
            }
            if (dlPhuongThucGiaiNgan.SelectedValue == "0")
            {
                if (txtTaiKhoanVayChinh.Text.Trim() == "")
                {
                    s += " - Nhập tài khoản vay chính<br />";
                }
            }

            //if (s == "")
            //{
            //    vatlieu.sp_QuanLyKhoanVay_CheckSuaHopDong(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNganHang), ref s);
            //}
            //if (s == "")
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        if (dt.Rows[i]["TrangThai"].ToString() == "3")
            //        {
            //            vatlieu.sp_QuanLyKhoanVay_CheckXoaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), ref s);
            //        }
            //        else if (dt.Rows[i]["TrangThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() == "0")
            //        {
            //            vatlieu.sp_QuanLyKhoanVay_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayVay.Text)), new Guid(dt.Rows[i]["IDNganHang"].ToString()),
            //                new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()),
            //                DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);
            //        }
            //        else if (dt.Rows[i]["TrangThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() != "1")
            //        {
            //            vatlieu.sp_QuanLyKhoanVay_CheckSuaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), DateTime.Parse(GetNgayThang(txtNgayVay.Text)), new Guid(dt.Rows[i]["IDNganHang"].ToString()),
            //                new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()),
            //                decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString()), decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString()),
            //                DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);
            //        }
            //        if (s != "")
            //        {
            //            break;
            //        }
            //    }
            //}
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckThemChiTiet()
        {
            string s = "";
            if (txtNgayVay.Text.Trim() == "")
            {
                s += " - Nhập ngày vay<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNganHang.SelectedValue == "")
            {
                s += " - Chọn ngân hàng<br />";
            }
            if (dlDuyTriTraLaiTuDong.SelectedValue == "0")
            {
                if (dlBenThuHuong.SelectedValue == "")
                {
                    s += " - Nhập bên thụ hưởng<br />";
                }
                if (txtSoTaiKhoan.Text.Trim() == "")
                {
                    s += " - Nhập tài khoản thụ hưởng<br />";
                }
                if (dlHangMucChi.SelectedValue == "")
                {
                    s += " - Chọn hạng mục chi<br />";
                }
            }
            if (txtSoHoaDon.Text.Trim() == "")
            {
                s += " - Nhập số hóa đơn<br />";
            }
            if (txtNgayGiaiNgan.Text.Trim() == "")
            {
                s += " - Nhập ngày giải ngân<br />";
            }
            if (txtMucDich.Text.Trim() == "")
            {
                s += " - Nhập mục đích<br />";
            }
            //if (txtSoTienTrenHoaDon.Text.Trim() == "")
            //{
            //    s += " - Nhập số tiền trên hóa đơn<br />";
            //}
            if (txtSoTienGiaiNgan.Text.Trim() == "")
            {
                s += " - Nhập số tiền giải ngân<br />";
            }

            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}

            //if (s == "")
            //{
            //    vatlieu.sp_QuanLyKhoanVay_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayVay.Text)), GetDrop(dlNganHang), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh),
            //        DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            //}
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaChiTiet()
        {
            string s = "";
            if (txtNgayVay.Text.Trim() == "")
            {
                s += " - Nhập ngày vay<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNganHang.SelectedValue == "")
            {
                s += " - Chọn ngân hàng<br />";
            }
            if (dlDuyTriTraLaiTuDong.SelectedValue == "0")
            {
                if (dlBenThuHuong.SelectedValue == "")
                {
                    s += " - Chọn bên thụ hưởng<br />";
                }
                if (txtSoTaiKhoan.Text == "")
                {
                    s += " - Nhập tài khoản thụ hưởng<br />";
                }
                if (dlHangMucChi.SelectedValue == "")
                {
                    s += " - Chọn hạng mục chi<br />";
                }
            }
            if (txtSoHoaDon.Text == "")
            {
                s += " - Nhập số hóa đơn<br />";
            }
            if (txtNgayGiaiNgan.Text == "")
            {
                s += " - Nhập ngày giải ngân<br />";
            }
            if (txtMucDich.Text == "")
            {
                s += " - Nhập mục đích<br />";
            }
            //if (txtSoTienTrenHoaDon.Text == "")
            //{
            //    s += " - Nhập số tiền trên hóa đơn<br />";
            //}
            if (txtSoTienGiaiNgan.Text == "")
            {
                s += " - Nhập số tiền giải ngân<br />";
            }

            //if (s == "")
            //{
            //    vatlieu.sp_QuanLyKhoanVay_CheckSuaChiTiet(new Guid(hdChiTiet.Value), DateTime.Parse(GetNgayThang(txtNgayVay.Text)), GetDrop(dlNganHang), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh),
            //        decimal.Parse(txtDonGiaCoThue.Text), decimal.Parse(txtDonGiaKhongThue.Text), DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            //}

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (Session["QuanLyKhoanVay"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["QuanLyKhoanVay"] as DataTable;
            }

            //DateTime ngaykyhd = DateTime.Parse(GetNgayThang(txtNgayVay.Text));
            //DateTime ngayss = ngaykyhd;
            //DateTime ngaymin = ngaykyhd;
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    if (dt.Rows[i]["TuNgay"].ToString() != "" && DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())) < ngayss)
            //    {
            //        ngaymin = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
            //    }
            //}

            if (dt.Rows.Count == 0 && gvChiTiet.Rows.Count > 0)
            {
                Warning("Thông tin khoản vay đã thay đổi.");
            }
            else if (dt.Rows.Count == 0)
            {
                Warning("Bạn chưa thiết lập thông tin khoản vay");
            }
            else if (dt.Rows.Count > 0 && dt.Rows[0]["hdForm"].ToString() != hdForm.Value)
            {
                Warning("Bạn không được mở 2 tab trên cùng 1 trình duyệt.");
            }
            //else if (ngaymin < ngaykyhd)
            //{
            //    Warning("Ngày bắt đầu tính khoản vay không được nhỏ hơn ngày vay.");
            //}
            else
            {
                if (hdID.Value == "")
                {
                    if (CheckThem() == "")
                    {
                        string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            Guid ID = Guid.NewGuid();
                            var QuanLyKhoanVay = new tblQuanLyKhoanVay()
                            {
                                ID = new Guid(hdChung.Value),
                                NgayVay = DateTime.Parse(GetNgayThang(txtNgayVay.Text)),
                                IDChiNhanh = GetDrop(dlChiNhanh),
                                IDNganHang = GetDrop(dlNganHang),
                                LanThu = txtLanThu.Text.Trim() == "" ? 0 : int.Parse(txtLanThu.Text.Trim()),
                                SoTienGiaiNgan = txtSoTienGiaiNganChinh.Text.Trim() == "" ? 0 : decimal.Parse(txtSoTienGiaiNganChinh.Text.Trim()),
                                MucDichSuDung = txtMucDichSuDung.Text.Trim(),
                                KyHan = int.Parse(dlKyHan.SelectedValue),
                                TenKyHan = dlKyHan.SelectedItem.Text.ToString(),
                                NgayDaoHan = txtNgayDaoHan.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayDaoHan.Text)),
                                HanTraCuoiCung = txtHanTraCuoiCung.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtHanTraCuoiCung.Text)),
                                LaiSuat = txtLaiSuat.Text.Trim() == "" ? 0 : double.Parse(txtLaiSuat.Text.Trim()),
                                LaiSuatQuaHan = txtLaiSuatQuaHan.Text.Trim() == "" ? 0 : double.Parse(txtLaiSuatQuaHan.Text.Trim()),
                                NgayTraLaiDauTien = txtNgayTraLaiDauTien.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayTraLaiDauTien.Text)),
                                DuyTriTraLaiTuDong = dlDuyTriTraLaiTuDong.SelectedValue == "0" ? true : false,
                                PhuongThucGiaiNgan = int.Parse(dlPhuongThucGiaiNgan.SelectedValue),
                                TenPhuongThucGiaiNgan = dlPhuongThucGiaiNgan.SelectedItem.Text.ToString(),
                                SoTaiKhoanTra = txtTaiKhoanVayChinh.Text,
                                TrangThai = 1,
                                TrangThaiText = "Chờ duyệt",
                                //STT = 0,
                                NguoiTao = Session["IDND"].ToString(),
                                NgayTao = DateTime.Now
                            };
                            db.tblQuanLyKhoanVays.InsertOnSubmit(QuanLyKhoanVay);

                            List<tblQuanLyKhoanVay_ChiTiet> them = new List<tblQuanLyKhoanVay_ChiTiet>();

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["Loai"].ToString() != "10")
                                {
                                    var themchitiet = new tblQuanLyKhoanVay_ChiTiet();
                                    themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());

                                    themchitiet.IDKhoanVay = new Guid(dt.Rows[i]["IDKhoanVay"].ToString());
                                    themchitiet.SoTaiKhoan = dt.Rows[i]["SoTaiKhoan"].ToString();
                                    themchitiet.HangMucChi = dt.Rows[i]["HangMucChi"].ToString() == "" ? (Guid?)null : new Guid(dt.Rows[i]["HangMucChi"].ToString());
                                    themchitiet.TenHangMucChi = dt.Rows[i]["TenHangMucChi"].ToString();
                                    themchitiet.BenThuHuong = dt.Rows[i]["BenThuHuong"].ToString() == "" ? (Guid?)null : new Guid(dt.Rows[i]["BenThuHuong"].ToString());
                                    themchitiet.TenBenHuongThu = dt.Rows[i]["TenBenHuongThu"].ToString();
                                    themchitiet.SoHoaDon = dt.Rows[i]["SoHoaDon"].ToString();
                                    themchitiet.NgayGiaiNgan = dt.Rows[i]["NgayGiaiNgan"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["NgayGiaiNgan"].ToString()));
                                    themchitiet.MucDich = dt.Rows[i]["MucDich"].ToString();
                                    themchitiet.SoTienTrenHoaDon = decimal.Parse(dt.Rows[i]["SoTienTrenHoaDon"].ToString());
                                    themchitiet.SoTienGiaiNgan = decimal.Parse(dt.Rows[i]["SoTienGiaiNgan"].ToString());
                                    themchitiet.TrangThai = int.Parse(dt.Rows[i]["TrangThai"].ToString());
                                    themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                    themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();

                                    them.Add(themchitiet);
                                }
                            }
                            db.tblQuanLyKhoanVay_ChiTiets.InsertAllOnSubmit(them);
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Lưu thành công.");
                        }
                    }
                }
                else
                {
                    if (CheckSua() == "")
                    {

                        string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            var query = (from p in db.tblQuanLyKhoanVays
                                         where p.ID == new Guid(hdID.Value)
                                         select p).FirstOrDefault();
                            if (query != null && query.ID != null)
                            {
                                DateTime dtcheck = DateTime.Parse(hdNgayTao.Value);
                                if (hdID.Value != ""
                                    && (
                                    dtcheck.Year != query.NgayTao.Value.Year
                                    || dtcheck.Month != query.NgayTao.Value.Month
                                    || dtcheck.Day != query.NgayTao.Value.Day
                                    || dtcheck.Hour != query.NgayTao.Value.Hour
                                    || dtcheck.Minute != query.NgayTao.Value.Minute
                                    || dtcheck.Second != query.NgayTao.Value.Second
                                    )
                                    )
                                {
                                    Warning("Thông tin hợp đồng đã bị sửa, xin vui lòng kiểm tra lại.");
                                }
                                else
                                {
                                    query.NgayVay = DateTime.Parse(GetNgayThang(txtNgayVay.Text));
                                    query.IDChiNhanh = GetDrop(dlChiNhanh);
                                    query.IDNganHang = GetDrop(dlNganHang);
                                    query.LanThu = txtLanThu.Text.Trim() == "" ? 0 : int.Parse(txtLanThu.Text.Trim());
                                    query.SoTienGiaiNgan = txtSoTienGiaiNganChinh.Text.Trim() == "" ? 0 : decimal.Parse(txtSoTienGiaiNganChinh.Text.Trim());
                                    query.MucDichSuDung = txtMucDichSuDung.Text.Trim();
                                    query.KyHan = int.Parse(dlKyHan.SelectedValue);
                                    query.TenKyHan = dlKyHan.SelectedItem.Text.ToString();
                                    query.NgayDaoHan = txtNgayDaoHan.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayDaoHan.Text));
                                    query.HanTraCuoiCung = txtHanTraCuoiCung.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtHanTraCuoiCung.Text));
                                    query.LaiSuat = txtLaiSuat.Text.Trim() == "" ? 0 : double.Parse(txtLaiSuat.Text.Trim());
                                    query.LaiSuatQuaHan = txtLaiSuatQuaHan.Text.Trim() == "" ? 0 : double.Parse(txtLaiSuatQuaHan.Text.Trim());
                                    query.NgayTraLaiDauTien = txtNgayTraLaiDauTien.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayTraLaiDauTien.Text));
                                    query.DuyTriTraLaiTuDong = dlDuyTriTraLaiTuDong.SelectedValue == "0" ? true : false;
                                    query.PhuongThucGiaiNgan = int.Parse(dlPhuongThucGiaiNgan.SelectedValue);
                                    query.TenPhuongThucGiaiNgan = dlPhuongThucGiaiNgan.SelectedItem.Text.ToString();
                                    query.SoTaiKhoanTra = txtTaiKhoanVayChinh.Text;
                                    query.TrangThai = 1;
                                    query.TrangThaiText = "Chờ duyệt";
                                    //query.STT = query.STT + 1;
                                    query.NguoiTao = Session["IDND"].ToString();
                                    query.NgayTao = DateTime.Now;


                                    List<tblQuanLyKhoanVay_ChiTiet> xoa = new List<tblQuanLyKhoanVay_ChiTiet>();
                                    List<tblQuanLyKhoanVay_ChiTiet> them = new List<tblQuanLyKhoanVay_ChiTiet>();

                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        if (dt.Rows[i]["Loai"].ToString() != "10")
                                        {
                                            var xoachitiet = (from p in db.tblQuanLyKhoanVay_ChiTiets
                                                              where p.ID == new Guid(dt.Rows[i]["ID"].ToString())
                                                              select p).FirstOrDefault();
                                            if (xoachitiet != null && xoachitiet.ID != null)
                                            {
                                                xoa.Add(xoachitiet);
                                            }
                                            var themchitiet = new tblQuanLyKhoanVay_ChiTiet();
                                            themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                            themchitiet.IDKhoanVay = new Guid(dt.Rows[i]["IDKhoanVay"].ToString());
                                            themchitiet.SoTaiKhoan = dt.Rows[i]["SoTaiKhoan"].ToString();
                                            themchitiet.HangMucChi = dt.Rows[i]["HangMucChi"].ToString() == "" ? (Guid?)null : new Guid(dt.Rows[i]["HangMucChi"].ToString());
                                            themchitiet.TenHangMucChi = dt.Rows[i]["TenHangMucChi"].ToString();
                                            themchitiet.BenThuHuong = dt.Rows[i]["BenThuHuong"].ToString() == "" ? (Guid?)null : new Guid(dt.Rows[i]["BenThuHuong"].ToString());
                                            themchitiet.TenBenHuongThu = dt.Rows[i]["TenBenHuongThu"].ToString();
                                            themchitiet.SoHoaDon = dt.Rows[i]["SoHoaDon"].ToString();
                                            themchitiet.NgayGiaiNgan = dt.Rows[i]["NgayGiaiNgan"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["NgayGiaiNgan"].ToString()));
                                            themchitiet.MucDich = dt.Rows[i]["MucDich"].ToString();
                                            themchitiet.SoTienTrenHoaDon = decimal.Parse(dt.Rows[i]["SoTienTrenHoaDon"].ToString());
                                            themchitiet.SoTienGiaiNgan = decimal.Parse(dt.Rows[i]["SoTienGiaiNgan"].ToString());
                                            themchitiet.TrangThai = int.Parse(dt.Rows[i]["TrangThai"].ToString());
                                            themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                            themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                            them.Add(themchitiet);
                                        }
                                    }
                                    db.tblQuanLyKhoanVay_ChiTiets.DeleteAllOnSubmit(xoa);
                                    db.tblQuanLyKhoanVay_ChiTiets.InsertAllOnSubmit(them);

                                    db.SubmitChanges();

                                    lblTaoMoiHopDong_Click(sender, e);
                                    Search(1);
                                    Success("Sửa thành công");
                                }
                            }
                            else
                            {
                                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                                lblTaoMoiHopDong_Click(sender, e);
                            }
                        }
                    }
                }
            }
        }
        public void KhoaHD(bool b)
        {
            if (b == true)
            {
                txtNgayVay.Enabled = false;
                dlChiNhanh.Enabled = false;
                dlNganHang.Enabled = false;
            }
            else
            {
                txtNgayVay.Enabled = true;
                dlChiNhanh.Enabled = true;
                dlNganHang.Enabled = true;
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            //KhoaHD(false);
            dlChiNhanh.SelectedIndex = 0;
            dlNganHang.SelectedValue = "";
            hdID.Value = "";
            hdChung.Value = "";
            hdForm.Value = "";
            hdChiTiet.Value = "";
            txtLanThu.Text = "";
            txtSoTienGiaiNganChinh.Text = "";
            txtMucDichSuDung.Text = "";
            dlKyHan.SelectedValue = "";
            txtNgayDaoHan.Text = "";
            txtHanTraCuoiCung.Text = "";
            txtLaiSuat.Text = "";
            txtLaiSuatQuaHan.Text = "";
            txtNgayTraLaiDauTien.Text = "";
            txtTaiKhoanVayChinh.Text = "";

            Session["QuanLyKhoanVay"] = null;
            gvChiTiet.DataSource = null;
            gvChiTiet.DataBind();
            LoadKhachHangSearch();
        }
        protected void lbtSave_Click(object sender, EventArgs e)
        {
            string url = "";
            if (Session["QuanLyKhoanVay"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["QuanLyKhoanVay"] as DataTable;
            }

            if (hdChiTiet.Value == "")
            {
                if (CheckThemChiTiet() == "" && CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (hdForm.Value == "")
                        {
                            hdForm.Value = Guid.NewGuid().ToString();
                        }

                        if (hdID.Value == "" && hdChung.Value == "")
                            hdChung.Value = Guid.NewGuid().ToString();

                        dt.Rows.Add(Guid.NewGuid(),
                        new Guid(hdChung.Value),
                        txtSoTaiKhoan.Text.Trim(),
                        dlHangMucChi.SelectedValue,
                        dlHangMucChi.SelectedItem.Text,
                        dlBenThuHuong.SelectedValue,
                        dlBenThuHuong.SelectedItem.Text,
                        txtSoHoaDon.Text.Trim(),
                        txtNgayGiaiNgan.Text.Trim(),
                        txtMucDich.Text.Trim(),
                        decimal.Parse(txtSoTienGiaiNgan.Text),
                        decimal.Parse(txtSoTienGiaiNgan.Text),
                        DateTime.Now,
                        Session["IDND"].ToString(),
                        1,
                        0,
                        hdForm.Value
                        );

                        lbtTaoGiaMoi_Click(sender, e);
                        gvChiTiet.DataSource = dt;
                        gvChiTiet.DataBind();
                        Session["QuanLyKhoanVay"] = dt;
                        Success("Lưu thành công.");
                        //KhoaHD(true);
                    }
                }
            }
            else
            {
                if (CheckSuaChiTiet() == "" && CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (hdForm.Value == "")
                        {
                            hdForm.Value = Guid.NewGuid().ToString();
                        }

                        DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(hdChiTiet.Value)).FirstOrDefault();
                        if (dr != null)
                        {
                            dr["IDKhoanVay"] = new Guid(hdChung.Value);
                            dr["SoTaiKhoan"] = txtSoTaiKhoan.Text.Trim();
                            dr["HangMucChi"] = dlHangMucChi.SelectedValue;
                            dr["TenHangMucChi"] = dlHangMucChi.SelectedItem.Text;
                            dr["BenThuHuong"] = dlBenThuHuong.SelectedValue;
                            dr["TenBenHuongThu"] = dlBenThuHuong.SelectedItem.Text;
                            dr["SoHoaDon"] = txtSoHoaDon.Text.Trim();
                            dr["NgayGiaiNgan"] = txtNgayGiaiNgan.Text.Trim();
                            dr["MucDich"] = txtMucDich.Text.Trim();
                            dr["SoTienTrenHoaDon"] = decimal.Parse(txtSoTienGiaiNgan.Text.Trim());
                            dr["SoTienGiaiNgan"] = decimal.Parse(txtSoTienGiaiNgan.Text.Trim());
                            dr["TrangThai"] = 1;
                            dr["Loai"] = 1;
                            dr["hdForm"] = hdForm.Value;
                        }

                        lbtTaoGiaMoi_Click(sender, e);
                        gvChiTiet.DataSource = dt;
                        gvChiTiet.DataBind();
                        Session["QuanLyKhoanVay"] = dt;
                        Success("Lưu thành công.");
                        //KhoaHD(true);
                    }
                }
            }
        }

        protected void lbtTaoGiaMoi_Click(object sender, EventArgs e)
        {
            txtSoTaiKhoan.Text = "";
            dlBenThuHuong.SelectedValue = "";
            dlHangMucChi.SelectedValue = "";
            txtSoHoaDon.Text = "";
            txtNgayGiaiNgan.Text = "";
            txtMucDich.Text = "";
            txtSoTienTrenHoaDon.Text = "";
            txtSoTienGiaiNgan.Text = "";
        }

        protected void gvChiTiet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();

            if (Session["QuanLyKhoanVay"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["QuanLyKhoanVay"] as DataTable;
            }

            DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(id)).FirstOrDefault();
            if (dr != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtSoTaiKhoan.Text = dr["SoTaiKhoan"].ToString();
                        dlBenThuHuong.SelectedValue = dr["BenThuHuong"].ToString();
                        dlHangMucChi.SelectedValue = dr["HangMucChi"].ToString();
                        txtSoHoaDon.Text = dr["SoHoaDon"].ToString();
                        txtNgayGiaiNgan.Text = dr["NgayGiaiNgan"].ToString();
                        txtMucDich.Text = dr["MucDich"].ToString();
                        txtSoTienTrenHoaDon.Text = string.Format("{0:N0}", decimal.Parse(dr["SoTienGiaiNgan"].ToString()));
                        txtSoTienGiaiNgan.Text = string.Format("{0:N0}", decimal.Parse(dr["SoTienGiaiNgan"].ToString()));

                        hdChiTiet.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //vatlieu.sp_QuanLyKhoanVay_CheckXoaChiTiet(new Guid(id), ref thongbao);
                        //if (thongbao != "")
                        //{
                        //    Warning(thongbao);
                        //}
                        //else
                        //{
                        var query = (from p in db.tblQuanLyKhoanVay_ChiTiets
                                     where p.ID == new Guid(id)
                                     select p).FirstOrDefault();

                        if (query != null && query.ID != null)
                        {
                            dr["Loai"] = "3";
                            dr["TrangThai"] = "3";
                            dr["TrangThaiText"] = "Chờ duyệt xóa";

                            Success("Xóa thành công.");
                            gvChiTiet.DataSource = dt;
                            gvChiTiet.DataBind();
                            Session["GiBanGach"] = dt;
                            lbtTaoGiaMoi_Click(sender, e);
                        }
                        else
                        {
                            dt.Rows.Remove(dr);
                            dt.AcceptChanges();

                            Success("Xóa thành công.");
                            gvChiTiet.DataSource = dt;
                            gvChiTiet.DataBind();
                            Session["GiBanGach"] = dt;
                            lbtTaoGiaMoi_Click(sender, e);
                        }
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = vatlieu.sp_QuanLyKhoanVay_LichSuChiTiet(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblQuanLyKhoanVays
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh.Value, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayVay.Text = query.NgayVay.Value.ToString("dd/MM/yyyy");
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNganHang.SelectedValue = query.IDNganHang.ToString();
                        txtLanThu.Text = query.LanThu.ToString();
                        txtSoTienGiaiNganChinh.Text = string.Format("{0:N0}", query.SoTienGiaiNgan);
                        txtMucDichSuDung.Text = query.MucDichSuDung;
                        dlKyHan.SelectedValue = query.KyHan.ToString();
                        txtNgayDaoHan.Text = query.NgayDaoHan.Value.ToString("dd/MM/yyyy");
                        txtHanTraCuoiCung.Text = query.HanTraCuoiCung.Value.ToString("dd/MM/yyyy");
                        txtLaiSuat.Text = query.LaiSuat.ToString();
                        txtLaiSuatQuaHan.Text = query.LaiSuatQuaHan.ToString();
                        txtNgayTraLaiDauTien.Text = query.NgayTraLaiDauTien.Value.ToString("dd/MM/yyyy");
                        txtTaiKhoanVayChinh.Text = query.SoTaiKhoanTra;
                        dlPhuongThucGiaiNgan.SelectedValue = query.PhuongThucGiaiNgan.ToString();
                        dlDuyTriTraLaiTuDong.SelectedValue = query.DuyTriTraLaiTuDong.ToString();
                        hdID.Value = id;
                        hdChung.Value = id;
                        hdNgayTao.Value = query.NgayTao.ToString();
                        hdForm.Value = Guid.NewGuid().ToString();
                        LoadChiTiet();
                        //KhoaHD(true);
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh.Value, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //vatlieu.sp_QuanLyKhoanVay_CheckXoa(query.ID, ref thongbao);
                        //if (thongbao != "")
                        //    Warning(thongbao);
                        //else
                        //{
                        if (query.TrangThai == 1)
                        {
                            int countxoa = (from p in db.tblQuanLyKhoanVay_Logs
                                            where p.IDChung == query.ID
                                            select p).Count();
                            if (countxoa == 1)
                            {
                                var chitiet = from p in db.tblQuanLyKhoanVay_ChiTiets
                                              where p.IDKhoanVay == query.ID
                                              select p;
                                db.tblQuanLyKhoanVay_ChiTiets.DeleteAllOnSubmit(chitiet);
                                db.tblQuanLyKhoanVays.DeleteOnSubmit(query);
                                db.SubmitChanges();
                                Success("Đã xóa");
                            }
                            else
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                        }
                        else if (query.TrangThai == 2)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                            Success("Xóa thành công");
                        }
                        else
                        {
                            Warning("Dữ liệu đang chờ duyệt xóa");
                        }
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = vatlieu.sp_QuanLyKhoanVay_LichSu(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        //private string Check(DataTable tableinsert, string tungay, string denngay)
        //{
        //    string _er = "";
        //    DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
        //    DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

        //    var dr_tontai =
        //        (tableinsert.AsEnumerable()
        //            .Where(p => p.Field<Guid>("IDLoaiVatLieu") == GetDrop(dlLoaiVatLieu)
        //                        && p.Field<Guid>("IDDonViTinh") == GetDrop(dlDonViTinh)
        //                        &&
        //                        (
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay && DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_tungay)
        //                        ||
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_denngay && (p.Field<string>("DenNgay") == "" || DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_denngay))
        //                        ||
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay && (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_denngay))
        //                        ||
        //                        (p.Field<string>("DenNgay") != "" && (DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_tungay && (DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) <= dt_denngay)))
        //                        || (p.Field<string>("DenNgay") == "" && denngay == "")
        //                        )
        //            )).Count();
        //    if (dr_tontai > 0)
        //    {
        //        _er = "Trong cùng 1 khoảng thời gian không được tồn tại 2 thông tin giá giống nhau";
        //        Warning(_er);
        //    }
        //    return _er;
        //}

        //private string CheckSua(DataTable tableinsert, string tungay, string denngay, string id)
        //{
        //    string _er = "";
        //    DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
        //    DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

        //    var dr_tontai =
        //        (tableinsert.AsEnumerable()
        //            .Where(p => p.Field<Guid>("IDLoaiVatLieu") == GetDrop(dlLoaiVatLieu)
        //                        && p.Field<Guid>("IDDonViTinh") == GetDrop(dlDonViTinh)
        //                        &&
        //                        (
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay && (p.Field<string>("DenNgay") != "" && DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_tungay))
        //                        ||
        //                        (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_denngay && (p.Field<string>("DenNgay") == "" || DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_denngay))
        //                        //||
        //                        //(DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay && (DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_denngay))
        //                        //||
        //                        //(p.Field<string>("DenNgay") != "" && (DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) >= dt_tungay && (DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay"))) <= dt_denngay)))
        //                        //|| (p.Field<string>("DenNgay") == "" && denngay == "")
        //                        )
        //                        && p.Field<Guid>("ID") != new Guid(id)
        //            )).Count();
        //    if (dr_tontai > 0)
        //    {
        //        _er = "Trong cùng 1 khoảng thời gian không được tồn tại 2 thông tin giá giống nhau";
        //        Warning(_er);
        //    }
        //    return _er;
        //}
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void dlNganHang_SelectedIndexChanged(object sender, EventArgs e)
        {
            //LoadLoaiVatLieu();
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            var query = soquy.sp_QuanLyKhoanVay_Search(GetDrop(dlChiNhanhSearch), dlKhachHangSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlKhachHangSearch), 20, page);

            GV.DataSource = query;
            GV.DataBind();
        }
        private void LoadChiTiet()
        {
            List<sp_QuanLyKhoanVay_ChiTietSearchResult> query = soquy.sp_QuanLyKhoanVay_ChiTietSearch(new Guid(hdID.Value), hdForm.Value).ToList();
            GetTable();
            foreach (var item in query)
            {
                dt.Rows.Add(
                                item.ID,
                                item.IDKhoanVay,
                                item.SoTaiKhoan,
                                item.HangMucChi,
                                item.TenHangMucChi,
                                item.BenThuHuong,
                                item.TenBenHuongThu,
                                item.SoHoaDon,
                                item.NgayGiaiNgan,
                                item.MucDich,
                                item.SoTienTrenHoaDon,
                                item.SoTienGiaiNgan,
                                item.NgayTao,
                                item.NguoiTao,
                                item.TrangThai,
                                item.Loai,
                                item.hdForm
                            );
            }
            gvChiTiet.DataSource = dt;
            gvChiTiet.DataBind();
            Session["QuanLyKhoanVay"] = dt;
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadKhachHangSearch();
        }
        void LoadKhachHangSearch()
        {
            dlKhachHangSearch.Items.Clear();
            var query = soquy.sp_QuanLyKhoanVay_LoadNganHangSearch(GetDrop(dlChiNhanhSearch));
            dlKhachHangSearch.DataSource = query;
            dlKhachHangSearch.DataBind();
            dlKhachHangSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnTaoMoiCongTrinh_Click(object sender, EventArgs e)
        {
            //mpCongTrinh.Show();
            //txtTenNganHang.Text = "";
            //txtSoDienThoai.Text = "";
            //txtMaSoThue.Text = "";
            //txtDiaChi.Text = "";
        }
        protected void btnLuuCongTrinh_Click(object sender, EventArgs e)
        {
            //var checktrung = from p in db.tblNganHangs
            //                 where p.TenNganHang.ToUpper() == txtTenNganHang.Text.Trim().ToUpper()
            //                 && p.IDNhomNganHang == GetDrop(dlLoaiNganHang)
            //                 select p;
            //if (txtTenNganHang.Text.Trim() == "")
            //{
            //    GstGetMess("Nhập tên ngân hàng.", "");
            //    mpCongTrinh.Show();
            //}
            //else if (checktrung.Count() > 0)
            //{
            //    GstGetMess("Tên ngân hàng theo loại ngân hàng đã tồn tại.", "");
            //    mpCongTrinh.Show();
            //}
            //else
            //{
            //    Guid id = Guid.NewGuid();
            //    var insertcongtrinh = new tblNganHang()
            //    {
            //        ID = Guid.NewGuid(),
            //        IDNhomNganHang = GetDrop(dlLoaiNganHang),
            //        TenNganHang = txtTenNganHang.Text.Trim(),
            //        SoDienThoai = txtSoDienThoai.Text.Trim(),
            //        MaSoThue = txtMaSoThue.Text.Trim(),
            //        DiaChi = txtDiaChi.Text.Trim(),
            //        NgayTao = DateTime.Now,
            //        NguoiTao = Session["IDND"].ToString()
            //    };
            //    db.tblNganHangs.InsertOnSubmit(insertcongtrinh);
            //    db.SubmitChanges();
            //    LoadNganHang();
            //    //dlNganHang.SelectedValue = id.ToString().ToUpper();
            //}
        }

        protected void txtNgayVay_TextChanged(object sender, EventArgs e)
        {
            if (txtNgayVay.Text.Trim() != "" && dlKyHan.SelectedValue != "")
            {
                DateTime dt = DateTime.Parse(GetNgayThang(txtNgayVay.Text)).AddMonths(int.Parse(dlKyHan.SelectedValue));

                if ((int)dt.DayOfWeek == 6)
                {
                    dt = dt.AddDays(2);
                }
                else if ((int)dt.DayOfWeek == 7)
                {
                    dt = dt.AddDays(1);
                }
                txtNgayDaoHan.Text = dt.ToString("dd/MM/yyyy");
                txtHanTraCuoiCung.Text = txtNgayDaoHan.Text;
            }
            else
            {
                txtNgayGiaiNgan.Text = "";
                txtHanTraCuoiCung.Text = "";
            }
        }
    }
}