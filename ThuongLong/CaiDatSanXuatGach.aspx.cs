﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class CaiDatSanXuatGach : System.Web.UI.Page
    {
        private DBDataContext db = new DBDataContext();
        private string frmName = "frmCaiDatSanXuatGach";
        private string TabForm = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    //IEnumerable<AjaxControlToolkit.TabPanel> selectedTab = from AjaxControlToolkit.TabPanel tab in TabContainer1.Tabs
                    //                                                       select tab;
                    //foreach (AjaxControlToolkit.TabPanel item in selectedTab)
                    //{
                    //    TabForm += "+" + item.ID.ToString() + "+";
                    //}
                    //                    sp_PhanQuyenCaiDat_CheckTruyCapResult setup = db.sp_PhanQuyenCaiDat_CheckTruyCap(GetDrop(dlChiNhanh), Session["IDND"].ToString(), TabForm).FirstOrDefault();
                    //if (setup.SoLuong > 0)
                    //{
                    IQueryable<tblUserAccount> setup = from x in db.tblUserAccounts
                                                       where x.UserName.ToUpper() == Session["IDND"].ToString()
                                                       && x.LoaiTaiKhoan == 2
                                                       //&& x.IDChiNhanh == new Guid(Session["IDDN"].ToString())
                                                       select x;
                    if (setup.Count() == 0)
                    {
                        gstGetMess("Bạn không có quyền truy cập trang này.", "default.aspx");
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadNguoiDung();
                        LoadTabContainer();
                        TabContainer1_ActiveTabChanged(sender, e);
                    }
                    //}
                    //else
                    //{
                    //    gstGetMess("Bạn không có quyền truy cập trang này.", "default.aspx");
                    //}
                }
            }
        }
        private void LoadTabContainer()
        {
            System.Data.Linq.ISingleResult<sp_PhanQuyenCaiDat_LoadTabResult> query = db.sp_PhanQuyenCaiDat_LoadTab(GetDrop(dlChiNhanh), Session["IDND"].ToString(), TabForm);
            foreach (sp_PhanQuyenCaiDat_LoadTabResult item in query)
            {
                IEnumerable<AjaxControlToolkit.TabPanel> selectedTab = from AjaxControlToolkit.TabPanel tab in TabContainer1.Tabs
                                                                       select tab;

                foreach (AjaxControlToolkit.TabPanel tab in selectedTab)
                {
                    if (item.Admins == 1 || (item.Admins == 0 && item.Tag == tab.ID))
                    {
                        tab.Visible = true;
                    }
                }
            }
        }
        private void LoadForm(bool nghiepvu, bool chot, bool chothet, bool thanhly)
        {
            #region nghiệp vụ cơ bản
            divChung.Visible = nghiepvu;
            Them.Visible = nghiepvu;
            Sua.Visible = nghiepvu;
            Xoa.Visible = nghiepvu;
            Xem.Visible = nghiepvu;
            Xuat.Visible = nghiepvu;

            ThuCap.Visible = nghiepvu;
            DuyetXoa.Visible = nghiepvu;

            ckThem.Checked = false;
            ckSua.Checked = false;
            ckXoa.Checked = false;
            ckXem.Checked = false;
            ckXuat.Checked = false;

            ckThuCap.Checked = false;
            ckDuyetXoa.Checked = false;

            #endregion

            #region chốt
            Chot.Visible = chot;
            MoChot.Visible = chot;
            DuyetChot.Visible = chot;
            DuyetMoChot.Visible = chot;

            ckChot.Checked = false;
            ckMoChot.Checked = false;
            ckDuyetChot.Checked = false;
            ckDuyetMoChot.Checked = false;

            divChot.Visible = nghiepvu;
            #endregion

            #region thanh lý
            ThanhLy.Visible = thanhly;
            MoThanhLy.Visible = thanhly;
            DuyetThanhLy.Visible = thanhly;
            DuyetMoThanhLy.Visible = thanhly;

            ckThanhLy.Checked = false;
            ckMoThanhLy.Checked = false;
            ckDuyetThanhLy.Checked = false;
            ckDuyetMoThanhLy.Checked = false;

            divThanhLy.Visible = nghiepvu;
            #endregion
        }
        private void LoadQuyen(string tag)
        {
            if (dlChiNhanh.SelectedValue != "" && dlNguoiDung.SelectedValue != "")
            {
                var thaotac = (from p in db.tblPhanQuyens
                               where p.UserName == dlNguoiDung.SelectedValue
                               && p.Tag == tag
                               select new
                               {
                                   p.LoaiQuyen
                               }).Distinct().OrderBy(p => p.LoaiQuyen);

                #region load thao tác

                foreach (var item in thaotac)
                {
                    if (item.LoaiQuyen == 1)
                    {
                        ckThem.Checked = true;
                    }
                    else if (item.LoaiQuyen == 2)
                    {
                        ckSua.Checked = true;
                    }
                    else if (item.LoaiQuyen == 3)
                    {
                        ckXoa.Checked = true;
                    }
                    else if (item.LoaiQuyen == 4)
                    {
                        ckXem.Checked = true;
                    }
                    else if (item.LoaiQuyen == 5)
                    {
                        ckXuat.Checked = true;
                    }
                    else if (item.LoaiQuyen == 6)
                    {
                        ckChot.Checked = true;
                    }
                    else if (item.LoaiQuyen == 7)
                    {
                        ckMoChot.Checked = true;
                    }
                    else if (item.LoaiQuyen == 8)
                    {
                        ckThanhLy.Checked = true;
                    }
                    else if (item.LoaiQuyen == 9)
                    {
                        ckMoThanhLy.Checked = true;
                    }
                    else if (item.LoaiQuyen == 10)
                    {
                        ckThuCap.Checked = true;
                    }
                    else if (item.LoaiQuyen == 11)
                    {
                        ckDuyetXoa.Checked = true;
                    }
                    else if (item.LoaiQuyen == 12)
                    {
                        ckDuyetChot.Checked = true;
                    }
                    else if (item.LoaiQuyen == 13)
                    {
                        ckDuyetMoChot.Checked = true;
                    }
                    else if (item.LoaiQuyen == 14)
                    {
                        ckDuyetThanhLy.Checked = true;
                    }
                    else if (item.LoaiQuyen == 15)
                    {
                        ckDuyetMoThanhLy.Checked = true;
                    }
                }
                #endregion
            }
        }
        private void LoadChiNhanh()
        {
            System.Data.Linq.ISingleResult<sp_LoadChiNhanhResult> query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
        }
        private Guid? GetDrop(DropDownList dl)
        {
            Guid? gId = dl.SelectedValue == "" ? (Guid?)null : new Guid(dl.SelectedValue);
            return gId;
        }
        private string khoaphanquyen(Guid iddn, string idnd)
        {
            string s = "";

            tblUserAccount khoataikhoan = (from p in db.tblUserAccounts where p.UserName == idnd select p).FirstOrDefault();
            if (khoataikhoan == null)
            {
                s += "Tài khoản của bạn đã bị xóa, xin vui lòng kiểm tra lại<br />";
                gstGetMess("Tài khoản của bạn đã bị xóa, xin vui lòng kiểm tra lại", "login.aspx");
            }
            else if (khoataikhoan != null && khoataikhoan.TrangThai == false)
            {
                s = "1";
                gstGetMess("Tài khoản của bạn đã bị khóa, xin vui lòng kiểm tra lại", "login.aspx");
            }
            int index = TabContainer1.ActiveTabIndex;
            string TagName = TabContainer1.ActiveTab.ID.ToString();
            if (dlChiNhanh.SelectedIndex > 0)
            {
                sp_PhanQuyen_CheckQuyenResult setup = db.sp_PhanQuyen_CheckQuyen(GetDrop(dlChiNhanh), Session["IDND"].ToString(), TagName).FirstOrDefault();
                if (Session["LTK"].ToString() != "2" && setup.SoLuong == 0)
                {
                    s = "Bạn không có quyền thực hiện chức năng này.";
                    gstGetMess("Bạn không có quyền thực hiện chức năng này", "");
                }
            }
            else
            {
                sp_PhanQuyen_CheckQuyenResult setup = db.sp_PhanQuyen_CheckQuyen(GetDrop(dlChiNhanh), Session["IDND"].ToString(), TagName).FirstOrDefault();
                if (Session["LTK"].ToString() != "2" && setup.SoLuong == 0)
                {
                    s = "Bạn không có quyền thực hiện chức năng này.";
                    gstGetMess("Bạn không có quyền thực hiện chức năng này", "");
                }
            }
            return s;
        }
        private string khoataikhoan(string idnd)
        {
            string s = "";
            tblUserAccount khoataikhoan = (from p in db.tblUserAccounts where p.UserName == idnd select p).FirstOrDefault();
            if (khoataikhoan == null)
            {
                s += "Tài khoản " + idnd + " đã bị xóa, xin vui lòng kiểm tra lại<br />";
                gstGetMess("Tài khoản " + idnd + " đã bị xóa, xin vui lòng kiểm tra lại", "");
            }
            else if (khoataikhoan != null && khoataikhoan.TrangThai == false)
            {
                s = "1";
                gstGetMess("Tài khoản " + idnd + " đã bị khóa, xin vui lòng kiểm tra lại", "");
            }
            return s;
        }
        private void LoadNguoiDung()
        {
            string TagName = TabContainer1.ActiveTab.ID.ToString();
            System.Data.Linq.ISingleResult<sp_PhanQuyen_LoadNguoiDungResult> cn = db.sp_PhanQuyen_LoadNguoiDung();

            dlNguoiDung.Items.Clear();
            dlNguoiDung.DataSource = cn;
            dlNguoiDung.DataBind();
            dlNguoiDung.Items.Insert(0, new ListItem("Chọn người dùng", ""));
            dlNguoiDung.SelectedIndex = 0;
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int index = TabContainer1.ActiveTabIndex;
                string TagName = TabContainer1.ActiveTab.ID.ToString();
                TabContainer1_ActiveTabChanged(sender, e);
            }
            catch (Exception ax)
            {
                gstGetMess(ax.Message, "");
            }
        }
        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {
            int index = TabContainer1.ActiveTabIndex;
            string name = TabContainer1.ActiveTab.ID.ToString();

            #region load div tag name
            //if (name == "frmGiaMuaVatLieu")
            //{
            LoadForm(true, false, false, false);
            //}
            //else if (name == "frmNhapKhoVatLieu")
            //{
            //    LoadForm(true, true, false, false);
            //}
            //else if (name == "frmGiaBanVatLieu")
            //{
            //    LoadForm(true, false, false, false);
            //}
            //else if (name == "frmBanVatLieu")
            //{
            //    LoadForm(true, true, false, false);
            //}
            //else if (name == "frmSoDuVatLieu")
            //{
            //    LoadForm(true, true, false, false);
            //}

            #endregion

            LoadQuyen(name);
        }
        private void gstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private bool ValidateQuyen()
        {
            bool b = false;

            if (Them.Visible == true && ckThem.Checked == true)
            {
                b = true;
            }
            else if (Sua.Visible == true && ckSua.Checked == true)
            {
                b = true;
            }
            else if (Xoa.Visible == true && ckXoa.Checked == true)
            {
                b = true;
            }
            else if (Xem.Visible == true && ckXem.Checked == true)
            {
                b = true;
            }
            else if (Xuat.Visible == true && ckXuat.Checked == true)
            {
                b = true;
            }
            else if (Chot.Visible == true && ckChot.Checked == true)
            {
                b = true;
            }
            else if (MoChot.Visible == true && ckMoChot.Checked == true)
            {
                b = true;
            }
            else if (ThanhLy.Visible == true && ckThanhLy.Checked == true)
            {
                b = true;
            }
            else if (MoThanhLy.Visible == true && ckMoThanhLy.Checked == true)
            {
                b = true;
            }
            else if (ThuCap.Visible == true && ckThuCap.Checked == true)
            {
                b = true;
            }
            else if (DuyetXoa.Visible == true && ckDuyetXoa.Checked == true)
            {
                b = true;
            }
            else if (DuyetChot.Visible == true && ckDuyetChot.Checked == true)
            {
                b = true;
            }
            else if (DuyetMoChot.Visible == true && ckDuyetMoChot.Checked == true)
            {
                b = true;
            }
            else if (DuyetThanhLy.Visible == true && ckDuyetThanhLy.Checked == true)
            {
                b = true;
            }
            else if (DuyetMoThanhLy.Visible == true && ckDuyetMoThanhLy.Checked == true)
            {
                b = true;
            }
            return b;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int index = TabContainer1.ActiveTabIndex;
                string gs = "";
                string TagName = TabContainer1.ActiveTab.ID.ToString();

                if (dlChiNhanh.SelectedValue == "")
                {
                    gs += " - Chọn chi nhánh.<br />";
                }
                if (dlNguoiDung.SelectedValue == "")
                {
                    gs += " - Chọn người dùng<br />";
                }
                //if (ValidateQuyen() == true)
                //{
                //    gs += " - Bạn phải chọn quyền<br />";
                //}
                if (gs != "")
                {
                    Warning(gs);
                }
                else
                {
                    //IQueryable<tblPhanQuyen> setup = from x in db.tblPhanQuyens
                    //                                 where x.UserName.ToUpper() == dlNguoiDung.SelectedValue.ToUpper()
                    //                                 && x.Tag.Equals(TagName)
                    //                                 //&& x.IDChiNhanh == new Guid(Session["IDDN"].ToString())
                    //                                 select x;
                    //if (setup.Count() > 0)
                    //{
                    //    Warning(" - Tài khoản này đã được phân quyền bời người quản lý. Xin vui lòng kiểm tra lại.");
                    //}
                    //else
                    //{
                    IQueryable<tblUserAccount> setup = from x in db.tblUserAccounts
                                                       where x.UserName.ToUpper() == Session["IDND"].ToString()
                                                       && x.LoaiTaiKhoan == 2
                                                       //&& x.IDChiNhanh == new Guid(Session["IDDN"].ToString())
                                                       select x;
                    if (setup.Count() == 0)
                    {
                        gstGetMess("Bạn không có quyền truy cập trang này.", "default.aspx");
                    }
                    else
                    {
                        if (khoataikhoan(dlNguoiDung.SelectedValue) == "")
                        {
                            List<tblPhanQuyen> phanquyennguoidung = new List<tblPhanQuyen>();
                            string name = TabContainer1.ActiveTab.ID.ToString();
                            LuuQuyen(phanquyennguoidung, name);
                            IQueryable<tblPhanQuyen> xoa = from p in db.tblPhanQuyens
                                                           where p.IDChiNhanh == GetDrop(dlChiNhanh)
                                                           && p.UserName == dlNguoiDung.SelectedValue
                                                           && p.Tag == name
                                                           select p;
                            db.tblPhanQuyens.DeleteAllOnSubmit(xoa);
                            db.tblPhanQuyens.InsertAllOnSubmit(phanquyennguoidung);
                            db.SubmitChanges();
                            Success("Lưu thành công");
                        }
                    }
                    //}
                }
            }
            catch (Exception ax)
            {
                gstGetMess(ax.Message, "");
            }
        }
        private void LuuQuyen(List<tblPhanQuyen> phanquyennguoidung, string tag)
        {
            if (Them.Visible == true && ckThem.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 1, "Thêm"));
            }

            if (Sua.Visible == true && ckSua.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 2, "Sửa"));
            }

            if (Xoa.Visible == true && ckXoa.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 3, "Xóa"));
            }

            if (Xem.Visible == true && ckXem.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 4, "Xem"));
            }

            if (Xuat.Visible == true && ckXuat.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 5, "Xuất"));
            }

            if (Chot.Visible == true && ckChot.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 6, "Chốt"));
            }

            if (MoChot.Visible == true && ckMoChot.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 7, "Mở chốt"));
            }

            if (ThanhLy.Visible == true && ckThanhLy.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 8, "Thanh lý"));
            }

            if (MoThanhLy.Visible == true && ckMoThanhLy.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 9, "Mở thanh lý"));
            }

            if (ThuCap.Visible == true && ckThuCap.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 10, "Duyệt"));
            }

            if (DuyetXoa.Visible == true && ckDuyetXoa.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 11, "Duyệt xóa"));
            }

            if (DuyetChot.Visible == true && ckDuyetChot.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 12, "Duyệt chốt"));
            }

            if (DuyetMoChot.Visible == true && ckDuyetMoChot.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 13, "Duyệt mở chốt"));
            }

            if (DuyetThanhLy.Visible == true && ckDuyetThanhLy.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 14, "Duyệt thanh lý"));
            }

            if (DuyetMoThanhLy.Visible == true && ckDuyetMoThanhLy.Checked == true)
            {
                phanquyennguoidung.Add(AddtblPhanQuyen(tag, 15, "Duyệt mở thanh lý"));
            }
        }
        private tblPhanQuyen AddtblPhanQuyen(string tag, int LoaiQuyen, string TenLoaiQuyen)
        {
            tblPhanQuyen them = new tblPhanQuyen()
            {
                ID = Guid.NewGuid(),
                IDChiNhanh = GetDrop(dlChiNhanh),
                UserName = dlNguoiDung.SelectedValue,
                LoaiQuyen = LoaiQuyen,
                Tag = tag,
                TenLoaiQuyen = TenLoaiQuyen
            };
            return them;
        }
    }
}