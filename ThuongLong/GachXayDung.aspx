﻿<%@ Page Title="Sản xuất gạch xây dựng" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GachXayDung.aspx.cs" Inherits="ThuongLong.GachXayDung" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày tháng</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Loại gạch</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại gạch"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiGach" runat="server" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div2" runat="server">
                        <label for="exampleInputEmail1">Thời tiết</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Thời tiết"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThoiTiet" runat="server" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Nắng</asp:ListItem>
                            <asp:ListItem Value="1">Mưa</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số lượng đạt</label>
                        <asp:TextBox ID="txtSoLuong" runat="server" class="form-control" placeholder="Số viên loại(lớp mặt)" Width="98%" OnTextChanged="txtSoLuong_TextChanged" AutoPostBack="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtSoLuong" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số lượng hao hụt</label>
                        <asp:TextBox ID="txtSoLuongHaoHut" runat="server" class="form-control" placeholder="Số viên loại(lớp mặt)" Width="98%" OnTextChanged="txtSoLuong_TextChanged" AutoPostBack="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtSoLuongHaoHut" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xi măng</label>
                        <asp:TextBox ID="txtKLXiMang" runat="server" class="form-control" placeholder="Xi măng" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLXiMang" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Cát</label>
                        <asp:TextBox ID="txtKLCat" runat="server" class="form-control" placeholder="Cát" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLCat" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá mạt</label>
                        <asp:TextBox ID="txtKLDaMat" runat="server" class="form-control" placeholder="Mạt đá" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLDaMat" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Vật liệu khác</label>
                        <asp:TextBox ID="txtKLVLKhac" runat="server" class="form-control" placeholder="Vật liệu khác" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLVLKhac" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ghi chú</label>
                        <asp:TextBox ID="txtGhiChu" runat="server" class="form-control" placeholder="Ghi chú"></asp:TextBox>
                    </div>

                    <div id="divan" runat="server" visible="false">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Xi măng PCB40</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMang" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Cát sông đà</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCat" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Mạt đá</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDaMat" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">VL khác</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlVLKhac"
                                runat="server">
                            </asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Xi măng</label>
                            <asp:TextBox ID="txtDMXiMang" runat="server" class="form-control" placeholder="Xi măng PCB40 1" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDMXiMang" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Cát</label>
                            <asp:TextBox ID="txtDMCat" runat="server" class="form-control" placeholder="Cát sông đà" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDMCat" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Mạt đá</label>
                            <asp:TextBox ID="txtDMDaMat" runat="server" class="form-control" placeholder="Mạt đá" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDMDaMat" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">VL khác</label>
                            <asp:TextBox ID="txtDMVLKhac" runat="server" class="form-control" placeholder="Xi măng PCB40 2" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDMVLKhac" ValidChars=".," />
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                    <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách nhập gạch</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-warning" OnClick="btnPrint_Click" Visible="false"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                            <%--<asp:LinkButton ID="btnOpenChot" runat="server" class="btn btn-app bg-orange" OnClick="btnOpenChot_Click"><i class="fa fa-close"></i>Chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnOpenMoChot" runat="server" class="btn btn-app bg-aqua" OnClick="btnOpenMoChot_Click"><i class="fa fa-opencart"></i>Mở chốt</asp:LinkButton>--%>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12" style="overflow: auto; width: 100%;">
                        <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" Width="100%" OnRowCreated="GV_RowCreated" ShowFooter="true"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8px" ItemStyle-Width="8px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8px" ItemStyle-Width="8px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại gạch" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuong" HeaderText="Số lượng đạt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuongHaoHut" HeaderText="Số lượng hao hụt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLXiMang" HeaderText="Xi măng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLCat" HeaderText="Cát" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLDaMat" HeaderText="Đá mạt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLVLKhac" HeaderText="Vật liệu khác" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="GhiChu" HeaderText="Ghi chú" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdCan" runat="server" Value="" />
            <asp:HiddenField ID="hdChot" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:TemplateField HeaderText="Trạng thái" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại gạch" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuong" HeaderText="Số lượng đạt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuongHaoHut" HeaderText="Số lượng hao hụt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLXiMang" HeaderText="Xi măng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLCat" HeaderText="Cát" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLDaMat" HeaderText="Đá mạt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLVLKhac" HeaderText="Vật liệu khác" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="GhiChu" HeaderText="Ghi chú" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpChot" runat="server" CancelControlID="btnDongChot"
                Drag="True" TargetControlID="hdChot" BackgroundCssClass="modalBackground" PopupControlID="pnChot"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnChot" runat="server" Style="width: 98%; height: 100%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        <asp:Label ID="lblChot" runat="server" Text="Chốt nhập kho vật liệu"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh chốt"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhChot" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày chốt</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayChot" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <asp:LinkButton ID="lbtSearchChot" runat="server" class="btn btn-app bg-green" OnClick="lbtSearchChot_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnChot" runat="server" class="btn btn-app bg-orange" OnClick="btnChot_Click"><i class="fa fa-close"></i>Chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnMoChot" runat="server" class="btn btn-app bg-aqua" OnClick="btnMoChot_Click"><i class="fa fa-opencart"></i>Mở chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnDongChot" runat="server" class="btn btn-app bg-warning"><i class="fa fa-opencart"></i>Đóng</asp:LinkButton>
                        </div>
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="GVChot" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound" ShowFooter="true" OnRowCreated="GVChot_RowCreated"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Trạng thái" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại gạch" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuong" HeaderText="Số lượng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLXiMang" HeaderText="Xi măng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLCat" HeaderText="Cát" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLDaMat" HeaderText="Đá mạt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KLVLKhac" HeaderText="Vật liệu khác" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="GhiChu" HeaderText="Ghi chú" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
