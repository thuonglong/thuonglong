﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class XeVanChuyen : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        ThietBiDataContext tb = new ThietBiDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmXeVanChuyen";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmXeVanChuyen", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNhomThietBi();
                        LoadLoaiThietBi();
                        hdPage.Value = "1";
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNhomThietBi()
        {
            var query = tb.sp_XeVanChuyen_LoadNhomThietBi();
            dlNhomThietBi.DataSource = query;
            dlNhomThietBi.DataBind();
            dlNhomThietBi.Items.Insert(0, new ListItem("--Chọn nhóm tài sản--", ""));
        }
        protected void LoadLoaiThietBi()
        {
            dlLoaiThietBi.Items.Clear();
            var query = db.sp_LoadLoaiThietBiByNhom(GetDrop(dlNhomThietBi));
            dlLoaiThietBi.DataSource = query;
            dlLoaiThietBi.DataBind();
            dlLoaiThietBi.Items.Insert(0, new ListItem("--Chọn loại tài sản--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";

            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhomThietBi.SelectedValue == "")
            {
                s += " - Chọn nhóm tài sản<br />";
            }
            if (dlLoaiThietBi.SelectedValue == "")
            {
                s += " - Chọn loại tài sản<br />";
            }
            if (txtTenThietBi.Text.Trim() == "")
            {
                s += " - Nhập tên tài sản<br />";
            }
            if (s == "")
            {
                tb.sp_XeVanChuyen_CheckThem(GetDrop(dlChiNhanh), GetDrop(dlNhomThietBi), GetDrop(dlLoaiThietBi), txtTenThietBi.Text.Trim().ToUpper(), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhomThietBi.SelectedValue == "")
            {
                s += " - Chọn nhóm tài sản<br />";
            }
            if (dlLoaiThietBi.SelectedValue == "")
            {
                s += " - Chọn loại tài sản<br />";
            }
            if (txtTenThietBi.Text.Trim() == "")
            {
                s += " - Nhập tên tài sản<br />";
            }
            if (s == "")
            {
                tb.sp_XeVanChuyen_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhomThietBi), GetDrop(dlLoaiThietBi), txtTenThietBi.Text.Trim().ToUpper(), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var XeVanChuyen = new tblXeVanChuyen()
                        {
                            ID = Guid.NewGuid(),
                            TenThietBi = txtTenThietBi.Text.Trim().ToUpper(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhomThietBi = GetDrop(dlNhomThietBi),
                            IDLoaiThietBi = GetDrop(dlLoaiThietBi),
                            IsTrangThai = int.Parse(dlTrangThai.SelectedValue),
                            TenIsTrangThai = dlTrangThai.SelectedItem.Text.ToString(),
                            TrongLuong = txtTrongLuong.Text == "" ? 0 : double.Parse(txtTrongLuong.Text),
                            KhoiLuong = txtKhoiLuong.Text == "" ? 0 : double.Parse(txtKhoiLuong.Text),
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblXeVanChuyens.InsertOnSubmit(XeVanChuyen);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblXeVanChuyens
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            //LoaiNguon =   ,
                            //IDNguon =   ,
                            query.TenThietBi = txtTenThietBi.Text.Trim().ToUpper();
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhomThietBi = GetDrop(dlNhomThietBi);
                            query.IDLoaiThietBi = GetDrop(dlLoaiThietBi);
                            query.IsTrangThai = int.Parse(dlTrangThai.SelectedValue);
                            query.TenIsTrangThai = dlTrangThai.SelectedItem.Text.ToString();
                            query.TrongLuong = txtTrongLuong.Text == "" ? 0 : double.Parse(txtTrongLuong.Text);
                            query.KhoiLuong = txtKhoiLuong.Text == "" ? 0 : double.Parse(txtKhoiLuong.Text);
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.STT = 0;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin tài sản đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            hdID.Value = "";
            dlChiNhanh.SelectedIndex = 0;
            dlNhomThietBi.SelectedIndex = 0;
            LoadLoaiThietBi();
            txtTenThietBi.Text = "";
            txtTrongLuong.Text = "";
            txtKhoiLuong.Text = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblXeVanChuyens
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtTenThietBi.Text = query.TenThietBi;
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhomThietBi.SelectedValue = query.IDNhomThietBi.ToString();
                        LoadLoaiThietBi();
                        dlLoaiThietBi.SelectedValue = query.IDLoaiThietBi.ToString();
                        dlTrangThai.SelectedValue = query.IsTrangThai.ToString();
                        txtTrongLuong.Text = query.TrongLuong.ToString();
                        txtKhoiLuong.Text = query.KhoiLuong.ToString();
                        hdID.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        int tontai = (from p in db.tblNguoiVanHanhs
                                      where p.IDThietBi == new Guid(id)
                                      select p).Count();
                        if (tontai > 0)
                            Warning("Thông tin tài sản đã được sử dụng tại người vận hành");
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblXeVanChuyen_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblXeVanChuyens.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = tb.sp_XeVanChuyen_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin tài sản đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void dlNhomThietBi_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiThietBi();
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void LoadDl1()
        {
            List<sp_XeVanChuyen_LoadDl1Result> query = tb.sp_XeVanChuyen_LoadDl1(GetDrop(dlChiNhanhSearch),
             dlXemTheo.SelectedValue).ToList();

            if (query != null)
            {
                dl1.Items.Clear();
                dl1.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL1,
                                      Ten = p.TenDL1
                                  }).Distinct().OrderBy(p => p.Ten);
                dl1.DataBind();
                dl1.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl2.Items.Clear();
                dl2.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL2,
                                      Ten = p.TenDL2
                                  }).Distinct().OrderBy(p => p.Ten);
                dl2.DataBind();
                dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        private void LoadDl2()
        {
            List<sp_XeVanChuyen_LoadDl2Result> query = tb.sp_XeVanChuyen_LoadDl2(GetDrop(dlChiNhanhSearch), dl1.SelectedValue,
             dlXemTheo.SelectedValue).ToList();

            if (query != null)
            {
                dl2.Items.Clear();
                dl2.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL2,
                                      Ten = p.TenDL2
                                  }).Distinct().OrderBy(p => p.Ten);
                dl2.DataBind();
                dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        private void LoadDl3()
        {
            List<sp_XeVanChuyen_LoadDl3Result> query = tb.sp_XeVanChuyen_LoadDl3(GetDrop(dlChiNhanhSearch), dl1.SelectedValue, dl2.SelectedValue,
             dlXemTheo.SelectedValue).ToList();

            if (query != null)
            {
                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }

        protected void dl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dl2.Items.Clear();
            dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

            dl3.Items.Clear();
            dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

            if (dl1.SelectedValue != "")
                LoadDl2();
        }
        protected void dl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dl3.Items.Clear();
            dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

            if (dl2.SelectedValue != "")
                LoadDl3();
        }
        protected void dlXemTheo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlXemTheo.SelectedValue == "0")
            {
                divdl.Visible = false;
            }
            else if (dlXemTheo.SelectedValue == "1")
            {
                lbl1.Text = "Nhóm tài sản";
                lbl2.Text = "Loại tài sản";
                lbl3.Text = "Trạng thái";

                divdl.Visible = true;
                LoadDl1();
            }
            else if (dlXemTheo.SelectedValue == "2")
            {
                lbl1.Text = "Loại tài sản";
                lbl2.Text = "Trạng thái";
                lbl3.Text = "Nhóm tài sản";

                divdl.Visible = true;
                LoadDl1();
            }
            else if (dlXemTheo.SelectedValue == "3")
            {
                lbl1.Text = "Trạng thái";
                lbl2.Text = "Nhóm tài sản";
                lbl3.Text = "Loại tài sản";

                divdl.Visible = true;
                LoadDl1();
            }
        }
        private void Search(int page)
        {
            string index = "";
            if (dlXemTheo.SelectedValue == "0" || dl1.SelectedValue == "")
                index = "0";
            else if (dl3.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "3";
            }
            else if (dl2.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "2";
            }
            else if (dl1.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "1";
            }
            var query = tb.sp_XeVanChuyen_Search(GetDrop(dlChiNhanhSearch), dl1.SelectedValue, dl2.SelectedValue, dl3.SelectedValue,
            index, 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
        }
    }
}