﻿<%@ Page Title="Nhập kho vật liệu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NhapKhoVatLieu.aspx.cs" Inherits="ThuongLong.NhapKhoVatLieu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin nhập kho</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày tháng</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Nhà cung cấp</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCap" OnSelectedIndexChanged="dlNhaCungCap_SelectedIndexChanged" AutoPostBack="true"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số HĐ</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Số HĐ"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlIDHDong" OnSelectedIndexChanged="dlIDHDong_SelectedIndexChanged" AutoPostBack="true"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Nhóm vật liệu</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm vật liệu" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhomVatLieu" runat="server" OnSelectedIndexChanged="dlNhomVatLieu_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Loại vật liệu</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại vật liệu"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiVatLieu" runat="server" OnSelectedIndexChanged="dlLoaiVatLieu_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn vị tính</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDonViTinh" runat="server" OnSelectedIndexChanged="dlDonViTinh_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">KL mua</label>
                        <asp:TextBox ID="txtKLMua" runat="server" class="form-control" placeholder="KL mua" Width="98%" onchange="KLMua()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLMua" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">KL nhập kho</label>
                        <asp:TextBox ID="txtKLNhapKho" runat="server" class="form-control" placeholder="KL nhập kho" Width="98%"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKLNhapKho" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá hóa đơn</label>
                        <asp:TextBox ID="txtDonGiaCoThue" runat="server" class="form-control" placeholder="Đơn giá hóa đơn" Width="98%" disabled></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đơn giá thanh toán</label>
                        <asp:TextBox ID="txtDonGiaKhongThue" runat="server" class="form-control" placeholder="Đơn giá thanh toán" Width="98%" disabled></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Thành tiền hóa đơn</label>
                        <asp:TextBox ID="txtThanhTienCoThue" runat="server" class="form-control" placeholder="Thành tiền hóa đơn" Width="98%" disabled></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Thành tiền thanh toán</label>
                        <asp:TextBox ID="txtThanhTienKhongThue" runat="server" class="form-control" placeholder="Thành tiền thanh toán" Width="98%" disabled></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3" id="divChonPhieuCan" runat="server">
                        <div class="input-group margin">
                            <div class="input-group-btn">
                                <asp:Button ID="btnCan" runat="server" Text="Chọn" class="btn btn-info" OnClick="btnCan_Click" />
                            </div>
                            <asp:TextBox ID="txtSoPhieuCan" runat="server" class="form-control" placeholder="Phiếu cân" Width="98%" ReadOnly="true"></asp:TextBox>
                            <div class="input-group-btn">
                                <asp:Button ID="btnResetCan" runat="server" Text="Reset" class="btn btn-info" OnClick="btnResetCan_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Biển số xe</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlBienSoXe" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Lái xe</label>
                        <asp:TextBox ID="txtTenLaiXe" runat="server" class="form-control" placeholder="Lái xe" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">TL tổng</label>
                        <asp:TextBox ID="txtTLTong" runat="server" class="form-control" placeholder="TL tổng" Width="98%" Text="0" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLTong" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">TL bì</label>
                        <asp:TextBox ID="txtTLBi" runat="server" class="form-control" placeholder="TL bì" Width="98%" Text="0" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLBi" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">TL hàng</label>
                        <asp:TextBox ID="txtTLHang" runat="server" class="form-control" placeholder="TL hàng" Width="98%" Text="0" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Tỷ lệ quy đổi</label>
                        <asp:TextBox ID="txtTyLeQuyDoi" runat="server" class="form-control" placeholder="Tỷ lệ quy đổi" Width="98%" Text="0" onchange="KLQuaCan()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTyLeQuyDoi" ValidChars="." />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">KL qua cân</label>
                        <asp:TextBox ID="txtKLQuaCan" runat="server" class="form-control" placeholder="KL qua cân" Width="98%" Text="0" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ghi chú</label>
                        <asp:TextBox ID="txtMoTa" runat="server" class="form-control" placeholder="Ghi chú" Width="98%"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">KM vận chuyển</label>
                        <asp:TextBox ID="txtKMVanChuyen" runat="server" class="form-control" placeholder="KL mua" Width="98%" onchange="KLMua()"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtKMVanChuyen" ValidChars=".," />
                    </div>
                    <%--<div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Hình ảnh</label>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </div>--%>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                    <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách nhập kho</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Nhà cung cấp</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCapSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <%--<asp:LinkButton ID="btnResetFilter" runat="server" class="btn btn-app bg-warning" OnClick="btnResetFilter_Click"><i class="fa fa-filter"></i>Lọc</asp:LinkButton>--%>
                            <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click" ><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                            <asp:LinkButton ID="btnOpenChot" runat="server" class="btn btn-app bg-green" OnClick="btnOpenChot_Click"><i class="fa fa-close"></i>Chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnOpenMoChot" runat="server" class="btn btn-app bg-warning" OnClick="btnOpenMoChot_Click"><i class="fa fa-opencart"></i>Mở chốt</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="box-body" id="divdl" visible="false" runat="server">
                    <div class="col-md-12">
                        <%--<div class="form-group col-lg-3">
                            <asp:Label ID="lbl1" runat="server">Nhà cung cấp</asp:Label><br />
                            <asp:ListBox ID="dlNhaCungCapSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>--%>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl2" runat="server">Nhóm vật liệu</asp:Label><br />
                            <asp:ListBox ID="dlNhomVatLieuSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl3" runat="server">Loại vật liệu</asp:Label><br />
                            <asp:ListBox ID="dlLoaiVatLieuSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="lbl4" runat="server">Đơn vị tính</asp:Label><br />
                            <asp:ListBox ID="dlDonViTinhSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label1" runat="server">Biển số xe</asp:Label><br />
                            <asp:ListBox ID="dlBienSoXeSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Label ID="Label2" runat="server">Lái xe</asp:Label><br />
                            <asp:ListBox ID="dlLaiXeSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                                Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12" style="overflow: auto; width: 100%;">
                        <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" Width="2800px" OnRowCreated="GV_RowCreated" ShowFooter="true"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="SoPhieu" HeaderText="Số phiếu" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="NCC vật liệu" />
                                <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                                <asp:BoundField DataField="TenDonViTinh" HeaderText="ĐVT" />
                                <asp:BoundField DataField="TenBienSoXe" HeaderText="Biển số xe" />
                                <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                                <asp:BoundField DataField="KLMua" HeaderText="KL mua" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KLNhapKho" HeaderText="KL nhập kho" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KLQuanCan" HeaderText="KL qua cân" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLTong" HeaderText="TL tổng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLBi" HeaderText="TL bì" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLHang" HeaderText="TL hàng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TyLeQuyDoi" HeaderText="Tỷ lệ quy đổi" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="MoTa" HeaderText="Ghi chú" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function KLMua() {
                    var txtDonGiaCoThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaCoThue.ClientID %>").value));
                    var txtDonGiaKhongThue = parseFloat(getvalue(document.getElementById("<%=txtDonGiaKhongThue.ClientID %>").value));
                    var txtKLMua = parseFloat(getvalue(document.getElementById("<%=txtKLMua.ClientID %>").value));
                    document.getElementById("<%=txtThanhTienCoThue.ClientID %>").value = (txtKLMua * txtDonGiaCoThue).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    document.getElementById("<%=txtThanhTienKhongThue.ClientID %>").value = (txtKLMua * txtDonGiaKhongThue).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                }
                function KLQuaCan() {
                    var txtTLTong = parseFloat(getvalue(document.getElementById("<%=txtTLTong.ClientID %>").value));
                    var txtTLBi = parseFloat(getvalue(document.getElementById("<%=txtTLBi.ClientID %>").value));
                    var txtTLHang = parseFloat(getvalue(document.getElementById("<%=txtTLHang.ClientID %>").value));

                    var txtTyLeQuyDoi = parseFloat(getvalue(document.getElementById("<%=txtTyLeQuyDoi.ClientID %>").value));
                    if (txtTyLeQuyDoi > 0)
                        document.getElementById("<%=txtKLQuaCan.ClientID %>").value = (txtTLHang / txtTyLeQuyDoi).toFixed(1).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdCan" runat="server" Value="" />
            <asp:HiddenField ID="hdChot" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="SoPhieu" HeaderText="Số phiếu" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="NCC vật liệu" />
                                <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                                <asp:BoundField DataField="TenDonViTinh" HeaderText="ĐVT" />
                                <asp:BoundField DataField="TenBienSoXe" HeaderText="Biển số xe" />
                                <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                                <asp:BoundField DataField="KLMua" HeaderText="KL mua" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KLNhapKho" HeaderText="KL nhập kho" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KLQuanCan" HeaderText="KL qua cân" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLTong" HeaderText="TL tổng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLBi" HeaderText="TL bì" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLHang" HeaderText="TL hàng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TyLeQuyDoi" HeaderText="Tỷ lệ quy đổi" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="MoTa" HeaderText="Ghi chú" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpChot" runat="server" CancelControlID="btnDongChot"
                Drag="True" TargetControlID="hdChot" BackgroundCssClass="modalBackground" PopupControlID="pnChot"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnChot" runat="server" Style="width: 98%; height: 100%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        <asp:Label ID="lblChot" runat="server" Text="Chốt nhập kho vật liệu"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh chốt</label>
                            <asp:DropDownList CssClass="form-control" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh chốt"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhChot" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày chốt</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayChot" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:Button ID="lbtSearchChot" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="lbtSearchChot_Click" />
                            <asp:Button ID="btnChot" runat="server" Text="Chốt" CssClass="btn btn-primary" OnClick="btnChot_Click" />
                            <asp:Button ID="btnMoChot" runat="server" Text="Mở chốt" CssClass="btn btn-primary" OnClick="btnMoChot_Click" />
                            <asp:Button ID="btnDongChot" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                        </div>
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="GVChot" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound" ShowFooter="true" OnRowCreated="GVChot_RowCreated"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px" Width="2300px">
                            <Columns>
                                <asp:BoundField DataField="TrangThaiText" HeaderText="Trạng thái" />
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="SoPhieu" HeaderText="Số phiếu" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="NCC vật liệu" />
                                <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                                <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                                <asp:BoundField DataField="TenDonViTinh" HeaderText="ĐVT" />
                                <asp:BoundField DataField="TenBienSoXe" HeaderText="Biển số xe" />
                                <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                                <asp:BoundField DataField="KLMua" HeaderText="KL mua" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KLNhapKho" HeaderText="KL nhập kho" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KLQuanCan" HeaderText="KL qua cân" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLTong" HeaderText="TL tổng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLBi" HeaderText="TL bì" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLHang" HeaderText="TL hàng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TyLeQuyDoi" HeaderText="Tỷ lệ quy đổi" ItemStyle-HorizontalAlign="Right" />
                                <%--<asp:BoundField DataField="MoTa" HeaderText="Ghi chú" />--%>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpCan" runat="server" CancelControlID="btnDongCan"
                Drag="True" TargetControlID="hdCan" BackgroundCssClass="modalBackground" PopupControlID="pnCan"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnCan" runat="server" Style="width: 80%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Danh sách phiếu cân
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvCan" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound" OnRowCommand="gvCan_RowCommand"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="SoPhieu" HeaderText="Số phiếu cân" />
                                <asp:BoundField DataField="TGian2" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="NguoiBan" HeaderText="Người bán" />
                                <asp:BoundField DataField="NguoiMua" HeaderText="Người mua" />
                                <asp:BoundField DataField="LoaiHang" HeaderText="Loại hàng" />
                                <asp:BoundField DataField="BienSoXe" HeaderText="Biển số xe" />
                                <asp:TemplateField ItemStyle-Width="150" HeaderText="Lái xe">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLaiXeCan" runat="server" Text='<%# Eval("LaiXe") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="KlCanL1" HeaderText="Trọng lượng tổng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="KlCanL2" HeaderText="Trọng lượng bì" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TLHang" HeaderText="Trọng lượng hàng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:TemplateField HeaderText="Chọn" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtChon" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("IDMN")%>' ToolTip="Chọn" CommandName="Chon">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongCan" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <asp:UpdateProgress ID="UpdateProgress1"
                AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_initializeRequest(InitializeRequest);
                prm.add_endRequest(EndRequest);
                var postBackElement;
                function InitializeRequest(sender, args) {
                    if (prm.get_isInAsyncPostBack())
                        args.set_cancel(true);
                    postBackElement = args.get_postBackElement();

                    if (postBackElement.id == 'lbtLuuHopDong')
                        $get('UpdateProgress1').style.display = 'block';
                }
                function EndRequest(sender, args) {
                    if (postBackElement.id == 'lbtLuuHopDong')
                        $get('UpdateProgress1').style.display = 'none';
                }
            </script>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtLuuHopDong" />--%>
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
