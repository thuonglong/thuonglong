﻿<%@ Page Title="Báo cáo vật liệu tiêu hao" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BaoCaoVatLieuTieuhao.aspx.cs" Inherits="ThuongLong.BaoCaoVatLieuTieuhao" %>

<%@ Register Src="UCVatLieuTieuHao/uc_BaoCaoVatLieuTieuHaoBeTong.ascx" TagName="uc_BaoCaoVatLieuTieuHaoBeTong" TagPrefix="uc1" %>
<%@ Register Src="UCVatLieuTieuHao/uc_BaoCaoVatLieuTieuHaoGach_MenBong.ascx" TagName="uc_BaoCaoVatLieuTieuHaoGach_MenBong" TagPrefix="uc2" %>
<%@ Register Src="UCVatLieuTieuHao/uc_BaoCaoVatLieuTieuHaoGach_Terrazo.ascx" TagName="uc_BaoCaoVatLieuTieuHaoGach_Terrazo" TagPrefix="uc3" %>
<%@ Register Src="UCVatLieuTieuHao/uc_BaoCaoVatLieuTieuHaoGach_XayDung.ascx" TagName="uc_BaoCaoVatLieuTieuHaoGach_XayDung" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmBaoCaoDoanhThuBanGach" runat="server" HeaderText="Sản xuất bê tông">
                            <ContentTemplate>
                                <uc1:uc_BaoCaoVatLieuTieuHaoBeTong ID="uc_BaoCaoVatLieuTieuHaoBeTong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Gạch men bóng">
                            <ContentTemplate>
                                <uc2:uc_BaoCaoVatLieuTieuHaoGach_MenBong ID="uc_BaoCaoVatLieuTieuHaoGach_MenBong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Gạch Terrazo">
                            <ContentTemplate>
                                <uc3:uc_BaoCaoVatLieuTieuHaoGach_Terrazo ID="uc_BaoCaoVatLieuTieuHaoGach_Terrazo1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="Gạch xây dựng">
                            <ContentTemplate>
                                <uc4:uc_BaoCaoVatLieuTieuHaoGach_XayDung ID="uc_BaoCaoVatLieuTieuHaoGach_XayDung1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
