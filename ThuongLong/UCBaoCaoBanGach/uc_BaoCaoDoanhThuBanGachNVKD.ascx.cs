﻿using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCBaoCaoBanGach
{
    public partial class uc_BaoCaoDoanhThuBanGachNVKD : System.Web.UI.UserControl
    {
        private string frmName = "frmBaoCaoBanGach";
        private static int PageNumber = 20;
        private static int CurrentPage = 1;
        private DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        private clsPhanQuyen phanquyen = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else if (!IsPostBack)
            {
                DateTime dateTimenow = DateTime.Now;
                var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                txtTuNgay.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                txtDenNgay.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                LoadChiNhanh();
                LoadNhaCungCap();
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Tất cả chi nhánh", ""));
        }
        void LoadNhaCungCap()
        {
            dlNVKD.Items.Clear();
            var query = vatlieu.sp_BaoCaoBanGach_NVKD_LoadNVKD(dlChiNhanh.SelectedValue, DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text)));
            dlNVKD.DataSource = query;
            dlNVKD.DataBind();
            dlNVKD.Items.Insert(0, new ListItem("Tất cả nhân viên", ""));
        }
        public DateTime GetLastDayOfMonth(int iMonth, int iYear)
        {
            DateTime dtResult = new DateTime(iYear, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-1);
            return dtResult;
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }

        private Guid GetDrop(DropDownList dl)
        {
            if (dl.SelectedValue.Trim().Equals(""))
            {
                return Guid.Empty;
            }
            else
            {
                return new Guid(dl.SelectedValue);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        private void Search(int page)
        {
            List<sp_BaoCaoBanGach_NVKD_SearchResult> dt = vatlieu.sp_BaoCaoBanGach_NVKD_Search(dlChiNhanh.SelectedValue, dlNVKD.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text)), PageNumber, page).ToList();
            GV.DataSource = dt;
            GV.DataBind();
            hdTuNgay.Value = txtTuNgay.Text;
            hdDenNgay.Value = txtDenNgay.Text;
            if (GV.Rows.Count > 0)
            {

                sp_BaoCaoBanGach_NVKD_SearchFooterResult query = vatlieu.sp_BaoCaoBanGach_NVKD_SearchFooter(dlChiNhanh.SelectedValue, dlNVKD.SelectedValue,
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text))).FirstOrDefault();

                if (query != null)
                {
                    GV.FooterRow.Cells[0].ColumnSpan = 2;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng:" ;
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    GV.FooterRow.Cells[1].Text = string.Format("{0:N2}", query.SoLuongNhan);
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N2}", query.SoLuongThucXuat);

                    GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", query.ThanhTienCoThue);
                    GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", query.ThanhTienKhongThue);
                    GV.FooterRow.Cells[5].Text = string.Format("{0:N0}", query.PhuPhi);
                    GV.FooterRow.Cells[6].Text = string.Format("{0:N0}", query.TongTienCoThue);
                    GV.FooterRow.Cells[7].Text = string.Format("{0:N0}", query.TongTienKhongThue);

                }
            }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_BaoCaoBanGach_NVKD_PrintResult> query = vatlieu.sp_BaoCaoBanGach_NVKD_Print(dlChiNhanh.SelectedValue, dlNVKD.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text))).ToList();

            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "BÁO CÁO BÁN GẠCH";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 5);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 5);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgay.Text != txtDenNgay.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO BÁN GẠCH TỪ NGÀY" + " " + txtTuNgay.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgay.Text.Trim());
                }
                else if (txtTuNgay.Text == txtDenNgay.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO BÁN GẠCH NGÀY" + " " + txtTuNgay.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 9);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 9);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Chi nhánh",
                    "Nhân viên kinh doanh",
                    "Thông tin số lượng",
                    "Thông tin số lượng",
                    "Thành tiền",
                    "Thành tiền",
                    "Phụ phí",
                    "Tổng tiền",
                    "Tổng tiền"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Chi nhánh",
                    "Nhân viên kinh doanh",
                    "Số lượng nhận",
                    "Số lượng xuất kho",
                    "Hóa đơn",
                    "Thanh toán",
                    "Phụ phí",
                    "Hóa đơn",
                    "Thanh toán"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 3, 4);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 5, 6);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 7, 7);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 8, 9);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.TenNhanVien);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(double.Parse(item.SoLuongNhan.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(double.Parse(item.SoLuongThucXuat.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(double.Parse(item.ThanhTienCoThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(double.Parse(item.ThanhTienKhongThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(double.Parse(item.PhuPhi.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(double.Parse(item.TongTienCoThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(double.Parse(item.TongTienKhongThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= header2.Length; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 8);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(3);
                cell.CellFormula = "SUM(D" + RowDau.ToString() + ":D" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(4);
                cell.CellFormula = "SUM(E" + RowDau.ToString() + ":E" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(5);
                cell.CellFormula = "SUM(F" + RowDau.ToString() + ":F" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(6);
                cell.CellFormula = "SUM(G" + RowDau.ToString() + ":G" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(7);
                cell.CellFormula = "SUM(H" + RowDau.ToString() + ":H" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(8);
                cell.CellFormula = "SUM(I" + RowDau.ToString() + ":I" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(9);
                cell.CellFormula = "SUM(J" + RowDau.ToString() + ":J" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                #endregion

                #region[Set Footer]

                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanh.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                #endregion

                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgay.Text.Trim() == txtDenNgay.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO BÁN GẠCH -" + Bienso + "-Ngày: " + "-" + txtTuNgay.Text +
                                         ".xls";
                    }
                    else if (txtTuNgay.Text.Trim() != txtDenNgay.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO BÁN GẠCH -" + Bienso + "-Từ ngày" + "-" + txtTuNgay.Text +
                                         "-Đến ngày" + "-" + txtDenNgay.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            hdPage.Value = "1";
            Search(CurrentPage);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        protected void grdDanhSachHD_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#EEFFAA'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }
    }
}