﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_BaoCaoDoanhThuBanGach.ascx.cs" Inherits="ThuongLong.UCBaoCaoBanGach.uc_BaoCaoDoanhThuBanGach" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="panel-body">
            <div class="row">
                <div class="form-group col-lg-3">
                    <label for="exampleInputEmail1">Từ ngày</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox ID="txtTuNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""
                            Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group col-lg-3">
                    <label for="exampleInputEmail1">Đến ngày</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox ID="txtDenNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""
                            Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="col-lg-3" runat="server">
                    <div class="form-group">
                        <label>Chi nhánh</label>
                        <asp:DropDownList ID="dlChiNhanh" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2"
                            Width="98%" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3" runat="server">
                    <div class="form-group">
                        <label>Loại khách hàng</label>
                        <asp:DropDownList ID="dlLoaiKhachHang" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%" OnSelectedIndexChanged="dlLoaiKhachHang_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3" runat="server">
                    <div class="form-group">
                        <label>Khách hàng</label>
                        <asp:DropDownList ID="dlNhaCungCap" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Xem</asp:LinkButton>
                        <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" ShowFooter="true"
                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                <Columns>
                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                    <asp:TemplateField HeaderText="Khách hàng">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblLichSu"
                                Text='<%#Eval("TenNhaCungCap")%>' runat="server" CssClass="padding" CommandName="PrintKhachHang"
                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                    <asp:BoundField DataField="TenDonViTinh" HeaderText="Đơn vị tính" />
                    <asp:BoundField DataField="SoPhieu" HeaderText="Số phiếu" />
                    <asp:BoundField DataField="TenBienSoXe" HeaderText="Biển số xe" />
                    <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                    <asp:BoundField DataField="SoLuongNhan" HeaderText="Số lượng nhận" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="SoLuongThucXuat" HeaderText="Số lượng xuất kho" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="DonGiaCoThue" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Thành tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Thành tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                    HorizontalAlign="Right" />
                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
            </asp:GridView>
        </div>
        <div style="margin: 5px;" class="btn-group">
            <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
            <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
            <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
        </div>
        <asp:HiddenField ID="hdID" runat="server" />
        <asp:HiddenField ID="hdPage" runat="server" />
        <asp:HiddenField ID="hdDuyet" runat="server" />
        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
        <asp:HiddenField ID="hdTuNgay" runat="server" />
        <asp:HiddenField ID="hdDenNgay" runat="server" />
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnPrint" />
    </Triggers>
</asp:UpdatePanel>


