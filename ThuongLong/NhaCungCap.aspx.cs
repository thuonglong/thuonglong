﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class NhaCungCap : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmNhaCungCap";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmNhaCungCap", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        hdPage.Value = "1";
                        LoadNhomNhaCungCap();
                        txtSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSearch.ClientID + "')");
                    }
                }
            }
        }
        void LoadNhomNhaCungCap()
        {
            dlNhomNhaCungCap.Items.Clear();
            dlNhomNhaCungCap.DataSource = from p in db.tblNhomNhaCungCaps
                                          where p.TrangThai == 2
                                          orderby p.STT
                                          select new
                                          {
                                              p.ID,
                                              p.Name,
                                          };
            dlNhomNhaCungCap.DataBind();
            dlNhomNhaCungCap.Items.Insert(0, new ListItem("--Chọn nhóm nhà cung cấp/khách hàng--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckLuu()
        {
            string s = "";
            if (dlNhomNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhóm nhà cung cấp/khách hàng<br />";
            }
            if (txtName.Text.Trim() == "")
            {
                s += " - Nhập tên nhà cung cấp/khách hàng<br />";
            }
            if (hdID.Value == "")
            {
                var query = from p in db.tblNhaCungCaps
                            where p.TenNhaCungCap.ToUpper() == txtName.Text.Trim().ToUpper()
                            select p;
                if (query.Count() > 0)
                {
                    s += " - Tên nhà cung cấp/khách hàng này đã tồn tại<br />";
                }
            }
            else
            {
                var query = from p in db.tblNhaCungCaps
                            where p.TenNhaCungCap.ToUpper() == txtName.Text.Trim().ToUpper()
                            && p.ID != new Guid(hdID.Value)
                            select p;
                if (query.Count() > 0)
                {
                    s += " - Tên nhà cung cấp/khách hàng này đã tồn tại<br />";
                }
            }
            if (dlNhomNhaCungCap.SelectedValue != "")
            {
                var checknhomncc = (from p in db.tblNhomNhaCungCaps
                                    where p.ID == GetDrop(dlNhomNhaCungCap)
                                    select p).FirstOrDefault();
                if (checknhomncc != null && checknhomncc.ID != null)
                {
                    if (checknhomncc.TrangThai == 1)
                    {
                        s += " - Nhóm nhà cung cấp/khách hàng đang chờ duyệt.<br />";
                    }
                }
                else
                {
                    s += " - Nhóm nhà cung cấp/khách hàng không tồn tại<br />";
                }
            }
            //if (txtName.Text.Trim() == "")
            //{
            //    s += " - Nhập thứ tự sắp xếp<br />";
            //}
            if (s != "")
                Warning(s);
            return s;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string url = "";
            if (CheckLuu() == "")
            {
                if (hdID.Value == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var NhaCungCap = new tblNhaCungCap()
                        {
                            ID = Guid.NewGuid(),
                            IDNhomNhaCungCap = GetDrop(dlNhomNhaCungCap),
                            TenNhaCungCap = txtName.Text.Trim(),
                            DiaChi = txtDiaChi.Text,
                            Email = txtEmail.Text,
                            SoDienThoai = txtSoDienThoai.Text,
                            MaSoThue = txtMaSoThue.Text,
                            GiamDoc = txtGiamDoc.Text,
                            KeToanTruong = txtKeToanTruong.Text,
                            TrangThai = 2,
                            TrangThaiText = "Đã duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblNhaCungCaps.InsertOnSubmit(NhaCungCap);
                        db.SubmitChanges();
                        btnCancel_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
                else
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblNhaCungCaps
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDNhomNhaCungCap = GetDrop(dlNhomNhaCungCap);
                            query.TenNhaCungCap = txtName.Text.Trim();
                            query.DiaChi = txtDiaChi.Text.Trim();
                            query.Email = txtEmail.Text.Trim();
                            query.SoDienThoai = txtSoDienThoai.Text.Trim();
                            query.MaSoThue = txtMaSoThue.Text.Trim();
                            query.GiamDoc = txtGiamDoc.Text.Trim();
                            query.KeToanTruong = txtKeToanTruong.Text.Trim();
                            query.TrangThai = 2;
                            query.TrangThaiText = "Đã duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            btnCancel_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin tài khoản không tồn tại.");
                            btnCancel_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtName.Text = "";
            //ckStatus.Checked = false;
            dlNhomNhaCungCap.SelectedValue = "";
            txtName.Text = "";
            txtDiaChi.Text = "";
            txtEmail.Text = "";
            txtSoDienThoai.Text = "";
            txtMaSoThue.Text = "";
            txtGiamDoc.Text = "";
            txtKeToanTruong.Text = "";

            txtSearch.Text = "";
            btnSave.Text = "Lưu";
            hdID.Value = "";
        }
        void Search(int page)
        {
            GV.DataSource = db.sp_NhaCungCap_Search(txtSearch.Text.Trim(), 10, page);
            GV.DataBind();
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblNhaCungCaps
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlNhomNhaCungCap.SelectedValue = query.IDNhomNhaCungCap.ToString();
                        txtName.Text = query.TenNhaCungCap;
                        txtDiaChi.Text = query.DiaChi;
                        txtEmail.Text = query.Email;
                        txtSoDienThoai.Text = query.SoDienThoai;
                        txtMaSoThue.Text = query.MaSoThue;
                        txtGiamDoc.Text = query.GiamDoc;
                        txtKeToanTruong.Text = query.KeToanTruong;

                        hdID.Value = id;
                        btnSave.Text = "Cập nhật";
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        db.sp_NhaCungCap_CheckXoa(query.ID, thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            //query.TrangThai = 3;
                            //query.TrangThaiText = "Chờ duyệt xóa";
                            db.tblNhaCungCaps.DeleteOnSubmit(query);
                            db.SubmitChanges();
                            Success("Xóa thành công");
                            btnCancel_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Kích hoạt")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai == 1)
                        {
                            query.TrangThai = 2;
                            query.TrangThaiText = "Đã kích hoạt";
                            db.SubmitChanges();
                            Success("Đã kích hoạt");
                        }
                        else
                        {
                            Success("Đã kích hoạt");
                        }
                        Search(1);
                    }
                }
                else if (e.CommandName == "Khóa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai == 2)
                        {
                            query.TrangThai = 1;
                            query.TrangThaiText = "Đã khóa";
                            db.SubmitChanges();
                            Success("Đã khóa");
                        }
                        else
                        {
                            Success("Đã khóa");
                        }
                        Search(1);
                    }
                }
            }
            else
            {
                Warning("Thông tin nhà cung cấp/khách hàng không tồn tại");
                btnCancel_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            {
                return "";
            }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
    }
}