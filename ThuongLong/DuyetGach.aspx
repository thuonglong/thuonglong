﻿<%@ Page Title="Duyệt gạch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetGach.aspx.cs" Inherits="ThuongLong.DuyetGach" %>

<%@ Register Src="UCGach/uc_DuyetGachTerrazo.ascx" TagName="uc_DuyetGachTerrazo" TagPrefix="uc1" %>
<%@ Register Src="UCGach/uc_DuyetChotGachTerrazo.ascx" TagName="uc_DuyetChotGachTerrazo" TagPrefix="uc2" %>

<%@ Register Src="UCGach/uc_DuyetGachMenBong.ascx" TagName="uc_DuyetGachMenBong" TagPrefix="uc3" %>
<%@ Register Src="UCGach/uc_DuyetChotGachMenBong.ascx" TagName="uc_DuyetChotGachMenBong" TagPrefix="uc4" %>

<%@ Register Src="UCGach/uc_DuyetGachXayDung.ascx" TagName="uc_DuyetGachXayDung" TagPrefix="uc5" %>
<%@ Register Src="UCGach/uc_DuyetChotGachXayDung.ascx" TagName="uc_DuyetChotGachXayDung" TagPrefix="uc6" %>


<%@ Register Src="UCGach/uc_DuyetDinhMucGachTerrazo.ascx" TagName="uc_DuyetDinhMucGachTerrazo" TagPrefix="uc7" %>

<%@ Register Src="UCGach/uc_DuyetDinhMucGachMenBong.ascx" TagName="uc_DuyetDinhMucGachMenBong" TagPrefix="uc8" %>

<%@ Register Src="UCGach/uc_DuyetDinhMucGachXayDung.ascx" TagName="uc_DuyetDinhMucGachXayDung" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">

                        <ajaxToolkit:TabPanel ID="frmDinhMucGachTerrazo" runat="server" HeaderText="Định mức gạch Terrazo">
                            <ContentTemplate>
                                <uc7:uc_DuyetDinhMucGachTerrazo ID="uc_DuyetDinhMucGachTerrazo1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmGachTerrazo" runat="server" HeaderText="Gạch Terrazo">
                            <ContentTemplate>
                                <uc1:uc_DuyetGachTerrazo ID="uc_DuyetGachTerrazo1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxToolkit:TabPanel ID="frmChotGachTerrazo" runat="server" HeaderText="Chốt nhập gạch Terrazo">
                            <ContentTemplate>
                                <uc2:uc_DuyetChotGachTerrazo ID="uc_DuyetChotGachTerrazo1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>--%>

                        <ajaxToolkit:TabPanel ID="frmDinhMucGachMenBong" runat="server" HeaderText="Định mức gạch men bóng">
                            <ContentTemplate>
                                <uc8:uc_DuyetDinhMucGachMenBong ID="uc_DuyetDinhMucGachMenBong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmGachMenBong" runat="server" HeaderText="Gạch men bóng">
                            <ContentTemplate>
                                <uc3:uc_DuyetGachMenBong ID="uc_DuyetGachMenBong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxToolkit:TabPanel ID="frmChotGachMenBong" runat="server" HeaderText="Chốt nhập gạch men bóng">
                            <ContentTemplate>
                                <uc4:uc_DuyetChotGachMenBong ID="uc_DuyetChotGachMenBong1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>--%>

                        <ajaxToolkit:TabPanel ID="frmDinhMucGachXayDung" runat="server" HeaderText="Định mức gạch xây dựng">
                            <ContentTemplate>
                                <uc9:uc_DuyetDinhMucGachXayDung ID="uc_DuyetDinhMucGachXayDung" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmGachXayDung" runat="server" HeaderText="Gạch xây dựng">
                            <ContentTemplate>
                                <uc5:uc_DuyetGachXayDung ID="uc_DuyetGachXayDung1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxToolkit:TabPanel ID="frmChotGachXayDung" runat="server" HeaderText="Chốt nhập gạch xây dựng">
                            <ContentTemplate>
                                <uc6:uc_DuyetChotGachXayDung ID="uc_DuyetChotGachXayDung1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>--%>

                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
