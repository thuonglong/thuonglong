﻿<%@ Page Title="Duyệt thiết bị" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetThietBi.aspx.cs" Inherits="ThuongLong.DuyetThietBi" %>

<%--<%@ Register Src="UCThietBi/uc_DuyetQuanLyThietBi.ascx" TagName="uc_DuyetQuanLyThietBi" TagPrefix="uc1" %>--%>
<%@ Register Src="UCThietBi/uc_DuyetXeVanChuyen.ascx" TagName="uc_DuyetXeVanChuyen" TagPrefix="uc1" %>
<%@ Register Src="UCThietBi/uc_DuyetNguoiVanHanh.ascx" TagName="uc_DuyetNguoiVanHanh" TagPrefix="uc2" %>
<%@ Register Src="UCThietBi/uc_DuyetBaoHiemXe.ascx" TagName="uc_DuyetBaoHiemXe" TagPrefix="uc3" %>
<%@ Register Src="UCThietBi/uc_DuyetBaoDuongXe.ascx" TagName="uc_DuyetBaoDuongXe" TagPrefix="uc4" %>
<%@ Register Src="UCThietBi/uc_DuyetDangKiemXe.ascx" TagName="uc_DuyetDangKiemXe" TagPrefix="uc5" %>
<%@ Register Src="UCThietBi/uc_DuyetGiayDiDuong.ascx" TagName="uc_DuyetGiayDiDuong" TagPrefix="uc6" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmXeVanChuyen" runat="server" HeaderText="Quản lý xe">
                            <ContentTemplate>
                                <uc1:uc_DuyetXeVanChuyen ID="uc_DuyetXeVanChuyen" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmNguoiVanHanh" runat="server" HeaderText="Người vận hành">
                            <ContentTemplate>
                                <uc2:uc_DuyetNguoiVanHanh ID="uc_DuyetNguoiVanHanh" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>                        
                        <ajaxToolkit:TabPanel ID="frmBaoHiemXe" runat="server" HeaderText="Bảo hiểm xe">
                            <ContentTemplate>
                                <uc3:uc_DuyetBaoHiemXe ID="uc_DuyetBaoHiemXe" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>                       
                        <ajaxToolkit:TabPanel ID="frmBaoDuongXe" runat="server" HeaderText="Bảo dưỡng xe">
                            <ContentTemplate>
                                <uc4:uc_DuyetBaoDuongXe ID="uc_DuyetBaoDuongXe" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>                        
                        <ajaxToolkit:TabPanel ID="frmDangKiemXe" runat="server" HeaderText="Đăng kiểm xe">
                            <ContentTemplate>
                                <uc5:uc_DuyetDangKiemXe ID="uc_DuyetDangKiemXe" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmGiayDiDuong" runat="server" HeaderText="Giấy đi đường">
                            <ContentTemplate>
                                <uc6:uc_DuyetGiayDiDuong ID="uc_DuyetGiayDiDuong" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxToolkit:TabPanel ID="frmNhapKhoThietBi" runat="server" HeaderText="Nhập kho vật liệu">
                            <ContentTemplate>
                                <uc1:uc_DuyetQuanLyThietBi ID="uc_DuyetQuanLyThietBi4" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmChotNhapKhoThietBi" runat="server" HeaderText="Chốt nhập kho vật liệu">
                            <ContentTemplate>
                                <uc1:uc_DuyetQuanLyThietBi ID="uc_DuyetQuanLyThietBi5" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>--%>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
