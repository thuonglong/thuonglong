﻿using NPOI.HSSF.Record;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class BanGach : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmBanGach";
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmBanGach", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        txtNgayThang.Text = dateTimenow.ToString("dd/MM/yyyy");
                        LoadChiNhanh();
                        LoadNhaCungCap();
                        LoadNhomVatLieu();
                        LoadLoaiVatLieu();
                        LoadDonViTinh();
                        LoadXe();
                        LoadKhachHangSearch();
                        hdPage.Value = "1";
                        Session["BanGach"] = null;
                        hdForm.Value = Guid.NewGuid().ToString();
                        GetTable();
                    }
                }
            }
        }
        void LoadXe()
        {
            dlBienSoXe.Items.Clear();
            dlBienSoXe.DataSource = from p in db.tblXeVanChuyens
                                    where p.TrangThai == 2
                                    && p.IsTrangThai == 1
                                    && (
                                    p.IDNhomThietBi == new Guid("E58CFEE9-38A8-4791-ABE9-993562BA715F")
                                    || p.IDNhomThietBi == new Guid("AB7BDA38-9DA8-4796-AD2C-F73081CC61C7")
                                    //|| p.IDNhomThietBi == new Guid("AB7BDA38-9DA8-4796-AD2C-F73081CC61C7")
                                    )
                                    orderby p.STT
                                    select new
                                    {
                                        p.ID,
                                        Ten = p.TenThietBi,
                                    };
            dlBienSoXe.DataBind();
            dlBienSoXe.Items.Insert(0, new ListItem("--Chọn --", ""));
        }
        void GetTable()
        {
            dt.Columns.Add("ID", typeof(Guid));
            dt.Columns.Add("IDBan", typeof(Guid));
            dt.Columns.Add("IDLich", typeof(Guid));
            dt.Columns.Add("IDNhomVatLieu", typeof(Guid));
            dt.Columns.Add("TenNhomVatLieu", typeof(string));
            dt.Columns.Add("IDLoaiVatLieu", typeof(Guid));
            dt.Columns.Add("TenLoaiVatLieu", typeof(string));
            dt.Columns.Add("IDDonViTinh", typeof(Guid));
            dt.Columns.Add("TenDonViTinh", typeof(string));
            dt.Columns.Add("IDHopDongChiTiet", typeof(Guid));
            dt.Columns.Add("SoLuongThucXuat", typeof(double));
            dt.Columns.Add("SoLuongNhan", typeof(double));
            dt.Columns.Add("DonGiaCoThue", typeof(decimal));
            dt.Columns.Add("DonGiaKhongThue", typeof(decimal));
            dt.Columns.Add("TTCoThue", typeof(decimal));
            dt.Columns.Add("TTKhongThue", typeof(decimal));
            dt.Columns.Add("PhuPhi", typeof(decimal));
            dt.Columns.Add("TThai", typeof(int));
            dt.Columns.Add("TThaiText", typeof(string));
            dt.Columns.Add("NguoiDuyet", typeof(string));
            dt.Columns.Add("NguoiXoa", typeof(string));
            dt.Columns.Add("NgayTao", typeof(DateTime));
            dt.Columns.Add("NguoiTao", typeof(string));
            dt.Columns.Add("MoTa", typeof(string));
            dt.Columns.Add("Loai", typeof(Int32));
            dt.Columns.Add("hdForm", typeof(string));
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));

            dlChiNhanhChot.DataSource = query;
            dlChiNhanhChot.DataBind();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem(string idlich, string idchitiet)
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlCongTrinh.SelectedValue == "")
            {
                s += " - Chọn công trình<br />";
            }

            if (s == "")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vatlieu.sp_BanGach_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue,
                       new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()),
                       decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString()), decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString()), ref idlich, ref idchitiet, ref s);
                    if (s != "")
                    {
                        break;
                    }
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua(string idlich, string idchitiet)
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlCongTrinh.SelectedValue == "")
            {
                s += " - Chọn công trình<br />";
            }
            //if (s == "")
            //{
            //    vatlieu.sp_BanGach_CheckSuaHopDong(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), ref s);
            //}
            if (s == "")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["TThai"].ToString() == "3")
                    {
                        vatlieu.sp_BanGach_CheckXoaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), ref s);
                    }
                    else if (dt.Rows[i]["TThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() == "0")
                    {
                        vatlieu.sp_BanGach_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue,
                           new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()),
                           decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString()), decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString()), ref idlich, ref idchitiet, ref s);
                    }
                    else if (dt.Rows[i]["TThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() != "1")
                    {
                        vatlieu.sp_BanGach_CheckSuaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue,
                            new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString()), new Guid(dt.Rows[i]["IDDonViTinh"].ToString()), decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString()), decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString()),
                            ref idlich, ref idchitiet, ref s);
                    }
                    if (s != "")
                    {
                        break;
                    }
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckThemChiTiet(ref string idlich, ref string idchitiet)
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlCongTrinh.SelectedValue == "")
            {
                s += " - Chọn công trình<br />";
            }
            if (dlNhomVatLieu.SelectedValue == "")
            {
                s += " - Chọn nhóm vật liệu<br />";
            }
            if (dlLoaiVatLieu.SelectedValue == "")
            {
                s += " - Chọn loại vật liệu<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtSoLuongThucXuat.Text == "")
            {
                s += " - Nhập số lượng thực xuất<br />";
            }
            if (txtSoLuongThucXuat.Text == "")
            {
                s += " - Nhập số lượng khách hàng xác nhận<br />";
            }
            if (s == "")
            {
                vatlieu.sp_BanGach_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlLoaiVatLieu),
                    GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text), decimal.Parse(txtDonGiaKhongThue.Text), ref idlich, ref idchitiet, ref s);

            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaChiTiet(ref string idlich, ref string idchitiet)
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlCongTrinh.SelectedValue == "")
            {
                s += " - Chọn công trình<br />";
            }
            if (dlNhomVatLieu.SelectedValue == "")
            {
                s += " - Chọn nhóm vật liệu<br />";
            }
            if (dlLoaiVatLieu.SelectedValue == "")
            {
                s += " - Chọn loại vật liệu<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtSoLuongThucXuat.Text == "")
            {
                s += " - Nhập số lượng thực xuất<br />";
            }
            if (txtSoLuongThucXuat.Text == "")
            {
                s += " - Nhập số lượng khách hàng xác nhận<br />";
            }

            if (s == "")
            {
                vatlieu.sp_BanGach_CheckSuaChiTiet(new Guid(hdChiTiet.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh),
                    decimal.Parse(txtDonGiaCoThue.Text), decimal.Parse(txtDonGiaKhongThue.Text), ref idlich, ref idchitiet, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string idlich = "";
            string idchitiet = "";
            if (Session["BanGach"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["BanGach"] as DataTable;
            }

            if (dt.Rows.Count == 0 && gvChiTiet.Rows.Count > 0)
            {
                Warning("Thông tin giá hợp đồng đã thay đổi.");
            }
            else if (dt.Rows.Count == 0)
            {
                Warning("Bạn chưa nhập thông tin bán gạch");
            }
            else if (dt.Rows.Count > 0 && dt.Rows[0]["hdForm"].ToString() != hdForm.Value)
            {
                Warning("Bạn không được mở 2 tab trên cùng 1 trình duyệt.");
            }
            else
            {
                if (hdID.Value == "")
                {
                    if (CheckThem(idlich, idchitiet) == "")
                    {
                        string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            Guid ID = Guid.NewGuid();
                            var BanGach = new tblBanGach()
                            {
                                ID = new Guid(hdChung.Value),
                                NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                                IDChiNhanh = GetDrop(dlChiNhanh),
                                IDNhaCungCap = GetDrop(dlNhaCungCap),
                                IDHopDong = GetDrop(dlCongTrinh),
                                IDNVKD = GetDrop(dlNhanVien),
                                HangMuc = dlHangMuc.SelectedItem.Text.Trim(),
                                IDBienSoXe = dlBienSoXe.SelectedValue == "" ? (Guid?)null : GetDrop(dlBienSoXe),
                                TenBienSoXe = dlBienSoXe.SelectedValue == "" ? "" : dlBienSoXe.SelectedItem.Text.ToUpper(),
                                TenLaiXe = txtTenLaiXe.Text.Trim(),
                                MoTa = txtMoTa.Text.Trim(),
                                //IDNhomVatLieu = GetDrop(dlNhomVatLieu),
                                //IDLoaiVatLieu = GetDrop(dlLoaiVatLieu),
                                //IDDonViTinh = GetDrop(dlDonViTinh),
                                //DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text),
                                //DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text),
                                //TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                                //DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                                TrangThai = 1,
                                TrangThaiText = "Chờ duyệt",
                                //STT = 0,
                                NguoiTao = Session["IDND"].ToString(),
                                NgayTao = DateTime.Now
                            };
                            db.tblBanGaches.InsertOnSubmit(BanGach);

                            //List<tblBanGach_ChiTiet> xoa = new List<tblBanGach_ChiTiet>();
                            List<tblBanGach_ChiTiet> them = new List<tblBanGach_ChiTiet>();

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["Loai"].ToString() != "10")
                                {
                                    //var xoachitiet = (from p in db.tblBanGach_ChiTiets
                                    //                  where p.ID == new Guid(dt.Rows[i]["ID"].ToString())
                                    //                  select p).FirstOrDefault();
                                    //if (xoachitiet != null && xoachitiet.ID != null)
                                    //{
                                    //    xoa.Add(xoachitiet);
                                    //}
                                    var themchitiet = new tblBanGach_ChiTiet();
                                    themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                    themchitiet.IDBan = new Guid(dt.Rows[i]["IDBan"].ToString());
                                    themchitiet.IDLich = new Guid(dt.Rows[i]["IDLich"].ToString());
                                    themchitiet.IDNhomVatLieu = new Guid(dt.Rows[i]["IDNhomVatLieu"].ToString());
                                    themchitiet.IDLoaiVatLieu = new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString());
                                    themchitiet.IDDonViTinh = new Guid(dt.Rows[i]["IDDonViTinh"].ToString());
                                    themchitiet.IDHopDongChiTiet = new Guid(dt.Rows[i]["IDHopDongChiTiet"].ToString());
                                    themchitiet.SoLuongThucXuat = double.Parse(dt.Rows[i]["SoLuongThucXuat"].ToString());
                                    themchitiet.SoLuongNhan = double.Parse(dt.Rows[i]["SoLuongNhan"].ToString());
                                    themchitiet.DonGiaCoThue = decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString());
                                    themchitiet.DonGiaKhongThue = decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString());
                                    themchitiet.TTCoThue = decimal.Parse(dt.Rows[i]["TTCoThue"].ToString());
                                    themchitiet.TTKhongThue = decimal.Parse(dt.Rows[i]["TTKhongThue"].ToString());
                                    themchitiet.PhuPhi = decimal.Parse(dt.Rows[i]["PhuPhi"].ToString());
                                    themchitiet.ThanhTienCoThue = decimal.Parse(dt.Rows[i]["TTCoThue"].ToString()) + decimal.Parse(dt.Rows[i]["PhuPhi"].ToString());
                                    themchitiet.ThanhTienKhongThue = decimal.Parse(dt.Rows[i]["TTKhongThue"].ToString()) + decimal.Parse(dt.Rows[i]["PhuPhi"].ToString());
                                    //themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                                    //themchitiet.DenNgay = dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString()));
                                    themchitiet.TThai = int.Parse(dt.Rows[i]["TThai"].ToString());
                                    themchitiet.TThaiText = dt.Rows[i]["TThaiText"].ToString();
                                    themchitiet.NguoiDuyet = dt.Rows[i]["NguoiDuyet"].ToString();
                                    themchitiet.NguoiXoa = dt.Rows[i]["NguoiXoa"].ToString();
                                    themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                    themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                    themchitiet.MoTa = dt.Rows[i]["MoTa"].ToString();
                                    them.Add(themchitiet);
                                }
                            }
                            //db.tblBanGach_ChiTiets.DeleteAllOnSubmit(xoa);
                            db.tblBanGach_ChiTiets.InsertAllOnSubmit(them);
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Lưu thành công.");
                        }
                    }
                }
                else
                {
                    if (CheckSua(idlich, idchitiet) == "")
                    {

                        string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            var query = (from p in db.tblBanGaches
                                         where p.ID == new Guid(hdID.Value)
                                         select p).FirstOrDefault();
                            if (query != null && query.ID != null)
                            {
                                DateTime dtcheck = DateTime.Parse(hdNgayTao.Value);
                                if (hdID.Value != ""
                                    && (
                                    dtcheck.Year != query.NgayTao.Value.Year
                                    || dtcheck.Month != query.NgayTao.Value.Month
                                    || dtcheck.Day != query.NgayTao.Value.Day
                                    || dtcheck.Hour != query.NgayTao.Value.Hour
                                    || dtcheck.Minute != query.NgayTao.Value.Minute
                                    || dtcheck.Second != query.NgayTao.Value.Second
                                    )
                                    )
                                {
                                    Warning("Thông tin hợp đồng đã bị sửa, xin vui lòng kiểm tra lại.");
                                }
                                else
                                {
                                    query.IDChiNhanh = GetDrop(dlChiNhanh);
                                    query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                                    query.IDHopDong = GetDrop(dlCongTrinh);
                                    query.IDNVKD = GetDrop(dlNhanVien);
                                    query.HangMuc = dlHangMuc.SelectedItem.Text.Trim();
                                    query.IDBienSoXe = dlBienSoXe.SelectedValue == "" ? (Guid?)null : GetDrop(dlBienSoXe);
                                    query.TenBienSoXe = dlBienSoXe.SelectedValue == "" ? "" : dlBienSoXe.SelectedItem.Text.ToUpper();
                                    query.TenLaiXe = txtTenLaiXe.Text.Trim();
                                    query.MoTa = txtMoTa.Text.Trim();
                                    query.TrangThai = 1;
                                    query.TrangThaiText = "Chờ duyệt";
                                    //query.STT = query.STT + 1;
                                    query.NguoiTao = Session["IDND"].ToString();
                                    query.NgayTao = DateTime.Now;


                                    List<tblBanGach_ChiTiet> xoa = new List<tblBanGach_ChiTiet>();
                                    List<tblBanGach_ChiTiet> them = new List<tblBanGach_ChiTiet>();

                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        if (dt.Rows[i]["Loai"].ToString() != "10")
                                        {
                                            var xoachitiet = (from p in db.tblBanGach_ChiTiets
                                                              where p.ID == new Guid(dt.Rows[i]["ID"].ToString())
                                                              select p).FirstOrDefault();
                                            if (xoachitiet != null && xoachitiet.ID != null)
                                            {
                                                xoa.Add(xoachitiet);
                                            }
                                            var themchitiet = new tblBanGach_ChiTiet();
                                            themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                            themchitiet.IDBan = new Guid(dt.Rows[i]["IDBan"].ToString());
                                            themchitiet.IDLich = new Guid(dt.Rows[i]["IDLich"].ToString());
                                            themchitiet.IDNhomVatLieu = new Guid(dt.Rows[i]["IDNhomVatLieu"].ToString());
                                            themchitiet.IDLoaiVatLieu = new Guid(dt.Rows[i]["IDLoaiVatLieu"].ToString());
                                            themchitiet.IDDonViTinh = new Guid(dt.Rows[i]["IDDonViTinh"].ToString());
                                            themchitiet.IDHopDongChiTiet = new Guid(dt.Rows[i]["IDHopDongChiTiet"].ToString());
                                            themchitiet.SoLuongThucXuat = double.Parse(dt.Rows[i]["SoLuongThucXuat"].ToString());
                                            themchitiet.SoLuongNhan = double.Parse(dt.Rows[i]["SoLuongNhan"].ToString());
                                            themchitiet.DonGiaCoThue = decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString());
                                            themchitiet.DonGiaKhongThue = decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString());
                                            themchitiet.TTCoThue = decimal.Parse(dt.Rows[i]["TTCoThue"].ToString());
                                            themchitiet.TTKhongThue = decimal.Parse(dt.Rows[i]["TTKhongThue"].ToString());
                                            themchitiet.PhuPhi = decimal.Parse(dt.Rows[i]["PhuPhi"].ToString());
                                            themchitiet.ThanhTienCoThue = decimal.Parse(dt.Rows[i]["TTCoThue"].ToString()) + decimal.Parse(dt.Rows[i]["PhuPhi"].ToString());
                                            themchitiet.ThanhTienKhongThue = decimal.Parse(dt.Rows[i]["TTKhongThue"].ToString()) + decimal.Parse(dt.Rows[i]["PhuPhi"].ToString());
                                            //themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                                            //themchitiet.DenNgay = dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString()));
                                            themchitiet.TThai = int.Parse(dt.Rows[i]["TThai"].ToString());
                                            themchitiet.TThaiText = dt.Rows[i]["TThaiText"].ToString();
                                            themchitiet.NguoiDuyet = dt.Rows[i]["NguoiDuyet"].ToString();
                                            themchitiet.NguoiXoa = dt.Rows[i]["NguoiXoa"].ToString();
                                            themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                            themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                            themchitiet.MoTa = dt.Rows[i]["MoTa"].ToString();
                                            them.Add(themchitiet);
                                        }
                                    }
                                    db.tblBanGach_ChiTiets.DeleteAllOnSubmit(xoa);
                                    db.tblBanGach_ChiTiets.InsertAllOnSubmit(them);

                                    db.SubmitChanges();

                                    lblTaoMoiHopDong_Click(sender, e);
                                    Search(1);
                                    Success("Sửa thành công");
                                }
                            }
                            else
                            {
                                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                                lblTaoMoiHopDong_Click(sender, e);
                            }
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh_SelectedIndexChanged(sender, e);
            dlBienSoXe.SelectedValue = "";
            txtTenLaiXe.Text = "";
            txtMoTa.Text = "";
            hdID.Value = "";
            hdChung.Value = "";
            hdForm.Value = "";
            hdChiTiet.Value = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";

            Session["BanGach"] = null;
            gvChiTiet.DataSource = null;
            gvChiTiet.DataBind();
            txtNgayThang.Enabled = true;
            dlChiNhanh.Enabled = true;
            dlNhaCungCap.Enabled = true;
            dlCongTrinh.Enabled = true;
            dlHangMuc.Enabled = true;
            LoadKhachHangSearch();
        }
        protected void lbtSave_Click(object sender, EventArgs e)
        {
            string url = "";
            string idlich = "";
            string idchitiet = "";
            if (Session["BanGach"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["BanGach"] as DataTable;
            }

            if (hdChiTiet.Value == "")
            {
                if (CheckThemChiTiet(ref idlich, ref idchitiet) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (hdForm.Value == "")
                        {
                            hdForm.Value = Guid.NewGuid().ToString();
                        }

                        if (hdID.Value == "" && hdChung.Value == "")
                            hdChung.Value = Guid.NewGuid().ToString();

                        dt.Rows.Add(
                            Guid.NewGuid(),
                        new Guid(hdChung.Value),
                        new Guid(idlich),
                        GetDrop(dlNhomVatLieu),
                        dlNhomVatLieu.SelectedItem.Text.ToString(),
                        GetDrop(dlLoaiVatLieu),
                        dlLoaiVatLieu.SelectedItem.Text.ToString(),
                        GetDrop(dlDonViTinh),
                        dlDonViTinh.SelectedItem.Text.ToString(),
                        new Guid(idchitiet),
                        double.Parse(txtSoLuongThucXuat.Text),
                        double.Parse(txtSoLuongNhan.Text),
                        decimal.Parse(txtDonGiaCoThue.Text),
                        decimal.Parse(txtDonGiaKhongThue.Text),
                        decimal.Parse(txtThanhTienCoThue.Text),
                        decimal.Parse(txtThanhTienKhongThue.Text),
                        txtPhuPhi.Text.Trim() == "" ? 0 : decimal.Parse(txtPhuPhi.Text),
                        1,
                        "Chờ duyệt",
                        "",
                        "",
                        DateTime.Now,
                        Session["IDND"].ToString(),
                        "",
                        0,
                        hdForm.Value
                        );

                        lbtTaoGiaMoi_Click(sender, e);
                        gvChiTiet.DataSource = dt;
                        gvChiTiet.DataBind();
                        Session["BanGach"] = dt;
                        Success("Lưu thành công.");
                        txtNgayThang.Enabled = false;
                        dlChiNhanh.Enabled = false;
                        dlNhaCungCap.Enabled = false;
                        dlCongTrinh.Enabled = false;
                        dlHangMuc.Enabled = false;
                    }
                }
            }
            else
            {
                if (CheckSuaChiTiet(ref idlich, ref idchitiet) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (hdForm.Value == "")
                        {
                            hdForm.Value = Guid.NewGuid().ToString();
                        }

                        DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(hdChiTiet.Value)).FirstOrDefault();
                        if (dr != null)
                        {
                            dr["IDBan"] = new Guid(hdChung.Value);
                            dr["IDLich"] = new Guid(idlich);
                            dr["IDNhomVatLieu"] = GetDrop(dlNhomVatLieu);
                            dr["TenNhomVatLieu"] = dlNhomVatLieu.SelectedItem.Text.ToString();
                            dr["IDLoaiVatLieu"] = GetDrop(dlLoaiVatLieu);
                            dr["TenLoaiVatLieu"] = dlLoaiVatLieu.SelectedItem.Text.ToString();
                            dr["IDDonViTinh"] = GetDrop(dlDonViTinh);
                            dr["TenDonViTinh"] = dlDonViTinh.SelectedItem.Text.ToString();
                            dr["IDHopDongChiTiet"] = new Guid(idchitiet);
                            dr["SoLuongThucXuat"] = double.Parse(txtSoLuongThucXuat.Text);
                            dr["SoLuongNhan"] = double.Parse(txtSoLuongNhan.Text);
                            dr["DonGiaCoThue"] = decimal.Parse(txtDonGiaCoThue.Text);
                            dr["DonGiaKhongThue"] = decimal.Parse(txtDonGiaKhongThue.Text);
                            dr["TTCoThue"] = decimal.Parse(txtThanhTienCoThue.Text);
                            dr["TTKhongThue"] = decimal.Parse(txtThanhTienKhongThue.Text);
                            dr["PhuPhi"] = txtPhuPhi.Text.Trim() == "" ? 0 : decimal.Parse(txtPhuPhi.Text);
                            dr["TThai"] = 1;
                            dr["TThaiText"] = "Chờ duyệt";
                            dr["NguoiDuyet"] = "";
                            dr["NguoiXoa"] = "";
                            dr["NgayTao"] = DateTime.Now;
                            dr["NguoiTao"] = Session["IDND"].ToString();
                            dr["MoTa"] = "";
                            dr["Loai"] = 1;
                            dr["hdForm"] = hdForm.Value;
                        }

                        lbtTaoGiaMoi_Click(sender, e);
                        gvChiTiet.DataSource = dt;
                        gvChiTiet.DataBind();
                        Session["BanGach"] = dt;
                        Success("Lưu thành công.");
                        txtNgayThang.Enabled = false;
                        dlChiNhanh.Enabled = false;
                        dlNhaCungCap.Enabled = false;
                        dlCongTrinh.Enabled = false;
                        dlHangMuc.Enabled = false;
                    }
                }
            }
        }
        protected void lbtTaoGiaMoi_Click(object sender, EventArgs e)
        {
            dlNhomVatLieu.SelectedIndex = 0;
            dlNhomVatLieu_SelectedIndexChanged(sender, e);

            hdChiTiet.Value = "";
            txtSoLuongThucXuat.Text = "";
            txtSoLuongNhan.Text = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            txtPhuPhi.Text = "";
        }

        protected void gvChiTiet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();

            if (Session["BanGach"] == null)
            {
                GetTable();
            }
            else
            {
                dt = Session["BanGach"] as DataTable;
            }

            DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(id)).FirstOrDefault();
            if (dr != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlNhomVatLieu.SelectedValue = dr["IDNhomVatLieu"].ToString();
                        LoadLoaiVatLieu();
                        dlLoaiVatLieu.SelectedValue = dr["IDLoaiVatLieu"].ToString();
                        LoadDonViTinh();
                        dlDonViTinh.SelectedValue = dr["IDDonViTinh"].ToString();
                        LoadGiaBanGach();

                        txtSoLuongThucXuat.Text = string.Format("{0:N2}", dr["SoLuongThucXuat"]);
                        txtSoLuongNhan.Text = string.Format("{0:N2}", dr["SoLuongNhan"]);

                        txtDonGiaCoThue.Text = string.Format("{0:N2}", dr["DonGiaCoThue"]);
                        txtDonGiaKhongThue.Text = string.Format("{0:N2}", dr["DonGiaKhongThue"]);

                        txtThanhTienCoThue.Text = string.Format("{0:N2}", dr["TTCoThue"]);
                        txtThanhTienKhongThue.Text = string.Format("{0:N2}", dr["TTKhongThue"]);
                        txtPhuPhi.Text = string.Format("{0:N2}", dr["PhuPhi"]);

                        hdChiTiet.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_BanGach_CheckXoaChiTiet(new Guid(id), ref thongbao);
                        if (thongbao != "")
                        {
                            Warning(thongbao);
                        }
                        else
                        {
                            var query = (from p in db.tblBanGach_ChiTiets
                                         where p.ID == new Guid(id)
                                         select p).FirstOrDefault();

                            if (query != null && query.ID != null)
                            {
                                dr["Loai"] = "3";
                                dr["TThai"] = "3";
                                dr["TThaiText"] = "Chờ duyệt xóa";

                                Success("Xóa thành công.");
                                gvChiTiet.DataSource = dt;
                                gvChiTiet.DataBind();
                                Session["GiBanGach"] = dt;
                                lbtTaoGiaMoi_Click(sender, e);
                            }
                            else
                            {
                                dt.Rows.Remove(dr);
                                dt.AcceptChanges();

                                Success("Xóa thành công.");
                                gvChiTiet.DataSource = dt;
                                gvChiTiet.DataBind();
                                Session["GiBanGach"] = dt;
                                lbtTaoGiaMoi_Click(sender, e);
                            }
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = vatlieu.sp_BanGach_LichSu(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblBanGaches
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        LoadNhaCungCap();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        LoadCongTrinh();
                        dlCongTrinh.SelectedValue = query.IDHopDong.ToString();
                        LoadNhomVatLieu();
                        dlBienSoXe.SelectedValue = query.IDBienSoXe.ToString();
                        txtTenLaiXe.Text = query.TenLaiXe;
                        txtMoTa.Text = query.MoTa;
                        hdID.Value = id;
                        hdChung.Value = id;
                        hdNgayTao.Value = query.NgayTao.ToString();
                        hdForm.Value = Guid.NewGuid().ToString();
                        LoadChiTiet();
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //vatlieu.sp_BanGach_CheckXoa(query.ID, ref thongbao);
                        //if (thongbao != "")
                        //    Warning(thongbao);
                        //else
                        //{
                        if (query.TrangThai == 1)
                        {
                            int countxoa = (from p in db.tblBanGach_Logs
                                            where p.IDChung == query.ID
                                            select p).Count();
                            if (countxoa == 1)
                            {
                                db.tblBanGaches.DeleteOnSubmit(query);
                                db.SubmitChanges();
                                Success("Đã xóa");
                            }
                            else
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                        }
                        else if (query.TrangThai == 2)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                            Success("Xóa thành công");
                        }
                        else
                        {
                            Warning("Dữ liệu đang chờ duyệt xóa");
                        }
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = vatlieu.sp_BanGach_LichSu(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            var query = vatlieu.sp_BanGach_Search(GetDrop(dlChiNhanhSearch), dlKhachHangSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlKhachHangSearch), dlKhachHangSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlKhachHangSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 20, page);

            GV.DataSource = query;
            GV.DataBind();

            for (int rowIndex = GV.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = GV.Rows[rowIndex];
                GridViewRow previousRow = GV.Rows[rowIndex + 1];
                var lbtSua1 = (LinkButton)row.FindControl("lbtSua");
                var lbtSua2 = (LinkButton)previousRow.FindControl("lbtSua");
                var lbtXoa1 = (LinkButton)row.FindControl("lbtXoa");
                var lbtXoa2 = (LinkButton)previousRow.FindControl("lbtXoa");
                var lblLichSu1 = (LinkButton)row.FindControl("lblLichSu");
                var lblLichSu2 = (LinkButton)previousRow.FindControl("lblLichSu");
                if (
                    lbtSua1.CommandArgument.Equals(lbtSua2.CommandArgument)
                    && lbtXoa1.CommandArgument.Equals(lbtXoa2.CommandArgument)
                    && lblLichSu1.CommandArgument.Equals(lblLichSu2.CommandArgument)


                    && row.Cells[3].Text.Equals(previousRow.Cells[3].Text)
                    && row.Cells[4].Text.Equals(previousRow.Cells[4].Text)
                    && row.Cells[5].Text.Equals(previousRow.Cells[5].Text)
                    && row.Cells[6].Text.Equals(previousRow.Cells[6].Text)
                    )
                {
                    row.Cells[0].RowSpan = previousRow.Cells[0].RowSpan < 2 ? 2 : previousRow.Cells[0].RowSpan + 1;
                    row.Cells[1].RowSpan = previousRow.Cells[1].RowSpan < 2 ? 2 : previousRow.Cells[1].RowSpan + 1;

                    row.Cells[2].RowSpan = previousRow.Cells[2].RowSpan < 2 ? 2 : previousRow.Cells[2].RowSpan + 1;
                    row.Cells[3].RowSpan = previousRow.Cells[3].RowSpan < 2 ? 2 : previousRow.Cells[3].RowSpan + 1;
                    row.Cells[4].RowSpan = previousRow.Cells[4].RowSpan < 2 ? 2 : previousRow.Cells[4].RowSpan + 1;
                    row.Cells[5].RowSpan = previousRow.Cells[5].RowSpan < 2 ? 2 : previousRow.Cells[5].RowSpan + 1;
                    row.Cells[6].RowSpan = previousRow.Cells[6].RowSpan < 2 ? 2 : previousRow.Cells[6].RowSpan + 1;

                    previousRow.Cells[0].Visible = false;
                    previousRow.Cells[1].Visible = false;

                    previousRow.Cells[2].Visible = false;
                    previousRow.Cells[3].Visible = false;
                    previousRow.Cells[4].Visible = false;
                    previousRow.Cells[5].Visible = false;
                    previousRow.Cells[6].Visible = false;
                }
            }
            if (GV.Rows.Count > 0)
            {
                sp_BanGach_SearchFooterResult footer = vatlieu.sp_BanGach_SearchFooter(GetDrop(dlChiNhanhSearch), dlKhachHangSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlKhachHangSearch), dlKhachHangSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlKhachHangSearch),
                    DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();

                GV.FooterRow.Cells[0].ColumnSpan = 13;
                GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N2}", footer.SoLuong);
                GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                GV.FooterRow.Cells[1].Text = string.Format("{0:N2}", footer.SoLuongThucXuat);
                GV.FooterRow.Cells[2].Text = string.Format("{0:N2}", footer.SoLuongNhan);
                GV.FooterRow.Cells[3].ColumnSpan = 2;
                GV.FooterRow.Cells[4].Text = string.Format("{0:N2}", footer.TTCoThue);
                GV.FooterRow.Cells[5].Text = string.Format("{0:N2}", footer.TTKhongThue);
                GV.FooterRow.Cells[6].Text = string.Format("{0:N2}", footer.PhuPhi);
                GV.FooterRow.Cells[7].Text = string.Format("{0:N2}", footer.ThanhTienCoThue);
                GV.FooterRow.Cells[8].Text = string.Format("{0:N2}", footer.ThanhTienKhongThue);

                GV.FooterRow.Cells[9].Visible = false;
                GV.FooterRow.Cells[10].Visible = false;
                GV.FooterRow.Cells[11].Visible = false;
                GV.FooterRow.Cells[12].Visible = false;
                GV.FooterRow.Cells[13].Visible = false;
                GV.FooterRow.Cells[14].Visible = false;
                GV.FooterRow.Cells[15].Visible = false;
                GV.FooterRow.Cells[16].Visible = false;
                GV.FooterRow.Cells[17].Visible = false;
                GV.FooterRow.Cells[18].Visible = false;
                GV.FooterRow.Cells[19].Visible = false;
                GV.FooterRow.Cells[20].Visible = false;
                GV.FooterRow.Cells[21].Visible = false;
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_BanGach_PrintResult> query = vatlieu.sp_BanGach_Print(GetDrop(dlChiNhanhSearch), dlKhachHangSearch.SelectedValue, dlKhachHangSearch.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();


            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "BÁO CÁO BÁN GẠCH";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 7);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO BÁN GẠCH TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                }
                else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO BÁN GẠCH NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 19);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 19);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Số phiếu",
                    "Chi nhánh",
                    "Ngày tháng",
                    "Nhà cung cấp",
                    "Nhóm vật liệu",
                    "Loại vật liệu",
                    "Đơn vị tính",
                    "Biển số xe",
                    "Lái xe",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Đơn giá",
                    "Đơn giá",
                    "Thành tiền",
                    "Thành tiền",
                    "Phụ phí",
                    "Tổng tiền",
                    "Tổng tiền",
                    "Ghi chú"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Số phiếu",
                    "Chi nhánh",
                    "Ngày tháng",
                    "Nhà cung cấp",
                    "Nhóm vật liệu",
                    "Loại vật liệu",
                    "Đơn vị tính",
                    "Biển số xe",
                    "Lái xe",
                    "Thực xuất",
                    "Khách hàng nhận",
                    "Hóa đơn",
                    "Thanh toán",
                    "Hóa đơn",
                    "Thanh toán",
                    "Phụ phí",
                    "Hóa đơn",
                    "Thanh toán",
                    "Ghi chú"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 3, 3);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 4, 4);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 5, 5);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 6, 6);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 7, 7);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 8, 8);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 9, 9);
                sheet.AddMergedRegion(cra);

                cra = new CellRangeAddress(4, 4, 10, 11);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 12, 13);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 14, 15);
                sheet.AddMergedRegion(cra);

                cra = new CellRangeAddress(4, 5, 16, 16);
                sheet.AddMergedRegion(cra);

                cra = new CellRangeAddress(4, 4, 17, 18);
                sheet.AddMergedRegion(cra);

                cra = new CellRangeAddress(4, 5, 19, 19);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.SoPhieu.ToString());
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(item.NgayThang);
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(item.TenNhaCungCap);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(item.TenNhomVatLieu);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(item.TenLoaiVatLieu);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(item.TenDonViTinh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(item.TenBienSoXe);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(item.TenLaiXe);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(double.Parse(item.SoLuongThucXuat.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(11);
                    cell.SetCellValue(double.Parse(item.SoLuongNhan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(12);
                    cell.SetCellValue(double.Parse(item.DonGiaCoThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(13);
                    cell.SetCellValue(double.Parse(item.DonGiaKhongThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(14);
                    cell.SetCellValue(double.Parse(item.TTCoThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(15);
                    cell.SetCellValue(double.Parse(item.TTKhongThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(16);
                    cell.SetCellValue(double.Parse(item.PhuPhi.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(17);
                    cell.SetCellValue(double.Parse(item.ThanhTienCoThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(18);
                    cell.SetCellValue(double.Parse(item.ThanhTienKhongThue.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(19);
                    cell.SetCellValue(item.MoTa);
                    cell.CellStyle = styleBody;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= 19; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 8);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(9);
                cell.CellFormula = "COUNT(A" + RowDau.ToString() + ":A" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(10);
                cell.CellFormula = "SUM(K" + RowDau.ToString() + ":K" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(11);
                cell.CellFormula = "SUM(L" + RowDau.ToString() + ":L" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(14);
                cell.CellFormula = "SUM(O" + RowDau.ToString() + ":O" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(15);
                cell.CellFormula = "SUM(P" + RowDau.ToString() + ":P" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(16);
                cell.CellFormula = "SUM(Q" + RowDau.ToString() + ":Q" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(17);
                cell.CellFormula = "SUM(R" + RowDau.ToString() + ":R" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(18);
                cell.CellFormula = "SUM(S" + RowDau.ToString() + ":S" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                #endregion

                #region[Set Footer]

                //rowIndex++;
                //rowIndex++;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo ca:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo khối:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê xe:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Không dùng bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Mua bê tông:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;
                //tên
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 1);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(0);
                cell.SetCellValue("Người xuất hàng");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(2);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 4, 5);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(4);
                cell.SetCellValue("Trưởng bộ phận");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 8);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(6);
                cell.SetCellValue("Lái xe");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 9, 10);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(9);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 11, 13);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(11);
                cell.SetCellValue("Giám sát");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                #endregion

                sheet.SetColumnWidth(4, 7120);
                sheet.SetColumnWidth(5, 3000);
                sheet.SetColumnWidth(6, 7000);
                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO BÁN GẠCH -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                         ".xls";
                    }
                    else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO BÁN GẠCH -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                         "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }

        private void LoadChiTiet()
        {
            List<sp_BanGach_ChiTietSearchResult> query = vatlieu.sp_BanGach_ChiTietSearch(new Guid(hdID.Value), hdForm.Value).ToList();
            GetTable();
            foreach (var item in query)
            {
                dt.Rows.Add(
                            item.ID,
                            item.IDBan,
                            item.IDLich,
                            item.IDNhomVatLieu,
                            item.TenNhomVatLieu,
                            item.IDLoaiVatLieu,
                            item.TenLoaiVatLieu,
                            item.IDDonViTinh,
                            item.TenDonViTinh,
                            item.IDHopDongChiTiet,
                            item.SoLuongThucXuat,
                            item.SoLuongNhan,
                            item.DonGiaCoThue,
                            item.DonGiaKhongThue,
                            item.ThanhTienCoThue,
                            item.ThanhTienKhongThue,
                            item.PhuPhi,
                            item.TrangThai,
                            item.TrangThaiText,
                            item.NguoiDuyet,
                            item.NguoiXoa,
                            item.NgayTao,
                            item.NguoiTao,
                            item.MoTa,
                            item.Loai,
                            item.hdForm
                            );
            }
            gvChiTiet.DataSource = dt;
            gvChiTiet.DataBind();
            Session["BanGach"] = dt;
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadKhachHangSearch();
        }
        void LoadKhachHangSearch()
        {
            dlKhachHangSearch.Items.Clear();
            var query = vatlieu.sp_BanGach_LoadNhaCungCapSearch(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)));
            dlKhachHangSearch.DataSource = query;
            dlKhachHangSearch.DataBind();
            dlKhachHangSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }
        protected void dlNhaCungCap_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCongTrinh();
        }

        protected void dlCongTrinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNVKD();
            LoadHangMuc();
        }

        protected void dlHangMuc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhomVatLieu();
        }
        protected void dlNhomVatLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiVatLieu();
        }
        protected void dlLoaiVatLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDonViTinh();
        }

        protected void dlDonViTinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGiaBanGach();
        }
        protected void LoadNhaCungCap()
        {
            dlNhaCungCap.Items.Clear();
            dlCongTrinh.Items.Clear();
            dlHangMuc.Items.Clear();
            dlNhanVien.Items.Clear();
            dlNhomVatLieu.Items.Clear();
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            if (dlChiNhanh.SelectedValue != "" && txtNgayThang.Text != "")
            {
                var query = vatlieu.sp_BanGach_LoadNhaCungCap(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh));
                dlNhaCungCap.DataSource = query;
                dlNhaCungCap.DataBind();
                if (dlNhaCungCap.Items.Count == 1)
                {
                    LoadCongTrinh();
                }
                else
                {
                    dlNhaCungCap.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadCongTrinh()
        {
            dlCongTrinh.Items.Clear();
            dlHangMuc.Items.Clear();
            dlNhanVien.Items.Clear();
            dlNhomVatLieu.Items.Clear();
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            if (dlNhaCungCap.SelectedValue != "" && txtNgayThang.Text != "")
            {
                var query = vatlieu.sp_BanGach_LoadCongTrinh(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap));
                dlCongTrinh.DataSource = query;
                dlCongTrinh.DataBind();
                if (dlCongTrinh.Items.Count == 1)
                {
                    LoadNVKD();
                    LoadHangMuc();
                }
                else
                {
                    dlCongTrinh.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadHangMuc()
        {
            dlHangMuc.Items.Clear();
            dlNhomVatLieu.Items.Clear();
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            if (dlCongTrinh.SelectedValue != "" && txtNgayThang.Text != "")
            {
                var query = vatlieu.sp_BanGach_LoadHangMuc(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh));
                dlHangMuc.DataSource = query;
                dlHangMuc.DataBind();
                if (dlHangMuc.Items.Count == 1)
                {
                    LoadNhomVatLieu();
                }
                else
                {
                    dlHangMuc.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadNVKD()
        {
            dlNhanVien.Items.Clear();
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            if (dlCongTrinh.SelectedValue != "" && txtNgayThang.Text != "")
            {
                var query = vatlieu.sp_BanGach_LoadNhanVienKinhDoanh(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh));
                dlNhanVien.DataSource = query;
                dlNhanVien.DataBind();
            }
        }
        protected void LoadNhomVatLieu()
        {
            dlNhomVatLieu.Items.Clear();
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            if (dlCongTrinh.SelectedValue != "" && txtNgayThang.Text != "")
            {
                var query = vatlieu.sp_BanGach_LoadNhomVatLieu(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue);
                dlNhomVatLieu.DataSource = query;
                dlNhomVatLieu.DataBind();
                if (dlNhomVatLieu.Items.Count == 1)
                {
                    LoadLoaiVatLieu();
                }
                else
                {
                    dlNhomVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadLoaiVatLieu()
        {
            dlLoaiVatLieu.Items.Clear();
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            if (dlNhomVatLieu.SelectedValue != "" && txtNgayThang.Text != "")
            {
                var query = vatlieu.sp_BanGach_LoadLoaiVatLieu(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlNhomVatLieu));
                dlLoaiVatLieu.DataSource = query;
                dlLoaiVatLieu.DataBind();
                if (dlLoaiVatLieu.Items.Count == 1)
                {
                    LoadDonViTinh();
                }
                else
                {
                    dlLoaiVatLieu.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadDonViTinh()
        {
            dlDonViTinh.Items.Clear();
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtSoLuongNhan.Text = "";
            txtSoLuongThucXuat.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            if (dlLoaiVatLieu.SelectedValue != "" && txtNgayThang.Text != "")
            {
                var query = vatlieu.sp_BanGach_LoadDonViTinh(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlLoaiVatLieu));
                dlDonViTinh.DataSource = query;
                dlDonViTinh.DataBind();
                if (dlDonViTinh.Items.Count == 1)
                {
                    LoadGiaBanGach();
                }
                else
                {
                    dlDonViTinh.Items.Insert(0, new ListItem("--Chọn--", ""));
                }
            }
        }
        protected void LoadGiaBanGach()
        {
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThanhTienCoThue.Text = "";
            txtThanhTienKhongThue.Text = "";
            if (dlDonViTinh.SelectedValue != "" && txtNgayThang.Text != "")
            {
                sp_BanGach_LoadGiaGachResult query = vatlieu.sp_BanGach_LoadGiaGach(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh)).FirstOrDefault();

                if (query != null && query.DonGiaCoThue != null)
                {
                    txtDonGiaCoThue.Text = string.Format("{0:N2}", query.DonGiaCoThue);
                    txtDonGiaKhongThue.Text = string.Format("{0:N2}", query.DonGiaKhongThue);
                    if (txtSoLuongNhan.Text != "" && decimal.Parse(txtSoLuongNhan.Text) > 0)
                    {
                        txtThanhTienCoThue.Text = string.Format("{0:N2}", query.DonGiaKhongThue * decimal.Parse(txtSoLuongNhan.Text));
                        txtThanhTienKhongThue.Text = string.Format("{0:N2}", query.DonGiaKhongThue * decimal.Parse(txtSoLuongNhan.Text));
                    }
                }
            }
        }
        protected void btnChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 6, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else
                {
                    vatlieu.sp_BanGach_CheckChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        vatlieu.sp_BanGach_Chot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), Session["IDND"].ToString(), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }
        protected void btnMoChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 7, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh mở chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else
                {
                    vatlieu.sp_BanGach_CheckMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        vatlieu.sp_BanGach_MoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), Session["IDND"].ToString(), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }
        protected void btnOpenChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Chốt bán gạch";
            mpChot.Show();
            btnChot.Visible = true;
            btnMoChot.Visible = false;
        }
        protected void btnOpenMoChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Mở chốt bán gạch";
            mpChot.Show();
            btnChot.Visible = false;
            btnMoChot.Visible = true;
        }
        protected void lbtSearchChot_Click(object sender, EventArgs e)
        {
            if (btnChot.Visible == true)
            {
                List<sp_BanGach_ListChotResult> query = vatlieu.sp_BanGach_ListChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
                GVChot.DataSource = query;
                GVChot.DataBind();

                //if (GVChot.Rows.Count > 0)
                //{
                //    if (query != null)
                //    {
                //        var valuefooter = (from p in query
                //                           group p by 1 into g
                //                           select new
                //                           {
                //                               SumTLTong = g.Sum(x => x.TLTong),
                //                               SumTLBi = g.Sum(x => x.TLBi),
                //                               SumTLHang = g.Sum(x => x.TLHang),
                //                               SumSumDone = g.Sum(x => x.TLBi),
                //                               SumKLMua = g.Sum(x => x.KLBan),
                //                               SumKLNhapKho = g.Sum(x => x.KLXuatKho),
                //                               SumKLQuanCan = g.Sum(x => x.KLQuanCan),
                //                               SumThanhTienCoThue = g.Sum(x => x.ThanhTienCoThue),
                //                               SumThanhTienKhongThue = g.Sum(x => x.ThanhTienKhongThue)
                //                           }).FirstOrDefault();
                //        GVChot.FooterRow.Cells[0].ColumnSpan = 5;
                //        GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N2}", query.Count) + " phiếu nhập kho";
                //        GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                //        GVChot.FooterRow.Cells[5].Text = string.Format("{0:N2}", valuefooter.SumKLMua);
                //        GVChot.FooterRow.Cells[6].Text = string.Format("{0:N2}", valuefooter.SumKLNhapKho);
                //        GVChot.FooterRow.Cells[7].Text = string.Format("{0:N2}", valuefooter.SumKLQuanCan);
                //        GVChot.FooterRow.Cells[10].Text = string.Format("{0:N2}", valuefooter.SumThanhTienCoThue);
                //        GVChot.FooterRow.Cells[11].Text = string.Format("{0:N2}", valuefooter.SumThanhTienKhongThue);
                //        GVChot.FooterRow.Cells[12].Text = string.Format("{0:N2}", valuefooter.SumTLTong);
                //        GVChot.FooterRow.Cells[13].Text = string.Format("{0:N2}", valuefooter.SumTLBi);
                //        GVChot.FooterRow.Cells[14].Text = string.Format("{0:N2}", valuefooter.SumTLHang);

                //        GVChot.FooterRow.Cells[15].Visible = false;
                //        GVChot.FooterRow.Cells[16].Visible = false;
                //        GVChot.FooterRow.Cells[17].Visible = false;
                //        GVChot.FooterRow.Cells[18].Visible = false;
                //        GVChot.FooterRow.Cells[19].Visible = false;
                //    }
                //}
            }
            else if (btnChot.Visible == false)
            {
                List<sp_BanGach_ListMoChotResult> query = vatlieu.sp_BanGach_ListMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
                GVChot.DataSource = query;
                GVChot.DataBind();

                //if (GVChot.Rows.Count > 0)
                //{
                //    if (query != null)
                //    {
                //        var valuefooter = (from p in query
                //                           group p by 1 into g
                //                           select new
                //                           {
                //                               SumTLTong = g.Sum(x => x.TLTong),
                //                               SumTLBi = g.Sum(x => x.TLBi),
                //                               SumTLHang = g.Sum(x => x.TLHang),
                //                               SumSumDone = g.Sum(x => x.TLBi),
                //                               SumKLMua = g.Sum(x => x.KLBan),
                //                               SumKLNhapKho = g.Sum(x => x.KLXuatKho),
                //                               SumKLQuanCan = g.Sum(x => x.KLQuanCan),
                //                               SumThanhTienCoThue = g.Sum(x => x.ThanhTienCoThue),
                //                               SumThanhTienKhongThue = g.Sum(x => x.ThanhTienKhongThue)
                //                           }).FirstOrDefault();
                //        GVChot.FooterRow.Cells[0].ColumnSpan = 5;
                //        GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N2}", query.Count) + " phiếu nhập kho";
                //        GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                //        GVChot.FooterRow.Cells[5].Text = string.Format("{0:N2}", valuefooter.SumKLMua);
                //        GVChot.FooterRow.Cells[6].Text = string.Format("{0:N2}", valuefooter.SumKLNhapKho);
                //        GVChot.FooterRow.Cells[7].Text = string.Format("{0:N2}", valuefooter.SumKLQuanCan);
                //        GVChot.FooterRow.Cells[10].Text = string.Format("{0:N2}", valuefooter.SumThanhTienCoThue);
                //        GVChot.FooterRow.Cells[11].Text = string.Format("{0:N2}", valuefooter.SumThanhTienKhongThue);
                //        GVChot.FooterRow.Cells[12].Text = string.Format("{0:N2}", valuefooter.SumTLTong);
                //        GVChot.FooterRow.Cells[13].Text = string.Format("{0:N2}", valuefooter.SumTLBi);
                //        GVChot.FooterRow.Cells[14].Text = string.Format("{0:N2}", valuefooter.SumTLHang);


                //        GVChot.FooterRow.Cells[15].Visible = false;
                //        GVChot.FooterRow.Cells[16].Visible = false;
                //        GVChot.FooterRow.Cells[17].Visible = false;
                //        GVChot.FooterRow.Cells[18].Visible = false;
                //        GVChot.FooterRow.Cells[19].Visible = false;
                //    }
                //}
            }
            mpChot.Show();
        }

    }
}