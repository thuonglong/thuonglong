﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCBeTong
{
    public partial class uc_DuyetThueBom : System.Web.UI.UserControl
    {
        private string frmName = "frmThueBom";
        private static int PageNumber = 20;
        private static int CurrentPage = 1;
        private DBDataContext db = new DBDataContext();
        BeTongDataContext betong = new BeTongDataContext();
        private clsPhanQuyen phanquyen = new clsPhanQuyen();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    LoadChiNhanh();
                    //  IDDN = new Guid(Session["IDDN"].ToString());
                }
            }
        }

        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            //dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        public DateTime GetLastDayOfMonth(int iMonth, int iYear)
        {
            DateTime dtResult = new DateTime(iYear, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-1);
            return dtResult;
        }

        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        protected void btnDong_Click(object sender, EventArgs e)
        {
            mpDuyet.Hide();

        }
        private void Search(int page)
        {
            //  rdTrangThai.SelectedValue =="1" : chưa duyệt
            //  rdTrangThai.SelectedValue =="2" : đã duyệt
            int isDuyet = rdTrangThai.SelectedValue == "1" ? 1 : 2;
            System.Data.Linq.ISingleResult<sp_ThueBom_ListDuyetResult> dt = betong.sp_ThueBom_ListDuyet(GetDrop(dlChiNhanh), PageNumber, page);
            GV.DataSource = dt;
            GV.DataBind();

        }
        protected void btnXem_OnClick(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void grdDanhSachHD_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;
            GV.DataSource = null;
            GV.DataSource = betong.sp_ThueBom_LichSu(new Guid(ID));
            GV.DataBind();

            btnKhongDuyet.Visible = rdTrangThai.SelectedValue == "1" ? true : false;
            mpDuyet.Show();
        }

        private Guid GetDrop(DropDownList dl)
        {
            if (dl.SelectedValue.Trim().Equals(""))
            {
                return Guid.Empty;
            }
            else
            {
                return new Guid(dl.SelectedValue);
            }
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        protected void grdDanhSachHD_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#EEFFAA'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            hdPage.Value = "1";
            Search(CurrentPage);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }

        protected void btnDuyetTB_Click(object sender, EventArgs e)
        {
            tblThueBom updatehd = (from p in db.tblThueBoms
                                   where p.ID == new Guid(hdID.Value)
                                   select p).FirstOrDefault();
            if (updatehd.TrangThai == 1)
            {
                #region [Duyệt]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 10, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    updatehd.TrangThai = 2;
                    updatehd.TrangThaiText = "Đã duyệt";
                    db.SubmitChanges();

                    Success("Đã duyệt giá bán bê tông");
                }
                Search(1);
                #endregion
            }
            // Duyệt xóa
            else if (updatehd.TrangThai == 3)
            {
                #region [Duyệt xóa]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 11, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    db.tblThueBoms.DeleteOnSubmit(updatehd);
                    db.SubmitChanges();
                    Success("Đã duyệt xóa giá bán bê tông.");
                    Search(1);
                }
                #endregion [Duyệt xóa]
            }
            else
            {

            }
        }

        protected void btnKDuyetTB_Click(object sender, EventArgs e)
        {
            #region [Không duyệt]
            var query = (from p in db.tblThueBoms
                         where p.ID == new Guid(hdID.Value)
                         select p).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                query.TrangThaiText = query.TrangThai == 1 ? "Không duyệt" : "Không duyệt xóa";
                query.MoTa = txtMota.Text;
                db.SubmitChanges();
                Success("Không duyệt thành công");
                Search(int.Parse(hdPage.Value));
            }
            #endregion [Không duyệt]
        }
        protected void rdDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Xem";
        }
        protected void rdKhongDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Duyệt";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;

            var updatehd = (from p in db.tblThueBoms
                            where p.ID == new Guid(ID)
                            select p).FirstOrDefault();
            if (e.CommandName.Equals("Sua"))
            {
                //System.Data.Linq.ISingleResult<sp_ThueBom_LichSuResult> item = dm.sp_ThueBom_LichSu(new Guid(ID));
                //gvDuyet.DataSource = item;
                //gvDuyet.DataBind();
                //btnDuyet.Visible = rdTrangThai.SelectedItem.Value == "1" ? true : false;
                //btnKhongDuyet.Visible = rdTrangThai.SelectedItem.Value == "1" ? true : false;
                //mpDuyet.Show();
                if (updatehd != null && updatehd.ID != null)
                {
                    if (updatehd.TrangThai == 1)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 10, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            updatehd.NguoiDuyet = Session["IDND"].ToString();
                            updatehd.TrangThai = 2;
                            updatehd.TrangThaiText = "Đã duyệt";
                            db.SubmitChanges();
                            Success("Đã duyệt");

                            Search(int.Parse(hdPage.Value));
                        }
                    }
                    else if (updatehd.TrangThai == 3)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 11, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Inverted(thongbao);
                            }
                        }
                        else
                        {
                            db.tblThueBoms.DeleteOnSubmit(updatehd);
                            db.SubmitChanges();
                            Success("Đã duyệt xóa");
                            Search(int.Parse(hdPage.Value));
                        }
                    }
                }
                else
                {
                    Search(int.Parse(hdPage.Value));
                }
            }
            else if (e.CommandName == "Huy")
            {
                hdID.Value = ID;
                mpDuyet.Show();
                txtMota.Text = "";
            }
            else if (e.CommandName == "Xem")
            {
                var view = betong.sp_ThueBom_LichSu(new Guid(ID));
                gvLichSu.DataSource = view;
                gvLichSu.DataBind();
                mpLichSu.Show();
            }
        }
        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[10].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Phụ phí",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tổng tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }

        protected void gvLichSu_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Người tạo",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tạo",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giờ xuất",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khách hàng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Gold,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xe vận chuyển",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khối lượng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Violet,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá bán bê tông",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.LawnGreen,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Aquamarine,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tổng tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Thông tin vật liệu trộn",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.Goldenrod,
                //};
                //headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Trọng lượng vật liệu trộn",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.DarkSlateGray,
                //};
                //headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin người làm việc",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
    }
}