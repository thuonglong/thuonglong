﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCGach
{
    public partial class uc_DuyetChotGachMenBong : System.Web.UI.UserControl
    {
        private string frmName = "frmSanXuatGach";
        private static int PageNumber = 20;
        private static int CurrentPage = 1;
        private DBDataContext db = new DBDataContext();
        private VatLieuDataContext vatlieu = new VatLieuDataContext();
        private clsPhanQuyen phanquyen = new clsPhanQuyen();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    LoadChiNhanh();
                    //  IDDN = new Guid(Session["IDDN"].ToString());
                }
            }
        }

        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            //dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        public DateTime GetLastDayOfMonth(int iMonth, int iYear)
        {
            DateTime dtResult = new DateTime(iYear, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-1);
            return dtResult;
        }

        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        protected void btnDong_Click(object sender, EventArgs e)
        {
            mpDuyet.Hide();

        }
        private void Search(int page)
        {
            //  rdTrangThai.SelectedValue =="1" : chưa duyệt
            //  rdTrangThai.SelectedValue =="2" : đã duyệt
            int isDuyet = rdTrangThai.SelectedValue == "1" ? 1 : 2;
            System.Data.Linq.ISingleResult<sp_GachMenBong_ListDuyetChotResult> dt = vatlieu.sp_GachMenBong_ListDuyetChot(GetDrop(dlChiNhanh), 1, PageNumber, page);
            GV.DataSource = dt;
            GV.DataBind();

        }
        protected void btnXem_OnClick(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                for (int i = 1; i <= gvDuyet.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvDuyet.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvDuyet.Rows[index - 1].Cells[i].BackColor = Color.LightCoral;
                            e.Row.Cells[i].BackColor = Color.LightCoral;
                        }
                    }
                }
            }

        }
        protected void grdDanhSachHD_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;
            GV.DataSource = null;
            GV.DataSource = vatlieu.sp_GachMenBong_LichSu(new Guid(ID));
            GV.DataBind();

            btnDuyet.Visible = rdTrangThai.SelectedValue == "1" ? true : false;
            btnKhongDuyet.Visible = rdTrangThai.SelectedValue == "1" ? true : false;
            mpDuyet.Show();
        }

        private Guid GetDrop(DropDownList dl)
        {
            if (dl.SelectedValue.Trim().Equals(""))
            {
                return Guid.Empty;
            }
            else
            {
                return new Guid(dl.SelectedValue);
            }
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        protected void grdDanhSachHD_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#EEFFAA'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            hdPage.Value = "1";
            Search(CurrentPage);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }

        protected void btnDuyetTB_Click(object sender, EventArgs e)
        {
            tblGachMenBong updatehd = (from p in db.tblGachMenBongs
                                       where p.ID == new Guid(hdID.Value)
                                       select p).FirstOrDefault();
            if (updatehd.TrangThai == 1)
            {
                #region [Duyệt]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    updatehd.TrangThai = 2;
                    updatehd.TrangThaiText = "Đã duyệt";
                    db.SubmitChanges();
                    Success("Đã duyệt giá mua vật liệu");
                }
                Search(1);
                #endregion
            }
            // Duyệt xóa
            else if (updatehd.TrangThai == 3)
            {
                #region [Duyệt xóa]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    db.tblGachMenBongs.DeleteOnSubmit(updatehd);
                    db.SubmitChanges();
                    Success("Đã duyệt xóa giá mua vật liệu.");
                    Search(1);
                }
                #endregion [Duyệt xóa]
            }
            else
            {

            }
        }

        protected void btnKDuyetTB_Click(object sender, EventArgs e)
        {
            #region [Không duyệt]

            #endregion [Không duyệt]

        }
        protected void rdDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Xem";
        }
        protected void rdKhongDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Duyệt";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;
            HiddenField hd = GV.FindControl("hdChiChanh") as HiddenField;

            var query = (from p in db.tblGachMenBongs
                         where p.NgayThang == DateTime.Parse(ID)
                         && p.IDChiNhanh == GetDrop(dlChiNhanh)
                         select p).FirstOrDefault();
            if (e.CommandName.Equals("Sua"))
            {
                if (query != null && query.ID != null)
                {
                    if (query.TrangThaiChot == 1)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            vatlieu.sp_GachMenBong_DuyetChot(query.IDChiNhanh, query.NgayThang, ref thongbao);
                            Search(int.Parse(hdPage.Value));
                            Success("Đã duyệt chốt");
                        }
                    }
                    else if (query.TrangThaiChot == 4)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Inverted(thongbao);
                            }
                        }
                        else
                        {
                            vatlieu.sp_GachMenBong_DuyetMoChot(query.IDChiNhanh, query.NgayThang, ref thongbao);
                            Success("Đã duyệt mở chốt");
                            Search(int.Parse(hdPage.Value));
                        }
                    }
                }
                else
                {
                    Search(int.Parse(hdPage.Value));
                }
            }
            else if (e.CommandName == "Huy")
            {
                if (query != null && query.ID != null)
                {
                    if (query.TrangThaiChot == 1)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            vatlieu.sp_GachTerrazo_DuyetMoChot(query.IDChiNhanh, query.NgayThang, ref thongbao);
                            Search(int.Parse(hdPage.Value));
                            Success("Đã duyệt chốt");
                        }
                    }
                    else if (query.TrangThaiChot == 4)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Inverted(thongbao);
                            }
                        }
                        else
                        {
                            vatlieu.sp_GachTerrazo_DuyetChot(query.IDChiNhanh, query.NgayThang, ref thongbao);
                            Success("Đã duyệt mở chốt");
                            Search(int.Parse(hdPage.Value));
                        }
                    }
                }
                else
                {
                    Search(int.Parse(hdPage.Value));
                }
            }
            //else if (e.CommandName == "Xem")
            //{
            //    var view = vatlieu.sp_GachMenBong_LichSuDuyetChot(query.IDChiNhanh, query.NgayThang).ToList();
            //    gvLichSu.DataSource = query;
            //    gvLichSu.DataBind();
            //    mpLichSu.Show();
            //}
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
    }
}