﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_DuyetChotGachTerrazo.ascx.cs" Inherits="ThuongLong.UCGach.uc_DuyetChotGachTerrazo" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-3" id="divchinhanh" runat="server">
                    <div class="form-group">
                        <label>Chi nhánh</label>
                        <asp:DropDownList ID="dlChiNhanh" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%" OnSelectedIndexChanged="btnSearch_Click" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3" runat="server" visible="false">
                    <div class="form-group">
                        <label>Trạng thái</label>
                        <asp:RadioButtonList ID="rdTrangThai" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Value="1">Chưa duyệt</asp:ListItem>
                            <asp:ListItem Value="2">Đã duyệt</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label></label>
                        <br />
                        <asp:Button ID="btnSearch" runat="server" Text="Xem" class="btn btn-primary" OnClick="btnSearch_Click" />
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                <Columns>
                    <asp:TemplateField HeaderText="Duyệt" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("NgayThang")%>' ToolTip="Duyệt"
                                CommandName="Sua" OnClientClick="return confirm('Bạn có muốn duyệt không?')"><i class="fa fa-check"></i></asp:LinkButton>
                            <asp:HiddenField ID="hdChiChanh" runat="server" Value='<%#Eval("IDChiNhanh")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Không duyệt" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtHuy" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Duyệt"
                                CommandName="Huy"><i class="fa fa-times" style="color:#ff0000;"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trạng thái">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblLichSu"
                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                CommandArgument='<%#Eval("TrangThaiText")%>' ForeColor="Purple" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                    <asp:BoundField DataField="SoVien300x300" HeaderText="Loại 300x300" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="SoVien400x400" HeaderText="Loại 400x400" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                    HorizontalAlign="Right" />
                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
            </asp:GridView>
        </div>
        <div style="margin: 5px;" class="btn-group">
            <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
            <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
            <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
        </div>

        <ajaxToolkit:ModalPopupExtender ID="mpDuyet" runat="server" CancelControlID="btnDong"
            Drag="True" TargetControlID="hdDuyet" BackgroundCssClass="modalBackground" PopupControlID="pnDuyet"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnDuyet" runat="server" Style="display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    DUYỆT
                </div>
                <div class="panel-body">
                    <asp:GridView ID="gvDuyet" runat="server" AutoGenerateColumns="false" Width="1120px" OnRowDataBound="GridView2_RowDataBound"
                        EmptyDataText="Không có dữ liệu nào" class="table table-striped table-bordered table-hover">
                        <Columns>
                            <asp:BoundField DataField="STT" HeaderText="Trạng thái" />
                            <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                            <asp:BoundField DataField="TenNhaCungCap" HeaderText="NCC vật liệu" />
                            <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                            <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                            <asp:BoundField DataField="TenDonViTinh" HeaderText="ĐVT" />
                            <asp:BoundField DataField="TenBienSoXe" HeaderText="Biển số xe" />
                            <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                            <asp:BoundField DataField="KLMua" HeaderText="KL mua" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="KLNhapKho" HeaderText="KL nhập kho" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="KLQuanCan" HeaderText="KL qua cân" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DonGiaCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TLTong" HeaderText="TL tổng" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TLBi" HeaderText="TL bì" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TLHang" HeaderText="TL hàng" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TyLeQuyDoi" HeaderText="Tỷ lệ quy đổi" ItemStyle-HorizontalAlign="Right" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnDuyet" runat="server" CssClass="btn btn-primary" OnClick="btnDuyetTB_Click"
                        OnClientClick="return confirm('Bạn chắc chắn muốn duyệt ?')" Text="Duyệt" />
                    <asp:Button ID="btnKhongDuyet" runat="server" CssClass="btn btn-warning" OnClick="btnKDuyetTB_Click"
                        Text="Không duyệt" />
                    <asp:Button ID="btnDong" runat="server" CssClass="btn btn-danger"
                        Text="Đóng" />
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hdID" runat="server" />
        <asp:HiddenField ID="hdPage" runat="server" />
        <asp:HiddenField ID="hdDuyet" runat="server" />

        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

        <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
            Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center">
                    Lịch sử
                </div>
                <div class="panel-body" style="max-height: 500px; overflow: auto;">
                    <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                        EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                        <Columns>
                            <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                            <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                            <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                            <asp:BoundField DataField="TenNhaCungCap" HeaderText="NCC vật liệu" />
                            <asp:BoundField DataField="TenNhomVatLieu" HeaderText="Nhóm vật liệu" />
                            <asp:BoundField DataField="TenLoaiVatLieu" HeaderText="Loại vật liệu" />
                            <asp:BoundField DataField="TenDonViTinh" HeaderText="ĐVT" />
                            <asp:BoundField DataField="TenBienSoXe" HeaderText="Biển số xe" />
                            <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                            <asp:BoundField DataField="KLMua" HeaderText="KL mua" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="KLNhapKho" HeaderText="KL nhập kho" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="KLQuanCan" HeaderText="KL qua cân" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DonGiaCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DonGiaKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="ThanhTienCoThue" HeaderText="Hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="ThanhTienKhongThue" HeaderText="Thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TLTong" HeaderText="TL tổng" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TLBi" HeaderText="TL bì" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TLHang" HeaderText="TL hàng" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="TyLeQuyDoi" HeaderText="Tỷ lệ quy đổi" ItemStyle-HorizontalAlign="Right" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer" style="align-content: center; text-align: center">
                    <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                </div>
            </div>
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <%--<asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="btnGiaTri" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btncapnhat" />
            <asp:PostBackTrigger ControlID="btnKhoiLuong" />--%>
    </Triggers>
</asp:UpdatePanel>


