﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Globalization;

namespace ThuongLong
{
    public static class JSONHelper
    {
        public static string ToJSON(this object obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(obj);
        }

        public static string ToJSON(this object obj, int recursionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recursionDepth;
            return serializer.Serialize(obj);
        }
    }

    public partial class _Default : Page
    {
        DBDataContext db = new DBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    LoadChiNhanh();
                    GetData(dlChiNhanh.SelectedValue);
                    //LoadDuLieuNgay();
                    //LoadBienDo();

                    DateTime dateTimenow = DateTime.Now;
                    txtNgayThang.Text = dateTimenow.ToString("dd/MM/yyyy");
                }
            }
        }

        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Tất cả chi nhánh", "All"));
        }
        public void GetData(string idchinhanh)
        {
            var query = db.sp_Default_LoadBieuDo(idchinhanh, DateTime.Now.Month, DateTime.Now.Year).FirstOrDefault();

            string chart = "";


            double? kLBeTongBan = 0;
            double? kLBeTongDaTron = 0;
            double? kLBeTongDuKien = 0;
            decimal? tongThu = 0;
            decimal? tongChi = 0;
            decimal? congNoThu = 0;
            decimal? congNoTra = 0;



            List<sp_Default_LoadDuLieuResult> query1 = db.sp_Default_LoadDuLieu(idchinhanh, DateTime.Now.Date, ref kLBeTongBan,
             ref kLBeTongDaTron,
             ref kLBeTongDuKien,
             ref tongThu,
             ref tongChi,
             ref congNoThu,
             ref congNoTra).ToList();


            lbTongThu.InnerText = string.Format("{0:N0}", tongThu == null ? 0 : tongThu);
            lbTongChi.InnerText = string.Format("{0:N0}", tongChi == null ? 0 : tongChi);
            lbCongNoThu.InnerText = string.Format("{0:N0}", congNoThu == null ? 0 : congNoThu);
            lbCongNoChi.InnerText = string.Format("{0:N0}", congNoTra == null ? 0 : congNoTra);

            //lbKLBeTongBan.InnerText = string.Format("{0:N2}", kLBeTongBan == null ? 0 : kLBeTongBan);
            //lbKLBeTongDaTron.InnerText = string.Format("{0:N2}", kLBeTongDaTron == null ? 0 : kLBeTongDaTron);
            //lbKLBeTongDuKien.InnerText = string.Format("{0:N2}", kLBeTongDuKien == null ? 0 : kLBeTongDuKien);


            chart += "<div class=\"row\">\n";

            chart += "<div class=\"col-md-6\">\n";
            chart += "            <div class=\"box box-primary\">\n";
            chart += "                <div class=\"box-header\">\n";
            chart += "                    <h3 class=\"box-title\">Quỹ</h3>\n";
            chart += "                </div>\n";
            chart += "                <div class=\"box-body table-responsive no-padding\">\n";
            chart += "                    <table class=\"table table-hover\">\n";
            chart += "                        <tbody>\n";
            chart += "                            <tr>\n";
            chart += "                                <th style=\"width: 70%\">Task</th>\n";
            chart += "                                <th style=\"width: 30%\">VNĐ</th>\n";
            chart += "                            </tr>\n";
            chart += "                            <tr>\n";
            chart += "                                <td>Tổng thu trong ngày</td>\n";
            chart += "                                <td>" + string.Format("{0:N0}", tongThu) + "</td>\n";
            chart += "                            </tr>\n";
            chart += "                            <tr>\n";
            chart += "                                <td>Tổng chi trong ngày</td>\n";
            chart += "                                <td>" + string.Format("{0:N0}", tongChi) + "</td>\n";
            chart += "                            </tr>\n";
            chart += "                            <tr>\n";
            chart += "                                <td>Công nợ phải thu phát sinh trong ngày</td>\n";
            chart += "                                <td>" + string.Format("{0:N0}", congNoThu) + "</td>\n";
            chart += "                            </tr>\n";
            chart += "                            <tr>\n";
            chart += "                                <td>Công nợ phải trả phát sinh trong ngày</td>\n";
            chart += "                                <td>" + string.Format("{0:N0}", congNoTra) + "</td>\n";
            chart += "                            </tr>\n";
            chart += "                        </tbody>\n";
            chart += "                    </table>\n";
            chart += "                </div>\n";

            chart += "            </div>\n";

            //chart += "        </div>\n";

            //chart += "<div class=\"col-md-6\">\n";

            chart += "            <div class=\"box box-info\">\n";
            chart += "                <div class=\"box-header\">\n";
            chart += "                    <h3 class=\"box-title\">Sản xuất bê tông</h3>\n";
            chart += "                </div>\n";
            chart += "                <div class=\"box-body table-responsive no-padding\">\n";
            chart += "                    <table class=\"table table-hover\">\n";
            chart += "                        <tbody>\n";
            chart += "                            <tr>\n";
            chart += "                                <th style=\"width: 70%\">Task</th>\n";
            chart += "                                <th style=\"width: 30%\">Khối lượng</th>\n";
            chart += "                            </tr>\n";
            chart += "                            <tr>\n";
            chart += "                                <td>Khối lượng bê tông đã bán trong ngày</td>\n";
            chart += "                                <td>" + string.Format("{0:N2}", kLBeTongBan) + "</td>\n";
            chart += "                            </tr>\n";
            chart += "                            <tr>\n";
            chart += "                                <td>Khối lượng bê tông đã trộn trong ngày</td>\n";
            chart += "                                <td>" + string.Format("{0:N2}", kLBeTongDaTron) + "</td>\n";
            chart += "                            </tr>\n";
            chart += "                            <tr>\n";
            chart += "                                <td>Khối lượng bê tông dự kiến bán trong ngày</td>\n";
            chart += "                                <td>" + string.Format("{0:N2}", kLBeTongDuKien) + "</td>\n";
            chart += "                            </tr>\n";
            chart += "                        </tbody>\n";
            chart += "                    </table>\n";
            chart += "                </div>\n";
            chart += "            </div>\n";

            //chart += "            </div>\n";

            //chart += "<div class=\"col-md-6\">\n";

            if (query1.Count > 0)
            {
                chart += "            <div class=\"box box-success\">\n";
                chart += "                <div class=\"box-header\">\n";
                chart += "                    <h3 class=\"box-title\">Sản xuất gạch</h3>\n";
                chart += "                </div>\n";
                chart += "                <div class=\"box-body table-responsive no-padding\">\n";
                chart += "                    <table class=\"table table-hover\">\n";
                chart += "                        <tbody>\n";
                chart += "                            <tr>\n";
                chart += "                                <th style=\"width: 70%\">Loại gạch</th>\n";
                chart += "                                <th style=\"width: 30%\">Số lượng</th>\n";
                chart += "                            </tr>\n";
                foreach (var item in query1)
                {
                    chart += "                            <tr>\n";
                    chart += "                                <td>" + item.TenLoaiVatLieu + "</td>\n";
                    chart += "                                <td>" + string.Format("{0:N0}", item.SoLuong) + "</td>\n";
                    chart += "                            </tr>\n";
                }
                chart += "                        </tbody>\n";
                chart += "                    </table>\n";
                chart += "                </div>\n";
                chart += "            </div>\n";
            }

            //chart += "            </div>\n";

            List<sp_Default_LoadBanGachResult> bangach = db.sp_Default_LoadBanGach(idchinhanh, DateTime.Now.Date).ToList();

            //chart += "<div class=\"col-md-6\">\n";

            if (bangach.Count > 0)
            {
                chart += "            <div class=\"box box-warning\">\n";
                chart += "                <div class=\"box-header\">\n";
                chart += "                    <h3 class=\"box-title\">Bán gạch</h3>\n";
                chart += "                </div>\n";
                chart += "                <div class=\"box-body table-responsive no-padding\">\n";
                chart += "                    <table class=\"table table-hover\">\n";
                chart += "                        <tbody>\n";
                chart += "                            <tr>\n";
                chart += "                                <th style=\"width: 70%\">Loại gạch</th>\n";
                chart += "                                <th style=\"width: 30%\">Số lượng</th>\n";
                chart += "                            </tr>\n";
                foreach (var item in bangach)
                {
                    chart += "                            <tr>\n";
                    chart += "                                <td>" + item.TenLoaiVatLieu + "</td>\n";
                    chart += "                                <td>" + string.Format("{0:N0}", item.SoLuong) + "</td>\n";
                    chart += "                            </tr>\n";
                }
                chart += "                        </tbody>\n";
                chart += "                    </table>\n";
                chart += "                </div>\n";
                chart += "            </div>\n";
            }


            List<sp_Default_LoadNKVLResult> nkvl = db.sp_Default_LoadNKVL(idchinhanh, DateTime.Now.Date).ToList();

            //chart += "<div class=\"col-md-6\">\n";
            if (nkvl.Count > 0)
            {
                chart += "            <div class=\"box box-danger\">\n";
                chart += "                <div class=\"box-header\">\n";
                chart += "                    <h3 class=\"box-title\">Nhập kho vật liệu</h3>\n";
                chart += "                </div>\n";
                chart += "                <div class=\"box-body table-responsive no-padding\">\n";
                chart += "                    <table class=\"table table-hover\">\n";
                chart += "                        <tbody>\n";
                chart += "                            <tr>\n";
                chart += "                                <th style=\"width: 70%\">Loại vật liệu</th>\n";
                chart += "                                <th style=\"width: 30%\">Số lượng</th>\n";
                chart += "                            </tr>\n";
                foreach (var item in nkvl)
                {
                    chart += "                            <tr>\n";
                    chart += "                                <td>" + item.TenLoaiVatLieu + "</td>\n";
                    chart += "                                <td>" + string.Format("{0:N0}", item.SoLuong) + "</td>\n";
                    chart += "                            </tr>\n";
                }
                chart += "                        </tbody>\n";
                chart += "                    </table>\n";
                chart += "                </div>\n";
                chart += "            </div>\n";
            }
            //chart += "            </div>\n";

            List<sp_Default_LoadBanVLResult> banvl = db.sp_Default_LoadBanVL(idchinhanh, DateTime.Now.Date).ToList();

            //chart += "<div class=\"col-md-6\">\n";
            if (banvl.Count > 0)
            {
                chart += "            <div class=\"box box-info\">\n";
                chart += "                <div class=\"box-header\">\n";
                chart += "                    <h3 class=\"box-title\">Bán vật liệu</h3>\n";
                chart += "                </div>\n";
                chart += "                <div class=\"box-body table-responsive no-padding\">\n";
                chart += "                    <table class=\"table table-hover\">\n";
                chart += "                        <tbody>\n";
                chart += "                            <tr>\n";
                chart += "                                <th style=\"width: 70%\">Loại vật liệu</th>\n";
                chart += "                                <th style=\"width: 30%\">Số lượng</th>\n";
                chart += "                            </tr>\n";
                foreach (var item in banvl)
                {
                    chart += "                            <tr>\n";
                    chart += "                                <td>" + item.TenLoaiVatLieu + "</td>\n";
                    chart += "                                <td>" + string.Format("{0:N0}", item.SoLuong) + "</td>\n";
                    chart += "                            </tr>\n";
                }
                chart += "                        </tbody>\n";
                chart += "                    </table>\n";
                chart += "                </div>\n";
                chart += "            </div>\n";
            }
            //chart += "            </div>\n";

            chart += "            </div>\n";

            //cột bên phải
            chart += "<div class=\"col-md-6\">\n";
            chart += "<div class=\"chart\">\n";
            chart += "<canvas id = \"canvas\" style = \"display: block; height: 500px;\" height = \"500\" class=\"chartjs -render-monitor\" ></canvas>";
            chart += "</div>\n";

            chart += "<div class=\"box box-warning\">\n";
            chart += "  <div class=\"box-header\">\n";
            chart += "      <h3 class=\"box-title\">Doanh thu các bộ phận trong tháng</h3>\n";
            chart += "  </div>\n";
            chart += "  <div class=\"box-body table-responsive no-padding\">\n";
            chart += LoadDoanhThuBoPhan();
            chart += "  </div>\n";
            chart += "</div>\n";

            chart += "</div>\n";

            chart += "<script>\n";

            chart += "var barChartData = {\n";
            chart += "    labels: ['Bê tông', 'Gạch', 'Vật liệu', 'Tổng'],\n";
            chart += "	datasets:\n";
            chart += "    [{\n";
            chart += "    label: 'Năm trước',\n";
            chart += "		backgroundColor: 'rgb(' + 255 + ', ' + 195 + ', ' + 0 + ',' + 0.5 + ')',\n";
            chart += "		borderColor: 'rgb(' + 255 + ', ' + 195 + ', ' + 0 + ')',\n";
            chart += "		borderWidth: 1,\n";
            chart += "		data:[\n";
            chart += "           " + query.BeTongNamTruoc + ",\n";
            chart += "           " + query.GachNamTruoc + ",\n";
            chart += "           " + query.VatLieuNamTruoc + ",\n";
            chart += "           " + query.TongNamTruoc + "\n";
            chart += "       ]\n";
            chart += "\n";
            chart += "    }, {\n";
            chart += "    label: 'Năm hiện tại',\n";
            chart += "		backgroundColor: 'rgb(' + 0 + ', ' + 128 + ', ' + 0 + ', ' + 0.5 + ')',\n";
            chart += "		borderColor: 'rgb(' + 0 + ', ' + 128 + ', ' + 0 + ')',\n";
            chart += "		borderWidth: 1,\n";
            chart += "		data:[\n";
            chart += "           " + query.BeTong + ",\n";
            chart += "           " + query.Gach + ",\n";
            chart += "           " + query.VatLieu + ",\n";
            chart += "           " + query.Tong + "\n";
            chart += "       ]\n";
            chart += "\n";
            chart += "    }]\n";
            chart += "\n";
            chart += "};\n";

            chart += "window.onload = function()\n";
            chart += "{\n";
            chart += "var ctx = document.getElementById('canvas').getContext('2d');\n";
            chart += "window.myBar = new Chart(ctx, {\n";
            chart += "type: 'bar',\n";
            chart += "data: barChartData,\n";
            chart += "options: {\n";

            chart += "tooltips:\n";
            chart += "    {\n";
            chart += "    callbacks:\n";
            chart += "        {\n";
            chart += "        label: function(tooltipItem, data) {\n";
            chart += "                return Number(tooltipItem.yLabel).toFixed(0).replace(/(\\d)(?=(\\d\\d\\d)+(?!\\d))/g, \"$&,\");\n";
            chart += "            }\n";
            chart += "        }\n";
            chart += "    },\n";

            //chart += "scales:\n";
            //chart += "    {\n";
            //chart += "    yAxes:\n";
            //chart += "        [{\n";
            //chart += "        ticks:\n";
            //chart += "            {\n";
            //chart += "              beginAtZero: true,\n";
            //chart += "              stepSize: 3.5\n";
            //chart += "                            },\n";
            //chart += "                        }]\n";
            //chart += "                    },\n";


            chart += "responsive: true,\n";

            chart += "legend:\n";
            chart += "{\n";
            chart += "position: 'top',\n";

            chart += "},\n";

            chart += "title:\n";
            chart += "{\n";
            chart += "display: true,\n";

            chart += "  text: 'Tình hình kinh doanh'\n";

            chart += "}\n";
            chart += "}\n";
            chart += "});\n";


            chart += "};\n";
            chart += "</script>\n";

            chart += "        </div>\n";

            Literal1.Text = chart;
        }
        void LoadBienDo()
        {
            string idchinhanh = "";
            foreach (ListItem item in dlChiNhanh.Items)
            {
                if (item.Selected)
                {
                    idchinhanh += item.Value;
                }
            }
            var query = db.sp_Default_LoadBieuDo(idchinhanh, DateTime.Now.Month, DateTime.Now.Year).FirstOrDefault();

            string chart = "";
            DateTime dateTime = DateTime.Now;
            int day = dateTime.Day;


            chart += "<div class=\"col-md-6\">\n";
            chart += "<div class=\"chart\">\n";
            chart += "<canvas id = \"canvas\" style = \"display: block; height: 500px;\" height = \"500\" class=\"chartjs -render-monitor\" ></canvas>";
            chart += "</div>\n";
            chart += "</div>\n";


            chart += "<script>\n";

            chart += "var barChartData = {\n";
            chart += "    labels: ['Bê tông', 'Gạch', 'Vật liệu', 'Tổng'],\n";
            chart += "	datasets:\n";
            chart += "    [{\n";
            chart += "    label: 'Năm trước',\n";
            chart += "		backgroundColor: 'rgb(' + 255 + ', ' + 195 + ', ' + 0 + ',' + 0.5 + ')',\n";
            chart += "		borderColor: 'rgb(' + 255 + ', ' + 195 + ', ' + 0 + ')',\n";
            chart += "		borderWidth: 1,\n";
            chart += "		data:[\n";
            chart += "           " + query.BeTongNamTruoc + ",\n";
            chart += "           " + query.GachNamTruoc + ",\n";
            chart += "           " + query.VatLieuNamTruoc + ",\n";
            chart += "           " + query.TongNamTruoc + "\n";
            chart += "       ]\n";
            chart += "\n";
            chart += "    }, {\n";
            chart += "    label: 'Năm hiện tại',\n";
            chart += "		backgroundColor: 'rgb(' + 0 + ', ' + 128 + ', ' + 0 + ', ' + 0.5 + ')',\n";
            chart += "		borderColor: 'rgb(' + 0 + ', ' + 128 + ', ' + 0 + ')',\n";
            chart += "		borderWidth: 1,\n";
            chart += "		data:[\n";
            chart += "           " + query.BeTong + ",\n";
            chart += "           " + query.Gach + ",\n";
            chart += "           " + query.VatLieu + ",\n";
            chart += "           " + query.Tong + "\n";
            chart += "       ]\n";
            chart += "\n";
            chart += "    }]\n";
            chart += "\n";
            chart += "};\n";

            chart += "window.onload = function()\n";
            chart += "{\n";
            chart += "var ctx = document.getElementById('canvas').getContext('2d');\n";
            chart += "window.myBar = new Chart(ctx, {\n";
            chart += "type: 'bar',\n";
            chart += "data: barChartData,\n";
            chart += "options: {\n";
            chart += "responsive: true,\n";

            chart += "legend:\n";
            chart += "{\n";
            chart += "position: 'top',\n";

            chart += "},\n";

            chart += "title:\n";
            chart += "{\n";
            chart += "display: true,\n";

            chart += "  text: 'Tình hình kinh doanh'\n";

            chart += "}\n";
            chart += "}\n";
            chart += "});\n";


            chart += "};\n";
            chart += "</script>\n";

            //betongChart.Text = chart;
        }

        string LoadDoanhThuBoPhan()
        {
            var query = db.sp_Default_LoadDoanhThuBoPhan().FirstOrDefault();

            string chart;

            chart = "<canvas id = \"donutChart\" style = \"min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;\" ></canvas>\n";
            chart += "<script>\n";
            chart += "$(function() {\n";

            chart += " var donutChartCanvas = $('#donutChart').get(0).getContext('2d')\n";
            chart += " var donutData = {\n";

            chart += "  labels: ['Bê tông', 'Gạch', 'Vật liệu'],\n";
            chart += "  datasets: [\n";
            chart += "    {\n";
            chart += "      data: [" + query.BeTong.ToString() + "," + query.Gach.ToString() + "," + query.VatLieu.ToString() + "],\n";

            chart += "      backgroundColor : ['#f56954', '#00a65a', '#f39c12'],\n";

            chart += "    }\n";
            chart += "  ]\n";
            chart += "}\n";
            chart += " var donutOptions = {\n";
            chart += "  maintainAspectRatio : false,\n";
            chart += "  responsive : true,\n";
            chart += "}\n";

            chart += " var donutChart = new Chart(donutChartCanvas, {\n";
            chart += "  type: 'doughnut',\n";
            chart += "  data: donutData,\n";
            chart += "  options: donutOptions\n";
            chart += "})\n";

            chart += "})</script>\n";
            return chart;
        }



        [WebMethod(EnableSession = true)]
        public static string LoadChart(string idChinhanh, string date)
        {
            //DateTime dateSearch = DateTime.Parse(date);
            DateTime dateSearch = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //var data = new
            //{
            //    labels = new[] { "South Western", "Western", "Northern", "North Eastern", "Eastern" },
            //    datasets = new[] {
            //        new  { label = "2016 - 2017", backgroundColor = new[] { "rgba(220, 220, 220, 0.5)" }, pointBorderColor = "#fff", data = new[] {"100","200","150","375","500"} }
            //        ,new  { label = "2017 - 2018", backgroundColor = new[] { "#018AC8", "#058EAA", "#DEB816", "#E37F1C", "#4A5D0E" }, pointBorderColor = "#fff", data = new[] {"122","210","175","375","525"}  }

            //    }
            //};

            DBDataContext db = new DBDataContext();
            //List<sp_LoadChiNhanhDaiLyResult> queryChinhanh = db.sp_LoadChiNhanhDaiLy().ToList();

            var query = db.sp_Default_LoadBieuDo(idChinhanh, dateSearch.Month, dateSearch.Year).FirstOrDefault();
            var dataSumaryMonthWithPlan = new
            {
                labels = new[] { "Bê tông", "Gạch", "Vật liệu" },
                datasets = new object[] {
                    new { label = "Thực tế",
                            
                            backgroundColor = new[] { "rgba(255, 99, 132, 0.5)",
                                "rgba(255, 99, 132, 0.5)",
                                "rgba(255, 99, 132, 0.5)",
                                "rgba(255, 99, 132, 0.5)"},
                            borderColor = new[] { "rgba(255,99,132,1)",
                                "rgba(255,99,132,1)",
                                "rgba(255,99,132,1)",
                                "rgba(255,99,132,1)"},
                            borderWidth = 1,
                            data = new[] { query.BeTong ,
                                query.Gach, 
                                query.VatLieu }
                    },
                    new { label = "Kế hoạch",
                            backgroundColor = new[] {
                                "rgba(54, 162, 235, 0.2)",
                                "rgba(54, 162, 235, 0.2)",
                                "rgba(54, 162, 235, 0.2)",
                                "rgba(54, 162, 235, 0.2)"
                            },
                            borderColor = new[] {
                                "rgba(54, 162, 235, 1)",
                                "rgba(54, 162, 235, 1)",
                                "rgba(54, 162, 235, 1)",
                                "rgba(54, 162, 235, 1)"
                            },
                            borderWidth = 1,
                            data = new[] { query.KeHoachBeTong,
                                query.KeHoachGach,
                                query.KeHoachVatLieu}
                    }
                }
            };

            //dataSummaryMonth
            var queryDataMonth = db.sp_Default_LoadDoanhThuBoPhan().FirstOrDefault();
            var dataSumaryMonth = new
            {
                labels = new[] { "Bê tông", "Gạch", "Vật liệu" },
                datasets = new object[] {
                    new { 
                            backgroundColor = new[] {                                 
                                "rgba(255, 99, 132, 0.5)",
                                "rgba(54, 162, 235, 0.5)",
                                "rgba(255, 206, 86, 0.5)",
                                "rgba(75, 192, 192, 0.5)",
                                "rgba(153, 102, 255, 0.5)",
                                "rgba(255, 159, 64, 0.5)" },
                            borderColor = new[] {
                                "rgba(255,99,132,1)",
                                "rgba(54, 162, 235, 1)",
                                "rgba(255, 206, 86, 1)",
                                "rgba(75, 192, 192, 1)",
                                "rgba(153, 102, 255, 1)",
                                "rgba(255, 159, 64, 1)"
                            },
                            data = new[] { query.BeTong.ToString() , 
                                query.Gach.ToString(),
                                query.VatLieu.ToString() }
                    }
                }
            };



            double? kLBeTongBan = 0;
            double? kLBeTongDaTron = 0;
            double? kLBeTongDuKien = 0;
            decimal? tongThu = 0;
            decimal? tongChi = 0;
            decimal? congNoThu = 0;
            decimal? congNoTra = 0;
            
            List<sp_Default_LoadDuLieuResult> query1 = db.sp_Default_LoadDuLieu(idChinhanh, dateSearch, ref kLBeTongBan,
             ref kLBeTongDaTron,
             ref kLBeTongDuKien,
             ref tongThu,
             ref tongChi,
             ref congNoThu,
             ref congNoTra).ToList();

            string chartSanXuat = "";
            //ban gach
            chartSanXuat += getInfoSanXuatGach(query1);

            List<sp_Default_LoadBanGachResult> bangach = db.sp_Default_LoadBanGach(idChinhanh, dateSearch).ToList();
            chartSanXuat += getInfoBanGach(bangach);
     

            List<sp_Default_LoadNKVLResult> nkvl = db.sp_Default_LoadNKVL(idChinhanh, dateSearch).ToList();
            chartSanXuat += getInfoNhapKhoVL(nkvl);
            

            List<sp_Default_LoadBanVLResult> banvl = db.sp_Default_LoadBanVL(idChinhanh, dateSearch).ToList();
            chartSanXuat += getInfoBanVL(banvl);


            //summary data
            var data = new
            {
                dataSumaryMonthWithPlanChart = dataSumaryMonthWithPlan,
                dataSumaryMonthChart = dataSumaryMonth,
                KLBeTongBan = string.Format("{0:N2}", kLBeTongBan == null ? 0 : kLBeTongBan),
                KLBeTongDaTron = string.Format("{0:N2}", kLBeTongDaTron == null ? 0 : kLBeTongDaTron),
                KLBeTongDuKien = string.Format("{0:N2}", kLBeTongDuKien == null ? 0 : kLBeTongDuKien),
                TongThu = string.Format("{0:N0}", tongThu == null ? 0 : tongThu),
                TongChi = string.Format("{0:N0}", tongChi == null ? 0 : tongChi),
                CongNoThu = string.Format("{0:N0}", congNoThu == null ? 0 : congNoThu),
                CongNoChi = string.Format("{0:N0}", congNoTra == null ? 0 : congNoTra),
                dataChartSanXuat = chartSanXuat,
            };
            return data.ToJSON();
        }

        public static string getInfoSanXuatGach(List<sp_Default_LoadDuLieuResult> query1)
        {
            string chart = "";
            if (query1.Count > 0)
            {
                chart += "<div class=\"col-md-6 grid-margin stretch-card\">\n";
                chart += "      <div class=\"card\">\n";
                chart += "            <div class=\"card-body\">\n";
                chart += "              <h4 class=\"card-title\">Sản xuất gạch</h4>\n";
                chart += "                 <div class=\"table-responsive \">\n";
                chart += "                    <table class=\"table table-striped\">\n";
                chart += "                        <thead>\n";
                chart += "                            <tr>\n";
                chart += "                                <th style=\"width: 70%\">Loại gạch</th>\n";
                chart += "                                <th style=\"width: 30%\">Số lượng</th>\n";
                chart += "                            </tr>\n";
                chart += "                        </thead>\n";
                chart += "                        <tbody>\n";
                foreach (var item in query1)
                {
                    chart += "                            <tr>\n";
                    chart += "                                <td>" + item.TenLoaiVatLieu + "</td>\n";
                    chart += "                                <td><strong class=\"text-primary\">" + string.Format("{0:N0}", item.SoLuong) + "</strong></td>\n";
                    chart += "                            </tr>\n";
                }
                chart += "                        </tbody>\n";
                chart += "                    </table>\n";
                chart += "                </div>\n";
                chart += "            </div>\n";
                chart += "       </div>\n";
                chart += "</div>\n";
            }

            return chart;
        }
        public static string getInfoBanGach(List<sp_Default_LoadBanGachResult> bangach)
        {
            string chart = "";
            if (bangach.Count > 0)
            {
                chart += "<div class=\"col-md-6 grid-margin stretch-card\">\n";
                chart += "      <div class=\"card\">\n";
                chart += "            <div class=\"card-body\">\n";
                chart += "              <h4 class=\"card-title\">Bán gạch</h4>\n";
                chart += "                 <div class=\"table-responsive \">\n";
                chart += "                    <table class=\"table table-striped\">\n";
                chart += "                        <thead>\n";
                chart += "                            <tr>\n";
                chart += "                                <th style=\"width: 70%\">Loại gạch</th>\n";
                chart += "                                <th style=\"width: 30%\">Số lượng</th>\n";
                chart += "                            </tr>\n";
                chart += "                        </thead>\n";
                chart += "                        <tbody>\n";
                foreach (var item in bangach)
                {
                    chart += "                            <tr>\n";
                    chart += "                                <td>" + item.TenLoaiVatLieu + "</td>\n";
                    chart += "                                <td><strong class=\"text-primary\">" + string.Format("{0:N0}", item.SoLuong) + "</strong></td>\n";
                    chart += "                            </tr>\n";
                }
                chart += "                        </tbody>\n";
                chart += "                    </table>\n";
                chart += "                </div>\n";
                chart += "            </div>\n";
                chart += "       </div>\n";
                chart += "</div>\n";
            }

            return chart;
        }
        public static string getInfoBanVL(List<sp_Default_LoadBanVLResult> banvl)
        {
            string chart = "";
            if (banvl.Count > 0)
            {
                chart += "<div class=\"col-md-6 grid-margin stretch-card\">\n";
                chart += "      <div class=\"card\">\n";
                chart += "            <div class=\"card-body\">\n";
                chart += "              <h4 class=\"card-title\">Bán vật liệu</h4>\n";
                chart += "                 <div class=\"table-responsive \">\n";
                chart += "                    <table class=\"table table-striped\">\n";
                chart += "                        <thead>\n";
                chart += "                            <tr>\n";
                chart += "                                <th style=\"width: 70%\">Loại gạch</th>\n";
                chart += "                                <th style=\"width: 30%\">Số lượng</th>\n";
                chart += "                            </tr>\n";
                chart += "                        </thead>\n";
                chart += "                        <tbody>\n";
                foreach (var item in banvl)
                {
                    chart += "                            <tr>\n";
                    chart += "                                <td>" + item.TenLoaiVatLieu + "</td>\n";
                    chart += "                                <td><strong class=\"text-primary\">" + string.Format("{0:N0}", item.SoLuong) + "</strong></td>\n";
                    chart += "                            </tr>\n";
                }
                chart += "                        </tbody>\n";
                chart += "                    </table>\n";
                chart += "                </div>\n";
                chart += "            </div>\n";
                chart += "       </div>\n";
                chart += "</div>\n";
            }
            return chart;
        }
        public static string getInfoNhapKhoVL(List<sp_Default_LoadNKVLResult> nkvl)
        {
            string chart = "";
            if (nkvl.Count > 0)
            {
                chart += "<div class=\"col-md-6 grid-margin stretch-card\">\n";
                chart += "      <div class=\"card\">\n";
                chart += "            <div class=\"card-body\">\n";
                chart += "              <h4 class=\"card-title\">Nhập kho vật liệu</h4>\n";
                chart += "                 <div class=\"table-responsive \">\n";
                chart += "                    <table class=\"table table-striped\">\n";
                chart += "                        <thead>\n";
                chart += "                            <tr>\n";
                chart += "                                <th style=\"width: 70%\">Loại gạch</th>\n";
                chart += "                                <th style=\"width: 30%\">Số lượng</th>\n";
                chart += "                            </tr>\n";
                chart += "                        </thead>\n";
                chart += "                        <tbody>\n";
                foreach (var item in nkvl)
                {
                    chart += "                            <tr>\n";
                    chart += "                                <td>" + item.TenLoaiVatLieu + "</td>\n";
                    chart += "                                <td><strong class=\"text-primary\">" + string.Format("{0:N0}", item.SoLuong) + "</strong></td>\n";
                    chart += "                            </tr>\n";
                }
                chart += "                        </tbody>\n";
                chart += "                    </table>\n";
                chart += "                </div>\n";
                chart += "            </div>\n";
                chart += "       </div>\n";
                chart += "</div>\n";
            }
            return chart;
        }

    }
}