﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class CongVanDen : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmCongVanDen";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmCongVanDen", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNhaCungCap();
                        LoadNhaCungCapSearch();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNhaCungCap()
        {
            var query = db.sp_LoadNhaCungCap();
            dlNhaCungCap.DataSource = query;
            dlNhaCungCap.DataBind();
            dlNhaCungCap.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadNhaCungCapSearch()
        {
            dlNhaCungCapSearch.Items.Clear();
            var query = (from p in db.tblCongVanDens
                         join q in db.tblNhaCungCaps on p.IDNhaCungCap equals q.ID
                         where p.IDChiNhanh == GetDrop(dlChiNhanhSearch)
                         select new
                         {
                             ID = p.IDNhaCungCap,
                             Ten = q.TenNhaCungCap
                         }).Distinct().OrderBy(p => p.Ten);
            dlNhaCungCapSearch.DataSource = query;
            dlNhaCungCapSearch.DataBind();
            dlNhaCungCapSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }
            else if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp";
            }
            else if (txtTenCongVan.Text.Trim() == "")
            {
                s += " - Nhập tên công văn";
            }
            else if (txtNoiDung.Text.Trim() == "")
            {
                s += " - Nhập nội dung";
            }
            else if (txtNguoiNhan.Text.Trim() == "")
            {
                s += " - Nhập người nhận";
            }
            else if (txtNgayLamCongVan.Text.Trim() == "")
            {
                s += " - Nhập ngày làm công văn";
            }
            else if (txtNgayNhan.Text.Trim() == "")
            {
                s += " - Nhập ngày nhận";
            }
            else if (FileUpload1.HasFile == false)
            {
                s += " - Chọn file scan công văn";
            }
            //if (s == "")
            //{
            //    var query = vatlieu.sp_CongVanDen_CheckThem(GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), GetDrop(dlNoiNhan), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text) > 0 ? true : false,
            //        DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            //}
            if (s != "")
                GstGetMess(s, "");
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh";
            }
            else if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp";
            }
            else if (txtTenCongVan.Text.Trim() == "")
            {
                s += " - Nhập tên công văn";
            }
            else if (txtNoiDung.Text.Trim() == "")
            {
                s += " - Nhập nội dung";
            }
            else if (txtNguoiNhan.Text.Trim() == "")
            {
                s += " - Nhập người nhận";
            }
            else if (txtNgayLamCongVan.Text.Trim() == "")
            {
                s += " - Nhập ngày làm công văn";
            }
            else if (txtNgayNhan.Text.Trim() == "")
            {
                s += " - Nhập ngày nhận";
            }
            //else if (FileUpload1.HasFile == false)
            //{
            //    s += " - Chọn file scan công văn";
            //}
            //if (s == "")
            //{
            //    var query = vatlieu.sp_CongVanDen_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), GetDrop(dlNoiNhan), GetDrop(dlNhomVatLieu), GetDrop(dlLoaiVatLieu), GetDrop(dlDonViTinh), decimal.Parse(txtDonGiaCoThue.Text) > 0 ? true : false,
            //        DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            //}

            if (s != "")
                GstGetMess(s, "");
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string filename = "";
            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        Guid ID = Guid.NewGuid();
                        if (FileUpload1.HasFile)
                            UploadFile(1, ID.ToString(), ref filename);

                        var CongVanDen = new tblCongVanDen()
                        {
                            ID = ID,
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhaCungCap = GetDrop(dlNhaCungCap),
                            TenCongVan = txtTenCongVan.Text.Trim(),
                            NoiDung = txtNoiDung.Text.Trim(),
                            NguoiNhan = txtNguoiNhan.Text.Trim(),
                            NgayNhan = txtNgayNhan.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayNhan.Text)),
                            NgayLamCongVan = txtNgayLamCongVan.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayLamCongVan.Text)),
                            FileUpload = filename,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",

                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblCongVanDens.InsertOnSubmit(CongVanDen);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        GstGetMess("Lưu thành công.", "");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblCongVanDens
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                            query.TenCongVan = txtTenCongVan.Text.Trim();
                            query.NoiDung = txtNoiDung.Text.Trim();
                            query.NguoiNhan = txtNguoiNhan.Text.Trim();
                            query.NgayNhan = txtNgayNhan.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayNhan.Text));
                            query.NgayLamCongVan = txtNgayLamCongVan.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtNgayLamCongVan.Text));
                            if (FileUpload1.HasFile)
                            {
                                UploadFile(1, hdID.Value, ref filename);
                                query.FileUpload = filename;
                            }
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            GstGetMess("Sửa thành công", "");
                        }
                        else
                        {
                            Warning("Thông tin công văn đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        void UploadFile(int loai, string ID, ref string filename)
        {
            if (FileUpload1.HasFile)
            {
                if (loai == 1)
                {
                    string extision = FileUpload1.FileName.Trim();
                    string time = DateTime.Now.ToString("yyyyMMddHHmmss");
                    filename = ID.ToUpper() + "-" + time + extision.Substring(extision.LastIndexOf('.'));
                    FileUpload1.SaveAs(Server.MapPath("~/ImageCongVanDen/") + filename);
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlNhaCungCap.SelectedValue = "";
            txtTenCongVan.Text = "";
            txtNoiDung.Text = "";
            txtNguoiNhan.Text = "";
            txtNgayNhan.Text = "";
            txtNgayLamCongVan.Text = "";
            hdID.Value = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblCongVanDens
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        txtTenCongVan.Text = query.TenCongVan;
                        txtNoiDung.Text = query.NoiDung;
                        txtNguoiNhan.Text = query.NguoiNhan;
                        txtNgayNhan.Text = query.NgayNhan.Value == null ? "" : query.NgayNhan.Value.ToString("dd/MM/yyyy");
                        txtNgayLamCongVan.Text = query.NgayLamCongVan.Value == null ? "" : query.NgayLamCongVan.Value.ToString("dd/MM/yyyy");
                        hdID.Value = id;
                        //btnSave.Text = "Cập nhật";
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai != 3)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                        }
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = db.sp_CongVanDen_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
                else if (e.CommandName == "HinhAnh")
                {
                    imgHinhAnh.ImageUrl = "~/ImageCongVanDen/" + query.FileUpload;
                    mpHinhAnh.Show();
                }
            }
            else
            {
                Warning("Thông tin công văn đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = db.sp_CongVanDen_Search(GetDrop(dlChiNhanhSearch), dlNhaCungCapSearch.SelectedValue,
            DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text)), 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCapSearch();
        }
    }
}