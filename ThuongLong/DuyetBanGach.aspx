﻿<%@ Page Title="Duyệt bán gạch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetBanGach.aspx.cs" Inherits="ThuongLong.DuyetBanGach" %>

<%@ Register Src="UCBanGach/uc_DuyetGiaBanGach.ascx" TagName="uc_DuyetGiaBanGach" TagPrefix="uc1" %>
<%@ Register Src="UCBanGach/uc_DuyetBanGach.ascx" TagName="uc_DuyetBanGach" TagPrefix="uc2" %>
<%@ Register Src="UCBanGach/uc_DuyetChotBanGach.ascx" TagName="uc_DuyetChotBanGach" TagPrefix="uc3" %>
<%@ Register Src="UCBanGach/uc_DuyetLichBanGach.ascx" TagName="uc_DuyetLichBanGach" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmGiaBanGach" runat="server" HeaderText="Hợp đồng bán gạch">
                            <ContentTemplate>
                                <uc1:uc_DuyetGiaBanGach ID="uc_DuyetGiaBanGach1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmLichBanGach" runat="server" HeaderText="Lịch bán gạch">
                            <ContentTemplate>
                                <uc4:uc_DuyetLichBanGach ID="uc_DuyetLichBanGach1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmBanGach" runat="server" HeaderText="Phiếu bán gạch">
                            <ContentTemplate>
                                <uc2:uc_DuyetBanGach ID="uc_DuyetBanGach1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmChotBanGach" runat="server" HeaderText="Chốt bán gạch">
                            <ContentTemplate>
                                <uc3:uc_DuyetChotBanGach ID="uc_DuyetChotBanGach1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
