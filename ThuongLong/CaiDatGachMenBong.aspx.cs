﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class CaiDatGachMenBong : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmDinhMucGach";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmCaiDatGachMenBong", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadLoaiVatLieu();
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadLoaiVatLieu()
        {
            var qlLoaiSanPham = (from p in db.tblLoaiVatLieus
                                 where p.TrangThai == 2
                                 && p.IDNhomVatLieu == new Guid("C2B47A53-3F0A-4E9F-8172-3C3809034BCA")
                                 select new
                                 {
                                     p.ID,
                                     Ten = p.TenLoaiVatLieu
                                 }).OrderBy(p => p.Ten);

            dlLoaiSanPham.DataSource = qlLoaiSanPham;
            dlLoaiSanPham.DataBind();
            dlLoaiSanPham.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlCatSongDa1 = (from p in db.tblLoaiVatLieus
                                where p.TrangThai == 2
                                && p.IDNhomVatLieu == new Guid("8B27D124-A687-400E-89F6-E3114A08CF87")
                                select new
                                {
                                    p.ID,
                                    Ten = p.TenLoaiVatLieu
                                }).OrderBy(p => p.Ten);

            dlCatSongDa1.DataSource = qlCatSongDa1;
            dlCatSongDa1.DataBind();
            dlCatSongDa1.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlBotMau = (from p in db.tblLoaiVatLieus
                            where p.TrangThai == 2
                     && p.IDNhomVatLieu == new Guid("6D66F6C2-E6E9-4116-9BAF-1C69CE0F845B")
                            select new
                            {
                                p.ID,
                                Ten = p.TenLoaiVatLieu
                            }).OrderBy(p => p.Ten);

            dlBotMau.DataSource = qlBotMau;
            dlBotMau.DataBind();
            dlBotMau.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlKeoBong = (from p in db.tblLoaiVatLieus
                             where p.TrangThai == 2
                      && p.IDNhomVatLieu == new Guid("4B8BE577-EABD-468C-95A6-C4EC4E805856")
                             select new
                             {
                                 p.ID,
                                 Ten = p.TenLoaiVatLieu
                             }).OrderBy(p => p.Ten);

            dlKeoBong.DataSource = qlKeoBong;
            dlKeoBong.DataBind();
            dlKeoBong.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlXiMangPCB401 = (from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                           && p.IDNhomVatLieu == new Guid("36FCDA11-FEF0-43C5-8AB8-C55F24281FF4")
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu
                                  }).OrderBy(p => p.Ten);

            dlXiMangPCB401.DataSource = qlXiMangPCB401;
            dlXiMangPCB401.DataBind();
            dlXiMangPCB401.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlCatSongDa2 = (from p in db.tblLoaiVatLieus
                                where p.TrangThai == 2
                         && p.IDNhomVatLieu == new Guid("8B27D124-A687-400E-89F6-E3114A08CF87")
                                select new
                                {
                                    p.ID,
                                    Ten = p.TenLoaiVatLieu
                                }).OrderBy(p => p.Ten);

            dlCatSongDa2.DataSource = qlCatSongDa2;
            dlCatSongDa2.DataBind();
            dlCatSongDa2.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlXiMangPCB402 = (from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                           && p.IDNhomVatLieu == new Guid("36FCDA11-FEF0-43C5-8AB8-C55F24281FF4")
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu
                                  }).OrderBy(p => p.Ten);

            dlXiMangPCB402.DataSource = qlXiMangPCB402;
            dlXiMangPCB402.DataBind();
            dlXiMangPCB402.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlDaMat = (from p in db.tblLoaiVatLieus
                           where p.TrangThai == 2
                    && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                           select new
                           {
                               p.ID,
                               Ten = p.TenLoaiVatLieu
                           }).OrderBy(p => p.Ten);

            dlDaMat.DataSource = qlDaMat;
            dlDaMat.DataBind();
            dlDaMat.Items.Insert(0, new ListItem("--Chọn--", ""));

            //dlCatSongDa1.Items.Insert(0, new ListItem("Cát sông đà", "51C056B4-DB1F-4024-817F-6E08F9EBF1F0"));
            //dlBotMau.Items.Insert(0, new ListItem("Bột màu", "7390D325-C47F-405D-84E5-B8E1AC793461"));
            //dlKeoBong.Items.Insert(0, new ListItem("Keo bóng", "2786A269-23CD-4052-A96F-A4D29A47D713"));
            //dlXiMangPCB401.Items.Insert(0, new ListItem("Xi măng PCB 40", "840482F9-D5F6-4926-B6F1-77657A7A95D6"));
            //dlCatSongDa2.Items.Insert(0, new ListItem("Cát sông đà", "51C056B4-DB1F-4024-817F-6E08F9EBF1F0"));
            //dlXiMangPCB402.Items.Insert(0, new ListItem("Xi măng PCB 40", "840482F9-D5F6-4926-B6F1-77657A7A95D6"));
            //dlDaMat.Items.Insert(0, new ListItem("Mạt đá", "7C716F3F-6262-4621-8452-9F8F43D02411"));
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlLoaiSanPham.SelectedValue == "")
            {
                s += " - Chọn loại sản phẩm<br />";
            }

            if (dlCatSongDa1.SelectedValue == "")
            {
                s += " - Chọn cát sông đà lớp mặt<br />";
            }
            if (dlBotMau.SelectedValue == "")
            {
                s += " - Chọn bột màu<br />";
            }
            if (dlKeoBong.SelectedValue == "")
            {
                s += " - Chọn keo bóng<br />";
            }
            if (dlXiMangPCB401.SelectedValue == "")
            {
                s += " - Chọn xi măng PCB40 lớp mặt<br />";
            }
            if (dlCatSongDa2.SelectedValue == "")
            {
                s += " - Chọn cát sông đà lớp thân<br />";
            }
            if (dlXiMangPCB402.SelectedValue == "")
            {
                s += " - Chọn xi măng PCB40 lớp thân<br />";
            }
            if (dlDaMat.SelectedValue == "")
            {
                s += " - Chọn đá mạt<br />";
            }

            if (txtCatSongDa1.Text == "")
            {
                s += " - Nhập định mức cát sông đà lớp mặt<br />";
            }
            if (txtBotMau.Text == "")
            {
                s += " - Nhập định mức bột màu<br />";
            }
            if (txtKeoBong.Text == "")
            {
                s += " - Nhập định mức keo bóng<br />";
            }
            if (txtXiMangPCB401.Text == "")
            {
                s += " - Nhập định mức xi măng PCB40 lớp mặt<br />";
            }
            if (txtCatSongDa2.Text == "")
            {
                s += " - Nhập định mức cát sông đà lớp thân<br />";
            }
            if (txtXiMangPCB402.Text == "")
            {
                s += " - Nhập định mức xi măng PCB40 lớp thân<br />";
            }
            if (txtDaMat.Text == "")
            {
                s += " - Nhập định mức đá mạt<br />";
            }
            if (hdID.Value == "")
            {
                var query = (from p in db.tblCaiDatGachMenBongs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    s += "Đã thiết lập định mức cho loại gạch này";
                }
            }
            else
            {
                var query = (from p in db.tblCaiDatGachMenBongs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             && p.ID != new Guid(hdID.Value)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    s += "Đã thiết lập định mức cho loại gạch này";
                }
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    if (CheckThem() == "")
                    {
                        var CaiDatGachMenBong = new tblCaiDatGachMenBong()
                        {
                            ID = Guid.NewGuid(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            ThoiTiet = int.Parse(dlThoiTiet.SelectedValue),
                            IDLoaiVatLieu = GetDrop(dlLoaiSanPham),
                            CatSongDa1 = GetDrop(dlCatSongDa1),
                            BotMau = GetDrop(dlBotMau),
                            KeoBong = GetDrop(dlKeoBong),
                            XiMangPCB401 = GetDrop(dlXiMangPCB401),
                            CatSongDa2 = GetDrop(dlCatSongDa2),
                            XiMangPCB402 = GetDrop(dlXiMangPCB402),
                            DaMat = GetDrop(dlDaMat),

                            DMCatSongDa1 = double.Parse(txtCatSongDa1.Text.Trim()),
                            DMBotMau = double.Parse(txtBotMau.Text.Trim()),
                            DMKeoBong = double.Parse(txtKeoBong.Text.Trim()),
                            DMXiMangPCB401 = double.Parse(txtXiMangPCB401.Text.Trim()),
                            DMCatSongDa2 = double.Parse(txtCatSongDa2.Text.Trim()),
                            DMXiMangPCB402 = double.Parse(txtXiMangPCB402.Text.Trim()),
                            DMDaMat = double.Parse(txtDaMat.Text.Trim()),

                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblCaiDatGachMenBongs.InsertOnSubmit(CaiDatGachMenBong);
                        db.SubmitChanges();
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                var query = (from p in db.tblCaiDatGachMenBongs
                             where p.ID == new Guid(hdID.Value)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckThem() == "")
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.ThoiTiet = int.Parse(dlThoiTiet.SelectedValue);
                            query.IDLoaiVatLieu = GetDrop(dlLoaiSanPham);
                            query.CatSongDa1 = GetDrop(dlCatSongDa1);
                            query.BotMau = GetDrop(dlBotMau);
                            query.KeoBong = GetDrop(dlKeoBong);
                            query.XiMangPCB401 = GetDrop(dlXiMangPCB401);
                            query.CatSongDa2 = GetDrop(dlCatSongDa2);
                            query.XiMangPCB402 = GetDrop(dlXiMangPCB402);
                            query.DaMat = GetDrop(dlDaMat);

                            query.DMCatSongDa1 = double.Parse(txtCatSongDa1.Text.Trim());
                            query.DMBotMau = double.Parse(txtBotMau.Text.Trim());
                            query.DMKeoBong = double.Parse(txtKeoBong.Text.Trim());
                            query.DMXiMangPCB401 = double.Parse(txtXiMangPCB401.Text.Trim());
                            query.DMCatSongDa2 = double.Parse(txtCatSongDa2.Text.Trim());
                            query.DMXiMangPCB402 = double.Parse(txtXiMangPCB402.Text.Trim());
                            query.DMDaMat = double.Parse(txtDaMat.Text.Trim());

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";

                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            //lblTaoMoiHopDong_Click(sender, e);

                            Success("Sửa thành công");
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlCatSongDa1.SelectedValue = "";
            dlBotMau.SelectedValue = "";
            dlKeoBong.SelectedValue = "";
            dlXiMangPCB401.SelectedValue = "";
            dlCatSongDa2.SelectedValue = "";
            dlXiMangPCB402.SelectedValue = "";
            dlDaMat.SelectedValue = "";

            txtCatSongDa1.Text = "";
            txtBotMau.Text = "";
            txtKeoBong.Text = "";
            txtXiMangPCB401.Text = "";
            txtCatSongDa2.Text = "";
            txtXiMangPCB402.Text = "";
            txtDaMat.Text = "";
            hdID.Value = "";

        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlChiNhanh.SelectedValue != "" && dlLoaiSanPham.SelectedValue != "")
            {
                var query = (from p in db.tblCaiDatGachMenBongs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    dlCatSongDa1.SelectedValue = query.CatSongDa1.ToString();
                    dlBotMau.SelectedValue = query.BotMau.ToString();
                    dlKeoBong.SelectedValue = query.KeoBong.ToString();
                    dlXiMangPCB401.SelectedValue = query.XiMangPCB401.ToString();
                    dlCatSongDa2.SelectedValue = query.CatSongDa2.ToString();
                    dlXiMangPCB402.SelectedValue = query.XiMangPCB402.ToString();
                    dlDaMat.SelectedValue = query.DaMat.ToString();

                    txtCatSongDa1.Text = query.DMCatSongDa1.ToString();
                    txtBotMau.Text = query.DMBotMau.ToString();
                    txtKeoBong.Text = query.DMKeoBong.ToString();
                    txtXiMangPCB401.Text = query.DMXiMangPCB401.ToString();
                    txtCatSongDa2.Text = query.DMCatSongDa2.ToString();
                    txtXiMangPCB402.Text = query.DMXiMangPCB402.ToString();
                    txtDaMat.Text = query.DMDaMat.ToString();
                }
                else
                    lblTaoMoiHopDong_Click(sender, e);
            }
            else
                lblTaoMoiHopDong_Click(sender, e);
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblCaiDatGachMenBongs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlThoiTiet.SelectedValue = query.ThoiTiet.ToString();
                        dlLoaiSanPham.SelectedValue = query.IDLoaiVatLieu.ToString();
                        dlCatSongDa1.SelectedValue = query.CatSongDa1.ToString();
                        dlBotMau.SelectedValue = query.BotMau.ToString();
                        dlKeoBong.SelectedValue = query.KeoBong.ToString();
                        dlXiMangPCB401.SelectedValue = query.XiMangPCB401.ToString();
                        dlCatSongDa2.SelectedValue = query.CatSongDa2.ToString();
                        dlXiMangPCB402.SelectedValue = query.XiMangPCB402.ToString();
                        dlDaMat.SelectedValue = query.DaMat.ToString();
                        txtCatSongDa1.Text = query.DMCatSongDa1.ToString();
                        txtBotMau.Text = query.DMBotMau.ToString();
                        txtKeoBong.Text = query.DMKeoBong.ToString();
                        txtXiMangPCB401.Text = query.DMXiMangPCB401.ToString();
                        txtCatSongDa2.Text = query.DMCatSongDa2.ToString();
                        txtXiMangPCB402.Text = query.DMXiMangPCB402.ToString();
                        txtDaMat.Text = query.DMDaMat.ToString();

                        hdID.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai == 1)
                        {
                            int countxoa = (from p in db.tblCaiDatGachMenBong_Logs
                                            where p.IDChung == query.ID
                                            select p).Count();
                            if (countxoa == 1)
                            {
                                db.tblCaiDatGachMenBongs.DeleteOnSubmit(query);
                                db.SubmitChanges();
                                Success("Đã xóa");
                            }
                            else
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                        }
                        else if (query.TrangThai == 2)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                            Success("Xóa thành công");
                        }
                        else
                        {
                            Warning("Dữ liệu đang chờ duyệt xóa");
                        }
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_CaiDatGachMenBong_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin định mức đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = vatlieu.sp_CaiDatGachMenBong_Search(GetDrop(dlChiNhanhSearch), dlLoaiVatLieuSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlLoaiVatLieuSearch), 20, page);

            GV.DataSource = query;
            GV.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiVatLieuSearch();
        }
        void LoadLoaiVatLieuSearch()
        {
            dlLoaiVatLieuSearch.Items.Clear();
            var query = (from p in db.tblCaiDatGachMenBongs
                         join q in db.tblLoaiVatLieus on p.IDLoaiVatLieu equals q.ID
                         where p.IDChiNhanh == GetDrop(dlChiNhanhSearch)
                         select new
                         {
                             ID = p.IDLoaiVatLieu,
                             Ten = q.TenLoaiVatLieu
                         }).Distinct().OrderBy(p => p.Ten);
            dlLoaiVatLieuSearch.DataSource = query;
            dlLoaiVatLieuSearch.DataBind();
            dlLoaiVatLieuSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }

        protected void dlThoiTiet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlLoaiSanPham.SelectedValue != "" && dlChiNhanh.SelectedValue != "")
            {
                dlChiNhanhSearch_SelectedIndexChanged(sender, e);
            }
        }
    }
}