﻿<%@ Page Title="Duyệt nhân sự" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetQuanLyNhanSu.aspx.cs" Inherits="ThuongLong.DuyetQuanLyNhanSu" %>

<%@ Register Src="DuyetNhanSu/uc_DuyetNhanSu.ascx" TagName="uc_DuyetNhanSu" TagPrefix="uc1" %>
<%@ Register Src="DuyetNhanSu/uc_DuyetBoPhanChucVu.ascx" TagName="uc_DuyetBoPhanChucVu" TagPrefix="uc2" %>
<%@ Register Src="DuyetNhanSu/uc_DuyetLuongNhanVien.ascx" TagName="uc_DuyetLuongNhanVien" TagPrefix="uc3" %>
<%--<%@ Register Src="DuyetNhanSu/uc_DuyetNhanSu.ascx" TagName="uc_DuyetNhanSu" TagPrefix="uc4" %>
<%@ Register Src="DuyetNhanSu/uc_DuyetNhanSu.ascx" TagName="uc_DuyetNhanSu" TagPrefix="uc5" %>

<%@ Register Src="DuyetNhanSu/uc_DuyetNhanSu.ascx" TagName="uc_DuyetNhanSu" TagPrefix="uc6" %>
<%@ Register Src="DuyetNhanSu/uc_DuyetNhanSu.ascx" TagName="uc_DuyetNhanSu" TagPrefix="uc7" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmNhanSu" runat="server" HeaderText="Nhân sự">
                            <ContentTemplate>
                                <uc1:uc_DuyetNhanSu ID="uc_DuyetNhanSu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmLoaiThietBi" runat="server" HeaderText="Bộ phận chức vụ">
                            <ContentTemplate>
                                <uc2:uc_DuyetBoPhanChucVu ID="uc_DuyetBoPhanChucVu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="frmLuongNhanvien" runat="server" HeaderText="Lương nhân viên">
                            <ContentTemplate>
                                <uc3:uc_DuyetLuongNhanVien ID="uc_DuyetLuongNhanVien" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxToolkit:TabPanel ID="frmLoaiVatLieu" runat="server" HeaderText="Loại vật liệu">
                            <ContentTemplate>
                                <uc4:uc_DuyetNhanSu ID="uc_DuyetNhanSu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmDonViTinh" runat="server" HeaderText="Đơn vị tính">
                            <ContentTemplate>
                                <uc5:uc_DuyetNhanSu ID="uc_DuyetNhanSu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmTrinhDo" runat="server" HeaderText="Trình độ">
                            <ContentTemplate>
                                <uc6:uc_DuyetNhanSu ID="uc_DuyetNhanSu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmChuyenMon" runat="server" HeaderText="Chuyên môn">
                            <ContentTemplate>
                                <uc7:uc_DuyetNhanSu ID="uc_DuyetNhanSu" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>--%>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
