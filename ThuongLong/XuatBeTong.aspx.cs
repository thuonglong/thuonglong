﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class XuatBeTong : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        BeTongDataContext betong = new BeTongDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmXuatBeTong";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmXuatBeTong", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        LoadChiNhanh();
                        LoadXe();
                        //LoadDanhMuc();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        txtTyLeQuyDoi.Text = "2300";
                        dlChiNhanhSearch_SelectedIndexChanged(sender, e);
                    }
                }
            }
        }

        void LoadXe()
        {
            dlXeTron.Items.Clear();
            dlXeTron.DataSource = from p in db.tblXeVanChuyens
                                  where p.TrangThai == 2
                                  && p.IsTrangThai == 1
                                  && p.IDNhomThietBi == new Guid("3EB17A86-BCA4-4852-B1F4-01B1CABE106D")
                                  orderby p.STT
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenThietBi,
                                  };
            dlXeTron.DataBind();
            dlXeTron.Items.Insert(0, new ListItem("--Chọn --", ""));

            dlXeBom.Items.Clear();
            dlXeBom.DataSource = from p in db.tblXeVanChuyens
                                 where p.TrangThai == 2
                                 && p.IsTrangThai == 1
                                 && p.IDNhomThietBi == new Guid("0F47524B-BB6F-4ED6-898E-77528924024E")
                                 orderby p.STT
                                 select new
                                 {
                                     p.ID,
                                     Ten = p.TenThietBi,
                                 };
            dlXeBom.DataBind();
            dlXeBom.Items.Insert(0, new ListItem("--Chọn --", ""));
        }
        //void LoadDanhMuc()
        //{
        //    dlXiMang.Items.Clear();
        //    dlXiMang.DataSource = from p in db.tblLoaiVatLieus
        //                          where p.TrangThai == 2
        //                          && p.IDNhomVatLieu == new Guid("36FCDA11-FEF0-43C5-8AB8-C55F24281FF4")
        //                          orderby p.STT
        //                          select new
        //                          {
        //                              p.ID,
        //                              Ten = p.TenLoaiVatLieu,
        //                          };
        //    dlXiMang.DataBind();
        //    dlXiMang.Items.Insert(0, new ListItem("--Chọn --", ""));

        //    dlCat.Items.Clear();
        //    dlCat.DataSource = from p in db.tblLoaiVatLieus
        //                       where p.TrangThai == 2
        //                       && p.IDNhomVatLieu == new Guid("8B27D124-A687-400E-89F6-E3114A08CF87")
        //                       orderby p.STT
        //                       select new
        //                       {
        //                           p.ID,
        //                           Ten = p.TenLoaiVatLieu,
        //                       };
        //    dlCat.DataBind();
        //    dlCat.Items.Insert(0, new ListItem("--Chọn --", ""));

        //    dlDa.Items.Clear();
        //    dlDa.DataSource = from p in db.tblLoaiVatLieus
        //                      where p.TrangThai == 2
        //                      && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
        //                      orderby p.STT
        //                      select new
        //                      {
        //                          p.ID,
        //                          Ten = p.TenLoaiVatLieu,
        //                      };
        //    dlDa.DataBind();
        //    dlDa.Items.Insert(0, new ListItem("--Chọn --", ""));

        //    dlPhuGia.Items.Clear();
        //    dlPhuGia.DataSource = from p in db.tblLoaiVatLieus
        //                          where p.TrangThai == 2
        //                          && p.IDNhomVatLieu == new Guid("6C435EB7-394E-408D-809F-54DE7F3245B4")
        //                          orderby p.STT
        //                          select new
        //                          {
        //                              p.ID,
        //                              Ten = p.TenLoaiVatLieu,
        //                          };
        //    dlPhuGia.DataBind();
        //    dlPhuGia.Items.Insert(0, new ListItem("--Chọn --", ""));

        //    dlTroBay.Items.Clear();
        //    dlTroBay.DataSource = from p in db.tblLoaiVatLieus
        //                          where p.TrangThai == 2
        //                          && p.IDNhomVatLieu == new Guid("F97D4631-E74D-4272-A9A1-34189C409AF3")
        //                          orderby p.STT
        //                          select new
        //                          {
        //                              p.ID,
        //                              Ten = p.TenLoaiVatLieu,
        //                          };
        //    dlTroBay.DataBind();
        //    dlTroBay.Items.Insert(0, new ListItem("--Chọn --", ""));
        //}

        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
            dlChiNhanhSearch.Items.Insert(0, new ListItem("Tất cả chi nhánh", ""));
            dlChiNhanhChot.DataSource = query;
            dlChiNhanhChot.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem(ref string IDHopDong, ref string IDHopDongBom, ref string IDLich, ref Guid? loaiximang, ref Guid? loaida, ref Guid? loaicat, ref Guid? loaiphugia, ref Guid? loaitrobay, ref double? klximang, ref double? klda, ref double? klcat, ref double? klphugia, ref double? kltrobay)
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn khách hàng<br />";
            }
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            if (txtKLThucXuat.Text == "" || double.Parse(txtKLThucXuat.Text) == 0)
            {
                s += " - Nhập khối lượng thực xuất<br />";
            }
            if (txtKLKhachHang.Text == "" || double.Parse(txtKLKhachHang.Text) == 0)
            {
                s += " - Nhập khối lượng khách hàng<br />";
            }
            if (txtKLHoaDon.Text == "")
            {
                s += " - Nhập khối lượng hóa đơn<br />";
            }
            if (s == "")
            {
                betong.sp_XuatBeTong_CheckThem(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlMacBeTong),
                    GetDrop(dlHinhThucBom), ref loaiximang, ref loaicat, ref loaida, ref loaiphugia, ref loaitrobay, ref IDHopDong, ref IDHopDongBom, ref IDLich, ref klximang, ref klda, ref klcat, ref klphugia, ref kltrobay, ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua(ref string IDHopDong, ref string IDHopDongBom, ref string IDLich, ref Guid? loaiximang, ref Guid? loaida, ref Guid? loaicat, ref Guid? loaiphugia, ref Guid? loaitrobay, ref double? klximang, ref double? klda, ref double? klcat, ref double? klphugia, ref double? kltrobay)
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            if (txtKLThucXuat.Text == "" || double.Parse(txtKLThucXuat.Text) == 0)
            {
                s += " - Nhập khối lượng thực xuất<br />";
            }
            if (txtKLKhachHang.Text == "" || double.Parse(txtKLKhachHang.Text) == 0)
            {
                s += " - Nhập khối lượng khách hàng<br />";
            }
            if (txtKLHoaDon.Text == "")
            {
                s += " - Nhập khối lượng hóa đơn<br />";
            }
            if (s == "")
            {
                betong.sp_XuatBeTong_CheckSua(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlMacBeTong),
                    GetDrop(dlHinhThucBom), ref loaiximang, ref loaicat, ref loaida, ref loaiphugia, ref loaitrobay, ref IDHopDong, ref IDHopDongBom, ref IDLich, ref klximang, ref klda, ref klcat, ref klphugia, ref kltrobay, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string idhopdong = "";
            string idhopdongbom = "";
            string idlich = "";
            double? klximang = 0;
            double? klda = 0;
            double? klcat = 0;
            double? klphugia = 0;
            double? kltrobay = 0;

            Guid? loaiximang = (Guid?)null;
            Guid? loaida = (Guid?)null;
            Guid? loaicat = (Guid?)null;
            Guid? loaiphugia = (Guid?)null;
            Guid? loaitrobay = (Guid?)null;

            if (hdID.Value == "")
            {
                if (CheckThem(ref idhopdong, ref idhopdongbom, ref idlich, ref loaiximang, ref loaida, ref loaicat, ref loaiphugia, ref loaitrobay,
                    ref klximang, ref klda, ref klcat, ref klphugia, ref kltrobay) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var xuatBeTong = new tblXuatBeTong();

                        xuatBeTong.ID = Guid.NewGuid();
                        xuatBeTong.GioXuat = TimeSpan.Parse(txtGioXuat.Text);
                        xuatBeTong.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                        xuatBeTong.IDChiNhanh = GetDrop(dlChiNhanh);
                        xuatBeTong.IDNhaCungCap = GetDrop(dlNhaCungCap);
                        xuatBeTong.IDCongTrinh = GetDrop(dlCongTrinh);
                        xuatBeTong.HangMuc = dlHangMuc.SelectedItem.Text;
                        xuatBeTong.MacBeTong = GetDrop(dlMacBeTong);
                        xuatBeTong.HinhThucBom = GetDrop(dlHinhThucBom);
                        xuatBeTong.IDHopDong = new Guid(idhopdong);
                        if (idhopdongbom != null && idhopdongbom != "")
                            xuatBeTong.IDHopDongBom = new Guid(idhopdongbom);
                        xuatBeTong.IDLich = new Guid(idlich);
                        xuatBeTong.IDNVKD = dlNhanVienKinhDoanh.SelectedValue == "" ? (Guid?)null : GetDrop(dlNhanVienKinhDoanh);
                        xuatBeTong.KLThucXuat = double.Parse(txtKLThucXuat.Text);
                        xuatBeTong.KLKhachHang = double.Parse(txtKLKhachHang.Text);
                        xuatBeTong.KLHoaDon = txtKLHoaDon.Text == "" ? 0 : double.Parse(txtKLHoaDon.Text);
                        xuatBeTong.KLQuaCan = txtKLQuaCan.Text == "" ? 0 : double.Parse(txtKLQuaCan.Text);

                        xuatBeTong.TLTong = txtTLTong.Text == "" ? 0 : double.Parse(txtTLTong.Text);
                        xuatBeTong.TLBi = txtTLBi.Text == "" ? 0 : double.Parse(txtTLBi.Text);
                        xuatBeTong.TLHang = txtTLHang.Text == "" ? 0 : double.Parse(txtTLHang.Text);
                        xuatBeTong.TyLeQuyDoi = txtTyLeQuyDoi.Text == "" ? 0 : double.Parse(txtTyLeQuyDoi.Text);
                        xuatBeTong.IDPhieuCan = txtSoPhieuCan.Text.Trim();

                        xuatBeTong.XiMang = loaiximang;
                        xuatBeTong.Cat = loaida;
                        xuatBeTong.Da = loaicat;
                        xuatBeTong.PhuGia = loaiphugia;
                        xuatBeTong.TroBay = loaitrobay;

                        xuatBeTong.IDXeTron = dlXeTron.SelectedValue == "" ? (Guid?)null : GetDrop(dlXeTron);
                        xuatBeTong.IDXeBom = dlXeBom.SelectedValue == "" ? (Guid?)null : GetDrop(dlXeBom);

                        xuatBeTong.TenXeTron = dlXeTron.SelectedValue == "" ? "" : dlXeTron.SelectedItem.Text.ToString();
                        xuatBeTong.TenXeBom = dlXeBom.SelectedValue == "" ? "" : dlXeBom.SelectedItem.Text.ToString();

                        xuatBeTong.DonGiaHoaDon = decimal.Parse(txtDonGiaHoaDon.Text);
                        xuatBeTong.DonGiaThanhToan = decimal.Parse(txtDonGiaThanhToan.Text);
                        xuatBeTong.TienHoaDon = decimal.Parse(txtTienHoaDon.Text);
                        xuatBeTong.TienThanhToan = decimal.Parse(txtTienThanhToan.Text);
                        xuatBeTong.DonGiaHoaDonBom = decimal.Parse(txtDonGiaHoaDonBom.Text);
                        xuatBeTong.DonGiaThanhToanBom = decimal.Parse(txtDonGiaThanhToanBom.Text);
                        xuatBeTong.TienHoaDonBom = decimal.Parse(txtTienHoaDonBom.Text);
                        xuatBeTong.TienThanhToanBom = decimal.Parse(txtTienThanhToanBom.Text);
                        xuatBeTong.LoaiBom = hdLoaiBom.Value == "" ? 10 : int.Parse(hdLoaiBom.Value);

                        xuatBeTong.KLXiMang = txtKLXiMang.Text == "" ? 0 : double.Parse(txtKLXiMang.Text) * klximang.Value;
                        xuatBeTong.KLDa = txtKLDa.Text == "" ? 0 : double.Parse(txtKLDa.Text) * klda.Value;
                        xuatBeTong.KLCat = txtKLCat.Text == "" ? 0 : double.Parse(txtKLCat.Text) * klcat.Value;
                        xuatBeTong.KLPhuGia = txtKLPhuGia.Text == "" ? 0 : double.Parse(txtKLPhuGia.Text) * klphugia.Value;
                        xuatBeTong.KLTroBay = txtKLTroBay.Text == "" ? 0 : double.Parse(txtKLTroBay.Text) * kltrobay.Value;
                        xuatBeTong.PhuPhi = txtPhuPhi.Text == "" ? 0 : decimal.Parse(txtPhuPhi.Text);

                        xuatBeTong.DMXiMang = klximang.Value;
                        xuatBeTong.DMDa = klda.Value;
                        xuatBeTong.DMCat = klcat.Value;
                        xuatBeTong.DMPhuGia = klphugia.Value;
                        xuatBeTong.DMTroBay = kltrobay.Value;

                        xuatBeTong.NguoiTruc = txtNguoiTruc.Text.Trim();
                        xuatBeTong.KyThuat = txtKyThuat.Text.Trim();
                        xuatBeTong.NguoiGioiThieu = txtNguoiGioiThieu.Text.Trim();
                        xuatBeTong.DaThuTien = "Chưa thu tiền";

                        xuatBeTong.TrangThai = 1;
                        xuatBeTong.TrangThaiText = "Chờ duyệt";

                        xuatBeTong.NguoiTao = Session["IDND"].ToString();
                        xuatBeTong.NgayTao = DateTime.Now;

                        db.tblXuatBeTongs.InsertOnSubmit(xuatBeTong);
                        db.SubmitChanges();

                        ////luu nhat trinh
                        //if (dlXeBom.SelectedValue != "")
                        //{
                        //    var query = (from p in db.tblDinhMucNhienLieus
                        //                 where p.IDXe == GetDrop(dlXeBom)
                        //                 && p.TrangThai == 2
                        //                 select p).FirstOrDefault();
                        //    string txtDMDiChuyen = "";
                        //    string txtDMBomBeTong = "";
                        //    string txtDMVeSinhRaChan = "";
                        //    if (query != null && query.ID != null)
                        //    {
                        //        txtDMDiChuyen = query.DiChuyen.ToString();
                        //        txtDMBomBeTong = query.BomBeTong.ToString();
                        //        txtDMVeSinhRaChan = query.VeSinhRaChan.ToString();
                        //    }

                        //    var queryLich = (from p in db.tblLichXuatBeTongs
                        //                     where p.ID == new Guid(idlich)
                        //                     && p.TrangThai == 2
                        //                     select p).FirstOrDefault();
                        //    string txtDiChuyen = "";
                        //    if (queryLich != null && queryLich.ID != null)
                        //    {
                        //        txtDiChuyen = queryLich.CuLyVanChuyen.ToString();
                        //    }
                        //    var NhatTrinhXe = new tblNhatTrinhXe()
                        //    {
                        //        ID = Guid.NewGuid(),
                        //        NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                        //        IDNhomThietBi = new Guid("3EB17A86-BCA4-4852-B1F4-01B1CABE106D"),
                        //        IDChiNhanh = GetDrop(dlChiNhanh),
                        //        IDXe = GetDrop(dlXeBom),
                        //        NoiDung = "Nhật trình sinh từ phiếu bán bê tông",
                        //        GioChay = TimeSpan.Parse(txtGioXuat.Text),
                        //        IDPhieuBan = xuatBeTong.ID,

                        //        DMDiChuyenCoTaiBen = 0,
                        //        DMDiChuyenKhongTaiBen = 0,
                        //        DMCauHaHang = 0,

                        //        DMDiChuyenCoTaiBeTong = 0,
                        //        DMDiChuyenKhongTaiBeTong = 0,
                        //        DMLayBeTongTaiTram = 0,
                        //        DMXaBeTongVeSinh = 0,
                        //        DMNoMayQuayThung = 0,

                        //        DMDiChuyen = txtDMBomBeTong == "" ? 0 :  double.Parse(txtDMBomBeTong),
                        //        DMBomBeTong = txtDMBomBeTong == "" ? 0 : double.Parse(txtDMBomBeTong),
                        //        DMVeSinhRaChan = txtDMVeSinhRaChan == "" ? 0 : double.Parse(txtDMVeSinhRaChan),

                        //        DiChuyenCoTaiBen = 0,
                        //        DiChuyenKhongTaiBen = 0,
                        //        CauHaHang = 0,

                        //        DiChuyenCoTaiBeTong = 0,
                        //        DiChuyenKhongTaiBeTong = 0,
                        //        LayBeTongTaiTram = 0,
                        //        XaBeTongVeSinh = 0,
                        //        NoMayQuayThung = 0,

                        //        DiChuyen = txtDiChuyen == "" ? 0 : double.Parse(txtDiChuyen),
                        //        BomBeTong = 1,
                        //        VeSinhRaChan = 1,
                        //        HeSoDiChuyen = 1,

                        //        DauDiChuyenCoTaiBen = 0,
                        //        DauDiChuyenKhongTaiBen = 0,
                        //        DauCauHaHang = 0,

                        //        DauDiChuyenCoTaiBeTong = 0,
                        //        DauDiChuyenKhongTaiBeTong = 0,
                        //        DauLayBeTongTaiTram = 0,
                        //        DauXaBeTongVeSinh = 0,
                        //        DauNoMayQuayThung = 0,

                        //        DauDiChuyen = txtDiChuyen == "" || txtDMDiChuyen  == "" ? 0 : double.Parse(txtDMDiChuyen) * double.Parse(txtDiChuyen),
                        //        DauBomBeTong = txtDMBomBeTong == "" ? 0 : double.Parse(txtDMBomBeTong) * 1,
                        //        DauVeSinhRaChan = txtDMVeSinhRaChan == "" ? 0 : double.Parse(txtDMVeSinhRaChan) * 1,

                        //        DuDauKy = 0,
                        //        DoDau = 0,
                        //        DuCuoiKy = 0,
                        //        KhoiLuong = 0,
                        //        ChuyenChay = 0,
                        //        BomCa = 0,
                        //        BomKhoi = 0,
                        //        Loai = "NhatTrinhXeBom",

                        //        TrangThai = 2,
                        //        TrangThaiText = "Đã duyệt",
                        //        NguoiTao = Session["IDND"].ToString(),
                        //        NgayTao = DateTime.Now
                        //    };
                        //    db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                        //    db.SubmitChanges();
                        //}


                        lblTaoMoiHopDong_Click(sender, e);
                        //Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua(ref idhopdong, ref idhopdongbom, ref idlich, ref loaiximang, ref loaida, ref loaicat, ref loaiphugia, ref loaitrobay,
                    ref klximang, ref klda, ref klcat, ref klphugia, ref kltrobay) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblXuatBeTongs
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.GioXuat = TimeSpan.Parse(txtGioXuat.Text);
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                            query.HangMuc = dlHangMuc.SelectedItem.Text;
                            query.IDCongTrinh = GetDrop(dlCongTrinh);
                            query.MacBeTong = GetDrop(dlMacBeTong);
                            query.HinhThucBom = GetDrop(dlHinhThucBom);
                            query.IDHopDong = new Guid(idhopdong);
                            if (idhopdongbom != null && idhopdongbom != "")
                                query.IDHopDongBom = idhopdongbom == "" ? (Guid?)null : new Guid(idhopdongbom);
                            query.IDLich = new Guid(idlich);
                            query.IDNVKD = dlNhanVienKinhDoanh.SelectedValue == "" ? (Guid?)null : GetDrop(dlNhanVienKinhDoanh);

                            query.KLThucXuat = double.Parse(txtKLThucXuat.Text);
                            query.KLKhachHang = double.Parse(txtKLKhachHang.Text);
                            query.KLHoaDon = txtKLHoaDon.Text == "" ? 0 : double.Parse(txtKLHoaDon.Text);
                            query.KLQuaCan = txtKLQuaCan.Text == "" ? 0 : double.Parse(txtKLQuaCan.Text);

                            query.TLTong = txtTLTong.Text == "" ? 0 : double.Parse(txtTLTong.Text);
                            query.TLBi = txtTLBi.Text == "" ? 0 : double.Parse(txtTLBi.Text);
                            query.TLHang = txtTLHang.Text == "" ? 0 : double.Parse(txtTLHang.Text);
                            query.TyLeQuyDoi = txtTyLeQuyDoi.Text == "" ? 0 : double.Parse(txtTyLeQuyDoi.Text);
                            query.IDPhieuCan = txtSoPhieuCan.Text.Trim();

                            query.XiMang = loaiximang;
                            query.Cat = loaida;
                            query.Da = loaicat;
                            query.PhuGia = loaiphugia;
                            query.TroBay = loaitrobay;

                            query.IDXeTron = dlXeTron.SelectedValue == "" ? (Guid?)null : GetDrop(dlXeTron);
                            query.IDXeBom = dlXeBom.SelectedValue == "" ? (Guid?)null : GetDrop(dlXeBom);

                            query.TenXeTron = dlXeTron.SelectedValue == "" ? "" : dlXeTron.SelectedItem.Text.ToString();
                            query.TenXeBom = dlXeBom.SelectedValue == "" ? "" : dlXeBom.SelectedItem.Text.ToString();

                            query.DonGiaHoaDon = decimal.Parse(txtDonGiaHoaDon.Text);
                            query.DonGiaThanhToan = decimal.Parse(txtDonGiaThanhToan.Text);
                            query.TienHoaDon = decimal.Parse(txtTienHoaDon.Text);
                            query.TienThanhToan = decimal.Parse(txtTienThanhToan.Text);
                            query.DonGiaHoaDonBom = decimal.Parse(txtDonGiaHoaDonBom.Text);
                            query.DonGiaThanhToanBom = decimal.Parse(txtDonGiaThanhToanBom.Text);
                            query.TienHoaDonBom = decimal.Parse(txtTienHoaDonBom.Text);
                            query.TienThanhToanBom = decimal.Parse(txtTienThanhToanBom.Text);
                            query.LoaiBom = hdLoaiBom.Value == "" ? 10 : int.Parse(hdLoaiBom.Value);

                            query.KLXiMang = txtKLXiMang.Text == "" ? 0 : double.Parse(txtKLXiMang.Text) * klximang.Value;
                            query.KLDa = txtKLDa.Text == "" ? 0 : double.Parse(txtKLDa.Text) * klda.Value;
                            query.KLCat = txtKLCat.Text == "" ? 0 : double.Parse(txtKLCat.Text) * klcat.Value;
                            query.KLPhuGia = txtKLPhuGia.Text == "" ? 0 : double.Parse(txtKLPhuGia.Text) * klphugia.Value;
                            query.KLTroBay = txtKLTroBay.Text == "" ? 0 : double.Parse(txtKLTroBay.Text) * kltrobay.Value;

                            query.PhuPhi = txtPhuPhi.Text == "" ? 0 : decimal.Parse(txtPhuPhi.Text);

                            query.DMXiMang = klximang.Value;
                            query.DMDa = klda.Value;
                            query.DMCat = klcat.Value;
                            query.DMPhuGia = klphugia.Value;
                            query.DMTroBay = kltrobay.Value;

                            query.NguoiTruc = txtNguoiTruc.Text.Trim();
                            query.KyThuat = txtKyThuat.Text.Trim();
                            query.NguoiGioiThieu = txtNguoiGioiThieu.Text.Trim();

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            //Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin phiếu xuất bê tông đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlXeBom.SelectedValue = "";
            dlXeTron.SelectedValue = "";
            dlChiNhanh_SelectedIndexChanged(sender, e);
            hdID.Value = "";
            hdLoaiBom.Value = "";

            txtDonGiaHoaDon.Text = "";
            txtDonGiaThanhToan.Text = "";
            txtTienHoaDon.Text = "";
            txtTienThanhToan.Text = "";

            txtDonGiaHoaDonBom.Text = "";
            txtDonGiaThanhToanBom.Text = "";
            txtTienHoaDonBom.Text = "";
            txtTienThanhToanBom.Text = "";

            txtKLKhachHang.Text = "";
            txtKLHoaDon.Text = "";
            txtKLThucXuat.Text = "";
            txtKLQuaCan.Text = "";

            txtTLTong.Text = "";
            txtTLBi.Text = "";
            txtTLHang.Text = "";
            txtTyLeQuyDoi.Text = "2300";
            txtSoPhieuCan.Text = "";

            //dlXiMang.SelectedValue = "";
            //dlCat.SelectedValue = "";
            //dlDa.SelectedValue = "";
            //dlPhuGia.SelectedValue = "";
            //dlTroBay.SelectedValue = "";

            txtKLXiMang.Text = "";
            txtKLDa.Text = "";
            txtKLCat.Text = "";
            txtKLPhuGia.Text = "";
            txtKLTroBay.Text = "";
            txtPhuPhi.Text = "";

            txtNguoiTruc.Text = "";
            txtKyThuat.Text = "";
            txtNguoiGioiThieu.Text = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblXuatBeTongs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        betong.sp_XuatBeTong_CheckSuaGV(query.IDHopDong, query.IDHopDongBom, query.IDLich, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            txtGioXuat.Text = query.GioXuat.ToString();
                            txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            LoadNhaCungCap();
                            dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                            if (dlCongTrinh.Items.Count == 0)
                                LoadCongTrinh();
                            dlCongTrinh.SelectedValue = query.IDCongTrinh.ToString();

                            if (dlHangMuc.Items.Count == 0)
                                LoadHangMuc();
                            dlHangMuc.SelectedValue = query.HangMuc.ToString();

                            if (dlMacBeTong.Items.Count == 0)
                                LoadMacBeTong();
                            dlMacBeTong.SelectedValue = query.MacBeTong.ToString();

                            if (dlHinhThucBom.Items.Count == 0)
                                LoadHinhThucBom();
                            dlHinhThucBom.SelectedValue = query.HinhThucBom.ToString();
                            LoadGiaBom();
                            LoadGiaBanBeTong();
                            txtKLThucXuat.Text = query.KLThucXuat.ToString();
                            txtKLKhachHang.Text = query.KLKhachHang.ToString();
                            txtKLHoaDon.Text = query.KLHoaDon.ToString();

                            txtTLTong.Text = query.TLTong.ToString();
                            txtTLBi.Text = query.TLBi.ToString();
                            txtTLHang.Text = query.TLHang.ToString();
                            txtTyLeQuyDoi.Text = query.TyLeQuyDoi.ToString();
                            txtSoPhieuCan.Text = query.IDPhieuCan;

                            //dlXiMang.SelectedValue = query.XiMang.ToString();
                            //dlCat.SelectedValue = query.Cat.ToString();
                            //dlDa.SelectedValue = query.Da.ToString();
                            //dlPhuGia.SelectedValue = query.PhuGia.ToString();
                            //dlTroBay.SelectedValue = query.TroBay.ToString();

                            txtDonGiaHoaDon.Text = string.Format("{0:N0}", query.DonGiaHoaDon);
                            txtDonGiaThanhToan.Text = string.Format("{0:N0}", query.DonGiaThanhToan);
                            txtTienHoaDon.Text = string.Format("{0:N0}", query.TienHoaDon);
                            txtTienThanhToan.Text = string.Format("{0:N0}", query.TienThanhToan);
                            txtDonGiaHoaDonBom.Text = string.Format("{0:N0}", query.DonGiaHoaDonBom);
                            txtDonGiaThanhToanBom.Text = string.Format("{0:N0}", query.DonGiaThanhToanBom);
                            txtTienHoaDonBom.Text = string.Format("{0:N0}", query.TienHoaDonBom);
                            txtTienThanhToanBom.Text = string.Format("{0:N0}", query.TienThanhToanBom);
                            hdLoaiBom.Value = query.LoaiBom.ToString();

                            //txtKLTramTron.Text = query.KLTramTron.ToString();
                            txtKLXiMang.Text = query.KLXiMang.ToString();
                            txtKLDa.Text = query.KLDa.ToString();
                            txtKLCat.Text = query.KLCat.ToString();
                            txtKLPhuGia.Text = query.KLPhuGia.ToString();
                            txtKLTroBay.Text = query.KLTroBay.ToString();
                            txtPhuPhi.Text = string.Format("{0:N0}", query.PhuPhi);

                            txtNguoiTruc.Text = query.NguoiTruc.ToString();
                            txtKyThuat.Text = query.KyThuat.ToString();
                            txtNguoiGioiThieu.Text = query.NguoiGioiThieu.ToString();

                            try
                            {
                                dlXeTron.SelectedValue = query.IDXeTron.ToString();
                            }
                            catch (Exception ex)
                            {
                                dlXeTron.SelectedValue = "";
                            }
                            try
                            {
                                dlXeBom.SelectedValue = query.IDXeBom.ToString();
                            }
                            catch (Exception ex)
                            {
                                dlXeBom.SelectedValue = "";
                            }

                            hdID.Value = id;
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        betong.sp_XuatBeTong_CheckXoa(query.ID, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblXuatBeTong_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblXuatBeTongs.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = betong.sp_XuatBeTong_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
                else if (e.CommandName == "ThuTien")
                {
                    if (query.DaThuTien == "Đã thu tiền")
                    {
                        query.DaThuTien = "Chưa thu tiền";
                    }
                    else
                    {
                        query.DaThuTien = "Đã thu tiền";
                    }
                    db.SubmitChanges();
                    Search(hdPage.Value == "" ? 1 : int.Parse(hdPage.Value));
                }
            }
            else
            {
                Warning("Thông tin phiếu xuất bê tông đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = false;
                e.Row.Cells[27].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thu tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giờ xuất",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khách hàng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Gold,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xe vận chuyển",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khối lượng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Violet,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá bán bê tông",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.LawnGreen,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Aquamarine,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Phụ phí",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tổng tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Thông tin vật liệu trộn",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.Goldenrod,
                //};
                //headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Trọng lượng vật liệu trộn",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.DarkSlateGray,
                //};
                //headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin người làm việc",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void GVChot_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giờ xuất",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khách hàng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Gold,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xe vận chuyển",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khối lượng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Violet,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá bán bê tông",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.LawnGreen,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Aquamarine,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tổng tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Thông tin vật liệu trộn",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.Goldenrod,
                //};
                //headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Trọng lượng vật liệu trộn",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.DarkSlateGray,
                //};
                //headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin người làm việc",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void gvLichSu_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Người tạo",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tạo",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giờ xuất",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khách hàng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Gold,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xe vận chuyển",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khối lượng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Violet,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá bán bê tông",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 4,
                    CssClass = "HeaderStyle",
                    BackColor = Color.LawnGreen,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Giá thuê bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Aquamarine,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tổng tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Cyan,
                };
                headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Thông tin vật liệu trộn",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.Goldenrod,
                //};
                //headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Trọng lượng vật liệu trộn",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.DarkSlateGray,
                //};
                //headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin người làm việc",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFilter();
        }
        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            //if (divdl.Visible == true)
            //{
            //    divdl.Visible = false;
            //    dlNhaCungCapSearch.Items.Clear();
            //    dlCongTrinhSearch.Items.Clear();
            //    dlMacBeTongSearch.Items.Clear();
            //}
            //else
            //{
            //    divdl.Visible = true;
            //    ResetFilter();
            //}
        }
        protected void ResetFilter()
        {

            dlKhachHangSearch.Items.Clear();
            dlMacBeTongSearch.Items.Clear();

            List<sp_XuatBeTong_ResetFilterLan1Result> query = betong.sp_XuatBeTong_ResetFilterLan1(GetDrop(dlChiNhanhSearch)).ToList();

            dlKhachHangSearch.DataSource = (from p in query
                                            select new
                                            {
                                                ID = p.IDNhaCungCap,
                                                Ten = p.TenNhaCungCap
                                            }).Distinct().OrderBy(p => p.Ten);
            dlKhachHangSearch.DataBind();
            dlKhachHangSearch.Items.Insert(0, new ListItem("Tất cả", ""));
            dlMacBeTongSearch.DataSource = (from p in query
                                            select new
                                            {
                                                ID = p.MacBeTong,
                                                Ten = p.TenMacBeTong
                                            }).Distinct().OrderBy(p => p.Ten);
            dlMacBeTongSearch.DataBind();
            dlMacBeTongSearch.Items.Insert(0, new ListItem("Tất cả", ""));

        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {

            if (txtTuNgaySearch.Text == "")
            {
                Warning("Nhập từ ngày tìm kiếm");
            }
            else if (txtDenNgaySearch.Text == "")
            {
                Warning("Nhập đến ngày tìm kiếm");
            }
            else if (DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)) > DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)))
            {
                Warning("Ngày bắt đầu tìm kiếm không được lớn hơn ngày kết thúc");
            }
            else
            {
                var query = betong.sp_XuatBeTong_Search(dlChiNhanhSearch.SelectedValue, dlKhachHangSearch.SelectedValue, dlMacBeTongSearch.SelectedValue,
                    DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 20, page);

                GV.DataSource = query;
                GV.DataBind();

                if (GV.Rows.Count > 0)
                {
                    sp_XuatBeTong_FooterResult footer = betong.sp_XuatBeTong_Footer(dlChiNhanhSearch.SelectedValue, dlKhachHangSearch.SelectedValue, dlMacBeTongSearch.SelectedValue,
                        DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();

                    GV.FooterRow.Cells[0].ColumnSpan = 14;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", decimal.Parse(footer.SoLuong.ToString()));
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", decimal.Parse(footer.KLThucXuat.ToString()));
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", decimal.Parse(footer.KLHoaDon.ToString()));
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", decimal.Parse(footer.KLKhachHang.ToString()));
                    GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", decimal.Parse(footer.KLQuaCan.ToString()));

                    GV.FooterRow.Cells[7].Text = string.Format("{0:N0}", decimal.Parse(footer.TienHoaDon.ToString()));
                    GV.FooterRow.Cells[8].Text = string.Format("{0:N0}", decimal.Parse(footer.TienThanhToan.ToString()));

                    GV.FooterRow.Cells[12].Text = string.Format("{0:N0}", decimal.Parse(footer.TienHoaDonBom.ToString()));
                    GV.FooterRow.Cells[13].Text = string.Format("{0:N0}", decimal.Parse(footer.TienThanhToanBom.ToString()));
                    GV.FooterRow.Cells[14].Text = string.Format("{0:N0}", decimal.Parse(footer.TongHoaDon.ToString()));
                    GV.FooterRow.Cells[15].Text = string.Format("{0:N0}", decimal.Parse(footer.PhuPhi.ToString()));
                    GV.FooterRow.Cells[16].Text = string.Format("{0:N0}", decimal.Parse(footer.TongThanhToan.ToString()));

                    //GV.FooterRow.Cells[20].Text = string.Format("{0:N0}", decimal.Parse(footer.KLXiMang"].ToString()));
                    //GV.FooterRow.Cells[21].Text = string.Format("{0:N0}", decimal.Parse(footer.KLDa"].ToString()));
                    //GV.FooterRow.Cells[22].Text = string.Format("{0:N0}", decimal.Parse(footer.KLCat"].ToString()));
                    //GV.FooterRow.Cells[23].Text = string.Format("{0:N0}", decimal.Parse(footer.KLPhuGia"].ToString()));
                    //GV.FooterRow.Cells[24].Text = string.Format("{0:N0}", decimal.Parse(footer.KLTroBay"].ToString()));

                    GV.FooterRow.Cells[20].Visible = false;
                    GV.FooterRow.Cells[21].Visible = false;
                    GV.FooterRow.Cells[22].Visible = false;
                    GV.FooterRow.Cells[23].Visible = false;
                    GV.FooterRow.Cells[25].Visible = false;
                    GV.FooterRow.Cells[26].Visible = false;
                    GV.FooterRow.Cells[27].Visible = false;
                    GV.FooterRow.Cells[28].Visible = false;
                    GV.FooterRow.Cells[29].Visible = false;
                    GV.FooterRow.Cells[30].Visible = false;
                    GV.FooterRow.Cells[31].Visible = false;
                    GV.FooterRow.Cells[32].Visible = false;
                    //GV.FooterRow.Cells[31].Visible = false;
                }
            }

        }

        //private void Search(int page)
        //{
        //    string query = "SELECT a.ID, a.NgayThang, a.GioXuat, i.TenChiNhanh, TenCongTrinh = h.CongTrinh, a.HangMuc,ns.TenNhanVien, b.TenNhaCungCap, TenMacBeTong = c.TenLoaiVatLieu, TenLoaiDa = d.TenLoaiVatLieu, TenXiMang = j.TenLoaiVatLieu, " +
        //        "TenCat = k.TenLoaiVatLieu, TenPhuGia = l.TenLoaiVatLieu, TenTroBay = m.TenLoaiVatLieu, g.TenHinhThucBom, a.IDXeTron, a.TenXeTron, a.IDXeBom, a.TenXeBom, a.KLThucXuat, a.KLKhachHang, " +
        //        "a.DonGiaHoaDon, a.DonGiaThanhToan, a.TienHoaDon, a.TienThanhToan, a.DonGiaHoaDonBom, a.DonGiaThanhToanBom, a.TienHoaDonBom, a.TienThanhToanBom, TongHoaDon = a.TienHoaDon + a.TienHoaDonBom, " +
        //        "TongThanhToan = a.TienThanhToan + a.TienThanhToanBom, a.KLKhachHang,a.KLHoaDon, a.KLThucXuat, a.KLQuaCan, a.KLTramTron, a.KLXiMang, a.KLDa, a.KLCat, a.KLPhuGia, a.KLTroBay,a.PhuPhi, " +
        //        "a.DMXiMang, a.DMDa, a.DMCat, a.DMPhuGia, a.DMTroBay, a.TrangThaiText, a.NguoiTao, a.NgayTao,a.NguoiTruc,a.KyThuat,a.NguoiGioiThieu,a.DaThuTien,a.TLTong,a.TLBi,a.TLHang,a.TyLeQuyDoi,a.IDPhieuCan " +
        //        "FROM tblXuatBeTong AS a JOIN tblNhaCungCap AS b ON a.IDNhaCungCap = b.ID " +
        //        "JOIN tblLoaiVatLieu AS c ON a.MacBeTong = c.ID " +
        //        "JOIN tblLoaiVatLieu AS d ON a.Da = d.ID " +
        //        "LEFT JOIN tblHinhThucBom AS g ON a.HinhThucBom = g.ID " +
        //        "JOIN tblHopDongBanBeTong AS h ON a.IDCongTrinh = h.ID JOIN " +
        //        "tblLoaiVatLieu AS j ON a.XiMang = j.ID " +
        //        "JOIN tblLoaiVatLieu AS k ON a.Cat = k.ID LEFT JOIN " +
        //        "tblLoaiVatLieu AS l ON a.PhuGia = l.ID " +
        //        "LEFT JOIN tblLoaiVatLieu AS m ON a.TroBay = m.ID " +
        //        "JOIN tblChiNhanh AS i ON a.IDChiNhanh = i.ID " +
        //        "left JOIN tblNhanSu AS ns ON a.IDNVKD = ns.ID " +
        //        "WHERE a.IDChiNhanh = '" + dlChiNhanhSearch.SelectedValue + "' AND a.NgayThang BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "' ";
        //    string sqlgiua = "";

        //    sqlgiua += GetValueSelectedListBox(" and a.IDNhaCungCap ", dlNhaCungCapSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.IDCongTrinh ", dlCongTrinhSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.MacBeTong ", dlMacBeTongSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.HinhThucBom ", dlHinhThucBomSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.XiMang ", dlXiMangSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.Cat ", dlCatSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.Da ", dlDaSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.PhuGia ", dlPhuGiaSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.TroBay ", dlTroBaySearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.IDXeTron ", dlXeTronSearch);
        //    sqlgiua += GetValueSelectedListBox(" and a.IDXeBom ", dlXeBomSearch);

        //    string sqlcuoi = " ORDER BY a.NgayThang desc OFFSET 20 * (" + Convert.ToString(page) + " - 1) ROWS FETCH NEXT 20 ROWS ONLY";

        //    query = query + sqlgiua + sqlcuoi;
        //    SqlConnection con = new SqlConnection(constr);

        //    SqlCommand cmd = new SqlCommand(query, con);
        //    SqlDataAdapter sda = new SqlDataAdapter(cmd);
        //    DataTable ds = new DataTable();
        //    sda.Fill(ds);

        //    DataTable dt = new DataTable();
        //    string querysum = "SELECT SoLuong = COUNT(*), SumKLKhachHang = SUM(KLKhachHang), SumKLHoaDon = SUM(KLHoaDon), SumKLThucXuat = SUM(KLThucXuat), SumKLQuaCan = SUM(KLQuaCan), SumTienHoaDon = SUM(TienHoaDon), SumTienThanhToan = SUM(TienThanhToan), " +
        //        "SumTienHoaDonBom = SUM(TienHoaDonBom), SumTienThanhToanBom = SUM(TienThanhToanBom), SumTongHoaDon = SUM(TienHoaDon + TienHoaDonBom), SumTongThanhToan = SUM(TienThanhToan + TienThanhToanBom), SumKLXiMang = SUM(KLXiMang), " +
        //        "SumKLDa = SUM(KLDa), SumKLCat = SUM(KLCat), SumKLPhuGia = SUM(KLPhuGia), SumKLTroBay = SUM(KLTroBay),SumPhuPhi = SUM(PhuPhi) FROM tblXuatBeTong as a WHERE a.IDChiNhanh = '" + dlChiNhanhSearch.SelectedValue + "' AND a.NgayThang BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "'  ";
        //    querysum += sqlgiua;
        //    SqlCommand cmd1 = new SqlCommand(querysum, con);
        //    SqlDataAdapter sda1 = new SqlDataAdapter(cmd1);
        //    sda1.Fill(dt);
        //    con.Close();
        //    con.Dispose();

        //    GV.DataSource = ds;
        //    GV.DataBind();

        //    if (GV.Rows.Count > 0)
        //    {
        //        if (query != null)
        //        {
        //            GV.FooterRow.Cells[0].ColumnSpan = 14;
        //            GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SoLuong"].ToString()));
        //            GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

        //            GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLThucXuat"].ToString()));
        //            GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLHoaDon"].ToString()));
        //            GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLKhachHang"].ToString()));
        //            GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLQuaCan"].ToString()));

        //            GV.FooterRow.Cells[7].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumTienHoaDon"].ToString()));
        //            GV.FooterRow.Cells[8].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumTienThanhToan"].ToString()));

        //            GV.FooterRow.Cells[12].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumTienHoaDonBom"].ToString()));
        //            GV.FooterRow.Cells[13].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumTienThanhToanBom"].ToString()));
        //            GV.FooterRow.Cells[14].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumTongHoaDon"].ToString()));
        //            GV.FooterRow.Cells[15].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumTongThanhToan"].ToString()));

        //            //GV.FooterRow.Cells[20].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLXiMang"].ToString()));
        //            //GV.FooterRow.Cells[21].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLDa"].ToString()));
        //            //GV.FooterRow.Cells[22].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLCat"].ToString()));
        //            //GV.FooterRow.Cells[23].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLPhuGia"].ToString()));
        //            //GV.FooterRow.Cells[24].Text = string.Format("{0:N0}", decimal.Parse(dt.Rows[0]["SumKLTroBay"].ToString()));

        //            GV.FooterRow.Cells[19].Visible = false;
        //            GV.FooterRow.Cells[20].Visible = false;
        //            GV.FooterRow.Cells[21].Visible = false;
        //            GV.FooterRow.Cells[22].Visible = false;
        //            GV.FooterRow.Cells[23].Visible = false;
        //            GV.FooterRow.Cells[25].Visible = false;
        //            GV.FooterRow.Cells[26].Visible = false;
        //            GV.FooterRow.Cells[27].Visible = false;
        //            GV.FooterRow.Cells[28].Visible = false;
        //            GV.FooterRow.Cells[29].Visible = false;
        //            GV.FooterRow.Cells[30].Visible = false;
        //            //GV.FooterRow.Cells[31].Visible = false;
        //        }
        //    }
        //}
        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        void LoadNhaCungCap()
        {
            if (txtNgayThang.Text != "" && dlChiNhanh.SelectedValue != "")
            {
                dlNhaCungCap.Items.Clear();
                var query = betong.sp_XuatBeTong_LoadNhaCungCap(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh));
                dlNhaCungCap.DataSource = query;
                dlNhaCungCap.DataBind();
                if (dlNhaCungCap.Items.Count == 1)
                {
                    LoadCongTrinh();
                }
                else
                {
                    dlNhaCungCap.Items.Insert(0, new ListItem("Chọn", ""));
                    dlCongTrinh.Items.Clear();
                    dlMacBeTong.Items.Clear();
                }
            }
            else
            {
                dlNhaCungCap.Items.Clear();
                dlCongTrinh.Items.Clear();
                dlMacBeTong.Items.Clear();
            }
        }
        void LoadCongTrinh()
        {
            if (txtNgayThang.Text != "" && dlNhaCungCap.SelectedValue != "")
            {
                dlCongTrinh.Items.Clear();
                var query = betong.sp_XuatBeTong_LoadCongTrinh(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap));
                dlCongTrinh.DataSource = query;
                dlCongTrinh.DataBind();
                if (dlCongTrinh.Items.Count == 1)
                {
                    LoadHangMuc();
                }
                else
                {
                    dlCongTrinh.Items.Insert(0, new ListItem("Chọn", ""));
                    dlHangMuc.Items.Clear();
                }
            }
            else
            {
                dlCongTrinh.Items.Clear();
                dlHangMuc.Items.Clear();
                dlMacBeTong.Items.Clear();
            }
        }
        void LoadHangMuc()
        {
            if (txtNgayThang.Text != "" && dlCongTrinh.SelectedValue != "")
            {
                dlHangMuc.Items.Clear();
                var query = betong.sp_XuatBeTong_LoadHangMuc(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlCongTrinh));
                dlHangMuc.DataSource = query;
                dlHangMuc.DataBind();
                if (dlHangMuc.Items.Count == 1)
                {
                    LoadMacBeTong();
                    LoadHinhThucBom();
                }
                else
                {
                    dlHangMuc.Items.Insert(0, new ListItem("Chọn", ""));
                    dlMacBeTong.Items.Clear();
                }
            }
            else
            {
                dlHangMuc.Items.Clear();
                dlMacBeTong.Items.Clear();
            }
        }

        void LoadMacBeTong()
        {
            if (txtNgayThang.Text != "" && dlCongTrinh.SelectedValue != "")
            {
                dlMacBeTong.Items.Clear();
                var query = betong.sp_XuatBeTong_LoadMacBeTong(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue);
                dlMacBeTong.DataSource = query;
                dlMacBeTong.DataBind();
                if (dlMacBeTong.Items.Count == 1)
                {
                    LoadGiaBanBeTong();
                    LoadHinhThucBom();
                }
                else
                {
                    dlMacBeTong.Items.Insert(0, new ListItem("Chọn", ""));
                }
            }
            else
            {
                dlMacBeTong.Items.Clear();
            }

            dlNhanVienKinhDoanh.Items.Clear();
            var nvkd = (from p in db.tblLichXuatBeTongs
                        join q in db.tblNhanSus on p.IDNVKD equals q.ID
                        where p.IDCongTrinh == GetDrop(dlCongTrinh)
                        && p.NgayThang == DateTime.Parse(GetNgayThang(txtNgayThang.Text))
                        select new
                        {
                            ID = p.IDNVKD,
                            Ten = q.TenNhanVien
                        }).Distinct();
            dlNhanVienKinhDoanh.DataSource = nvkd;
            dlNhanVienKinhDoanh.DataBind();
        }
        void LoadHinhThucBom()
        {
            if (txtNgayThang.Text != "" && dlCongTrinh.SelectedValue != "")
            {
                dlHinhThucBom.Items.Clear();
                var query = betong.sp_XuatBeTong_LoadHinhThucBom(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlMacBeTong));
                dlHinhThucBom.DataSource = query;
                dlHinhThucBom.DataBind();
                if (dlHinhThucBom.Items.Count == 1)
                {
                    var loaibom = (from p in db.tblHinhThucBoms
                                   where p.ID == GetDrop(dlHinhThucBom)
                                   select p).FirstOrDefault();
                    if (loaibom != null && loaibom.ID != null)
                    {
                        hdLoaiBom.Value = loaibom.LoaiBom.ToString();
                    }
                    LoadGiaBom();
                }
                else
                {
                    dlHinhThucBom.Items.Insert(0, new ListItem("Chọn", ""));
                }
            }
            else
            {
                dlHinhThucBom.Items.Clear();
            }
        }
        void LoadGiaBom()
        {
            if (txtNgayThang.Text != "" && dlHinhThucBom.SelectedValue.ToUpper() != "2862C6F2-0AE1-499B-93B7-6E2B0AB46B83".ToUpper())
            {
                var query = betong.sp_XuatBeTong_LoadGiaBom(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlMacBeTong), GetDrop(dlHinhThucBom)).FirstOrDefault();
                if (query != null && query.DonGiaCoThue != null)
                {
                    txtDonGiaHoaDonBom.Text = string.Format("{0:N0}", query.DonGiaCoThue);
                    txtDonGiaThanhToanBom.Text = string.Format("{0:N0}", query.DonGiaKhongThue);
                    if (txtKLKhachHang.Text != "" && decimal.Parse(txtKLKhachHang.Text) > 0)
                    {
                        if (hdLoaiBom.Value == "1")
                        {
                            txtTienThanhToanBom.Text = string.Format("{0:N0}", query.DonGiaKhongThue * decimal.Parse(txtKLKhachHang.Text));
                        }
                        else if (hdLoaiBom.Value == "0")
                        {
                            txtTienThanhToanBom.Text = string.Format("{0:N0}", query.DonGiaKhongThue);
                        }
                        else
                        {
                            txtTienThanhToanBom.Text = "0";
                        }
                    }
                    if (txtKLHoaDon.Text != "" && decimal.Parse(txtKLHoaDon.Text) > 0)
                    {
                        if (hdLoaiBom.Value == "1")
                        {
                            txtTienHoaDonBom.Text = string.Format("{0:N0}", query.DonGiaCoThue * decimal.Parse(txtKLHoaDon.Text));
                        }
                        else if (hdLoaiBom.Value == "0")
                        {
                            txtTienHoaDonBom.Text = string.Format("{0:N0}", query.DonGiaCoThue);
                        }
                        else
                        {
                            txtTienHoaDonBom.Text = "0";
                        }
                    }
                }
                else
                {
                    txtDonGiaHoaDonBom.Text = "0";
                    txtDonGiaThanhToanBom.Text = "0";
                    txtTienHoaDonBom.Text = "0";
                    txtTienThanhToanBom.Text = "0";
                }
            }
            else
            {
                txtDonGiaHoaDonBom.Text = "0";
                txtDonGiaThanhToanBom.Text = "0";
                txtTienHoaDonBom.Text = "0";
                txtTienThanhToanBom.Text = "0";
            }
        }
        void LoadGiaBanBeTong()
        {
            if (txtNgayThang.Text != "" && dlMacBeTong.SelectedValue != "")
            {
                var query = betong.sp_XuatBeTong_LoadGiaBanBeTong(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh), dlHangMuc.SelectedValue, GetDrop(dlMacBeTong)).FirstOrDefault();
                if (query != null && query.DonGiaCoThue != null)
                {
                    txtDonGiaHoaDon.Text = string.Format("{0:N0}", query.DonGiaCoThue);
                    txtDonGiaThanhToan.Text = string.Format("{0:N0}", query.DonGiaKhongThue);
                    if (txtKLKhachHang.Text != "" && decimal.Parse(txtKLKhachHang.Text) > 0)
                    {
                        txtTienThanhToan.Text = string.Format("{0:N0}", query.DonGiaKhongThue * decimal.Parse(txtKLKhachHang.Text));
                    }
                    if (txtKLHoaDon.Text != "" && decimal.Parse(txtKLHoaDon.Text) > 0)
                    {
                        txtTienHoaDon.Text = string.Format("{0:N0}", query.DonGiaCoThue * decimal.Parse(txtKLHoaDon.Text));
                    }
                }
                else
                {
                    txtDonGiaHoaDon.Text = "0";
                    txtDonGiaThanhToan.Text = "0";
                    txtTienHoaDon.Text = "0";
                    txtTienThanhToan.Text = "0";
                }
            }
            else
            {
                txtDonGiaHoaDon.Text = "0";
                txtDonGiaThanhToan.Text = "0";
                txtTienHoaDon.Text = "0";
                txtTienThanhToan.Text = "0";
            }
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }
        protected void dlNhaCungCap_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCongTrinh();
        }
        protected void dlCongTrinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadHangMuc();
        }
        protected void dlHangMuc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadMacBeTong();
        }
        protected void dlMacBeTong_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGiaBanBeTong();
            LoadHinhThucBom();
        }
        protected void dlHinhThucBom_SelectedIndexChanged(object sender, EventArgs e)
        {
            var loaibom = (from p in db.tblHinhThucBoms
                           where p.ID == GetDrop(dlHinhThucBom)
                           select p).FirstOrDefault();
            if (loaibom != null && loaibom.ID != null)
            {
                hdLoaiBom.Value = loaibom.LoaiBom.ToString();
            }
            LoadGiaBom();
        }
        protected void btnCan_Click(object sender, EventArgs e)
        {
            if (dlChiNhanh.SelectedValue == "")
            {
                GstGetMess("Bạn phải chọn chi nhánh.", "");
            }
            else
            {
                var query = db.sp_Can_LoadPhieuCanTheoNgay(GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)));
                gvCan.DataSource = query;
                gvCan.DataBind();
                mpCan.Show();
            }
        }
        protected void btnResetCan_Click(object sender, EventArgs e)
        {
            txtTLTong.Text = "0";
            txtTLBi.Text = "0";
            txtTLHang.Text = "0";
            txtKLQuaCan.Text = "0";
        }
        protected void gvCan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblCans
                         where p.IDMN == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Chon")
                {
                    txtTLTong.Text = query.KlCanL1 > query.KlCanL2 ? string.Format("{0:N0}", query.KlCanL1) : string.Format("{0:N0}", query.KlCanL2);
                    txtTLBi.Text = query.KlCanL1 < query.KlCanL2 ? string.Format("{0:N0}", query.KlCanL1) : string.Format("{0:N0}", query.KlCanL2);
                    txtTLHang.Text = string.Format("{0:N0}", query.TLHang);
                    txtTyLeQuyDoi.Text = query.TyKhoi.ToString();

                    if (query.TyKhoi != "" && query.TyKhoi != "0")
                    {
                        txtKLQuaCan.Text = string.Format("{0:N2}", (query.TLHang / double.Parse(query.TyKhoi)));
                    }
                    else
                    {
                        txtTyLeQuyDoi.Text = "2300";
                        txtKLQuaCan.Text = string.Format("{0:N2}", (query.TLHang / double.Parse(txtTyLeQuyDoi.Text.Trim())));
                    }
                    //txtTenBienSoXe.Text = query.BienSoXe.ToUpper();
                    //txtTenLaiXe.Text = query.LaiXe;
                    txtSoPhieuCan.Text = query.SoPhieu.ToString();
                }
            }
        }
        protected void btnChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 6, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else
                {
                    betong.sp_XuatBeTong_CheckChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        betong.sp_XuatBeTong_Chot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), Session["IDND"].ToString(), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }
        protected void btnMoChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 7, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh mở chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else
                {
                    betong.sp_XuatBeTong_CheckMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        betong.sp_XuatBeTong_MoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), Session["IDND"].ToString(), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }
        protected void btnOpenChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Chốt BÁN BÊ TÔNG";
            mpChot.Show();
            btnChot.Visible = true;
            btnMoChot.Visible = false;
        }
        protected void btnOpenMoChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Mở chốt BÁN BÊ TÔNG";
            mpChot.Show();
            btnChot.Visible = false;
            btnMoChot.Visible = true;
        }
        protected void lbtSearchChot_Click(object sender, EventArgs e)
        {
            if (btnChot.Visible == true)
            {
                List<sp_XuatBeTong_ListChotResult> query = betong.sp_XuatBeTong_ListChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
                GVChot.DataSource = query;
                GVChot.DataBind();

                //if (GVChot.Rows.Count > 0)
                //{
                //    if (query != null)
                //    {
                //        var valuefooter = (from p in query
                //                           group p by 1 into g
                //                           select new
                //                           {
                //                               SumTLTong = g.Sum(x => x.TLTong),
                //                               SumTLBi = g.Sum(x => x.TLBi),
                //                               SumTLHang = g.Sum(x => x.TLHang),
                //                               SumSumDone = g.Sum(x => x.TLBi),
                //                               SumKLMua = g.Sum(x => x.KLMua),
                //                               SumKLNhapKho = g.Sum(x => x.KLNhapKho),
                //                               SumKLQuanCan = g.Sum(x => x.KLQuanCan),
                //                               SumThanhTienCoThue = g.Sum(x => x.ThanhTienCoThue),
                //                               SumThanhTienKhongThue = g.Sum(x => x.ThanhTienKhongThue)
                //                           }).FirstOrDefault();
                //        GVChot.FooterRow.Cells[0].ColumnSpan = 5;
                //        GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Count) + " phiếu nhập kho";
                //        GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                //        GVChot.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.SumKLMua);
                //        GVChot.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.SumKLNhapKho);
                //        GVChot.FooterRow.Cells[7].Text = string.Format("{0:N0}", valuefooter.SumKLQuanCan);
                //        GVChot.FooterRow.Cells[10].Text = string.Format("{0:N0}", valuefooter.SumThanhTienCoThue);
                //        GVChot.FooterRow.Cells[11].Text = string.Format("{0:N0}", valuefooter.SumThanhTienKhongThue);
                //        GVChot.FooterRow.Cells[12].Text = string.Format("{0:N0}", valuefooter.SumTLTong);
                //        GVChot.FooterRow.Cells[13].Text = string.Format("{0:N0}", valuefooter.SumTLBi);
                //        GVChot.FooterRow.Cells[14].Text = string.Format("{0:N0}", valuefooter.SumTLHang);

                //        GVChot.FooterRow.Cells[15].Visible = false;
                //        GVChot.FooterRow.Cells[16].Visible = false;
                //        GVChot.FooterRow.Cells[17].Visible = false;
                //        GVChot.FooterRow.Cells[18].Visible = false;
                //        GVChot.FooterRow.Cells[19].Visible = false;
                //    }
                //}
            }
            else if (btnChot.Visible == false)
            {
                List<sp_XuatBeTong_ListMoChotResult> query = betong.sp_XuatBeTong_ListMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text))).ToList();
                GVChot.DataSource = query;
                GVChot.DataBind();

                //if (GVChot.Rows.Count > 0)
                //{
                //    if (query != null)
                //    {
                //        var valuefooter = (from p in query
                //                           group p by 1 into g
                //                           select new
                //                           {
                //                               SumTLTong = g.Sum(x => x.TLTong),
                //                               SumTLBi = g.Sum(x => x.TLBi),
                //                               SumTLHang = g.Sum(x => x.TLHang),
                //                               SumSumDone = g.Sum(x => x.TLBi),
                //                               SumKLMua = g.Sum(x => x.KLMua),
                //                               SumKLNhapKho = g.Sum(x => x.KLNhapKho),
                //                               SumKLQuanCan = g.Sum(x => x.KLQuanCan),
                //                               SumThanhTienCoThue = g.Sum(x => x.ThanhTienCoThue),
                //                               SumThanhTienKhongThue = g.Sum(x => x.ThanhTienKhongThue)
                //                           }).FirstOrDefault();
                //        GVChot.FooterRow.Cells[0].ColumnSpan = 5;
                //        GVChot.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Count) + " phiếu nhập kho";
                //        GVChot.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                //        GVChot.FooterRow.Cells[5].Text = string.Format("{0:N0}", valuefooter.SumKLMua);
                //        GVChot.FooterRow.Cells[6].Text = string.Format("{0:N0}", valuefooter.SumKLNhapKho);
                //        GVChot.FooterRow.Cells[7].Text = string.Format("{0:N0}", valuefooter.SumKLQuanCan);
                //        GVChot.FooterRow.Cells[10].Text = string.Format("{0:N0}", valuefooter.SumThanhTienCoThue);
                //        GVChot.FooterRow.Cells[11].Text = string.Format("{0:N0}", valuefooter.SumThanhTienKhongThue);
                //        GVChot.FooterRow.Cells[12].Text = string.Format("{0:N0}", valuefooter.SumTLTong);
                //        GVChot.FooterRow.Cells[13].Text = string.Format("{0:N0}", valuefooter.SumTLBi);
                //        GVChot.FooterRow.Cells[14].Text = string.Format("{0:N0}", valuefooter.SumTLHang);


                //        GVChot.FooterRow.Cells[15].Visible = false;
                //        GVChot.FooterRow.Cells[16].Visible = false;
                //        GVChot.FooterRow.Cells[17].Visible = false;
                //        GVChot.FooterRow.Cells[18].Visible = false;
                //        GVChot.FooterRow.Cells[19].Visible = false;
                //    }
                //}
            }
            mpChot.Show();
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_XuatBeTong_PrintResult> query = betong.sp_XuatBeTong_Print(dlChiNhanhSearch.SelectedValue, dlKhachHangSearch.SelectedValue, dlMacBeTongSearch.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();


            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "BÁO CÁO BÁN BÊ TÔNG";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 7);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO BÁN BÊ TÔNG TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                }
                else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO BÁN BÊ TÔNG NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 29);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 29);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Giờ xuất",
                    "Ngày tháng",
                    "Chi nhánh",
                    "Thông tin khách hàng",
                    "Thông tin khách hàng",
                    "Thông tin khách hàng",
                    "Thông tin khách hàng",
                    "Thông tin khách hàng",
                    "Xe vận chuyển",
                    "Xe vận chuyển",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Giá bán bê tông",
                    "Giá bán bê tông",
                    "Giá bán bê tông",
                    "Giá bán bê tông",
                    "Giá thuê bơm",
                    "Giá thuê bơm",
                    "Giá thuê bơm",
                    "Giá thuê bơm",
                    "Giá thuê bơm",
                    "Phụ phí",
                    "Thành tiền",
                    "Thành tiền",
                    "Thông tin người làm việc",
                    "Thông tin người làm việc",
                    "Thông tin người làm việc"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Giờ xuất",
                    "Ngày tháng",
                    "Chi nhánh",
                    "Khách hàng",
                    "Công trình",
                    "Hạng mục",
                    "Tên nhân viên",
                    "Mác bê tông",
                    "Xe trộn",
                    "Xe bơm",
                    "KL thực xuất",
                    "KL hóa đơn",
                    "KL khách hàng",
                    "KL cân",
                    "Giá hóa đơn",
                    "Giá thanh toán",
                    "Tiền hóa đơn",
                    "Tiền thanh toán",
                    "Hình thức bơm",
                    "Giá hoán đơn",
                    "Giá thanh toán",
                    "Tiền hóa đơn",
                    "Tiền thanh toán",
                    "Phụ phí",
                    "Hóa đơn",
                    "Thanh toán",
                    "Người trực",
                    "Kỹ thuật",
                    "Người giới thiệu"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 3, 3);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 4, 8);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 9, 10);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 11, 14);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 15, 18);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 19, 23);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 24, 24);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 25, 26);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 27, 29);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.GioXuat.ToString());
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.NgayThang);
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(item.TenNhaCungCap);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(item.TenCongTrinh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(item.HangMuc);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(item.TenNhanVien);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(item.TenMacBeTong);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(item.TenXeTron);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(item.TenXeBom);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(11);
                    cell.SetCellValue(double.Parse(item.KLThucXuat.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(12);
                    cell.SetCellValue(double.Parse(item.KLHoaDon.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(13);
                    cell.SetCellValue(double.Parse(item.KLKhachHang.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(14);
                    cell.SetCellValue(double.Parse(item.KLQuaCan.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(15);
                    cell.SetCellValue(double.Parse(item.DonGiaHoaDon.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(16);
                    cell.SetCellValue(double.Parse(item.DonGiaThanhToan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(17);
                    cell.SetCellValue(double.Parse(item.TienHoaDon.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(18);
                    cell.SetCellValue(double.Parse(item.TienThanhToan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(19);
                    cell.SetCellValue(item.TenHinhThucBom);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(20);
                    cell.SetCellValue(double.Parse(item.DonGiaHoaDonBom.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(21);
                    cell.SetCellValue(double.Parse(item.DonGiaThanhToanBom.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(22);
                    cell.SetCellValue(double.Parse(item.TienHoaDonBom.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(23);
                    cell.SetCellValue(double.Parse(item.TienThanhToanBom.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(24);
                    cell.SetCellValue(double.Parse(item.PhuPhi.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(25);
                    cell.SetCellValue(double.Parse(item.TongHoaDon.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(26);
                    cell.SetCellValue(double.Parse(item.TongThanhToan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(27);
                    cell.SetCellValue(item.NguoiTruc);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(28);
                    cell.SetCellValue(item.KyThuat);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(29);
                    cell.SetCellValue(item.NguoiGioiThieu);
                    cell.CellStyle = styleBody;


                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= 20; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 8);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(11);
                cell.CellFormula = "SUM(L" + RowDau.ToString() + ":L" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(12);
                cell.CellFormula = "SUM(M" + RowDau.ToString() + ":M" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(13);
                cell.CellFormula = "SUM(N" + RowDau.ToString() + ":N" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(14);
                cell.CellFormula = "SUM(O" + RowDau.ToString() + ":O" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(17);
                cell.CellFormula = "SUM(R" + RowDau.ToString() + ":R" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(18);
                cell.CellFormula = "SUM(S" + RowDau.ToString() + ":S" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(19); cell.CellStyle = styleBodyDoubleFooterRight;
                cell = row.CreateCell(20); cell.CellStyle = styleBodyDoubleFooterRight;
                cell = row.CreateCell(21); cell.CellStyle = styleBodyDoubleFooterRight;
                cell = row.CreateCell(22); cell.CellStyle = styleBodyDoubleFooterRight;

                cell.CellFormula = "SUM(W" + RowDau.ToString() + ":W" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(23);
                cell.CellFormula = "SUM(X" + RowDau.ToString() + ":X" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(24);
                cell.CellFormula = "SUM(Y" + RowDau.ToString() + ":Y" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(25);
                cell.CellFormula = "SUM(Z" + RowDau.ToString() + ":Z" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(26);
                cell.CellFormula = "SUM(AA" + RowDau.ToString() + ":AA" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(27); cell.CellStyle = styleBodyDoubleFooterRight;
                cell = row.CreateCell(28); cell.CellStyle = styleBodyDoubleFooterRight;
                cell = row.CreateCell(29); cell.CellStyle = styleBodyDoubleFooterRight;
                #endregion

                #region[Set Footer]

                //rowIndex++;
                //rowIndex++;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo ca:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo khối:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê xe:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Không dùng bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Mua bê tông:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;
                //tên
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 1);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(0);
                cell.SetCellValue("Người xuất hàng");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(2);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 4, 5);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(4);
                cell.SetCellValue("Trưởng bộ phận");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 8);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(6);
                cell.SetCellValue("Lái xe");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 9, 10);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(9);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 11, 13);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(11);
                cell.SetCellValue("Giám sát");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                #endregion


                sheet.SetColumnWidth(4, 7120);
                sheet.SetColumnWidth(5, 10120);
                sheet.SetColumnWidth(5, 10120);
                sheet.SetColumnWidth(8, 4000);
                sheet.SetColumnWidth(19, 3300);


                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO BÁN BÊ TÔNG -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                         ".xls";
                    }
                    else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO BÁN BÊ TÔNG -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                         "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }
    }
}