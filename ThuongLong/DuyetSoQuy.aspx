﻿<%@ Page Title="Duyệt sổ quỹ" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetSoQuy.aspx.cs" Inherits="ThuongLong.DuyetSoQuy" %>

<%@ Register Src="UCSoQuy/uc_DuyetPhieuThuChi.ascx" TagName="uc_DuyetPhieuThuChi" TagPrefix="uc1" %>
<%@ Register Src="UCSoQuy/uc_DuyetChotSoQuyCongTy.ascx" TagName="uc_DuyetChotSoQuyCongTy" TagPrefix="uc2" %>
<%--<%@ Register Src="UCSoQuy/uc_DuyetGiaBanVatLieu.ascx" TagName="uc_DuyetGiaBanVatLieu" TagPrefix="uc3" %>
<%@ Register Src="UCSoQuy/uc_DuyetBanVatLieu.ascx" TagName="uc_DuyetBanVatLieu" TagPrefix="uc4" %>
<%@ Register Src="UCSoQuy/uc_DuyetChotPhieuThu.ascx" TagName="uc_DuyetChotPhieuThu" TagPrefix="uc5" %>
<%@ Register Src="UCSoQuy/uc_DuyetChotBanVatLieu.ascx" TagName="uc_DuyetChotBanVatLieu" TagPrefix="uc6" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="frmPhieuThuChi" runat="server" HeaderText="Phiếu thu - chi">
                            <ContentTemplate>
                                <uc1:uc_DuyetPhieuThuChi ID="uc_DuyetPhieuThuChi1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxtoolkit:tabpanel id="frmChotSoQuyCongTy" runat="server" headertext="Chốt sổ quỹ">
                            <contenttemplate>
                                <uc2:uc_DuyetChotSoQuyCongTy id="uc_DuyetChotSoQuyCongTy1" runat="server" />
                            </contenttemplate>
                        </ajaxtoolkit:tabpanel>--%>
                        
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
