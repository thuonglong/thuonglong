﻿<%@ Page Title="Định mức xe" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DinhMucXe.aspx.cs" Inherits="ThuongLong.DinhMucXe" %>

<%@ Register Src="UCDinhMucXe/uc_DinhMucXeBen.ascx" TagName="uc_DinhMucXeBen" TagPrefix="uc1" %>
<%@ Register Src="UCDinhMucXe/uc_DinhMucXeDauKeo.ascx" TagName="uc_DinhMucXeDauKeo" TagPrefix="uc2" %>
<%@ Register Src="UCDinhMucXe/uc_DinhMucXeXucLat.ascx" TagName="uc_DinhMucXeXucLat" TagPrefix="uc3" %>
<%@ Register Src="UCDinhMucXe/uc_DinhMucXeXucDao.ascx" TagName="uc_DinhMucXeXucDao" TagPrefix="uc4" %>
<%@ Register Src="UCDinhMucXe/uc_DinhMucXeBom.ascx" TagName="uc_DinhMucXeBom" TagPrefix="uc5" %>
<%@ Register Src="UCDinhMucXe/uc_DinhMucXeBeTong.ascx" TagName="uc_DinhMucXeBeTong" TagPrefix="uc6" %>
<%@ Register Src="UCDinhMucXe/uc_DinhMucXeCau.ascx" TagName="uc_DinhMucXeCau" TagPrefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chọn loại xe</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chọn loại xe"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiXe" runat="server" OnSelectedIndexChanged="dlLoaiXe_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="">Chọn loại xe</asp:ListItem>
                                <asp:ListItem Value="1">Xe ben</asp:ListItem>
                                <asp:ListItem Value="2">Xe đầu kéo</asp:ListItem>
                                <asp:ListItem Value="3">Xe xúc lật</asp:ListItem>
                                <asp:ListItem Value="4">Xe xúc đào</asp:ListItem>
                                <asp:ListItem Value="5">Xe bơm bê tông</asp:ListItem>
                                <asp:ListItem Value="6">Xe vận chuyển bê tông</asp:ListItem>
                                <asp:ListItem Value="7">Xe cẩu</asp:ListItem>
                                <asp:ListItem Value="">Xe nâng</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <uc1:uc_DinhMucXeBen ID="DinhMucXeBen" runat="server" />
            <uc2:uc_DinhMucXeDauKeo ID="DinhMucXeDauKeo" runat="server" />
            <uc3:uc_DinhMucXeXucLat ID="DinhMucXeXucLat" runat="server" />
            <uc4:uc_DinhMucXeXucDao ID="DinhMucXeXucDao" runat="server" />
            <uc5:uc_DinhMucXeBom ID="DinhMucXeBom" runat="server" />
            <uc6:uc_DinhMucXeBeTong ID="DinhMucXeBeTong" runat="server" />
            <uc7:uc_DinhMucXeCau ID="DinhMucXeCau" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
