﻿using NPOI.HSSF.Record;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class HopDongBanBeTong : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        BeTongDataContext vatlieu = new BeTongDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmHopDongBanBeTong";
        DataTable dt = new DataTable();
        DataTable dtbom = new DataTable();
        DataTable dtNVKD = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmHopDongBanBeTong", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadLoaiNhaCungCap();
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNhaCungCap();
                        LoadMacBeTong();
                        LoadHinhThucBom();
                        LoadNVKD();
                        hdPage.Value = "1";
                        Session["HopDongBanBeTong"] = null;
                        Session["HopDongBanBeTong_Bom"] = null;
                        hdForm.Value = Guid.NewGuid().ToString();
                        LoadKhachHangSearch();
                    }
                }
            }
        }
        void GetTable(DataTable table)
        {
            table.Columns.Add("ID", typeof(Guid));
            table.Columns.Add("IDHD", typeof(Guid));
            table.Columns.Add("IDMacBeTong", typeof(Guid));
            table.Columns.Add("TenMacBeTong", typeof(string));
            table.Columns.Add("DonGiaCoThue", typeof(decimal));
            table.Columns.Add("DonGiaKhongThue", typeof(decimal));
            table.Columns.Add("KhoiLuongDuKien", typeof(double));
            table.Columns.Add("TuNgay", typeof(string));
            table.Columns.Add("DenNgay", typeof(string));
            table.Columns.Add("TrangThai", typeof(Int32));
            table.Columns.Add("TrangThaiText", typeof(string));
            table.Columns.Add("NguoiDuyet", typeof(string));
            table.Columns.Add("NguoiXoa", typeof(string));
            table.Columns.Add("NgayTao", typeof(DateTime));
            table.Columns.Add("NguoiTao", typeof(string));
            table.Columns.Add("MoTa", typeof(string));
            table.Columns.Add("Loai", typeof(Int32));
            table.Columns.Add("hdForm", typeof(string));
        }
        void GetBom(DataTable table)
        {
            table.Columns.Add("ID", typeof(Guid));
            table.Columns.Add("IDHD", typeof(Guid));
            table.Columns.Add("IDHinhThucBom", typeof(Guid));
            table.Columns.Add("TenHinhThucBom", typeof(string));
            table.Columns.Add("DonGiaCoThue", typeof(decimal));
            table.Columns.Add("DonGiaKhongThue", typeof(decimal));
            table.Columns.Add("TuNgay", typeof(string));
            table.Columns.Add("DenNgay", typeof(string));
            table.Columns.Add("TrangThai", typeof(Int32));
            table.Columns.Add("TrangThaiText", typeof(string));
            table.Columns.Add("NguoiDuyet", typeof(string));
            table.Columns.Add("NguoiXoa", typeof(string));
            table.Columns.Add("NgayTao", typeof(DateTime));
            table.Columns.Add("NguoiTao", typeof(string));
            table.Columns.Add("MoTa", typeof(string));
            table.Columns.Add("Loai", typeof(Int32));
            table.Columns.Add("hdForm", typeof(string));
        }
        void GetNVKD(DataTable table)
        {
            table.Columns.Add("ID", typeof(Guid));
            table.Columns.Add("IDHD", typeof(Guid));
            table.Columns.Add("IDNhanVien", typeof(Guid));
            table.Columns.Add("TenNhanVien", typeof(string));
            table.Columns.Add("TuNgay", typeof(string));
            table.Columns.Add("DenNgay", typeof(string));
            table.Columns.Add("TrangThai", typeof(Int32));
            table.Columns.Add("TrangThaiText", typeof(string));
            table.Columns.Add("NguoiDuyet", typeof(string));
            table.Columns.Add("NguoiXoa", typeof(string));
            table.Columns.Add("NgayTao", typeof(DateTime));
            table.Columns.Add("NguoiTao", typeof(string));
            table.Columns.Add("MoTa", typeof(string));
            table.Columns.Add("Loai", typeof(Int32));
            table.Columns.Add("hdForm", typeof(string));
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
            dlChiNhanhSearch.Items.Insert(0, new ListItem("Tất cả chi nhánh", ""));
        }
        protected void LoadNhaCungCap()
        {
            var query = db.sp_LoadNhaCungCap();
            dlNhaCungCap.DataSource = query;
            dlNhaCungCap.DataBind();
            dlNhaCungCap.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadLoaiNhaCungCap()
        {
            var query = (from p in db.tblNhomNhaCungCaps
                         select new
                         {
                             p.ID,
                             Ten = p.Name
                         }).OrderBy(p => p.Ten);
            dlLoaiNhaCungCap.DataSource = query;
            dlLoaiNhaCungCap.DataBind();
        }
        protected void LoadMacBeTong()
        {
            dlMacBeTong.Items.Clear();
            var query = (from p in db.tblLoaiVatLieus
                         where p.IDNhomVatLieu == new Guid("29FC56FA-D0DD-469C-A551-CDC8123C1189")
                         select new
                         {
                             p.ID,
                             Ten = p.TenLoaiVatLieu
                         }).OrderBy(p => p.Ten);
            dlMacBeTong.DataSource = query;
            dlMacBeTong.DataBind();
            dlMacBeTong.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadHinhThucBom()
        {
            dlHinhThucBom.Items.Clear();
            var query = (from p in db.tblHinhThucBoms
                         where p.ID != new Guid("2862C6F2-0AE1-499B-93B7-6E2B0AB46B83")
                         select new
                         {
                             p.ID,
                             Ten = p.TenHinhThucBom
                         }).OrderBy(p => p.Ten);
            dlHinhThucBom.DataSource = query;
            dlHinhThucBom.DataBind();
            dlHinhThucBom.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadNVKD()
        {
            dlNhanVien.Items.Clear();
            var query = (from p in db.tblNhanSus
                         where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenNhanVien
                         }).OrderBy(p => p.Ten);
            dlNhanVien.DataSource = query;
            dlNhanVien.DataBind();
            dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem(DataTable dt, DataTable dtbom)
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }

            if (s == "")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vatlieu.sp_HopDongBanBeTong_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDMacBeTong"].ToString()),
                        DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);

                    if (s != "")
                    {
                        break;
                    }
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua(DataTable dt, DataTable dtbom)
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (s == "" && hdID.Value != "")
            {
                vatlieu.sp_HopDongBanBeTong_CheckSuaHopDong(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), ref s);
            }
            if (s == "")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["TrangThai"].ToString() == "3")
                    {
                        vatlieu.sp_HopDongBanBeTong_CheckXoaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), ref s);
                    }
                    else if (dt.Rows[i]["TrangThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() == "0")
                    {
                        vatlieu.sp_HopDongBanBeTong_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDMacBeTong"].ToString()),
                            DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);
                    }
                    else if (dt.Rows[i]["TrangThai"].ToString() == "1" && dt.Rows[i]["Loai"].ToString() != "1")
                    {
                        vatlieu.sp_HopDongBanBeTong_CheckSuaChiTiet(new Guid(dt.Rows[i]["ID"].ToString()), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), new Guid(dt.Rows[i]["IDMacBeTong"].ToString()),
                            decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString()), decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString()),
                            DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())), dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString())), ref s);
                    }
                    if (s != "")
                    {
                        break;
                    }
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckThemChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongBanBeTong_CheckThemChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlMacBeTong),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            //if (txtDonGiaCoThue.Text == "" || decimal.Parse(txtDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtDonGiaKhongThue.Text == "" || decimal.Parse(txtDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongBanBeTong_CheckSuaChiTiet(new Guid(hdChiTiet.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlMacBeTong),
                    decimal.Parse(txtDonGiaCoThue.Text), decimal.Parse(txtDonGiaKhongThue.Text), DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckThemBomChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlHinhThucBom.SelectedValue == "")
            {
                s += " - Chọn hình thức bơm<br />";
            }
            //if (txtBomDonGiaCoThue.Text == "" || decimal.Parse(txtBomDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtBomDonGiaKhongThue.Text == "" || decimal.Parse(txtBomDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtBomTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongBanBeTong_CheckThemBomChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlHinhThucBom),
                    DateTime.Parse(GetNgayThang(txtBomTuNgay.Text)), txtBomDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtBomDenNgay.Text)), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaBomChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlHinhThucBom.SelectedValue == "")
            {
                s += " - Chọn hình thức bơm<br />";
            }
            //if (txtBomDonGiaCoThue.Text == "" || decimal.Parse(txtBomDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtBomDonGiaKhongThue.Text == "" || decimal.Parse(txtBomDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtBomTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongBanBeTong_CheckSuaBomChiTiet(new Guid(hdBomChiTiet.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlHinhThucBom),
                    decimal.Parse(txtBomDonGiaCoThue.Text), decimal.Parse(txtBomDonGiaKhongThue.Text), DateTime.Parse(GetNgayThang(txtBomTuNgay.Text)), txtBomDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtBomDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckThemKDChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (dlNhanVien.SelectedValue == "")
            {
                s += " - Chọn nhân viên kinh doanh<br />";
            }
            //if (txtKDDonGiaCoThue.Text == "" || decimal.Parse(txtKDDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtKDDonGiaKhongThue.Text == "" || decimal.Parse(txtKDDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtKDTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongBanBeTong_CheckThemKDChiTiet(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlNhanVien),
                    DateTime.Parse(GetNgayThang(txtKDTuNgay.Text)), txtKDDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtKDDenNgay.Text)), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSuaKDChiTiet()
        {
            string s = "";
            if (txtNgayThang.Text.Trim() == "")
            {
                s += " - Nhập ngày ký hợp đồng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (txtCongTrinh.Text.Trim() == "")
            {
                s += " - Nhập công trình<br />";
            }
            if (txtKDTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            //if (txtKDDonGiaCoThue.Text == "" || decimal.Parse(txtKDDonGiaCoThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá hóa đơn<br />";
            //}
            //if (txtKDDonGiaKhongThue.Text == "" || decimal.Parse(txtKDDonGiaKhongThue.Text) == 0)
            //{
            //    s += " - Nhập đơn giá thanh toán<br />";
            //}
            if (txtKDTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_HopDongBanBeTong_CheckSuaKDChiTiet(new Guid(hdKinhDoanh.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlNhanVien),
                    DateTime.Parse(GetNgayThang(txtKDTuNgay.Text)), txtKDDenNgay.Text.Trim() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtKDDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (Session["HopDongBanBeTong"] == null)
            {
                GetTable(dt);
            }
            else
            {
                dt = Session["HopDongBanBeTong"] as DataTable;
            }

            if (Session["HopDongBanBeTong_Bom"] == null)
            {
                GetBom(dtbom);
            }
            else
            {
                dtbom = Session["HopDongBanBeTong_Bom"] as DataTable;
            }

            if (Session["HopDongBanBeTong_NVKD"] == null)
            {
                GetNVKD(dtNVKD);
            }
            else
            {
                dtNVKD = Session["HopDongBanBeTong_NVKD"] as DataTable;
            }

            DateTime ngaykyhd = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
            DateTime ngayss = ngaykyhd;
            DateTime ngaymin = ngaykyhd;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["TuNgay"].ToString() != "" && DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString())) < ngayss)
                {
                    ngaymin = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                }
            }

            if (dt.Rows.Count == 0 && gvChiTiet.Rows.Count > 0)
            {
                Warning("Thông tin giá hợp đồng đã thay đổi.");
            }
            else if (dt.Rows.Count == 0)
            {
                Warning("Bạn chưa thiết lập thông tin giá bê tông");
            }
            else if (dt.Rows.Count > 0 && dt.Rows[0]["hdForm"].ToString() != hdForm.Value)
            {
                Warning("Bạn không được mở 2 tab trên cùng 1 trình duyệt.");
            }
            else if (ngaymin < ngaykyhd)
            {
                Warning("Ngày bắt đầu tính giá bán gạch không được nhỏ hơn ngày ký hợp đồng.");
            }
            else
            {
                if (hdID.Value == "")
                {
                    if (CheckThem(dt, dtbom) == "")
                    {
                        string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            Guid ID = Guid.NewGuid();
                            var HopDongBanBeTong = new tblHopDongBanBeTong()
                            {
                                ID = new Guid(hdChung.Value),
                                NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                                IDChiNhanh = GetDrop(dlChiNhanh),
                                IDNhaCungCap = GetDrop(dlNhaCungCap),
                                CongTrinh = txtCongTrinh.Text.Trim().ToUpper(),
                                //IDNhomVatLieu = GetDrop(dlNhomVatLieu),
                                //IDLoaiVatLieu = GetDrop(dlLoaiVatLieu),
                                //IDMacBeTong = GetDrop(dlMacBeTong),
                                //DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text),
                                //DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text),
                                //TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                                //DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                                TrangThai = 1,
                                TrangThaiText = "Chờ duyệt",
                                //STT = 0,
                                NguoiTao = Session["IDND"].ToString(),
                                NgayTao = DateTime.Now
                            };
                            db.tblHopDongBanBeTongs.InsertOnSubmit(HopDongBanBeTong);

                            List<tblHopDongBanBeTong_ChiTiet> them = new List<tblHopDongBanBeTong_ChiTiet>();

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["Loai"].ToString() != "10")
                                {
                                    var themchitiet = new tblHopDongBanBeTong_ChiTiet();
                                    themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                    themchitiet.IDHD = new Guid(dt.Rows[i]["IDHD"].ToString());
                                    themchitiet.MacBeTong = new Guid(dt.Rows[i]["IDMacBeTong"].ToString());
                                    themchitiet.DonGiaCoThue = decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString());
                                    themchitiet.DonGiaKhongThue = decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString());
                                    themchitiet.KhoiLuongDuKien = double.Parse(dt.Rows[i]["KhoiLuongDuKien"].ToString());
                                    themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                                    themchitiet.DenNgay = dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString()));
                                    themchitiet.TrangThai = int.Parse(dt.Rows[i]["TrangThai"].ToString());
                                    themchitiet.TrangThaiText = dt.Rows[i]["TrangThaiText"].ToString();
                                    themchitiet.NguoiDuyet = dt.Rows[i]["NguoiDuyet"].ToString();
                                    themchitiet.NguoiXoa = dt.Rows[i]["NguoiXoa"].ToString();
                                    themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                    themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                    themchitiet.MoTa = dt.Rows[i]["MoTa"].ToString();
                                    them.Add(themchitiet);
                                }
                            }
                            db.tblHopDongBanBeTong_ChiTiets.InsertAllOnSubmit(them);


                            List<tblHopDongBanBeTong_Bom> thembom = new List<tblHopDongBanBeTong_Bom>();

                            for (int i = 0; i < dtbom.Rows.Count; i++)
                            {
                                if (dtbom.Rows[i]["Loai"].ToString() != "10")
                                {
                                    var themchitiet = new tblHopDongBanBeTong_Bom();
                                    themchitiet.ID = new Guid(dtbom.Rows[i]["ID"].ToString());
                                    themchitiet.IDHD = new Guid(dtbom.Rows[i]["IDHD"].ToString());
                                    themchitiet.HinhThucBom = new Guid(dtbom.Rows[i]["IDHinhThucBom"].ToString());
                                    themchitiet.DonGiaCoThue = decimal.Parse(dtbom.Rows[i]["DonGiaCoThue"].ToString());
                                    themchitiet.DonGiaKhongThue = decimal.Parse(dtbom.Rows[i]["DonGiaKhongThue"].ToString());
                                    themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dtbom.Rows[i]["TuNgay"].ToString()));
                                    themchitiet.DenNgay = dtbom.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dtbom.Rows[i]["DenNgay"].ToString()));
                                    themchitiet.TrangThai = int.Parse(dtbom.Rows[i]["TrangThai"].ToString());
                                    themchitiet.TrangThaiText = dtbom.Rows[i]["TrangThaiText"].ToString();
                                    themchitiet.NguoiDuyet = dtbom.Rows[i]["NguoiDuyet"].ToString();
                                    themchitiet.NguoiXoa = dtbom.Rows[i]["NguoiXoa"].ToString();
                                    themchitiet.NgayTao = DateTime.Parse(dtbom.Rows[i]["NgayTao"].ToString());
                                    themchitiet.NguoiTao = dtbom.Rows[i]["NguoiTao"].ToString();
                                    themchitiet.MoTa = dtbom.Rows[i]["MoTa"].ToString();
                                    thembom.Add(themchitiet);
                                }
                            }
                            db.tblHopDongBanBeTong_Boms.InsertAllOnSubmit(thembom);

                            List<tblHopDongBanBeTong_NVKD> themnvkd = new List<tblHopDongBanBeTong_NVKD>();

                            for (int i = 0; i < dtNVKD.Rows.Count; i++)
                            {
                                if (dtNVKD.Rows[i]["Loai"].ToString() != "10")
                                {
                                    var themchitiet = new tblHopDongBanBeTong_NVKD();
                                    themchitiet.ID = new Guid(dtNVKD.Rows[i]["ID"].ToString());
                                    themchitiet.IDHD = new Guid(dtNVKD.Rows[i]["IDHD"].ToString());
                                    themchitiet.IDNhanVien = new Guid(dtNVKD.Rows[i]["IDNhanVien"].ToString());
                                    themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dtNVKD.Rows[i]["TuNgay"].ToString()));
                                    themchitiet.DenNgay = dtNVKD.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dtNVKD.Rows[i]["DenNgay"].ToString()));
                                    themchitiet.TrangThai = int.Parse(dtNVKD.Rows[i]["TrangThai"].ToString());
                                    themchitiet.TrangThaiText = dtNVKD.Rows[i]["TrangThaiText"].ToString();
                                    themchitiet.NguoiDuyet = dtNVKD.Rows[i]["NguoiDuyet"].ToString();
                                    themchitiet.NguoiXoa = dtNVKD.Rows[i]["NguoiXoa"].ToString();
                                    themchitiet.NgayTao = DateTime.Parse(dtNVKD.Rows[i]["NgayTao"].ToString());
                                    themchitiet.NguoiTao = dtNVKD.Rows[i]["NguoiTao"].ToString();
                                    themchitiet.MoTa = dtNVKD.Rows[i]["MoTa"].ToString();
                                    themnvkd.Add(themchitiet);
                                }
                            }
                            db.tblHopDongBanBeTong_NVKDs.InsertAllOnSubmit(themnvkd);

                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Lưu thành công.");
                        }
                    }
                }
                else
                {
                    if (CheckSua(dt, dtbom) == "")
                    {

                        string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            var query = (from p in db.tblHopDongBanBeTongs
                                         where p.ID == new Guid(hdID.Value)
                                         select p).FirstOrDefault();
                            if (query != null && query.ID != null)
                            {
                                DateTime dtcheck = DateTime.Parse(hdNgayTao.Value);
                                if (hdID.Value != ""
                                    && (
                                    dtcheck.Year != query.NgayTao.Value.Year
                                    || dtcheck.Month != query.NgayTao.Value.Month
                                    || dtcheck.Day != query.NgayTao.Value.Day
                                    || dtcheck.Hour != query.NgayTao.Value.Hour
                                    || dtcheck.Minute != query.NgayTao.Value.Minute
                                    || dtcheck.Second != query.NgayTao.Value.Second
                                    )
                                    )
                                {
                                    Warning("Thông tin hợp đồng đã bị sửa, xin vui lòng kiểm tra lại.");
                                }
                                else
                                {

                                    query.IDChiNhanh = GetDrop(dlChiNhanh);
                                    query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                                    query.CongTrinh = txtCongTrinh.Text.Trim().ToUpper();
                                    //query.IDNhomVatLieu = GetDrop(dlNhomVatLieu);
                                    //query.IDLoaiVatLieu = GetDrop(dlLoaiVatLieu);
                                    //query.IDMacBeTong = GetDrop(dlMacBeTong);
                                    //query.DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text);
                                    //query.DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text);
                                    //query.TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text));
                                    //query.DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text));
                                    query.TrangThai = 1;
                                    query.TrangThaiText = "Chờ duyệt";
                                    //query.STT = query.STT + 1;
                                    query.NguoiTao = Session["IDND"].ToString();
                                    query.NgayTao = DateTime.Now;

                                    List<tblHopDongBanBeTong_ChiTiet> xoa = new List<tblHopDongBanBeTong_ChiTiet>();
                                    List<tblHopDongBanBeTong_ChiTiet> them = new List<tblHopDongBanBeTong_ChiTiet>();

                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        if (dt.Rows[i]["Loai"].ToString() != "10")
                                        {
                                            var xoachitiet = (from p in db.tblHopDongBanBeTong_ChiTiets
                                                              where p.ID == new Guid(dt.Rows[i]["ID"].ToString())
                                                              select p).FirstOrDefault();
                                            if (xoachitiet != null && xoachitiet.ID != null)
                                            {
                                                xoa.Add(xoachitiet);
                                            }
                                            var themchitiet = new tblHopDongBanBeTong_ChiTiet();
                                            themchitiet.ID = new Guid(dt.Rows[i]["ID"].ToString());
                                            themchitiet.IDHD = new Guid(dt.Rows[i]["IDHD"].ToString());
                                            themchitiet.MacBeTong = new Guid(dt.Rows[i]["IDMacBeTong"].ToString());
                                            themchitiet.DonGiaCoThue = decimal.Parse(dt.Rows[i]["DonGiaCoThue"].ToString());
                                            themchitiet.DonGiaKhongThue = decimal.Parse(dt.Rows[i]["DonGiaKhongThue"].ToString());
                                            themchitiet.KhoiLuongDuKien = double.Parse(dt.Rows[i]["KhoiLuongDuKien"].ToString());
                                            themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dt.Rows[i]["TuNgay"].ToString()));
                                            themchitiet.DenNgay = dt.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dt.Rows[i]["DenNgay"].ToString()));
                                            themchitiet.TrangThai = int.Parse(dt.Rows[i]["TrangThai"].ToString());
                                            themchitiet.TrangThaiText = dt.Rows[i]["TrangThaiText"].ToString();
                                            themchitiet.NguoiDuyet = dt.Rows[i]["NguoiDuyet"].ToString();
                                            themchitiet.NguoiXoa = dt.Rows[i]["NguoiXoa"].ToString();
                                            themchitiet.NgayTao = DateTime.Parse(dt.Rows[i]["NgayTao"].ToString());
                                            themchitiet.NguoiTao = dt.Rows[i]["NguoiTao"].ToString();
                                            themchitiet.MoTa = dt.Rows[i]["MoTa"].ToString();
                                            them.Add(themchitiet);
                                        }
                                    }
                                    db.tblHopDongBanBeTong_ChiTiets.DeleteAllOnSubmit(xoa);
                                    db.tblHopDongBanBeTong_ChiTiets.InsertAllOnSubmit(them);

                                    List<tblHopDongBanBeTong_Bom> xoabom = new List<tblHopDongBanBeTong_Bom>();
                                    List<tblHopDongBanBeTong_Bom> thembom = new List<tblHopDongBanBeTong_Bom>();

                                    for (int i = 0; i < dtbom.Rows.Count; i++)
                                    {
                                        if (dtbom.Rows[i]["Loai"].ToString() != "10")
                                        {
                                            var xoachitiet = (from p in db.tblHopDongBanBeTong_Boms
                                                              where p.ID == new Guid(dtbom.Rows[i]["ID"].ToString())
                                                              select p).FirstOrDefault();
                                            if (xoachitiet != null && xoachitiet.ID != null)
                                            {
                                                xoabom.Add(xoachitiet);
                                            }
                                            var themchitiet = new tblHopDongBanBeTong_Bom();
                                            themchitiet.ID = new Guid(dtbom.Rows[i]["ID"].ToString());
                                            themchitiet.IDHD = new Guid(dtbom.Rows[i]["IDHD"].ToString());
                                            themchitiet.HinhThucBom = new Guid(dtbom.Rows[i]["IDHinhThucBom"].ToString());
                                            themchitiet.DonGiaCoThue = decimal.Parse(dtbom.Rows[i]["DonGiaCoThue"].ToString());
                                            themchitiet.DonGiaKhongThue = decimal.Parse(dtbom.Rows[i]["DonGiaKhongThue"].ToString());
                                            themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dtbom.Rows[i]["TuNgay"].ToString()));
                                            themchitiet.DenNgay = dtbom.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dtbom.Rows[i]["DenNgay"].ToString()));
                                            themchitiet.TrangThai = int.Parse(dtbom.Rows[i]["TrangThai"].ToString());
                                            themchitiet.TrangThaiText = dtbom.Rows[i]["TrangThaiText"].ToString();
                                            themchitiet.NguoiDuyet = dtbom.Rows[i]["NguoiDuyet"].ToString();
                                            themchitiet.NguoiXoa = dtbom.Rows[i]["NguoiXoa"].ToString();
                                            themchitiet.NgayTao = DateTime.Parse(dtbom.Rows[i]["NgayTao"].ToString());
                                            themchitiet.NguoiTao = dtbom.Rows[i]["NguoiTao"].ToString();
                                            themchitiet.MoTa = dtbom.Rows[i]["MoTa"].ToString();
                                            thembom.Add(themchitiet);
                                        }
                                    }
                                    db.tblHopDongBanBeTong_Boms.DeleteAllOnSubmit(xoabom);
                                    db.tblHopDongBanBeTong_Boms.InsertAllOnSubmit(thembom);

                                    List<tblHopDongBanBeTong_NVKD> xoanvkd = new List<tblHopDongBanBeTong_NVKD>();
                                    List<tblHopDongBanBeTong_NVKD> themnvkd = new List<tblHopDongBanBeTong_NVKD>();

                                    for (int i = 0; i < dtNVKD.Rows.Count; i++)
                                    {
                                        if (dtNVKD.Rows[i]["Loai"].ToString() != "10")
                                        {
                                            var xoachitiet = (from p in db.tblHopDongBanBeTong_NVKDs
                                                              where p.ID == new Guid(dtNVKD.Rows[i]["ID"].ToString())
                                                              select p).FirstOrDefault();
                                            if (xoachitiet != null && xoachitiet.ID != null)
                                            {
                                                xoanvkd.Add(xoachitiet);
                                            }
                                            var themchitiet = new tblHopDongBanBeTong_NVKD();
                                            themchitiet.ID = new Guid(dtNVKD.Rows[i]["ID"].ToString());
                                            themchitiet.IDHD = new Guid(dtNVKD.Rows[i]["IDHD"].ToString());
                                            themchitiet.IDNhanVien = new Guid(dtNVKD.Rows[i]["IDNhanVien"].ToString());
                                            themchitiet.TuNgay = DateTime.Parse(GetNgayThang(dtNVKD.Rows[i]["TuNgay"].ToString()));
                                            themchitiet.DenNgay = dtNVKD.Rows[i]["DenNgay"].ToString() == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(dtNVKD.Rows[i]["DenNgay"].ToString()));
                                            themchitiet.TrangThai = int.Parse(dtNVKD.Rows[i]["TrangThai"].ToString());
                                            themchitiet.TrangThaiText = dtNVKD.Rows[i]["TrangThaiText"].ToString();
                                            themchitiet.NguoiDuyet = dtNVKD.Rows[i]["NguoiDuyet"].ToString();
                                            themchitiet.NguoiXoa = dtNVKD.Rows[i]["NguoiXoa"].ToString();
                                            themchitiet.NgayTao = DateTime.Parse(dtNVKD.Rows[i]["NgayTao"].ToString());
                                            themchitiet.NguoiTao = dtNVKD.Rows[i]["NguoiTao"].ToString();
                                            themchitiet.MoTa = dtNVKD.Rows[i]["MoTa"].ToString();
                                            themnvkd.Add(themchitiet);
                                        }
                                    }
                                    db.tblHopDongBanBeTong_NVKDs.DeleteAllOnSubmit(xoanvkd);
                                    db.tblHopDongBanBeTong_NVKDs.InsertAllOnSubmit(themnvkd);

                                    db.SubmitChanges();

                                    lblTaoMoiHopDong_Click(sender, e);
                                    Search(1);
                                    Success("Sửa thành công");
                                }
                            }
                            else
                            {
                                Warning("Thông tin giá bán bê tông đã bị xóa.");
                                lblTaoMoiHopDong_Click(sender, e);
                            }
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlNhaCungCap.SelectedValue = "";
            txtCongTrinh.Text = "";
            dlMacBeTong.SelectedValue = "";
            dlHinhThucBom.SelectedValue = "";
            dlNhanVien.SelectedValue = "";
            hdID.Value = "";
            hdChung.Value = "";
            hdForm.Value = "";
            hdChiTiet.Value = "";
            hdBomChiTiet.Value = "";
            hdKinhDoanh.Value = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtBomDonGiaCoThue.Text = "";
            txtBomDonGiaKhongThue.Text = "";
            txtKhoiLuongDuKien.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
            txtBomTuNgay.Text = "";
            txtBomDenNgay.Text = "";
            txtKDTuNgay.Text = "";
            txtKDDenNgay.Text = "";
            Session["HopDongBanBeTong"] = null;
            Session["HopDongBanBeTong_Bom"] = null;
            Session["HopDongBanBeTong_NVKD"] = null;
            gvChiTiet.DataSource = null;
            gvChiTiet.DataBind();
            GVBomChiTiet.DataSource = null;
            GVBomChiTiet.DataBind();
            GVKDChiTiet.DataSource = null;
            GVKDChiTiet.DataBind();
            LoadKhachHangSearch();
        }
        protected void lbtSave_Click(object sender, EventArgs e)
        {
            string url = "";
            if (Session["HopDongBanBeTong"] == null)
            {
                GetTable(dt);
            }
            else
            {
                dt = Session["HopDongBanBeTong"] as DataTable;
            }

            if (hdChiTiet.Value == "")
            {
                if (CheckThemChiTiet() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (Check(dt, txtTuNgay.Text.Trim(), txtDenNgay.Text.Trim()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            if (hdID.Value == "" && hdChung.Value == "")
                                hdChung.Value = Guid.NewGuid().ToString();

                            dt.Rows.Add(Guid.NewGuid(),
                            new Guid(hdChung.Value),
                            GetDrop(dlMacBeTong),
                            dlMacBeTong.SelectedItem.Text.ToString(),
                            decimal.Parse(txtDonGiaCoThue.Text),
                            decimal.Parse(txtDonGiaKhongThue.Text),
                            double.Parse(txtKhoiLuongDuKien.Text),
                            txtTuNgay.Text.Trim(),
                            txtDenNgay.Text.Trim(),
                            1,
                            "Chờ duyệt",
                            "",
                            "",
                            DateTime.Now, Session["IDND"].ToString(),
                            "",
                            0,
                            hdForm.Value
                            );

                            lbtTaoGiaMoi_Click(sender, e);
                            gvChiTiet.DataSource = dt;
                            gvChiTiet.DataBind();
                            Session["HopDongBanBeTong"] = dt;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
            else
            {
                if (CheckSuaChiTiet() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckSua(dt, txtTuNgay.Text.Trim(), txtDenNgay.Text.Trim(), hdChiTiet.Value.ToUpper()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(hdChiTiet.Value)).FirstOrDefault();
                            if (dr != null)
                            {
                                dr["IDHD"] = new Guid(hdChung.Value);
                                dr["IDMacBeTong"] = GetDrop(dlMacBeTong);
                                dr["TenMacBeTong"] = dlMacBeTong.SelectedItem.Text.ToString();
                                dr["DonGiaCoThue"] = decimal.Parse(txtDonGiaCoThue.Text);
                                dr["DonGiaKhongThue"] = decimal.Parse(txtDonGiaKhongThue.Text);
                                dr["KhoiLuongDuKien"] = double.Parse(txtKhoiLuongDuKien.Text);
                                dr["TuNgay"] = txtTuNgay.Text;
                                dr["DenNgay"] = txtDenNgay.Text;
                                dr["TrangThai"] = 1;
                                dr["TrangThaiText"] = "Chờ duyệt";
                                dr["NguoiDuyet"] = "";
                                dr["NguoiXoa"] = "";
                                dr["NgayTao"] = DateTime.Now;
                                dr["NguoiTao"] = Session["IDND"].ToString();
                                dr["MoTa"] = "";
                                dr["Loai"] = 1;
                                dr["hdForm"] = hdForm.Value;
                            }

                            lbtTaoGiaMoi_Click(sender, e);
                            gvChiTiet.DataSource = dt;
                            gvChiTiet.DataBind();
                            Session["HopDongBanBeTong"] = dt;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
        }

        protected void lbtTaoGiaMoi_Click(object sender, EventArgs e)
        {
            dlMacBeTong.SelectedValue = "";
            hdChiTiet.Value = "";
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtKhoiLuongDuKien.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
        }
        protected void btnTaoMoiCongTrinh_Click(object sender, EventArgs e)
        {
            mpCongTrinh.Show();
            txtTenNhaCungCap.Text = "";
            txtSoDienThoai.Text = "";
            txtMaSoThue.Text = "";
            txtDiaChi.Text = "";
        }
        protected void btnLuuCongTrinh_Click(object sender, EventArgs e)
        {
            var checktrung = from p in db.tblNhaCungCaps
                             where p.TenNhaCungCap.ToUpper() == txtTenNhaCungCap.Text.Trim().ToUpper()
                             && p.IDNhomNhaCungCap == GetDrop(dlLoaiNhaCungCap)
                             select p;
            if (txtTenNhaCungCap.Text.Trim() == "")
            {
                GstGetMess("Nhập tên nhà cung cấp.", "");
                mpCongTrinh.Show();
            }
            else if (checktrung.Count() > 0)
            {
                GstGetMess("Tên nhà cung cấp theo loại nhà cung cấp đã tồn tại.", "");
                mpCongTrinh.Show();
            }
            else
            {
                Guid id = Guid.NewGuid();
                var insertcongtrinh = new tblNhaCungCap()
                {
                    ID = Guid.NewGuid(),
                    IDNhomNhaCungCap = GetDrop(dlLoaiNhaCungCap),
                    TenNhaCungCap = txtTenNhaCungCap.Text.Trim(),
                    SoDienThoai = txtSoDienThoai.Text.Trim(),
                    MaSoThue = txtMaSoThue.Text.Trim(),
                    DiaChi = txtDiaChi.Text.Trim(),
                    NgayTao = DateTime.Now,
                    NguoiTao = Session["IDND"].ToString()
                };
                db.tblNhaCungCaps.InsertOnSubmit(insertcongtrinh);
                db.SubmitChanges();
                LoadNhaCungCap();
                //dlNhaCungCap.SelectedValue = id.ToString().ToUpper();
            }
        }
        protected void gvChiTiet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();

            if (Session["HopDongBanBeTong"] == null)
            {
                GetTable(dt);
            }
            else
            {
                dt = Session["HopDongBanBeTong"] as DataTable;
            }

            DataRow dr = dt.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(id)).FirstOrDefault();
            if (dr != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlMacBeTong.SelectedValue = dr["IDMacBeTong"].ToString();
                        txtDonGiaCoThue.Text = string.Format("{0:N0}", dr["DonGiaCoThue"]);
                        txtDonGiaKhongThue.Text = string.Format("{0:N0}", dr["DonGiaKhongThue"]);
                        txtKhoiLuongDuKien.Text = string.Format("{0:N0}", dr["KhoiLuongDuKien"]);
                        txtTuNgay.Text = dr["TuNgay"].ToString();
                        txtDenNgay.Text = dr["DenNgay"].ToString();
                        hdChiTiet.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_HopDongBanBeTong_CheckXoaChiTiet(new Guid(id), ref thongbao);
                        if (thongbao != "")
                        {
                            Warning(thongbao);
                        }
                        else
                        {
                            var query = (from p in db.tblHopDongBanBeTong_ChiTiets
                                         where p.ID == new Guid(id)
                                         select p).FirstOrDefault();

                            if (query != null && query.ID != null)
                            {
                                dr["Loai"] = "3";
                                dr["TrangThai"] = "3";
                                dr["TrangThaiText"] = "Chờ duyệt xóa";

                                Success("Xóa thành công.");
                                gvChiTiet.DataSource = dt;
                                gvChiTiet.DataBind();
                                Session["HopDongBanBeTong"] = dt;
                                lbtTaoGiaMoi_Click(sender, e);
                            }
                            else
                            {
                                dt.Rows.Remove(dr);
                                dt.AcceptChanges();

                                Success("Xóa thành công.");
                                gvChiTiet.DataSource = dt;
                                gvChiTiet.DataBind();
                                Session["HopDongBanBeTong"] = dt;
                                lbtTaoGiaMoi_Click(sender, e);
                            }
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_HopDongBanBeTong_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá bán bê tông đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        protected void GVBomChiTiet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();

            if (Session["HopDongBanBeTong_Bom"] == null)
            {
                GetBom(dtbom);
            }
            else
            {
                dtbom = Session["HopDongBanBeTong_Bom"] as DataTable;
            }

            DataRow dr = dtbom.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(id)).FirstOrDefault();
            if (dr != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlHinhThucBom.SelectedValue = dr["IDHinhThucBom"].ToString();
                        txtBomDonGiaCoThue.Text = string.Format("{0:N0}", dr["DonGiaCoThue"]);
                        txtBomDonGiaKhongThue.Text = string.Format("{0:N0}", dr["DonGiaKhongThue"]);
                        txtBomTuNgay.Text = dr["TuNgay"].ToString();
                        txtBomDenNgay.Text = dr["DenNgay"].ToString();
                        hdBomChiTiet.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_HopDongBanBeTong_CheckXoaBomChiTiet(new Guid(id), ref thongbao);
                        if (thongbao != "")
                        {
                            Warning(thongbao);
                        }
                        else
                        {
                            var query = (from p in db.tblHopDongBanBeTong_Boms
                                         where p.ID == new Guid(id)
                                         select p).FirstOrDefault();

                            if (query != null && query.ID != null)
                            {
                                dr["Loai"] = "3";
                                dr["TrangThai"] = "3";
                                dr["TrangThaiText"] = "Chờ duyệt xóa";

                                Success("Xóa thành công.");
                                GVBomChiTiet.DataSource = dtbom;
                                GVBomChiTiet.DataBind();
                                Session["HopDongBanBeTong_Bom"] = dtbom;
                                lbtBomTaoGiaMoi_Click(sender, e);
                            }
                            else
                            {
                                dtbom.Rows.Remove(dr);
                                dtbom.AcceptChanges();

                                Success("Xóa thành công.");
                                GVBomChiTiet.DataSource = dtbom;
                                GVBomChiTiet.DataBind();
                                Session["HopDongBanBeTong_Bom"] = dtbom;
                                lbtBomTaoGiaMoi_Click(sender, e);
                            }
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_HopDongBanBeTong_LichSuBom(new Guid(id));
                    gvLichSuBom.DataSource = view;
                    gvLichSuBom.DataBind();
                    mpLichSuBom.Show();
                }
            }
            else
            {
                Warning("Thông tin giá bán bê tông đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void lbtBomSave_Click(object sender, EventArgs e)
        {
            string url = "";
            if (Session["HopDongBanBeTong_Bom"] == null)
            {
                GetBom(dtbom);
            }
            else
            {
                dtbom = Session["HopDongBanBeTong_Bom"] as DataTable;
            }

            if (hdBomChiTiet.Value == "")
            {
                if (CheckThemBomChiTiet() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckBom(dtbom, txtBomTuNgay.Text.Trim(), txtBomDenNgay.Text.Trim()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            if (hdID.Value == "" && hdChung.Value == "")
                                hdChung.Value = Guid.NewGuid().ToString();

                            dtbom.Rows.Add(Guid.NewGuid(),
                            new Guid(hdChung.Value),
                            GetDrop(dlHinhThucBom),
                            dlHinhThucBom.SelectedItem.Text.ToString(),
                            decimal.Parse(txtBomDonGiaCoThue.Text),
                            decimal.Parse(txtBomDonGiaKhongThue.Text),
                            txtBomTuNgay.Text.Trim(),
                            txtBomDenNgay.Text.Trim(),
                            1,
                            "Chờ duyệt",
                            "",
                            "",
                            DateTime.Now, Session["IDND"].ToString(),
                            "",
                            0,
                            hdForm.Value
                            );

                            lbtBomTaoGiaMoi_Click(sender, e);
                            GVBomChiTiet.DataSource = dtbom;
                            GVBomChiTiet.DataBind();
                            Session["HopDongBanBeTong_Bom"] = dtbom;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
            else
            {
                if (CheckSuaBomChiTiet() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckSuaBom(dtbom, txtBomTuNgay.Text.Trim(), txtBomDenNgay.Text.Trim(), hdBomChiTiet.Value.ToUpper()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            DataRow dr = dtbom.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(hdBomChiTiet.Value)).FirstOrDefault();
                            if (dr != null)
                            {
                                dr["IDHD"] = new Guid(hdChung.Value);
                                dr["IDHinhThucBom"] = GetDrop(dlHinhThucBom);
                                dr["TenHinhThucBom"] = dlHinhThucBom.SelectedItem.Text.ToString();
                                dr["DonGiaCoThue"] = decimal.Parse(txtBomDonGiaCoThue.Text);
                                dr["DonGiaKhongThue"] = decimal.Parse(txtBomDonGiaKhongThue.Text);
                                dr["TuNgay"] = txtBomTuNgay.Text;
                                dr["DenNgay"] = txtBomDenNgay.Text;
                                dr["TrangThai"] = 1;
                                dr["TrangThaiText"] = "Chờ duyệt";
                                dr["NguoiDuyet"] = "";
                                dr["NguoiXoa"] = "";
                                dr["NgayTao"] = DateTime.Now;
                                dr["NguoiTao"] = Session["IDND"].ToString();
                                dr["MoTa"] = "";
                                dr["Loai"] = 1;
                                dr["hdForm"] = hdForm.Value;
                            }

                            lbtBomTaoGiaMoi_Click(sender, e);
                            GVBomChiTiet.DataSource = dtbom;
                            GVBomChiTiet.DataBind();
                            Session["HopDongBanBeTong_Bom"] = dtbom;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
        }

        protected void lbtBomTaoGiaMoi_Click(object sender, EventArgs e)
        {
            dlHinhThucBom.SelectedValue = "";
            hdBomChiTiet.Value = "";
            txtBomDonGiaCoThue.Text = "";
            txtBomDonGiaKhongThue.Text = "";
            txtBomTuNgay.Text = "";
            txtBomDenNgay.Text = "";
        }

        protected void GVKDChiTiet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();

            if (Session["HopDongBanBeTong_NVKD"] == null)
            {
                GetNVKD(dtNVKD);
            }
            else
            {
                dtNVKD = Session["HopDongBanBeTong_NVKD"] as DataTable;
            }

            DataRow dr = dtNVKD.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(id)).FirstOrDefault();
            if (dr != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlNhanVien.SelectedValue = dr["IDNhanVien"].ToString();
                        txtKDTuNgay.Text = dr["TuNgay"].ToString();
                        txtKDDenNgay.Text = dr["DenNgay"].ToString();
                        hdKinhDoanh.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_HopDongBanBeTong_CheckXoaKDChiTiet(new Guid(id), ref thongbao);
                        if (thongbao != "")
                        {
                            Warning(thongbao);
                        }
                        else
                        {
                            var query = (from p in db.tblHopDongBanBeTong_NVKDs
                                         where p.ID == new Guid(id)
                                         select p).FirstOrDefault();

                            if (query != null && query.ID != null)
                            {
                                dr["Loai"] = "3";
                                dr["TrangThai"] = "3";
                                dr["TrangThaiText"] = "Chờ duyệt xóa";

                                Success("Xóa thành công.");
                                GVKDChiTiet.DataSource = dtNVKD;
                                GVKDChiTiet.DataBind();
                                Session["HopDongBanBeTong_NVKD"] = dtNVKD;
                                lbtKDTaoMoi_Click(sender, e);
                            }
                            else
                            {
                                dtNVKD.Rows.Remove(dr);
                                dtNVKD.AcceptChanges();

                                Success("Xóa thành công.");
                                GVKDChiTiet.DataSource = dtNVKD;
                                GVKDChiTiet.DataBind();
                                Session["HopDongBanBeTong_NVKD"] = dtNVKD;
                                lbtKDTaoMoi_Click(sender, e);
                            }
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_HopDongBanBeTong_LichSuKD(new Guid(id));
                    gvLichSuKD.DataSource = view;
                    gvLichSuKD.DataBind();
                    mpLichSuKD.Show();
                }
            }
            else
            {
                Warning("Thông tin nhân viên kinh doanh đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void lbtKDSave_Click(object sender, EventArgs e)
        {
            string url = "";
            if (Session["HopDongBanBeTong_NVKD"] == null)
            {
                GetNVKD(dtNVKD);
            }
            else
            {
                dtNVKD = Session["HopDongBanBeTong_NVKD"] as DataTable;
            }

            if (hdKinhDoanh.Value == "")
            {
                if (CheckThemKDChiTiet() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckKD(dtNVKD, txtKDTuNgay.Text.Trim(), txtKDDenNgay.Text.Trim()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            if (hdID.Value == "" && hdChung.Value == "")
                                hdChung.Value = Guid.NewGuid().ToString();

                            dtNVKD.Rows.Add(Guid.NewGuid(),
                            new Guid(hdChung.Value),
                            GetDrop(dlNhanVien),
                            dlNhanVien.SelectedItem.Text.ToString(),
                            txtKDTuNgay.Text.Trim(),
                            txtKDDenNgay.Text.Trim(),
                            1,
                            "Chờ duyệt",
                            "",
                            "",
                            DateTime.Now, Session["IDND"].ToString(),
                            "",
                            0,
                            hdForm.Value
                            );

                            lbtKDTaoMoi_Click(sender, e);
                            GVKDChiTiet.DataSource = dtNVKD;
                            GVKDChiTiet.DataBind();
                            Session["HopDongBanBeTong_NVKD"] = dtNVKD;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
            else
            {
                if (CheckSuaKDChiTiet() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckSuaKD(dtNVKD, txtKDTuNgay.Text.Trim(), txtKDDenNgay.Text.Trim(), hdKinhDoanh.Value.ToUpper()) == "")
                        {
                            if (hdForm.Value == "")
                            {
                                hdForm.Value = Guid.NewGuid().ToString();
                            }

                            DataRow dr = dtNVKD.AsEnumerable().Where(dv => dv.Field<Guid>("ID") == new Guid(hdKinhDoanh.Value)).FirstOrDefault();
                            if (dr != null)
                            {
                                dr["IDHD"] = new Guid(hdChung.Value);
                                dr["IDNhanVien"] = GetDrop(dlNhanVien);
                                dr["TenNhanVien"] = dlNhanVien.SelectedItem.Text.ToString();
                                dr["TuNgay"] = txtKDTuNgay.Text;
                                dr["DenNgay"] = txtKDDenNgay.Text;
                                dr["TrangThai"] = 1;
                                dr["TrangThaiText"] = "Chờ duyệt";
                                dr["NguoiDuyet"] = "";
                                dr["NguoiXoa"] = "";
                                dr["NgayTao"] = DateTime.Now;
                                dr["NguoiTao"] = Session["IDND"].ToString();
                                dr["MoTa"] = "";
                                dr["Loai"] = 1;
                                dr["hdForm"] = hdForm.Value;
                            }

                            lbtKDTaoMoi_Click(sender, e);
                            GVKDChiTiet.DataSource = dtNVKD;
                            GVKDChiTiet.DataBind();
                            Session["HopDongBanBeTong_NVKD"] = dtNVKD;
                            Success("Lưu thành công.");
                        }
                    }
                }
            }
        }

        protected void lbtKDTaoMoi_Click(object sender, EventArgs e)
        {
            dlNhanVien.SelectedValue = "";
            hdKinhDoanh.Value = "";
            txtKDTuNgay.Text = "";
            txtKDDenNgay.Text = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblHopDongBanBeTongs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        txtCongTrinh.Text = query.CongTrinh;
                        hdID.Value = id;
                        hdChung.Value = id;
                        hdNgayTao.Value = query.NgayTao.ToString();
                        hdForm.Value = Guid.NewGuid().ToString();
                        LoadChiTiet();
                        LoadChiTietBom();
                        LoadChiTietKD();
                    }
                }

                else if (e.CommandName == "duplicate")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                        txtCongTrinh.Text = query.CongTrinh;
                        hdDuplicate.Value = query.ID.ToString();
                        hdChung.Value = Guid.NewGuid().ToString();
                        hdNgayTao.Value = query.NgayTao.ToString();
                        hdForm.Value = Guid.NewGuid().ToString();

                        LoadChiTietDuplicate();
                        LoadChiTietBomDuplicate();
                        LoadChiTietKDDuplicate();
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_HopDongBanBeTong_CheckXoa(query.ID, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblHopDongBanBeTong_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblHopDongBanBeTongs.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    //var view = vatlieu.sp_HopDongBanBeTong_LichSu(new Guid(id));
                    //gvLichSu.DataSource = view;
                    //gvLichSu.DataBind();
                    //mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá bán bê tông đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }

        private string Check(DataTable tableinsert, string tungay, string denngay)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDMacBeTong") == GetDrop(dlMacBeTong) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDMacBeTong") == GetDrop(dlMacBeTong) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá";
                else _err = "";
            }
            if (_err != "")
            {
                Warning(_err);
            }

            return _err;

        }
        private string CheckSua(DataTable tableinsert, string tungay, string denngay, string ids)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDMacBeTong") == GetDrop(dlMacBeTong) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDMacBeTong") == GetDrop(dlMacBeTong) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá";
                else _err = "";
            }

            if (_err != "")
            {
                Warning(_err);
            }

            return _err;
        }
        private string CheckBom(DataTable tableinsert, string tungay, string denngay)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 hình thức bơm";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 hình thức bơm";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 hình thức bơm";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 hình thức bơm";
                else _err = "";
            }
            if (_err != "")
            {
                Warning(_err);
            }

            return _err;

        }
        private string CheckSuaBom(DataTable tableinsert, string tungay, string denngay, string ids)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDHinhThucBom") == GetDrop(dlHinhThucBom) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 hình thức bơm";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 hình thức bơm";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 hình thức bơm";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 loại giá cho cùng 1 hình thức bơm";
                else _err = "";
            }

            if (_err != "")
            {
                Warning(_err);
            }

            return _err;
        }

        private string CheckKD(DataTable tableinsert, string tungay, string denngay)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                else _err = "";
            }
            if (_err != "")
            {
                Warning(_err);
            }

            return _err;

        }
        private string CheckSuaKD(DataTable tableinsert, string tungay, string denngay, string ids)
        {
            string _err = "";

            DateTime dt_tungay = DateTime.Parse(GetNgayThang(tungay));
            DateTime dt_denngay = denngay.Equals("") ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(denngay));

            var dr_min =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDNhanVien") == GetDrop(dlNhanVien) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) <= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderByDescending(b => b.TuNgay)).FirstOrDefault();

            var dr_max =
                (tableinsert.AsEnumerable()
                    .Where(p => p.Field<Guid>("IDNhanVien") == GetDrop(dlNhanVien) &&
                                p.Field<Guid>("ID") != new Guid(ids) &&
                                DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))) >= dt_tungay
                    ).Select(p => new
                    {
                        TuNgay = DateTime.Parse(GetNgayThang(p.Field<string>("TuNgay"))),
                        DenNgay = p.Field<string>("DenNgay").ToString().Equals("")
                                ? DateTime.MaxValue : DateTime.Parse(GetNgayThang(p.Field<string>("DenNgay")))
                    }).OrderBy(b => b.TuNgay)).FirstOrDefault();

            if (dr_min != null)
            {
                if (dr_max == null)//chi co min
                {
                    if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay))
                        _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                    else if (dr_min.DenNgay > dt_tungay)
                        _err = "Ngày trước đó chưa tồn tại ngày kết thúc";
                    else
                        _err = "";
                }
                else if (dr_max != null)//ca min ca max
                {
                    if (dt_denngay.Equals(DateTime.MaxValue))
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay))
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                    else
                    {
                        if (dt_tungay.Equals(dr_min.TuNgay) || dt_tungay.Equals(dr_min.DenNgay) || dt_tungay.Equals(dr_max.TuNgay) || dt_tungay.Equals(dr_max.DenNgay) ||
                            dt_denngay.Equals(dr_min.TuNgay) || dt_denngay.Equals(dr_min.DenNgay) || dt_denngay.Equals(dr_max.TuNgay) || dt_denngay.Equals(dr_max.DenNgay)
                            )
                            _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                        else if (dt_tungay > dr_min.DenNgay)
                        {
                            if (dt_denngay > dr_max.TuNgay)
                                _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                            else _err = "";
                        }
                    }
                }
            }
            else if (dr_max != null)//chi co max
            {
                if (dt_denngay > dr_max.TuNgay)
                    _err = "Bạn phải nhập ngày kết thúc nhỏ hơn " + dr_max.TuNgay.ToString("dd/MM/yyyy");
                else if (dt_denngay.Equals(dr_max.TuNgay))
                    _err = "Trong cùng một khoảng thời gian, không thể tồn tại 2 nhân viên kinh doanh";
                else _err = "";
            }

            if (_err != "")
            {
                Warning(_err);
            }

            return _err;
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            var query = vatlieu.sp_HopDongBanBeTong_Search(dlChiNhanhSearch.SelectedValue, dlKhachHangSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlKhachHangSearch), 20, page);

            GV.DataSource = query;
            GV.DataBind();
        }
        private void LoadChiTiet()
        {
            List<sp_HopDongBanBeTong_ChiTietSearchResult> query = vatlieu.sp_HopDongBanBeTong_ChiTietSearch(new Guid(hdID.Value), hdForm.Value).ToList();
            GetTable(dt);
            foreach (var item in query)
            {
                dt.Rows.Add(
                                item.ID,
                                item.IDHD,
                                item.IDMacBeTong,
                                item.TenMacBeTong,
                                item.DonGiaCoThue,
                                item.DonGiaKhongThue,
                                item.KhoiLuongDuKien,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                item.TrangThai,
                                item.TrangThaiText,
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                item.Loai,
                                item.hdForm
                            );
            }
            gvChiTiet.DataSource = query;
            gvChiTiet.DataBind();
            Session["HopDongBanBeTong"] = dt;
        }
        private void LoadChiTietBom()
        {
            List<sp_HopDongBanBeTong_ChiTietBomSearchResult> query = vatlieu.sp_HopDongBanBeTong_ChiTietBomSearch(new Guid(hdID.Value), hdForm.Value).ToList();
            GetBom(dtbom);
            foreach (var item in query)
            {
                dtbom.Rows.Add(
                                item.ID,
                                item.IDHD,
                                item.IDHinhThucBom,
                                item.TenHinhThucBom,
                                item.DonGiaCoThue,
                                item.DonGiaKhongThue,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                item.TrangThai,
                                item.TrangThaiText,
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                item.Loai,
                                item.hdForm
                            );
            }
            GVBomChiTiet.DataSource = query;
            GVBomChiTiet.DataBind();
            Session["HopDongBanBeTong_Bom"] = dtbom;
        }
        private void LoadChiTietKD()
        {
            List<sp_HopDongBanBeTong_ChiTietKDSearchResult> query = vatlieu.sp_HopDongBanBeTong_ChiTietKDSearch(new Guid(hdID.Value), hdForm.Value).ToList();
            GetNVKD(dtNVKD);
            foreach (var item in query)
            {
                dtNVKD.Rows.Add(
                                item.ID,
                                item.IDHD,
                                item.IDNhanVien,
                                item.TenNhanVien,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                item.TrangThai,
                                item.TrangThaiText,
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                item.Loai,
                                item.hdForm
                            );
            }
            GVKDChiTiet.DataSource = query;
            GVKDChiTiet.DataBind();
            Session["HopDongBanBeTong_NVKD"] = dtNVKD;
        }

        private void LoadChiTietDuplicate()
        {
            List<sp_HopDongBanBeTong_ChiTietSearchResult> query = vatlieu.sp_HopDongBanBeTong_ChiTietSearch(new Guid(hdDuplicate.Value), hdForm.Value).ToList();
            GetTable(dt);
            foreach (var item in query)
            {
                dt.Rows.Add(
                                Guid.NewGuid(),
                                new Guid(hdChung.Value),
                                item.IDMacBeTong,
                                item.TenMacBeTong,
                                item.DonGiaCoThue,
                                item.DonGiaKhongThue,
                                item.KhoiLuongDuKien,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                1,
                                "Chờ duyệt",
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                0,
                                item.hdForm
                            );
            }
            gvChiTiet.DataSource = dt;
            gvChiTiet.DataBind();
            Session["HopDongBanBeTong"] = dt;
        }
        private void LoadChiTietBomDuplicate()
        {
            List<sp_HopDongBanBeTong_ChiTietBomSearchResult> query = vatlieu.sp_HopDongBanBeTong_ChiTietBomSearch(new Guid(hdDuplicate.Value), hdForm.Value).ToList();
            GetBom(dtbom);
            foreach (var item in query)
            {
                dtbom.Rows.Add(
                                Guid.NewGuid(),
                                new Guid(hdChung.Value),
                                item.IDHinhThucBom,
                                item.TenHinhThucBom,
                                item.DonGiaCoThue,
                                item.DonGiaKhongThue,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                1,
                                "Chờ duyệt",
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                0,
                                item.hdForm
                            );
            }
            GVBomChiTiet.DataSource = dtbom;
            GVBomChiTiet.DataBind();
            Session["HopDongBanBeTong_Bom"] = dtbom;
        }
        private void LoadChiTietKDDuplicate()
        {
            List<sp_HopDongBanBeTong_ChiTietKDSearchResult> query = vatlieu.sp_HopDongBanBeTong_ChiTietKDSearch(new Guid(hdDuplicate.Value), hdForm.Value).ToList();
            GetNVKD(dtNVKD);
            foreach (var item in query)
            {
                dtNVKD.Rows.Add(
                                Guid.NewGuid(),
                                new Guid(hdChung.Value),
                                item.IDNhanVien,
                                item.TenNhanVien,
                                item.TuNgay,
                                item.DenNgay == null ? "" : item.DenNgay,
                                1,
                                "Chờ duyệt",
                                item.NguoiDuyet,
                                item.NguoiXoa,
                                item.NgayTao,
                                item.NguoiTao,
                                item.MoTa,
                                0,
                                item.hdForm
                            );
            }
            GVKDChiTiet.DataSource = dtNVKD;
            GVKDChiTiet.DataBind();
            Session["HopDongBanBeTong_NVKD"] = dtNVKD;
        }


        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadKhachHangSearch();
        }
        void LoadKhachHangSearch()
        {
            dlKhachHangSearch.Items.Clear();
            var query = vatlieu.sp_HopDongBanBeTong_LoadNhaCungCapSearch(GetDrop(dlChiNhanhSearch));
            dlKhachHangSearch.DataSource = query;
            dlKhachHangSearch.DataBind();
            dlKhachHangSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }

    }
}