﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public class clsPhanQuyen
    {
        private DBDataContext db = new DBDataContext();
        public string CheckQuyenTruyCap(string IDNguoiDung, string Tag, ref string url)
        {
            string s = "";
            url = "";
            sp_PhanQuyen_CheckTruyCapResult query = db.sp_PhanQuyen_CheckTruyCap(IDNguoiDung, Tag).FirstOrDefault();
            s = query.ThongBao;
            url = query.Url;
            return s;
        }
        public string CheckQuyenThaoTac(Guid IDChiNhanh, string IDNguoiDung, int ThaoTac, string Tag, ref string url)
        {
            //idthaotac 1 them, 2 sua, 3 xóa, 4 xem, 5 xuat
            string s = "";
            url = "";
            var query = db.sp_PhanQuyen_CheckQuyenThaoTac(IDChiNhanh, IDNguoiDung, ThaoTac, Tag).FirstOrDefault();
            s = query.ThongBao;
            url = query.Url;
            return s;
        }
        public string CheckQuyenThaoTacDanhMuc(string IDNguoiDung, int ThaoTac, string Tag, ref string url)
        {
            //idthaotac 1 them, 2 sua, 3 xóa, 4 xem, 5 xuat
            string s = "";
            var query = db.sp_PhanQuyen_CheckQuyenThaoTacDanhMuc(IDNguoiDung, ThaoTac, Tag).FirstOrDefault();
            s = query.ThongBao;
            url = query.Url;
            return s;
        }
        public Guid GetDrop(DropDownList Dl)
        {
            Guid ID = new Guid();
            if (Dl.SelectedValue != "")
            {
                ID = new Guid(Dl.SelectedValue);
            }
            else
            {
                ID = new Guid("00000000-0000-0000-0000-000000000000");
            }
            return ID;
        }
        public Guid GetDrop(string Dl)
        {
            Guid ID = new Guid();
            if (Dl == "")
            {
                ID = new Guid("00000000-0000-0000-0000-000000000000");
            }
            else
            {
                ID = new Guid(Dl);
            }
            return ID;
        }
        public string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        public string IsNullNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "1/1/1990";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "6/6/2079";
                }
            }
            catch
            { return ""; }
        }
        public DateTime IsNull(string ns)
        {
            try
            {
                return ns == null ? DateTime.Parse("6/6/2079") : ns == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(ns);
            }
            catch
            {
                return DateTime.Parse("6/6/2079");
            }
        }
    }
}