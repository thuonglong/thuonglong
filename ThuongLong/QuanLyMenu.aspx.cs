﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class QuanLyMenu : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])) ||
            //       string.IsNullOrEmpty(Convert.ToString(Session["IDDN"])))
            //    Response.Redirect("login.aspx");
            //else
            //{
            if (!IsPostBack)
            {
                //string url = "";
                //string thongbao = pq.TruyCapDanhMuc(new Guid(Session["IDDN"].ToString()), Session["IDND"].ToString(), "frmNhomThietBi", ref url);
                //if (thongbao != "")
                //{
                //    if (url != "")
                //    {
                //        GstGetMess(thongbao, url);
                //    }
                //    else
                //    {
                //        Warning(thongbao);
                //    }
                //}
                //else
                //{
                //    hdPage.Value = "1";
                //}
                LoadMenuCha();
            }
            //}
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            Search(1);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        string Mahoa(string password)
        {
            MD5 mh = MD5.Create();
            //Chuyển kiểu chuổi thành kiểu byte
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(password);
            //mã hóa chuỗi đã chuyển
            byte[] hash = mh.ComputeHash(inputBytes);
            //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        void LoadMenuCha()
        {
            var query = (from p in db.tblMenus
                         where p.IDCha == null
                         select new
                         {
                             p.ID,
                             p.Ten
                         }).OrderBy(p => p.Ten);
            dlIDCha.Items.Clear();
            dlIDCha.DataSource = query;
            dlIDCha.DataBind();
            dlIDCha.Items.Insert(0, new ListItem("Chọn menu cha", ""));
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hdID.Value == "")
            {
                var Menu = new tblMenu()
                {
                    ID = Guid.NewGuid(),
                    Ten = txtTen.Text.Trim(),
                    IDCha = dlIDCha.SelectedValue == "" ? (Guid?)null : GetDrop(dlIDCha),
                    ViTri = int.Parse(txtViTri.Text),
                    Link = txtLink.Text.Trim().ToLower(),
                    Tag = txtTag.Text.Trim(),
                    TrangThai = ckTrangThai.Checked
                };
                db.tblMenus.InsertOnSubmit(Menu);
                db.SubmitChanges();
                btnCancel_Click(sender, e);
                Search(int.Parse(hdPage.Value));
                Success("Lưu thành công.");
            }
            else
            {
                var query = (from p in db.tblMenus
                             where p.ID == new Guid(hdID.Value)
                             select p).FirstOrDefault();
                if (query != null)
                {
                    query.Ten = txtTen.Text.Trim();
                    query.IDCha = dlIDCha.SelectedValue == "" ? (Guid?)null : GetDrop(dlIDCha);
                    query.ViTri = int.Parse(txtViTri.Text);
                    query.Link = txtLink.Text.Trim().ToLower();
                    query.Tag = txtTag.Text.Trim();
                    query.TrangThai = ckTrangThai.Checked;

                    db.SubmitChanges();
                    btnCancel_Click(sender, e);
                    Search(int.Parse(hdPage.Value));
                    Success("Sửa thành công");
                }
                else
                {
                    Warning("Thông tin tài khoản không tồn tại.");
                    btnCancel_Click(sender, e);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtTen.Text = "";
            //dlIDCha.SelectedValue = "";
            txtViTri.Text = "";
            txtLink.Text = "";
            txtTag.Text = "";
            ckTrangThai.Checked = true;
            txtSearch.Text = "";
            btnSave.Text = "Lưu";
            hdID.Value = "";
        }
        void Search(int page)
        {
            GV.DataSource = db.sp_Menu_Search(txtSearch.Text.Trim(), 10, page);
            GV.DataBind();
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblMenus
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null)
            {
                if (e.CommandName == "Sua")
                {
                    txtTen.Text = query.Ten;
                    dlIDCha.SelectedValue = query.IDCha.ToString();
                    txtViTri.Text = query.ViTri.ToString();
                    txtLink.Text = query.Link.ToString();
                    txtTag.Text = query.Tag.ToString();
                    ckTrangThai.Checked = query.TrangThai.Value;
                    hdID.Value = query.ID.ToString();
                    btnSave.Text = "Cập nhật";
                }
                else if (e.CommandName == "Xoa")
                {
                    db.tblMenus.DeleteOnSubmit(query);
                    db.SubmitChanges();
                    Success("Xóa thành công");
                    btnCancel_Click(sender, e);
                    Search(int.Parse(hdPage.Value));
                }
                else if (e.CommandName == "Kích hoạt")
                {
                    if (query.TrangThai == false)
                    {
                        query.TrangThai = true;
                        db.SubmitChanges();
                        Success("Đã kích hoạt");
                    }
                    else
                    {
                        Success("Đã kích hoạt");
                    }
                    Search(int.Parse(hdPage.Value));
                }
                else if (e.CommandName == "Khóa")
                {
                    if (query.TrangThai == true)
                    {
                        query.TrangThai = false;
                        db.SubmitChanges();
                        Success("Đã khóa");
                    }
                    else
                    {
                        Success("Đã khóa");
                    }
                    Search(int.Parse(hdPage.Value));
                }
            }
            else
            {
                Warning("Thông tin menu không tồn tại");
                btnCancel_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            Search(1);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            int CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            int CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
    }
}