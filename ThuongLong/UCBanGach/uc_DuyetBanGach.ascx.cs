﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCBanGach
{
    public partial class uc_DuyetBanGach : System.Web.UI.UserControl
    {
        private string frmName = "frmBanGach";
        private static int PageNumber = 20;
        private static int CurrentPage = 1;
        private DBDataContext db = new DBDataContext();
        private VatLieuDataContext vatlieu = new VatLieuDataContext();
        private clsPhanQuyen phanquyen = new clsPhanQuyen();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    LoadChiNhanh();
                    //  IDDN = new Guid(Session["IDDN"].ToString());
                }
            }
        }

        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            //dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        public DateTime GetLastDayOfMonth(int iMonth, int iYear)
        {
            DateTime dtResult = new DateTime(iYear, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-1);
            return dtResult;
        }

        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        protected void btnDong_Click(object sender, EventArgs e)
        {
            mpDuyet.Hide();
        }
        private void Search(int page)
        {
            System.Data.Linq.ISingleResult<sp_BanGach_ListDuyetResult> dt = vatlieu.sp_BanGach_ListDuyet(GetDrop(dlChiNhanh), 1, PageNumber, page);
            GV.DataSource = dt;
            GV.DataBind();


            for (int rowIndex = GV.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = GV.Rows[rowIndex];
                GridViewRow previousRow = GV.Rows[rowIndex + 1];
                var lbtSua1 = (LinkButton)row.FindControl("lbtSua");
                var lbtSua2 = (LinkButton)previousRow.FindControl("lbtSua");
                var lbtHuy1 = (LinkButton)row.FindControl("lbtHuy");
                var lbtHuy2 = (LinkButton)previousRow.FindControl("lbtHuy");
                if (
                    lbtSua1.CommandArgument.Equals(lbtSua2.CommandArgument)
                    && lbtHuy1.CommandArgument.Equals(lbtHuy2.CommandArgument)

                    && row.Cells[2].Text.Equals(previousRow.Cells[2].Text)
                    && row.Cells[3].Text.Equals(previousRow.Cells[3].Text)
                    && row.Cells[4].Text.Equals(previousRow.Cells[4].Text)
                    && row.Cells[5].Text.Equals(previousRow.Cells[5].Text)
                    && row.Cells[6].Text.Equals(previousRow.Cells[6].Text)
                    )
                {
                    row.Cells[0].RowSpan = previousRow.Cells[0].RowSpan < 2 ? 2 : previousRow.Cells[0].RowSpan + 1;
                    row.Cells[1].RowSpan = previousRow.Cells[1].RowSpan < 2 ? 2 : previousRow.Cells[1].RowSpan + 1;

                    row.Cells[2].RowSpan = previousRow.Cells[2].RowSpan < 2 ? 2 : previousRow.Cells[2].RowSpan + 1;
                    row.Cells[3].RowSpan = previousRow.Cells[3].RowSpan < 2 ? 2 : previousRow.Cells[3].RowSpan + 1;
                    row.Cells[4].RowSpan = previousRow.Cells[4].RowSpan < 2 ? 2 : previousRow.Cells[4].RowSpan + 1;
                    row.Cells[5].RowSpan = previousRow.Cells[5].RowSpan < 2 ? 2 : previousRow.Cells[5].RowSpan + 1;
                    row.Cells[6].RowSpan = previousRow.Cells[6].RowSpan < 2 ? 2 : previousRow.Cells[6].RowSpan + 1;

                    previousRow.Cells[0].Visible = false;
                    previousRow.Cells[1].Visible = false;

                    previousRow.Cells[2].Visible = false;
                    previousRow.Cells[3].Visible = false;
                    previousRow.Cells[4].Visible = false;
                    previousRow.Cells[5].Visible = false;
                    previousRow.Cells[6].Visible = false;
                }
            }

        }
        protected void btnXem_OnClick(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        private Guid GetDrop(DropDownList dl)
        {
            if (dl.SelectedValue.Trim().Equals(""))
            {
                return Guid.Empty;
            }
            else
            {
                return new Guid(dl.SelectedValue);
            }
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            hdPage.Value = "1";
            Search(CurrentPage);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }

        protected void btnDuyetTB_Click(object sender, EventArgs e)
        {
            tblBanGach updatehd = (from p in db.tblBanGaches
                                      where p.ID == new Guid(hdID.Value)
                                      select p).FirstOrDefault();
            if (updatehd.TrangThai == 1)
            {
                #region [Duyệt]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    updatehd.TrangThai = 2;
                    updatehd.TrangThaiText = "Đã duyệt";
                    db.SubmitChanges();

                    //luu nhat trinh
                    var query = (from p in db.tblDinhMucNhienLieus
                                 where p.IDXe == updatehd.IDBienSoXe
                                 && p.TrangThai == 2
                                 select p).FirstOrDefault();
                    string txtDMDiChuyenCoTaiBen = "";
                    string txtDMDiChuyenKhongTaiBen = "";
                    string txtDMCauHaHang = "";
                    if (query != null && query.ID != null)
                    {
                        txtDMDiChuyenCoTaiBen = query.DiChuyenCoTaiBen.ToString();
                        txtDMDiChuyenKhongTaiBen = query.DiChuyenKhongTaiBen.ToString();
                        txtDMCauHaHang = query.CauHaHang.ToString();
                    }

                    var queryChiTiet = (from p in db.tblBanGach_ChiTiets
                                     where p.IDBan == updatehd.ID
                                     select p).FirstOrDefault();
                    var queryLich = (from p in db.tblLichBanGaches
                                     where p.ID == queryChiTiet.IDLich
                                     && p.TrangThai == 2
                                     select p).FirstOrDefault();
                    string txtDiChuyen = "";
                    if (queryLich != null && queryLich.ID != null)
                    {
                        txtDiChuyen = queryLich.CuLyVanChuyen.ToString();
                    }
                    var queryNhaCungCap = (from p in db.tblNhaCungCaps
                                        where p.ID == updatehd.IDNhaCungCap
                                        select p).FirstOrDefault();
                    string noiDung = "Nhật trình tự sinh từ phiếu bán gạch";
                    if (queryNhaCungCap != null && queryNhaCungCap.ID != null)
                    {
                        noiDung = queryNhaCungCap.TenNhaCungCap;
                    }
                    var NhatTrinhXe = new tblNhatTrinhXe()
                    {
                        ID = Guid.NewGuid(),
                        NgayThang = queryLich.NgayThang,
                        IDNhomThietBi = new Guid("E58CFEE9-38A8-4791-ABE9-993562BA715F"),
                        IDChiNhanh = GetDrop(dlChiNhanh),
                        IDXe = new Guid(updatehd.IDBienSoXe.ToString()),
                        NoiDung = noiDung,
                        GioChay = queryLich.GioXuat,
                        IDPhieuBan = updatehd.ID,

                        DMDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen),
                        DMDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen),
                        DMCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang),

                        DMDiChuyenCoTaiBeTong = 0,
                        DMDiChuyenKhongTaiBeTong = 0,
                        DMLayBeTongTaiTram = 0,
                        DMXaBeTongVeSinh = 0,
                        DMNoMayQuayThung = 0,

                        DMDiChuyen = 0,
                        DMBomBeTong = 0,
                        DMVeSinhRaChan = 0,

                        DiChuyenCoTaiBen = queryLich.CuLyVanChuyen.ToString() == "" 
                            ? 0 : double.Parse(queryLich.CuLyVanChuyen.ToString()),
                        DiChuyenKhongTaiBen = queryLich.CuLyVanChuyen.ToString() == ""
                            ? 0 : double.Parse(queryLich.CuLyVanChuyen.ToString()),
                        CauHaHang = 1,

                        DiChuyenCoTaiBeTong = 0,
                        DiChuyenKhongTaiBeTong = 0,
                        LayBeTongTaiTram = 0,
                        XaBeTongVeSinh = 0,
                        NoMayQuayThung = 0,

                        DiChuyen = 0,
                        BomBeTong = 0,
                        VeSinhRaChan = 0,

                        DauDiChuyenCoTaiBen = queryLich.CuLyVanChuyen.ToString() == "" || txtDMDiChuyenCoTaiBen == "" 
                            ? 0 : (double.Parse(txtDMDiChuyenCoTaiBen) * double.Parse(queryLich.CuLyVanChuyen.ToString())),
                        DauDiChuyenKhongTaiBen = queryLich.CuLyVanChuyen.ToString() == "" || txtDMDiChuyenKhongTaiBen == ""
                            ? 0 : (double.Parse(txtDMDiChuyenKhongTaiBen) * double.Parse(queryLich.CuLyVanChuyen.ToString())),
                        DauCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang) * 1,

                        DauDiChuyenCoTaiBeTong = 0,
                        DauDiChuyenKhongTaiBeTong = 0,
                        DauLayBeTongTaiTram = 0,
                        DauXaBeTongVeSinh = 0,
                        DauNoMayQuayThung = 0,

                        DauDiChuyen = 0,
                        DauBomBeTong = 0,
                        DauVeSinhRaChan = 0,

                        //DuDauKy = txtDuDauKy.Text == "" ? 0 : double.Parse(txtDuDauKy.Text),
                        //DoDau = txtDoDau.Text == "" ? 0 : double.Parse(txtDoDau.Text),
                        //DuCuoiKy = txtDuCuoiKy.Text == "" ? 0 : double.Parse(txtDuCuoiKy.Text),
                        //KhoiLuong = 0,
                        //ChuyenChay = txtChuyenChay.Text == "" ? 0 : double.Parse(txtChuyenChay.Text),

                        DuDauKy = 0,
                        DoDau = 0,
                        DuCuoiKy = 0,
                        KhoiLuong = 0,
                        ChuyenChay = 0,
                        BomCa = 0,
                        BomKhoi = 0,
                        Loai = "NhatTrinhXeCau",

                        TrangThai = 1,
                        TrangThaiText = "Chờ duyệt",
                        NguoiTao = Session["IDND"].ToString(),
                        NgayTao = DateTime.Now
                    };
                    db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                    db.SubmitChanges();

                    Success("Đã duyệt giá mua vật liệu");
                }
                Search(1);
                #endregion
            }
            // Duyệt xóa
            else if (updatehd.TrangThai == 3)
            {
                #region [Duyệt xóa]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    db.tblBanGaches.DeleteOnSubmit(updatehd);
                    db.SubmitChanges();
                    Success("Đã duyệt xóa giá mua vật liệu.");
                    Search(1);
                }
                #endregion [Duyệt xóa]

            }
            else
            {

            }
        }

        protected void btnKDuyetTB_Click(object sender, EventArgs e)
        {
            #region [Không duyệt]
            var query = (from p in db.tblBanGaches
                         where p.ID == new Guid(hdID.Value)
                         select p).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                query.TrangThaiText = query.TrangThai == 1 ? "Không duyệt" : "Không duyệt xóa";
                query.MoTa = txtMota.Text;
                db.SubmitChanges();
                Success("Không duyệt thành công");
                Search(int.Parse(hdPage.Value));
            }
            #endregion [Không duyệt]
        }
        protected void rdDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Xem";
        }
        protected void rdKhongDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Duyệt";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;

            if (e.CommandName.Equals("Sua"))
            {
                //System.Data.Linq.ISingleResult<sp_BanGach_LichSuResult> item = dm.sp_BanGach_LichSu(new Guid(ID));
                //gvDuyet.DataSource = item;
                //gvDuyet.DataBind();
                //btnDuyet.Visible = rdTrangThai.SelectedItem.Value == "1" ? true : false;
                //btnKhongDuyet.Visible = rdTrangThai.SelectedItem.Value == "1" ? true : false;
                //mpDuyet.Show();
                var updatehd = (from p in db.tblBanGaches
                             where p.ID == new Guid(ID)
                             select p).FirstOrDefault();
                if (updatehd != null && updatehd.ID != null)
                {
                    if (updatehd.TrangThai == 1)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            vatlieu.sp_BanGach_Duyet(updatehd.ID, Session["IDND"].ToString());
                            Success("Đã duyệt");

                            //luu nhat trinh
                            var query = (from p in db.tblDinhMucNhienLieus
                                         where p.IDXe == updatehd.IDBienSoXe
                                         && p.TrangThai == 2
                                         select p).FirstOrDefault();
                            string txtDMDiChuyenCoTaiBen = "";
                            string txtDMDiChuyenKhongTaiBen = "";
                            string txtDMCauHaHang = "";
                            if (query != null && query.ID != null)
                            {
                                txtDMDiChuyenCoTaiBen = query.DiChuyenCoTaiBen.ToString();
                                txtDMDiChuyenKhongTaiBen = query.DiChuyenKhongTaiBen.ToString();
                                txtDMCauHaHang = query.CauHaHang.ToString();

                                //chi sinh nhat trinh khi co dinh muc
                                var queryChiTiet = (from p in db.tblBanGach_ChiTiets
                                                    where p.IDBan == updatehd.ID
                                                    select p).FirstOrDefault();
                                var queryLich = (from p in db.tblLichBanGaches
                                                 where p.ID == queryChiTiet.IDLich
                                                 && p.TrangThai == 2
                                                 select p).FirstOrDefault();
                                string txtDiChuyen = "";
                                if (queryLich != null && queryLich.ID != null)
                                {
                                    txtDiChuyen = queryLich.CuLyVanChuyen.ToString();
                                }
                                var queryNhaCungCap = (from p in db.tblNhaCungCaps
                                                       where p.ID == updatehd.IDNhaCungCap
                                                       select p).FirstOrDefault();
                                string noiDung = "Nhật trình tự sinh từ phiếu bán gạch";
                                if (queryNhaCungCap != null && queryNhaCungCap.ID != null)
                                {
                                    noiDung = queryNhaCungCap.TenNhaCungCap;
                                }
                                var NhatTrinhXe = new tblNhatTrinhXe()
                                {
                                    ID = Guid.NewGuid(),
                                    NgayThang = queryLich.NgayThang,
                                    IDNhomThietBi = new Guid("E58CFEE9-38A8-4791-ABE9-993562BA715F"),
                                    IDChiNhanh = GetDrop(dlChiNhanh),
                                    IDXe = new Guid(updatehd.IDBienSoXe.ToString()),
                                    NoiDung = noiDung,
                                    GioChay = queryLich.GioXuat,
                                    IDPhieuBan = updatehd.ID,

                                    DMDiChuyenCoTaiBen = txtDMDiChuyenCoTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBen),
                                    DMDiChuyenKhongTaiBen = txtDMDiChuyenKhongTaiBen == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBen),
                                    DMCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang),

                                    DMDiChuyenCoTaiBeTong = 0,
                                    DMDiChuyenKhongTaiBeTong = 0,
                                    DMLayBeTongTaiTram = 0,
                                    DMXaBeTongVeSinh = 0,
                                    DMNoMayQuayThung = 0,

                                    DMDiChuyen = 0,
                                    DMBomBeTong = 0,
                                    DMVeSinhRaChan = 0,

                                    DiChuyenCoTaiBen = queryLich.CuLyVanChuyen.ToString() == ""
                                        ? 0 : double.Parse(queryLich.CuLyVanChuyen.ToString()),
                                    DiChuyenKhongTaiBen = queryLich.CuLyVanChuyen.ToString() == ""
                                        ? 0 : double.Parse(queryLich.CuLyVanChuyen.ToString()),
                                    CauHaHang = 1,

                                    DiChuyenCoTaiBeTong = 0,
                                    DiChuyenKhongTaiBeTong = 0,
                                    LayBeTongTaiTram = 0,
                                    XaBeTongVeSinh = 0,
                                    NoMayQuayThung = 0,

                                    DiChuyen = 0,
                                    BomBeTong = 0,
                                    VeSinhRaChan = 0,

                                    DauDiChuyenCoTaiBen = queryLich.CuLyVanChuyen.ToString() == "" || txtDMDiChuyenCoTaiBen == ""
                                        ? 0 : (double.Parse(txtDMDiChuyenCoTaiBen) * double.Parse(queryLich.CuLyVanChuyen.ToString())),
                                    DauDiChuyenKhongTaiBen = queryLich.CuLyVanChuyen.ToString() == "" || txtDMDiChuyenKhongTaiBen == ""
                                        ? 0 : (double.Parse(txtDMDiChuyenKhongTaiBen) * double.Parse(queryLich.CuLyVanChuyen.ToString())),
                                    DauCauHaHang = txtDMCauHaHang == "" ? 0 : double.Parse(txtDMCauHaHang) * 1,

                                    DauDiChuyenCoTaiBeTong = 0,
                                    DauDiChuyenKhongTaiBeTong = 0,
                                    DauLayBeTongTaiTram = 0,
                                    DauXaBeTongVeSinh = 0,
                                    DauNoMayQuayThung = 0,

                                    DauDiChuyen = 0,
                                    DauBomBeTong = 0,
                                    DauVeSinhRaChan = 0,

                                    //DuDauKy = txtDuDauKy.Text == "" ? 0 : double.Parse(txtDuDauKy.Text),
                                    //DoDau = txtDoDau.Text == "" ? 0 : double.Parse(txtDoDau.Text),
                                    //DuCuoiKy = txtDuCuoiKy.Text == "" ? 0 : double.Parse(txtDuCuoiKy.Text),
                                    //KhoiLuong = 0,
                                    //ChuyenChay = txtChuyenChay.Text == "" ? 0 : double.Parse(txtChuyenChay.Text),

                                    DuDauKy = 0,
                                    DoDau = 0,
                                    DuCuoiKy = 0,
                                    KhoiLuong = 0,
                                    ChuyenChay = 0,
                                    BomCa = 0,
                                    BomKhoi = 0,
                                    Loai = "NhatTrinhXeCau",
                                    Mota = "Auto",

                                    TrangThai = 2,
                                    TrangThaiText = "Đã duyệt",
                                    NguoiDuyet = Session["IDND"].ToString(),
                                    NguoiTao = Session["IDND"].ToString(),
                                    NgayTao = DateTime.Now
                                };
                                db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                                db.SubmitChanges();
                            } 

                            

                            Search(int.Parse(hdPage.Value));

                        }
                    }
                    else if (updatehd.TrangThai == 3)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Inverted(thongbao);
                            }
                        }
                        else
                        {
                            vatlieu.sp_BanGach_DuyetXoa(updatehd.ID);
                            Success("Đã duyệt xóa");
                            Search(int.Parse(hdPage.Value));
                        }
                    }
                }
                else
                {
                    Search(int.Parse(hdPage.Value));
                }
            }
            else if (e.CommandName == "Huy")
            {
                hdID.Value = ID;
                mpDuyet.Show();
                txtMota.Text = "";
            }
            else if (e.CommandName == "Xem")
            {
                var view = vatlieu.sp_BanGach_LichSu(new Guid(ID));
                gvLichSu.DataSource = view;
                gvLichSu.DataBind();
                mpLichSu.Show();
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
    }
}