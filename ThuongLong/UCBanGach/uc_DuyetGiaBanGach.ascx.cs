﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCBanGach
{
    public partial class uc_DuyetGiaBanGach : System.Web.UI.UserControl
    {
        private string frmName = "frmGiaBanGach";
        private static int PageNumber = 20;
        private static int CurrentPage = 1;
        private DBDataContext db = new DBDataContext();
        private VatLieuDataContext vatlieu = new VatLieuDataContext();
        private clsPhanQuyen phanquyen = new clsPhanQuyen();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    LoadChiNhanh();
                    //  IDDN = new Guid(Session["IDDN"].ToString());
                }
            }
        }

        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            //dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        public DateTime GetLastDayOfMonth(int iMonth, int iYear)
        {
            DateTime dtResult = new DateTime(iYear, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-1);
            return dtResult;
        }

        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        protected void btnDong_Click(object sender, EventArgs e)
        {
            mpDuyet.Hide();
        }
        private void Search(int page)
        {
            System.Data.Linq.ISingleResult<sp_GiaBanGach_ListDuyetResult> dt = vatlieu.sp_GiaBanGach_ListDuyet(GetDrop(dlChiNhanh), 1, PageNumber, page);
            GV.DataSource = dt;
            GV.DataBind();


            for (int rowIndex = GV.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = GV.Rows[rowIndex];
                GridViewRow previousRow = GV.Rows[rowIndex + 1];
                var lbtSua1 = (LinkButton)row.FindControl("lbtSua");
                var lbtSua2 = (LinkButton)previousRow.FindControl("lbtSua");
                var lbtHuy1 = (LinkButton)row.FindControl("lbtHuy");
                var lbtHuy2 = (LinkButton)previousRow.FindControl("lbtHuy");
                if (
                    lbtSua1.CommandArgument.Equals(lbtSua2.CommandArgument)
                    && lbtHuy1.CommandArgument.Equals(lbtHuy2.CommandArgument)

                    && row.Cells[2].Text.Equals(previousRow.Cells[2].Text)
                    && row.Cells[3].Text.Equals(previousRow.Cells[3].Text)
                    && row.Cells[4].Text.Equals(previousRow.Cells[4].Text)
                    && row.Cells[5].Text.Equals(previousRow.Cells[5].Text)
                    )
                {
                    row.Cells[0].RowSpan = previousRow.Cells[0].RowSpan < 2 ? 2 : previousRow.Cells[0].RowSpan + 1;
                    row.Cells[1].RowSpan = previousRow.Cells[1].RowSpan < 2 ? 2 : previousRow.Cells[1].RowSpan + 1;

                    row.Cells[2].RowSpan = previousRow.Cells[2].RowSpan < 2 ? 2 : previousRow.Cells[2].RowSpan + 1;
                    row.Cells[3].RowSpan = previousRow.Cells[3].RowSpan < 2 ? 2 : previousRow.Cells[3].RowSpan + 1;
                    row.Cells[4].RowSpan = previousRow.Cells[4].RowSpan < 2 ? 2 : previousRow.Cells[4].RowSpan + 1;
                    row.Cells[5].RowSpan = previousRow.Cells[5].RowSpan < 2 ? 2 : previousRow.Cells[5].RowSpan + 1;

                    previousRow.Cells[0].Visible = false;
                    previousRow.Cells[1].Visible = false;

                    previousRow.Cells[2].Visible = false;
                    previousRow.Cells[3].Visible = false;
                    previousRow.Cells[4].Visible = false;
                    previousRow.Cells[5].Visible = false;
                }
            }

        }
        protected void btnXem_OnClick(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        private Guid GetDrop(DropDownList dl)
        {
            if (dl.SelectedValue.Trim().Equals(""))
            {
                return Guid.Empty;
            }
            else
            {
                return new Guid(dl.SelectedValue);
            }
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            hdPage.Value = "1";
            Search(CurrentPage);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }

        protected void btnDuyetTB_Click(object sender, EventArgs e)
        {
            tblGiaBanGach updatehd = (from p in db.tblGiaBanGaches
                                      where p.ID == new Guid(hdID.Value)
                                      select p).FirstOrDefault();
            if (updatehd.TrangThai == 1)
            {
                #region [Duyệt]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    updatehd.TrangThai = 2;
                    updatehd.TrangThaiText = "Đã duyệt";
                    db.SubmitChanges();
                    Success("Đã duyệt giá mua vật liệu");
                }
                Search(1);
                #endregion
            }
            // Duyệt xóa
            else if (updatehd.TrangThai == 3)
            {
                #region [Duyệt xóa]
                string url = "";
                string thongbao = phanquyen.CheckQuyenThaoTac(updatehd.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    db.tblGiaBanGaches.DeleteOnSubmit(updatehd);
                    db.SubmitChanges();
                    Success("Đã duyệt xóa giá mua vật liệu.");
                    Search(1);
                }
                #endregion [Duyệt xóa]

            }
            else
            {

            }
        }

        protected void btnKDuyetTB_Click(object sender, EventArgs e)
        {
            #region [Không duyệt]
            var query = (from p in db.tblGiaBanGaches
                         where p.ID == new Guid(hdID.Value)
                         select p).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                query.TrangThaiText = query.TrangThai == 1 ? "Không duyệt" : "Không duyệt xóa";
                query.MoTa = txtMota.Text;
                db.SubmitChanges();
                Success("Không duyệt thành công");
                Search(int.Parse(hdPage.Value));
            }
            #endregion [Không duyệt]
        }
        protected void rdDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Xem";
        }
        protected void rdKhongDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GV.Columns[0].HeaderText = "Duyệt";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;

            if (e.CommandName.Equals("Sua"))
            {
                //System.Data.Linq.ISingleResult<sp_GiaBanGach_LichSuResult> item = dm.sp_GiaBanGach_LichSu(new Guid(ID));
                //gvDuyet.DataSource = item;
                //gvDuyet.DataBind();
                //btnDuyet.Visible = rdTrangThai.SelectedItem.Value == "1" ? true : false;
                //btnKhongDuyet.Visible = rdTrangThai.SelectedItem.Value == "1" ? true : false;
                //mpDuyet.Show();
                var query = (from p in db.tblGiaBanGaches
                             where p.ID == new Guid(ID)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    if (query.TrangThai == 1)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 10, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Warning(thongbao);
                            }
                        }
                        else
                        {
                            vatlieu.sp_GiaBanGach_Duyet(query.ID, Session["IDND"].ToString());
                            Success("Đã duyệt");
                            Search(int.Parse(hdPage.Value));

                        }
                    }
                    else if (query.TrangThai == 3)
                    {
                        string url = "";
                        string thongbao = phanquyen.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 11, frmName, ref url);
                        if (thongbao != "")
                        {
                            if (url != "")
                            {
                                GstGetMess(thongbao, url);
                            }
                            else
                            {
                                Inverted(thongbao);
                            }
                        }
                        else
                        {
                            vatlieu.sp_GiaBanGach_DuyetXoa(query.ID);
                            Success("Đã duyệt xóa");
                            Search(int.Parse(hdPage.Value));
                        }
                    }
                }
                else
                {
                    Search(int.Parse(hdPage.Value));
                }
            }
            else if (e.CommandName == "Huy")
            {
                hdID.Value = ID;
                mpDuyet.Show();
                txtMota.Text = "";
            }
            else if (e.CommandName == "Xem")
            {
                var view = vatlieu.sp_GiaBanGach_LichSuChiTiet(new Guid(ID));
                gvLichSu.DataSource = view;
                gvLichSu.DataBind();
                mpLichSu.Show();
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
    }
}