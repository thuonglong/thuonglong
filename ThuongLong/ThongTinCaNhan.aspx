﻿<%@ Page Title="Thông tin cá nhân" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ThongTinCaNhan.aspx.cs" Inherits="ThuongLong.ThongTinCaNhan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Câu hỏi bí mật</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Câu hỏi 1</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Câu hỏi 1"
                                    Font-Size="13px" DataTextField="TenCauHoi" DataValueField="ID" ID="dlCauHoi1" runat="server">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtCauTraLoi1" runat="server" class="form-control" placeholder="Câu trả lời 1" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Câu hỏi 2</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Câu hỏi 2"
                                    Font-Size="13px" DataTextField="TenCauHoi" DataValueField="ID" ID="dlCauHoi2" runat="server">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtCauTraLoi2" runat="server" class="form-control" placeholder="Câu trả lời 2" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Câu hỏi 3</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Câu hỏi 3"
                                    Font-Size="13px" DataTextField="TenCauHoi" DataValueField="ID" ID="dlCauHoi3" runat="server">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtCauTraLoi3" runat="server" class="form-control" placeholder="Câu trả lời 3" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Địa chỉ email</label>
                                <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <!-- /.col -->

                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:LinkButton ID="lbtLuu" runat="server" class="btn btn-app bg-green" OnClick="lbtLuu_Click"><i class="fa fa-save"></i>Lưu thay đổi</asp:LinkButton>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Đổi mật khẩu</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Mật khẩu hiện tại</label>
                                <asp:TextBox ID="txtMatKhauHienTai" runat="server" class="form-control" placeholder="Mật khẩu hiện tại" Width="98%" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Mật khẩu mới</label>
                                <asp:TextBox ID="txtMatKhauMoi" runat="server" class="form-control" placeholder="Mật khẩu mới" Width="98%" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nhập lại mật khẩu mới</label>
                                <asp:TextBox ID="txtNhapLaiMatKhauMoi" runat="server" class="form-control" placeholder="Nhập lại mật khẩu mới" Width="98%" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <!-- /.col -->

                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:LinkButton ID="lbtDoiMatKhau" runat="server" class="btn btn-app bg-green" OnClick="lbtDoiMatKhau_Click"><i class="fa fa-save"></i>Đổi mật khẩu</asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
