﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class ChiNhanh : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])) ||
            //       string.IsNullOrEmpty(Convert.ToString(Session["IDDN"])))
            //    Response.Redirect("login.aspx");
            //else
            //{
            if (!IsPostBack)
            {
                //string url = "";
                //string thongbao = pq.TruyCapDanhMuc(new Guid(Session["IDDN"].ToString()), Session["IDND"].ToString(), "frmNhomThietBi", ref url);
                //if (thongbao != "")
                //{
                //    if (url != "")
                //    {
                //        GstGetMess(thongbao, url);
                //    }
                //    else
                //    {
                //        Warning(thongbao);
                //    }
                //}
                //else
                //{
                //    hdPage.Value = "1";
                //}
            }
            //}
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hdID.Value == "")
            {
                Guid ID = Guid.NewGuid();
                var ChiNhanh = new tblChiNhanh()
                {
                    ID = ID,
                    IDCha = ID,
                    TenChiNhanh = txtTenChiNhanh.Text.Trim(),
                    DiaChi = txtDiaChi.Text.Trim(),
                    Email = txtEmail.Text.Trim().ToLower(),
                    PhoneNumber = txtPhone.Text.Trim(),
                    TrangThai = false,
                    TrangThaiText = "Đã kích hoạt",
                    LoaiChiNhanh = 2,
                    NguoiTao = Session["IDND"].ToString(),
                    NgayTao = DateTime.Now

                };
                db.tblChiNhanhs.InsertOnSubmit(ChiNhanh);
                db.SubmitChanges();
                btnCancel_Click(sender, e);
                Search(1);
                Success("Lưu thành công.");
            }
            else
            {
                var query = (from p in db.tblChiNhanhs
                             where p.ID == new Guid(hdID.Value)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    query.TenChiNhanh = txtTenChiNhanh.Text.Trim();
                    query.DiaChi = txtDiaChi.Text.Trim();
                    query.Email = txtEmail.Text.Trim().ToLower();
                    query.PhoneNumber = txtPhone.Text.Trim();
                    db.SubmitChanges();
                    btnCancel_Click(sender, e);
                    Search(1);
                    Success("Sửa thành công");
                }
                else
                {
                    Warning("Thông tin chi nhánh không tồn tại.");
                    btnCancel_Click(sender, e);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtTenChiNhanh.Text = "";
            txtEmail.Text = "";
            txtDiaChi.Text = "";
            txtPhone.Text = "";
            txtSearch.Text = "";
            btnSave.Text = "Lưu";
            hdID.Value = "";
        }
        void Search(int page)
        {
            GV.DataSource = db.sp_ChiNhanh_Search(txtSearch.Text.Trim(), 10, page);
            GV.DataBind();
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblChiNhanhs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    txtTenChiNhanh.Text = query.TenChiNhanh;
                    txtDiaChi.Text = query.DiaChi;
                    txtEmail.Text = query.Email;
                    txtPhone.Text = query.PhoneNumber;
                    hdID.Value = id;
                    btnSave.Text = "Cập nhật";
                }
                else if (e.CommandName == "Xoa")
                {
                    string error = "";
                    var checkxoa = db.sp_ChiNhanh_CheckXoa(query.ID, ref error);
                    if (error != "")
                        Warning(error);
                    else
                    {
                        var xoadaily = from p in db.tblChiNhanhs
                                       where p.IDCha == query.ID
                                       select p;
                        if (xoadaily != null)
                            db.tblChiNhanhs.DeleteAllOnSubmit(xoadaily);
                        db.tblChiNhanhs.DeleteOnSubmit(query);
                        db.SubmitChanges();
                        Success("Xóa thành công");
                        btnCancel_Click(sender, e);
                        Search(1);
                    }
                }
                else if (e.CommandName == "Kích hoạt")
                {
                    if (query.TrangThai == false)
                    {
                        query.TrangThai = true;
                        query.TrangThaiText = "Đã kích hoạt";
                        db.SubmitChanges();
                        Success("Đã kích hoạt");
                    }
                    else
                    {
                        Success("Đã kích hoạt");
                    }
                    Search(1);
                }
                else if (e.CommandName == "Khóa")
                {
                    if (query.TrangThai == true)
                    {
                        query.TrangThai = false;
                        query.TrangThaiText = "Đã khóa";
                        db.SubmitChanges();
                        Success("Đã khóa");
                    }
                    else
                    {
                        Success("Đã khóa");
                    }
                    Search(1);
                }
            }
            else
            {
                Warning("Thông tin chi nhánh không tồn tại");
                btnCancel_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            Search(1);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            int CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            int CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
    }
}