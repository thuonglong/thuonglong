﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class QuanLyDangKiemXe : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        ThietBiDataContext tb = new ThietBiDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmQuanLyDangKiemXe";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmQuanLyDangKiemXe", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadThietBi();
                        LoadThietBiSearch();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        txtNgayMua.Text = dateTimenow.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadThietBi()
        {
            var query = from p in db.tblXeVanChuyens
                        where p.TrangThai == 2
                        && p.IsTrangThai == 1
                        orderby p.TenThietBi
                        select new
                        {
                            p.ID,
                            Ten = p.TenThietBi
                        };
            dlXe.DataSource = query;
            dlXe.DataBind();
            dlXe.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlXe_SelectedIndexChanged(object sender, EventArgs e)
        {
            var query = (from p in db.tblNguoiVanHanhs
                         join q in db.tblNhanSus on p.IDNhanVien equals q.ID
                         where p.IDThietBi == new Guid(dlXe.SelectedValue)
                         select q
                         ).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                txtLaiXe.Text = query.TenNhanVien;
                
            }
            var queryNhanVien = (from p in db.tblNguoiVanHanhs   
                         where p.IDThietBi == new Guid(dlXe.SelectedValue)
                         select p
             ).FirstOrDefault();
            if (queryNhanVien != null && queryNhanVien.ID != null)
            {
                dlChiNhanh.SelectedValue = queryNhanVien.IDChiNhanh.ToString();
            }
        }
        protected void txtThoiHanDangKiem_Changed(object sender, EventArgs e)
        {
            if (txtThoiHanDangKiem.Text == "")
            {
                txtNgayHetHan.Text = "";
            }
            else
            {
                txtNgayHetHan.Text = DateTime.Parse(GetNgayThang(txtNgayMua.Text)).AddMonths(int.Parse(txtThoiHanDangKiem.Text)).ToString("dd/MM/yyyy");
            }
        }
        protected void LoadThietBiSearch()
        {
            dlThietBiSearch.Items.Clear();
            var query = from p in db.tblXeVanChuyens
                        where p.TrangThai == 2
                        && p.IsTrangThai == 1
                        orderby p.TenThietBi
                        select new
                        {
                            p.ID,
                            Ten = p.TenThietBi
                        };
            dlThietBiSearch.DataSource = query;
            dlThietBiSearch.DataBind();
            dlThietBiSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (txtNgayMua.Text.Trim() == "")
            {
                s += " - Nhập ngày đăng kiểm<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlXe.SelectedValue == "")
            {
                s += " - Chọn biển số xe<br />";
            }
            if (txtThoiHanDangKiem.Text.Trim() == "")
            {
                s += " - Nhập thời hạn đăng kiểm<br />";
            }
            if (txtSoTienThanhToan.Text.Trim() == "")
            {
                s += " - Nhập số tiền thanh toán<br />";
            }
            if (s == "")
            {
                tb.sp_DangKiemXe_CheckThem(GetDrop(dlChiNhanh), GetDrop(dlXe), DateTime.Parse(GetNgayThang(txtNgayMua.Text)), DateTime.Parse(GetNgayThang(txtNgayHetHan.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (txtNgayMua.Text.Trim() == "")
            {
                s += " - Nhập ngày đăng kiểm<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlXe.SelectedValue == "")
            {
                s += " - Chọn biển số xe<br />";
            }
            if (txtThoiHanDangKiem.Text.Trim() == "")
            {
                s += " - Nhập thời hạn đăng kiểm<br />";
            }
            if (txtSoTienThanhToan.Text.Trim() == "")
            {
                s += " - Nhập số tiền thanh toán<br />";
            }
            if (s == "")
            {
                tb.sp_DangKiemXe_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlXe), DateTime.Parse(GetNgayThang(txtNgayMua.Text)), DateTime.Parse(GetNgayThang(txtNgayHetHan.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var QuanLyDangKiemXe = new tblDangKiemXe()
                        {
                            ID = Guid.NewGuid(),
                            NgayMua = DateTime.Parse(GetNgayThang(txtNgayMua.Text)),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDXe = GetDrop(dlXe),
                            TenLaiXe = txtLaiXe.Text,
                            ThoiHanDangKiem = txtThoiHanDangKiem.Text == "" ? 0 : double.Parse(txtThoiHanDangKiem.Text),
                            NgayHetHan = DateTime.Parse(GetNgayThang(txtNgayHetHan.Text)),
                            DonViDangKiem = txtDonViDangKiem.Text,
                            SoTienThanhToan = txtSoTienThanhToan.Text == "" ? 0 : double.Parse(txtSoTienThanhToan.Text),
                            GhiChu = txtGhiChu.Text,

                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblDangKiemXes.InsertOnSubmit(QuanLyDangKiemXe);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblDangKiemXes
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            //LoaiNguon =   ,
                            //IDNguon =   ,
                            query.NgayMua = DateTime.Parse(GetNgayThang(txtNgayMua.Text));
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDXe = GetDrop(dlXe);
                            query.TenLaiXe = txtLaiXe.Text;
                            query.ThoiHanDangKiem = txtThoiHanDangKiem.Text == "" ? 0 : double.Parse(txtThoiHanDangKiem.Text);
                            query.NgayHetHan = DateTime.Parse(GetNgayThang(txtNgayHetHan.Text));
                            query.DonViDangKiem = txtDonViDangKiem.Text;
                            query.SoTienThanhToan = txtSoTienThanhToan.Text == "" ? 0 : double.Parse(txtSoTienThanhToan.Text);
                            query.GhiChu = txtGhiChu.Text;
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin đăng kiểm đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            hdID.Value = "";
            LoadChiNhanh();
            LoadThietBi();
            dlChiNhanh.SelectedIndex = 0;
            dlXe.SelectedIndex = 0;

            txtNgayMua.Text = "";
            txtLaiXe.Text = "";
            txtThoiHanDangKiem.Text = "";
            txtNgayHetHan.Text = "";
            txtDonViDangKiem.Text = "";
            txtSoTienThanhToan.Text = "";
            txtGhiChu.Text = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblDangKiemXes
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadThietBi();
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlXe.SelectedValue = query.IDXe.ToString();
                        LoadChiNhanh();
                        LoadThietBi();
                        txtNgayMua.Text = query.NgayMua.ToString("dd/MM/yyyy");
                        txtLaiXe.Text = query.TenLaiXe;
                        txtThoiHanDangKiem.Text = query.ThoiHanDangKiem == null ? "" : DateTime.Parse(query.NgayHetHan.ToString()).ToString("dd/MM/yyyy"); 
                        txtNgayHetHan.Text = query.NgayMua.ToString("dd/MM/yyyy");
                        txtDonViDangKiem.Text = query.DonViDangKiem;
                        txtSoTienThanhToan.Text = query.SoTienThanhToan == null ? "" : query.SoTienThanhToan.ToString();
                        txtGhiChu.Text = query.GhiChu;
                        hdID.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblDangKiemXe_Logs
                                                where p.ID == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblDangKiemXes.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = tb.sp_DangKiemXe_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin tài sản đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    //return ns;
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }

        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }

        private void Search(int page)
        {
            if (txtTuNgaySearch.Text == "")
            {
                Warning("Nhập từ ngày tìm kiếm");
            }
            else if (txtDenNgaySearch.Text == "")
            {
                Warning("Nhập đến ngày tìm kiếm");
            }
            else if (DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)) > DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)))
            {
                Warning("Ngày bắt đầu tìm kiếm không được lớn hơn ngày kết thúc");
            }
            else
            {
                var query = tb.sp_DangKiemXe_Search(GetDrop(dlChiNhanhSearch), GetDrop(dlThietBiSearch), "0",
                    DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 20, page);

                GV.DataSource = query;
                GV.DataBind();
            }
        }
    }
}