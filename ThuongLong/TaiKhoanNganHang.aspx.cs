﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class TaiKhoanNganHang : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        NganHangDataContext soquy = new NganHangDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmTaiKhoanNganHang";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmTaiKhoanNganHang", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNganHang();
                        hdPage.Value = "1";

                        LoadNganHangSearch();
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));

        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNganHang()
        {
            var query = soquy.sp_TaiKhoanNganHang_LoadNganHang();
            dlNganHang.DataSource = query;
            dlNganHang.DataBind();
            dlNganHang.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";

            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlLoai.SelectedValue == "")
            {
                s += " - Chọn loại tiền<br />";
            }
            if (dlNganHang.SelectedValue == "")
            {
                s += " - Chọn ngân hàng<br />";
            }
            if (txtSoTaiKhoan.Text == "")
            {
                s += " - Nhập tài khoản<br />";
            }

            if (s == "")
            {
                var query = from p in db.tblTaiKhoanNganHangs
                            where p.SoTaiKhoan.ToUpper() == txtSoTaiKhoan.Text.Trim().ToUpper()
                            && p.IDChiNhanh == GetDrop(dlChiNhanh)
                            select p;
                if (query.Count() > 0)
                {
                    s += " - Số tài khoản này đã tồn tại<br />";
                }
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";

            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlLoai.SelectedValue == "")
            {
                s += " - Chọn loại tiền<br />";
            }
            if (dlNganHang.SelectedValue == "")
            {
                s += " - Chọn ngân hàng<br />";
            }
            if (txtSoTaiKhoan.Text == "")
            {
                s += " - Nhập tài khoản<br />";
            }
            if (s == "")
            {
                var query = from p in db.tblTaiKhoanNganHangs
                            where p.SoTaiKhoan.ToUpper() == txtSoTaiKhoan.Text.Trim().ToUpper()
                            && p.IDChiNhanh == GetDrop(dlChiNhanh)
                            && p.ID != new Guid(hdID.Value)
                            select p;
                if (query.Count() > 0)
                {
                    s += " - Số tài khoản này đã tồn tại<br />";
                }
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var TaiKhoanNganHang = new tblTaiKhoanNganHang()
                        {
                            ID = Guid.NewGuid(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNganHang = GetDrop(dlNganHang),
                            SoTaiKhoan = txtSoTaiKhoan.Text.Trim(),
                            LoaiTien = dlLoai.SelectedItem.Text.ToString(),
                            GhiChu = txtGhiChu.Text.Trim(),
                            TrangThai = 2,
                            TrangThaiText = "Đã duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblTaiKhoanNganHangs.InsertOnSubmit(TaiKhoanNganHang);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");

                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblTaiKhoanNganHangs
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNganHang = GetDrop(dlNganHang);
                            query.SoTaiKhoan = txtSoTaiKhoan.Text.Trim();
                            query.LoaiTien = dlLoai.SelectedItem.Text.ToString();
                            query.GhiChu = txtGhiChu.Text.Trim();
                            query.TrangThai = 2;
                            query.TrangThaiText = "Đã duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin tài khoản ngân hàng đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlNganHang.SelectedValue = "";
            txtSoTaiKhoan.Text = "";
            txtGhiChu.Text = "";
            hdID.Value = "";
        }
        protected void btnUpLoad_Click(object sender, EventArgs e)
        {
            //if (FileUpload1.HasFile)
            //{
            //    string extision = FileUpload1.FileName.Trim();
            //    string time = DateTime.Now.ToString("yyyyMMddHHmmss");
            //    string filename = hdHinhAnh.Value.ToUpper() + "-" + time + extision.Substring(extision.LastIndexOf('.'));
            //    FileUpload1.SaveAs(Server.MapPath("~/ImageTaiKhoanNganHang/") + filename);

            //    var query = (from p in db.tblTaiKhoanNganHangs
            //                 where p.ID == new Guid(hdHinhAnh.Value)
            //                 select p).FirstOrDefault();
            //    if (query != null && query.ID != null)
            //    {
            //        query.HinhAnh = filename;
            //        var log = (from p in db.tblTaiKhoanNganHang_Logs
            //                   where p.IDChung == new Guid(hdHinhAnh.Value)
            //                   orderby p.NgayTao descending
            //                   select p).FirstOrDefault();

            //        log.HinhAnh = filename;
            //        db.SubmitChanges();
            //    }
            //    GstGetMess("Upload thành công", "");
            //}
            //else
            //{
            //    GstGetMess("Bạn phải chọn file upload", "");
            //    mpUpLoad.Show();
            //}
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";

            string id = e.CommandArgument.ToString();

            var query = (from p in db.tblTaiKhoanNganHangs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        soquy.sp_TaiKhoanNganHang_CheckSuaGV(query.ID, query.IDChiNhanh, query.IDNganHang, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            dlLoai.SelectedValue = query.LoaiTien.ToString();
                            dlNganHang.SelectedValue = query.IDNganHang.ToString();
                            txtSoTaiKhoan.Text = query.SoTaiKhoan;
                            txtGhiChu.Text = query.GhiChu;
                            hdID.Value = id;
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        soquy.sp_TaiKhoanNganHang_CheckXoa(query.IDNganHang, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai != 3)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                            }
                            Success("Xóa thành công");
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = soquy.sp_TaiKhoanNganHang_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
                else if (e.CommandName == "HinhAnh")
                {
                    //imgHinhAnh.ImageUrl = "~/ImageTaiKhoanNganHang/" + query.HinhAnh;
                    //mpHinhAnh.Show();
                }
                else if (e.CommandName == "UpLoad")
                {
                    hdHinhAnh.Value = id;
                    mpUpLoad.Show();
                }
            }
            else
            {
                Warning("Thông tin tài khoản ngân hàng đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void ResetFilter()
        {
            //dlHangMucChiSearch.Items.Clear();
            //dlNganHangSearch.Items.Clear();
            //dlSoTaiKhoanSearch.Items.Clear();
            //List<sp_TaiKhoanNganHang_ResetFilterResult> query = soquy.sp_TaiKhoanNganHang_ResetFilter(GetDrop(dlChiNhanhSearch), int.Parse(dlLoaiSearch.SelectedValue), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text))).ToList();

            //dlHangMucChiSearch.DataSource = (from p in query
            //                                 select new
            //                                 {
            //                                     ID = p.HangMucChi,
            //                                     Ten = p.TenHangMucThuChi
            //                                 }).Distinct().OrderBy(p => p.Ten);
            //dlHangMucChiSearch.DataBind();
            //dlNganHangSearch.DataSource = (from p in query
            //                                   select new
            //                                   {
            //                                       ID = p.IDNganHang,
            //                                       Ten = p.TenNganHang
            //                                   }).Distinct().OrderBy(p => p.Ten);
            //dlNganHangSearch.DataBind();
            //dlSoTaiKhoanSearch.DataSource = (from p in query
            //                                select new
            //                                {
            //                                    ID = p.SoTaiKhoan,
            //                                    Ten = p.SoTaiKhoan
            //                                }).Distinct().OrderBy(p => p.Ten);
            //dlSoTaiKhoanSearch.DataBind();
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private string GetValueSelectedSoTaiKhoan(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "N'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",N'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = N'" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            List<sp_TaiKhoanNganHang_SearchResult> query = soquy.sp_TaiKhoanNganHang_Search(dlChiNhanhSearch.SelectedValue, dlNganHangSearch.SelectedValue, 10, page).ToList();

            GV.DataSource = query;
            GV.DataBind();
            //if (GV.Rows.Count > 0)
            //{
            //    sp_TaiKhoanNganHang_SearchFooterResult footer = soquy.sp_TaiKhoanNganHang_SearchFooter(GetDrop(dlChiNhanhSearch),  dlNganHangSearch.SelectedValue,
            //    DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();

            //    GV.FooterRow.Cells[0].ColumnSpan = 11;
            //    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", footer.SoLuong);
            //    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

            //    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", footer.SoTienThu);
            //    GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", footer.SoTienChi);

            //    GV.FooterRow.Cells[3].Visible = false;
            //    GV.FooterRow.Cells[4].Visible = false;
            //    GV.FooterRow.Cells[5].Visible = false;
            //    GV.FooterRow.Cells[6].Visible = false;
            //    GV.FooterRow.Cells[7].Visible = false;
            //    GV.FooterRow.Cells[8].Visible = false;
            //    GV.FooterRow.Cells[9].Visible = false;
            //    GV.FooterRow.Cells[10].Visible = false;
            //    GV.FooterRow.Cells[11].Visible = false;
            //    GV.FooterRow.Cells[12].Visible = false;
            //}

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 5, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                List<sp_TaiKhoanNganHang_PrintResult> query = soquy.sp_TaiKhoanNganHang_Print(dlChiNhanhSearch.SelectedValue, dlNganHangSearch.SelectedValue).ToList();
                if (query.Count > 0)
                {
                    string dc = "", tencongty = "", sdt = "";
                    var tencty = (from x in db.tblThamSos
                                  select x).FirstOrDefault();
                    if (tencty != null && tencty.ID != null)
                    {
                        dc = tencty.DiaChi;
                        sdt = tencty.SoDienThoai;
                        tencongty = tencty.TenCongTy;
                    }


                    var workbook = new HSSFWorkbook();
                    IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                    string sheetname = "DANH SÁCH TÀI KHOẢN NGÂN HÀNG";

                    #region[CSS]

                    var sheet = workbook.CreateSheet(sheetname);
                    sheet = xl.SetPropertySheet(sheet);

                    IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                    IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                    IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                    IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                    ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                    ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                    ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                    ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                    ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                    ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                    ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                    ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                    ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                    ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                    ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                    ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                    ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                    ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                    ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                    #endregion

                    #region[Tạo header trên]

                    var rowIndex = 0;
                    var row = sheet.CreateRow(rowIndex);
                    ICell r1c1 = row.CreateCell(0);
                    r1c1.SetCellValue(tencongty);
                    r1c1.CellStyle = styleboldcenternoborder;
                    r1c1.Row.Height = 400;
                    CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 2);
                    sheet.AddMergedRegion(cra);
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);
                    r1c1 = row.CreateCell(0);
                    r1c1.SetCellValue(dc);
                    r1c1.CellStyle = styleboldcenternoborder;
                    r1c1.Row.Height = 400;
                    cra = new CellRangeAddress(1, 1, 0, 2);
                    sheet.AddMergedRegion(cra);
                    //Tiêu đề báo cáo

                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);
                    r1c1 = row.CreateCell(0);

                    r1c1.SetCellValue("DANH SÁCH TÀI KHOẢN NGÂN HÀNG");

                    r1c1.CellStyle = styleHeader1;
                    r1c1.Row.Height = 800;
                    cra = new CellRangeAddress(2, 2, 0, 5);
                    sheet.AddMergedRegion(cra);

                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    r1c1 = row.CreateCell(0);
                    string kq = "";

                    r1c1.SetCellValue(kq);

                    r1c1.CellStyle = styleHeader2;
                    r1c1.Row.Height = 500;
                    cra = new CellRangeAddress(3, 3, 0, 5);
                    sheet.AddMergedRegion(cra);
                    sheet.CreateFreezePane(0, 5);
                    #endregion

                    #region[Tạo header dưới]
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    string[] header2 =
                        {
                        "Chi nhánh",
                        "Ngân hàng",
                        "Số tài khoản",
                        "Loại tiền",
                        "Nội dung"
                    };
                    var cell = row.CreateCell(0);

                    cell.SetCellValue("STT");
                    cell.CellStyle = styleHeaderChiTietCenter;
                    cell.Row.Height = 600;

                    for (int hk = 0; hk < header2.Length; hk++)
                    {
                        r1c1 = row.CreateCell(hk + 1);
                        r1c1.SetCellValue(header2[hk].ToString());
                        r1c1.CellStyle = styleHeaderChiTietCenter;
                        r1c1.Row.Height = 800;
                    }
                    #endregion

                    #region[ghi dữ liệu]
                    int STT2 = 0;

                    string RowDau = (rowIndex + 2).ToString();
                    foreach (var item in query)
                    {
                        STT2++;
                        rowIndex++;
                        row = sheet.CreateRow(rowIndex);

                        cell = row.CreateCell(0);
                        cell.SetCellValue(STT2);
                        cell.CellStyle = styleBodyIntCenter;

                        cell = row.CreateCell(1);
                        cell.SetCellValue(item.TenChiNhanh);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(2);
                        cell.SetCellValue(item.TenNganHang);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(3);
                        cell.SetCellValue(item.SoTaiKhoan);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(4);
                        cell.SetCellValue(item.LoaiTien);
                        cell.CellStyle = styleBody;

                        cell = row.CreateCell(5);
                        cell.SetCellValue(item.GhiChu);
                        cell.CellStyle = styleBody;

                    }
                    #endregion

                    #region[Set Footer]

                    //rowIndex++;
                    //rowIndex++;

                    //rowIndex++;
                    //row = sheet.CreateRow(rowIndex);

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(3);
                    //cell.SetCellValue("Bơm theo ca:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(6);
                    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(8);
                    //cell.SetCellValue("Thuê bơm:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(12);
                    //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //rowIndex++;
                    //row = sheet.CreateRow(rowIndex);

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(3);
                    //cell.SetCellValue("Bơm theo khối:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(6);
                    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(8);
                    //cell.SetCellValue("Thuê xe:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(12);
                    //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //rowIndex++;
                    //row = sheet.CreateRow(rowIndex);

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(3);
                    //cell.SetCellValue("Không dùng bơm:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(6);
                    //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                    //sheet.AddMergedRegion(cra);
                    //cell = row.CreateCell(8);
                    //cell.SetCellValue("Mua bê tông:");
                    //cell.CellStyle = styleFooterText;

                    //cell = row.CreateCell(12);
                    //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                    //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                    //rowIndex++;
                    //rowIndex++;
                    //row = sheet.CreateRow(rowIndex);

                    //đại diện
                    rowIndex++; rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                    sheet.AddMergedRegion(cra);

                    //tên
                    rowIndex++;

                    cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
                    sheet.AddMergedRegion(cra);
                    cell = row.CreateCell(2);
                    cell.SetCellValue("Kế toán");
                    cell.CellStyle = styleBodyFooterCenterNoborder;

                    cra = new CellRangeAddress(rowIndex, rowIndex, 5, 6);
                    sheet.AddMergedRegion(cra);
                    cell = row.CreateCell(5);
                    cell.SetCellValue("Giám đốc");
                    cell.CellStyle = styleBodyFooterCenterNoborder;

                    #endregion

                    using (var exportData = new MemoryStream())
                    {
                        workbook.Write(exportData);

                        #region [Set title dưới]

                        string saveAsFileName = "";
                        string Bienso = "";

                        saveAsFileName = "DANH SÁCH TÀI KHOẢN NGÂN HÀNG.xls";

                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                        #endregion

                        Response.Clear();
                        Response.BinaryWrite(exportData.GetBuffer());
                        Response.End();
                    }
                }
                else
                    GstGetMess("Không có dữ liệu nào để in", "");
            }
        }

        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            //if (divdl.Visible == true)
            //{
            //    divdl.Visible = false;
            //    dlHangMucChiSearch.Items.Clear();
            //    dlNganHangSearch.Items.Clear();
            //    dlSoTaiKhoanSearch.Items.Clear();
            //}
            //else
            //{
            //    divdl.Visible = true;
            //    ResetFilter();
            //}
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNganHangSearch();
        }
        protected void LoadNganHangSearch()
        {
            dlNganHangSearch.Items.Clear();
            List<sp_TaiKhoanNganHang_SearchLoadNganHangResult> query = soquy.sp_TaiKhoanNganHang_SearchLoadNganHang(GetDrop(dlChiNhanhSearch)).ToList();
            dlNganHangSearch.DataSource = query;
            dlNganHangSearch.DataBind();
            dlNganHangSearch.Items.Insert(0, new ListItem("Tất cả", ""));

        }

    }
}