﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class DinhMucBeTong : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        BeTongDataContext betong = new BeTongDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmDinhMucBeTong";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmDinhMucBeTong", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        hdPage.Value = "1";
                        LoadDanhMuc();
                        LoadLoaiMacSearch();
                    }
                }
            }
        }
        void LoadLoaiMacSearch()
        {
            dlMacBeTongSearch.Items.Clear();
            dlMacBeTongSearch.DataSource = (from p in db.tblDinhMucBeTongs
                                            join q in db.tblLoaiVatLieus on p.MacBeTong equals q.ID
                                            select new
                                            {
                                                ID = p.MacBeTong,
                                                Ten = q.TenLoaiVatLieu,
                                            }).Distinct().OrderBy(p => p.Ten);
            dlMacBeTongSearch.DataBind();
            dlMacBeTongSearch.Items.Insert(0, new ListItem("--Chọn --", ""));
        }
        void LoadDanhMuc()
        {
            dlMacBeTong.Items.Clear();
            dlMacBeTong.DataSource = from p in db.tblLoaiVatLieus
                                     where p.TrangThai == 2
                                     && p.IDNhomVatLieu == new Guid("29FC56FA-D0DD-469C-A551-CDC8123C1189")
                                     orderby p.STT
                                     select new
                                     {
                                         p.ID,
                                         Ten = p.TenLoaiVatLieu,
                                     };
            dlMacBeTong.DataBind();
            dlMacBeTong.Items.Insert(0, new ListItem("--Chọn --", ""));

            dlXiMang.Items.Clear();
            dlXiMang.DataSource = from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                                  && p.IDNhomVatLieu == new Guid("36FCDA11-FEF0-43C5-8AB8-C55F24281FF4")
                                  orderby p.STT
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu,
                                  };
            dlXiMang.DataBind();
            dlXiMang.Items.Insert(0, new ListItem("--Chọn --", ""));

            dlDa.Items.Clear();
            dlDa.DataSource = from p in db.tblLoaiVatLieus
                              where p.TrangThai == 2
                              && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                              orderby p.STT
                              select new
                              {
                                  p.ID,
                                  Ten = p.TenLoaiVatLieu,
                              };
            dlDa.DataBind();
            dlDa.Items.Insert(0, new ListItem("--Chọn --", ""));

            dlCat.Items.Clear();
            dlCat.DataSource = from p in db.tblLoaiVatLieus
                               where p.TrangThai == 2
                               && p.IDNhomVatLieu == new Guid("8B27D124-A687-400E-89F6-E3114A08CF87")
                               orderby p.STT
                               select new
                               {
                                   p.ID,
                                   Ten = p.TenLoaiVatLieu,
                               };
            dlCat.DataBind();
            dlCat.Items.Insert(0, new ListItem("--Chọn --", ""));

            dlPhuGia.Items.Clear();
            dlPhuGia.DataSource = from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                                  && p.IDNhomVatLieu == new Guid("6C435EB7-394E-408D-809F-54DE7F3245B4")
                                  orderby p.STT
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu,
                                  };
            dlPhuGia.DataBind();
            dlPhuGia.Items.Insert(0, new ListItem("--Chọn --", ""));

            dlTroBay.Items.Clear();
            dlTroBay.DataSource = from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                                  && p.IDNhomVatLieu == new Guid("F97D4631-E74D-4272-A9A1-34189C409AF3")
                                  orderby p.STT
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu,
                                  };
            dlTroBay.DataBind();
            dlTroBay.Items.Insert(0, new ListItem("--Chọn --", ""));

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckLuu()
        {
            string s = "";
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            if (dlXiMang.SelectedValue == "")
            {
                s += " - Chọn loại xi măng<br />";
            }
            if (dlDa.SelectedValue == "")
            {
                s += " - Chọn loại đá<br />";
            }
            if (dlCat.SelectedValue == "")
            {
                s += " - Chọn loại cát<br />";
            }

            if (txtDMXiMang.Text.Trim() == "")
            {
                s += " - Nhập định mức xi măng<br />";
            }
            if (txtDMDa.Text.Trim() == "")
            {
                s += " - Nhập định mức đá<br />";
            }
            if (txtDMCat.Text.Trim() == "")
            {
                s += " - Nhập định mức cát<br />";
            }
            if (dlPhuGia.SelectedValue != "" && txtDMPhuGia.Text.Trim() == "")
            {
                s += " - Nhập định mức phụ gia<br />";
            }
            if (dlTroBay.SelectedValue != "" && txtDMTroBay.Text.Trim() == "")
            {
                s += " - Nhập định mức tro bay<br />";
            }
            if (txtDMNuoc.Text.Trim() == "")
            {
                s += " - Nhập định mức nước<br />";
            }
            //if (txtTyLeQuyDoi.Text.Trim() == "")
            //{
            //    s += " - Nhập tỷ lệ quy đổi<br />";
            //}
            if (s == "")
                betong.sp_DinhMucBeTong_CkechLuu(hdID.Value, GetDrop(dlMacBeTong), GetDrop(dlXiMang), GetDrop(dlCat), GetDrop(dlDa), GetDrop(dlPhuGia), GetDrop(dlTroBay), ref s);
            if (s != "")
                Warning(s);
            return s;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string url = "";
            if (CheckLuu() == "")
            {
                if (hdID.Value == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var DinhMucBeTong = new tblDinhMucBeTong()
                        {
                            ID = Guid.NewGuid(),
                            MacBeTong = GetDrop(dlMacBeTong),
                            XiMang = GetDrop(dlXiMang),
                            Cat = GetDrop(dlCat),
                            Da = GetDrop(dlDa),
                            PhuGia = GetDrop(dlPhuGia),
                            Trobay = GetDrop(dlTroBay),
                            DMXiMang = txtDMXiMang.Text.Trim() == "" ? 0 : double.Parse(txtDMXiMang.Text.Trim()),
                            DMCat = txtDMCat.Text.Trim() == "" ? 0 : double.Parse(txtDMCat.Text.Trim()),
                            DMDa = txtDMDa.Text.Trim() == "" ? 0 : double.Parse(txtDMDa.Text.Trim()),
                            DMPhuGia = txtDMPhuGia.Text.Trim() == "" ? 0 : double.Parse(txtDMPhuGia.Text.Trim()),
                            DMTroBay = txtDMTroBay.Text.Trim() == "" ? 0 : double.Parse(txtDMTroBay.Text.Trim()),
                            DMNuoc = txtDMNuoc.Text.Trim() == "" ? 0 : double.Parse(txtDMNuoc.Text.Trim()),
                            TyLeQuyDoi = 1,
                            //TyLeQuyDoi = txtTyLeQuyDoi.Text.Trim() == "" ? 0 : double.Parse(txtTyLeQuyDoi.Text.Trim()),
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblDinhMucBeTongs.InsertOnSubmit(DinhMucBeTong);
                        db.SubmitChanges();
                        btnCancel_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
                else
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblDinhMucBeTongs
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.MacBeTong = GetDrop(dlMacBeTong);
                            query.XiMang = GetDrop(dlXiMang);
                            query.Cat = GetDrop(dlCat);
                            query.Da = GetDrop(dlDa);
                            query.PhuGia = GetDrop(dlPhuGia);
                            query.Trobay = GetDrop(dlTroBay);
                            query.DMXiMang = txtDMXiMang.Text.Trim() == "" ? 0 : double.Parse(txtDMXiMang.Text.Trim());
                            query.DMCat = txtDMCat.Text.Trim() == "" ? 0 : double.Parse(txtDMCat.Text.Trim());
                            query.DMDa = txtDMDa.Text.Trim() == "" ? 0 : double.Parse(txtDMDa.Text.Trim());
                            query.DMPhuGia = txtDMPhuGia.Text.Trim() == "" ? 0 : double.Parse(txtDMPhuGia.Text.Trim());
                            query.DMTroBay = txtDMTroBay.Text.Trim() == "" ? 0 : double.Parse(txtDMTroBay.Text.Trim());
                            query.DMNuoc = txtDMNuoc.Text.Trim() == "" ? 0 : double.Parse(txtDMNuoc.Text.Trim());
                            query.TyLeQuyDoi = 1;
                            //query.TyLeQuyDoi = txtTyLeQuyDoi.Text.Trim() == "" ? 0 : double.Parse(txtTyLeQuyDoi.Text.Trim());
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            btnCancel_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin tài khoản không tồn tại.");
                            btnCancel_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dlMacBeTong.SelectedValue = "";
            dlXiMang.SelectedValue = "";
            dlCat.SelectedValue = "";
            dlDa.SelectedValue = "";
            dlPhuGia.SelectedValue = "";
            dlTroBay.SelectedValue = "";
            txtDMXiMang.Text = "";
            txtDMCat.Text = "";
            txtDMDa.Text = "";
            txtDMPhuGia.Text = "";
            txtDMTroBay.Text = "";
            txtDMNuoc.Text = "";
            txtTyLeQuyDoi.Text = "";

            LoadLoaiMacSearch();
            btnSave.Text = "Lưu";
            hdID.Value = "";
        }
        void Search(int page)
        {
            GV.DataSource = betong.sp_DinhMucBeTong_Search(dlMacBeTongSearch.SelectedValue, 10, page);
            GV.DataBind();
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblDinhMucBeTongs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlMacBeTong.SelectedValue = query.MacBeTong.ToString();
                        dlXiMang.SelectedValue = query.XiMang.ToString();
                        dlCat.SelectedValue = query.Cat.ToString();
                        dlDa.SelectedValue = query.Da.ToString();
                        if (query.PhuGia.ToString() == "00000000-0000-0000-0000-000000000000")
                            dlPhuGia.SelectedValue = "";
                        else
                            dlPhuGia.SelectedValue = query.PhuGia.ToString();
                        if (query.Trobay.ToString() == "00000000-0000-0000-0000-000000000000")
                            dlTroBay.SelectedValue = "";
                        else
                            dlTroBay.SelectedValue = query.Trobay.ToString();

                        txtDMXiMang.Text = string.Format("{0:N0.##}", query.DMXiMang.Value);
                        txtDMCat.Text = string.Format("{0:N0.##}", query.DMCat);
                        txtDMDa.Text = string.Format("{0:N0.##}", query.DMDa);
                        txtDMPhuGia.Text = string.Format("{0:N0.##}", query.DMPhuGia);
                        txtDMTroBay.Text = string.Format("{0:N0.##}", query.DMTroBay);
                        txtDMNuoc.Text = String.Format("{0:N0.##}", query.DMNuoc.Value);
                        //txtTyLeQuyDoi.Text = string.Format("{0:N0}", query.TyLeQuyDoi);

                        hdID.Value = id;
                        btnSave.Text = "Cập nhật";
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai == 1)
                        {
                            int countxoa = (from p in db.tblDinhMucBeTong_Logs
                                            where p.IDChung == query.ID
                                            select p).Count();
                            if (countxoa == 1)
                            {
                                db.tblDinhMucBeTongs.DeleteOnSubmit(query);
                                db.SubmitChanges();
                                Success("Đã xóa");
                            }
                            else
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                        }
                        else if (query.TrangThai == 2)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                            Success("Xóa thành công");
                        }
                        else
                        {
                            Warning("Dữ liệu đang chờ duyệt xóa");
                        }
                        //lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = betong.sp_DinhMucBeTong_LichSu(query.ID);
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin định mức bê tông không tồn tại");
                btnCancel_Click(sender, e);
            }
        }

        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;

                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Mác bê tông - Vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 6,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Beige,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Định mức vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 6,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void gvLichSu_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;

                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Mác bê tông - Vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 8,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Beige,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Định mức vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 6,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            {
                return "";
            }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }

    }
}