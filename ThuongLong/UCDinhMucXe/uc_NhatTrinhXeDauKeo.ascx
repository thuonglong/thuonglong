﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_NhatTrinhXeDauKeo.ascx.cs" Inherits="ThuongLong.UCDinhMucXe.uc_NhatTrinhXeDauKeo" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Thông tin</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày tháng</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Biển số xe</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXe" runat="server" OnSelectedIndexChanged="dlXe_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Giờ xuất phát</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtGioChay" runat="server" class="form-control" data-inputmask="'alias': 'hh:mm'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Nội dung</label>
                        <asp:TextBox ID="txtNoiDung" runat="server" class="form-control"></asp:TextBox>
                    </div>
                     <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Lái xe</label>
                        <asp:TextBox ID="txtLaiXe" runat="server" class="form-control" disabled></asp:TextBox>

                    </div>
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <h3 class="box-title">Định mức nhiên liệu</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Di chuyển có tải(lít/km)</label>
                                <asp:TextBox ID="txtDMDiChuyenCoTaiBen" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMDiChuyenCoTaiBen" ValidChars=".," />
                            </div>

                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Di chuyển không tải(lít/km)</label>
                                <asp:TextBox ID="txtDMDiChuyenKhongTaiBen" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMDiChuyenKhongTaiBen" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Động cơ lai(lít/lần)</label>
                                <asp:TextBox ID="txtDMCauHaHang" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMCauHaHang" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Loại nhiên liệu</label>
                                <asp:TextBox ID="txtLoaiNhienLieu" runat="server" class="form-control" disabled></asp:TextBox>

<%--                                    <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại nhiên liệu"
                                        Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiVatLieu" runat="server" AutoPostBack="true" disabled>
                                    </asp:DropDownList>--%>
                            </div>
                            <%--<div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển có tải(km)</label>
                                    <asp:TextBox ID="txtDMDiChuyenCoTaiBeTong" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMDiChuyenCoTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển không tải(km)(lít/km)</label>
                                    <asp:TextBox ID="txtDMDiChuyenKhongTaiBeTong" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMDiChuyenKhongTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Lấy bê tông(lần)</label>
                                    <asp:TextBox ID="txtDMLayBeTongTaiTram" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMLayBeTongTaiTram" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, xả bê tông(lần)</label>
                                    <asp:TextBox ID="txtDMXaBeTongVeSinh" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMXaBeTongVeSinh" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Nổ máy quay thùng(lần)</label>
                                    <asp:TextBox ID="txtDMNoMayQuayThung" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMNoMayQuayThung" ValidChars=".," />
                                </div>

                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển(km)</label>
                                    <asp:TextBox ID="txtDMDiChuyen" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMDiChuyen" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Bơm bê tông(lần)</label>
                                    <asp:TextBox ID="txtDMBomBeTong" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMBomBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, ra chân(lần)</label>
                                    <asp:TextBox ID="txtDMVeSinhRaChan" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDMVeSinhRaChan" ValidChars=".," />
                                </div>--%>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <h3 class="box-title">Nhật trình xe</h3>
                        </div>
                        <div class="box-body">

                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Di chuyển có tải(km)</label>
                                <asp:TextBox ID="txtDiChuyenCoTaiBen" runat="server" class="form-control" OnTextChanged="txtDiChuyenCoTaiBen_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiChuyenCoTaiBen" ValidChars=".," />
                            </div>

                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Di chuyển không tải(km)</label>
                                <asp:TextBox ID="txtDiChuyenKhongTaiBen" runat="server" class="form-control" OnTextChanged="txtDiChuyenKhongTaiBen_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiChuyenKhongTaiBen" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Động cơ lai(lần)</label>
                                <asp:TextBox ID="txtCauHaHang" runat="server" class="form-control" OnTextChanged="txtCauHaHang_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtCauHaHang" ValidChars=".," />
                            </div>

                            <%--<div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển có tải(km)</label>
                                    <asp:TextBox ID="txtDiChuyenCoTaiBeTong" runat="server" class="form-control" onchange="DiChuyenCoTaiBeTong()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDiChuyenCoTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển không tải(km)(lít/km)</label>
                                    <asp:TextBox ID="txtDiChuyenKhongTaiBeTong" runat="server" class="form-control" onchange="DiChuyenKhongTaiBeTong()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDiChuyenKhongTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Lấy bê tông(lần)</label>
                                    <asp:TextBox ID="txtLayBeTongTaiTram" runat="server" class="form-control" onchange="LayBeTongTaiTram()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtLayBeTongTaiTram" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, xả bê tông(lần)</label>
                                    <asp:TextBox ID="txtXaBeTongVeSinh" runat="server" class="form-control" onchange="XaBeTongVeSinh()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtXaBeTongVeSinh" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Nổ máy quay thùng(lần)</label>
                                    <asp:TextBox ID="txtNoMayQuayThung" runat="server" class="form-control" onchange="NoMayQuayThung()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtNoMayQuayThung" ValidChars=".," />
                                </div>

                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển(km)</label>
                                    <asp:TextBox ID="txtDiChuyen" runat="server" class="form-control" onchange="DiChuyen()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDiChuyen" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Bơm bê tông(lần)</label>
                                    <asp:TextBox ID="txtBomBeTong" runat="server" class="form-control" onchange="BomBeTong()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtBomBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, ra chân(lần)</label>
                                    <asp:TextBox ID="txtVeSinhRaChan" runat="server" class="form-control" onchange="VeSinhRaChan()"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtVeSinhRaChan" ValidChars=".," />
                                </div>--%>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-info">
                            <h3 class="box-title">Nhiên liệu tiêu hao thực tế</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Di chuyển có tải(lít)</label>
                                <asp:TextBox ID="txtDauDiChuyenCoTaiBen" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDauDiChuyenCoTaiBen" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Di chuyển không tải(lít)</label>
                                <asp:TextBox ID="txtDauDiChuyenKhongTaiBen" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDauDiChuyenKhongTaiBen" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Động cơ lai(lít)</label>
                                <asp:TextBox ID="txtDauCauHaHang" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDauCauHaHang" ValidChars=".," />
                            </div>

                            <%--<div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển có tải(km)</label>
                                    <asp:TextBox ID="txtDauDiChuyenCoTaiBeTong" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauDiChuyenCoTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển không tải(km)(lít/km)</label>
                                    <asp:TextBox ID="txtDauDiChuyenKhongTaiBeTong" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauDiChuyenKhongTaiBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Lấy bê tông(lần)</label>
                                    <asp:TextBox ID="txtDauLayBeTongTaiTram" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauLayBeTongTaiTram" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, xả bê tông(lần)</label>
                                    <asp:TextBox ID="txtDauXaBeTongVeSinh" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauXaBeTongVeSinh" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Nổ máy quay thùng(lần)</label>
                                    <asp:TextBox ID="txtDauNoMayQuayThung" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauNoMayQuayThung" ValidChars=".," />
                                </div>

                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Di chuyển(km)</label>
                                    <asp:TextBox ID="txtDauDiChuyen" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauDiChuyen" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Bơm bê tông(lần)</label>
                                    <asp:TextBox ID="txtDauBomBeTong" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauBomBeTong" ValidChars=".," />
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Vệ sinh, ra chân(lần)</label>
                                    <asp:TextBox ID="txtDauVeSinhRaChan" runat="server" class="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" FilterType="Custom,Numbers"
                                        TargetControlID="txtDauVeSinhRaChan" ValidChars=".," />
                                </div>--%>
                        </div>
                    </div>

                    <%--<div class="col-md-12">
                        <div class="box box-success">
                            <h3 class="box-title">Dầu cấp</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Dư đầu kỳ</label>
                                <asp:TextBox ID="txtDuDauKy" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDuDauKy" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Dầu cấp</label>
                                <asp:TextBox ID="txtDoDau" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDoDau" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Dư cuối kỳ</label>
                                <asp:TextBox ID="txtDuCuoiKy" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDuCuoiKy" ValidChars=".," />
                            </div>

                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Số chuyến</label>
                                <asp:TextBox ID="txtChuyenChay" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtChuyenChay" ValidChars=".," />
                            </div>
                        </div>
                    </div>--%>

                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-md-12">
                    <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                    <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                </div>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Danh sách xe đầu kéo</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Từ ngày</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"
                                ></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đến ngày</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"
                                ></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" runat="server" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Biển số xe</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThietBiSearch" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>

                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12" style="overflow: auto; width: 100%;">
                        <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" OnRowDataBound="GridViewRowDataBound" OnRowCreated="GV_RowCreated" ShowFooter="true"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Xóa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Tên chi nhánh" />
                                <asp:BoundField DataField="TenThietBi" HeaderText="Biển số xe" />
                                <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                                <asp:BoundField DataField="LoaiNhienLieu" HeaderText="Loại nhiên liệu" />
                                <asp:BoundField DataField="GioChay" HeaderText="Giờ xuất phát" />
                                <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />

                                <asp:BoundField DataField="DMDiChuyenCoTaiBen" HeaderText="Di chuyển có tải(km)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMDiChuyenKhongTaiBen" HeaderText="Di chuyển không tải(km)(lít/km)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMCauHaHang" HeaderText="Động cơ lai(lần)" ItemStyle-HorizontalAlign="Right" />

                                <%--                                    <asp:BoundField DataField="DMDiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải(km)" />
                                    <asp:BoundField DataField="DMDiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải(km)(lít/km)" />
                                    <asp:BoundField DataField="DMLayBeTongTaiTram" HeaderText="Lấy bê tông(lần) tại trạm" />
                                    <asp:BoundField DataField="DMXaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" />
                                    <asp:BoundField DataField="DMNoMayQuayThung" HeaderText="Nổ máy, quay thùng" />

                                    <asp:BoundField DataField="DMDiChuyen" HeaderText="Di chuyển(km)" />
                                    <asp:BoundField DataField="DMBomBeTong" HeaderText="Bơm bê tông(lần)" />
                                    <asp:BoundField DataField="DMVeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" />--%>

                                <asp:BoundField DataField="DiChuyenCoTaiBen" HeaderText="Di chuyển có tải(km)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DiChuyenKhongTaiBen" HeaderText="Di chuyển không tải(km)(lít/km)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="CauHaHang" HeaderText="Động cơ lai(lần)" ItemStyle-HorizontalAlign="Right" />

                                <%--                                    <asp:BoundField DataField="DiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải(km)" />
                                    <asp:BoundField DataField="DiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải(km)(lít/km)" />
                                    <asp:BoundField DataField="LayBeTongTaiTram" HeaderText="Lấy bê tông(lần) tại trạm" />
                                    <asp:BoundField DataField="XaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" />
                                    <asp:BoundField DataField="NoMayQuayThung" HeaderText="Nổ máy, quay thùng" />

                                    <asp:BoundField DataField="DiChuyen" HeaderText="Di chuyển(km)" />
                                    <asp:BoundField DataField="BomBeTong" HeaderText="Bơm bê tông(lần)" />
                                    <asp:BoundField DataField="VeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" />--%>

                                <asp:BoundField DataField="DauDiChuyenCoTaiBen" HeaderText="Di chuyển có tải(lít)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DauDiChuyenKhongTaiBen" HeaderText="Di chuyển không tải(lít)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DauCauHaHang" HeaderText="Động cơ lai(lần)" ItemStyle-HorizontalAlign="Right" />

                                <%--                                    <asp:BoundField DataField="DauDiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải(km)" />
                                    <asp:BoundField DataField="DauDiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải(km)(lít/km)" />
                                    <asp:BoundField DataField="DauLayBeTongTaiTram" HeaderText="Lấy bê tông(lần) tại trạm" />
                                    <asp:BoundField DataField="DauXaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" />
                                    <asp:BoundField DataField="DauNoMayQuayThung" HeaderText="Nổ máy, quay thùng" />

                                    <asp:BoundField DataField="DauDiChuyen" HeaderText="Di chuyển(km)" />
                                    <asp:BoundField DataField="DauBomBeTong" HeaderText="Bơm bê tông(lần)" />
                                    <asp:BoundField DataField="DauVeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" />--%>

    <%--                            <asp:BoundField DataField="DuDauKy" HeaderText="Dư đầu kỳ" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DoDau" HeaderText="Cấp dầu" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DuCuoiKy" HeaderText="Dư cuối kỳ" ItemStyle-HorizontalAlign="Right" />--%>

                                <%--<asp:BoundField DataField="KhoiLuong" HeaderText="Khối lượng"  ItemStyle-HorizontalAlign="Right" />--%>
                                <%--<asp:BoundField DataField="ChuyenChay" HeaderText="Chuyến" ItemStyle-HorizontalAlign="Right" />--%>
                                <%--<asp:BoundField DataField="BomCa" HeaderText="Theo ca"  ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="BomKhoi" HeaderText="Theo khối"  ItemStyle-HorizontalAlign="Right" />--%>
                                <asp:BoundField DataField="TongDau" HeaderText="Tổng nhiên liệu" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
            </div>
        </div>

        <script type="text/javascript">
            function getvalue(vat) {
                if (vat == '') vat = "0";
                vat = vat.replace(',', '').replace(',', '').replace(',', '');
                var t1 = parseFloat(vat);
                return t1;
            }
            function DiChuyenCoTaiBen() {
                var txtDiChuyenCoTaiBen = parseFloat(getvalue(document.getElementById("<%=txtDiChuyenCoTaiBen.ClientID %>").value));
                var txtDMDiChuyenCoTaiBen = parseFloat(getvalue(document.getElementById("<%=txtDMDiChuyenCoTaiBen.ClientID %>").value));
                if (txtDiChuyenCoTaiBen != '0' && txtDMDiChuyenCoTaiBen != '0') {
                    document.getElementById("<%=txtDauDiChuyenCoTaiBen.ClientID %>").value = (txtDiChuyenCoTaiBen * txtDMDiChuyenCoTaiBen);
                }
                else {
                    document.getElementById("<%=txtDauDiChuyenCoTaiBen.ClientID %>").value = '0';
                }
            }
            function DiChuyenKhongTaiBen() {
                var txtDiChuyenKhongTaiBen = parseFloat(getvalue(document.getElementById("<%=txtDiChuyenKhongTaiBen.ClientID %>").value));
                var txtDMDiChuyenKhongTaiBen = parseFloat(getvalue(document.getElementById("<%=txtDMDiChuyenKhongTaiBen.ClientID %>").value));
                if (txtDiChuyenKhongTaiBen != '0' && txtDMDiChuyenKhongTaiBen != '0') {
                    document.getElementById("<%=txtDauDiChuyenKhongTaiBen.ClientID %>").value = (txtDiChuyenKhongTaiBen * txtDMDiChuyenKhongTaiBen);
                }
                else {
                    document.getElementById("<%=txtDauDiChuyenKhongTaiBen.ClientID %>").value = '0';
                }
            }
            function CauHaHang() {
                var txtCauHaHang = parseFloat(getvalue(document.getElementById("<%=txtCauHaHang.ClientID %>").value));
                var txtDMCauHaHang = parseFloat(getvalue(document.getElementById("<%=txtDMCauHaHang.ClientID %>").value));
                if (txtCauHaHang != '0' && txtDMCauHaHang != '0') {
                    document.getElementById("<%=txtDauCauHaHang.ClientID %>").value = (txtCauHaHang * txtDMCauHaHang);
                }
                else {
                    document.getElementById("<%=txtDauCauHaHang.ClientID %>").value = '0';
                }
            }

        </script>

        <asp:HiddenField ID="hdPage" runat="server" Value="1" />
        <asp:HiddenField ID="hdID" runat="server" Value="" />
        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
        <asp:HiddenField ID="hdNhomThietBi" runat="server" Value="AB7BDA38-9DA8-4796-AD2C-F73081CC61C7" />

        <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
            Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center">
                    Lịch sử
                </div>
                <div class="panel-body" style="max-height: 500px; overflow: auto;">
                    <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                        EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                        <Columns>
                            <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                            <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />

                            <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="TenChiNhanh" HeaderText="Tên chi nhánh" />
                            <asp:BoundField DataField="TenThietBi" HeaderText="Biển số xe" />
                            <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                            <asp:BoundField DataField="LoaiNhienLieu" HeaderText="Loại nhiên liệu" />
                            <asp:BoundField DataField="GioChay" HeaderText="Giờ xuất phát" />
                            <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />

                            <asp:BoundField DataField="DMDiChuyenCoTaiBen" HeaderText="Di chuyển có tải(km)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DMDiChuyenKhongTaiBen" HeaderText="Di chuyển không tải(km)(lít/km)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DMCauHaHang" HeaderText="Động cơ lai(lần)" ItemStyle-HorizontalAlign="Right" />

                            <%--                                <asp:BoundField DataField="DMDiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải(km)" />
                                <asp:BoundField DataField="DMDiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải(km)(lít/km)" />
                                <asp:BoundField DataField="DMLayBeTongTaiTram" HeaderText="Lấy bê tông(lần) tại trạm" />
                                <asp:BoundField DataField="DMXaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" />
                                <asp:BoundField DataField="DMNoMayQuayThung" HeaderText="Nổ máy, quay thùng" />

                                <asp:BoundField DataField="DMDiChuyen" HeaderText="Di chuyển(km)" />
                                <asp:BoundField DataField="DMBomBeTong" HeaderText="Bơm bê tông(lần)" />
                                <asp:BoundField DataField="DMVeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" />--%>

                            <asp:BoundField DataField="DiChuyenCoTaiBen" HeaderText="Di chuyển có tải(lít)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DiChuyenKhongTaiBen" HeaderText="Di chuyển không tải(lít)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="CauHaHang" HeaderText="Động cơ lai(lít)" ItemStyle-HorizontalAlign="Right" />

                            <%--                                <asp:BoundField DataField="DiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải(km)" />
                                <asp:BoundField DataField="DiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải(km)(lít/km)" />
                                <asp:BoundField DataField="LayBeTongTaiTram" HeaderText="Lấy bê tông(lần) tại trạm" />
                                <asp:BoundField DataField="XaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" />
                                <asp:BoundField DataField="NoMayQuayThung" HeaderText="Nổ máy, quay thùng" />

                                <asp:BoundField DataField="DiChuyen" HeaderText="Di chuyển(km)" />
                                <asp:BoundField DataField="BomBeTong" HeaderText="Bơm bê tông(lần)" />
                                <asp:BoundField DataField="VeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" />--%>

                            <asp:BoundField DataField="DauDiChuyenCoTaiBen" HeaderText="Di chuyển có tải(km)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DauDiChuyenKhongTaiBen" HeaderText="Di chuyển không tải(km)(lít/km)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DauCauHaHang" HeaderText="Động cơ lai(lần)" ItemStyle-HorizontalAlign="Right" />

                            <%--                                <asp:BoundField DataField="DauDiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải(km)" />
                                <asp:BoundField DataField="DauDiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải(km)(lít/km)" />
                                <asp:BoundField DataField="DauLayBeTongTaiTram" HeaderText="Lấy bê tông(lần) tại trạm" />
                                <asp:BoundField DataField="DauXaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" />
                                <asp:BoundField DataField="DauNoMayQuayThung" HeaderText="Nổ máy, quay thùng" />

                                <asp:BoundField DataField="DauDiChuyen" HeaderText="Di chuyển(km)" />
                                <asp:BoundField DataField="DauBomBeTong" HeaderText="Bơm bê tông(lần)" />
                                <asp:BoundField DataField="DauVeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" />--%>

                            <asp:BoundField DataField="DuDauKy" HeaderText="Dư đầu kỳ" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DoDau" HeaderText="Cấp dầu" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DuCuoiKy" HeaderText="Dư cuối kỳ" ItemStyle-HorizontalAlign="Right" />

                            <%--<asp:BoundField DataField="KhoiLuong" HeaderText="Khối lượng"  ItemStyle-HorizontalAlign="Right" />--%>
                            <asp:BoundField DataField="ChuyenChay" HeaderText="Chuyến" ItemStyle-HorizontalAlign="Right" />
                            <%--<asp:BoundField DataField="BomCa" HeaderText="Theo ca"  ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="BomKhoi" HeaderText="Theo khối"  ItemStyle-HorizontalAlign="Right" />--%>

                            <asp:BoundField DataField="NguoiDuyet" HeaderText="Người duyệt" />
                            <asp:BoundField DataField="NguoiXoa" HeaderText="Người xóa" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer" style="align-content: center; text-align: center">
                    <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnPrint" />
    </Triggers>
</asp:UpdatePanel>
