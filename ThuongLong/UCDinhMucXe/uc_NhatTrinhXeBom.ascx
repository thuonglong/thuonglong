﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_NhatTrinhXeBom.ascx.cs" Inherits="ThuongLong.UCDinhMucXe.uc_NhatTrinhXeBom" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Thông tin</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày tháng</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Biển số xe</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXe" runat="server" OnSelectedIndexChanged="dlXe_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Giờ xuất phát</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtGioChay" runat="server" class="form-control" data-inputmask="'alias': 'hh:mm'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Nội dung</label>
                        <asp:TextBox ID="txtNoiDung" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Lái xe</label>
                        <asp:TextBox ID="txtLaiXe" runat="server" class="form-control" disabled></asp:TextBox>

                    </div>
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <h3 class="box-title">Định mức nhiên liệu</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Di chuyển(lít/km)</label>
                                <asp:TextBox ID="txtDMDiChuyen" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMDiChuyen" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Bơm bê tông(lít/lần)</label>
                                <asp:TextBox ID="txtDMBomBeTong" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMBomBeTong" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Vệ sinh, ra chân(lít/lần)</label>
                                <asp:TextBox ID="txtDMVeSinhRaChan" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMVeSinhRaChan" ValidChars=".," />
                            </div>

                            <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Loại nhiên liệu</label>
                                <asp:TextBox ID="txtLoaiNhienLieu" runat="server" class="form-control" disabled></asp:TextBox>

<%--                                    <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại nhiên liệu"
                                        Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiVatLieu" runat="server" AutoPostBack="true" disabled>
                                    </asp:DropDownList>--%>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <h3 class="box-title">Nhật trình xe</h3>
                        </div>
                        <div class="box-body">

                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Di chuyển(km)</label>
                                <asp:TextBox ID="txtDiChuyen" runat="server" class="form-control" OnTextChanged="txtDiChuyen_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiChuyen" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Bơm bê tông(lần)</label>
                                <asp:TextBox ID="txtBomBeTong" runat="server" class="form-control" OnTextChanged="txtBomBeTong_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtBomBeTong" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Vệ sinh, ra chân(lần)</label>
                                <asp:TextBox ID="txtVeSinhRaChan" runat="server" class="form-control" OnTextChanged="txtVeSinhRaChan_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtVeSinhRaChan" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Hệ số di chuyển</label>
                                <asp:TextBox ID="txtHeSoDiChuyen" runat="server" class="form-control" OnTextChanged="txtHeSoDiChuyen_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtHeSoDiChuyen" ValidChars=".," />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-info">
                            <h3 class="box-title">Nhiên liệu tiêu hao thực tế</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Di chuyển(lít)</label>
                                <asp:TextBox ID="txtDauDiChuyen" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDauDiChuyen" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Bơm bê tông(lít)</label>
                                <asp:TextBox ID="txtDauBomBeTong" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDauBomBeTong" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Vệ sinh, ra chân(lít)</label>
                                <asp:TextBox ID="txtDauVeSinhRaChan" runat="server" class="form-control" disabled></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDauVeSinhRaChan" ValidChars=".," />
                            </div>
                        </div>
                    </div>

<%--                    <div class="col-md-12">
                        <div class="box box-success">
                            <h3 class="box-title">Dầu cấp</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Dư đầu kỳ</label>
                                <asp:TextBox ID="txtDuDauKy" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDuDauKy" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Dầu cấp</label>
                                <asp:TextBox ID="txtDoDau" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDoDau" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Dư cuối kỳ</label>
                                <asp:TextBox ID="txtDuCuoiKy" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDuCuoiKy" ValidChars=".," />
                            </div>

                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Khối lượng</label>
                                <asp:TextBox ID="txtKhoiLuong" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtKhoiLuong" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Bơm theo ca</label>
                                <asp:TextBox ID="txtBomCa" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtBomCa" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Bơm theo khối</label>
                                <asp:TextBox ID="txtBomKhoi" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtBomKhoi" ValidChars=".," />
                            </div>
                        </div>
                    </div>
--%>


                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-md-12">
                    <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                    <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                </div>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Danh sách xe bơm bê tông</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Từ ngày</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"
                                ></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đến ngày</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"
                                ></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" AutoPostBack="true"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" runat="server" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Biển số xe</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThietBiSearch" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click" ><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>

                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12" style="overflow: auto; width: 100%;">
                        <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" OnRowDataBound="GridViewRowDataBound" OnRowCreated="GV_RowCreated" ShowFooter="true"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Xóa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Tên chi nhánh" />
                                <asp:BoundField DataField="TenThietBi" HeaderText="Biển số xe" />
                                <asp:BoundField DataField="GioChay" HeaderText="Giờ xuất phát" />
                                <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                                <asp:BoundField DataField="LoaiNhienLieu" HeaderText="Loại nhiên liệu" />
                                <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />

                                <asp:BoundField DataField="DMDiChuyen" HeaderText="Di chuyển(km)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMBomBeTong" HeaderText="Bơm bê tông(lần)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMVeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" ItemStyle-HorizontalAlign="Right" />

                                <asp:BoundField DataField="DiChuyen" HeaderText="Di chuyển(km)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="BomBeTong" HeaderText="Bơm bê tông(lần)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="VeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" ItemStyle-HorizontalAlign="Right" />

                                <asp:BoundField DataField="DauDiChuyen" HeaderText="Di chuyển(lít)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DauBomBeTong" HeaderText="Bơm bê tông(lít)" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DauVeSinhRaChan" HeaderText="Vệ sinh, ra chân(lít)" ItemStyle-HorizontalAlign="Right" />

                               <%-- <asp:BoundField DataField="DuDauKy" HeaderText="Dư đầu kỳ" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DoDau" HeaderText="Cấp dầu" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DuCuoiKy" HeaderText="Dư cuối kỳ" ItemStyle-HorizontalAlign="Right" />

                                <asp:BoundField DataField="KhoiLuong" HeaderText="Khối lượng" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="BomCa" HeaderText="Theo ca" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="BomKhoi" HeaderText="Theo khối" ItemStyle-HorizontalAlign="Right" />--%>

                                <asp:BoundField DataField="TongDau" HeaderText="Tổng nhiên liệu" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
            </div>
        </div>

        <script type="text/javascript">
            function getvalue(vat) {
                if (vat == '') vat = "0";
                vat = vat.replace(',', '').replace(',', '').replace(',', '');
                var t1 = parseFloat(vat);
                return t1;
            }

            function DiChuyen() {
                var txtDiChuyen = parseFloat(getvalue(document.getElementById("<%=txtDiChuyen.ClientID %>").value));
                    var txtDMDiChuyen = parseFloat(getvalue(document.getElementById("<%=txtDMDiChuyen.ClientID %>").value));
                    if (txtDiChuyen != '0' && txtDMDiChuyen != '0') {
                        document.getElementById("<%=txtDauDiChuyen.ClientID %>").value = (txtDiChuyen * txtDMDiChuyen);
                    }
                    else {
                        document.getElementById("<%=txtDauDiChuyen.ClientID %>").value = '0';
                }
            }
            function BomBeTong() {
                var txtBomBeTong = parseFloat(getvalue(document.getElementById("<%=txtBomBeTong.ClientID %>").value));
                    var txtDMBomBeTong = parseFloat(getvalue(document.getElementById("<%=txtDMBomBeTong.ClientID %>").value));
                    if (txtBomBeTong != '0' && txtDMBomBeTong != '0') {
                        document.getElementById("<%=txtDauBomBeTong.ClientID %>").value = (txtBomBeTong * txtDMBomBeTong);
                    }
                    else {
                        document.getElementById("<%=txtDauBomBeTong.ClientID %>").value = '0';
                }
            }
            function VeSinhRaChan() {
                var txtVeSinhRaChan = parseFloat(getvalue(document.getElementById("<%=txtVeSinhRaChan.ClientID %>").value));
                    var txtDMVeSinhRaChan = parseFloat(getvalue(document.getElementById("<%=txtDMVeSinhRaChan.ClientID %>").value));
                    if (txtVeSinhRaChan != '0' && txtDMVeSinhRaChan != '0') {
                        document.getElementById("<%=txtDauVeSinhRaChan.ClientID %>").value = (txtVeSinhRaChan * txtDMVeSinhRaChan);
                    }
                    else {
                        document.getElementById("<%=txtDauVeSinhRaChan.ClientID %>").value = '0';
                }
            }

        </script>

        <asp:HiddenField ID="hdPage" runat="server" Value="1" />
        <asp:HiddenField ID="hdID" runat="server" Value="" />
        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
        <asp:HiddenField ID="hdNhomThietBi" runat="server" Value="0F47524B-BB6F-4ED6-898E-77528924024E" />

        <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
            Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center">
                    Lịch sử
                </div>
                <div class="panel-body" style="max-height: 500px; overflow: auto;">
                    <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                        EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                        <Columns>
                            <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                            <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />

                            <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="TenChiNhanh" HeaderText="Tên chi nhánh" />
                            <asp:BoundField DataField="TenThietBi" HeaderText="Biển số xe" />
                            <asp:BoundField DataField="TenLaiXe" HeaderText="Lái xe" />
                            <asp:BoundField DataField="LoaiNhienLieu" HeaderText="Loại nhiên liệu" />
                            <asp:BoundField DataField="GioChay" HeaderText="Giờ xuất phát" />
                            <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />

                            <asp:BoundField DataField="DMDiChuyen" HeaderText="Di chuyển(km)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DMBomBeTong" HeaderText="Bơm bê tông(lần)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DMVeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" ItemStyle-HorizontalAlign="Right" />

                            <asp:BoundField DataField="DiChuyen" HeaderText="Di chuyển(km)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="BomBeTong" HeaderText="Bơm bê tông(lần)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="VeSinhRaChan" HeaderText="Vệ sinh, ra chân(lần)" ItemStyle-HorizontalAlign="Right" />

                            <asp:BoundField DataField="DauDiChuyen" HeaderText="Di chuyển(lít)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DauBomBeTong" HeaderText="Bơm bê tông(lít)" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DauVeSinhRaChan" HeaderText="Vệ sinh, ra chân(lít)" ItemStyle-HorizontalAlign="Right" />

                            <asp:BoundField DataField="DuDauKy" HeaderText="Dư đầu kỳ" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DoDau" HeaderText="Cấp dầu" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DuCuoiKy" HeaderText="Dư cuối kỳ" ItemStyle-HorizontalAlign="Right" />

                            <asp:BoundField DataField="KhoiLuong" HeaderText="Khối lượng" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="BomCa" HeaderText="Theo ca" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="BomKhoi" HeaderText="Theo khối" ItemStyle-HorizontalAlign="Right" />

                            <asp:BoundField DataField="NguoiDuyet" HeaderText="Người duyệt" />
                            <asp:BoundField DataField="NguoiXoa" HeaderText="Người xóa" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer" style="align-content: center; text-align: center">
                    <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>

           <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtLuuHopDong" />--%>
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
</asp:UpdatePanel>
