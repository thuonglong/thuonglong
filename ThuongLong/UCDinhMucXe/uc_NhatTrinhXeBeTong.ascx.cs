﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;

using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCDinhMucXe
{
    public partial class uc_NhatTrinhXeBeTong : System.Web.UI.UserControl
    {
        DBDataContext db = new DBDataContext();
        ThietBiDataContext thietbi = new ThietBiDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmNhatTrinhXe";
        clsXuLy xl = new clsXuLy();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmNhatTrinhXe", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadThietBi();
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        txtNgayThang.Text = dateTimenow.ToString("dd/MM/yyyy");
                        LoadThietBiSearch();
                        hdPage.Value = "1";
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            //dlChiNhanhChot.DataSource = query;
            //dlChiNhanhChot.DataBind();
            //dlChiNhanhChot.Items.Insert(0, new ListItem("Chọn chi nhánh", ""));
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadThietBi()
        {
            var query = (from p in db.tblDinhMucNhienLieus
                         join q in db.tblXeVanChuyens on p.IDXe equals q.ID
                         where p.IDNhomThietBi == new Guid(hdNhomThietBi.Value)
                         && q.TrangThai == 2 && p.TrangThai == 2
                         select new
                         {
                             ID = p.IDXe,
                             Ten = q.TenThietBi
                         }).Distinct().OrderBy(p => p.Ten);
            dlXe.DataSource = query;
            dlXe.DataBind();
            dlXe.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlXe_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDinhMuc();
            if (dlXe.SelectedValue != "")
            {
                var query = (from p in db.tblNguoiVanHanhs
                             join q in db.tblNhanSus on p.IDNhanVien equals q.ID
                             where p.IDThietBi == new Guid(dlXe.SelectedValue)
                             select q
                        ).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    txtLaiXe.Text = query.TenNhanVien;

                }
            }
        }
        void LoadDinhMuc()
        {
            var query = (from p in db.tblDinhMucNhienLieus
                         where p.IDXe == GetDrop(dlXe)
                         && p.TrangThai == 2
                         select p).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                txtDMDiChuyenCoTaiBeTong.Text = query.DiChuyenCoTaiBeTong.ToString();
                txtDMDiChuyenKhongTaiBeTong.Text = query.DiChuyenKhongTaiBeTong.ToString();
                LoadLoaiVatLieu(query.IDLoaiNhienLieu.ToString());
                txtDMLayBeTongTaiTram.Text = query.LayBeTongTaiTram.ToString();
                txtDMXaBeTongVeSinh.Text = query.XaBeTongVeSinh.ToString();
                txtDMNoMayQuayThung.Text = query.NoMayQuayThung.ToString();

                txtDiChuyenCoTaiBeTong.Text = "";
                txtDiChuyenKhongTaiBeTong.Text = "";
                txtLayBeTongTaiTram.Text = "";
                txtXaBeTongVeSinh.Text = "";
                txtNoMayQuayThung.Text = "";

                txtDauDiChuyenCoTaiBeTong.Text = "";
                txtDauDiChuyenKhongTaiBeTong.Text = "";
                txtDauLayBeTongTaiTram.Text = "";
                txtDauXaBeTongVeSinh.Text = "";
                txtDauNoMayQuayThung.Text = "";

                //txtDuDauKy.Text = "";
                //txtDoDau.Text = "";
                //txtDuCuoiKy.Text = "";
                //txtKhoiLuong.Text = "";
                //txtChuyenChay.Text = "";
            }
            else
            {
                txtDMDiChuyenCoTaiBeTong.Text = "";
                txtDMDiChuyenKhongTaiBeTong.Text = "";
                txtDMLayBeTongTaiTram.Text = "";
                txtDMXaBeTongVeSinh.Text = "";
                txtDMNoMayQuayThung.Text = "";

                txtDiChuyenCoTaiBeTong.Text = "";
                txtDiChuyenKhongTaiBeTong.Text = "";
                txtLayBeTongTaiTram.Text = "";
                txtXaBeTongVeSinh.Text = "";
                txtNoMayQuayThung.Text = "";

                txtDauDiChuyenCoTaiBeTong.Text = "";
                txtDauDiChuyenKhongTaiBeTong.Text = "";
                txtDauLayBeTongTaiTram.Text = "";
                txtDauXaBeTongVeSinh.Text = "";
                txtDauNoMayQuayThung.Text = "";

                //txtDuDauKy.Text = "";
                //txtDoDau.Text = "";
                //txtDuCuoiKy.Text = "";
                //txtKhoiLuong.Text = "";
                //txtChuyenChay.Text = "";

                dlXe.SelectedValue = "";
                
            }
        }
        //protected void LoadNhanVien()
        //{
        //    var query = (from p in db.tblNhanSus
        //                 select new
        //                 {
        //                     p.ID,
        //                     Ten = p.TenNhanVien
        //                 }).Distinct().OrderBy(p => p.Ten);
        //    dlXe.DataSource = query;
        //    dlXe.DataBind();
        //    dlXe.Items.Insert(0, new ListItem("--Chọn--", ""));
        //}
        protected void LoadThietBiSearch()
        {
            dlThietBiSearch.Items.Clear();
            var query = (from p in db.tblDinhMucNhienLieus
                         join q in db.tblXeVanChuyens on p.IDXe equals q.ID
                         where p.IDNhomThietBi == new Guid(hdNhomThietBi.Value)
                         select new
                         {
                             ID = p.IDXe,
                             Ten = q.TenThietBi
                         }).Distinct().OrderBy(p => p.Ten);
            dlThietBiSearch.DataSource = query;
            dlThietBiSearch.DataBind();
            dlThietBiSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlXe.SelectedValue == "")
            {
                s += " - Chọn biển số xe<br />";
            }

            if (txtNgayThang.Text == "" && valDate(txtNgayThang.Text) == true)
            {
                s += "- Nhập ngày tháng<br/>";
            }
            if (txtNoiDung.Text == "")
            {
                s += "- Nhập nội dung<br/>";
            }
            if (txtGioChay.Text == "")
            {
                s += "- Nhập giờ chạy<br/>";
            }

            if (s == "")
            {
                if (hdID.Value == "")
                {
                    int query = (from p in db.tblDinhMucNhienLieus
                                 where p.IDXe == GetDrop(dlXe)
                                 select p).Count();
                    if (query == 0)
                        s += "Bạn phải thiết lập định mức cho xe trước khi nhập nhật trình xe.";

                }
                else
                {
                    var querycheck = (from p in db.tblNhatTrinhXes
                                      where p.ID == new Guid(hdID.Value)
                                      select p).FirstOrDefault();
                    if (querycheck != null && querycheck.IDXe != null && querycheck.IDXe != GetDrop(dlXe))
                    {
                        int query = (from p in db.tblNhatTrinhXes
                                     where p.IDXe == GetDrop(dlXe)
                                     && p.ID != new Guid(hdID.Value)
                                     select p).Count();
                        if (query > 0)
                            s += "Đã thiết lập nhật trình cho xe " + dlXe.SelectedItem.Text.ToString() + "";
                    }
                }
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var NhatTrinhXe = new tblNhatTrinhXe()
                        {
                            ID = Guid.NewGuid(),
                            NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                            IDNhomThietBi = new Guid(hdNhomThietBi.Value),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDXe = GetDrop(dlXe),
                            NoiDung = txtNoiDung.Text.Trim(), GioChay = TimeSpan.Parse(txtGioChay.Text.Trim()),

                            DMDiChuyenCoTaiBen = 0,
                            DMDiChuyenKhongTaiBen = 0,
                            DMCauHaHang = 0,

                            DMDiChuyenCoTaiBeTong = txtDMDiChuyenCoTaiBeTong.Text == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBeTong.Text),
                            DMDiChuyenKhongTaiBeTong = txtDMDiChuyenKhongTaiBeTong.Text == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBeTong.Text),
                            DMLayBeTongTaiTram = txtDMLayBeTongTaiTram.Text == "" ? 0 : double.Parse(txtDMLayBeTongTaiTram.Text),
                            DMXaBeTongVeSinh = txtDMXaBeTongVeSinh.Text == "" ? 0 : double.Parse(txtDMXaBeTongVeSinh.Text),
                            DMNoMayQuayThung = txtDMNoMayQuayThung.Text == "" ? 0 : double.Parse(txtDMNoMayQuayThung.Text),

                            DMDiChuyen = 0,
                            DMBomBeTong = 0,
                            DMVeSinhRaChan = 0,

                            DiChuyenCoTaiBen = 0,
                            DiChuyenKhongTaiBen = 0,
                            CauHaHang = 0,

                            DiChuyenCoTaiBeTong = txtDiChuyenCoTaiBeTong.Text == "" ? 0 : double.Parse(txtDiChuyenCoTaiBeTong.Text),
                            DiChuyenKhongTaiBeTong = txtDiChuyenKhongTaiBeTong.Text == "" ? 0 : double.Parse(txtDiChuyenKhongTaiBeTong.Text),
                            LayBeTongTaiTram = txtLayBeTongTaiTram.Text == "" ? 0 : double.Parse(txtLayBeTongTaiTram.Text),
                            XaBeTongVeSinh = txtXaBeTongVeSinh.Text == "" ? 0 : double.Parse(txtXaBeTongVeSinh.Text),
                            NoMayQuayThung = txtNoMayQuayThung.Text == "" ? 0 : double.Parse(txtNoMayQuayThung.Text),

                            DiChuyen = 0,
                            BomBeTong = 0,
                            VeSinhRaChan = 0,

                            DauDiChuyenCoTaiBen = 0,
                            DauDiChuyenKhongTaiBen = 0,
                            DauCauHaHang = 0,

                            DauDiChuyenCoTaiBeTong = txtDauDiChuyenCoTaiBeTong.Text == "" ? 0 : double.Parse(txtDauDiChuyenCoTaiBeTong.Text),
                            DauDiChuyenKhongTaiBeTong = txtDauDiChuyenKhongTaiBeTong.Text == "" ? 0 : double.Parse(txtDauDiChuyenKhongTaiBeTong.Text),
                            DauLayBeTongTaiTram = txtDauLayBeTongTaiTram.Text == "" ? 0 : double.Parse(txtDauLayBeTongTaiTram.Text),
                            DauXaBeTongVeSinh = txtDauXaBeTongVeSinh.Text == "" ? 0 : double.Parse(txtDauXaBeTongVeSinh.Text),
                            DauNoMayQuayThung = txtDauNoMayQuayThung.Text == "" ? 0 : double.Parse(txtDauNoMayQuayThung.Text),

                            DauDiChuyen = 0,
                            DauBomBeTong = 0,
                            DauVeSinhRaChan = 0,

                            //DuDauKy = txtDuDauKy.Text == "" ? 0 : double.Parse(txtDuDauKy.Text),
                            //DoDau = txtDoDau.Text == "" ? 0 : double.Parse(txtDoDau.Text),
                            //DuCuoiKy = txtDuCuoiKy.Text == "" ? 0 : double.Parse(txtDuCuoiKy.Text),
                            //KhoiLuong = txtKhoiLuong.Text == "" ? 0 : double.Parse(txtKhoiLuong.Text),
                            //ChuyenChay = txtChuyenChay.Text == "" ? 0 : double.Parse(txtChuyenChay.Text),
                            DuDauKy = 0,
                            DoDau = 0,
                            DuCuoiKy = 0,
                            KhoiLuong = 0,
                            ChuyenChay = 0,
                            BomCa = 0,
                            BomKhoi = 0,
                            Loai = "NhatTrinhXeBeTong",

                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblNhatTrinhXes.InsertOnSubmit(NhatTrinhXe);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblNhatTrinhXes
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.IDNhomThietBi = new Guid(hdNhomThietBi.Value);
                            query.IDXe = GetDrop(dlXe);
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.NoiDung = txtNoiDung.Text.Trim(); query.GioChay = TimeSpan.Parse(txtGioChay.Text.Trim());

                            query.DMDiChuyenCoTaiBeTong = txtDMDiChuyenCoTaiBeTong.Text == "" ? 0 : double.Parse(txtDMDiChuyenCoTaiBeTong.Text);
                            query.DMDiChuyenKhongTaiBeTong = txtDMDiChuyenKhongTaiBeTong.Text == "" ? 0 : double.Parse(txtDMDiChuyenKhongTaiBeTong.Text);
                            query.DMLayBeTongTaiTram = txtDMLayBeTongTaiTram.Text == "" ? 0 : double.Parse(txtDMLayBeTongTaiTram.Text);
                            query.DMXaBeTongVeSinh = txtDMXaBeTongVeSinh.Text == "" ? 0 : double.Parse(txtDMXaBeTongVeSinh.Text);
                            query.DMNoMayQuayThung = txtDMNoMayQuayThung.Text == "" ? 0 : double.Parse(txtDMNoMayQuayThung.Text);

                            query.DiChuyenCoTaiBeTong = txtDiChuyenCoTaiBeTong.Text == "" ? 0 : double.Parse(txtDiChuyenCoTaiBeTong.Text);
                            query.DiChuyenKhongTaiBeTong = txtDiChuyenKhongTaiBeTong.Text == "" ? 0 : double.Parse(txtDiChuyenKhongTaiBeTong.Text);
                            query.LayBeTongTaiTram = txtLayBeTongTaiTram.Text == "" ? 0 : double.Parse(txtLayBeTongTaiTram.Text);
                            query.XaBeTongVeSinh = txtXaBeTongVeSinh.Text == "" ? 0 : double.Parse(txtXaBeTongVeSinh.Text);
                            query.NoMayQuayThung = txtNoMayQuayThung.Text == "" ? 0 : double.Parse(txtNoMayQuayThung.Text);

                            query.DauDiChuyenCoTaiBeTong = txtDauDiChuyenCoTaiBeTong.Text == "" ? 0 : double.Parse(txtDauDiChuyenCoTaiBeTong.Text);
                            query.DauDiChuyenKhongTaiBeTong = txtDauDiChuyenKhongTaiBeTong.Text == "" ? 0 : double.Parse(txtDauDiChuyenKhongTaiBeTong.Text);
                            query.DauLayBeTongTaiTram = txtDauLayBeTongTaiTram.Text == "" ? 0 : double.Parse(txtDauLayBeTongTaiTram.Text);
                            query.DauXaBeTongVeSinh = txtDauXaBeTongVeSinh.Text == "" ? 0 : double.Parse(txtDauXaBeTongVeSinh.Text);
                            query.DauNoMayQuayThung = txtDauNoMayQuayThung.Text == "" ? 0 : double.Parse(txtDauNoMayQuayThung.Text);

                            //query.DuDauKy = txtDuDauKy.Text == "" ? 0 : double.Parse(txtDuDauKy.Text);
                            //query.DoDau = txtDoDau.Text == "" ? 0 : double.Parse(txtDoDau.Text);
                            //query.DuCuoiKy = txtDuCuoiKy.Text == "" ? 0 : double.Parse(txtDuCuoiKy.Text);
                            //query.KhoiLuong = txtKhoiLuong.Text == "" ? 0 : double.Parse(txtKhoiLuong.Text);
                            //query.ChuyenChay = txtChuyenChay.Text == "" ? 0 : double.Parse(txtChuyenChay.Text);
                            query.DuDauKy = 0;
                            query.DoDau = 0;
                            query.DuCuoiKy = 0;
                            query.KhoiLuong = 0;
                            query.ChuyenChay = 0;

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin giá mua vật liệu đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlXe.SelectedValue = "";
            dlXe_SelectedIndexChanged(sender, e);
            txtNoiDung.Text = ""; txtGioChay.Text = "";

            txtDMDiChuyenCoTaiBeTong.Text = "";
            txtDMDiChuyenKhongTaiBeTong.Text = "";
            txtDMLayBeTongTaiTram.Text = "";
            txtDMXaBeTongVeSinh.Text = "";
            txtDMNoMayQuayThung.Text = "";

            txtDiChuyenCoTaiBeTong.Text = "";
            txtDiChuyenKhongTaiBeTong.Text = "";
            txtLayBeTongTaiTram.Text = "";
            txtXaBeTongVeSinh.Text = "";
            txtNoMayQuayThung.Text = "";

            txtDauDiChuyenCoTaiBeTong.Text = "";
            txtDauDiChuyenKhongTaiBeTong.Text = "";
            txtDauLayBeTongTaiTram.Text = "";
            txtDauXaBeTongVeSinh.Text = "";
            txtDauNoMayQuayThung.Text = "";

            //txtDuDauKy.Text = "";
            //txtDoDau.Text = "";
            //txtDuCuoiKy.Text = "";
            //txtKhoiLuong.Text = "";
            //txtChuyenChay.Text = "";

            LoadThietBiSearch();

            hdID.Value = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblNhatTrinhXes
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        //dlXe.SelectedValue = query.IDXe.ToString();
                        try
                        {
                            dlXe.SelectedValue = query.IDXe.ToString();
                        }
                        catch (Exception ex)
                        {

                        }
                        txtNoiDung.Text = query.NoiDung.ToString(); txtGioChay.Text = query.GioChay.ToString();
                        LoadLoaiVatLieuEdit(query.IDXe.ToString());

                        txtDMDiChuyenCoTaiBeTong.Text = query.DMDiChuyenCoTaiBeTong.ToString();
                        txtDMDiChuyenKhongTaiBeTong.Text = query.DMDiChuyenKhongTaiBeTong.ToString();
                        txtDMLayBeTongTaiTram.Text = query.DMLayBeTongTaiTram.ToString();
                        txtDMXaBeTongVeSinh.Text = query.DMXaBeTongVeSinh.ToString();
                        txtDMNoMayQuayThung.Text = query.DMNoMayQuayThung.ToString();

                        txtDiChuyenCoTaiBeTong.Text = query.DiChuyenCoTaiBeTong.ToString();
                        txtDiChuyenKhongTaiBeTong.Text = query.DiChuyenKhongTaiBeTong.ToString();
                        txtLayBeTongTaiTram.Text = query.LayBeTongTaiTram.ToString();
                        txtXaBeTongVeSinh.Text = query.XaBeTongVeSinh.ToString();
                        txtNoMayQuayThung.Text = query.NoMayQuayThung.ToString();

                        txtDauDiChuyenCoTaiBeTong.Text = query.DauDiChuyenCoTaiBeTong.ToString();
                        txtDauDiChuyenKhongTaiBeTong.Text = query.DauDiChuyenKhongTaiBeTong.ToString();
                        txtDauLayBeTongTaiTram.Text = query.DauLayBeTongTaiTram.ToString();
                        txtDauXaBeTongVeSinh.Text = query.DauXaBeTongVeSinh.ToString();
                        txtDauNoMayQuayThung.Text = query.DauNoMayQuayThung.ToString();

                        //txtDuDauKy.Text = query.DuDauKy.ToString();
                        //txtDoDau.Text = query.DoDau.ToString();
                        //txtDuCuoiKy.Text = query.DuCuoiKy.ToString();
                        //txtKhoiLuong.Text = query.KhoiLuong.ToString();
                        //txtChuyenChay.Text = query.ChuyenChay.ToString();

                        hdID.Value = id;
                        //btnSave.Text = "Cập nhật";
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        //var checkxoa = from p in db.tblNhaCungCaps
                        //               where p.IDNhatTrinhXe == query.ID
                        //               select p;
                        //if (checkxoa.Count() > 0)
                        //{
                        //    Warning("Thông tin nhóm nhà cung cấp đã được sử dụng. Không được xóa.");
                        //}
                        //else
                        //{
                        if (query.TrangThai != 3)
                        {
                            query.TrangThai = 3;
                            query.NguoiXoa = Session["IDND"].ToString();
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                        }
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        //}
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = thietbi.sp_NhatTrinhXe_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin nhật trình đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = thietbi.sp_NhatTrinhXe_Search(GetDrop(dlChiNhanhSearch), dlThietBiSearch.SelectedValue, hdNhomThietBi.Value, DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }

            if (GV.Rows.Count > 0)
            {
                var getfooter = thietbi.sp_NhatTrinhXe_GetFooter(GetDrop(dlChiNhanhSearch), dlThietBiSearch.SelectedValue, hdNhomThietBi.Value, DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();
                if (getfooter != null && getfooter.DuCuoiKy != null)
                {
                    GV.FooterRow.Cells[0].ColumnSpan = 15;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", getfooter.SoLuong);
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", getfooter.DiChuyenCoTaiBeTong);
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", getfooter.DiChuyenKhongTaiBeTong);
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N0}", getfooter.LayBeTongTaiTram);
                    GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", getfooter.XaBeTongVeSinh);
                    GV.FooterRow.Cells[5].Text = string.Format("{0:N0}", getfooter.NoMayQuayThung);
                    GV.FooterRow.Cells[6].Text = string.Format("{0:N0}", getfooter.DauDiChuyenCoTaiBeTong);
                    GV.FooterRow.Cells[7].Text = string.Format("{0:N0}", getfooter.DauDiChuyenKhongTaiBeTong);
                    GV.FooterRow.Cells[8].Text = string.Format("{0:N0}", getfooter.DauLayBeTongTaiTram);
                    GV.FooterRow.Cells[9].Text = string.Format("{0:N0}", getfooter.DauXaBeTongVeSinh);
                    GV.FooterRow.Cells[10].Text = string.Format("{0:N0}", getfooter.DauNoMayQuayThung);

                    //GV.FooterRow.Cells[11].Text = string.Format("{0:N0}", getfooter.DuDauKy);
                    //GV.FooterRow.Cells[12].Text = string.Format("{0:N0}", getfooter.DoDau);
                    //GV.FooterRow.Cells[13].Text = string.Format("{0:N0}", getfooter.DuCuoiKy);
                    //GV.FooterRow.Cells[14].Text = string.Format("{0:N0}", getfooter.KhoiLuong);
                    //GV.FooterRow.Cells[15].Text = string.Format("{0:N0}", getfooter.ChuyenChay);
                    GV.FooterRow.Cells[11].Visible = false;
                    GV.FooterRow.Cells[12].Visible = false;
                    GV.FooterRow.Cells[13].Visible = false;
                    GV.FooterRow.Cells[14].Visible = false;
                    GV.FooterRow.Cells[15].Visible = false;
                    GV.FooterRow.Cells[16].Visible = false;
                    GV.FooterRow.Cells[17].Visible = false;
                    GV.FooterRow.Cells[18].Visible = false;
                    GV.FooterRow.Cells[19].Visible = false;
                    GV.FooterRow.Cells[20].Visible = false;
                    GV.FooterRow.Cells[21].Visible = false;
                    GV.FooterRow.Cells[22].Visible = false;
                }
            }
        }

        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Sửa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Xóa",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Trạng thái",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Biến số xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Lái xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Loại nhiên liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);


                headerCell = new TableCell
                {
                    Text = "Giờ xuất phát",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);


                headerCell = new TableCell
                {
                    Text = "Nội dung",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Định mức nhiên liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Beige,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Nhật trình xe",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Nhiên liệu tiêu hao thực tế",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 5,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Thông tin cấp dầu",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    ColumnSpan = 5,
                //    CssClass = "HeaderStyle",
                //    BackColor = Color.DarkCyan,
                //};
                //headerRow.Cells.Add(headerCell);

                //headerCell = new TableCell
                //{
                //    Text = "Tổng dầu",
                //    HorizontalAlign = HorizontalAlign.Center,
                //    RowSpan = 2,
                //    CssClass = "HeaderStyle"
                //};
                //headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadThietBiSearch();
        }

        protected void txtDiChuyenCoTaiBeTong_TextChanged(object sender, EventArgs e)
        {
            if (txtDiChuyenCoTaiBeTong.Text.Trim() != "" && txtDMDiChuyenCoTaiBeTong.Text != "")
            {
                txtDauDiChuyenCoTaiBeTong.Text = (double.Parse(txtDiChuyenCoTaiBeTong.Text) * double.Parse(txtDMDiChuyenCoTaiBeTong.Text)).ToString();
            }
            else
            {
                txtDauDiChuyenCoTaiBeTong.Text = "0";
            }
        }

        protected void txtDiChuyenKhongTaiBeTong_TextChanged(object sender, EventArgs e)
        {
            if (txtDiChuyenKhongTaiBeTong.Text.Trim() != "" && txtDMDiChuyenKhongTaiBeTong.Text != "")
            {
                txtDauDiChuyenKhongTaiBeTong.Text = (double.Parse(txtDiChuyenKhongTaiBeTong.Text) * double.Parse(txtDMDiChuyenKhongTaiBeTong.Text)).ToString();
            }
            else
            {
                txtDauDiChuyenKhongTaiBeTong.Text = "0";
            }
        }

        protected void txtLayBeTongTaiTram_TextChanged(object sender, EventArgs e)
        {
            if (txtLayBeTongTaiTram.Text.Trim() != "" && txtDMLayBeTongTaiTram.Text != "")
            {
                txtDauLayBeTongTaiTram.Text = (double.Parse(txtLayBeTongTaiTram.Text) * double.Parse(txtDMLayBeTongTaiTram.Text)).ToString();
            }
            else
            {
                txtDauLayBeTongTaiTram.Text = "0";
            }
        }

        protected void txtXaBeTongVeSinh_TextChanged(object sender, EventArgs e)
        {
            if (txtXaBeTongVeSinh.Text.Trim() != "" && txtDMXaBeTongVeSinh.Text != "")
            {
                txtDauXaBeTongVeSinh.Text = (double.Parse(txtXaBeTongVeSinh.Text) * double.Parse(txtDMXaBeTongVeSinh.Text)).ToString();
            }
            else
            {
                txtDauXaBeTongVeSinh.Text = "0";
            }
        }

        protected void txtNoMayQuayThung_TextChanged(object sender, EventArgs e)
        {
            if (txtNoMayQuayThung.Text.Trim() != "" && txtDMNoMayQuayThung.Text != "")
            {
                txtDauNoMayQuayThung.Text = (double.Parse(txtNoMayQuayThung.Text) * double.Parse(txtDMNoMayQuayThung.Text)).ToString();
            }
            else
            {
                txtDauNoMayQuayThung.Text = "0";
            }
        }
        void LoadLoaiVatLieu(string idLoaiNhienLieu)
        {
            var query = (from p in db.tblLoaiVatLieus
                         where p.ID == new Guid(idLoaiNhienLieu)
                         select p
                         ).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                txtLoaiNhienLieu.Text = query.TenLoaiVatLieu;

            }

        }

        void LoadLoaiVatLieuEdit(string idxe)
        {
            var query = (from p in db.tblDinhMucNhienLieus
                         join q in db.tblLoaiVatLieus on p.IDLoaiNhienLieu equals q.ID
                         where p.IDXe == new Guid(idxe)
                         && p.TrangThai == 2
                         select q).FirstOrDefault();
            if (query != null && query.ID != null)
            {
                txtLoaiNhienLieu.Text = query.TenLoaiVatLieu;

            }

        }


        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //List<sp_LichBanGach_PrintResult> query = vatlieu.sp_LichBanGach_Print(dlChiNhanhSearch.SelectedValue, dlNhaCungCapSearch.SelectedValue,
            //    DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();

            List<sp_NhatTrinhXe_SearchResult> query = thietbi.sp_NhatTrinhXe_Search(GetDrop(dlChiNhanhSearch), 
                dlThietBiSearch.SelectedValue, hdNhomThietBi.Value, DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), 
                DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text)), 0, 1).ToList();


            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "NHẬT TRÌNH XE BÊ TÔNG";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 7);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("NHẬT TRÌNH XE BÊ TÔNG TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                }
                else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("NHẬT TRÌNH XE BÊ TÔNG NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 14);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 14);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Ngày tháng",
                    "Chi nhánh",
                    "Biển số xe",
                    "Lái xe",
                    "Loại nhiên liệu",
                    "Giờ xuất phát",
                    "Nội dung",

                    "Định mức nhiên liệu",
                    "Định mức nhiên liệu",
                    "Định mức nhiên liệu",
                    "Định mức nhiên liệu",
                    "Định mức nhiên liệu",


                    "Nhật trình xe",
                    "Nhật trình xe",
                    "Nhật trình xe",
                    "Nhật trình xe",
                    "Nhật trình xe",

                    "Nhiên liệu tiêu hao thực tế",
                    "Nhiên liệu tiêu hao thực tế",
                    "Nhiên liệu tiêu hao thực tế",
                    "Nhiên liệu tiêu hao thực tế",
                    "Nhiên liệu tiêu hao thực tế",

                    "Tổng dầu"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {

                    "Ngày tháng",
                    "Chi nhánh",
                    "Biển số xe",
                    "Lái xe",
                    "Loại nhiên liệu",
                    "Giờ xuất phát",
                    "Nội dung",

                    "Di chuyển có tải(km)",
                    "Di chuyển không tải(km)",
                    "Lấy bê tông(lần) tại trạm",
                    "Xả bê tông, vệ sinh",
                    "Nổ máy, quay thùng",

                    "Di chuyển có tải(km)",
                    "Di chuyển không tải(km)",
                    "Lấy bê tông(lần) tại trạm",
                    "Xả bê tông, vệ sinh",
                    "Nổ máy, quay thùng",

                    "Di chuyển có tải(lít)",
                    "Di chuyển không tải(lít)",
                    "Lấy bê tông tại trạm (lít)",
                    "Xả bê tông, vệ sinh (lít)",
                    "Nổ máy, quay thùng (lít)",

                    "Tổng dầu"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 3, 3);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 4, 4);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 5, 5);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 6, 6);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 7, 7);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 8, 12);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 13, 17);
                sheet.AddMergedRegion(cra);

                cra = new CellRangeAddress(4, 4, 18, 22);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 23, 23);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.NgayThang);
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(item.TenThietBi);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(item.TenLaiXe);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(item.LoaiNhienLieu);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(item.GioChay.ToString());
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(item.NoiDung);
                    cell.CellStyle = styleBody;

                    ////////////////////////////////////////////////////////
                    cell = row.CreateCell(8);
                    cell.SetCellValue(double.Parse(item.DMDiChuyenCoTaiBeTong.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(double.Parse(item.DMDiChuyenKhongTaiBeTong.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(double.Parse(item.DMLayBeTongTaiTram.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(11);
                    cell.SetCellValue(double.Parse(item.DMXaBeTongVeSinh.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(12);
                    cell.SetCellValue(double.Parse(item.DMNoMayQuayThung.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    //////////////////////////////////////////////////////////////////////////////
                    cell = row.CreateCell(13);
                    cell.SetCellValue(double.Parse(item.DiChuyenCoTaiBeTong.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(14);
                    cell.SetCellValue(double.Parse(item.DiChuyenKhongTaiBeTong.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(15);
                    cell.SetCellValue(double.Parse(item.LayBeTongTaiTram.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(16);
                    cell.SetCellValue(double.Parse(item.XaBeTongVeSinh.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(17);
                    cell.SetCellValue(double.Parse(item.NoMayQuayThung.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    //////////////////////////////////////////////////////////////////////////////
                    cell = row.CreateCell(18);
                    cell.SetCellValue(double.Parse(item.DauDiChuyenCoTaiBeTong.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(19);
                    cell.SetCellValue(double.Parse(item.DauDiChuyenKhongTaiBeTong.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(20);
                    cell.SetCellValue(double.Parse(item.DauLayBeTongTaiTram.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(21);
                    cell.SetCellValue(double.Parse(item.DauXaBeTongVeSinh.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(22);
                    cell.SetCellValue(double.Parse(item.DauNoMayQuayThung.ToString()));
                    cell.CellStyle = styleBodyIntRight;


                    cell = row.CreateCell(23);
                    cell.SetCellValue(item.TongDau);
                    cell.CellStyle = styleBody;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= 23; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 7);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(8);
                cell.CellFormula = "COUNT(A" + RowDau.ToString() + ":A" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(23);
                cell.CellFormula = "SUM(X" + RowDau.ToString() + ":X" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;


                #endregion

                #region[Set Footer]

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;
                //tên
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 1);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(0);
                cell.SetCellValue("Người xuất hàng");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(2);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 4, 5);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(4);
                cell.SetCellValue("Trưởng bộ phận");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 8);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(6);
                cell.SetCellValue("Lái xe");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 9, 10);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(9);
                cell.SetCellValue("Kế toán");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 11, 13);
                sheet.AddMergedRegion(cra);
                cell = row.CreateCell(11);
                cell.SetCellValue("Giám sát");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                #endregion

                sheet.SetColumnWidth(4, 7120);
                sheet.SetColumnWidth(5, 3000);
                sheet.SetColumnWidth(6, 7000);
                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "NHẬT TRÌNH XE BÊ TÔNG -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                         ".xls";
                    }
                    else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "NHẬT TRÌNH XE BÊ TÔNG -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                         "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }

    }
}