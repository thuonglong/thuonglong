﻿using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCBaoCaoBeTong
{
    public partial class uc_BaoCaoDoanhThuBanBeTongNVKD : System.Web.UI.UserControl
    {
        private string frmName = "frmBaoCaoDoanhThuBanBeTong";
        private static int PageNumber = 20;
        private static int CurrentPage = 1;
        private DBDataContext db = new DBDataContext();
        BeTongDataContext betong = new BeTongDataContext();
        private clsPhanQuyen phanquyen = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    DateTime dateTimenow = DateTime.Now;
                    var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    txtTuNgay.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                    txtDenNgay.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    LoadChiNhanh();
                    LoadNVKD();
                    //  IDDN = new Guid(Session["IDDN"].ToString());
                }
            }
        }

        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Tất cả chi nhánh", ""));
        }
        void LoadNVKD()
        {
            dlNVKD.Items.Clear();
            var query = betong.sp_BaoCaoDoanhThuBanBeTong_SearchLoadNVKD(dlChiNhanh.SelectedValue, DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text)));
            dlNVKD.DataSource = query;
            dlNVKD.DataBind();
            dlNVKD.Items.Insert(0, new ListItem("Tất cả nhân viên", ""));
        }
        public DateTime GetLastDayOfMonth(int iMonth, int iYear)
        {
            DateTime dtResult = new DateTime(iYear, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-1);
            return dtResult;
        }

        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        protected void btnDong_Click(object sender, EventArgs e)
        {
            mpDuyet.Hide();

        }
        private void Search(int page)
        {
            List<sp_BaoCaoDoanhThuBanBeTong_SearchNVKDResult> dt = betong.sp_BaoCaoDoanhThuBanBeTong_SearchNVKD(dlChiNhanh.SelectedValue, dlNVKD.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text)), PageNumber, page).ToList();
            GV.DataSource = dt;
            GV.DataBind();
            hdTuNgay.Value = txtTuNgay.Text;
            hdDenNgay.Value = txtDenNgay.Text;
            if (GV.Rows.Count > 0)
            {

                sp_BaoCaoDoanhThuBanBeTong_SearchMacFooterResult query = betong.sp_BaoCaoDoanhThuBanBeTong_SearchMacFooter(dlChiNhanh.SelectedValue, dlNVKD.SelectedValue,
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text))).FirstOrDefault();

                if (query != null)
                {
                    GV.FooterRow.Cells[0].ColumnSpan = 2;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng:";
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    GV.FooterRow.Cells[1].Text = string.Format("{0:N2}", query.KLThucXuat);
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N2}", query.KLKhachHang);
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N2}", query.KLQuaCan);

                    GV.FooterRow.Cells[4].Text = string.Format("{0:N0}", query.TienHoaDon);
                    GV.FooterRow.Cells[5].Text = string.Format("{0:N0}", query.TienThanhToan);

                    GV.FooterRow.Cells[6].Text = string.Format("{0:N0}", query.TienHoaDonBom);
                    GV.FooterRow.Cells[7].Text = string.Format("{0:N0}", query.TienThanhToanBom);
                    GV.FooterRow.Cells[8].Text = string.Format("{0:N0}", query.PhuPhi);
                    GV.FooterRow.Cells[9].Text = string.Format("{0:N0}", query.TongHoaDon);
                    GV.FooterRow.Cells[10].Text = string.Format("{0:N0}", query.TongThanhToan);

                }
            }
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_BaoCaoDoanhThuBanBeTong_PrintNVKDResult> query = betong.sp_BaoCaoDoanhThuBanBeTong_PrintNVKD(dlChiNhanh.SelectedValue, dlNVKD.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text))).ToList();


            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "BÁO CÁO DOANH THU BÁN BÊ TÔNG";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 7);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 7);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgay.Text != txtDenNgay.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO DOANH THU BÁN BÊ TÔNG TỪ NGÀY" + " " + txtTuNgay.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgay.Text.Trim());
                }
                else if (txtTuNgay.Text == txtDenNgay.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO DOANH THU BÁN BÊ TÔNG NGÀY" + " " + txtTuNgay.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 12);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 12);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Chi nhánh",
                    "Mác bê tông",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Thông tin khối lượng",
                    "Tiền bê tông",
                    "Tiền bê tông",
                    "Tiền bơm",
                    "Tiền bơm",
                    "Phụ phí",
                    "Tổng tiền",
                    "Tổng tiền"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Chi nhánh",
                    "Mác bê tông",
                    "KL thực xuất",
                    "KL bán",
                    "KL qua cân",
                    "Thành tiền hóa đơn",
                    "Thành tiền thanh toán",
                    "Thành tiền hóa đơn",
                    "Thành tiền thanh toán",
                    "Phụ phí",
                    "Hóa đơn",
                    "Thanh toán"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 3, 5);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 6, 7);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 8, 9);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 10, 10);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 11, 12);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.TenMacBeTong);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(double.Parse(item.KLThucXuat.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(double.Parse(item.KLKhachHang.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(double.Parse(item.KLQuaCan.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(double.Parse(item.TienHoaDon.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(double.Parse(item.TienThanhToan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(double.Parse(item.TienHoaDonBom.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(double.Parse(item.TienThanhToanBom.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(double.Parse(item.PhuPhi.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(11);
                    cell.SetCellValue(double.Parse(item.TongHoaDon.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(12);
                    cell.SetCellValue(double.Parse(item.TongThanhToan.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= header2.Length; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(3);
                cell.CellFormula = "SUM(D" + RowDau.ToString() + ":D" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(4);
                cell.CellFormula = "SUM(E" + RowDau.ToString() + ":E" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(5);
                cell.CellFormula = "SUM(F" + RowDau.ToString() + ":F" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyDoubleFooterRight;

                cell = row.CreateCell(6);
                cell.CellFormula = "SUM(G" + RowDau.ToString() + ":G" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(7);
                cell.CellFormula = "SUM(H" + RowDau.ToString() + ":H" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(8);
                cell.CellFormula = "SUM(I" + RowDau.ToString() + ":I" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(9);
                cell.CellFormula = "SUM(J" + RowDau.ToString() + ":J" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(10);
                cell.CellFormula = "SUM(K" + RowDau.ToString() + ":K" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(11);
                cell.CellFormula = "SUM(L" + RowDau.ToString() + ":L" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(12);
                cell.CellFormula = "SUM(M" + RowDau.ToString() + ":M" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                #endregion

                #region[Set Footer]

                //rowIndex++;
                //rowIndex++;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo ca:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo khối:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê xe:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Không dùng bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Mua bê tông:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanh.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                #endregion

                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgay.Text.Trim() == txtDenNgay.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO DOANH THU BÁN BÊ TÔNG -" + Bienso + "-Ngày: " + "-" + txtTuNgay.Text +
                                         ".xls";
                    }
                    else if (txtTuNgay.Text.Trim() != txtDenNgay.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO DOANH THU BÁN BÊ TÔNG -" + Bienso + "-Từ ngày" + "-" + txtTuNgay.Text +
                                         "-Đến ngày" + "-" + txtDenNgay.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }
        protected void btnXem_OnClick(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }

        void Print(string idnhacungcap)
        {
            decimal? dunotruocky = 0;
            decimal? thanhtoankynay = 0;
            decimal? thuephaithu = 0;

            DateTime tungay = DateTime.Parse(GetNgayThang(hdTuNgay.Value));
            DateTime denngay = DateTime.Parse(GetNgayThang(hdDenNgay.Value));

            List<sp_BaoCaoDoanhThuBanBeTong_DoanhThuKhachHangLoadTongMacBeTongResult> query = betong.sp_BaoCaoDoanhThuBanBeTong_DoanhThuKhachHangLoadTongMacBeTong(dlChiNhanh.SelectedValue, idnhacungcap,
                tungay, denngay, ref dunotruocky, ref thanhtoankynay, ref thuephaithu).ToList();

            List<sp_BaoCaoDoanhThuBanBeTong_DoanhThuKhachHangLoadTongBomResult> bom = betong.sp_BaoCaoDoanhThuBanBeTong_DoanhThuKhachHangLoadTongBom(dlChiNhanh.SelectedValue, idnhacungcap,
                tungay, denngay).ToList();

            string dc = "", tencongty = "", sdt = "", giamdoc = "", ketoantruong = "";
            var tencty = (from x in db.tblThamSos
                          select x).FirstOrDefault();
            if (tencty != null && tencty.ID != null)
            {
                dc = tencty.DiaChi;
                sdt = tencty.SoDienThoai;
                tencongty = tencty.TenCongTy;
                giamdoc = tencty.GiamDoc;
                ketoantruong = tencty.KeToanTruong;
            }

            tblNhaCungCap ncc = (from p in db.tblNhaCungCaps
                                 where p.ID == new Guid(idnhacungcap)
                                 select p).FirstOrDefault();
            string tenncc = ncc.TenNhaCungCap;

            DateTime ngaykyhd = (from p in db.tblHopDongBanBeTongs
                                 where p.IDNhaCungCap == new Guid(idnhacungcap)
                                 select p).FirstOrDefault().NgayThang;


            string ngay = "", thang = "", nam = "";
            string ngaybd = "", thangbd = "", nambd = "";
            string ngaykt = "", thangkt = "", namkt = "";

            DateTime dateTime = DateTime.Now;
            ngay = dateTime.Day.ToString();
            thang = dateTime.Month.ToString();
            nam = dateTime.Year.ToString();

            ngaybd = tungay.Day.ToString();
            thangbd = tungay.Month.ToString();
            nambd = tungay.Year.ToString();

            ngaykt = denngay.Day.ToString();
            thangkt = denngay.Month.ToString();
            namkt = denngay.Year.ToString();

            var workbook = new HSSFWorkbook();
            IDataFormat dataFormatCustom = workbook.CreateDataFormat();
            string sheetname = "CN";

            ISheet sheet;
            IFont khong105 = xl.CreateFontSH(workbook, "Times New Roman", 10.5, false, false, false);
            IFont khong11 = xl.CreateFontSH(workbook, "Times New Roman", 11, false, false, false);
            IFont Dam11 = xl.CreateFontSH(workbook, "Times New Roman", 11, true, false, false);
            IFont Nghieng11 = xl.CreateFontSH(workbook, "Times New Roman", 11, false, true, false);
            IFont DamNghieng11 = xl.CreateFontSH(workbook, "Times New Roman", 11, true, true, false);
            IFont Dam12 = xl.CreateFontSH(workbook, "Times New Roman", 12, true, false, false);
            IFont DamGachChan12 = xl.CreateFontSH(workbook, "Times New Roman", 12, true, false, true);
            IFont Dam13 = xl.CreateFontSH(workbook, "Times New Roman", 13, true, false, false);
            IFont Dam16 = xl.CreateFontSH(workbook, "Times New Roman", 16, true, false, false);
            IFont DamNghieng12 = xl.CreateFontSH(workbook, "Times New Roman", 12, true, true, false);
            IFont DamNghieng10 = xl.CreateFontSH(workbook, "Times New Roman", 10, true, true, false);

            ICellStyle skhong105 = xl.CreateCellSH(workbook, 0, false, khong105);
            ICellStyle skhongTraiBorder105 = xl.CreateCellSH(workbook, 0, true, khong105);
            ICellStyle skhonggiuaBorder105 = xl.CreateCellSH(workbook, 1, true, khong105);
            ICellStyle skhongTraiBorder105int = xl.CreateCellStyleIntSH(workbook, 2, true, khong105);
            ICellStyle skhongTraiBorder105float = xl.CreateCellStyleDoubleSH(workbook, 2, true, khong105);
            //ICellStyle skhongTraiBorder105 = xl.CreateCellSH(workbook, 0, true, khong105);
            ICellStyle skhong11 = xl.CreateCellSH(workbook, 0, false, khong11);
            ICellStyle sDam11 = xl.CreateCellSH(workbook, 0, false, Dam11);
            ICellStyle sDamGiuaBorder11 = xl.CreateCellSH(workbook, 1, true, Dam11);
            ICellStyle sDamTraiBorder11 = xl.CreateCellSH(workbook, 0, true, Dam11);
            ICellStyle sDamPhaiBorder11int = xl.CreateCellStyleIntSH(workbook, 2, true, Dam11);
            ICellStyle sKhongPhaiBorder11int = xl.CreateCellStyleIntSH(workbook, 2, true, Dam11);
            ICellStyle sDamTrai12 = xl.CreateCellSH(workbook, 0, false, Dam12);
            ICellStyle sDamGachChan12 = xl.CreateCellSH(workbook, 1, false, DamGachChan12);
            ICellStyle sDamGiua12 = xl.CreateCellSH(workbook, 1, false, Dam12);
            ICellStyle sDam13 = xl.CreateCellSH(workbook, 1, false, Dam13);
            ICellStyle sDam16 = xl.CreateCellSH(workbook, 1, false, Dam16);
            ICellStyle sDamNghieng12 = xl.CreateCellSH(workbook, 0, false, DamNghieng12);
            ICellStyle sDamNghieng10 = xl.CreateCellSH(workbook, 0, false, DamNghieng10);
            ICellStyle sNghieng11 = xl.CreateCellSH(workbook, 0, false, Nghieng11);
            ICellStyle sDamNghieng11 = xl.CreateCellSH(workbook, 0, false, DamNghieng11);

            IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
            IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
            IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
            IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

            ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
            ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
            ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
            ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
            ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
            ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
            ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
            ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

            ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
            ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

            ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
            ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

            ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
            ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
            ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);

            var rowIndex = 0;
            CellRangeAddress cra;
            ICell cell;
            IRow row;
            HSSFRichTextString s;

            #region sheet congno
            sheet = workbook.CreateSheet(sheetname);
            sheet = xl.SetPropertySheet(sheet);

            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
            cell.CellStyle = sDam13;
            cra = new CellRangeAddress(0, 0, 0, 7);
            sheet.AddMergedRegion(cra);
            cell.Row.Height = 585;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("Độc lập - Tự do - Hạnh phúc");
            cell.CellStyle = sDamGachChan12;
            cra = new CellRangeAddress(1, 1, 0, 7);
            sheet.AddMergedRegion(cra);
            cell.Row.Height = 330;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("Số: 01/ĐCCN");
            cell.CellStyle = sDamNghieng10;
            cra = new CellRangeAddress(2, 2, 0, 1);
            sheet.AddMergedRegion(cra);
            cell.Row.Height = 330;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("BIÊN BẢN ĐỐI CHIẾU CÔNG NỢ");
            cell.CellStyle = sDam16;
            cra = new CellRangeAddress(3, 3, 0, 7);
            sheet.AddMergedRegion(cra);
            cell.Row.Height = 585;

            rowIndex++;
            rowIndex++;

            string ngaythangtext = "ngày " + ngaykyhd.Day.ToString() + " tháng " + ngaykyhd.Month.ToString() + " năm " + ngaykyhd.Year.ToString();

            s = new HSSFRichTextString(" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL  " + ngaythangtext + " giữa Công ty Cổ phần Thượng Long và " + tenncc + " Về việc cung cấp bê tông thương phẩm.");

            int lenghtngaythang = ngaythangtext.Length;

            int a = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL ").Length;
            int a1 = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL " + ngaythangtext + " giữa ").Length;
            int a2 = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL " + ngaythangtext + " giữa Công ty Cổ phần Thượng Long ").Length;
            int a3 = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL " + ngaythangtext + " giữa Công ty Cổ phần Thượng Long và ").Length;
            int a4 = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL " + ngaythangtext + " giữa Công ty Cổ phần Thượng Long và " + tenncc + "").Length;

            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);

            s.ApplyFont(0, a1, Nghieng11);
            s.ApplyFont(a1 + 1, a2, DamNghieng11);
            s.ApplyFont(a2 + 1, a3, Nghieng11);
            s.ApplyFont(a3 + 1, a4, DamNghieng11);
            s.ApplyFont(a4 + 1, s.Length, Nghieng11);

            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 930;
            cra = new CellRangeAddress(5, 5, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("   - Căn cứ vào biên bản xác nhận khối lượng tại công trình.");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 375;
            cra = new CellRangeAddress(6, 6, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("   - Hôm nay, ngày " + ngay + " tháng " + thang + " năm " + nam + ", tại văn phòng Công ty Cổ phần Thượng Long chúng tôi gồm có:");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 375;
            cra = new CellRangeAddress(7, 7, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);

            cell = row.CreateCell(0);
            cell.SetCellValue("A/ ĐẠI DIỆN BÊN A: " + tenncc.ToUpper() + "");
            cell.CellStyle = sDam11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(8, 8, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            s = new HSSFRichTextString("Ông: " + ncc.GiamDoc + "");
            row = sheet.CreateRow(rowIndex);

            cell = row.CreateCell(0);
            s.ApplyFont(0, 4, khong11);
            s.ApplyFont(5, s.Length, Dam11);
            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(9, 9, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            s = new HSSFRichTextString("Chức vụ: Giám đốc");
            s.ApplyFont(0, 8, khong11);
            s.ApplyFont(9, s.Length, Dam11);
            cell.SetCellValue(s);
            cell.CellStyle = sDamTrai12;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(9, 9, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            s = new HSSFRichTextString("Ông:    ");
            row = sheet.CreateRow(rowIndex);

            cell = row.CreateCell(0);
            s.ApplyFont(0, 4, khong11);
            s.ApplyFont(5, s.Length, Dam11);
            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(10, 10, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            s = new HSSFRichTextString("Chức vụ:    ");
            s.ApplyFont(0, 8, khong11);
            s.ApplyFont(9, s.Length, Dam11);
            cell.SetCellValue(s);
            cell.CellStyle = sDamTrai12;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(10, 10, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);

            cell = row.CreateCell(0);
            cell.SetCellValue("B/ ĐẠI DIỆN BÊN B: " + tenncc.ToUpper() + "");
            cell.CellStyle = sDam11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(11, 11, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            s = new HSSFRichTextString("Ông: " + giamdoc + "");
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);

            s.ApplyFont(0, 4, khong11);
            s.ApplyFont(5, s.Length, Dam11);

            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(12, 12, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            s = new HSSFRichTextString("Chức vụ: Giám đốc");

            s.ApplyFont(0, 8, khong11);
            s.ApplyFont(9, s.Length, Dam11);

            cell.SetCellValue(s);
            cell.CellStyle = sDamTrai12;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(12, 12, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            s = new HSSFRichTextString("Bà: " + ketoantruong + "");
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);

            s.ApplyFont(0, 3, khong11);
            s.ApplyFont(4, s.Length, Dam11);

            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(13, 13, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            s = new HSSFRichTextString("Chức vụ: Kế toán trưởng");

            s.ApplyFont(0, 8, khong11);
            s.ApplyFont(9, s.Length, Dam11);

            cell.SetCellValue(s);
            cell.CellStyle = sDamTrai12;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(13, 13, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("         Cùng nhau đối chiếu xác nhận công nợ từ ngày " + ngaybd + " tháng " + thangbd + " năm " + nambd + " đến hết ngày " + ngaykt + " tháng " + thangkt + " năm " + namkt + ". Sau khi kiểm tra đối chiếu chúng tôi đi đến thống nhất như sau:");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 765;
            cra = new CellRangeAddress(14, 14, 0, 7);
            sheet.AddMergedRegion(cra);


            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("STT");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.SetCellValue("Ngày tháng");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Nội dung công việc");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("ĐVT");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.SetCellValue("Số lượng");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.SetCellValue("Đơn giá");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.SetCellValue("Thành tiền");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(7);
            cell.SetCellValue("Ghi chú");
            cell.CellStyle = sDamGiuaBorder11;

            decimal tongduno = 0;
            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            int demdunodauky = rowIndex;
            cell = row.CreateCell(0);
            cell.SetCellValue("I");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(2);
            cell.SetCellValue("Số dư nợ đầu kỳ chuyển sang:");
            cell.CellStyle = sDamTraiBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.SetCellValue(dunotruocky == null ? 0 : double.Parse(dunotruocky.ToString()));
            cell.CellStyle = sDamPhaiBorder11int;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;
            tongduno += dunotruocky == null ? 0 : dunotruocky.Value;

            rowIndex++;
            int dongdunodauky = rowIndex;
            row = sheet.CreateRow(rowIndex);
            int demdunophatsinh = rowIndex;
            cell = row.CreateCell(0);
            cell.SetCellValue("II");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Phát sinh nợ trong kỳ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);

            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;
            int stt = 1;

            int batdau = rowIndex + 1;
            foreach (var item in query)
            {
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue(stt.ToString());
                cell.CellStyle = skhonggiuaBorder105;
                cell.Row.Height = 585;

                cell = row.CreateCell(1);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(2);
                cell.SetCellValue(item.MacBeTong);
                cell.CellStyle = skhongTraiBorder105;
                cell = row.CreateCell(3);
                cell.CellStyle = skhonggiuaBorder105;
                cell.SetCellValue("m3");
                cell = row.CreateCell(4);
                cell.SetCellValue(item.KLHoaDon.Value);
                cell.CellStyle = skhongTraiBorder105float;
                cell = row.CreateCell(5);
                cell.SetCellValue(double.Parse(item.DonGiaHoaDon.ToString()));
                cell.CellStyle = skhongTraiBorder105int;
                cell = row.CreateCell(6);
                cell.SetCellValue(double.Parse(item.TienHoaDon.ToString()));
                cell.CellStyle = sKhongPhaiBorder11int;
                cell = row.CreateCell(7);
                cell.CellStyle = skhongTraiBorder105;
                stt++;
                tongduno += item.TienThanhToan == null ? 0 : item.TienThanhToan.Value;
            }
            foreach (var item in bom)
            {
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue(stt.ToString());
                cell.CellStyle = skhonggiuaBorder105;
                cell.Row.Height = 585;
                cell = row.CreateCell(1);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(2);
                cell.SetCellValue(item.TenHinhThucBom);
                cell.CellStyle = skhongTraiBorder105;
                cell = row.CreateCell(3);
                cell.CellStyle = skhonggiuaBorder105;
                cell.SetCellValue(item.LoaiBom);
                cell = row.CreateCell(4);
                cell.SetCellValue(item.SoLuongHoaDon.Value);
                cell.CellStyle = skhongTraiBorder105float;
                cell = row.CreateCell(5);
                cell.SetCellValue(double.Parse(item.DonGiaHoaDonBom.ToString()));
                cell.CellStyle = skhongTraiBorder105int;
                cell = row.CreateCell(6);
                cell.SetCellValue(double.Parse(item.TienHoaDonBom.ToString()));
                cell.CellStyle = sKhongPhaiBorder11int;
                cell = row.CreateCell(7);
                cell.CellStyle = skhongTraiBorder105;
                stt++;
                tongduno += item.TienHoaDonBom == null ? 0 : item.TienHoaDonBom.Value;
            }
            if (rowIndex >= batdau)
            {
                row = sheet.CreateRow(dongdunodauky);
                cell = row.CreateCell(0);
                cell.SetCellValue("II");
                cell.CellStyle = sDamGiuaBorder11;
                cell.Row.Height = 585;
                cell = row.CreateCell(1);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(2);
                cell.SetCellValue("Phát sinh nợ trong kỳ");
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(3);
                cell.SetCellValue("VNĐ");
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(4);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(5);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(6);
                cell.CellFormula = "SUM(G" + (batdau + 1).ToString() + ":G" + (rowIndex + 1).ToString() + ")";
                cell.CellStyle = sDamPhaiBorder11int;
                cell = row.CreateCell(7);
                cell.CellStyle = sDamGiuaBorder11;
            }

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            int demthanhtoan = rowIndex;
            cell = row.CreateCell(0);
            cell.SetCellValue("IV");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Số thanh toán nợ trong kỳ:");
            cell.CellStyle = sDamTraiBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.SetCellValue(thanhtoankynay == null ? 0 : double.Parse(thanhtoankynay.ToString()));
            cell.CellStyle = sDamPhaiBorder11int;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;
            tongduno = tongduno - thanhtoankynay == null ? 0 : thanhtoankynay.Value;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("V");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Số dư nợ cuối kỳ(V=I+II-III):");
            cell.CellStyle = sDamTraiBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.CellFormula = "SUM(G" + (demdunodauky + 1).ToString() + "+G" + (demdunophatsinh + 1).ToString() + "-G" + (demthanhtoan + 1).ToString() + ")";
            cell.CellStyle = sDamPhaiBorder11int;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("         Như vậy tính đến hết ngày " + ngaykt + " tháng " + thangkt + " năm " + namkt + " bên mua Công ty Cổ phần cổ phần xây dựng và thương mại Phúc Sinh còn nợ bên bán Công ty Cổ phần Thượng Long số tiền là: " + string.Format("{0:N0}", tongduno) + " đồng");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 780;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("( Bằng chữ: " + xl.VietBangChu(double.Parse(tongduno.ToString())) + ")");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 465;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("     Biên bản được lập thành 02 bản, mỗi bên giữ 01 bản, được hai bên thống nhất thống qua ký vào biên bản làm cơ sở cho việc thanh toán./.");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 840;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 7);
            sheet.AddMergedRegion(cra);


            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("ĐẠI DIỆN BÊN A");
            cell.CellStyle = sDamGiua12;
            cell.Row.Height = 585;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            cell.SetCellValue("ĐẠI DIỆN BÊN B");
            cell.CellStyle = sDamGiua12;
            cell.Row.Height = 585;
            cra = new CellRangeAddress(rowIndex, rowIndex, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("         Giám đốc                                Kế toán ");
            cell.CellStyle = sDamGiua12;
            cell.Row.Height = 585;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            cell.SetCellValue("            Giám đốc                               Kế toán ");
            cell.CellStyle = sDamGiua12;
            cell.Row.Height = 330;
            cra = new CellRangeAddress(rowIndex, rowIndex, 3, 7);
            sheet.AddMergedRegion(cra);

            sheet.SetColumnWidth(0, 1369);
            sheet.SetColumnWidth(1, 3145);
            sheet.SetColumnWidth(2, 8695);
            sheet.SetColumnWidth(3, 1887);
            sheet.SetColumnWidth(4, 2257);
            sheet.SetColumnWidth(5, 2923);
            sheet.SetColumnWidth(6, 3737);
            sheet.SetColumnWidth(7, 2701);

            #endregion

            #region sheet giá trị
            sheet = workbook.CreateSheet("GN");
            sheet = xl.SetPropertySheet(sheet);

            rowIndex = 0;

            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
            cell.CellStyle = sDam13;
            cra = new CellRangeAddress(0, 0, 0, 7);
            sheet.AddMergedRegion(cra);
            cell.Row.Height = 585;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("Độc lập - Tự do - Hạnh phúc");
            cell.CellStyle = sDamGachChan12;
            cra = new CellRangeAddress(1, 1, 0, 7);
            sheet.AddMergedRegion(cra);
            cell.Row.Height = 330;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("Số: 01/ĐCCN");
            cell.CellStyle = sDamNghieng10;
            cra = new CellRangeAddress(2, 2, 0, 1);
            sheet.AddMergedRegion(cra);
            cell.Row.Height = 330;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("GHI NHỚ CÔNG NỢ");
            cell.CellStyle = sDam16;
            cra = new CellRangeAddress(3, 3, 0, 7);
            sheet.AddMergedRegion(cra);
            cell.Row.Height = 585;

            rowIndex++;
            rowIndex++;

            ngaythangtext = "ngày " + ngaykyhd.Day.ToString() + " tháng " + ngaykyhd.Month.ToString() + " năm " + ngaykyhd.Year.ToString();

            s = new HSSFRichTextString(" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL  " + ngaythangtext + " giữa Công ty Cổ phần Thượng Long và " + tenncc + " Về việc cung cấp bê tông thương phẩm.");

            lenghtngaythang = ngaythangtext.Length;

            a = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL ").Length;
            a1 = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL " + ngaythangtext + " giữa ").Length;
            a2 = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL " + ngaythangtext + " giữa Công ty Cổ phần Thượng Long ").Length;
            a3 = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL " + ngaythangtext + " giữa Công ty Cổ phần Thượng Long và ").Length;
            a4 = (" - Căn cứ vào hợp đồng kinh tế số: 29/HĐKT/TL " + ngaythangtext + " giữa Công ty Cổ phần Thượng Long và " + tenncc + "").Length;

            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);

            s.ApplyFont(0, a1, Nghieng11);
            s.ApplyFont(a1 + 1, a2, DamNghieng11);
            s.ApplyFont(a2 + 1, a3, Nghieng11);
            s.ApplyFont(a3 + 1, a4, DamNghieng11);
            s.ApplyFont(a4 + 1, s.Length, Nghieng11);

            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 930;
            cra = new CellRangeAddress(5, 5, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("   - Căn cứ vào biên bản xác nhận khối lượng tại công trình.");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 375;
            cra = new CellRangeAddress(6, 6, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("   - Hôm nay, ngày " + ngay + " tháng " + thang + " năm " + nam + ", tại văn phòng Công ty Cổ phần Thượng Long chúng tôi gồm có:");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 375;
            cra = new CellRangeAddress(7, 7, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);

            cell = row.CreateCell(0);
            cell.SetCellValue("A/ ĐẠI DIỆN BÊN A: " + tenncc.ToUpper() + "");
            cell.CellStyle = sDam11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(8, 8, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            s = new HSSFRichTextString("Ông: " + ncc.GiamDoc + "");
            row = sheet.CreateRow(rowIndex);

            cell = row.CreateCell(0);
            s.ApplyFont(0, 4, khong11);
            s.ApplyFont(5, s.Length, Dam11);
            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(9, 9, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            s = new HSSFRichTextString("Chức vụ: Giám đốc");
            s.ApplyFont(0, 8, khong11);
            s.ApplyFont(9, s.Length, Dam11);
            cell.SetCellValue(s);
            cell.CellStyle = sDamTrai12;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(9, 9, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            s = new HSSFRichTextString("Ông:    ");
            row = sheet.CreateRow(rowIndex);

            cell = row.CreateCell(0);
            s.ApplyFont(0, 4, khong11);
            s.ApplyFont(5, s.Length, Dam11);
            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(10, 10, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            s = new HSSFRichTextString("Chức vụ:    ");
            s.ApplyFont(0, 8, khong11);
            s.ApplyFont(9, s.Length, Dam11);
            cell.SetCellValue(s);
            cell.CellStyle = sDamTrai12;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(10, 10, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);

            cell = row.CreateCell(0);
            cell.SetCellValue("B/ ĐẠI DIỆN BÊN B: " + tenncc.ToUpper() + "");
            cell.CellStyle = sDam11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(11, 11, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            s = new HSSFRichTextString("Ông: " + giamdoc + "");
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);

            s.ApplyFont(0, 4, khong11);
            s.ApplyFont(5, s.Length, Dam11);

            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(12, 12, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            s = new HSSFRichTextString("Chức vụ: Giám đốc");

            s.ApplyFont(0, 8, khong11);
            s.ApplyFont(9, s.Length, Dam11);

            cell.SetCellValue(s);
            cell.CellStyle = sDamTrai12;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(12, 12, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            s = new HSSFRichTextString("Bà: " + ketoantruong + "");
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);

            s.ApplyFont(0, 3, khong11);
            s.ApplyFont(4, s.Length, Dam11);

            cell.SetCellValue(s);
            cell.CellStyle = skhong11;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(13, 13, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            s = new HSSFRichTextString("Chức vụ: Kế toán trưởng");

            s.ApplyFont(0, 8, khong11);
            s.ApplyFont(9, s.Length, Dam11);

            cell.SetCellValue(s);
            cell.CellStyle = sDamTrai12;
            cell.Row.Height = 405;
            cra = new CellRangeAddress(13, 13, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("         Cùng nhau đối chiếu xác nhận công nợ từ ngày " + ngaybd + " tháng " + thangbd + " năm " + nambd + " đến hết ngày " + ngaykt + " tháng " + thangkt + " năm " + namkt + ". Sau khi kiểm tra đối chiếu chúng tôi đi đến thống nhất như sau:");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 765;
            cra = new CellRangeAddress(14, 14, 0, 7);
            sheet.AddMergedRegion(cra);


            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("STT");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.SetCellValue("Ngày tháng");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Nội dung công việc");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("ĐVT");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.SetCellValue("Số lượng");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.SetCellValue("Đơn giá");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.SetCellValue("Thành tiền");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(7);
            cell.SetCellValue("Ghi chú");
            cell.CellStyle = sDamGiuaBorder11;

            tongduno = 0;
            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            demdunodauky = rowIndex;
            cell = row.CreateCell(0);
            cell.SetCellValue("I");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(2);
            cell.SetCellValue("Số dư nợ đầu kỳ chuyển sang:");
            cell.CellStyle = sDamTraiBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.SetCellValue(dunotruocky == null ? 0 : double.Parse(dunotruocky.ToString()));
            cell.CellStyle = sDamPhaiBorder11int;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;
            tongduno += dunotruocky == null ? 0 : dunotruocky.Value;

            rowIndex++;
            dongdunodauky = rowIndex;
            row = sheet.CreateRow(rowIndex);
            demdunophatsinh = rowIndex;
            cell = row.CreateCell(0);
            cell.SetCellValue("II");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Phát sinh nợ trong kỳ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);

            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;
            stt = 1;

            batdau = rowIndex + 1;
            foreach (var item in query)
            {
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue(stt.ToString());
                cell.CellStyle = skhonggiuaBorder105;
                cell.Row.Height = 585;

                cell = row.CreateCell(1);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(2);
                cell.SetCellValue(item.MacBeTong);
                cell.CellStyle = skhongTraiBorder105;
                cell = row.CreateCell(3);
                cell.CellStyle = skhonggiuaBorder105;
                cell.SetCellValue("m3");
                cell = row.CreateCell(4);
                cell.SetCellValue(item.KLKhachHang.Value);
                cell.CellStyle = skhongTraiBorder105float;
                cell = row.CreateCell(5);
                cell.SetCellValue(double.Parse(item.DonGiaThanhToan.ToString()));
                cell.CellStyle = skhongTraiBorder105int;
                cell = row.CreateCell(6);
                cell.SetCellValue(double.Parse(item.TienThanhToan.ToString()));
                cell.CellStyle = sKhongPhaiBorder11int;
                cell = row.CreateCell(7);
                cell.CellStyle = skhongTraiBorder105;
                stt++;
                tongduno += item.TienThanhToan == null ? 0 : item.TienThanhToan.Value;
            }
            foreach (var item in bom)
            {
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue(stt.ToString());
                cell.CellStyle = skhonggiuaBorder105;
                cell.Row.Height = 585;

                cell = row.CreateCell(1);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(2);
                cell.SetCellValue(item.TenHinhThucBom);
                cell.CellStyle = skhongTraiBorder105;
                cell = row.CreateCell(3);
                cell.CellStyle = skhonggiuaBorder105;
                cell.SetCellValue(item.LoaiBom);
                cell = row.CreateCell(4);
                cell.SetCellValue(item.SoLuongThanhToan.Value);
                cell.CellStyle = skhongTraiBorder105float;
                cell = row.CreateCell(5);
                cell.SetCellValue(double.Parse(item.DonGiaThanhToanBom.ToString()));
                cell.CellStyle = skhongTraiBorder105int;
                cell = row.CreateCell(6);
                cell.SetCellValue(double.Parse(item.TienThanhToanBom.ToString()));
                cell.CellStyle = sKhongPhaiBorder11int;
                cell = row.CreateCell(7);
                cell.CellStyle = skhongTraiBorder105;
                stt++;
                tongduno += item.TienThanhToanBom == null ? 0 : item.TienThanhToanBom.Value;
            }
            if (rowIndex >= batdau)
            {
                row = sheet.CreateRow(dongdunodauky);
                cell = row.CreateCell(0);
                cell.SetCellValue("II");
                cell.CellStyle = sDamGiuaBorder11;
                cell.Row.Height = 585;
                cell = row.CreateCell(1);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(2);
                cell.SetCellValue("Phát sinh nợ trong kỳ");
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(3);
                cell.SetCellValue("VNĐ");
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(4);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(5);
                cell.CellStyle = sDamGiuaBorder11;
                cell = row.CreateCell(6);
                cell.CellFormula = "SUM(G" + (batdau + 1).ToString() + ":G" + (rowIndex + 1).ToString() + ")";
                cell.CellStyle = sDamPhaiBorder11int;
                cell = row.CreateCell(7);
                cell.CellStyle = sDamGiuaBorder11;
            }

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            int demthuthue = rowIndex;
            cell = row.CreateCell(0);
            cell.SetCellValue("III");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Thuế phải thu:");
            cell.CellStyle = sDamTraiBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.SetCellValue(thanhtoankynay == null ? 0 : double.Parse(thanhtoankynay.ToString()));
            cell.CellStyle = sDamPhaiBorder11int;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;
            //tongduno += thuephaithu;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            demthanhtoan = rowIndex;
            cell = row.CreateCell(0);
            cell.SetCellValue("IV");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Số thanh toán nợ trong kỳ:");
            cell.CellStyle = sDamTraiBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.SetCellValue(thanhtoankynay == null ? 0 : double.Parse(thanhtoankynay.ToString()));
            cell.CellStyle = sDamPhaiBorder11int;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;
            tongduno = tongduno - thanhtoankynay == null ? 0 : thanhtoankynay.Value;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("V");
            cell.CellStyle = sDamGiuaBorder11;
            cell.Row.Height = 585;
            cell = row.CreateCell(1);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(2);
            cell.SetCellValue("Số dư nợ cuối kỳ(V=I+II+III-IV):");
            cell.CellStyle = sDamTraiBorder11;
            cell = row.CreateCell(3);
            cell.SetCellValue("VNĐ");
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(4);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(5);
            cell.CellStyle = sDamGiuaBorder11;
            cell = row.CreateCell(6);
            cell.CellFormula = "SUM(G" + (demdunodauky + 1).ToString() + "+G" + (demdunophatsinh + 1).ToString() + "+G" + (demthuthue + 1).ToString() + "-G" + (demthanhtoan + 1).ToString() + ")";
            cell.CellStyle = sDamPhaiBorder11int;
            cell = row.CreateCell(7);
            cell.CellStyle = sDamGiuaBorder11;

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("         Như vậy tính đến hết ngày " + ngaykt + " tháng " + thangkt + " năm " + namkt + " bên mua Công ty Cổ phần cổ phần xây dựng và thương mại Phúc Sinh còn nợ bên bán Công ty Cổ phần Thượng Long số tiền là: " + string.Format("{0:N0}", tongduno) + " đồng");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 780;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("( Bằng chữ: " + xl.VietBangChu(double.Parse(tongduno.ToString())) + ")");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 465;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("     Biên bản được lập thành 02 bản, mỗi bên giữ 01 bản, được hai bên thống nhất thống qua ký vào biên bản làm cơ sở cho việc thanh toán./.");
            cell.CellStyle = sNghieng11;
            cell.Row.Height = 840;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("ĐẠI DIỆN BÊN A");
            cell.CellStyle = sDamGiua12;
            cell.Row.Height = 585;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            cell.SetCellValue("ĐẠI DIỆN BÊN B");
            cell.CellStyle = sDamGiua12;
            cell.Row.Height = 585;
            cra = new CellRangeAddress(rowIndex, rowIndex, 3, 7);
            sheet.AddMergedRegion(cra);

            rowIndex++;
            row = sheet.CreateRow(rowIndex);
            cell = row.CreateCell(0);
            cell.SetCellValue("         Giám đốc                                Kế toán ");
            cell.CellStyle = sDamGiua12;
            cell.Row.Height = 585;
            cra = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
            sheet.AddMergedRegion(cra);

            cell = row.CreateCell(3);
            cell.SetCellValue("            Giám đốc                               Kế toán ");
            cell.CellStyle = sDamGiua12;
            cell.Row.Height = 330;
            cra = new CellRangeAddress(rowIndex, rowIndex, 3, 7);
            sheet.AddMergedRegion(cra);

            sheet.SetColumnWidth(0, 1369);
            sheet.SetColumnWidth(1, 3145);
            sheet.SetColumnWidth(2, 8695);
            sheet.SetColumnWidth(3, 1887);
            sheet.SetColumnWidth(4, 2257);
            sheet.SetColumnWidth(5, 2923);
            sheet.SetColumnWidth(6, 3737);
            sheet.SetColumnWidth(7, 2701);

            #endregion


            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);

                #region [Set title dưới]

                string saveAsFileName = "";
                string Bienso = "";

                if (txtTuNgay.Text.Trim() == txtDenNgay.Text.Trim())
                {
                    saveAsFileName = "BÁO CÁO DOANH THU BÁN BÊ TÔNG -" + tenncc.ToUpper() + "-Ngày: " + "-" + txtTuNgay.Text +
                                     ".xls";
                }
                else if (txtTuNgay.Text.Trim() != txtDenNgay.Text.Trim())
                {
                    saveAsFileName = "BÁO CÁO DOANH THU BÁN BÊ TÔNG -" + tenncc.ToUpper() + "-Từ ngày" + "-" + txtTuNgay.Text +
                                     "-Đến ngày" + "-" + txtDenNgay.Text + ".xls";
                }

                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                #endregion

                Response.Clear();
                Response.BinaryWrite(exportData.GetBuffer());
                Response.End();
            }
        }

        private Guid GetDrop(DropDownList dl)
        {
            if (dl.SelectedValue.Trim().Equals(""))
            {
                return Guid.Empty;
            }
            else
            {
                return new Guid(dl.SelectedValue);
            }
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        protected void grdDanhSachHD_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#EEFFAA'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            hdPage.Value = "1";
            Search(CurrentPage);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string ID = e.CommandArgument.ToString();
            hdID.Value = ID;

            var query = (from p in db.tblXuatBeTongs
                         where p.ID == new Guid(ID)
                         select p).FirstOrDefault();
            if (e.CommandName.Equals("PrintKhachHang"))
            {
                Print(query.IDNhaCungCap.ToString());
            }
        }
        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[9].Visible = false;

                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Mác bê tông",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin khối lượng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tiền bê tông",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tiền bơm",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Phụ phí",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Tổng tiền",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void gvLichSu_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;

                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Người tạo",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Ngày tạo",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Mác bê tông - Vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 8,
                    CssClass = "HeaderStyle",
                    BackColor = Color.Beige,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Định mức vật liệu",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 6,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }

        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNVKD();
        }

    }
}