﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_BaoCaoDoanhThuBanBeTong.ascx.cs" Inherits="ThuongLong.UCBaoCaoBeTong.uc_BaoCaoDoanhThuBanBeTong" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="panel-body">
            <div class="row">
                <div class="form-group col-lg-3">
                    <label for="exampleInputEmail1">Từ ngày</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox ID="txtTuNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""
                            Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group col-lg-3">
                    <label for="exampleInputEmail1">Đến ngày</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox ID="txtDenNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""
                            Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="col-lg-3" runat="server">
                    <div class="form-group">
                        <label>Chi nhánh</label>
                        <asp:DropDownList ID="dlChiNhanh" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2"
                            Width="98%" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3" runat="server">
                    <div class="form-group">
                        <label>Loại khách hàng</label>
                        <asp:DropDownList ID="dlLoaiKhachHang" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%" OnSelectedIndexChanged="dlLoaiKhachHang_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3" runat="server">
                    <div class="form-group">
                        <label>Khách hàng</label>
                        <asp:DropDownList ID="dlNhaCungCap" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Xem</asp:LinkButton>
                        <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                        <%--<asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>--%>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" ShowFooter="true" OnRowCreated="GV_RowCreated"
                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                <Columns>
                    <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                    <asp:TemplateField HeaderText="Khách hàng">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblLichSu"
                                Text='<%#Eval("TenNhaCungCap")%>' runat="server" CssClass="padding" CommandName="PrintKhachHang"
                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TenCongTrinh" HeaderText="Công trình" />
                    <asp:BoundField DataField="HangMuc" HeaderText="Hạng mục" />
                    <asp:BoundField DataField="TenNhanVien" HeaderText="NVKD" />
                    <asp:BoundField DataField="TenMacBeTong" HeaderText="Mác bê tông" />
                    <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" />
                    <asp:BoundField DataField="KLThucXuat" HeaderText="KL thực xuất" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLKhachHang" HeaderText="KL bán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLQuaCan" HeaderText="KL qua cân" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="DonGiaHoaDon" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="DonGiaThanhToan" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="TienHoaDon" HeaderText="Thành tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="TienThanhToan" HeaderText="Thành tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="DonGiaHoaDonBom" HeaderText="Đơn giá hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="DonGiaThanhToanBom" HeaderText="Đơn giá thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="TienHoaDonBom" HeaderText="Thành tiền hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="TienThanhToanBom" HeaderText="Thành tiền thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="PhuPhi" HeaderText="Phụ phí" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="TongHoaDon" HeaderText="Tổng hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="TongThanhToan" HeaderText="Tổng thanh toán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                    HorizontalAlign="Right" />
                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
            </asp:GridView>
        </div>
        <div style="margin: 5px;" class="btn-group">
            <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
            <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
            <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
        </div>

        <ajaxToolkit:ModalPopupExtender ID="mpDuyet" runat="server" CancelControlID="btnDong"
            Drag="True" TargetControlID="hdDuyet" BackgroundCssClass="modalBackground" PopupControlID="pnDuyet"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnDuyet" runat="server" Style="display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    DUYỆT
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12">
                        <label for="exampleInputEmail1">Lý do không duyệt</label>
                        <asp:TextBox ID="txtMota" runat="server" class="form-control" placeholder="Lý do không duyệt" Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="panel-footer">

                    <asp:Button ID="btnDong" runat="server" CssClass="btn btn-danger"
                        Text="Đóng" />
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hdID" runat="server" />
        <asp:HiddenField ID="hdPage" runat="server" />
        <asp:HiddenField ID="hdDuyet" runat="server" />
        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
        <asp:HiddenField ID="hdTuNgay" runat="server" />
        <asp:HiddenField ID="hdDenNgay" runat="server" />

        <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
            Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center">
                    Lịch sử
                </div>
                <div class="panel-body" style="max-height: 500px; overflow: auto;">
                    <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound" OnRowCreated="gvLichSu_RowCreated"
                        EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                        <Columns>
                            <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                            <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                            <asp:BoundField DataField="MacBeTong" HeaderText="Mác" />
                            <asp:BoundField DataField="XiMang" HeaderText="Xi măng" />
                            <asp:BoundField DataField="Cat" HeaderText="Cát" />
                            <asp:BoundField DataField="Da" HeaderText="Đá" />
                            <asp:BoundField DataField="PhuGia" HeaderText="Phụ gia" />
                            <asp:BoundField DataField="TroBay" HeaderText="Tro bay" />
                            <asp:BoundField DataField="DMXiMang" HeaderText="Xi măng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DMDa" HeaderText="Đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DMCat" HeaderText="Cát" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DMPhuGia" HeaderText="Phụ gia" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DMTroBay" HeaderText="Tro bay" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            <%--<asp:BoundField DataField="TyLeQuyDoi" HeaderText="Tỷ lệ quy đổi" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />--%>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer" style="align-content: center; text-align: center">
                    <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                </div>
            </div>
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnPrint" />
        <asp:PostBackTrigger ControlID="GV" />
    </Triggers>
</asp:UpdatePanel>


