﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DoiMatKhau.aspx.cs" Inherits="ThuongLong.DoiMatKhau" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Đăng nhập</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="Content/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="Content/jquery.modal.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="favicontl.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body class="hold-transition register-page">
    <form id="form1" runat="server">
        <div class="register-box">
            <div class="register-logo">
                <a href="#"><b>THƯỢNG</b>LONG</a>
            </div>
            <!-- /.register-logo -->
            <div class="register-box-body">
                <p class="register-box-msg">Quên mật khẩu</p>

                <div class="form-group has-feedback">

                    <label>Tên đăng nhập</label>
                    <asp:TextBox ID="txtUsername" runat="server" class="form-control" placeholder="Tên đăng nhập"></asp:TextBox>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group">
                    <label>Câu hỏi 1</label>
                    <asp:DropDownList CssClass="form-control select2" Width="100%" data-toggle="tooltip" data-original-title="Câu hỏi 2"
                        Font-Size="13px" DataTextField="TenCauHoi" DataValueField="ID" ID="dlCauHoi1" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtTraLoi1" runat="server" class="form-control" placeholder="Câu trả lời 2"></asp:TextBox>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group">
                    <label>Câu hỏi 2</label>
                    <asp:DropDownList CssClass="form-control select2" Width="100%" data-toggle="tooltip" data-original-title="Câu hỏi 2"
                        Font-Size="13px" DataTextField="TenCauHoi" DataValueField="ID" ID="dlCauHoi2" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtTraLoi2" runat="server" class="form-control" placeholder="Câu trả lời 2"></asp:TextBox>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <label>Mật khẩu mới</label>
                    <asp:TextBox ID="txtPassword" runat="server" class="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <asp:Button ID="btnRegister" runat="server" Text="Đổi mật khẩu" class="btn btn-primary btn-block btn-flat" OnClick="btnRegister_Click" />
                </div>
                <a href="Login.aspx">Đăng nhập</a>
            </div>
            <!-- /.register-box-body -->
        </div>
        <!-- /.register-box -->

        <!-- jQuery 3 -->
        <script src="Scripts/jquery-3.4.1.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="Scripts/bootstrap.min.js"></script>
        <script src="Scripts/jquery.modal.min.js"></script>
        <!-- iCheck -->
        <script src="plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' /* optional */
                });
            });
        </script>
        <script type="text/javascript">
            function success(s) {
                modal({
                    type: 'success',
                    title: 'Thông báo',
                    text: s,
                    size: "small",
                });
            };
            function warning(s) {
                modal({
                    type: 'warning',
                    title: 'Thông báo lỗi',
                    text: s,
                    size: "small",
                });
            };
            function inverted(s) {
                modal({
                    type: 'error',
                    title: 'Lỗi',
                    text: s,
                    size: "small",
                });
            };
        </script>
    </form>
</body>
</html>
