﻿<%@ Page Title="Sản xuất gạch Terrazo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GachTerrazo.aspx.cs" Inherits="ThuongLong.GachTerrazo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ngày tháng</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtNgayThang" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Chi nhánh</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Loại gạch</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại gạch"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiGach" runat="server" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3" id="div2" runat="server">
                        <label for="exampleInputEmail1">Thời tiết</label>
                        <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Thời tiết"
                            Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThoiTiet" runat="server" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Nắng</asp:ListItem>
                            <asp:ListItem Value="1">Mưa</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số lượng đạt</label>
                        <asp:TextBox ID="txtSoLuong" runat="server" class="form-control" placeholder="Số viên loại(lớp mặt)" Width="98%" OnTextChanged="txtSoLuong_TextChanged" AutoPostBack="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtSoLuong" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Số lượng hao hụt</label>
                        <asp:TextBox ID="txtSoLuongHaoHut" runat="server" class="form-control" placeholder="Số viên loại(lớp mặt)" Width="98%" OnTextChanged="txtSoLuong_TextChanged" AutoPostBack="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtSoLuongHaoHut" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Màu xi</label>
                        <asp:TextBox ID="txtTLMauXi" runat="server" class="form-control" placeholder="Màu xi" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLMauXi" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Màu đỏ</label>
                        <asp:TextBox ID="txtTLMauDo" runat="server" class="form-control" placeholder="Màu đỏ" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLMauDo" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Bột đá</label>
                        <asp:TextBox ID="txtTLBotDa" runat="server" class="form-control" placeholder="Bột đá" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLBotDa" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá đen 2 ly</label>
                        <asp:TextBox ID="txtTLDaDen2Ly" runat="server" class="form-control" placeholder="Đá đen 2 ly" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLDaDen2Ly" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá trắng 2 ly</label>
                        <asp:TextBox ID="txtTLDaTrang2Ly" runat="server" class="form-control" placeholder="Đá trắng 2 ly" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLDaTrang2Ly" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Đá trắng 4 ly</label>
                        <asp:TextBox ID="txtTLDaTrang4Ly" runat="server" class="form-control" placeholder="Đá trắng 4 ly" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLDaTrang4Ly" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xi măng PCB40(lớp mặt)</label>
                        <asp:TextBox ID="txtTLXiMangPCB401" runat="server" class="form-control" placeholder="Xi măng PCB40(lớp mặt)" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLXiMangPCB401" ValidChars=".," />
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Xi măng PCB40(lớp thân)</label>
                        <asp:TextBox ID="txtTLXiMangPCB402" runat="server" class="form-control" placeholder="Xi măng PCB40(lớp thân)" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLXiMangPCB402" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Mạt đá</label>
                        <asp:TextBox ID="txtTLMatDa" runat="server" class="form-control" placeholder="Mạt đá" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLMatDa" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Cát sông đà</label>
                        <asp:TextBox ID="txtTLCatSongDa" runat="server" class="form-control" placeholder="Cát sông đà" Width="98%" ReadOnly="true"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="txtTLCatSongDa" ValidChars=".," />
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="exampleInputEmail1">Ghi chú</label>
                        <asp:TextBox ID="txtGhiChu" runat="server" class="form-control" placeholder="Ghi chú"></asp:TextBox>
                    </div>

                    <div id="divan" runat="server" visible="false">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Màu xi</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMauXi"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Màu đỏ</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm vật liệu"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMauDo" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Bột đá</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhóm vật liệu"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlBotDa" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đá đen 2 ly</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại vật liệu"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDaDen2Ly" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đá trắng 2 ly</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại vật liệu"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDaTrang2Ly" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đá trắng 4 ly</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDaTrang4Ly" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Xi măng PCB40(lớp mặt)</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMangPCB401" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Xi măng PCB40(lớp thân)</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMangPCB402" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Mạt đá</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMatDa" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Cát sông đà</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Đơn vị tính"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCatSongDa" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Màu xi</label>
                            <asp:TextBox ID="txtMauXi" runat="server" class="form-control" placeholder="Màu xi" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtMauXi" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Màu đỏ</label>
                            <asp:TextBox ID="txtMauDo" runat="server" class="form-control" placeholder="Màu đỏ" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtMauDo" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Bột đá</label>
                            <asp:TextBox ID="txtBotDa" runat="server" class="form-control" placeholder="Bột đá" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtBotDa" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đá đen 2 ly</label>
                            <asp:TextBox ID="txtDaDen2Ly" runat="server" class="form-control" placeholder="Đá đen 2 ly" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDaDen2Ly" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đá trắng 2 ly</label>
                            <asp:TextBox ID="txtDaTrang2Ly" runat="server" class="form-control" placeholder="Đá trắng 2 ly" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDaTrang2Ly" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đá trắng 4 ly</label>
                            <asp:TextBox ID="txtDaTrang4Ly" runat="server" class="form-control" placeholder="Đá trắng 4 ly" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDaTrang4Ly" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Xi măng PCB40(lớp mặt)</label>
                            <asp:TextBox ID="txtXiMangPCB401" runat="server" class="form-control" placeholder="Xi măng PCB40(lớp mặt)" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtXiMangPCB401" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Xi măng PCB40(lớp thân)</label>
                            <asp:TextBox ID="txtXiMangPCB402" runat="server" class="form-control" placeholder="Xi măng PCB40(lớp thân)" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtXiMangPCB402" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Mạt đá</label>
                            <asp:TextBox ID="txtMatDa" runat="server" class="form-control" placeholder="Mạt đá" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtMatDa" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Cát sông đà</label>
                            <asp:TextBox ID="txtCatSongDa" runat="server" class="form-control" placeholder="Cát sông đà" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtCatSongDa" ValidChars=".," />
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                    <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách nhập gạch</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                                DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-warning" OnClick="btnPrint_Click" Visible="false"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                            <%--<asp:LinkButton ID="btnOpenChot" runat="server" class="btn btn-app bg-orange" OnClick="btnOpenChot_Click"><i class="fa fa-close"></i>Chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnOpenMoChot" runat="server" class="btn btn-app bg-aqua" OnClick="btnOpenMoChot_Click"><i class="fa fa-opencart"></i>Mở chốt</asp:LinkButton>
                            --%>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12" style="overflow: auto; width: 100%;">
                        <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" Width="100%" OnRowCreated="GV_RowCreated" ShowFooter="true"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8px" ItemStyle-Width="8px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8px" ItemStyle-Width="8px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                <asp:BoundField DataField="TenLoaiGach" HeaderText="Loại gạch" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuong" HeaderText="Số lượng đạt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuongHaoHut" HeaderText="Số lượng hao hụt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMauXi" HeaderText="Màu xi" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMauDo" HeaderText="Màu đỏ" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLBotDa" HeaderText="Bột đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaDen2ly" HeaderText="Đá đen 2 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaTrang2Ly" HeaderText="Đá trắng 2 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaTrang4Ly" HeaderText="Đá trắng 4 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLXiMangPCB401" HeaderText="Xi măng PCB40" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLXiMangPCB402" HeaderText="Xi măng PCB40" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMatDa" HeaderText="Mạt đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLCatSongDa" HeaderText="Cát sông đà" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="GhiChu" HeaderText="Ghi chú" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdCan" runat="server" Value="" />
            <asp:HiddenField ID="hdChot" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:TemplateField HeaderText="Trạng thái" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                <asp:BoundField DataField="TenLoaiGach" HeaderText="Loại gạch" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuong" HeaderText="Số lượng đạt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuongHaoHut" HeaderText="Số lượng hao hụt" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMauXi" HeaderText="Màu xi" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMauDo" HeaderText="Màu đỏ" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLBotDa" HeaderText="Bột đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaDen2ly" HeaderText="Đá đen 2 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaTrang2Ly" HeaderText="Đá trắng 2 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaTrang4Ly" HeaderText="Đá trắng 4 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLXiMangPCB401" HeaderText="Xi măng PCB40" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLXiMangPCB402" HeaderText="Xi măng PCB40" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMatDa" HeaderText="Mạt đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLCatSongDa" HeaderText="Cát sông đà" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="GhiChu" HeaderText="Ghi chú" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpChot" runat="server" CancelControlID="btnDongChot"
                Drag="True" TargetControlID="hdChot" BackgroundCssClass="modalBackground" PopupControlID="pnChot"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnChot" runat="server" Style="width: 98%; height: 100%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        <asp:Label ID="lblChot" runat="server" Text="Chốt nhập kho vật liệu"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh chốt"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhChot" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Ngày chốt</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayChot" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <asp:LinkButton ID="lbtSearchChot" runat="server" class="btn btn-app bg-green" OnClick="lbtSearchChot_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnChot" runat="server" class="btn btn-app bg-orange" OnClick="btnChot_Click"><i class="fa fa-close"></i>Chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnMoChot" runat="server" class="btn btn-app bg-aqua" OnClick="btnMoChot_Click"><i class="fa fa-opencart"></i>Mở chốt</asp:LinkButton>
                            <asp:LinkButton ID="btnDongChot" runat="server" class="btn btn-app bg-warning"><i class="fa fa-opencart"></i>Đóng</asp:LinkButton>
                        </div>
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="GVChot" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound" ShowFooter="true" OnRowCreated="GVChot_RowCreated"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Trạng thái" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblLichSu"
                                            Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                            CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="ThoiTiet" HeaderText="Thời tiết" />
                                <asp:BoundField DataField="TenLoaiGach" HeaderText="Loại gạch" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoLuong" HeaderText="Số lượng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMauXi" HeaderText="Màu xi" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMauDo" HeaderText="Màu đỏ" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLBotDa" HeaderText="Bột đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaDen2ly" HeaderText="Đá đen 2 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaTrang2Ly" HeaderText="Đá trắng 2 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLDaTrang4Ly" HeaderText="Đá trắng 4 ly" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLXiMangPCB401" HeaderText="Xi măng PCB40" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLXiMangPCB402" HeaderText="Xi măng PCB40" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLMatDa" HeaderText="Mạt đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TLCatSongDa" HeaderText="Cát sông đà" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="GhiChu" HeaderText="Ghi chú" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
