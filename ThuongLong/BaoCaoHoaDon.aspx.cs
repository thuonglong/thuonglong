﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class BaoCaoHoaDon : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        clsXuLy xl = new clsXuLy();
        DBDataContext db = new DBDataContext();
        SoQuyDataContext soquy = new SoQuyDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmBaoCaoHoaDon";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmBaoCaoHoaDon", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        LoadChiNhanhSearch();
                        LoadNhaCungCapSearch();
                        hdPage.Value = "1";
                    }
                }
            }
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNhaCungCapSearch()
        {
            dlNhaCungCapSearch.Items.Clear();
            var query = soquy.sp_BaoCaoHoaDon_LoadNhaCungCap(GetDrop(dlChiNhanhSearch), DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text)));
            dlNhaCungCapSearch.DataSource = query;
            dlNhaCungCapSearch.DataBind();
            dlNhaCungCapSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = soquy.sp_BaoCaoHoaDon_Search(GetDrop(dlChiNhanhSearch), dlNhaCungCapSearch.SelectedValue, int.Parse(dlDuNo.SelectedValue),
            DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text)), 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }

            //if (GV.Rows.Count > 0)
            //{
            //    var footer = soquy.sp_BaoCaoHoaDon_GetFooter(GetDrop(dlChiNhanhSearch), dlNhaCungCapSearch.SelectedValue,
            //    DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();
            //    if (footer != null)
            //    {
            //        GV.FooterRow.Cells[0].ColumnSpan = 8;
            //        GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", footer.SoLuong);
            //        GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

            //        GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", footer.SumTienCoThue);
            //        GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", footer.SumTienKhongThue);

            //        GV.FooterRow.Cells[3].Visible = false;
            //        GV.FooterRow.Cells[4].Visible = false;
            //        GV.FooterRow.Cells[5].Visible = false;
            //        GV.FooterRow.Cells[6].Visible = false;
            //        GV.FooterRow.Cells[7].Visible = false;
            //        GV.FooterRow.Cells[8].Visible = false;
            //        GV.FooterRow.Cells[9].Visible = false;
            //        //GV.FooterRow.Cells[17].Visible = false;
            //        //GV.FooterRow.Cells[18].Visible = false;
            //        //GV.FooterRow.Cells[19].Visible = false;
            //        //GV.FooterRow.Cells[20].Visible = false;
            //    }
            //}

        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCapSearch();
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //string url = "";
            //string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 5, TagName, ref url);
            //if (thongbao != "")
            //{
            //    if (url != "")
            //    {
            //        GstGetMess(thongbao, url);
            //    }
            //    else
            //    {
            //        Warning(thongbao);
            //    }
            //}
            //else
            //{
            //    List<sp_BaoCaoHoaDon_PrintResult> query = soquy.sp_BaoCaoHoaDon_Print(GetDrop(dlChiNhanhSearch), dlNhaCungCapSearch.SelectedValue,
            //DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            //    if (query.Count > 0)
            //    {
            //        string dc = "", tencongty = "", sdt = "";
            //        var tencty = (from x in db.tblThamSos
            //                      select x).FirstOrDefault();
            //        if (tencty != null && tencty.ID != null)
            //        {
            //            dc = tencty.DiaChi;
            //            sdt = tencty.SoDienThoai;
            //            tencongty = tencty.TenCongTy;
            //        }


            //        var workbook = new HSSFWorkbook();
            //        IDataFormat dataFormatCustom = workbook.CreateDataFormat();
            //        string sheetname = "BÁO CÁO HÓA ĐƠN";

            //        #region[CSS]

            //        var sheet = workbook.CreateSheet(sheetname);
            //        sheet = xl.SetPropertySheet(sheet);

            //        IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
            //        IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
            //        IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
            //        IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

            //        ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
            //        ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
            //        ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
            //        ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
            //        ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
            //        ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
            //        ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
            //        ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

            //        ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
            //        ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

            //        ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
            //        ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

            //        ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
            //        ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
            //        ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
            //        #endregion

            //        #region[Tạo header trên]

            //        var rowIndex = 0;
            //        var row = sheet.CreateRow(rowIndex);
            //        ICell r1c1 = row.CreateCell(0);
            //        r1c1.SetCellValue(tencongty);
            //        r1c1.CellStyle = styleboldcenternoborder;
            //        r1c1.Row.Height = 400;
            //        CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 2);
            //        sheet.AddMergedRegion(cra);
            //        rowIndex++;
            //        row = sheet.CreateRow(rowIndex);
            //        r1c1 = row.CreateCell(0);
            //        r1c1.SetCellValue(dc);
            //        r1c1.CellStyle = styleboldcenternoborder;
            //        r1c1.Row.Height = 400;
            //        cra = new CellRangeAddress(1, 1, 0, 2);
            //        sheet.AddMergedRegion(cra);
            //        //Tiêu đề báo cáo

            //        rowIndex++;
            //        row = sheet.CreateRow(rowIndex);
            //        r1c1 = row.CreateCell(0);

            //        if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
            //        {
            //            r1c1.SetCellValue("BÁO CÁO HÓA ĐƠN TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
            //                              "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
            //        }
            //        else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
            //        {
            //            r1c1.SetCellValue("BÁO CÁO HÓA ĐƠN NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
            //        }

            //        r1c1.CellStyle = styleHeader1;
            //        r1c1.Row.Height = 800;
            //        cra = new CellRangeAddress(2, 2, 0, 7);
            //        sheet.AddMergedRegion(cra);

            //        rowIndex++;
            //        row = sheet.CreateRow(rowIndex);

            //        r1c1 = row.CreateCell(0);
            //        string kq = "";

            //        r1c1.SetCellValue(kq);



            //        r1c1.CellStyle = styleHeader2;
            //        r1c1.Row.Height = 500;
            //        cra = new CellRangeAddress(3, 3, 0, 7);
            //        sheet.AddMergedRegion(cra);
            //        sheet.CreateFreezePane(0, 6);
            //        #endregion

            //        #region[Tạo header dưới]
            //        rowIndex++;
            //        row = sheet.CreateRow(rowIndex);
            //        //for (int i = 1; i <= 60; i++)
            //        //{
            //        //    cra = new CellRangeAddress(4, 5, i, i);
            //        //    sheet.AddMergedRegion(cra);
            //        //}

            //        sheet.AddMergedRegion(cra);
            //        string[] header1 =
            //          {
            //        "STT",
            //        "Ngày tháng",
            //        "Chi nhánh",
            //        "Nhà cung cấp",
            //        "Tên hóa đơn",
            //        "Hóa đơn phải thu",
            //        "Hóa đơn phải trả",
            //        "Trạng thái"
            //      };

            //        for (int h = 0; h < header1.Length; h++)
            //        {
            //            r1c1 = row.CreateCell(h);
            //            r1c1.SetCellValue(header1[h].ToString());
            //            r1c1.CellStyle = styleHeaderChiTietCenter;
            //            r1c1.Row.Height = 500;
            //        }

            //        rowIndex++;
            //        row = sheet.CreateRow(rowIndex);

            //        string[] header2 =
            //            {
            //        "Ngày tháng",
            //        "Chi nhánh",
            //        "Nhà cung cấp",
            //        "Tên hóa đơn",
            //        "Hóa đơn phải thu",
            //        "Hóa đơn phải trả",
            //        "Trạng thái"
            //    };
            //        var cell = row.CreateCell(0);

            //        cell.SetCellValue("STT");
            //        cell.CellStyle = styleHeaderChiTietCenter;
            //        cell.Row.Height = 600;

            //        cra = new CellRangeAddress(4, 5, 0, 0);
            //        sheet.AddMergedRegion(cra);
            //        cra = new CellRangeAddress(4, 5, 1, 1);
            //        sheet.AddMergedRegion(cra);
            //        cra = new CellRangeAddress(4, 5, 2, 2);
            //        sheet.AddMergedRegion(cra);
            //        cra = new CellRangeAddress(4, 5, 3, 3);
            //        sheet.AddMergedRegion(cra);
            //        cra = new CellRangeAddress(4, 5, 4, 4);
            //        sheet.AddMergedRegion(cra);
            //        cra = new CellRangeAddress(4, 5, 5, 5);
            //        sheet.AddMergedRegion(cra);
            //        cra = new CellRangeAddress(4, 5, 6, 6);
            //        sheet.AddMergedRegion(cra);
            //        cra = new CellRangeAddress(4, 5, 7, 7);
            //        sheet.AddMergedRegion(cra);

            //        for (int hk = 0; hk < header2.Length; hk++)
            //        {
            //            r1c1 = row.CreateCell(hk + 1);
            //            r1c1.SetCellValue(header2[hk].ToString());
            //            r1c1.CellStyle = styleHeaderChiTietCenter;
            //            r1c1.Row.Height = 800;
            //        }
            //        #endregion

            //        #region[ghi dữ liệu]
            //        int STT2 = 0;

            //        string RowDau = (rowIndex + 2).ToString();
            //        foreach (var item in query)
            //        {
            //            STT2++;
            //            rowIndex++;
            //            row = sheet.CreateRow(rowIndex);

            //            cell = row.CreateCell(0);
            //            cell.SetCellValue(STT2);
            //            cell.CellStyle = styleBodyIntCenter;

            //            cell = row.CreateCell(1);
            //            cell.SetCellValue(item.NgayNhan.Value);
            //            cell.CellStyle = styleBodyDatetime;

            //            cell = row.CreateCell(2);
            //            cell.SetCellValue(item.TenChiNhanh.ToString());
            //            cell.CellStyle = styleBody;

            //            cell = row.CreateCell(3);
            //            cell.SetCellValue(item.NhaCungCap.ToString());
            //            cell.CellStyle = styleBody;

            //            cell = row.CreateCell(4);
            //            cell.SetCellValue(item.TenHoaDon.ToString());
            //            cell.CellStyle = styleBody;

            //            cell = row.CreateCell(5);
            //            cell.SetCellValue(double.Parse(item.HoaDonPhaiThu.ToString()));
            //            cell.CellStyle = styleBodyIntRight;

            //            cell = row.CreateCell(6);
            //            cell.SetCellValue(double.Parse(item.HoaDonPhaiTra.ToString()));
            //            cell.CellStyle = styleBodyIntRight;

            //            cell = row.CreateCell(7);
            //            cell.SetCellValue(item.TrangThaiText.ToString());
            //            cell.CellStyle = styleBody;

            //        }
            //        #endregion

            //        #region Tổng cộng
            //        rowIndex++;
            //        string RowCuoi = rowIndex.ToString();
            //        row = sheet.CreateRow(rowIndex);
            //        for (int i = 1; i <= 7; i++)
            //        {
            //            cell = row.CreateCell(i);
            //            cell.CellStyle = styleBodyIntRight;
            //            cell.Row.Height = 400;
            //        }
            //        cell = row.CreateCell(0);
            //        cell.SetCellValue("Tổng cộng:");
            //        cell.Row.Height = 400;
            //        cell.CellStyle = styleboldrightborder;

            //        cra = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
            //        sheet.AddMergedRegion(cra);

            //        cell = row.CreateCell(3);
            //        cell.CellFormula = "COUNTA(B" + RowDau.ToString() + ":B" + RowCuoi.ToString() + ")";
            //        cell.CellStyle = styleBodyIntFooterRight;

            //        cell = row.CreateCell(5);
            //        cell.CellFormula = "SUM(F" + RowDau.ToString() + ":F" + RowCuoi.ToString() + ")";
            //        cell.CellStyle = styleBodyIntRight;

            //        cell = row.CreateCell(6);
            //        cell.CellFormula = "SUM(G" + RowDau.ToString() + ":G" + RowCuoi.ToString() + ")";
            //        cell.CellStyle = styleBodyIntRight;

            //        #endregion

            //        #region[Set Footer]

            //        //rowIndex++;
            //        //rowIndex++;

            //        //rowIndex++;
            //        //row = sheet.CreateRow(rowIndex);

            //        //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
            //        //sheet.AddMergedRegion(cra);
            //        //cell = row.CreateCell(3);
            //        //cell.SetCellValue("Bơm theo ca:");
            //        //cell.CellStyle = styleFooterText;

            //        //cell = row.CreateCell(6);
            //        //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
            //        //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //        //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
            //        //sheet.AddMergedRegion(cra);
            //        //cell = row.CreateCell(8);
            //        //cell.SetCellValue("Thuê bơm:");
            //        //cell.CellStyle = styleFooterText;

            //        //cell = row.CreateCell(12);
            //        //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
            //        //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //        //rowIndex++;
            //        //row = sheet.CreateRow(rowIndex);

            //        //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
            //        //sheet.AddMergedRegion(cra);
            //        //cell = row.CreateCell(3);
            //        //cell.SetCellValue("Bơm theo khối:");
            //        //cell.CellStyle = styleFooterText;

            //        //cell = row.CreateCell(6);
            //        //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
            //        //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //        //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
            //        //sheet.AddMergedRegion(cra);
            //        //cell = row.CreateCell(8);
            //        //cell.SetCellValue("Thuê xe:");
            //        //cell.CellStyle = styleFooterText;

            //        //cell = row.CreateCell(12);
            //        //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
            //        //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //        //rowIndex++;
            //        //row = sheet.CreateRow(rowIndex);

            //        //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
            //        //sheet.AddMergedRegion(cra);
            //        //cell = row.CreateCell(3);
            //        //cell.SetCellValue("Không dùng bơm:");
            //        //cell.CellStyle = styleFooterText;

            //        //cell = row.CreateCell(6);
            //        //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
            //        //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //        //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
            //        //sheet.AddMergedRegion(cra);
            //        //cell = row.CreateCell(8);
            //        //cell.SetCellValue("Mua bê tông:");
            //        //cell.CellStyle = styleFooterText;

            //        //cell = row.CreateCell(12);
            //        //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
            //        //cell.CellStyle = styleBodyIntFooterLeftNoborder;

            //        //rowIndex++;
            //        //rowIndex++;
            //        //row = sheet.CreateRow(rowIndex);

            //        //đại diện
            //        rowIndex++; rowIndex++;
            //        row = sheet.CreateRow(rowIndex);

            //        cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
            //        sheet.AddMergedRegion(cra);

            //        //tên
            //        rowIndex++;

            //        cra = new CellRangeAddress(rowIndex, rowIndex, 2, 3);
            //        sheet.AddMergedRegion(cra);
            //        cell = row.CreateCell(2);
            //        cell.SetCellValue("Kế toán");
            //        cell.CellStyle = styleBodyFooterCenterNoborder;

            //        cra = new CellRangeAddress(rowIndex, rowIndex, 5, 6);
            //        sheet.AddMergedRegion(cra);
            //        cell = row.CreateCell(5);
            //        cell.SetCellValue("Giám đốc");
            //        cell.CellStyle = styleBodyFooterCenterNoborder;

            //        #endregion

            //        using (var exportData = new MemoryStream())
            //        {
            //            workbook.Write(exportData);

            //            #region [Set title dưới]

            //            string saveAsFileName = "";
            //            string Bienso = "";

            //            if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
            //            {
            //                saveAsFileName = "BÁO CÁO HÓA ĐƠN -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
            //                                 ".xls";
            //            }
            //            else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
            //            {
            //                saveAsFileName = "BÁO CÁO HÓA ĐƠN -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
            //                                 "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
            //            }

            //            Response.ContentType = "application/vnd.ms-excel";
            //            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

            //            #endregion

            //            Response.Clear();
            //            Response.BinaryWrite(exportData.GetBuffer());
            //            Response.End();
            //        }
            //    }
            //    else
            //        GstGetMess("Không có dữ liệu nào để in", "");
            //}
        }
    }
}