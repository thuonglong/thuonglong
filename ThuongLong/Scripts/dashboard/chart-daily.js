﻿$(function () {

    function selectChiNhanh(dlChiNhanh) {
        loadData(dlChiNhanh.value);
    }

    document.getElementById("MainContent_txtNgayThang")
        .addEventListener("keyup", function (event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                var cn = $('#MainContent_dlChiNhanh').select2().val();
                console.log(cn);
                loadData(cn);
            }
        });
    function search() {
        var cn = $('#MainContent_dlChiNhanh').select2().val();
        //console.log(cn);
        loadData('');
    }

    function loadData(idChinhanh) {
        //console.log("idChinhanh : " + idChinhanh);
        var input = { idChinhanh: idChinhanh, date: document.getElementById("MainContent_txtNgayThang").value };
        var element = document.getElementById("overlay");
        element.classList.add("visible");
        $.ajax({
            url: "Default.aspx/LoadChart",
            //url: '<%= ResolveUrl("Default.aspx/LoadChart") %>',
            type: "POST",
            data: JSON.stringify(input),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res, textStatus) {
                element.classList.remove("visible");

                var parsedJson = JSON.parse(res.d);
                //console.log(parsedJson);
                initChart(parsedJson);
                initBeTongInfo(parsedJson);
                initFundsInfo(parsedJson);
                $('#chartSanXuat').html(parsedJson.dataChartSanXuat);
            },
            error: function (res, textStatus) {
                alert('There was an error.');
                element.classList.remove("visible");
            }
        });
    }
    function initChart(responeData) {
        var data = {
            labels: ["Bê tông", "Gạch", "Vật liệu", "Tổng"],
            datasets: [
                {
                    label: 'Năm trước',
                    data: ['100000000', '900000000', '300000000', '500000000', '200000000', '300000000'],

                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(255, 206, 235, 1)',
                    borderWidth: 1,
                },
                {
                    label: 'Năm nay',
                    data: ['1000000000', '1900000000', '300000000', '1500000000', '2000000000', '3000000000'],
                    backgroundColor: 'rgba(255, 206, 86, 0.2)',
                    borderColor: 'rgba(255, 206, 86, 1)',
                    borderWidth: 1,
                }
            ]
        };

        var options = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function (label, index, labels) {
                            return Intl.NumberFormat().format(label);
                            // 1,350

                            /*
                            return Intl.NumberFormat('hi', { 
                                style: 'currency', currency: 'INR', minimumFractionDigits: 0, 
                            }).format(label).replace(/^(\D+)/, '$1 ');
                            // ₹ 1,350
    
                            // return Intl.NumberFormat('hi', {
                                style: 'currency', currency: 'INR', currencyDisplay: 'symbol', minimumFractionDigits: 2 
                            }).format(label).replace(/^(\D+)/, '$1 ');
                            // ₹ 1,350.00
                            */
                        }
                    }
                }],

                xAxes: [{
                    beginAtZero: true,
                    ticks: {
                        autoSkip: false
                    }
                }]
            },
            legend: {
                display: true
            },
            elements: {
                point: {
                    radius: 0
                }
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        return Intl.NumberFormat().format(tooltipItem.yLabel);
                    }
                }
            }

        };

        // Get context with jQuery - using jQuery's .get() method.
        if ($("#barChart").length) {
            var barChartCanvas = $("#barChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                //data: data,
                data: responeData.dataSumaryMonthWithPlanChart,
                options: options
            });
        }

        /*
        var chartdata = {
          type: 'bar',
          data: {
            labels: ["2013", "2014", "2014", "2015", "2016", "2017"],
            // labels: month,
            datasets: [
              {
                label: 'this year',
                backgroundColor: '#26B99A',
                //borderColor: 'rgba(255, 206, 235, 1)',
                //borderWidth: 1,
                data: ['100000000', '900000000', '300000000', '500000000', '200000000', '300000000'],
              },
              {
                label: 'previous year',
                backgroundColor: '#03586A',
                //borderColor: 'rgba(255, 206, 86, 1)',
                //borderWidth: 1,
                data: ['1000000000', '1900000000', '300000000', '1500000000', '2000000000', '3000000000'],
              }
            ]
          },
          //options: options
      }
  
  
      var ctx = document.getElementById('barChart').getContext('2d');
      new Chart(ctx, chartdata);*/

        var doughnutPieData = {
            datasets: [{
                data: [900000, 4000000, 3000000],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 206, 86, 0.5)',
                    'rgba(75, 192, 192, 0.5)',
                    'rgba(153, 102, 255, 0.5)',
                    'rgba(255, 159, 64, 0.5)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: ['Pink', 'Blue', 'Yellow',]
        };
        var doughnutPieOptions = {
            responsive: true,
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function (tooltipItem, data) {
                        //var indice = tooltipItem.index;                 
                        //return  data.labels[indice] +': '+data.datasets[0].data[indice] + '';
                        //console.log(tooltipItem);
                        return Intl.NumberFormat().format(data.datasets[0].data[tooltipItem.index]);
                    }
                }
            }
        };
        if ($("#doughnutChart").length) {
            var doughnutChartCanvas = $("#doughnutChart").get(0).getContext("2d");
            var doughnutChart = new Chart(doughnutChartCanvas, {
                type: 'doughnut',
                //data: doughnutPieData,
                data: responeData.dataSumaryMonthChart,
                options: doughnutPieOptions
            });
        }
    }

    function initBeTongInfo(responeData) {
        var plugins = [{
            beforeInit: (chart) => {
                const dataset = chart.data.datasets[0];
                chart.data.labels = [dataset.label];
                dataset.data = [70, 30];
            }
        },
        {
            beforeDraw: (chart) => {
                var width = chart.chart.width,
                    height = chart.chart.height,
                    ctx = chart.chart.ctx;
                ctx.restore();
                var fontSize = (height / 70).toFixed(2);
                ctx.font = fontSize + "rem sans-serif";
                ctx.fillStyle = "black";
                ctx.textBaseline = "middle";
                ctx.fontWeight = 500;
                var text = chart.data.datasets[0].percent,
                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                    textY = height / 2;
                ctx.fillText(text, textX, textY);
                ctx.save();
            }
        }
        ];

        var options = {
            maintainAspectRatio: false,
            cutoutPercentage: 94,
            rotation: -Math.PI / 2,
            legend: {
                display: false,
            },
            tooltips: {
                enabled: false,
                //filter: tooltipItem => tooltipItem.index == 0
            }
        };
        var myChartCircle = new Chart('chartKLBeTongBan', {
            type: 'doughnut',
            data: {
                datasets: [{
                    label: 'Khối lượng bê tông bán',
                    percent: responeData.KLBeTongBan,
                    backgroundColor: ['#ff6384']
                }]
            },
            plugins: plugins,
            options: options
        });
        new Chart('chartKLBeTongDaTron', {
            type: 'doughnut',
            data: {
                datasets: [{
                    label: 'Khối lượng bê tông đã trộn',
                    percent: responeData.KLBeTongDaTron,
                    backgroundColor: ['#36a2eb']
                }]
            },
            plugins: plugins,
            options: options
        });
        new Chart('chartKLBeTongDuKien', {
            type: 'doughnut',
            data: {
                datasets: [{
                    label: 'Khối lượng bê tông dự kiến bán',
                    percent: responeData.KLBeTongDuKien,
                    backgroundColor: ['#ffce56'],
                    borderWidth: [1]
                }]
            },
            plugins: plugins,
            options: options
        });

    }

    function initFundsInfo(responeData) {
        document.getElementById("MainContent_lbTongThu").innerText = responeData.TongThu;
        document.getElementById("MainContent_lbTongChi").innerText = responeData.TongChi;
        document.getElementById("MainContent_lbCongNoThu").innerText = responeData.CongNoThu;
        document.getElementById("MainContent_lbCongNoChi").innerText = responeData.CongNoChi;
    }
    //initChart();
    loadData("");

});