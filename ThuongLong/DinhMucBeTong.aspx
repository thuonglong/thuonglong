﻿<%@ Page Title="Định mức bê tông" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DinhMucBeTong.aspx.cs" Inherits="ThuongLong.DinhMucBeTong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin định mức</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Mác bê tông</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMacBeTong"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Loại xi măng</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXiMang"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Loại cát</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCat"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Loại đá</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlDa"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Loại phụ gia</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlPhuGia"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Loại tro bay</label>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlTroBay"
                                    runat="server">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">TL xi măng</label>
                                <asp:TextBox ID="txtDMXiMang" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMXiMang" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">TL cát</label>
                                <asp:TextBox ID="txtDMCat" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMCat" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">TL đá</label>
                                <asp:TextBox ID="txtDMDa" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMDa" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">TL phụ gia</label>
                                <asp:TextBox ID="txtDMPhuGia" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMPhuGia" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">TL tro bay</label>
                                <asp:TextBox ID="txtDMTroBay" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMTroBay" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">TL nước</label>
                                <asp:TextBox ID="txtDMNuoc" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtDMNuoc" ValidChars=".," />
                            </div>
                            <div class="form-group col-lg-3" runat="server" visible="false">
                                <label for="exampleInputEmail1">Tỷ lệ quy đổi</label>
                                <asp:TextBox ID="txtTyLeQuyDoi" runat="server" class="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                                    TargetControlID="txtTyLeQuyDoi" ValidChars=".," />
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnSave" runat="server" Text="Lưu" class="btn btn-primary" OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Hủy" class="btn btn-warning" OnClick="btnCancel_Click" />
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách định mức</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-lg-6">
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlMacBeTongSearch"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-6">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand" OnRowCreated="GV_RowCreated"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnSua" runat="server" Text="Sửa" class="btn btn-primary" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnXoa" runat="server" Text="Xóa" class="btn btn-danger" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="MacBeTong" HeaderText="Mác" />
                                    <asp:BoundField DataField="XiMang" HeaderText="Xi măng" />
                                    <asp:BoundField DataField="Cat" HeaderText="Cát" />
                                    <asp:BoundField DataField="Da" HeaderText="Đá" />
                                    <asp:BoundField DataField="PhuGia" HeaderText="Phụ gia" />
                                    <asp:BoundField DataField="TroBay" HeaderText="Tro bay" />
                                    <asp:BoundField DataField="DMXiMang" HeaderText="Xi măng" DataFormatString="{0:0.##}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMCat" HeaderText="Cát" DataFormatString="{0:0.##}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMDa" HeaderText="Đá" DataFormatString="{0:0.##}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMPhuGia" HeaderText="Phụ gia" DataFormatString="{0:0.##}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMTroBay" HeaderText="Tro bay" DataFormatString="{0:0.##}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DMNuoc" HeaderText="Nước" DataFormatString="{0:0.##}" ItemStyle-HorizontalAlign="Right" />
                                    <%--<asp:BoundField DataField="TyLeQuyDoi" HeaderText="Tỷ lệ quy đổi" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />--%>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound" OnRowCreated="gvLichSu_RowCreated"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="MacBeTong" HeaderText="Mác" />
                                <asp:BoundField DataField="XiMang" HeaderText="Xi măng" />
                                <asp:BoundField DataField="Cat" HeaderText="Cát" />
                                <asp:BoundField DataField="Da" HeaderText="Đá" />
                                <asp:BoundField DataField="PhuGia" HeaderText="Phụ gia" />
                                <asp:BoundField DataField="TroBay" HeaderText="Tro bay" />
                                <asp:BoundField DataField="DMXiMang" HeaderText="Xi măng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMCat" HeaderText="Cát" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMDa" HeaderText="Đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMPhuGia" HeaderText="Phụ gia" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMTroBay" HeaderText="Tro bay" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DMNuoc" HeaderText="Nước" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <%--<asp:BoundField DataField="TyLeQuyDoi" HeaderText="Tỷ lệ quy đổi" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />--%>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
