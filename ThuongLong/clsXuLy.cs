﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;

/// <summary>
/// Summary description for clsXuLy
/// </summary>
public class clsXuLy
{
    public clsXuLy()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string EscapeSheetName(string sheetName)
    {
        var escapedSheetName = sheetName
                                    .Replace("/", string.Empty)
                                    .Replace("\\", string.Empty)
                                    .Replace("?", string.Empty)
                                    .Replace("*", "x")
                                    .Replace("[", string.Empty)
                                    .Replace("]", string.Empty)
                                    .Replace(":", string.Empty);

        if (escapedSheetName.Length > 30)
            escapedSheetName = escapedSheetName.Substring(0, 29);

        return escapedSheetName;
    }
    public string VietBangChu(double number)
    {
        try
        {
            string s = number.ToString("#");
            string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] hang = new string[] { " ", "nghìn ", "triệu ", "tỷ " };
            int i, j, donvi, chuc, tram;
            string str = "";
            bool booAm = false;
            double decS = 0;
            try
            {
                if (number.Equals(0))
                    decS = 0;
                else
                    decS = Convert.ToDouble(s.ToString());
            }
            catch
            {
                return "";
            }
            if (decS < 0)
            {
                decS = -decS;
                s = decS.ToString();
                booAm = true;
            }
            i = s.Length;
            if (i == 0)
                str = so[0] + " ";
            else
            {
                j = 0;
                while (i > 0)
                {
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i--;
                    if (i > 0)
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        chuc = -1;
                    i--;
                    if (i > 0)
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        tram = -1;
                    i--;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                        str = hang[j] + str;
                    j++;
                    if (j > 3) j = 1;
                    if ((donvi == 1) && (chuc > 1))
                        str = "mốt " + str;
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                            str = "lăm " + str;
                        else if (donvi > 0)
                            str = so[donvi] + " " + str;
                    }
                    if (chuc < 0)
                        break;
                    else
                    {
                        if ((chuc == 0) && (donvi > 0)) str = "lẻ " + str;
                        if (chuc == 1) str = "mười " + str;
                        if (chuc > 1) str = so[chuc] + " mươi " + str;
                    }
                    if (tram < 0) break;
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0)) str = so[tram] + " trăm " + str;
                    }
                }
            }
            if (str.Length >= 3)
                str = str.Substring(0, 1).ToUpper() + str.Substring(1);
            if (booAm) str = "Âm " + str;
            return str + "đồng";
        }
        catch
        {
            return "";
        }

    }
    public ISheet SetPropertySheet(ISheet sheet)
    {
        sheet.SetMargin(MarginType.LeftMargin, 0.2);
        sheet.SetMargin(MarginType.RightMargin, 0.2);
        sheet.SetMargin(MarginType.TopMargin, 0.4);
        sheet.SetMargin(MarginType.BottomMargin, 0.4);
        sheet.PrintSetup.HeaderMargin = 0.4;
        sheet.PrintSetup.FooterMargin = 0.4;
        sheet.PrintSetup.Landscape = true;
        sheet.PrintSetup.PaperSize = (short)PaperSize.A4_Small;
        return sheet;
    }
    public IFont CreateFont(HSSFWorkbook workbook, string FontName, double Fontsize, bool isBold, bool isItalic)
    {
        var iFont = workbook.CreateFont();
        iFont.FontName = FontName;
        iFont.FontHeightInPoints = Fontsize;
        iFont.Color = HSSFColor.Black.Index;
        if (isBold == true)
        {
            iFont.IsBold = true;
            //iFont.Boldweight = (short)FontBoldWeight.Bold;
        }
        if (isItalic == true)
        {
            iFont.IsItalic = true;
        }
        return iFont;
    }


    public IFont CreateFontSH(HSSFWorkbook workbook, string FontName, double Fontsize, bool isBold, bool isItalic, bool underline)
    {
        var iFont = workbook.CreateFont();
        iFont.FontName = FontName;
        iFont.FontHeightInPoints = Fontsize;
        iFont.Color = HSSFColor.Black.Index;
        if (isBold == true)
        {
            iFont.IsBold = true;
            //iFont.Boldweight = (short)FontBoldWeight.Bold;
        }
        if (isItalic == true)
        {
            iFont.IsItalic = true;
        }
        if (underline == true)
        {
            iFont.Underline = FontUnderlineType.Single;
        }
        return iFont;
    }
    public ICellStyle CreateCellSH(HSSFWorkbook workbook, int isHorizontalAlignment, bool isBorder, IFont iFont)
    {
        ICellStyle cellStyle = workbook.CreateCellStyle();
        if (isHorizontalAlignment == 0)
        {
            cellStyle.Alignment = HorizontalAlignment.Left;
        }
        else if (isHorizontalAlignment == 1)
        {
            cellStyle.Alignment = HorizontalAlignment.Center;
        }
        else if (isHorizontalAlignment == 2)
        {
            cellStyle.Alignment = HorizontalAlignment.Right;
        }
        cellStyle.VerticalAlignment = VerticalAlignment.Center;
        if (isBorder == true)
        {
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
        }
        cellStyle.WrapText = true;
        cellStyle.SetFont(iFont);
        return cellStyle;
    }
    public ICellStyle CreateCellStyleIntSH(HSSFWorkbook workbook, int isHorizontalAlignment, bool isBorder, IFont iFont)
    {
        ICellStyle cellStyle = workbook.CreateCellStyle();
        if (isHorizontalAlignment == 0)
        {
            cellStyle.Alignment = HorizontalAlignment.Left;
        }
        else if (isHorizontalAlignment == 1)
        {
            cellStyle.Alignment = HorizontalAlignment.Center;
        }
        else if (isHorizontalAlignment == 2)
        {
            cellStyle.Alignment = HorizontalAlignment.Right;
        }
        cellStyle.VerticalAlignment = VerticalAlignment.Center;
        if (isBorder == true)
        {
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
        }
        cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0");
        cellStyle.SetFont(iFont);
        return cellStyle;
    }
    public ICellStyle CreateCellStyleDoubleSH(HSSFWorkbook workbook, int isHorizontalAlignment, bool isBorder, IFont iFont)
    {
        ICellStyle cellStyle = workbook.CreateCellStyle();
        if (isHorizontalAlignment == 0)
        {
            cellStyle.Alignment = HorizontalAlignment.Left;
        }
        else if (isHorizontalAlignment == 1)
        {
            cellStyle.Alignment = HorizontalAlignment.Center;
        }
        else if (isHorizontalAlignment == 2)
        {
            cellStyle.Alignment = HorizontalAlignment.Right;
        }
        cellStyle.VerticalAlignment = VerticalAlignment.Center;
        if (isBorder == true)
        {
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
        }
        cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0.00");
        cellStyle.SetFont(iFont);
        return cellStyle;
    }
    public ICellStyle CreateCellStyle(HSSFWorkbook workbook, int isHorizontalAlignment, bool isBorder, IFont iFont)
    {
        ICellStyle cellStyle = workbook.CreateCellStyle();
        if (isHorizontalAlignment == 0)
        {
            cellStyle.Alignment = HorizontalAlignment.Left;
        }
        else if (isHorizontalAlignment == 1)
        {
            cellStyle.Alignment = HorizontalAlignment.Center;
        }
        else if (isHorizontalAlignment == 2)
        {
            cellStyle.Alignment = HorizontalAlignment.Right;
        }
        cellStyle.VerticalAlignment = VerticalAlignment.Center;
        if (isBorder == true)
        {
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
        }
        cellStyle.WrapText = true;
        cellStyle.SetFont(iFont);
        return cellStyle;
    }
    public ICellStyle CreateCellStyleHeader(HSSFWorkbook workbook, int isHorizontalAlignment, bool isBorder, IFont iFont)
    {
        ICellStyle cellStyle = workbook.CreateCellStyle();
        cellStyle.FillForegroundColor = HSSFColor.Tan.Index;
        cellStyle.FillPattern = FillPattern.SolidForeground;
        if (isHorizontalAlignment == 0)
        {
            cellStyle.Alignment = HorizontalAlignment.Left;
        }
        else if (isHorizontalAlignment == 1)
        {
            cellStyle.Alignment = HorizontalAlignment.Center;
        }
        else if (isHorizontalAlignment == 2)
        {
            cellStyle.Alignment = HorizontalAlignment.Right;
        }
        cellStyle.VerticalAlignment = VerticalAlignment.Center;
        if (isBorder == true)
        {
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
        }
        cellStyle.WrapText = true;
        cellStyle.SetFont(iFont);
        return cellStyle;
    }
    public ICellStyle CreateCellStyleDatetime(HSSFWorkbook workbook, int isHorizontalAlignment, bool isBorder, IDataFormat dataFormatCustom, IFont iFont)
    {
        ICellStyle cellStyle = workbook.CreateCellStyle();
        if (isHorizontalAlignment == 0)
        {
            cellStyle.Alignment = HorizontalAlignment.Left;
        }
        else if (isHorizontalAlignment == 1)
        {
            cellStyle.Alignment = HorizontalAlignment.Center;
        }
        else if (isHorizontalAlignment == 2)
        {
            cellStyle.Alignment = HorizontalAlignment.Right;
        }
        cellStyle.VerticalAlignment = VerticalAlignment.Center;
        if (isBorder == true)
        {
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
        }
        cellStyle.DataFormat = dataFormatCustom.GetFormat("dd/MM/yyyy");
        cellStyle.SetFont(iFont);
        return cellStyle;
    }
    public ICellStyle CreateCellStyleInt(HSSFWorkbook workbook, int isHorizontalAlignment, bool isBorder, IFont iFont)
    {
        ICellStyle cellStyle = workbook.CreateCellStyle();
        if (isHorizontalAlignment == 0)
        {
            cellStyle.Alignment = HorizontalAlignment.Left;
        }
        else if (isHorizontalAlignment == 1)
        {
            cellStyle.Alignment = HorizontalAlignment.Center;
        }
        else if (isHorizontalAlignment == 2)
        {
            cellStyle.Alignment = HorizontalAlignment.Right;
        }
        cellStyle.VerticalAlignment = VerticalAlignment.Center;
        if (isBorder == true)
        {
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
        }
        cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0");
        cellStyle.SetFont(iFont);
        return cellStyle;
    }
    public ICellStyle CreateCellStyleDouble(HSSFWorkbook workbook, int isHorizontalAlignment, bool isBorder, IFont iFont)
    {
        ICellStyle cellStyle = workbook.CreateCellStyle();
        if (isHorizontalAlignment == 0)
        {
            cellStyle.Alignment = HorizontalAlignment.Left;
        }
        else if (isHorizontalAlignment == 1)
        {
            cellStyle.Alignment = HorizontalAlignment.Center;
        }
        else if (isHorizontalAlignment == 2)
        {
            cellStyle.Alignment = HorizontalAlignment.Right;
        }
        cellStyle.VerticalAlignment = VerticalAlignment.Center;
        if (isBorder == true)
        {
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.BorderLeft = BorderStyle.Thin;
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderTop = BorderStyle.Thin;
        }
        cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0.00");
        cellStyle.SetFont(iFont);
        return cellStyle;
    }
}