﻿<%@ Page Title="Báo cáo doanh thu tổng hợp" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BaoCaoDoanhThuTongHop.aspx.cs" Inherits="ThuongLong.BaoCaoDoanhThuTongHop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Chi nhánh</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3" id="divchinhanh" runat="server" >
                            <label for="exampleInputEmail1">Loại</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <asp:CheckBox ID="ckLoai" runat="server" Enabled="false" />
                                </span>
                                <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại" Font-Size="13px"
                                    DataTextField="Ten" DataValueField="ID" ID="dlLoai" runat="server" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0">Tất cả</asp:ListItem>
                                    <asp:ListItem Value="1">Bê tông</asp:ListItem>
                                    <asp:ListItem Value="2">Bán gạch</asp:ListItem>
                                    <asp:ListItem Value="3">Bán vật liệu</asp:ListItem>
                                    <%--<asp:ListItem Value="4">Nhập kho vật liệu</asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Nhà cung cấp</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                                Font-Size="13px" DataTextField="TenNhaCungCap" DataValueField="ID" ID="dlNhaCungCapSearch"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-6">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                            <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-blue" OnClick="btnPrint_Click" ><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <div class="box box-success" id="divtong" runat="server" visible="false">
                                <%--<div class="box-header with-border">
                                    <h3 class="box-title">Tổng</h3>
                                </div>--%>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group  col-lg-2">
                                            <label>Công nợ thu</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtCongNoThu" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group  col-lg-2">
                                            <label>Công nợ trả</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtCongNoTra" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group  col-lg-2">
                                            <label>Số tiền đã thu</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtSoTienThu" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group  col-lg-2">
                                            <label>Số tiền đã trả</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtSoTienTra" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group  col-lg-4">
                                            <label>Còn lại</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                                <asp:TextBox ID="txtConLai" runat="server" class="form-control" Width="98%" disabled></asp:TextBox>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>

                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" ShowFooter="true" OnRowCommand="GV_RowCommand" 
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Nhà cung cấp">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TenNhaCungCap")%>' runat="server" CssClass="padding" CommandName="Sua"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Tong" HeaderText="Tổng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-BackColor="Beige"/>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdHinhAnh" runat="server" Value="" />
            <asp:HiddenField ID="hdTuNgay" runat="server" Value="" />
            <asp:HiddenField ID="hdDenNgay" runat="server" Value="" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Chi tiết
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridViewRowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="Nhà cung cấp" />
                                <asp:BoundField DataField="TenHangMucThuChi" HeaderText="Loại" />
                                <asp:BoundField DataField="NoiDung" HeaderText="Nội dung" />
                                <asp:BoundField DataField="SoTien" HeaderText="Số tiền" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpHinhAnh" runat="server" CancelControlID="btnDongHinhAnh"
                Drag="True" TargetControlID="hdHinhAnh" BackgroundCssClass="modalBackground" PopupControlID="pnHinhAnh"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnHinhAnh" runat="server" Style="width: 98%; height: 100%; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Hình ảnh
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:Image ID="imgHinhAnh" runat="server" ImageUrl="" />
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongHinhAnh" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
