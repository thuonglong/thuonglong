﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class LichBanGach1 : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        BeTongDataContext betong = new BeTongDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmLichXuatBeTong";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmLichXuatBeTong", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();

                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem(ref string IDHopDong, ref string IDHopDongBom, ref string IDChiTietKD)
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            if (txtKLThucXuat.Text == "" || double.Parse(txtKLThucXuat.Text) == 0)
            {
                s += " - Nhập khối lượng tạm tính<br />";
            }
            if (txtKLKhachHang.Text == "" || double.Parse(txtKLKhachHang.Text) == 0)
            {
                s += " - Nhập khối lượng khách hàng<br />";
            }
            if (s == "")
            {
                betong.sp_LichXuatBeTong_CheckThem(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlCongTrinh), txtHangMuc.Text.Trim().ToUpper(), GetDrop(dlNhanVien), GetDrop(dlMacBeTong), GetDrop(dlHinhThucBom),
                      ref IDHopDong, ref IDHopDongBom, ref IDChiTietKD, ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua(ref string IDHopDong, ref string IDHopDongBom, ref string IDChiTietKD)
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }
            if (txtKLThucXuat.Text == "" || double.Parse(txtKLThucXuat.Text) == 0)
            {
                s += " - Nhập khối lượng tạm tính<br />";
            }
            if (txtKLKhachHang.Text == "" || double.Parse(txtKLKhachHang.Text) == 0)
            {
                s += " - Nhập khối lượng khách hàng<br />";
            }
            if (s == "")
            {
                betong.sp_LichXuatBeTong_CheckSua(new Guid(hdID.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlCongTrinh), txtHangMuc.Text.Trim().ToUpper(), GetDrop(dlNhanVien), GetDrop(dlMacBeTong), GetDrop(dlHinhThucBom),
                      ref IDHopDong, ref IDHopDongBom, ref IDChiTietKD, ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            string idhopdong = "";
            string idhopdongbom = "";
            string idchitietkinhdoanh = "";
            if (hdID.Value == "")
            {
                if (CheckThem(ref idhopdong, ref idhopdongbom, ref idchitietkinhdoanh) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var LichXuatBeTong = new tblLichXuatBeTong()
                        {
                            ID = Guid.NewGuid(),
                            GioXuat = TimeSpan.Parse(txtGioXuat.Text),
                            NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhaCungCap = GetDrop(dlNhaCungCap),
                            IDCongTrinh = GetDrop(dlCongTrinh),
                            HangMuc = txtHangMuc.Text.Trim().ToUpper(),
                            IDChiTietKinhDoanh = new Guid(idchitietkinhdoanh),
                            IDNVKD = GetDrop(dlNhanVien),
                            MacBeTong = GetDrop(dlMacBeTong),
                            HinhThucBom = dlHinhThucBom.SelectedValue == "" ? (Guid?)null : GetDrop(dlHinhThucBom),
                            IDHopDongBom = idhopdongbom == "" ? (Guid?)null : new Guid(idhopdongbom),
                            IDHopDong = new Guid(idhopdong),
                            KLThucXuat = double.Parse(txtKLThucXuat.Text),
                            KLKhachHang = double.Parse(txtKLKhachHang.Text),
                            CuLyVanChuyen = txtCuLyVanChuyen.Text.Trim() == "" ? 0 : double.Parse(txtCuLyVanChuyen.Text),
                            KyThuat = txtKyThuat.Text.Trim(),
                            NguoiThuTien = txtNguoiThuTien.Text.Trim(),
                            KLDaBan = 0,
                            KLDaXuat = 0,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            TrangThaiHoanThanh = "Chưa hoàn thành",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblLichXuatBeTongs.InsertOnSubmit(LichXuatBeTong);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua(ref idhopdong, ref idhopdongbom, ref idchitietkinhdoanh) == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblLichXuatBeTongs
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.GioXuat = TimeSpan.Parse(txtGioXuat.Text);
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                            query.IDCongTrinh = GetDrop(dlCongTrinh);
                            query.HangMuc = txtHangMuc.Text.Trim().ToUpper();
                            query.IDChiTietKinhDoanh = new Guid(idchitietkinhdoanh);
                            query.IDNVKD = GetDrop(dlNhanVien);
                            query.MacBeTong = GetDrop(dlMacBeTong);
                            query.IDHopDong = new Guid(idhopdong);
                            query.HinhThucBom = dlHinhThucBom.SelectedValue == "" ? (Guid?)null : GetDrop(dlHinhThucBom);
                            query.IDHopDongBom = idhopdongbom == "" ? (Guid?)null : new Guid(idhopdongbom);
                            query.KLThucXuat = double.Parse(txtKLThucXuat.Text);
                            query.KLKhachHang = txtCuLyVanChuyen.Text.Trim() == "" ? 0 : double.Parse(txtKLKhachHang.Text);
                            query.CuLyVanChuyen = double.Parse(txtCuLyVanChuyen.Text);
                            query.KyThuat = txtKyThuat.Text.Trim();
                            query.NguoiThuTien = txtNguoiThuTien.Text.Trim();

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin lịch xuất bê tông đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh_SelectedIndexChanged(sender, e);
            hdID.Value = "";
            txtKLThucXuat.Text = "";
            txtKLKhachHang.Text = "";
            txtCuLyVanChuyen.Text = "";
            txtHangMuc.Text = "";
            txtKyThuat.Text = "";
            txtNguoiThuTien.Text = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblLichXuatBeTongs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        betong.sp_LichXuatBeTong_CheckSuaGV(query.ID, query.IDHopDong, query.IDHopDongBom, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            txtGioXuat.Text = query.GioXuat.ToString();
                            txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            LoadNhaCungCap();
                            dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                            if (dlCongTrinh.Items.Count == 0)
                                LoadCongTrinh();
                            dlCongTrinh.SelectedValue = query.IDCongTrinh.ToString();
                            if (dlHinhThucBom.Items.Count == 0)
                                LoadHinhThucBom();
                            dlHinhThucBom.SelectedValue = query.HinhThucBom.ToString();
                            if (dlMacBeTong.Items.Count == 0)
                                LoadMacBeTong();
                            dlMacBeTong.SelectedValue = query.MacBeTong.ToString();
                            txtHangMuc.Text = query.HangMuc.ToString();
                            txtKLThucXuat.Text = query.KLThucXuat.ToString();
                            txtKLKhachHang.Text = query.KLKhachHang.ToString();
                            txtCuLyVanChuyen.Text = query.CuLyVanChuyen.ToString();

                            txtKyThuat.Text = query.KyThuat.ToString();
                            txtNguoiThuTien.Text = query.NguoiThuTien.ToString();
                            hdID.Value = id;
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        betong.sp_LichXuatBeTong_CheckXoa(query.ID, query.IDChiNhanh, query.IDNhaCungCap, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblLichXuatBeTong_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblLichXuatBeTongs.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "hoanthanh")
                {
                    if (query.TrangThaiHoanThanh == "Đã hoàn thành")
                    {
                        query.TrangThaiHoanThanh = "Chưa hoàn thành";
                    }
                    else
                    {
                        query.TrangThaiHoanThanh = "Đã hoàn thành";
                    }
                    db.SubmitChanges();
                    Search(int.Parse(hdPage.Value));
                }
                else if (e.CommandName == "Xem")
                {
                    var view = betong.sp_LichXuatBeTong_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin lịch xuất bê tông đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                            if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                            {
                                gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                                e.Row.Cells[i].BackColor = Color.LightCyan;
                            }
                    }
                }
            }
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (divdl.Visible == true)
                ResetFilter();
        }
        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            if (divdl.Visible == true)
            {
                divdl.Visible = false;
                dlNhaCungCapSearch.Items.Clear();
                dlCongTrinhSearch.Items.Clear();
                dlMacBeTongSearch.Items.Clear();

            }
            else
            {
                divdl.Visible = true;
                ResetFilter();
            }
        }

        protected void ResetFilter()
        {
            dlNhaCungCapSearch.Items.Clear();
            dlCongTrinhSearch.Items.Clear();
            dlMacBeTongSearch.Items.Clear();

            List<sp_LichXuatBeTong_ResetFilterResult> query = betong.sp_LichXuatBeTong_ResetFilter(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text))).ToList();

            dlNhaCungCapSearch.DataSource = (from p in query
                                             select new
                                             {
                                                 ID = p.IDNhaCungCap,
                                                 Ten = p.TenNhaCungCap
                                             }).Distinct().OrderBy(p => p.Ten);
            dlNhaCungCapSearch.DataBind();

            dlCongTrinhSearch.DataSource = (from p in query
                                            select new
                                            {
                                                ID = p.IDCongTrinh,
                                                Ten = p.TenCongTrinh
                                            }).Distinct().OrderBy(p => p.Ten);
            dlCongTrinhSearch.DataBind();

            dlMacBeTongSearch.DataSource = (from p in query
                                            select new
                                            {
                                                ID = p.MacBeTong,
                                                Ten = p.TenMacBeTong
                                            }).Distinct().OrderBy(p => p.Ten);
            dlMacBeTongSearch.DataBind();

            dlHinhThucBomSearch.DataSource = (from p in query
                                              select new
                                              {
                                                  ID = p.HinhThucBom,
                                                  Ten = p.TenHinhThucBom
                                              }).Distinct().OrderBy(p => p.Ten);
            dlHinhThucBomSearch.DataBind();
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            string query = "SELECT a.ID, a.NgayThang, a.GioXuat, i.TenChiNhanh, TenCongTrinh = h.CongTrinh, a.HangMuc,b.TenNhaCungCap, TenMacBeTong = c.TenLoaiVatLieu, g.TenHinhThucBom, j.TenNhanVien,  " +
                "a.KLThucXuat, a.KLKhachHang, a.CuLyVanChuyen, a.KLDaBan, a.KLDaXuat, a.TrangThaiHoanThanh,a.TrangThaiText, a.NguoiTao, a.NgayTao, a.KyThuat, a.NguoiThuTien " +
                "FROM tblLichXuatBeTong AS a JOIN tblNhaCungCap AS b ON a.IDNhaCungCap = b.ID JOIN tblLoaiVatLieu AS c ON a.MacBeTong = c.ID JOIN tblHinhThucBom AS g ON a.HinhThucBom = g.ID " +
                "JOIN tblHopDongBanBeTong AS h ON a.IDCongTrinh = h.ID JOIN tblChiNhanh AS i ON a.IDChiNhanh = i.ID   " +
                "JOIN tblNhanSu AS j ON j.ID = a.IDNVKD " +
                "WHERE a.IDChiNhanh = '" + dlChiNhanhSearch.SelectedValue + "' AND a.NgayThang BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "' ";
            string sqlgiua = "";

            sqlgiua += GetValueSelectedListBox(" and a.IDNhaCungCap ", dlNhaCungCapSearch);
            sqlgiua += GetValueSelectedListBox(" and a.IDCongTrinh ", dlCongTrinhSearch);
            sqlgiua += GetValueSelectedListBox(" and a.MacBeTong ", dlMacBeTongSearch);
            sqlgiua += GetValueSelectedListBox(" and a.HinhThucBom ", dlHinhThucBomSearch);
            string sqlcuoi = " ORDER BY a.TrangThaiText asc, a.NgayThang desc OFFSET 20 * (" + Convert.ToString(page) + " - 1) ROWS FETCH NEXT 20 ROWS ONLY";

            query = query + sqlgiua + sqlcuoi;
            SqlConnection con = new SqlConnection(constr);

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            GV.DataSource = ds;
            GV.DataBind();
        }

        bool valDate(string s)
        {
            bool b = true;
            try
            {
                DateTime t = DateTime.Parse(GetNgayThang(s));
            }
            catch (Exception)
            {
                b = false;
                GstGetMess("Sai định dạng ngày tháng", "");
            }
            return b;
        }
        void LoadNhaCungCap()
        {
            if (txtNgayThang.Text != "" && dlChiNhanh.SelectedValue != "")
            {
                dlNhaCungCap.Items.Clear();
                var query = betong.sp_LichXuatBeTong_LoadNhaCungCap(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh));
                dlNhaCungCap.DataSource = query;
                dlNhaCungCap.DataBind();
                if (dlNhaCungCap.Items.Count == 1)
                {
                    LoadCongTrinh();
                }
                else
                {
                    dlNhaCungCap.Items.Insert(0, new ListItem("Chọn", ""));
                    dlCongTrinh.Items.Clear();
                    dlNhanVien.Items.Clear();
                    dlMacBeTong.Items.Clear();
                    dlHinhThucBom.Items.Insert(0, new ListItem("Không dùng bơm", ""));
                }
            }
            else
            {
                dlNhaCungCap.Items.Clear();
                dlCongTrinh.Items.Clear();
                dlNhanVien.Items.Clear();
                dlMacBeTong.Items.Clear();
                dlHinhThucBom.Items.Insert(0, new ListItem("Không dùng bơm", ""));
            }
        }
        void LoadCongTrinh()
        {
            if (txtNgayThang.Text != "" && dlNhaCungCap.SelectedValue != "")
            {
                dlCongTrinh.Items.Clear();
                var query = betong.sp_LichXuatBeTong_LoadCongTrinh(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap));
                dlCongTrinh.DataSource = query;
                dlCongTrinh.DataBind();
                if (dlCongTrinh.Items.Count == 1)
                {
                    LoadMacBeTong();
                    LoadHinhThucBom();
                    LoadNVKD();
                }
                else
                {
                    dlCongTrinh.Items.Insert(0, new ListItem("Chọn", ""));
                    dlMacBeTong.Items.Clear();
                    dlNhanVien.Items.Clear();
                    dlHinhThucBom.Items.Insert(0, new ListItem("Không dùng bơm", ""));
                }
            }
            else
            {
                dlCongTrinh.Items.Clear();
                dlHinhThucBom.Items.Insert(0, new ListItem("Không dùng bơm", ""));
                dlMacBeTong.Items.Clear();
                dlNhanVien.Items.Clear();
            }
        }
        void LoadHinhThucBom()
        {
            dlHinhThucBom.Items.Clear();
            if (txtNgayThang.Text != "" && dlCongTrinh.SelectedValue != "")
            {
                var query = betong.sp_LichXuatBeTong_LoadHinhThucBom(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlChiNhanh), GetDrop(dlCongTrinh));
                dlHinhThucBom.DataSource = query;
                dlHinhThucBom.DataBind();
            }
            dlHinhThucBom.Items.Insert(0, new ListItem("Không dùng bơm", "2862C6F2-0AE1-499B-93B7-6E2B0AB46B83"));
        }
        void LoadMacBeTong()
        {
            if (txtNgayThang.Text != "" && dlCongTrinh.SelectedValue != "")
            {
                dlMacBeTong.Items.Clear();
                var query = betong.sp_LichXuatBeTong_LoadMacBeTong(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh));
                dlMacBeTong.DataSource = query;
                dlMacBeTong.DataBind();
                if (dlMacBeTong.Items.Count == 1)
                {
                    //LoadLoaiDa();
                }
                else
                {
                    dlMacBeTong.Items.Insert(0, new ListItem("Chọn", ""));
                }
            }
            else
            {
                dlMacBeTong.Items.Clear();
            }
        }
        void LoadNVKD()
        {
            if (txtNgayThang.Text != "" && dlCongTrinh.SelectedValue != "")
            {
                dlNhanVien.Items.Clear();
                var query = betong.sp_LichXuatBeTong_LoadNVKD(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlCongTrinh));
                dlNhanVien.DataSource = query;
                dlNhanVien.DataBind();
                if (dlNhanVien.Items.Count == 1)
                {
                    //LoadLoaiDa();
                }
                else
                {
                    dlNhanVien.Items.Insert(0, new ListItem("Chọn", ""));
                }
            }
            else
            {
                dlNhanVien.Items.Clear();
            }
        }
        protected void LoadNVKDDM()
        {
            dlNhanVien.Items.Clear();
            var query = (from p in db.tblNhanSus
                         where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenNhanVien
                         }).OrderBy(p => p.Ten);
            dlNhanVien.DataSource = query;
            dlNhanVien.DataBind();
            dlNhanVien.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }
        protected void dlNhaCungCap_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCongTrinh();
        }
        protected void dlCongTrinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNVKD();
            LoadMacBeTong();
            LoadHinhThucBom();
        }

    }
}