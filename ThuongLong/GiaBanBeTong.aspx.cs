﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class GiaBanBeTong : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        BeTongDataContext vatlieu = new BeTongDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmGiaBanBeTong";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmGiaBanBeTong", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNhaCungCap();
                        LoadDanhMuc();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNhaCungCap()
        {
            List<sp_LoadNhaCungCapResult> query = db.sp_LoadNhaCungCap().ToList();
            dlNhaCungCap.DataSource = query;
            dlNhaCungCap.DataBind();
            dlNhaCungCap.Items.Insert(0, new ListItem("--Chọn--", ""));

            dlNCC.DataSource = query;
            dlNCC.DataBind();
            dlNCC.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlNhaCungCap_SelectedIndexChanged(object sender, EventArgs e)
        {
            dlCongTrinh.Items.Clear();
            if (dlNhaCungCap.SelectedValue != "")
            {
                var query = (from p in db.tblCongTrinhNhaCungCaps
                             where p.IDNhaCungCap == GetDrop(dlNhaCungCap)
                             select new
                             {
                                 p.ID,
                                 Ten = p.TenCongTrinh
                             }).OrderBy(p => p.Ten);
                dlCongTrinh.DataSource = query;
                dlCongTrinh.DataBind();
            }
            dlCongTrinh.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        void LoadDanhMuc()
        {
            dlMacBeTong.Items.Clear();
            dlMacBeTong.DataSource = from p in db.tblLoaiVatLieus
                                     where p.TrangThai == 2
                                     && p.IDNhomVatLieu == new Guid("29FC56FA-D0DD-469C-A551-CDC8123C1189")
                                     orderby p.STT
                                     select new
                                     {
                                         p.ID,
                                         Ten = p.TenLoaiVatLieu,
                                     };
            dlMacBeTong.DataBind();
            dlMacBeTong.Items.Insert(0, new ListItem("--Chọn --", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhaCungCap.SelectedValue == "")
            {
                s += " - Chọn nhà cung cấp<br />";
            }
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }

            if (txtDonGiaHoaDon.Text == "" || decimal.Parse(txtDonGiaHoaDon.Text) == 0)
            {
                s += " - Nhập đơn giá hóa đơn<br />";
            }
            if (txtDonGiaThanhToan.Text == "" || decimal.Parse(txtDonGiaThanhToan.Text) == 0)
            {
                s += " - Nhập đơn giá thanh toán<br />";
            }
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_GiaBanBeTong_CheckThem(GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), GetDrop(dlCongTrinh), GetDrop(dlMacBeTong), decimal.Parse(txtDonGiaHoaDon.Text), decimal.Parse(txtDonGiaThanhToan.Text),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlMacBeTong.SelectedValue == "")
            {
                s += " - Chọn mác bê tông<br />";
            }

            if (txtDonGiaHoaDon.Text == "" || decimal.Parse(txtDonGiaHoaDon.Text) == 0)
            {
                s += " - Nhập đơn giá hóa đơn<br />";
            }
            if (txtDonGiaThanhToan.Text == "" || decimal.Parse(txtDonGiaThanhToan.Text) == 0)
            {
                s += " - Nhập đơn giá thanh toán<br />";
            }
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu tính giá<br />";
            }
            if (s == "")
            {
                vatlieu.sp_GiaBanBeTong_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), GetDrop(dlNhaCungCap), GetDrop(dlCongTrinh), GetDrop(dlMacBeTong), decimal.Parse(txtDonGiaHoaDon.Text), decimal.Parse(txtDonGiaThanhToan.Text),
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), txtDenNgay.Text == "" ? DateTime.Parse("6/6/2079") : DateTime.Parse(GetNgayThang(txtDenNgay.Text)), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var GiaBanBeTong = new tblGiaBanBeTong()
                        {
                            ID = Guid.NewGuid(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhaCungCap = GetDrop(dlNhaCungCap),
                            IDCongTrinh = GetDrop(dlCongTrinh),
                            MacBeTong = GetDrop(dlMacBeTong),

                            DonGiaHoaDon = decimal.Parse(txtDonGiaHoaDon.Text),
                            DonGiaThanhToan = decimal.Parse(txtDonGiaThanhToan.Text),
                            TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                            DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblGiaBanBeTongs.InsertOnSubmit(GiaBanBeTong);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblGiaBanBeTongs
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {

                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhaCungCap = GetDrop(dlNhaCungCap);
                            query.IDCongTrinh = GetDrop(dlCongTrinh);
                            query.MacBeTong = GetDrop(dlMacBeTong);

                            query.DonGiaHoaDon = decimal.Parse(txtDonGiaHoaDon.Text);
                            query.DonGiaThanhToan = decimal.Parse(txtDonGiaThanhToan.Text);
                            query.TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text));
                            query.DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text));
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin giá bán bê tông đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh.SelectedIndex = 0;
            dlNhaCungCap.SelectedValue = "";
            dlCongTrinh.SelectedValue = "";
            dlMacBeTong.SelectedValue = "";

            hdID.Value = "";
            txtDonGiaHoaDon.Text = "";
            txtDonGiaThanhToan.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblGiaBanBeTongs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_GiaBanBeTong_CheckSuaGV(query.ID, query.IDNhaCungCap, query.IDCongTrinh, query.MacBeTong, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            dlNhaCungCap.SelectedValue = query.IDNhaCungCap.ToString();
                            dlNhaCungCap_SelectedIndexChanged(sender, e);
                            dlCongTrinh.SelectedValue = query.IDCongTrinh.ToString();
                            dlMacBeTong.SelectedValue = query.MacBeTong.ToString();

                            txtDonGiaHoaDon.Text = string.Format("{0:N0}", query.DonGiaHoaDon);
                            txtDonGiaThanhToan.Text = string.Format("{0:N0}", query.DonGiaThanhToan);
                            txtTuNgay.Text = query.TuNgay.ToString("dd/MM/yyyy");
                            txtDenNgay.Text = query.DenNgay == null ? "" : query.DenNgay.Value.ToString("dd/MM/yyyy");
                            hdID.Value = id;
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        vatlieu.sp_GiaBanBeTong_CheckXoa(query.ID, query.IDChiNhanh, query.IDNhaCungCap, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai == 1)
                            {
                                int countxoa = (from p in db.tblGiaBanBeTong_Logs
                                                where p.IDChung == query.ID
                                                select p).Count();
                                if (countxoa == 1)
                                {
                                    db.tblGiaBanBeTongs.DeleteOnSubmit(query);
                                    db.SubmitChanges();
                                    Success("Đã xóa");
                                }
                                else
                                {
                                    query.TrangThai = 3;
                                    query.TrangThaiText = "Chờ duyệt xóa";
                                    db.SubmitChanges();
                                    Success("Xóa thành công");
                                }
                            }
                            else if (query.TrangThai == 2)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                            else
                            {
                                Warning("Dữ liệu đang chờ duyệt xóa");
                            }
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_GiaBanBeTong_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin giá bán bê tông đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (divdl.Visible == true)
                ResetFilter();
        }
        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            if (divdl.Visible == true)
            {
                divdl.Visible = false;
                dlNhaCungCapSearch.Items.Clear();
                dlCongTrinhSearch.Items.Clear();
                dlMacBeTongSearch.Items.Clear();
                dlLoaiDaSearch.Items.Clear();
                dlDoSutSearch.Items.Clear();
                dlYCDBSearch.Items.Clear();
            }
            else
            {
                divdl.Visible = true;
                ResetFilter();
            }
        }

        protected void ResetFilter()
        {
            dlNhaCungCapSearch.Items.Clear();
            dlCongTrinhSearch.Items.Clear();
            dlMacBeTongSearch.Items.Clear();
            dlLoaiDaSearch.Items.Clear();
            dlDoSutSearch.Items.Clear();
            dlYCDBSearch.Items.Clear();
            List<sp_GiaBanBeTong_ResetFilterResult> query = vatlieu.sp_GiaBanBeTong_ResetFilter(GetDrop(dlChiNhanhSearch), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text))).ToList();

            dlNhaCungCapSearch.DataSource = (from p in query
                                             select new
                                             {
                                                 ID = p.IDNhaCungCap,
                                                 Ten = p.TenNhaCungCap
                                             }).Distinct().OrderBy(p => p.Ten);
            dlNhaCungCapSearch.DataBind();

            dlCongTrinhSearch.DataSource = (from p in query
                                            select new
                                            {
                                                ID = p.IDCongTrinh,
                                                Ten = p.TenCongTrinh
                                            }).Distinct().OrderBy(p => p.Ten);
            dlCongTrinhSearch.DataBind();

            dlMacBeTongSearch.DataSource = (from p in query
                                            select new
                                            {
                                                ID = p.MacBeTong,
                                                Ten = p.TenMacBeTong
                                            }).Distinct().OrderBy(p => p.Ten);
            dlMacBeTongSearch.DataBind();

            dlLoaiDaSearch.DataSource = (from p in query
                                         select new
                                         {
                                             ID = p.LoaiDa,
                                             Ten = p.TenLoaiDa
                                         }).Distinct().OrderBy(p => p.Ten);
            dlLoaiDaSearch.DataBind();

            dlDoSutSearch.DataSource = (from p in query
                                        select new
                                        {
                                            ID = p.DoSut,
                                            Ten = p.TenDoSut
                                        }).Distinct().OrderBy(p => p.Ten);
            dlDoSutSearch.DataBind();

            dlYCDBSearch.DataSource = (from p in query
                                       select new
                                       {
                                           ID = p.YCDB,
                                           Ten = p.TenYCDB
                                       }).Distinct().OrderBy(p => p.Ten);
            dlYCDBSearch.DataBind();
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            string query = "SELECT a.ID, h.TenChiNhanh, g.TenCongTrinh, b.TenNhaCungCap, TenMacBeTong = c.TenLoaiVatLieu, a.DonGiaHoaDon, a.DonGiaThanhToan, a.TuNgay, a.DenNgay, a.TrangThaiText, a.NguoiTao, a.NgayTao " +
                "FROM tblGiaBanBeTong AS a JOIN tblNhaCungCap AS b ON a.IDNhaCungCap = b.ID JOIN tblLoaiVatLieu AS c ON a.MacBeTong = c.ID JOIN tblCongTrinhNhaCungCap AS g ON a.IDCongTrinh = g.ID " +
                "JOIN tblChiNhanh AS h ON a.IDChiNhanh = h.ID   " +
                "WHERE a.IDChiNhanh = '" + dlChiNhanhSearch.SelectedValue + "' AND('" + GetNgayThang(txtTuNgaySearch.Text) + "' BETWEEN a.TuNgay AND ISNULL(a.DenNgay, '6/6/2079') " +
                " OR '" + GetNgayThang(txtDenNgaySearch.Text) + "' BETWEEN a.TuNgay AND ISNULL(a.DenNgay, '6/6/2079')" +
                " OR a.TuNgay BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "'" +
                " OR ISNULL(a.DenNgay, '6/6/2079') BETWEEN '" + GetNgayThang(txtTuNgaySearch.Text) + "' AND '" + GetNgayThang(txtDenNgaySearch.Text) + "') ";
            string sqlgiua = "";

            sqlgiua += GetValueSelectedListBox(" and a.IDNhaCungCap ", dlNhaCungCapSearch);
            sqlgiua += GetValueSelectedListBox(" and a.IDCongTrinh ", dlCongTrinhSearch);
            sqlgiua += GetValueSelectedListBox(" and a.MacBeTong ", dlMacBeTongSearch);
            string sqlcuoi = " ORDER BY a.TrangThaiText asc, a.TuNgay desc OFFSET 20 * (" + Convert.ToString(page) + " - 1) ROWS FETCH NEXT 20 ROWS ONLY";

            query = query + sqlgiua + sqlcuoi;
            SqlConnection con = new SqlConnection(constr);

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            GV.DataSource = ds;
            GV.DataBind();
        }

        protected void btnTaoMoiCongTrinh_Click(object sender, EventArgs e)
        {
            mpCongTrinh.Show();
            dlNCC.SelectedValue = dlNhaCungCap.SelectedValue;
            txtTenCongTrinh.Text = "";
            txtKMVanChuyen.Text = "";
        }

        protected void btnLuuCongTrinh_Click(object sender, EventArgs e)
        {
            var checktrung = from p in db.tblCongTrinhNhaCungCaps
                             where p.IDNhaCungCap == GetDrop(dlNCC)
                             && p.TenCongTrinh.ToUpper() == txtTenCongTrinh.Text.Trim().ToUpper()
                             select p;
            if (txtTenCongTrinh.Text.Trim() == "")
            {
                GstGetMess("Nhập tên công trình.", "");
                mpCongTrinh.Show();
            }
            else if (checktrung.Count() > 0)
            {
                GstGetMess("Tên công trình cho nhà cung cấp đã tồn tại.", "");
                mpCongTrinh.Show();
            }
            else if (txtKMVanChuyen.Text.Trim() == "")
            {
                GstGetMess("Nhập số km đến công trình.", "");
                mpCongTrinh.Show();
            }
            else
            {
                Guid id = Guid.NewGuid();
                var insertcongtrinh = new tblCongTrinhNhaCungCap()
                {
                    ID = id,
                    IDNhaCungCap = GetDrop(dlNCC),
                    TenCongTrinh = txtTenCongTrinh.Text.Trim(),
                    KMVanChuyen = double.Parse(txtKMVanChuyen.Text),
                    TrangThai = 2,
                    TrangThaiText = "Đã duyệt",
                    STT = 0,
                    NgayTao = DateTime.Now,
                    NguoiTao = Session["IDND"].ToString()
                };
                db.tblCongTrinhNhaCungCaps.InsertOnSubmit(insertcongtrinh);
                db.SubmitChanges();
                dlNhaCungCap_SelectedIndexChanged(sender, e);
                dlCongTrinh.SelectedValue = id.ToString();
            }
        }
    }
}