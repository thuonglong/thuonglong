﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class PhieuThuChi : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        SoQuyDataContext soquy = new SoQuyDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        string TagName = "frmPhieuThuChi";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmPhieuThuChi", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadHangMucThuChi();
                        LoadDoiTuongNhan();
                        LoadCongTrinh();
                        LoadHangMucThuChiSearch();
                        LoadCongTrinhSearch();
                        hdPage.Value = "1";
                        LoadDoiTuongNhanSearch();
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();

            dlChiNhanhChot.DataSource = query;
            dlChiNhanhChot.DataBind();
            dlChiNhanhChot.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
            dlChiNhanhSearch.Items.Insert(0, new ListItem("Tất cả chi nhánh", ""));
        }
        protected void LoadHangMucThuChi()
        {
            var query = (from p in db.tblHangMucThuChis
                         where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenHangMucThuChi
                         }).OrderBy(p => p.Ten);
            dlHangMucChi.DataSource = query;
            dlHangMucChi.DataBind();
            dlHangMucChi.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadHangMucThuChiSearch()
        {
            var query = (from p in db.tblHangMucThuChis
                         where p.TrangThai == 2
                         select new
                         {
                             p.ID,
                             Ten = p.TenHangMucThuChi
                         }).OrderBy(p => p.Ten);
            dlHangMucChiSearch.DataSource = query;
            dlHangMucChiSearch.DataBind();
            dlHangMucChiSearch.Items.Insert(0, new ListItem("Tất cả", ""));
        }
        protected void LoadDoiTuongNhan()
        {
            var query = soquy.sp_PhieuThuChi_LoadDoiTuongNhan();
            dlDoiTuongNhan.DataSource = query;
            dlDoiTuongNhan.DataBind();
            dlDoiTuongNhan.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void LoadCongTrinh()
        {
            dlCongTrinh.Items.Clear();
            if (dlDoiTuongNhan.SelectedValue != "")
            {
                var query = soquy.sp_PhieuThuChi_LoadCongTrinh(GetDrop(dlChiNhanh), GetDrop(dlDoiTuongNhan));
                dlCongTrinh.DataSource = query;
                dlCongTrinh.DataBind();
                dlCongTrinh.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlLoai.SelectedValue == "")
            {
                s += " - Chọn loại phiếu thu - chi<br />";
            }
            if (dlHangMucChi.SelectedValue == "")
            {
                s += " - Chọn hạng mục thu - chi<br />";
            }
            if (dlDoiTuongNhan.SelectedValue == "")
            {
                s += " - Chọn đối tượng nhận<br />";
            }
            if (txtNguoiNhan.Text == "")
            {
                s += " - Nhận người nhận<br />";
            }
            if (txtSoTien.Text.Trim() == "" || decimal.Parse(txtSoTien.Text) == 0)
            {
                s += " - Nhập số tiền<br />";
            }
            if (txtNoiDung.Text.Trim() == "")
            {
                s += " - Nhập nội dung<br />";
            }
            if (s == "")
            {
                var query = soquy.sp_PhieuThuChi_CheckThem(GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlDoiTuongNhan), ref s);
            }
            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (txtNgayThang.Text == "")
            {
                s += " - Nhập ngày tháng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlLoai.SelectedValue == "")
            {
                s += " - Chọn loại phiếu thu - chi<br />";
            }
            if (dlHangMucChi.SelectedValue == "")
            {
                s += " - Chọn hạng mục thu - chi<br />";
            }
            if (dlDoiTuongNhan.SelectedValue == "")
            {
                s += " - Chọn đối tượng nhận<br />";
            }
            if (txtNguoiNhan.Text == "")
            {
                s += " - Nhận người nhận<br />";
            }
            if (txtSoTien.Text.Trim() == "" || decimal.Parse(txtSoTien.Text) == 0)
            {
                s += " - Nhập số tiền<br />";
            }
            if (txtNoiDung.Text.Trim() == "")
            {
                s += " - Nhập nội dung<br />";
            }
            if (s == "")
            {
                soquy.sp_PhieuThuChi_CheckSua(new Guid(hdID.Value), GetDrop(dlChiNhanh), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), GetDrop(dlDoiTuongNhan), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var PhieuThuChi = new tblPhieuThuChi()
                        {
                            ID = Guid.NewGuid(),
                            NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text)),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            Loai = int.Parse(dlLoai.SelectedValue),
                            TenLoai = dlLoai.SelectedItem.Text.ToString(),
                            HangMucChi = GetDrop(dlHangMucChi),
                            IDDoiTuongNhan = GetDrop(dlDoiTuongNhan),
                            IDCongTrinh = dlCongTrinh.SelectedValue == "" ? (Guid?)null : GetDrop(dlCongTrinh),
                            TenCongTrinh = dlCongTrinh.SelectedValue == "" ? "" : dlCongTrinh.SelectedItem.Text.ToString(),
                            TenDoiTuongNhan = dlDoiTuongNhan.SelectedItem.Text.ToString(),
                            NguoiNhan = txtNguoiNhan.Text.Trim(),

                            SoTienThu = dlLoai.SelectedValue == "1" ? decimal.Parse(txtSoTien.Text) : 0,
                            SoTienChi = dlLoai.SelectedValue == "2" ? decimal.Parse(txtSoTien.Text) : 0,

                            NoiDung = txtNoiDung.Text.Trim(),
                            IsCongNo = ckLoai.Checked,
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblPhieuThuChis.InsertOnSubmit(PhieuThuChi);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        LoadCongTrinhSearch();
                        Search(1);
                        Success("Lưu thành công.");

                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblPhieuThuChis
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            query.NgayThang = DateTime.Parse(GetNgayThang(txtNgayThang.Text));
                            query.IDChiNhanh = GetDrop(dlChiNhanh);

                            query.Loai = int.Parse(dlLoai.SelectedValue);
                            query.TenLoai = dlLoai.SelectedItem.Text.ToString();

                            query.HangMucChi = GetDrop(dlHangMucChi);
                            query.IDDoiTuongNhan = GetDrop(dlDoiTuongNhan);
                            query.TenDoiTuongNhan = dlDoiTuongNhan.SelectedItem.Text.ToString();
                            query.IDCongTrinh = dlCongTrinh.SelectedValue == "" ? (Guid?)null : GetDrop(dlCongTrinh);
                            query.TenCongTrinh = dlCongTrinh.SelectedValue == "" ? "" : dlCongTrinh.SelectedItem.Text.ToString();
                            query.NguoiNhan = txtNguoiNhan.Text.Trim();

                            query.SoTienThu = dlLoai.SelectedValue == "1" ? decimal.Parse(txtSoTien.Text) : 0;
                            query.SoTienChi = dlLoai.SelectedValue == "2" ? decimal.Parse(txtSoTien.Text) : 0;

                            query.NoiDung = txtNoiDung.Text.Trim();
                            query.IsCongNo = ckLoai.Checked;
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            //query.STT = query.STT + 1;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            LoadCongTrinhSearch();
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin giá mua vật liệu đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlChiNhanh_SelectedIndexChanged(sender, e);
            dlHangMucChi.SelectedValue = "";
            dlDoiTuongNhan.SelectedValue = "";

            txtSoTien.Text = "";

            txtNoiDung.Text = "";
            hdID.Value = "";
        }
        protected void dlHangMucChi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlHangMucChi.SelectedValue != "")
            {
                var query = (from p in db.tblHangMucThuChis
                             where p.ID == GetDrop(dlHangMucChi)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    ckLoai.Checked = query.Loai.Value;
                }
                else
                {
                    ckLoai.Checked = false;
                }
            }
            else
            {
                ckLoai.Checked = false;
            }
        }
        //protected void UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        //{
        //    string path = Server.MapPath("~/Uploads/") + e.FileName;
        //    FileUpload1.SaveAs(path);
        //}
        protected void btnUpLoad_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string extision = FileUpload1.FileName.Trim();
                string time = DateTime.Now.ToString("yyyyMMddHHmmss");
                string filename = hdHinhAnh.Value.ToUpper() + "-" + time + extision.Substring(extision.LastIndexOf('.'));
                FileUpload1.SaveAs(Server.MapPath("~/ImagePhieuThuChi/") + filename);

                var query = (from p in db.tblPhieuThuChis
                             where p.ID == new Guid(hdHinhAnh.Value)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    query.HinhAnh = filename;
                    var log = (from p in db.tblPhieuThuChi_Logs
                               where p.IDChung == new Guid(hdHinhAnh.Value)
                               orderby p.NgayTao descending
                               select p).FirstOrDefault();

                    log.HinhAnh = filename;
                    db.SubmitChanges();
                }
                GstGetMess("Upload thành công", "");
            }
            else
            {
                GstGetMess("Bạn phải chọn file upload", "");
                mpUpLoad.Show();
            }
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";

            string id = e.CommandArgument.ToString();

            var query = (from p in db.tblPhieuThuChis
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        soquy.sp_PhieuThuChi_CheckSuaGV(query.ID, query.IDChiNhanh, query.NgayThang, query.IDDoiTuongNhan, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            txtNgayThang.Text = query.NgayThang.ToString("dd/MM/yyyy");
                            dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                            dlLoai.SelectedValue = query.Loai.ToString();
                            dlHangMucChi.SelectedValue = query.HangMucChi.ToString();
                            dlHangMucChi_SelectedIndexChanged(sender, e);
                            dlDoiTuongNhan.SelectedValue = query.IDDoiTuongNhan.ToString();
                            dlDoiTuongNhan_SelectedIndexChanged(sender, e);
                            if (query.IDCongTrinh != null)
                                dlCongTrinh.SelectedValue = query.IDCongTrinh.ToString();
                            txtNguoiNhan.Text = query.NguoiNhan;
                            txtNoiDung.Text = query.NoiDung;
                            txtSoTien.Text = string.Format("{0:N0}", query.SoTienChi > 0 ? query.SoTienChi : query.SoTienThu);
                            ckLoai.Checked = query.IsCongNo.Value;
                            hdID.Value = id;
                        }
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        soquy.sp_PhieuThuChi_CheckXoa(query.IDChiNhanh, query.NgayThang, ref thongbao);
                        if (thongbao != "")
                            Warning(thongbao);
                        else
                        {
                            if (query.TrangThai != 3)
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                            }
                            Success("Xóa thành công");
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                        }
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = soquy.sp_PhieuThuChi_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
                else if (e.CommandName == "HinhAnh")
                {
                    imgHinhAnh.ImageUrl = "~/ImagePhieuThuChi/" + query.HinhAnh;
                    mpHinhAnh.Show();
                }
                else if (e.CommandName == "UpLoad")
                {
                    hdHinhAnh.Value = id;
                    mpUpLoad.Show();
                }
            }
            else
            {
                Warning("Thông tin giá mua vật liệu đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        protected void ResetFilter()
        {
            //dlHangMucChiSearch.Items.Clear();
            //dlDoiTuongNhanSearch.Items.Clear();
            //dlNguoiNhanSearch.Items.Clear();
            //List<sp_PhieuThuChi_ResetFilterResult> query = soquy.sp_PhieuThuChi_ResetFilter(GetDrop(dlChiNhanhSearch), int.Parse(dlLoaiSearch.SelectedValue), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text))).ToList();

            //dlHangMucChiSearch.DataSource = (from p in query
            //                                 select new
            //                                 {
            //                                     ID = p.HangMucChi,
            //                                     Ten = p.TenHangMucThuChi
            //                                 }).Distinct().OrderBy(p => p.Ten);
            //dlHangMucChiSearch.DataBind();
            //dlDoiTuongNhanSearch.DataSource = (from p in query
            //                                   select new
            //                                   {
            //                                       ID = p.IDDoiTuongNhan,
            //                                       Ten = p.TenDoiTuongNhan
            //                                   }).Distinct().OrderBy(p => p.Ten);
            //dlDoiTuongNhanSearch.DataBind();
            //dlNguoiNhanSearch.DataSource = (from p in query
            //                                select new
            //                                {
            //                                    ID = p.NguoiNhan,
            //                                    Ten = p.NguoiNhan
            //                                }).Distinct().OrderBy(p => p.Ten);
            //dlNguoiNhanSearch.DataBind();
        }
        private string GetValueSelectedListBox(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = '" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private string GetValueSelectedNguoiNhan(string query, ListBox listbox)
        {
            string s = "";
            string s1 = "";
            int counter = 0;
            foreach (ListItem item in listbox.Items)
            {
                if (item.Selected)
                {
                    if (s == "")
                    {
                        s += "N'" + item.Value + "'";
                        s1 = item.Value;
                    }
                    else
                    {
                        s += ",N'" + item.Value + "'";
                    }
                    counter++;
                }
            }
            if (counter == 0)
            {
                s = "";
            }
            else if (counter == 1)
            {
                s = query + " = N'" + s1 + "'";
            }
            else
            {
                s = query + " in (" + s + ")";
            }
            return s;
        }
        private void Search(int page)
        {
            List<sp_PhieuThuChi_SearchResult> query = soquy.sp_PhieuThuChi_Search(dlChiNhanhSearch.SelectedValue, dlLoaiSearch.SelectedValue, dlHangMucChiSearch.SelectedValue, dlDoiTuongNhanSearch.SelectedValue, dlCongTrinhSearch.SelectedValue,
            DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text)), 10, page).ToList();

            GV.DataSource = query;
            GV.DataBind();
            if (GV.Rows.Count > 0)
            {
                sp_PhieuThuChi_SearchFooterResult footer = soquy.sp_PhieuThuChi_SearchFooter(dlChiNhanhSearch.SelectedValue, dlLoaiSearch.SelectedValue, dlHangMucChiSearch.SelectedValue, dlDoiTuongNhanSearch.SelectedValue, dlCongTrinhSearch.SelectedValue,
                DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).FirstOrDefault();

                GV.FooterRow.Cells[0].ColumnSpan = 12;
                GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", footer.SoLuong);
                GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                GV.FooterRow.Cells[1].Text = string.Format("{0:N0}", footer.SoTienThu);
                GV.FooterRow.Cells[2].Text = string.Format("{0:N0}", footer.SoTienChi);

                GV.FooterRow.Cells[3].Visible = false;
                GV.FooterRow.Cells[4].Visible = false;
                GV.FooterRow.Cells[5].Visible = false;
                GV.FooterRow.Cells[6].Visible = false;
                GV.FooterRow.Cells[7].Visible = false;
                GV.FooterRow.Cells[8].Visible = false;
                GV.FooterRow.Cells[9].Visible = false;
                GV.FooterRow.Cells[10].Visible = false;
                GV.FooterRow.Cells[11].Visible = false;
                GV.FooterRow.Cells[12].Visible = false;
            }

        }

        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            //if (divdl.Visible == true)
            //{
            //    divdl.Visible = false;
            //    dlHangMucChiSearch.Items.Clear();
            //    dlDoiTuongNhanSearch.Items.Clear();
            //    dlNguoiNhanSearch.Items.Clear();
            //}
            //else
            //{
            //    divdl.Visible = true;
            //    ResetFilter();
            //}
        }

        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDoiTuongNhanSearch();
            LoadCongTrinhSearch();
        }
        protected void LoadDoiTuongNhanSearch()
        {
            dlDoiTuongNhanSearch.Items.Clear();
            List<sp_PhieuThuChi_SearchLoadDoiTuongNhanResult> query = soquy.sp_PhieuThuChi_SearchLoadDoiTuongNhan(dlChiNhanhSearch.SelectedValue, DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();
            dlDoiTuongNhanSearch.DataSource = query;
            dlDoiTuongNhanSearch.DataBind();
            dlDoiTuongNhanSearch.Items.Insert(0, new ListItem("Tất cả", ""));

        }
        protected void LoadCongTrinhSearch()
        {
            dlCongTrinhSearch.Items.Clear();
            List<sp_PhieuThuChi_SearchLoadCongTrinhResult> query = soquy.sp_PhieuThuChi_SearchLoadCongTrinh(dlChiNhanhSearch.SelectedValue, DateTime.Parse(GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(GetNgayThang(txtDenNgaySearch.Text))).ToList();
            dlCongTrinhSearch.DataSource = query;
            dlCongTrinhSearch.DataBind();
            dlCongTrinhSearch.Items.Insert(0, new ListItem("Tất cả", ""));
        }
        protected void btnOpenChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Chốt sổ quỹ";
            mpChot.Show();
            btnChot.Visible = true;
            btnMoChot.Visible = false;
        }

        protected void btnOpenMoChot_Click(object sender, EventArgs e)
        {
            lblChot.Text = "Mở chốt sổ quỹ";
            mpChot.Show();
            btnChot.Visible = false;
            btnMoChot.Visible = true;
        }
        protected void btnChot_Click(object sender, EventArgs e)
        {
            string thongbao;
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else
                {
                    soquy.sp_SoQuyCongTy_CheckChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)),
                        decimal.Parse(txtSoDu.Text), decimal.Parse(txtSoThu.Text), decimal.Parse(txtSoChi.Text), decimal.Parse(txtSoTon.Text), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        soquy.sp_SoQuyCongTy_Chot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)),
                        decimal.Parse(txtSoDu.Text), decimal.Parse(txtSoThu.Text), decimal.Parse(txtSoChi.Text), decimal.Parse(txtSoTon.Text), Session["IDND"].ToString(), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }

        protected void btnMoChot_Click(object sender, EventArgs e)
        {
            string thongbao = "";
            string url = "";
            thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    GstGetMess(thongbao, "");
                }
            }
            else
            {
                if (dlChiNhanhChot.SelectedValue == "")
                {
                    GstGetMess("Chọn chi nhánh mở chốt", "");
                    mpChot.Show();
                }
                else if (txtNgayChot.Text == "")
                {
                    GstGetMess("Nhập ngày chốt", "");
                    mpChot.Show();
                }
                else
                {
                    var query = soquy.sp_SoQuyCongTy_CheckMoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref thongbao);
                    if (thongbao != "")
                    {
                        GstGetMess(thongbao, "");
                        mpChot.Show();
                    }
                    else
                    {
                        soquy.sp_SoQuyCongTy_MoChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), Session["IDND"].ToString(), ref thongbao);
                        GstGetMess(thongbao, "");
                    }
                }
            }
        }

        protected void dlChiNhanhChot_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtNgayChot.Text != "" && dlChiNhanhChot.SelectedValue != "")
            {
                decimal? soDu = 0;
                decimal? soThu = 0;
                decimal? soChi = 0;
                List<sp_SoQuyCongTy_LoadChotResult> query = soquy.sp_SoQuyCongTy_LoadChot(GetDrop(dlChiNhanhChot), DateTime.Parse(GetNgayThang(txtNgayChot.Text)), ref soDu, ref soThu, ref soChi).ToList();
                GVChot.DataSource = query;
                GVChot.DataBind();
                txtSoDu.Text = string.Format("{0:N0}", soDu);
                txtSoThu.Text = string.Format("{0:N0}", soThu);
                txtSoChi.Text = string.Format("{0:N0}", soChi);
                txtSoTon.Text = string.Format("{0:N0}", soDu + soThu - soChi);
            }
            else
            {
                GVChot.DataSource = null;
                GVChot.DataBind();
                txtSoDu.Text = "0";
                txtSoThu.Text = "0";
                txtSoChi.Text = "0";
                txtSoTon.Text = "0";
            }
            mpChot.Show();
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_PhieuThuChi_PrintResult> query = soquy.sp_PhieuThuChi_Print(dlChiNhanhSearch.SelectedValue, dlLoaiSearch.SelectedValue, dlHangMucChiSearch.SelectedValue, dlDoiTuongNhanSearch.SelectedValue, dlCongTrinhSearch.SelectedValue,
            DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();


            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }

                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "PHIẾU THU - CHI";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 3);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 3);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgaySearch.Text != txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("PHIẾU THU - CHI TỪ NGÀY" + " " + txtTuNgaySearch.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgaySearch.Text.Trim());
                }
                else if (txtTuNgaySearch.Text == txtDenNgaySearch.Text)
                {
                    r1c1.SetCellValue("PHIẾU THU - CHI NGÀY" + " " + txtTuNgaySearch.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 10);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 10);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Ngày tháng",
                    "Chi nhánh",
                    "Loại phiếu",
                    "Hạng mục chi",
                    "Đối tượng nhận",
                    "Tên công trình",
                    "Người nhân",
                    "Số tiền thu",
                    "Số tiền chi",
                    "Nội dung"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.NgayThang.ToString());
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(item.TenLoai);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(item.TenHangMucThuChi);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(item.TenDoiTuongNhan);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(item.TenCongTrinh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(item.NguoiNhan);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(double.Parse(item.SoTienThu.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(double.Parse(item.SoTienChi.ToString()));
                    cell.CellStyle = styleBodyIntRight;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(item.NoiDung);
                    cell.CellStyle = styleBody;
                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= header2.Length; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.CellFormula = "COUNT(A" + RowDau.ToString() + ":A" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(8);
                cell.CellFormula = "SUM(I" + RowDau.ToString() + ":I" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(9);
                cell.CellFormula = "SUM(J" + RowDau.ToString() + ":J" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                #endregion

                #region[Set Footer]

                //rowIndex++;
                //rowIndex++;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo ca:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo khối:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê xe:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Không dùng bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Mua bê tông:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanhSearch.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 4);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(5);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 5, 9);
                sheet.AddMergedRegion(cra);


                #endregion


                sheet.SetColumnWidth(4, 7120);
                sheet.SetColumnWidth(5, 10120);
                sheet.SetColumnWidth(5, 10120);
                sheet.SetColumnWidth(8, 4000);


                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgaySearch.Text.Trim() == txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "PHIẾU THU - CHI -" + Bienso + "-Ngày: " + "-" + txtTuNgaySearch.Text +
                                         ".xls";
                    }
                    else if (txtTuNgaySearch.Text.Trim() != txtDenNgaySearch.Text.Trim())
                    {
                        saveAsFileName = "PHIẾU THU - CHI -" + Bienso + "-Từ ngày" + "-" + txtTuNgaySearch.Text +
                                         "-Đến ngày" + "-" + txtDenNgaySearch.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }


        public IList<string[]> GetHau()
        {
            // define 
            var day1 = new[] { "A", "B", "C", "D", "E", };// must be distinct before use
            var day2 = new[] { "1", "2", "3", };// must be distinct before use

            var result = new List<string[]>
            {
                //arr1,
                //arr2,
            };

            var sophantu = 3;

            // check length of arr1 and arr2 before use
            var tohop1 = ToHop(day1, sophantu);
            var tohop2 = ToHop(day2, sophantu);

            foreach (var t1 in tohop1)
            {
                foreach (var t2 in tohop2)
                {
                    var chinhhop2 = ChinhHop(t2);
                    foreach (var c2 in chinhhop2)
                    {
                        var dayhop = new List<string>();
                        for (int i = 0; i < sophantu; i++)
                        {
                            var phantu = $"{t1[i]}{c2[i]}";
                            dayhop.Add(phantu);
                        }

                        result.Add(dayhop.ToArray());
                    }
                }
            }
            return result;
        }

        static IList<T[]> ToHop<T>(IList<T> input, int k)
        {
            var n = input.Count;
            var a = new int[n + 1];
            var dau = new int[n + 1];
            for (int i = 0; i < n + 1; i++)
                dau[i] = 0;

            a[0] = 1;

            var indexes = new List<int[]>();
            ToHop(1, n, k, a, dau, indexes);

            var result = new List<T[]>();
            foreach (var index in indexes)
            {
                var items = index.Select(x => input[x - 1]).ToArray();
                result.Add(items);
            }

            return result;
        }

        static void ToHop(int i, int n, int k, int[] a, int[] dau, IList<int[]> result)
        {
            if (i > k)
            {
                XuatToHop(i, k, a, result);
            }
            else
            {
                int j;
                for (j = a[i - 1]; j <= n; j++)
                {
                    if (dau[j] == 0)
                    {
                        a[i] = j;
                        dau[j] = 1;
                        ToHop(i + 1, n, k, a, dau, result);
                        dau[j] = 0;
                    }
                }
            }
        }

        static void XuatToHop(int i, int k, int[] a, IList<int[]> result)
        {
            var items = new List<int>();
            for (int j = 1; j <= k; j++)
                items.Add(a[j]);

            result.Add(items.ToArray());
        }

        static IList<T[]> ChinhHop<T>(IList<T> input)
        {
            var n = input.Count;
            var a = new int[n + 1];
            var dau = new int[n + 1];

            var indexes = new List<int[]>();
            ChinhHop(1, n, a, dau, indexes);


            var result = new List<T[]>();
            foreach (var index in indexes)
            {
                var items = index.Select(x => input[x - 1]).ToArray();
                result.Add(items);
            }

            return result;
        }

        static void ChinhHop(int i, int n, int[] a, int[] dau, IList<int[]> result)
        {
            for (int j = 1; j <= n; j++)
            {
                if (dau[j] == 0)
                {
                    a[i] = j;
                    dau[j] = 1;
                    if (i == n)
                        XuatChinhHop(n, a, result);
                    else
                        ChinhHop(i + 1, n, a, dau, result);
                    dau[j] = 0;
                }
            }
        }

        static void XuatChinhHop(int n, int[] a, IList<int[]> result)
        {
            var items = new List<int>();
            for (int i = 1; i <= n; i++)
                items.Add(a[i]);

            result.Add(items.ToArray());
        }

        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCongTrinh();
        }

        protected void dlDoiTuongNhan_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCongTrinh();
        }
    }
}