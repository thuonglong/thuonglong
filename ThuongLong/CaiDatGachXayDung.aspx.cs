﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class CaiDatGachXayDung : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["TruyVan"].ConnectionString;
        DBDataContext db = new DBDataContext();
        VatLieuDataContext vatlieu = new VatLieuDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmDinhMucGach";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmCaiDatGachXayDung", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadLoaiVatLieu();
                    }
                }
            }
        }

        protected void LoadChiNhanh()
        {
            List<sp_LoadChiNhanhDaiLyResult> query = db.sp_LoadChiNhanhDaiLy().ToList();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadLoaiVatLieu()
        {
            var qlLoaiSanPham = (from p in db.tblLoaiVatLieus
                                 where p.TrangThai == 2
                                 && p.IDNhomVatLieu == new Guid("97CF73BB-E552-4A07-9F8B-5DD5C68F7287")
                                 select new
                                 {
                                     p.ID,
                                     Ten = p.TenLoaiVatLieu
                                 }).OrderBy(p => p.Ten);

            dlLoaiSanPham.DataSource = qlLoaiSanPham;
            dlLoaiSanPham.DataBind();
            dlLoaiSanPham.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlCatSongDa1 = (from p in db.tblLoaiVatLieus
                                where p.TrangThai == 2
                                && p.IDNhomVatLieu == new Guid("8B27D124-A687-400E-89F6-E3114A08CF87")
                                select new
                                {
                                    p.ID,
                                    Ten = p.TenLoaiVatLieu
                                }).OrderBy(p => p.Ten);

            dlCatSongDa.DataSource = qlCatSongDa1;
            dlCatSongDa.DataBind();
            dlCatSongDa.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlBotMau = (from p in db.tblLoaiVatLieus
                            where p.TrangThai == 2
                            && p.IDNhomVatLieu == new Guid("B4D442BD-773F-49F7-920E-0C22546F6C0A")
                            select new
                            {
                                p.ID,
                                Ten = p.TenLoaiVatLieu
                            }).OrderBy(p => p.Ten);

            dlDaMat.DataSource = qlBotMau;
            dlDaMat.DataBind();
            dlDaMat.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlKeoBong = (from p in db.tblLoaiVatLieus
                             where p.TrangThai == 2
                             && p.IDNhomVatLieu == new Guid("AC14CAFA-F77A-4F9B-8E06-6D19D7202AEC")
                             select new
                             {
                                 p.ID,
                                 Ten = p.TenLoaiVatLieu
                             }).OrderBy(p => p.Ten);

            dlVatLieuKhac.DataSource = qlKeoBong;
            dlVatLieuKhac.DataBind();
            dlVatLieuKhac.Items.Insert(0, new ListItem("--Chọn--", ""));

            var qlXiMangPCB401 = (from p in db.tblLoaiVatLieus
                                  where p.TrangThai == 2
                                  && p.IDNhomVatLieu == new Guid("36FCDA11-FEF0-43C5-8AB8-C55F24281FF4")
                                  select new
                                  {
                                      p.ID,
                                      Ten = p.TenLoaiVatLieu
                                  }).OrderBy(p => p.Ten);

            dlXiMangPCB40.DataSource = qlXiMangPCB401;
            dlXiMangPCB40.DataBind();
            dlXiMangPCB40.Items.Insert(0, new ListItem("--Chọn--", ""));

            //dlXiMangPCB40.Items.Insert(0, new ListItem("Xi măng PCB 40", "840482F9-D5F6-4926-B6F1-77657A7A95D6"));
            //dlCatSongDa.Items.Insert(0, new ListItem("Cát sông đà", "51C056B4-DB1F-4024-817F-6E08F9EBF1F0"));
            //dlDaMat.Items.Insert(0, new ListItem("Mạt đá", "7C716F3F-6262-4621-8452-9F8F43D02411"));
            //dlVatLieuKhac.Items.Insert(0, new ListItem("Vật liệu khác", "EC0AA0AB-697F-4449-8E84-CF79E20C8E0C"));
        }
        string CheckThem()
        {
            string s = "";
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlLoaiSanPham.SelectedValue == "")
            {
                s += " - Chọn loại sản phẩm<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn xi măng<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn cát<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn đá mạt<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn vật liệu khác<br />";
            }

            if (txtXiMangPCB40.Text == "")
            {
                s += " - Nhập định mức xi măng PCB40<br />";
            }
            if (txtCatSongDa.Text == "")
            {
                s += " - Nhập định mức cát sông đà 1<br />";
            }
            if (txtDaMat.Text == "")
            {
                s += " - Nhập định mức đá mạt<br />";
            }
            if (txtVatLieuKhac.Text == "")
            {
                s += " - Nhập định mức vật liệu khác<br />";
            }
            if (hdID.Value == "")
            {
                var query = (from p in db.tblCaiDatGachMenBongs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    s += "Đã thiết lập định mức cho loại gạch này";
                }
            }
            else
            {
                var query = (from p in db.tblCaiDatGachMenBongs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             && p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             && p.ThoiTiet == int.Parse(dlThoiTiet.SelectedValue)
                             && p.ID != new Guid(hdID.Value)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    s += "Đã thiết lập định mức cho loại gạch này";
                }
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";

            if (hdID.Value == "")
            {
                string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 1, TagName, ref url);
                if (thongbao != "")
                {
                    if (url != "")
                    {
                        GstGetMess(thongbao, url);
                    }
                    else
                    {
                        Warning(thongbao);
                    }
                }
                else
                {
                    if (CheckThem() == "")
                    {
                        var CaiDatGachXayDung = new tblCaiDatGachXayDung()
                        {
                            ID = Guid.NewGuid(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDLoaiVatLieu = GetDrop(dlLoaiSanPham),
                            ThoiTiet = int.Parse(dlThoiTiet.SelectedValue),
                            XiMangPCB40 = GetDrop(dlXiMangPCB40),
                            CatSongDa = GetDrop(dlCatSongDa),
                            DaMat = GetDrop(dlDaMat),
                            VatLieuKhac = GetDrop(dlVatLieuKhac),
                            DMXiMangPCB40 = double.Parse(txtXiMangPCB40.Text.Trim()),
                            DMCatSongDa = double.Parse(txtCatSongDa.Text.Trim()),
                            DMDaMat = double.Parse(txtDaMat.Text.Trim()),
                            DMVatLieuKhac = double.Parse(txtVatLieuKhac.Text.Trim()),

                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            //STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblCaiDatGachXayDungs.InsertOnSubmit(CaiDatGachXayDung);
                        db.SubmitChanges();
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                var query = (from p in db.tblCaiDatGachXayDungs
                             where p.ID == new Guid(hdID.Value)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanh), Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (CheckThem() == "")
                        {
                            query.IDChiNhanh = GetDrop(dlChiNhanh);

                            query.IDLoaiVatLieu = GetDrop(dlLoaiSanPham);
                            query.ThoiTiet = int.Parse(dlThoiTiet.SelectedValue);
                            query.XiMangPCB40 = GetDrop(dlXiMangPCB40);
                            query.CatSongDa = GetDrop(dlCatSongDa);
                            query.DaMat = GetDrop(dlDaMat);
                            query.VatLieuKhac = GetDrop(dlVatLieuKhac);
                            query.DMXiMangPCB40 = double.Parse(txtXiMangPCB40.Text.Trim());
                            query.DMCatSongDa = double.Parse(txtCatSongDa.Text.Trim());
                            query.DMDaMat = double.Parse(txtDaMat.Text.Trim());
                            query.DMVatLieuKhac = double.Parse(txtVatLieuKhac.Text.Trim());

                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";

                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            //lblTaoMoiHopDong_Click(sender, e);

                            Success("Sửa thành công");
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            dlXiMangPCB40.SelectedValue = "";
            dlCatSongDa.SelectedValue = "";
            dlDaMat.SelectedValue = "";
            dlVatLieuKhac.SelectedValue = "";

            txtXiMangPCB40.Text = "";
            txtCatSongDa.Text = "";
            txtDaMat.Text = "";
            txtVatLieuKhac.Text = "";
            hdID.Value = "";
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }

        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlChiNhanh.SelectedValue != "" && dlLoaiSanPham.SelectedValue != "")
            {
                var query = (from p in db.tblCaiDatGachXayDungs
                             where p.IDChiNhanh == GetDrop(dlChiNhanh)
                             where p.IDLoaiVatLieu == GetDrop(dlLoaiSanPham)
                             select p).FirstOrDefault();
                if (query != null && query.ID != null)
                {
                    dlXiMangPCB40.SelectedValue = query.XiMangPCB40.ToString();
                    dlCatSongDa.SelectedValue = query.CatSongDa.ToString();
                    dlDaMat.SelectedValue = query.DaMat.ToString();
                    dlVatLieuKhac.SelectedValue = query.VatLieuKhac.ToString();

                    txtXiMangPCB40.Text = query.DMXiMangPCB40.ToString();
                    txtCatSongDa.Text = query.DMCatSongDa.ToString();
                    txtDaMat.Text = query.DMDaMat.ToString();
                    txtVatLieuKhac.Text = query.DMVatLieuKhac.ToString();
                }
                else
                    lblTaoMoiHopDong_Click(sender, e);
            }
            else
                lblTaoMoiHopDong_Click(sender, e);
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblCaiDatGachXayDungs
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlThoiTiet.SelectedValue = query.ThoiTiet.ToString();
                        dlLoaiSanPham.SelectedValue = query.IDLoaiVatLieu.ToString();
                        dlXiMangPCB40.SelectedValue = query.XiMangPCB40.ToString();
                        dlCatSongDa.SelectedValue = query.CatSongDa.ToString();
                        dlDaMat.SelectedValue = query.DaMat.ToString();
                        dlVatLieuKhac.SelectedValue = query.VatLieuKhac.ToString();
                        txtXiMangPCB40.Text = query.DMXiMangPCB40.ToString();
                        txtCatSongDa.Text = query.DMCatSongDa.ToString();
                        txtDaMat.Text = query.DMDaMat.ToString();
                        txtVatLieuKhac.Text = query.DMVatLieuKhac.ToString();

                        hdID.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTac(query.IDChiNhanh, Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai == 1)
                        {
                            int countxoa = (from p in db.tblCaiDatGachXayDung_Logs
                                            where p.IDChung == query.ID
                                            select p).Count();
                            if (countxoa == 1)
                            {
                                db.tblCaiDatGachXayDungs.DeleteOnSubmit(query);
                                db.SubmitChanges();
                                Success("Đã xóa");
                            }
                            else
                            {
                                query.TrangThai = 3;
                                query.TrangThaiText = "Chờ duyệt xóa";
                                db.SubmitChanges();
                                Success("Xóa thành công");
                            }
                        }
                        else if (query.TrangThai == 2)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                            Success("Xóa thành công");
                        }
                        else
                        {
                            Warning("Dữ liệu đang chờ duyệt xóa");
                        }
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = vatlieu.sp_CaiDatGachXayDung_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin định mức đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void Search(int page)
        {
            var query = vatlieu.sp_CaiDatGachXayDung_Search(GetDrop(dlChiNhanhSearch), dlLoaiVatLieuSearch.SelectedValue == "" ? (Guid?)null : GetDrop(dlLoaiVatLieuSearch), 20, page);

            GV.DataSource = query;
            GV.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTac(GetDrop(dlChiNhanhSearch), Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void dlChiNhanhSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiVatLieuSearch();
        }
        void LoadLoaiVatLieuSearch()
        {
            dlLoaiVatLieuSearch.Items.Clear();
            var query = (from p in db.tblCaiDatGachXayDungs
                         join q in db.tblLoaiVatLieus on p.IDLoaiVatLieu equals q.ID
                         where p.IDChiNhanh == GetDrop(dlChiNhanhSearch)
                         select new
                         {
                             ID = p.IDLoaiVatLieu,
                             Ten = q.TenLoaiVatLieu
                         }).Distinct().OrderBy(p => p.Ten);
            dlLoaiVatLieuSearch.DataSource = query;
            dlLoaiVatLieuSearch.DataBind();
            dlLoaiVatLieuSearch.Items.Insert(0, new ListItem("--Chọn--", ""));
        }
        protected void dlThoiTiet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlLoaiSanPham.SelectedValue != "" && dlChiNhanh.SelectedValue != "")
            {
                dlChiNhanhSearch_SelectedIndexChanged(sender, e);
            }
        }
    }
}