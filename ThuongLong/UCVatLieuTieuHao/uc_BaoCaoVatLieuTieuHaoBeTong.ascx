﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_BaoCaoVatLieuTieuHaoBeTong.ascx.cs" Inherits="ThuongLong.UCVatLieuTieuHao.uc_BaoCaoVatLieuTieuHaoBeTong" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="panel-body">
            <div class="row">
                <div class="form-group col-lg-3">
                    <label for="exampleInputEmail1">Từ ngày</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox ID="txtTuNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""
                            Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group col-lg-3">
                    <label for="exampleInputEmail1">Đến ngày</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox ID="txtDenNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""
                            Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="col-lg-3" runat="server">
                    <div class="form-group">
                        <label>Chi nhánh</label>
                        <asp:DropDownList ID="dlChiNhanh" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2"
                            Width="98%" OnSelectedIndexChanged="dlChiNhanh_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3" runat="server">
                    <div class="form-group">
                        <label>Mác bê tông</label>
                        <asp:DropDownList ID="dlMacBeTong" runat="server" DataTextField="Ten" DataValueField="ID" CssClass="form-control select2" Width="98%"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Xem</asp:LinkButton>
                        <asp:LinkButton ID="btnPrint" runat="server" class="btn btn-app bg-green" OnClick="btnPrint_Click"><i class="fa fa-print"></i>Xuất Excel</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" ShowFooter="true" OnRowCreated="GV_RowCreated"
                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                <Columns>
                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                    <asp:BoundField DataField="MacBeTong" HeaderText="Mác bê tông" />
                    <asp:BoundField DataField="XiMang" HeaderText="Xi măng" />
                    <asp:BoundField DataField="Da" HeaderText="Đá" />
                    <asp:BoundField DataField="Cat" HeaderText="Cát" />
                    <asp:BoundField DataField="PhuGia" HeaderText="Phụ gia" />
                    <asp:BoundField DataField="TroBay" HeaderText="Tro bay" />
                    <asp:BoundField DataField="KLHoaDon" HeaderText="KL hóa đơn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLKhachHang" HeaderText="KL bán" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLThucXuat" HeaderText="KL thực xuất" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLQuaCan" HeaderText="KL qua cân" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <%--<asp:BoundField DataField="KLTramTron" HeaderText="KL trạm trộn" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />--%>
                    <asp:BoundField DataField="KLXiMang" HeaderText="KL xi măng" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLDa" HeaderText="KL đá" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLCat" HeaderText="KL cát" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLPhuGia" HeaderText="KL phụ gia" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="KLTroBay" HeaderText="KL tro bay" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                    HorizontalAlign="Right" />
                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
            </asp:GridView>
        </div>
        <div style="margin: 5px;" class="btn-group">
            <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
            <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
            <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
        </div>
        <asp:HiddenField ID="hdID" runat="server" />
        <asp:HiddenField ID="hdPage" runat="server" />
        <asp:HiddenField ID="hdDuyet" runat="server" />
        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
        <asp:HiddenField ID="hdTuNgay" runat="server" />
        <asp:HiddenField ID="hdDenNgay" runat="server" />
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnPrint" />
    </Triggers>
</asp:UpdatePanel>


