﻿using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong.UCVatLieuTieuHao
{
    public partial class uc_BaoCaoVatLieuTieuHaoGach_Terrazo : System.Web.UI.UserControl
    {
        private string frmName = "frmBaoCaoVatLieuTieuHaoGachTerrazo";
        private static int PageNumber = 20;
        private static int CurrentPage = 1;
        private DBDataContext db = new DBDataContext();
        VatLieuDataContext betong = new VatLieuDataContext();
        private clsPhanQuyen phanquyen = new clsPhanQuyen();
        clsXuLy xl = new clsXuLy();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else if (!IsPostBack)
            {
                DateTime dateTimenow = DateTime.Now;
                var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                txtTuNgay.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                txtDenNgay.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                LoadChiNhanh();
                LoadNhaCungCap();
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanh();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("Tất cả chi nhánh", ""));
        }
        void LoadNhaCungCap()
        {
            dlMacBeTong.Items.Clear();
            var query = betong.sp_BaoCaoVatLieuTieuHaoGach_Terrazo_LoadLoaiGach(dlChiNhanh.SelectedValue, DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text)));
            dlMacBeTong.DataSource = query;
            dlMacBeTong.DataBind();
            dlMacBeTong.Items.Insert(0, new ListItem("Tất cả loại gạch", ""));
        }
        public DateTime GetLastDayOfMonth(int iMonth, int iYear)
        {
            DateTime dtResult = new DateTime(iYear, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-1);
            return dtResult;
        }
        protected void dlChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }

        private Guid GetDrop(DropDownList dl)
        {
            if (dl.SelectedValue.Trim().Equals(""))
            {
                return Guid.Empty;
            }
            else
            {
                return new Guid(dl.SelectedValue);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdPage.Value = "1";
            CurrentPage = 1;
            Search(1);
            btnPre.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = true;
        }
        private void Search(int page)
        {
            List<sp_BaoCaoVatLieuTieuHaoGach_Terrazo_SearchResult> dt = betong.sp_BaoCaoVatLieuTieuHaoGach_Terrazo_Search(dlChiNhanh.SelectedValue, dlMacBeTong.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text)), PageNumber, page).ToList();
            GV.DataSource = dt;
            GV.DataBind();
            hdTuNgay.Value = txtTuNgay.Text;
            hdDenNgay.Value = txtDenNgay.Text;
            if (GV.Rows.Count > 0)
            {

                sp_BaoCaoVatLieuTieuHaoGach_Terrazo_SearchFooterResult query = betong.sp_BaoCaoVatLieuTieuHaoGach_Terrazo_SearchFooter(dlChiNhanh.SelectedValue, dlMacBeTong.SelectedValue,
                    DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text))).FirstOrDefault();

                if (query != null)
                {
                    GV.FooterRow.Cells[0].ColumnSpan = 12;
                    GV.FooterRow.Cells[0].Text = "Tổng cộng: " + string.Format("{0:N0}", query.Dem);
                    GV.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    GV.FooterRow.Cells[1].Text = string.Format("{0:N2}", query.SoLuong);
                    GV.FooterRow.Cells[2].Text = string.Format("{0:N2}", query.SoLuongHaoHut);
                    GV.FooterRow.Cells[3].Text = string.Format("{0:N2}", query.TLMauXi);
                    GV.FooterRow.Cells[4].Text = string.Format("{0:N2}", query.TLMauDo);
                    GV.FooterRow.Cells[5].Text = string.Format("{0:N2}", query.TLBotDa);
                    GV.FooterRow.Cells[6].Text = string.Format("{0:N2}", query.TLDaDen2ly);
                    GV.FooterRow.Cells[7].Text = string.Format("{0:N2}", query.TLDaTrang2Ly);
                    GV.FooterRow.Cells[8].Text = string.Format("{0:N2}", query.TLDaTrang4Ly);
                    GV.FooterRow.Cells[9].Text = string.Format("{0:N2}", query.TLXiMangPCB401);
                    GV.FooterRow.Cells[10].Text = string.Format("{0:N2}", query.TLXiMangPCB402);
                    GV.FooterRow.Cells[11].Text = string.Format("{0:N2}", query.TLMatDa);
                    GV.FooterRow.Cells[12].Text = string.Format("{0:N2}", query.TLCatSongDa);

                    GV.FooterRow.Cells[13].Visible = false;
                    GV.FooterRow.Cells[14].Visible = false;
                    GV.FooterRow.Cells[15].Visible = false;
                    GV.FooterRow.Cells[16].Visible = false;
                    GV.FooterRow.Cells[17].Visible = false;
                    GV.FooterRow.Cells[18].Visible = false;
                    GV.FooterRow.Cells[19].Visible = false;
                    GV.FooterRow.Cells[20].Visible = false;
                    GV.FooterRow.Cells[21].Visible = false;
                    GV.FooterRow.Cells[22].Visible = false;
                }
            }
        }

        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header) // If header created
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;

                GridView grid = (GridView)sender;
                GridViewRow headerRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell headerCell = new TableCell
                {
                    Text = "Ngày tháng",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Chi nhánh",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Loại gạch",
                    HorizontalAlign = HorizontalAlign.Center,
                    RowSpan = 2,
                    CssClass = "HeaderStyle"
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Vật liệu lớp mặt",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 7,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Vật liệu lớp thân",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Thông tin số lượng",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 2,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Số lượng vật liệu lớp mặt",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 7,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                headerCell = new TableCell
                {
                    Text = "Số lượng vật liệu lớp thân",
                    HorizontalAlign = HorizontalAlign.Center,
                    ColumnSpan = 3,
                    CssClass = "HeaderStyle",
                    BackColor = Color.BurlyWood,
                };
                headerRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerRow);
            }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            List<sp_BaoCaoVatLieuTieuHaoGach_Terrazo_PrintResult> query = betong.sp_BaoCaoVatLieuTieuHaoGach_Terrazo_Print(dlChiNhanh.SelectedValue, dlMacBeTong.SelectedValue,
                DateTime.Parse(GetNgayThang(txtTuNgay.Text)), DateTime.Parse(GetNgayThang(txtDenNgay.Text))).ToList();

            if (query.Count > 0)
            {
                string dc = "", tencongty = "", sdt = "";
                var tencty = (from x in db.tblThamSos
                              select x).FirstOrDefault();
                if (tencty != null && tencty.ID != null)
                {
                    dc = tencty.DiaChi;
                    sdt = tencty.SoDienThoai;
                    tencongty = tencty.TenCongTy;
                }


                var workbook = new HSSFWorkbook();
                IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                string sheetname = "BÁO CÁO VLTH GẠCH TERRAZO";

                #region[CSS]

                var sheet = workbook.CreateSheet(sheetname);
                sheet = xl.SetPropertySheet(sheet);

                IFont fontbody = xl.CreateFont(workbook, "Times New Roman", 10, false, false);
                IFont fontfooter = xl.CreateFont(workbook, "Times New Roman", 10, true, false);
                IFont fontheader1 = xl.CreateFont(workbook, "Times New Roman", 16, true, false);
                IFont fontheader2 = xl.CreateFont(workbook, "Times New Roman", 13, true, false);

                ICellStyle styleBody = xl.CreateCellStyle(workbook, 0, true, fontbody);
                ICellStyle styleBodyIntRight = xl.CreateCellStyleInt(workbook, 2, true, fontbody);
                ICellStyle styleBodyIntCenter = xl.CreateCellStyleInt(workbook, 1, true, fontbody);
                ICellStyle styleBodyIntFooterRight = xl.CreateCellStyleInt(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDoubleRight = xl.CreateCellStyleDouble(workbook, 2, true, fontbody);
                ICellStyle styleBodyDoubleFooterRight = xl.CreateCellStyleDouble(workbook, 2, true, fontfooter);
                ICellStyle styleBodyDatetime = xl.CreateCellStyleDatetime(workbook, 1, true, dataFormatCustom, fontbody);
                ICellStyle styleHeaderChiTietCenter = xl.CreateCellStyleHeader(workbook, 1, true, fontfooter);

                ICellStyle styleboldcenternoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                ICellStyle styleboldrightborder = xl.CreateCellStyle(workbook, 2, true, fontfooter);

                ICellStyle styleHeader1 = xl.CreateCellStyle(workbook, 1, false, fontheader1);
                ICellStyle styleHeader2 = xl.CreateCellStyle(workbook, 1, false, fontheader2);

                ICellStyle styleFooterText = xl.CreateCellStyle(workbook, 2, false, fontfooter);
                ICellStyle styleBodyIntFooterLeftNoborder = xl.CreateCellStyleInt(workbook, 0, false, fontfooter);
                ICellStyle styleBodyFooterCenterNoborder = xl.CreateCellStyle(workbook, 1, false, fontfooter);
                #endregion

                #region[Tạo header trên]

                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                ICell r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(tencongty);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 5);
                sheet.AddMergedRegion(cra);
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);
                r1c1.SetCellValue(dc);
                r1c1.CellStyle = styleboldcenternoborder;
                r1c1.Row.Height = 400;
                cra = new CellRangeAddress(1, 1, 0, 5);
                sheet.AddMergedRegion(cra);
                //Tiêu đề báo cáo

                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                r1c1 = row.CreateCell(0);

                if (txtTuNgay.Text != txtDenNgay.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO VLTH GẠCH TERRAZO TỪ NGÀY" + " " + txtTuNgay.Text.Trim() +
                                      "  " + "ĐẾN NGÀY " + "" + txtDenNgay.Text.Trim());
                }
                else if (txtTuNgay.Text == txtDenNgay.Text)
                {
                    r1c1.SetCellValue("BÁO CÁO VLTH GẠCH TERRAZO NGÀY" + " " + txtTuNgay.Text.Trim() + "");
                }

                r1c1.CellStyle = styleHeader1;
                r1c1.Row.Height = 800;
                cra = new CellRangeAddress(2, 2, 0, 25);
                sheet.AddMergedRegion(cra);

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                r1c1 = row.CreateCell(0);
                string kq = "";

                r1c1.SetCellValue(kq);



                r1c1.CellStyle = styleHeader2;
                r1c1.Row.Height = 500;
                cra = new CellRangeAddress(3, 3, 0, 25);
                sheet.AddMergedRegion(cra);
                sheet.CreateFreezePane(0, 6);
                #endregion

                #region[Tạo header dưới]
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                //for (int i = 1; i <= 60; i++)
                //{
                //    cra = new CellRangeAddress(4, 5, i, i);
                //    sheet.AddMergedRegion(cra);
                //}

                //sheet.AddMergedRegion(cra);
                string[] header1 =
                  {
                    "STT",
                    "Ngày tháng",
                    "Chi nhánh",
                    "Loại gạch",
                    "Vật liệu lớp mặt",
                    "Vật liệu lớp mặt",
                    "Vật liệu lớp mặt",
                    "Vật liệu lớp mặt",
                    "Vật liệu lớp mặt",
                    "Vật liệu lớp mặt",
                    "Vật liệu lớp mặt",
                    "Vật liệu lớp thân",
                    "Vật liệu lớp thân",
                    "Vật liệu lớp thân",
                    "Thông tin số lượng",
                    "Thông tin số lượng",
                    "Số lượng vật liệu lớp mặt",
                    "Số lượng vật liệu lớp mặt",
                    "Số lượng vật liệu lớp mặt",
                    "Số lượng vật liệu lớp mặt",
                    "Số lượng vật liệu lớp mặt",
                    "Số lượng vật liệu lớp mặt",
                    "Số lượng vật liệu lớp mặt",
                    "Số lượng vật liệu lớp thân",
                    "Số lượng vật liệu lớp thân",
                    "Số lượng vật liệu lớp thân"
                  };

                for (int h = 0; h < header1.Length; h++)
                {
                    r1c1 = row.CreateCell(h);
                    r1c1.SetCellValue(header1[h].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 500;
                }

                rowIndex++;
                row = sheet.CreateRow(rowIndex);

                string[] header2 =
                    {
                    "Ngày tháng",
                    "Chi nhánh",
                    "Loại gạch",
                    "Màu xi",
                    "Màu đỏ",
                    "Bột đá",
                    "Đá đen 2 ly",
                    "Đá trắng 2 ly",
                    "Đá trắng 4 ly",
                    "Xi măng PCB40",
                    "Xi măng PCB40",
                    "Mạt đá",
                    "Cát sông đà",
                    "Số lượng đạt",
                    "Số lượng hao hụt",
                    "Màu xi",
                    "Màu đỏ",
                    "Bột đá",
                    "Đá đen 2 ly",
                    "Đá trắng 2 ly",
                    "Đá trắng 4 ly",
                    "Xi măng PCB40",
                    "Xi măng PCB40",
                    "Mạt đá",
                    "Cát sông đà"
                };
                var cell = row.CreateCell(0);

                cell.SetCellValue("STT");
                cell.CellStyle = styleHeaderChiTietCenter;
                cell.Row.Height = 600;

                cra = new CellRangeAddress(4, 5, 0, 0);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 1, 1);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 2, 2);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 5, 3, 3);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 4, 10);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 11, 13);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 14, 15);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 16, 22);
                sheet.AddMergedRegion(cra);
                cra = new CellRangeAddress(4, 4, 23, 25);
                sheet.AddMergedRegion(cra);

                for (int hk = 0; hk < header2.Length; hk++)
                {
                    r1c1 = row.CreateCell(hk + 1);
                    r1c1.SetCellValue(header2[hk].ToString());
                    r1c1.CellStyle = styleHeaderChiTietCenter;
                    r1c1.Row.Height = 800;
                }
                #endregion

                #region[ghi dữ liệu]
                int STT2 = 0;

                string RowDau = (rowIndex + 2).ToString();
                foreach (var item in query)
                {
                    STT2++;
                    rowIndex++;
                    row = sheet.CreateRow(rowIndex);

                    cell = row.CreateCell(0);
                    cell.SetCellValue(STT2);
                    cell.CellStyle = styleBodyIntCenter;

                    cell = row.CreateCell(1);
                    cell.SetCellValue(item.NgayThang);
                    cell.CellStyle = styleBodyDatetime;

                    cell = row.CreateCell(2);
                    cell.SetCellValue(item.TenChiNhanh);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(3);
                    cell.SetCellValue(item.LoaiGach);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(4);
                    cell.SetCellValue(item.MauXi);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(5);
                    cell.SetCellValue(item.MauDo);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(6);
                    cell.SetCellValue(item.BotDa);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(7);
                    cell.SetCellValue(item.DaDen2ly);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(8);
                    cell.SetCellValue(item.DaTrang2Ly);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(9);
                    cell.SetCellValue(item.DaTrang4Ly);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(10);
                    cell.SetCellValue(item.XiMangPCB401);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(11);
                    cell.SetCellValue(item.XiMangPCB402);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(12);
                    cell.SetCellValue(item.MatDa);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(13);
                    cell.SetCellValue(item.CatSongDa);
                    cell.CellStyle = styleBody;

                    cell = row.CreateCell(14);
                    cell.SetCellValue(double.Parse(item.SoLuong.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(15);
                    cell.SetCellValue(double.Parse(item.SoLuongHaoHut.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(16);
                    cell.SetCellValue(double.Parse(item.TLMauXi.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(17);
                    cell.SetCellValue(double.Parse(item.TLMauDo.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(18);
                    cell.SetCellValue(double.Parse(item.TLBotDa.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(19);
                    cell.SetCellValue(double.Parse(item.TLDaDen2ly.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(20);
                    cell.SetCellValue(double.Parse(item.TLDaTrang2Ly.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(21);
                    cell.SetCellValue(double.Parse(item.TLDaTrang4Ly.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(22);
                    cell.SetCellValue(double.Parse(item.TLXiMangPCB401.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(23);
                    cell.SetCellValue(double.Parse(item.TLXiMangPCB402.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(24);
                    cell.SetCellValue(double.Parse(item.TLMatDa.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                    cell = row.CreateCell(25);
                    cell.SetCellValue(double.Parse(item.TLCatSongDa.ToString()));
                    cell.CellStyle = styleBodyDoubleRight;

                }
                #endregion

                #region Tổng cộng
                rowIndex++;
                string RowCuoi = rowIndex.ToString();
                row = sheet.CreateRow(rowIndex);
                for (int i = 1; i <= header2.Length; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = styleBodyIntRight;
                    cell.Row.Height = 400;
                }
                cell = row.CreateCell(0);
                cell.SetCellValue("Tổng cộng:");
                cell.Row.Height = 400;
                cell.CellStyle = styleboldrightborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 8);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(14);
                cell.CellFormula = "SUM(O" + RowDau.ToString() + ":O" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(15);
                cell.CellFormula = "SUM(P" + RowDau.ToString() + ":P" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(16);
                cell.CellFormula = "SUM(Q" + RowDau.ToString() + ":Q" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(17);
                cell.CellFormula = "SUM(R" + RowDau.ToString() + ":R" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(18);
                cell.CellFormula = "SUM(S" + RowDau.ToString() + ":S" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(19);
                cell.CellFormula = "SUM(T" + RowDau.ToString() + ":T" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(20);
                cell.CellFormula = "SUM(U" + RowDau.ToString() + ":U" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(21);
                cell.CellFormula = "SUM(V" + RowDau.ToString() + ":V" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(22);
                cell.CellFormula = "SUM(W" + RowDau.ToString() + ":W" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(23);
                cell.CellFormula = "SUM(X" + RowDau.ToString() + ":X" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(24);
                cell.CellFormula = "SUM(Y" + RowDau.ToString() + ":Y" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                cell = row.CreateCell(25);
                cell.CellFormula = "SUM(Z" + RowDau.ToString() + ":Z" + RowCuoi.ToString() + ")";
                cell.CellStyle = styleBodyIntFooterRight;

                #endregion

                #region[Set Footer]

                //rowIndex++;
                //rowIndex++;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo ca:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(Z" + RowDau.ToString() + ":Z" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Bơm theo khối:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Bơm theo ca\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Thuê xe:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AH" + RowDau.ToString() + ":AH" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //cra = new CellRangeAddress(rowIndex, rowIndex, 3, 5);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(3);
                //cell.SetCellValue("Không dùng bơm:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(6);
                //cell.CellFormula = "COUNTIF(V" + RowDau.ToString() + ":V" + RowCuoi + ",\"Không dùng bơm\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //cra = new CellRangeAddress(rowIndex, rowIndex, 8, 11);
                //sheet.AddMergedRegion(cra);
                //cell = row.CreateCell(8);
                //cell.SetCellValue("Mua bê tông:");
                //cell.CellStyle = styleFooterText;

                //cell = row.CreateCell(12);
                //cell.CellFormula = "COUNTIF(AO" + RowDau.ToString() + ":AO" + RowCuoi + ",\"x\")";
                //cell.CellStyle = styleBodyIntFooterLeftNoborder;

                //rowIndex++;
                //rowIndex++;
                //row = sheet.CreateRow(rowIndex);

                //đại diện
                rowIndex++; rowIndex++;
                row = sheet.CreateRow(rowIndex);

                cell = row.CreateCell(0);
                cell.SetCellValue("ĐẠI DIỆN (" + dlChiNhanh.SelectedItem.Text.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
                sheet.AddMergedRegion(cra);

                cell = row.CreateCell(6);
                cell.SetCellValue("ĐẠI DIỆN (" + tencongty.ToUpper() + ")");
                cell.CellStyle = styleBodyFooterCenterNoborder;

                cra = new CellRangeAddress(rowIndex, rowIndex, 6, 13);
                sheet.AddMergedRegion(cra);

                #endregion

                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);

                    #region [Set title dưới]

                    string saveAsFileName = "";
                    string Bienso = "";

                    if (txtTuNgay.Text.Trim() == txtDenNgay.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO VLTH GẠCH TERRAZO -" + Bienso + "-Ngày: " + "-" + txtTuNgay.Text +
                                         ".xls";
                    }
                    else if (txtTuNgay.Text.Trim() != txtDenNgay.Text.Trim())
                    {
                        saveAsFileName = "BÁO CÁO VLTH GẠCH TERRAZO -" + Bienso + "-Từ ngày" + "-" + txtTuNgay.Text +
                                         "-Đến ngày" + "-" + txtDenNgay.Text + ".xls";
                    }

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));

                    #endregion

                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            else
                GstGetMess("Không có dữ liệu nào để in", "");
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            hdPage.Value = "1";
            Search(CurrentPage);
            btnPre.Enabled = false;
            btnNext.Enabled = true;
            btnFirst.Enabled = false;
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            if (CurrentPage > 1)
            {
                CurrentPage--;
                btnPre.Enabled = true;
                btnNext.Enabled = true;
                btnFirst.Enabled = true;
                Search(CurrentPage);
            }
            else
            {
                btnFirst.Enabled = false;
                btnPre.Enabled = false;
                btnNext.Enabled = true;
            }
            hdPage.Value = Convert.ToString(CurrentPage);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage = int.Parse(hdPage.Value);
            CurrentPage++;
            Search(CurrentPage);
            if (GV.Rows.Count.Equals(0))
            {
                btnPre.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPre.Enabled = true;
                btnNext.Enabled = true;
            }
            btnFirst.Enabled = true;
            hdPage.Value = Convert.ToString(CurrentPage);
        }

        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        protected void grdDanhSachHD_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#EEFFAA'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }
    }
}