﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThuongLong
{
    public partial class QuanLyThietBi : System.Web.UI.Page
    {
        DBDataContext db = new DBDataContext();
        ThietBiDataContext tb = new ThietBiDataContext();
        clsPhanQuyen pq = new clsPhanQuyen();
        string TagName = "frmQuanLyThietBi";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["IDND"])))
                Response.Redirect("login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    string url = "";
                    string thongbao = pq.CheckQuyenTruyCap(Session["IDND"].ToString(), "frmQuanLyThietBi", ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        LoadChiNhanh();
                        LoadChiNhanhSearch();
                        LoadNhomThietBi();
                        LoadLoaiThietBi();
                        LoadDonViTinh();
                        hdPage.Value = "1";
                        DateTime dateTimenow = DateTime.Now;
                        var firstDayOfMonth = new DateTime(dateTimenow.Year, dateTimenow.Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        txtTuNgaySearch.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                        txtDenNgaySearch.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        protected void LoadChiNhanh()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanh.DataSource = query;
            dlChiNhanh.DataBind();
            dlChiNhanh.Items.Insert(0, new ListItem("--Chọn chi nhánh--", ""));
        }
        protected void LoadChiNhanhSearch()
        {
            var query = db.sp_LoadChiNhanhDaiLy();
            dlChiNhanhSearch.DataSource = query;
            dlChiNhanhSearch.DataBind();
        }
        protected void LoadNhomThietBi()
        {
            var query = db.sp_LoadNhomThietBi();
            dlNhomThietBi.DataSource = query;
            dlNhomThietBi.DataBind();
            dlNhomThietBi.Items.Insert(0, new ListItem("--Chọn nhóm tài sản--", ""));
        }
        protected void LoadLoaiThietBi()
        {
            dlLoaiThietBi.Items.Clear();
            var query = db.sp_LoadLoaiThietBiByNhom(GetDrop(dlNhomThietBi));
            dlLoaiThietBi.DataSource = query;
            dlLoaiThietBi.DataBind();
            dlLoaiThietBi.Items.Insert(0, new ListItem("--Chọn loại tài sản--", ""));
        }
        protected void LoadDonViTinh()
        {
            var query = db.sp_LoadDonViTinh();
            dlDonViTinh.DataSource = query;
            dlDonViTinh.DataBind();
            dlDonViTinh.Items.Insert(0, new ListItem("--Chọn đơn vị tính--", ""));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        string CheckThem()
        {
            string s = "";

            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhomThietBi.SelectedValue == "")
            {
                s += " - Chọn nhóm tài sản<br />";
            }
            if (dlLoaiThietBi.SelectedValue == "")
            {
                s += " - Chọn loại tài sản<br />";
            }
            if (txtThoiGiaKhauHao.Text == "")
            {
                s += " - Nhập thời gian khấu hao<br />";
            }
            if (txtDonGiaCoThue.Text == "" || txtDonGiaKhongThue.Text == "" || (decimal.Parse(txtDonGiaCoThue.Text) == 0 && decimal.Parse(txtDonGiaKhongThue.Text) == 0))
            {
                s += " - Nhập giá mua tài sản<br />";
            }
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            if (s == "")
            {
                //var query = tb.sp_QuanLyThietBi_CheckThem(DateTime.Parse(GetNgayThang(txtNgayThang.Text)), txtTenThietBi.Text.Trim().ToUpper(), GetDrop(dlChiNhanh),
                //    GetDrop(dlNhomThietBi), GetDrop(dlLoaiThietBi), GetDrop(dlDonViTinh), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        string CheckSua()
        {
            string s = "";
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            if (dlChiNhanh.SelectedValue == "")
            {
                s += " - Chọn chi nhánh<br />";
            }
            if (dlNhomThietBi.SelectedValue == "")
            {
                s += " - Chọn nhóm tài sản<br />";
            }
            if (dlLoaiThietBi.SelectedValue == "")
            {
                s += " - Chọn loại tài sản<br />";
            }
            if (dlDonViTinh.SelectedValue == "")
            {
                s += " - Chọn đơn vị tính<br />";
            }
            if (txtThoiGiaKhauHao.Text == "")
            {
                s += " - Nhập thời gian khấu hao<br />";
            }
            if (txtDonGiaCoThue.Text == "" || txtDonGiaKhongThue.Text == "" || (decimal.Parse(txtDonGiaCoThue.Text) == 0 && decimal.Parse(txtDonGiaKhongThue.Text) == 0))
            {
                s += " - Nhập giá mua tài sản<br />";
            }
            if (txtTuNgay.Text == "")
            {
                s += " - Nhập ngày bắt đầu<br />";
            }
            if (s == "")
            {
                //var query = tb.sp_QuanLyThietBi_CheckSua(new Guid(hdID.Value), DateTime.Parse(GetNgayThang(txtNgayThang.Text)), txtTenThietBi.Text.Trim().ToUpper(), GetDrop(dlChiNhanh),
                //    GetDrop(dlNhomThietBi), GetDrop(dlLoaiThietBi), GetDrop(dlDonViTinh), ref s);
            }

            if (s != "")
                Warning(s);
            return s;
        }
        protected void lbtLuuHopDong_Click(object sender, EventArgs e)
        {
            string url = "";
            if (hdID.Value == "")
            {
                if (CheckThem() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 1, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var QuanLyThietBi = new tblQuanLyThietBi()
                        {
                            ID = Guid.NewGuid(),
                            //LoaiNguon =   ,
                            //IDNguon =   ,
                            TenThietBi = txtTenThietBi.Text.Trim().ToUpper(),
                            IDChiNhanh = GetDrop(dlChiNhanh),
                            IDNhomThietBi = GetDrop(dlNhomThietBi),
                            IDLoaiThietBi = GetDrop(dlLoaiThietBi),
                            IDDonViTinh = GetDrop(dlDonViTinh),
                            DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text),
                            DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text),
                            ThoiGianKhauHao = int.Parse(txtThoiGiaKhauHao.Text),
                            GiaTriKhauHao = decimal.Parse(txtGiaTriKhauHao.Text),
                            NhienLieuDoDayBinh = txtNhienLieuDoDayBinh.Text == "" ? 0 : double.Parse(txtNhienLieuDoDayBinh.Text),
                            ChieuCaoBinhNhienLieu = txtChieuCaoBinhNhienLieu.Text == "" ? 0 : double.Parse(txtChieuCaoBinhNhienLieu.Text),
                            LitCm = txtLitCM.Text == "" ? 0 : double.Parse(txtLitCM.Text),
                            DinhMucNhienLieu = txtDinhMucNhienLieu.Text == "" ? 0 : double.Parse(txtDinhMucNhienLieu.Text),
                            TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text)),
                            DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text)),
                            TrangThai = 1,
                            TrangThaiText = "Chờ duyệt",
                            STT = 0,
                            NguoiTao = Session["IDND"].ToString(),
                            NgayTao = DateTime.Now
                        };
                        db.tblQuanLyThietBis.InsertOnSubmit(QuanLyThietBi);
                        db.SubmitChanges();
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                        Success("Lưu thành công.");
                    }
                }
            }
            else
            {
                if (CheckSua() == "")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        var query = (from p in db.tblQuanLyThietBis
                                     where p.ID == new Guid(hdID.Value)
                                     select p).FirstOrDefault();
                        if (query != null && query.ID != null)
                        {
                            //LoaiNguon =   ,
                            //IDNguon =   ,
                            query.TenThietBi = txtTenThietBi.Text.Trim().ToUpper();
                            query.IDChiNhanh = GetDrop(dlChiNhanh);
                            query.IDNhomThietBi = GetDrop(dlNhomThietBi);
                            query.IDLoaiThietBi = GetDrop(dlLoaiThietBi);
                            query.IDDonViTinh = GetDrop(dlDonViTinh);
                            query.DonGiaCoThue = decimal.Parse(txtDonGiaCoThue.Text);
                            query.DonGiaKhongThue = decimal.Parse(txtDonGiaKhongThue.Text);
                            query.ThoiGianKhauHao = int.Parse(txtThoiGiaKhauHao.Text);
                            query.GiaTriKhauHao = decimal.Parse(txtGiaTriKhauHao.Text);
                            query.NhienLieuDoDayBinh = txtNhienLieuDoDayBinh.Text == "" ? 0 : double.Parse(txtNhienLieuDoDayBinh.Text);
                            query.ChieuCaoBinhNhienLieu = txtChieuCaoBinhNhienLieu.Text == "" ? 0 : double.Parse(txtChieuCaoBinhNhienLieu.Text);
                            query.LitCm = txtLitCM.Text == "" ? 0 : double.Parse(txtLitCM.Text);
                            query.DinhMucNhienLieu = txtDinhMucNhienLieu.Text == "" ? 0 : double.Parse(txtDinhMucNhienLieu.Text);
                            query.TuNgay = DateTime.Parse(GetNgayThang(txtTuNgay.Text));
                            query.DenNgay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(GetNgayThang(txtDenNgay.Text));
                            query.TrangThai = 1;
                            query.TrangThaiText = "Chờ duyệt";
                            query.STT = 0;
                            query.NguoiTao = Session["IDND"].ToString();
                            query.NgayTao = DateTime.Now;
                            db.SubmitChanges();
                            lblTaoMoiHopDong_Click(sender, e);
                            Search(1);
                            Success("Sửa thành công");
                        }
                        else
                        {
                            Warning("Thông tin tài sản đã bị xóa.");
                            lblTaoMoiHopDong_Click(sender, e);
                        }
                    }
                }
            }
        }
        protected void lblTaoMoiHopDong_Click(object sender, EventArgs e)
        {
            hdID.Value = "";
            dlChiNhanh.SelectedIndex = 0;
            dlNhomThietBi.SelectedIndex = 0;
            LoadLoaiThietBi();
            dlDonViTinh.SelectedIndex = 0;
            txtDonGiaCoThue.Text = "";
            txtDonGiaKhongThue.Text = "";
            txtThoiGiaKhauHao.Text = "";
            txtGiaTriKhauHao.Text = "";
            txtNhienLieuDoDayBinh.Text = "";
            txtChieuCaoBinhNhienLieu.Text = "";
            txtLitCM.Text = "";
            txtDinhMucNhienLieu.Text = "";
        }
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string url = "";
            string id = e.CommandArgument.ToString();
            var query = (from p in db.tblQuanLyThietBis
                         where p.ID == new Guid(id)
                         select p).FirstOrDefault();

            if (query != null && query.ID != null)
            {
                if (e.CommandName == "Sua")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 2, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        txtTenThietBi.Text = query.TenThietBi;
                        dlChiNhanh.SelectedValue = query.IDChiNhanh.ToString();
                        dlNhomThietBi.SelectedValue = query.IDNhomThietBi.ToString();
                        LoadLoaiThietBi();
                        dlLoaiThietBi.SelectedValue = query.IDLoaiThietBi.ToString();
                        dlDonViTinh.SelectedValue = query.IDDonViTinh.ToString();
                        txtDonGiaCoThue.Text = string.Format("{0:N0}", query.DonGiaCoThue);
                        txtDonGiaKhongThue.Text = string.Format("{0:N0}", query.DonGiaKhongThue);
                        txtThoiGiaKhauHao.Text = string.Format("{0:N0}", query.ThoiGianKhauHao);
                        txtGiaTriKhauHao.Text = string.Format("{0:N0}", query.GiaTriKhauHao);
                        txtNhienLieuDoDayBinh.Text = query.NhienLieuDoDayBinh.ToString();
                        txtChieuCaoBinhNhienLieu.Text = query.ChieuCaoBinhNhienLieu.ToString();
                        txtLitCM.Text = query.LitCm.ToString();
                        txtDinhMucNhienLieu.Text = query.DinhMucNhienLieu.ToString();
                        txtTuNgay.Text = query.TuNgay.ToString("dd/MM/yyy");
                        txtDenNgay.Text = query.DenNgay == null ? "" : query.DenNgay.Value.ToString("dd/MM/yyy");
                        hdID.Value = id;
                    }
                }
                else if (e.CommandName == "Xoa")
                {
                    string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 3, TagName, ref url);
                    if (thongbao != "")
                    {
                        if (url != "")
                        {
                            GstGetMess(thongbao, url);
                        }
                        else
                        {
                            Warning(thongbao);
                        }
                    }
                    else
                    {
                        if (query.TrangThai != 3)
                        {
                            query.TrangThai = 3;
                            query.TrangThaiText = "Chờ duyệt xóa";
                            db.SubmitChanges();
                        }
                        Success("Xóa thành công");
                        lblTaoMoiHopDong_Click(sender, e);
                        Search(1);
                    }
                }
                else if (e.CommandName == "Xem")
                {
                    var view = tb.sp_QuanLyThietBi_LichSu(new Guid(id));
                    gvLichSu.DataSource = view;
                    gvLichSu.DataBind();
                    mpLichSu.Show();
                }
            }
            else
            {
                Warning("Thông tin tài sản đã bị xóa.");
                lblTaoMoiHopDong_Click(sender, e);
            }
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                hdPage.Value = "1";
                Search(1);
                btnPre.Enabled = false;
                btnNext.Enabled = true;
                btnFirst.Enabled = false;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                CurrentPage++;
                Search(CurrentPage);
                if (GV.Rows.Count.Equals(0))
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = false;
                }
                else
                {
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                }
                btnFirst.Enabled = true;
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            string url = "";
            string thongbao = pq.CheckQuyenThaoTacDanhMuc(Session["IDND"].ToString(), 4, TagName, ref url);
            if (thongbao != "")
            {
                if (url != "")
                {
                    GstGetMess(thongbao, url);
                }
                else
                {
                    Warning(thongbao);
                }
            }
            else
            {
                int CurrentPage = int.Parse(hdPage.Value);
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    btnPre.Enabled = true;
                    btnNext.Enabled = true;
                    btnFirst.Enabled = true;
                    Search(CurrentPage);
                }
                else
                {
                    btnFirst.Enabled = false;
                    btnPre.Enabled = false;
                    btnNext.Enabled = true;
                }
                hdPage.Value = Convert.ToString(CurrentPage);
            }
        }
        private string GetNgayThang(string ns)
        {
            try
            {
                if (!ns.Trim().Equals(""))
                {
                    string[] tmp = ns.Split('/');
                    string kq = tmp[1] + "/" + tmp[0] + "/" + tmp[2];
                    if (int.Parse(tmp[2]) < 1900)
                    {
                        return "";
                    }
                    else
                    {
                        return kq;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            { return ""; }
        }
        private void GstGetMess(string gstMess, string gstLink)
        {
            if (gstLink == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), " ", "alert('" + gstMess + "');window.location.href='" + gstLink + "'", true);
            }
        }
        private void Success(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "success('" + mess + "');", true);
        }
        private void Warning(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "warning('" + mess + "');", true);
        }
        private void Inverted(string mess)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "inverted('" + mess + "');", true);
        }
        private Guid GetDrop(DropDownList dl)
        {
            Guid gId = dl.SelectedValue == "" ? new Guid() : new Guid(dl.SelectedValue);
            return gId;
        }
        protected void dlNhomThietBi_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLoaiThietBi();
        }
        protected void GridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "FMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "FMouseEvents(this, event)");
            }
        }
        protected void gvLichSu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                // từ vị trí thứ i + 1 đến hết 
                for (int i = 2; i <= gvLichSu.Columns.Count - 1; i++)
                {
                    if (index > 0)
                    {
                        if (gvLichSu.Rows[index - 1].Cells[i].Text != e.Row.Cells[i].Text)
                        {
                            gvLichSu.Rows[index - 1].Cells[i].BackColor = Color.LightCyan;
                            e.Row.Cells[i].BackColor = Color.LightCyan;
                        }
                    }
                }
            }
        }
        private void LoadDl1()
        {
            List<sp_QuanLyThietBi_LoadDl1Result> query = tb.sp_QuanLyThietBi_LoadDl1(GetDrop(dlChiNhanhSearch),
             dlXemTheo.SelectedValue, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            if (query != null)
            {
                dl1.Items.Clear();
                dl1.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL1,
                                      Ten = p.TenDL1
                                  }).Distinct().OrderBy(p => p.Ten);
                dl1.DataBind();
                dl1.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl2.Items.Clear();
                dl2.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL2,
                                      Ten = p.TenDL2
                                  }).Distinct().OrderBy(p => p.Ten);
                dl2.DataBind();
                dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        private void LoadDl2()
        {
            List<sp_QuanLyThietBi_LoadDl2Result> query = tb.sp_QuanLyThietBi_LoadDl2(GetDrop(dlChiNhanhSearch), dl1.SelectedValue,
             dlXemTheo.SelectedValue, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            if (query != null)
            {
                dl2.Items.Clear();
                dl2.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL2,
                                      Ten = p.TenDL2
                                  }).Distinct().OrderBy(p => p.Ten);
                dl2.DataBind();
                dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }
        private void LoadDl3()
        {
            List<sp_QuanLyThietBi_LoadDl3Result> query = tb.sp_QuanLyThietBi_LoadDl3(GetDrop(dlChiNhanhSearch), dl1.SelectedValue, dl2.SelectedValue,
             dlXemTheo.SelectedValue, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text))).ToList();

            if (query != null)
            {
                dl3.Items.Clear();
                dl3.DataSource = (from p in query
                                  select new
                                  {
                                      ID = p.DL3,
                                      Ten = p.TenDL3
                                  }).Distinct().OrderBy(p => p.Ten);
                dl3.DataBind();
                dl3.Items.Insert(0, new ListItem("--Chọn--", ""));
            }
        }

        protected void dl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dl2.Items.Clear();
            dl2.Items.Insert(0, new ListItem("--Chọn--", ""));

            dl3.Items.Clear();
            dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

            if (dl1.SelectedValue != "")
                LoadDl2();
        }
        protected void dl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dl3.Items.Clear();
            dl3.Items.Insert(0, new ListItem("--Chọn--", ""));

            if (dl2.SelectedValue != "")
                LoadDl3();
        }
        protected void dlXemTheo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlXemTheo.SelectedValue == "0")
            {
                divdl.Visible = false;
            }
            else if (dlXemTheo.SelectedValue == "1")
            {
                lbl1.Text = "Nhóm tài sản";
                lbl2.Text = "Loại tài sản";
                lbl3.Text = "Đơn vị tính";

                divdl.Visible = true;
                LoadDl1();
            }
            else if (dlXemTheo.SelectedValue == "2")
            {
                lbl1.Text = "Loại tài sản";
                lbl2.Text = "Đơn vị tính";
                lbl3.Text = "Nhóm tài sản";

                divdl.Visible = true;
                LoadDl1();
            }
            else if (dlXemTheo.SelectedValue == "3")
            {
                lbl1.Text = "Đơn vị tính";
                lbl2.Text = "Nhóm tài sản";
                lbl3.Text = "Loại tài sản";

                divdl.Visible = true;
                LoadDl1();
            }
        }
        private void Search(int page)
        {
            string index = "";
            if (dlXemTheo.SelectedValue == "0" || dl1.SelectedValue == "")
                index = "0";
            else if (dl3.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "3";
            }
            else if (dl2.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "2";
            }
            else if (dl1.SelectedValue != "")
            {
                index = dlXemTheo.SelectedValue + "1";
            }
            var query = tb.sp_QuanLyThietBi_Search(GetDrop(dlChiNhanhSearch), dl1.SelectedValue, dl2.SelectedValue, dl3.SelectedValue,
            index, DateTime.Parse(pq.GetNgayThang(txtTuNgaySearch.Text)), DateTime.Parse(pq.GetNgayThang(txtDenNgaySearch.Text)), 10, page);
            if (query != null)
            {
                GV.DataSource = query;
                GV.DataBind();
            }
        }
    }
}