﻿<%@ Page Title="Duyệt công nợ" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DuyetCongNo.aspx.cs" Inherits="ThuongLong.DuyetCongNo" %>

<%@ Register Src="UCCongNo/uc_DuyetCongNoTra.ascx" TagName="uc_DuyetCongNoTra" TagPrefix="uc1" %>
<%@ Register Src="UCCongNo/uc_DuyetCongNoThu.ascx" TagName="uc_DuyetCongNoThu" TagPrefix="uc2" %>
<%@ Register Src="UCCongNo/uc_DuyetQuanLyHoaDon.ascx" TagName="uc_DuyetQuanLyHoaDon" TagPrefix="uc3" %>
<%@ Register Src="UCCongNo/uc_DuyetCongNo.ascx" TagName="uc_DuyetCongNo" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padding {
            margin-left: 5px;
        }

        .bang tr td {
            border: 1px solid #ccc;
            line-height: 30px;
        }

            .bang tr td b {
                margin-left: 5px;
            }

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('resource/css/fonts/glyphicons-halflings-regular.eot');
            src: url('resource/css/fonts//glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('resource/css/fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('resource/css/fonts/glyphicons-halflings-regular.woff') format('woff'), url('resource/css/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('resource/css/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        #process {
            position: fixed;
            left: 40%;
            top: 50%;
            border: 1px solid gray;
            background-color: Lavender;
            padding: 10px;
            color: Purple;
            margin: 5px;
            font-weight: bold;
        }

        .ajax__tab_default .ajax__tab_header {
            white-space: normal !important;
        }

        .ajax__tab_default .ajax__tab_outer {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_inner {
            display: -moz-inline-box;
            display: inline-block;
        }

        .ajax__tab_default .ajax__tab_tab {
            margin-right: 4px;
            overflow: hidden;
            text-align: center;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12" style="display: initial;">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="NewsTab" CssTheme="None"
                        ActiveTabIndex="0">
                        
                        <ajaxToolkit:TabPanel ID="frmCongNo" runat="server" HeaderText="Quản lý công nợ">
                            <ContentTemplate>
                                <uc4:uc_DuyetCongNo ID="uc_DuyetCongNo1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxToolkit:TabPanel ID="frmCongNoTra" runat="server" HeaderText="Công nợ trả">
                            <ContentTemplate>
                                <uc1:uc_DuyetCongNoTra ID="uc_DuyetCongNoTra1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="frmCongNoThu" runat="server" HeaderText="Công nợ thu">
                            <ContentTemplate>
                                <uc2:uc_DuyetCongNoThu ID="uc_DuyetCongNoThu1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>--%>
                        <ajaxToolkit:TabPanel ID="frmQuanLyHoaDon" runat="server" HeaderText="Quản lý hóa đơn">
                            <ContentTemplate>
                                <uc3:uc_DuyetQuanLyHoaDon ID="uc_DuyetQuanLyHoaDon1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
