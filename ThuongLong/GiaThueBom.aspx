﻿<%@ Page Title="Giá thuê bơm" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GiaThueBom.aspx.cs" Inherits="ThuongLong.GiaThueBom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .select2-container {
            z-index: 100000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin giá</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <h5>Chi nhánh</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Chi nhánh"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlChiNhanh"
                                runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Nhà cung cấp</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp" AutoPostBack="true"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNhaCungCap" OnSelectedIndexChanged="dlNhaCungCap_SelectedIndexChanged" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-6">
                            <h5>Công trình</h5>
                            <div class="input-group margin">
                                <asp:DropDownList CssClass="form-control select2" data-toggle="tooltip" data-original-title="Công trình" Width="98%"
                                    Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlCongTrinh"
                                    runat="server">
                                </asp:DropDownList>
                                <span class="input-group-btn">
                                    <asp:LinkButton ID="btnTaoMoiCongTrinh" runat="server" class="btn btn-info btn-flat" OnClick="btnTaoMoiCongTrinh_Click"><i class="fa fa-plus"></i></asp:LinkButton>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Hình thức bơm</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Hình thức bơm"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlHinhThucBom" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Giá có thuế</h5>
                            <asp:TextBox ID="txtDonGiaHoaDon" runat="server" class="form-control" placeholder="Giá có thuế" onchange="CoThue()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDonGiaHoaDon" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Giá không thuế</h5>
                            <asp:TextBox ID="txtDonGiaThanhToan" runat="server" class="form-control" placeholder="Giá không thuế" onchange="KhongThue()" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDonGiaThanhToan" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Từ ngày</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Đến ngày</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgay" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu hợp đồng</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách giá</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group col-lg-3">
                        <h5>Từ ngày</h5>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtTuNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="100%"
                                OnTextChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <h5>Đến ngày</h5>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtDenNgaySearch" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" Width="100%"
                                OnTextChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <h5>Chi nhánh</h5>
                        <asp:DropDownList CssClass="form-control select2" Width="100%" data-toggle="tooltip" data-original-title="Chi nhánh" Font-Size="13px"
                            DataTextField="Ten" DataValueField="ID" ID="dlChiNhanhSearch" OnSelectedIndexChanged="dlChiNhanhSearch_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3">
                        <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        <asp:LinkButton ID="btnResetFilter" runat="server" class="btn btn-app bg-warning" OnClick="btnResetFilter_Click"><i class="fa fa-filter"></i>Lọc</asp:LinkButton>
                    </div>
                </div>
                <div class="box-body" id="divdl" visible="false" runat="server">

                    <div class="form-group col-lg-3">
                        <h5>Nhà cung cấp</h5>
                        <asp:ListBox ID="dlNhaCungCapSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                            Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <h5>Công trình</h5>
                        <asp:ListBox ID="dlCongTrinhSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                            Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                    </div>
                    <div class="form-group col-lg-3">
                        <h5>Hình thức bơm</h5>
                        <asp:ListBox ID="dlHinhThucBomSearch" runat="server" SelectionMode="Multiple" DataValueField="ID" DataTextField="Ten"
                            Font-Size="12px" Width="100%" CssClass="select2"></asp:ListBox>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                    <asp:BoundField DataField="TenNhaCungCap" HeaderText="Nhà cung cấp" />
                                    <asp:BoundField DataField="TenCongTrinh" HeaderText="Công trình" />
                                    <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" />
                                    <asp:BoundField DataField="DonGiaHoaDon" HeaderText="Đơn giá có thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DonGiaThanhToan" HeaderText="Đơn giá không thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>
            <script type="text/javascript">
                function getvalue(vat) {
                    if (vat == '') vat = "0";
                    vat = vat.replace(',', '').replace(',', '').replace(',', '');
                    var t1 = parseFloat(vat);
                    return t1;
                }
                function CoThue() {
                    var txtDonGiaHoaDon = parseFloat(getvalue(document.getElementById("<%=txtDonGiaHoaDon.ClientID %>").value));
                    var txtDonGiaThanhToan = parseFloat(getvalue(document.getElementById("<%=txtDonGiaThanhToan.ClientID %>").value));
                    if (txtDonGiaHoaDon != '0') {
                        document.getElementById("<%=txtDonGiaHoaDon.ClientID %>").value = (txtDonGiaHoaDon).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtDonGiaThanhToan.ClientID %>").value = (txtDonGiaThanhToan).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtDonGiaHoaDon.ClientID %>").value = '0';
                    }
                }
                function KhongThue() {
                    var txtDonGiaHoaDon = parseFloat(getvalue(document.getElementById("<%=txtDonGiaHoaDon.ClientID %>").value));
                    var txtDonGiaThanhToan = parseFloat(getvalue(document.getElementById("<%=txtDonGiaThanhToan.ClientID %>").value));
                    if (txtDonGiaThanhToan != '0') {
                        document.getElementById("<%=txtDonGiaThanhToan.ClientID %>").value = (txtDonGiaThanhToan).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                        document.getElementById("<%=txtDonGiaHoaDon.ClientID %>").value = (txtDonGiaHoaDon).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$&,");
                    }
                    else {
                        document.getElementById("<%=txtDonGiaThanhToan.ClientID %>").value = '0';
                    }
                }
            </script>
            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdIDChiTiet" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdCongTrinh" runat="server" Value="" />


            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="TenChiNhanh" HeaderText="Chi nhánh" />
                                <asp:BoundField DataField="TenNhaCungCap" HeaderText="Nhà cung cấp" />
                                <asp:BoundField DataField="TenCongTrinh" HeaderText="Công trình" />
                                <asp:BoundField DataField="TenHinhThucBom" HeaderText="Hình thức bơm" />
                                <asp:BoundField DataField="DonGiaHoaDon" HeaderText="Đơn giá có thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DonGiaThanhToan" HeaderText="Đơn giá không thuế" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" DataFormatString="{0:dd/MM/yyyy}" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpCongTrinh" runat="server" CancelControlID="btnDongCongTrinh"
                Drag="True" TargetControlID="hdCongTrinh" BackgroundCssClass="modalBackground" PopupControlID="pnCongTrinh"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnCongTrinh" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Thêm công trình
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">

                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Nhà cung cấp</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Nhà cung cấp"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlNCC" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="exampleInputEmail1">Tên công trình</label>
                            <asp:TextBox ID="txtTenCongTrinh" runat="server" class="form-control" Width="98%"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>KM vận chuyển</h5>
                            <asp:TextBox ID="txtKMVanChuyen" runat="server" class="form-control" placeholder="Số km vận chuyển" Width="98%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtKMVanChuyen" ValidChars=".," />
                        </div>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnLuuCongTrinh" runat="server" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuuCongTrinh_Click" />
                        <asp:Button ID="btnDongCongTrinh" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
