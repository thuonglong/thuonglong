﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_DuyetNhanSu.ascx.cs" Inherits="ThuongLong.DuyetNhanSu.uc_DuyetNhanSu" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="panel-body">
            <div class="form-group">
                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-warning" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
            </div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                EmptyDataText="Không có dữ liệu nào để duyệt" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                <Columns>
                    <asp:TemplateField HeaderText="Duyệt" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Duyệt"
                                CommandName="Sua" OnClientClick="return confirm('Bạn có muốn duyệt không?')"><i class="fa fa-check"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Không duyệt" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtHuy" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Duyệt"
                                CommandName="Huy"><i class="fa fa-times" style="color:#ff0000;"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trạng thái">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblLichSu"
                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TenNhanVien" HeaderText="Tên nhân viên" />
                    <asp:BoundField DataField="MaSoThueCaNhan" HeaderText="Mã số thuế cá nhân" />
                    <asp:BoundField DataField="NgaySinh" HeaderText="Ngày sinh" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="GioiTinh" HeaderText="Giới tính" />
                    <asp:BoundField DataField="TinhTrangHonNhan" HeaderText="Tình trạng hôn nhân" />
                    <asp:BoundField DataField="DiaChi" HeaderText="Địa chỉ" />
                    <asp:BoundField DataField="CMND" HeaderText="CMND" />
                    <asp:BoundField DataField="Email" HeaderText="Email" />
                    <asp:BoundField DataField="DienThoai" HeaderText="Điện thoại" />
                    <asp:BoundField DataField="TenTrinhDo" HeaderText="Trình độ" />
                    <asp:BoundField DataField="TenChuyenMon" HeaderText="Chuyên môn" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                    HorizontalAlign="Right" />
                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
            </asp:GridView>
        </div>
        <div style="margin: 5px;" class="btn-group">
            <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
            <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
            <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
        </div>

        <ajaxToolkit:ModalPopupExtender ID="mpDuyet" runat="server" CancelControlID="btnDong"
            Drag="True" TargetControlID="hdDuyet" BackgroundCssClass="modalBackground" PopupControlID="pnDuyet"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnDuyet" runat="server" Style="display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    DUYỆT
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12">
                        <label for="exampleInputEmail1">Lý do không duyệt</label>
                        <asp:TextBox ID="txtMota" runat="server" class="form-control" placeholder="Lý do không duyệt" Width="98%"></asp:TextBox>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnKhongDuyet" runat="server" CssClass="btn btn-warning" OnClick="btnKDuyetTB_Click"
                        Text="Không duyệt" />
                    <asp:Button ID="btnDong" runat="server" CssClass="btn btn-danger"
                        Text="Đóng" />
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hdID" runat="server" />
        <asp:HiddenField ID="hdPage" runat="server" />
        <asp:HiddenField ID="hdDuyet" runat="server" />

        <asp:HiddenField ID="hdLichSu" runat="server" Value="" />

        <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
            Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
            RepositionMode="RepositionOnWindowResize">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
            <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center">
                    Lịch sử
                </div>
                <div class="panel-body" style="max-height: 500px; overflow: auto;">
                    <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                        EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                        <Columns>
                            <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                            <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                            <asp:BoundField DataField="TenNhanSu" HeaderText="Tên chuyên môn" />
                            <asp:BoundField DataField="STT" HeaderText="Sắp xếp" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                            HorizontalAlign="Right" />
                        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                        <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                    </asp:GridView>
                </div>
                <div class="panel-footer" style="align-content: center; text-align: center">
                    <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                </div>
            </div>
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <%--<asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="btnGiaTri" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btncapnhat" />
            <asp:PostBackTrigger ControlID="btnKhoiLuong" />--%>
    </Triggers>
</asp:UpdatePanel>


