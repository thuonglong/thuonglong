﻿<%@ Page Title="Định mức xe bê tông" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DinhMucXeBeTong.aspx.cs" Inherits="ThuongLong.DinhMucXeBeTong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Biển số xe</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlXe" runat="server">
                            </asp:DropDownList>
                        </div>


                        <%--Xe bê tông--%>

                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Di chuyển có tải</label>
                            <asp:TextBox ID="txtDiChuyenCoTaiBeTong" runat="server" class="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDiChuyenCoTaiBeTong" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Di chuyển không tải</label>
                            <asp:TextBox ID="txtDiChuyenKhongTaiBeTong" runat="server" class="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDiChuyenKhongTaiBeTong" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Lấy bê tông</label>
                            <asp:TextBox ID="txtLayBeTongTaiTram" runat="server" class="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtLayBeTongTaiTram" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Vệ sinh, xả bê tông</label>
                            <asp:TextBox ID="txtXaBeTongVeSinh" runat="server" class="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtXaBeTongVeSinh" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Nổ máy quay thùng</label>
                            <asp:TextBox ID="txtNoMayQuayThung" runat="server" class="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtNoMayQuayThung" ValidChars=".," />
                        </div>
                        <div class="form-group col-lg-3">
                            <h5>Loại nhiên liệu</h5>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Loại nhiên liệu"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlLoaiVatLieu" runat="server" OnSelectedIndexChanged="dlLoaiVatLieu_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-12">
                        <asp:LinkButton ID="lbtLuuHopDong" runat="server" class="btn btn-app bg-green" OnClick="lbtLuuHopDong_Click"><i class="fa fa-save"></i>Lưu</asp:LinkButton>
                        <asp:LinkButton ID="lblTaoMoiHopDong" runat="server" class="btn btn-app bg-warning" OnClick="lblTaoMoiHopDong_Click"><i class="fa fa-plus"></i>Tạo mới</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách xe</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group col-lg-3">
                            <label for="exampleInputEmail1">Biển số xe</label>
                            <asp:DropDownList CssClass="form-control select2" Width="98%" data-toggle="tooltip" data-original-title="Biển số xe"
                                Font-Size="13px" DataTextField="Ten" DataValueField="ID" ID="dlThietBiSearch" runat="server">
                            </asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3">
                            <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-app bg-green" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Tìm kiếm</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow: auto; width: 100%;">
                            <asp:GridView ID="GV" runat="server" AutoGenerateColumns="false" OnRowCommand="GV_RowCommand"
                                EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sửa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Sửa" CommandName="Sua">
                                                <i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Xóa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtXoa" runat="server" class="btn btn-small bg-warning" CommandArgument='<%#Eval("ID")%>' ToolTip="Xóa" CommandName="Xoa" OnClientClick="return confirm('Bạn chắc chắn muốn xóa ?')">
                                                <i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trạng thái">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLichSu"
                                                Text='<%#Eval("TrangThaiText")%>' runat="server" CssClass="padding" CommandName="Xem"
                                                CommandArgument='<%#Eval("ID")%>' ForeColor="Purple" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TenThietBi" HeaderText="Biển số xe" />

                                    <%--<asp:BoundField DataField="DiChuyenCoTaiBen" HeaderText="Di chuyển có tải" />
                                    <asp:BoundField DataField="DiChuyenKhongTaiBen" HeaderText="Di chuyển không tải" />
                                    <asp:BoundField DataField="CauHaHang" HeaderText="Cẩu hạ hàng" />--%>

                                    <asp:BoundField DataField="DiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải" />
                                    <asp:BoundField DataField="DiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải" />
                                    <asp:BoundField DataField="LayBeTongTaiTram" HeaderText="Lấy bê tông tại trạm" />
                                    <asp:BoundField DataField="XaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" />
                                    <asp:BoundField DataField="NoMayQuayThung" HeaderText="Nổ máy, quay thùng" />

                                    <%--<asp:BoundField DataField="DiChuyen" HeaderText="Di chuyển" />
                                    <asp:BoundField DataField="BomBeTong" HeaderText="Bơm bê tông" />
                                    <asp:BoundField DataField="VeSinhRaChan" HeaderText="Vệ sinh, ra chân" />--%>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                    HorizontalAlign="Right" />
                                <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                                <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <asp:Button ID="btnFirst" runat="server" Text="Đầu" CssClass="btn btn-warning" OnClick="btnFirst_Click" />
                    <asp:Button ID="btnPre" runat="server" Text="Trước" CssClass="btn btn-primary" OnClick="btnPre_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Sau" CssClass="btn btn-warning" OnClick="btnNext_Click" />
                </div>
            </div>

            <asp:HiddenField ID="hdPage" runat="server" Value="1" />
            <asp:HiddenField ID="hdID" runat="server" Value="" />
            <asp:HiddenField ID="hdLichSu" runat="server" Value="" />
            <asp:HiddenField ID="hdNhomThietBi" runat="server" Value="3EB17A86-BCA4-4852-B1F4-01B1CABE106D" />

            <ajaxToolkit:ModalPopupExtender ID="mpLichSu" runat="server" CancelControlID="btnDongLichSu"
                Drag="True" TargetControlID="hdLichSu" BackgroundCssClass="modalBackground" PopupControlID="pnLichSu"
                RepositionMode="RepositionOnWindowResize">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnLichSu" runat="server" Style="width: 70%; position: center; display: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="text-align: center">
                        Lịch sử
                    </div>
                    <div class="panel-body" style="max-height: 500px; overflow: auto;">
                        <asp:GridView ID="gvLichSu" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvLichSu_RowDataBound"
                            EmptyDataText="Không có dữ liệu nào" class="table table-bordered" Font-Names="Times new Roman" HeaderStyle-Font-Size="13px">
                            <Columns>
                                <asp:BoundField DataField="NguoiTao" HeaderText="Người tạo" />
                                <asp:BoundField DataField="NgayTao" HeaderText="Ngày tạo" />
                                <asp:BoundField DataField="TenThietBi" HeaderText="Biển số xe" />

<%--                                <asp:BoundField DataField="DiChuyenCoTaiBen" HeaderText="Di chuyển có tải" />
                                <asp:BoundField DataField="DiChuyenKhongTaiBen" HeaderText="Di chuyển không tải" />
                                <asp:BoundField DataField="CauHaHang" HeaderText="Cẩu hạ hàng" />--%>

                                <asp:BoundField DataField="DiChuyenCoTaiBeTong" HeaderText="Di chuyển có tải" />
                                <asp:BoundField DataField="DiChuyenKhongTaiBeTong" HeaderText="Di chuyển không tải" />
                                <asp:BoundField DataField="LayBeTongTaiTram" HeaderText="Lấy bê tông tại trạm" />
                                <asp:BoundField DataField="XaBeTongVeSinh" HeaderText="Xả bê tông, vệ sinh" />
                                <asp:BoundField DataField="NoMayQuayThung" HeaderText="Nổ máy, quay thùng" />

                                <%--<asp:BoundField DataField="DiChuyen" HeaderText="Di chuyển" />
                                <asp:BoundField DataField="BomBeTong" HeaderText="Bơm bê tông" />
                                <asp:BoundField DataField="VeSinhRaChan" HeaderText="Vệ sinh, ra chân" />--%>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Bold="True" Font-Size="11px"
                                HorizontalAlign="Right" />
                            <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#000066" Height="30px" Font-Size="12px" />
                            <SelectedRowStyle BackColor="#afe4ed" Font-Bold="false" />
                        </asp:GridView>
                    </div>
                    <div class="panel-footer" style="align-content: center; text-align: center">
                        <asp:Button ID="btnDongLichSu" runat="server" Text="Đóng" CssClass="btn btn-warning" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
